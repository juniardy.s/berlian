

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
  .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Daftar Kendaraan
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>

          <div class="row">
            <div class="col-md-12">
              <a onclick="print_daftar()" style="margin-top: 5px;" class="btn btn-lg blue "><i class="fa fa-print"></i> Print</a>

            </div>
          </div>
          <br>
          <div class="box-body">            
            <div class="sukses" ></div>
            <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
              <?php
              $this->db->order_by('nama_kendaraan','asc');
              $kendaraan = $this->db->get('master_kendaraan');
              $hasil_kendaraan = $kendaraan->result();
              ?>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Kendaraan</th>
                  <th>Nama Kendaraan</th>
                  <th>No. Kendaraan</th>
                  <th>STNK</th>

                  <th>Status</th>
                  <th>Keterangan</th>
                  <th>Action</th>

                </tr>
              </thead>
              <tbody>
                <?php
                $nomor = 1;

                foreach($hasil_kendaraan as $daftar){ ?> 
                <tr>
                  <td><?php echo $nomor; ?></td>
                  <td><?php echo @$daftar->kode_kendaraan; ?></td>
                  <td><?php echo @$daftar->nama_kendaraan; ?></td>
                  <td><?php echo @$daftar->no_kendaraan; ?></td>
                  <td><?php echo @$daftar->stnk; ?></td>

                  <td><?php echo cek_status(@$daftar->status); ?></td>
                  <td><?php echo @$daftar->keterangan; ?></td>
                  <td align="center"><?php echo get_detail_edit_delete(@$daftar->kode_kendaraan); ?></td>
                </tr>
                <?php $nomor++; } ?>
              </tbody>
              <tfoot>
               <tr>
                <th>No</th>
                <th>Kode Kendaraan</th>
                <th>Nama Kendaraan</th>
                <th>No. Kendaraan</th>
                <th>STNK</th>

                <th>Status</th>
                <th>Keterangan</th>
                <th>Action</th>

              </tr>
            </tfoot>
          </table>


        </div>

        <!------------------------------------------------------------------------------------------------------>

      </div>
    </div>


    <div class="box box-info">


      <div class="box-body">            



      </section><!-- /.Left col -->      
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/daftar/'; ?>";
  });
</script>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data Kendaraan tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>


<script>

  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }

  function delData() {
    var id = $('#id-delete').val();
    var url = '<?php echo base_url().'master/kendaraan/hapus'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id: id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $('#modal-confirm').modal('hide');
        window.location.reload();
      }
    });
    return false;
  }
  function print_daftar(){
    window.open('<?php echo base_url().'master/kendaraan/print_daftar'; ?>/');
  }
  $(document).ready(function(){
    $("#tabel_daftar").dataTable();
  })

</script>