<?php
$get_kode = $this->uri->segment(4);
$table = $this->uri->segment(3);

$this->db->where('kode_bahan_jadi', $get_kode);
if($table == 'table_temp_harga'){
	$get_temp = $this->db->get('opsi_harga_bahan_jadi_temp');
} else{
	$get_temp = $this->db->get('opsi_harga_bahan_jadi');
}
$hasil_temp = $get_temp->result();
$total = 0;
$no = 1;
foreach ($hasil_temp as $temp) {
	?>
	<tr>
		<td><?php echo $no++;?></td>
		<td><?php echo $temp->kategori ?></td>
		<td><?php echo $temp->qty_min ?></td>
		<td><?php echo $temp->qty_max ?></td>
		<td><?php echo format_rupiah($temp->harga);?></td>
		<td width="10%">
			<a style="padding:3.5px;" onclick="modal_harga(<?php echo $temp->id ?>)" data-toggle="tooltip" title="Delete" class="btn btn-icon-only btn-circle red"><i class="fa fa-trash"></i></a>
		</td>
	</tr>
	
	<?php }
	?>


	<script type="text/javascript">
// actDelete(<?php// echo $temp->id ?>)

function modal_harga(Object){
	$('#id-delete_h').val(Object);
	$('#price_harga').modal('show');
}
function actDelete_harga(){
	var id =$('#id-delete_h').val();
	$.ajax({
		type: 'POST',
		<?php 
		if($table == 'table_opsi_harga'){ ?>
			url: "<?php echo base_url() . 'master/bahan_jadi/delete_opsi_harga' ?>",
			<?php 
		} else { ?>
			url: "<?php echo base_url() . 'master/bahan_jadi/delete_temp_harga' ?>",
			<?php
		} ?>
		data: {id:id},
		success: function(data){
			<?php 
			if($table == 'table_opsi_harga'){ ?>
				get_opsi_harga();
				<?php 
			} else { ?>
				get_temp_harga();
				<?php
			} ?>
			$('#price_harga').modal('hide');
		},
		error : function(data) {  
			alert('Sorry');
		}  
	});
}
</script>