

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Form Tambahan Untuk HPP
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>


          <div class="box-body">                   
            <div class="sukses" ></div>
            <form id="data_jabatan"  method="post">         
              <div class="row">  

                <?php
                $uri = $this->uri->segment(4);
                if(!empty($uri)){
                  $data = $this->db->get_where('master_tambahan',array('id'=>$uri));
                  $hasil_data = $data->row();
                  ?>
                  <?php
                }

                ?>
                <input type="hidden" name="id" id="id" value="<?php echo $uri; ?>">
                <div class="form-group  col-xs-4">
                  <label class="gedhi">Item</label>
                  <select class="form-control" id="item" name="item">
                    <option value="">Pilih</option>
                    <option <?php if(@$hasil_data->item=='Finishing'){echo 'selected';} ?> value="Finishing">Finishing</option>
                    <option  <?php if(@$hasil_data->item=='Komisi Supir'){echo 'selected';} ?> value="Komisi Supir">Komisi Supir</option>
                  </select>
                </div>

                <div class="form-group  col-xs-4">
                  <label class="gedhi">Harga (Rp.)</label>
                  <input placeholder="Rp. 0" type="text" class="form-control" value="<?php echo @$hasil_data->harga; ?>" name="harga" id="harga"/>
                </div>



                <div class="form-group  col-xs-4">
                  <label class="gedhi">Status</label>
                  <select class="form-control" id="status" name="status">
                    <option>Pilih</option>
                    <option value="1" <?php if(@$hasil_data->status=='1'){echo "selected";}?>>Aktif</option>
                    <option value="0" <?php if(@$hasil_data->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                  </select>
                </div>                        
              </div>
              <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
              <br>
              <center><button type="submit" class="btn btn-primary btn-lg" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
            </form>
          </div>
        </div>
      </div>

      <!-- /.row (main row) -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'master/tambahan/'; ?>";
    });
  </script>


  <script type="text/javascript">

    $(document).ready(function(){


      //jika tombol Send diklik maka kirimkan data_form ke url berikut
      $("#data_jabatan").submit( function() { 

        $.ajax( {  
          type :"post", 
          <?php 
          if (empty($uri)) {
            ?>
            //jika tidak terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/tambahan/simpan_tambah_tambahan'; ?>",
            <?php }
            else { ?>
            //jika terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/tambahan/simpan_edit_tambahan'; ?>",
            <?php }
            ?>  
            cache :false,  
            data :$(this).serialize(),
            beforeSend:function(){
              $(".tunggu").show();  
            },
            success : function(data) {  
              $(".sukses").html(data);   
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'master/tambahan/' ?>";},1500);              
              },  
              error : function() {  
                alert("Data gagal dimasukkan.");  
              }  
            });
        return false;                          
      });   

      
    });

  </script>