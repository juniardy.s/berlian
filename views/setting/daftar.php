
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            Widget settings form goes here
          </div>
          <div class="modal-footer">
            <button type="button" class="btn blue">Save changes</button>
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->

    <!-- END STYLE CUSTOMIZER -->
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
    Master</h3>
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="#">Home</a>
          <i class="fa fa-angle-right"></i>
        </li>
        <li>
          <a href="#">Master</a>
        </li>
      </ul>
      <div class="page-toolbar">

      </div>
    </div>

    <div class="row">



      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light blue-soft" id="rak">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details" >
            <div class="number">

            </div>
            <div class="desc">
              Kategori
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light red-soft"  id="bahan_baku">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details">
            <div class="number">

            </div>
            <div class="desc">
              Bahan Baku
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light green-soft"  id="bahan_setengah">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details">
            <div class="number">

            </div>
            <div class="desc">
              Bahan Setengah Jadi
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light purple-soft"  id="bahan_jadi">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details">
            <div class="number">

            </div>
            <div class="desc">
              Bahan Jadi
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light blue-soft" id="supplier">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details">
            <div class="number">

            </div>
            <div class="desc">
              Supplier
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light red-soft" id="karyawan">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details" >
            <div class="number">

            </div>
            <div class="desc">
              Karyawan
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light green-soft" id="plasma">
          <div class="visual">
            <i class="glyphicon glyphicon-taskss" ></i>
          </div>
          <div class="details" >
            <div class="number">

            </div>
            <div class="desc">
              Plasma
            </div>
          </div>
        </a>
      </div>
        <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light blue-steel" id="jabatan">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Jabatan
              </div>
            </div>
          </a>
        </div> 
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light green-soft" id="user">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                User
              </div>
            </div>
          </a>
        </div>--> 
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light purple-soft"  id="jalur">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Jalur
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light blue-soft"  id="expedisi">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Expedisi
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light red-soft"  id="member">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Member
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light green-soft"  id="sopir">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Sopir
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light purple-soft"  id="sales">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Sales
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light blue-soft"  id="kendaraan">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Kendaraan
              </div>
            </div>
          </a>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light red-soft"  id="tambahan">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Tambahan Untuk HPP
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light green-soft"  id="inventori">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Inventaris
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a class="dashboard-stat dashboard-stat-light purple-soft"  id="user">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" ></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                User
              </div>
            </div>
          </a>
        </div>
      </div>
      <!-- END DASHBOARD STATS -->
      <div class="clearfix">
      </div>
<div class="row"><!--
<div class="col-md-6 col-sm-6">

<div class="portlet light ">
<div class="portlet-title">
<div class="caption">
<i class="icon-bar-chart font-green-sharp hide"></i>
<span class="caption-subject font-green-sharp bold uppercase">Site Visits</span>
<span class="caption-helper">weekly stats...</span>
</div>
<div class="actions">
<div class="btn-group btn-group-devided" data-toggle="buttons">
<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
<input type="radio" name="options" class="toggle" id="option1">New</label>
<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
<input type="radio" name="options" class="toggle" id="option2">Returning</label>
</div>
</div>
</div>
<div class="portlet-body">
<div id="site_statistics_loading">
<img src="../../assets/admin/layout2/img/loading.gif" alt="loading"/>
</div>
<div id="site_statistics_content" class="display-none">
<div id="site_statistics" class="chart">
</div>
</div>
</div>
</div>

</div>-->


<!--<div class="col-md-6 col-sm-6">
BEGIN PORTLET
<div class="portlet light ">
<div class="portlet-title">
<div class="caption">
<i class="icon-share font-red-sunglo hide"></i>
<span class="caption-subject font-red-sunglo bold uppercase">Revenue</span>
<span class="caption-helper">monthly stats...</span>
</div>
<div class="actions">
<div class="btn-group">
<a href="" class="btn grey-salsa btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
Filter Range&nbsp;<span class="fa fa-angle-down">
</span>
</a>
<ul class="dropdown-menu pull-right">
<li>
<a href="javascript:;">
Q1 2014 <span class="label label-sm label-default">
past </span>
</a>
</li>
<li>
<a href="javascript:;">
Q2 2014 <span class="label label-sm label-default">
past </span>
</a>
</li>
<li class="active">
<a href="javascript:;">
Q3 2014 <span class="label label-sm label-success">
current </span>
</a>
</li>
<li>
<a href="javascript:;">
Q4 2014 <span class="label label-sm label-warning">
upcoming </span>
</a>
</li>
</ul>
</div>
</div>
</div>
<div class="portlet-body">
<div id="site_activities_loading">
<img src="../../assets/admin/layout2/img/loading.gif" alt="loading"/>
</div>
<div id="site_activities_content" class="display-none">
<div id="site_activities" style="height: 228px;">
</div>
</div>
<div style="margin: 20px 0 10px 30px">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
<span class="label label-sm label-success">
Revenue: </span>
<h3>$13,234</h3>
</div>
<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
<span class="label label-sm label-danger">
Shipment: </span>
<h3>$1,134</h3>
</div>
<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
<span class="label label-sm label-primary">
Orders: </span>
<h3>235090</h3>
</div>
</div>
</div>
</div>
</div>

</div>-->

</div>
<!--  -->                                        
<!-- END QUICK SIDEBAR -->
</div>    
</div>
<script type="text/javascript">
  $(document).ready(function(){

    $("#karyawan").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/karyawan/' ?>";

    });
    $("#plasma").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/plasma/' ?>";

    });
    $("#sopir").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/sopir/' ?>";

    });
    $("#sales").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/sales/' ?>";

    });
    $("#kendaraan").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/kendaraan/' ?>";

    });

    $("#rak").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/rak/' ?>";

    });


    $("#bahan_baku").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/bahan_baku/' ?>";
    });

    $("#bahan_setengah").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/bahan_setengah/' ?>";
    });

    $("#bahan_jadi").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/bahan_jadi/' ?>";
    });

    $("#user").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/user/' ?>";
    });

    $("#jabatan").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/jabatan/' ?>";
    });

    $("#supplier").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/supplier/' ?>";
    });
    $("#produk").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/produk/' ?>";
    });
    $("#member").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/member/' ?>";
    });

    $("#tambahan").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/tambahan/' ?>";
    });
    $("#inventori").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/inventori/' ?>";
    });
    $("#jalur").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/jalur/' ?>";
    });
    $("#expedisi").click(function(){
      $('.tunggu').show();
      window.location = "<?php echo base_url() . 'master/expedisi/' ?>";
    });
  });
</script>
