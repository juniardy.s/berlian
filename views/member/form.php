<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah Data Member
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>

        <?php
        $param = $this->uri->segment(4);
        //echo $param;
        if(!empty($param)){
          $member = $this->db->get_where('master_member',array('id'=>$param));
          $hasil_member = $member->row();
          //echo $hasil_bahan_baku->kode_barang;
        }    
        $this->db->select_max('id');
        $get_max_member = $this->db->get('master_member');
        $max_member = $get_max_member->row();

        $this->db->where('id', $max_member->id);
        $get_member = $this->db->get('master_member');
        $member = $get_member->row();
        $nomor = substr(@$member->kode_member, 4);
        $nomor = $nomor + 1;
        $string = strlen($nomor);
        if($string == 1){
          $kode_member ='000'.$nomor;
        } else if($string == 2){
          $kode_member ='00'.$nomor;
        } else if($string == 3){
          $kode_member ='0'.$nomor;
        } else if($string == 4){
          $kode_member =''.$nomor;
        } 
        // else if($string == 5){
        //   $kode_member ='0'.$nomor;
        // } else if($string == 6){
        //   $kode_member =''.$nomor;
        // }

        ?>
        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">


                <div class="form-group  col-xs-6">
                  <label><b>Kode Member</label>
                  <div class="">
                    <input readonly name="kode_member" value="<?php if(!empty($param)){echo @$hasil_member->kode_member; }else{ echo "CST_".$kode_member;} ?>"   type="text" class="form-control" id="kode_member" />
                  </div>
                </div>

                <div class="form-group  col-xs-6">
                  <label><b>Nama Member</label>
                  <div class="">
                    <input name="nama_member" required="" value="<?php echo @$hasil_member->nama_member ?>"  type="text" class="form-control" id="nama_member" />
                  </div>
                </div>

                <div class="form-group  col-xs-6">
                  <label><b>Alamat</label>
                  <div class="">
                    <input name="alamat_member" required="" value="<?php echo @$hasil_member->alamat_member ?>"   type="text" class="form-control" id="alamat_member" />
                  </div>
                </div>

                <div class="form-group  col-xs-6">
                  <label><b>Telepon</label>
                  <div class="">
                    <input name="telp_member" required="" value="<?php echo @$hasil_member->telp_member ?>"  type="text" class="form-control" id="telp_member" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label><b>Kota</label>
                  <div class="">
                    <input name="kota" required="" value="<?php echo @$hasil_member->kota ?>"  type="text" class="form-control" id="kota" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label><b>Provinsi</label>
                  <div class="">
                    <input name="provinsi" required="" value="<?php echo @$hasil_member->provinsi ?>"  type="text" class="form-control" id="provinsi" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label><b>E-mail</label>
                  <div class="">
                    <input name="email" required="" value="<?php echo @$hasil_member->email ?>"  type="text" class="form-control" id="email" />
                  </div>
                </div>

                <div class="form-group  col-xs-6">
                  <label><b>Keterangan</label>
                  <div class="">
                    <!-- <input name="keterangan" value="<?php echo @$hasil_member->keterangan ?>"  type="text" class="form-control" id="keterangan" /> -->
                    <textarea class="form-control" required="" name="keterangan" id="keterangan"><?php echo @$hasil_member->keterangan; ?></textarea>
                  </div>
                </div>

                <div class="form-group  col-xs-6">
                  <label><b>Kategori Member</label>
                  <div class="">
                   <select class="form-control select2" id="kategori" name="kategori_member" required="">
                     <option <?php if(@$hasil_member->kategori_member==''){ echo 'selected'; } ?> value="">Pilih</option>
                     <!--   <option <?php if(@$hasil_member->kategori_member=='reguler'){ echo 'selected'; } ?> value="reguler">Reguler</option> -->
                     <option <?php if(@$hasil_member->kategori_member=='member'){ echo 'selected'; } ?> value="member">Member</option>
                     <option <?php if(@$hasil_member->kategori_member=='agen'){ echo 'selected'; } ?> value="agen">Agen</option>
                     <option <?php if(@$hasil_member->kategori_member=='kanvas'){ echo 'selected'; } ?> value="kanvas">Kanvas</option>
                     <option <?php if(@$hasil_member->kategori_member=='supermarket'){ echo 'selected'; } ?> value="supermarket">Supermarket</option>
                     <option <?php if(@$hasil_member->kategori_member=='konsinyasi'){ echo 'selected'; } ?> value="konsinyasi">Konsinyasi ( Toko )</option>
                   </select>
                 </div>
               </div>
               <div class="form-group  col-xs-6">
                <label class="gedhi"><b>Jalur</label>
                <?php

                $kode_unit = @$hasil_unit->kode_unit;
                $get_jalur = $this->db->get_where('master_jalur',array('status'=>'1'));
                $hasil_get_jalur = $get_jalur->result();
                  //echo $hasil_bahan_baku->kode_jalur;

                ?>
                <select  name="kode_jalur" id="kode_jalur" class="form-control select2" required="">
                  <option selected="true" value="">--Pilih Jalur--</option>
                  <?php 
                  foreach($hasil_get_jalur as $daftar){    
                    ?>
                    <option <?php if(@$hasil_member->kode_jalur==$daftar->kode_jalur){ echo "selected"; } ?> value="<?php echo $daftar->kode_jalur; ?>"><?php echo $daftar->nama_jalur; ?></option>
                    <?php  } ?>
                  </select>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Expedisi</label>
                  <?php

                  $kode_unit = @$hasil_unit->kode_unit;
                  $this->db->order_by('nama_expedisi','asc');
                  $get_expedisi = $this->db->get_where('master_expedisi',array('status'=>'1'));
                  $hasil_get_expedisi = $get_expedisi->result();
                  //echo $hasil_member->kode_expedisi;

                  ?>
                  <select  name="kode_expedisi" id="kode_expedisi" class="form-control select2" required="">
                    <option selected="true" value="">--Pilih Expedisi--</option>
                    <?php 
                    foreach($hasil_get_expedisi as $daftar){    
                      ?>
                      <option <?php if(@$hasil_member->kode_expedisi==$daftar->kode_expedisi){ echo "selected"; } ?> value="<?php echo $daftar->kode_expedisi; ?>"><?php echo $daftar->nama_expedisi; ?></option>
                      <?php  } ?>
                    </select>
                  </div>
                 <div class="form-group  col-xs-6">
                  <label><b>Sales Pendaftar</label>
                  <div class="">
                    <select class="select2 form-control" id="registered_sales" name="registered_sales">
                        <?php 
                          $q= $this->db->get_where('master_sales' , ['status' => 1])->result();
                          foreach($q as $fx){     
                         ?>
                         <option <?php if(@$hasil_member->registered_sales==$fx->kode_sales){ echo 'selected'; } ?> value="<?php echo $fx->kode_sales ?>"><?php echo $fx->kode_sales . ' - '. $fx->nama_sales ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
                  <div class="form-group  col-xs-6">
                    <label><b>Status Member</label>
                    <div class="">
                     <select class="form-control" id="status" name="status_member" required="">
                       <option <?php if(@$hasil_member->status_member==''){ echo 'selected'; } ?> value="">Pilih</option>
                       <option <?php if(@$hasil_member->status_member=='1'){ echo 'selected'; } ?> value="1">Aktif</option>
                       <option <?php if(@$hasil_member->status_member=='0'){ echo 'selected'; } ?> value="0">Tidak Aktif</option>
                     </select>
                   </div>
                 </div>


               </div>
               <div class="box-footer">
                <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
                <br>
                <center><button type="submit" class="btn btn-primary btn-lg" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
 $(document).ready(function() {
  $(".select2").select2();
});
 $('.btn-back').click(function(){
  $(".tunggu").show();
  window.location = "<?php echo base_url().'master/member/daftar'; ?>";
});
</script>
<script type="text/javascript">
  $(function(){

    $("#data_form").submit( function() {
     <?php if(!empty($param)){ ?>
      var url = "<?php echo base_url(). 'master/member/simpan_ubah'; ?>";  
      <?php }else{ ?>
        var url = "<?php echo base_url(). 'master/member/simpan_tambah'; ?>";
        <?php } ?>
        $.ajax( {
         type:"POST", 
         url : url,  
         cache :false,  
         data :$(this).serialize(),
         beforeSend: function(){
           $(".loading").show(); 
         },
         beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
        //if(data=="sukses"){
         $(".sukses").html('<div class="alert alert-success">Data Berhasil Disimpan</div>');
         setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/member/daftar' ?>?jalur=" + $('#kode_jalur').val();},1000);  
        // }
        // else{
        //     $(".sukses").html(data);
        // }
        $(".loading").hide();   

      },  
      error : function(data) {  
        alert(data);  
      }  
    });
        return false;                    
      }); 
  })
</script>