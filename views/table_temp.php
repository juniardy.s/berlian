<?php
$get_kode = $this->uri->segment(4);
$table = $this->uri->segment(3);

$this->db->where('kode_plasma', $get_kode);
if($table == 'table_temp'){
	$get_temp = $this->db->get('opsi_bahan_plasma_temp');
} else{
	$get_temp = $this->db->get('opsi_bahan_plasma');
}
$hasil_temp = $get_temp->result();
$total = 0;
$no = 1;
foreach ($hasil_temp as $temp) {
	?>
	<tr>
		<td><?php echo $no ?></td>
		<td><?php echo $temp->nama_bahan ?></td>
		<td><?php echo format_rupiah($temp->harga) ?></td>

		<td width="10%">
			<a style="padding:3.5px;" onclick="modal(<?php echo $temp->id ?>)" data-toggle="tooltip" title="Delete" class="btn btn-icon-only btn-circle red"><i class="fa fa-trash"></i></a>
		</td>
	</tr>
	<?php 
	$total += $temp->harga;
	$no++;
}
?>


<script type="text/javascript">
// actDelete(<?php// echo $temp->id ?>)

var total = $("#total_hpp").val();
$("#hpp").val(total);
function modal(Object){
	$('#id-delete').val(Object);
	$('#modal-confirm').modal('show');
}
function get_temp(){
	kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
	$("#opsi_bahan_jadi").load('<?php echo base_url().'master/bahan_jadi/table_temp/'; ?>'+kode_bahan_jadi);
}
function get_opsi(){
	kode_bahan_jadi = $("#kode_bahan_jadi").val();
	$("#opsi_bahan_jadi").load('<?php echo base_url().'master/bahan_jadi/table_opsi/'; ?>'+kode_bahan_jadi);
}
function actDelete(){
	var id =$('#id-delete').val();
	$.ajax({
		type: 'POST',
		<?php 
		if($table == 'table_opsi'){ ?>
			url: "<?php echo base_url() . 'master/plasma/delete_opsi' ?>",
			<?php 
		} else { ?>
			url: "<?php echo base_url() . 'master/plasma/delete_temp' ?>",
			<?php
		} ?>
		data: {id:id},
		success: function(data){

			kode_plasma = $("#kode_plasma").val();
			<?php
			if($table == 'table_temp'){ ?>
				$("#opsi_plasma").load('<?php echo base_url().'master/plasma/table_temp/'; ?>'+kode_plasma);
				<?php 
			}else{ ?>
				
				$("#opsi_plasma").load('<?php echo base_url().'master/plasma/table_opsi/'; ?>'+kode_plasma);
				<?php } ?>
				$('#modal-confirm').modal('hide');

			},
			error : function(data) {  
				alert('Sorry');
			}  
		});
}
</script>