
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Bahan Baku
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
        <div class="row">

          <div class="col-md-4">
            <div class="form-group">
              <label>Nama Bahan Baku</label>
              <input type="text" class="form-control" id="nama_produk" />
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Kategori</label>
              <?php 
                $hasil = $this->db->query('SELECT * FROM `master_rak` where status = 1')->result();
               ?>
             <select class="select2 form-control" name="kategori" id="kategori">
              <?php foreach($hasil as $v){ ?>
                <option value="<?= $v->kode_rak ?>"><?= $v->nama_rak ?></option>
              <?php } ?>
             </select>
            </div>
          </div>
          <div class="col-md-3">
            <a onclick="cari_produk()" style="margin-top: 25px;" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>

          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <a onclick="print_produk()" style="margin-top: 25px;" class="btn btn-lg blue pull-right"><i class="fa fa-print"></i> Print</a>

          </div>
        </div>

        <div class="box-body">            
          <div class="sukses" ></div>
          <div id="hasil_cari">
            <table  class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">

              <?php
              $kode_default = $this->db->get('setting_gudang');
              $hasil_unit =$kode_default->row();
              $param=$hasil_unit->kode_unit;
              $this->db->limit(50);
              $this->db->where('status','sendiri');
              $this->db->order_by('nama_bahan_baku','asc');
              $bahan_baku = $this->db->get_where('master_bahan_baku',array('kode_unit' => $param));
              $hasil_bahan_baku = $bahan_baku->result();
              ?>

              <thead>
                <tr width="100%">
                  <th>No</th>
                  <th>Kode Bahan Baku</th>
                  <th>Nama Bahan Baku</th>
                  <th>Unit</th>
                  <th>Kategori</th>
                  <th>Satuan Pembelian</th>
                  <th>Satuan Penggunaan</th>
                  <th style="width:50px">Isi Dalam 1 <br>(Satuan Pembelian)</th>
                  <th>Stok Minimal</th>
                  <th>HPP</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="daftar_bahan_baku">
                <?php
                $nomor=1;
                foreach($hasil_bahan_baku as $daftar){
                  ?>
                  <tr>
                    <td><?php echo $nomor; ?></td>
                    <td><?php echo $daftar->kode_bahan_baku; ?></td>
                    <td><?php echo $daftar->nama_bahan_baku; ?></td>

                    <td><?php echo $daftar->nama_unit; ?></td>
                    <td><?php echo $daftar->nama_rak; ?></td>
                    <td><?php echo $daftar->satuan_pembelian; ?></td>
                    <td><?php echo $daftar->satuan_stok; ?></td>
                    <td><?php echo $daftar->jumlah_dalam_satuan_pembelian; ?></td>
                    <td><?php echo $daftar->stok_minimal; ?></td>
                    <td><?php echo format_rupiah($daftar->hpp); ?></td>
                    <td><?php echo get_detail_edit_delete_string($daftar->id); ?></td>
                  </tr>
                  <?php $nomor++; } ?>
                </tbody>
                <tfoot>
                  <tr>
                   <th>No</th>
                   <th>Kode Bahan Baku</th>
                   <th>Nama Bahan Baku</th>
                   <th>Unit</th>
                   <th>Kategori</th>
                   <th>Satuan Pembelian</th>
                   <th>Satuan Penggunaan</th>
                   <th style="width:50px">Isi Dalam 1 <br>(Satuan Pembelian)</th>
                   <th>Stok Minimal</th>
                   <th>HPP</th>
                   <th>Action</th>
                 </tr>
               </tfoot>
             </table>
             <?php 
             $get_jumlah = $this->db->get_where('master_bahan_baku', array('kode_unit' => $param,'status' => 'sendiri'));
             $jumlah = $get_jumlah->num_rows();
             $jumlah = floor($jumlah/50);
             ?>
             <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
             <input type="hidden" class="form-control pagenum" value="0">
           </div>
         </div>

         <!------------------------------------------------------------------------------------------------------>

       </div>
     </div>
   </div><!-- /.col -->
 </div>
</div>    
</div>  


<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data bahan baku tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/daftar/'; ?>";
  });
</script>

<script>
  $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function load_table(page){
    var kategori = $("#kategori").val();
    var nama_produk = $("#nama_produk").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'master/bahan_baku/get_table' ?>",
      data: ({kategori: kategori,nama_produk:nama_produk, page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#daftar_bahan_baku").append(msg);

      }
    });
  }
</script>

<script>
  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }

  function delData() {
    var id = $('#id-delete').val();
    var url = '<?php echo base_url().'master/bahan_baku/hapus'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id: id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $('#modal-confirm').modal('hide');
            // alert(id);
            window.location.reload();
          }
        });
    return false;
  }

  function cari_produk(){
    var kategori = $("#kategori").val();
    var nama_produk = $("#nama_produk").val();
    var url = '<?php echo base_url().'master/bahan_baku/get_produk'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        kategori: kategori,nama_produk:nama_produk
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $("#hasil_cari").html(msg);
        $(".tunggu").hide();
      }
    });
    
  }
  function print_produk(){

    var nama_produk = $("#nama_produk").val();
    window.open('<?php echo base_url().'master/bahan_baku/print_produk'; ?>/'+nama_produk);
  }

  $(document).ready(function(){
    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": true,
      "searching": false
    });
  })

</script>
