<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah Data produk
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>

        <?php
        $param = $this->uri->segment(4);
        //echo $param;
        if(!empty($param)){
          $produk = $this->db->get_where('master_produk',array('id'=>$param));
          $hasil_produk = $produk->row();
          //echo $hasil_produk->kode_produk;

        }    

        ?>
        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">


                <div class="form-group  col-xs-6">
                  <label><b>Kode produk</label>
                  <div class="">
                    <input name="kode_produk" value="<?php echo @$hasil_produk->kode_produk ?>"  <?php if(!empty($param)){ echo "readonly='true'"; } ?> type="text" class="form-control" id="kode_produk" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Nama produk</label>
                  <input value="<?php echo @$hasil_produk->nama_produk; ?>" type="text" class="form-control" name="nama_produk" />
                </div>
               
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Unit</label>
                  <?php
                  $kode_default = $this->db->get('setting_gudang');
                  $hasil_unit =$kode_default->row();
                  $kode_unit=$hasil_unit->kode_unit;

                  $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
                  $hasil_unit = $unit->row();
                  ?>
                  <!-- <select class="form-control select2" style="width: 100%;" name="kode_unit" id="kode_unit">
                    <option selected="true" value="">--Pilih Unit--</option>
                    <?php foreach($hasil_unit as $item){ ?>
                    <option <?php if(@$hasil_produk->kode_unit==$item->kode_unit){ echo "selected"; } ?> value="<?php echo $item->kode_unit ?>"><?php echo $item->nama_unit ?></option>
                    <?php } ?> 
                  </select> -->
                   <input value="<?php echo @$hasil_unit->nama_unit; ?>" type="text" class="form-control" name="nama_unit" readonly />
                  <input value="<?php echo @$hasil_unit->kode_unit; ?>" type="hidden" class="form-control" name="kode_unit" />
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Rak</label>
                  <?php
                  
                    $kode_unit = @$hasil_unit->kode_unit;
                    $get_rak = $this->db->get_where('master_rak',array('kode_unit'=>$kode_unit));
                    $hasil_get_rak = $get_rak->result();
                  //echo $hasil_produk->kode_rak;
                  
                  ?>
                  <select name="kode_rak" id="kode_rak" class="form-control">
                    <option selected="true" value="">--Pilih Rak--</option>
                    <?php 
                      foreach($hasil_get_rak as $daftar){    
                        ?>
                        <option <?php if(@$hasil_produk->kode_rak==$daftar->kode_rak){ echo "selected"; } ?> value="<?php echo $daftar->kode_rak; ?>"><?php echo $daftar->nama_rak; ?></option>
                        <?php  } ?>
                      </select>
                    </div>
                   <!--  <div class="form-group  col-xs-6">
                      <label class="gedhi"><b>Satuan Pembelian</label>
                      <?php
                      $pembelian = $this->db->get('master_satuan');
                      $hasil_pembelian = $pembelian->result();
                      ?>
                      <select class="form-control select2 pembelian" name="id_satuan_pembelian">
                        <option selected="true" value="">--Pilih satuan pembelian--</option>
                        <?php foreach($hasil_pembelian as $daftar){ ?>
                        <option <?php if(@$hasil_produk->id_satuan_pembelian==$daftar->kode){ echo "selected"; } ?> value="<?php echo $daftar->kode; ?>" ><?php echo $daftar->nama; ?></option>
                        <?php } ?>
                      </select> 
                    </div> -->
                    <div class="form-group  col-xs-6">
                      <?php
                     
                        
                        $dft_satuan = $this->db->get_where('master_satuan');
                        $hasil_dft_satuan = $dft_satuan->result();
                    
                     
                      ?>
                      <label class="gedhi"><b>Satuan Stok</label>
                     
                      <select class="form-control stok select2" name="id_satuan_stok">
                        <option selected="true" value="">--Pilih satuan stok--</option>
                        <?php 
                          foreach($hasil_dft_satuan as $daftar){ 
                          
                            ?>
                            <option <?php if(@$hasil_produk->id_satuan_stok==$daftar->kode){ echo "selected"; } ?> value="<?php echo $daftar->kode; ?>"><?php echo $daftar->nama; ?></option>
                            <?php  }  ?>

                          </select> 
                        </div>
                       
                       
                       
                        <div class="form-group  col-xs-6">
                          <label class="gedhi"><b>Formulasi</label>
                          <input  type="text" class="form-control" name="formulasi" value="<?php echo @$hasil_produk->formulasi ?>" />
                        </div>
                        
                         <div class="form-group  col-xs-6">
                          <label class="gedhi"><b>HPP</label>
                          <input type="text" class="form-control" name="hpp" id="hpp" value="<?php echo @$hasil_produk->hpp ?>" />
                          <h3><div id="nilai_hpp"></div></h3>
                        </div>
                        
                        <div class="form-group  col-xs-6">
                          <label class="gedhi"><b>Harga Jual</label>
                          <input type="text" class="form-control" name="harga_jual" id="harga_jual" value="<?php echo @$hasil_produk->harga_jual ?>" />
                          <h3><div id="nilai_jual"></div></h3>
                        </div>
                        
                        <div class="form-group  col-xs-6">
                          <label class="gedhi"><b>Status</label>
                          <select class="form-control" id="status" name="status">
                            <option>--Pilih Status--</option>
                            <option value="1" <?php if(@$hasil_produk->status=='1'){echo "selected";}?>>Aktif</option>
                            <option value="0" <?php if(@$hasil_produk->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                          </select>
                        </div>
                      </div>
                      <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
$(".tunggu").show();
            window.location = "<?php echo base_url().'master/produk/'; ?>";
          });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
  //$(".select2").select2();
 //   $.ajax( {  
 //              type :"post",  
 //              url : "<?php echo base_url('master/produk/get_satuan_stok'); ?>",  
 //              cache :false,  
 //              data :({ id_pembelian:$(this).val()}),
            
 // success : function(data) {
 //                $(".stok").empty();
 //                $(".stok").html(data);   
 //              },  
 //              error : function(data) {  
 //                alert("das");  
 //              }  
 //            });
 //            return false;
});
        $(function(){

          $('#kode_produk').on('change',function(){
            var kode_produk = $('#kode_produk').val();
            //var kode_setting = $('#kode_setting').val();
            //var kode_produk = kode_setting + "_" + kode_input ;
            var url = "<?php echo base_url() . 'master/produk/get_kode' ?>";
            $.ajax({
              type: 'POST',
              url: url,
              data: {kode_produk:kode_produk},
              success: function(msg){
                if(msg == 1){
                  $(".sukses").html('<div class="alert alert-warning">Kode_Telah_dipakai</div>');
                  setTimeout(function(){
                    $('.sukses').html('');
                  },1700);              
                  $('#kode_produk').val('');

                }
                else{

                }
              }
            });
          });

          $(".pembelian").change(function(){

           
          });
$("#hpp").keyup(function(){
  var temp_data = "<?php echo base_url().'master/produk/get_rupiah/'?>";
  var hpp = $('#hpp').val() ;
  $.ajax({
    type: "POST",
    url: temp_data,
    data: {hpp:hpp},
    success: function(hasil) {              
      $("#nilai_hpp").html(hasil);
    }
  });

});

$("#harga_jual").keyup(function(){
  var temp_data = "<?php echo base_url().'master/produk/get_rupiah/'?>";
  var hpp = $('#harga_jual').val() ;
  $.ajax({
    type: "POST",
    url: temp_data,
    data: {hpp:hpp},
    success: function(hasil) {              
      $("#nilai_jual").html(hasil);
    }
  });

});

          $("#data_form").submit( function() {
       /* var vv = $(this).serialize();
       alert(vv);*/
       <?php if(!empty($param)){ ?>
        var url = "<?php echo base_url(). 'master/produk/simpan_ubah'; ?>";  
        <?php }else{ ?>
          var url = "<?php echo base_url(). 'master/produk/simpan_tambah'; ?>";
          <?php } ?>
          $.ajax( {
           type:"POST", 
           url : url,  
           cache :false,  
           data :$(this).serialize(),
           beforeSend: function(){
             $(".loading").show(); 
           },
           beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) {
        //if(data=="sukses"){
         $(".sukses").html('<div class="alert alert-success">Data Berhasil Disimpan</div>');
         setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/produk/daftar' ?>";},1000);  
        // }
        // else{
        //     $(".sukses").html(data);
        // }
        $(".loading").hide();   

      },  
      error : function(data) {  
        alert(data);  
      }  
    });
          return false;                    
        }); 
})
</script>