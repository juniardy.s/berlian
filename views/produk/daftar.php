
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Produk
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
<div class="row">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label>Nama Produk</label>
                    <input type="text" class="form-control" id="nama_produk" />
                </div>
            </div>
            <div class="col-md-3">
            <a onclick="cari_produk()" style="margin-top: 25px;" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>
                
            </div>
        </div>

        <div class="box-body">            
          <div class="sukses" ></div>
          <div id="hasil_cari">
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
                <th width="50px;">No</th>
                <th>Kode Produk</th>
                <th>Nama Produk</th>
                <th>HPP</th>
                <th>Hrga Jual</th>
                <th>Status</th>

                <th width="220px">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $kode_default = $this->db->get('setting_gudang');
              $hasil_unit =$kode_default->row();
              $param=$hasil_unit->kode_unit;


              $get_master_produk = $this->db->get_where('master_produk', array('kode_unit' => $param));
              $hasil_master_produk = $get_master_produk->result();

              $no = 1;
              foreach($hasil_master_produk as $item){
                if($this->session->flashdata('message')==$item->kode_produk){

                  echo '<tr id="warna" style="background: #88cc99; display: none;">';
                }
                else{
                  echo '<tr>';
                }
                ?>
                <td><?php echo $no;?></td>
                <td><?php echo $item->kode_produk; ?></td>                  
                <td><?php echo $item->nama_produk; ?></td>
                <td><?php echo format_rupiah($item->hpp); ?></td>
                <td><?php echo format_rupiah($item->harga_jual); ?></td>                  
                <td><?php echo cek_status($item->status); ?></td>
                <td align="center"><?php echo get_detail_edit_delete_string($item->id); ?></td>
              </tr>
              <?php
              $no++;
            } ?>
          </tbody>                
        </table>
            <?php 
           $get_jumlah = $this->db->get_where('master_produk', array('kode_unit' => $param));
           $jumlah = $get_jumlah->num_rows();
           $jumlah = floor($jumlah/50);
           ?>
           <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
           <input type="hidden" class="form-control pagenum" value="0">
        </div>
      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
</div>    
</div>  

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data Produk tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
$('.btn-back').click(function(){
$(".tunggu").show();
  window.location = "<?php echo base_url().'master/daftar'; ?>";
});
</script>


<script>
$(document).ready(function() {

  setTimeout(function(){
    $("#warna").fadeIn('slow');
  }, 1000);
  $("a#hapus").click( function() {    
    var r =confirm("Anda yakin ingin menghapus data ini ?");
    if (r==true)  
    {
      $.ajax( {  
        type :"post",  
        url :"<?php echo base_url() . 'master/produk/hapus' ?>",  
        cache :false,  
        data :({key:$(this).attr('key')}),
        beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
          $(".sukses").html(data);   
          setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/produk/daftar' ?>";},1500);              
        },  
        error : function() {  
          alert("Data gagal dimasukkan.");  
        }  
      });
      return false;
    }
    else {}        
  });

  
  $("#tabel_daftar").dataTable({
    "paging":   false,
    "ordering": true,
    "info":     false,
    "searching": false
  });
} );
setTimeout(function(){
  $("#warna").css("background-color", "white");
  $("#warna").css("transition", "all 3000ms linear");
}, 3000);
function actDelete(Object) {
  $('#id-delete').val(Object);
  $('#modal-confirm').modal('show');
}
function delData() {
  var key = $('#id-delete').val();
  var url = '<?php echo base_url().'master/produk/hapus'; ?>/delete';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      key: key
    },
    beforeSend:function(){
          $(".tunggu").show();  
        },
success: function(msg) {
      $('#modal-confirm').modal('hide');
            // alert(id);
            window.location.reload();
          }
        });
  return false;
}

</script>
<script>
    $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function load_table(page){
    var kategori = $("#kategori").val();
    var nama_produk = $("#nama_produk").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'master/produk/get_table' ?>",
      data: ({kategori: kategori,nama_produk:nama_produk, page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#posts").append(msg);

      }
    });
  }
  
  function cari_produk(){
    var kategori = $("#kategori").val();
    var nama_produk = $("#nama_produk").val();
    var url = '<?php echo base_url().'master/produk/get_produk'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        kategori: kategori,nama_produk:nama_produk
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $("#hasil_cari").html(msg);
        $(".tunggu").hide();
          }
        });
    
  }
</script>
