

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
    .ombo{
      width: 400px;
    } 

  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Form Plasma
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>

            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>

            <div class="box-body">                   
              <div class="sukses" ></div>
              <form id="data_jabatan"  method="post">         
                <div class="row">  

                  <?php
                  $uri = $this->uri->segment(4);
                  if(!empty($uri)){
                    $data = $this->db->get_where('master_plasma',array('kode_plasma'=>$uri));
                    $hasil_data = $data->row();
                    ?>
                    <?php
                  }

                  $this->db->select_max('id');
                  $get_max_member = $this->db->get('master_plasma');
                  $max_member = $get_max_member->row();

                  $this->db->where('id', $max_member->id);
                  $get_member = $this->db->get('master_plasma');
                  $member = $get_member->row();
                  $nomor = substr(@$member->kode_plasma, 4);
                  $nomor = $nomor + 1;
                  $string = strlen($nomor);
                  if($string == 1){
                    $kode_plasma ='000'.$nomor;
                  } else if($string == 2){
                    $kode_plasma ='00'.$nomor;
                  } else if($string == 3){
                    $kode_plasma ='0'.$nomor;
                  } else if($string == 4){
                    $kode_plasma =''.$nomor;
                  } 
                  ?>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Kode Plasma</label>
                    <input type="hidden" name="id" value="<?php echo @$hasil_data->id ?>" />
                    <input readonly="" <?php if(!empty($uri)){ echo "readonly='true'"; } ?> type="text" class="form-control" value="<?php if(!empty($uri)){echo @$hasil_data->kode_plasma;}else{ echo "PLM_".$kode_plasma;} ?>" name="kode_plasma" id="kode_plasma"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Nama Plasma</label>
                    <input type="text" class="form-control" value="<?php echo @$hasil_data->nama_plasma; ?>" name="nama_plasma" id="nama_plasma"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Alamat</label>
                    <input type="text" class="form-control" value="<?php echo @$hasil_data->alamat; ?>" name="alamat" id="alamat"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Telepon</label>
                    <input type="text" class="form-control" value="<?php echo @$hasil_data->telp; ?>" name="telp" id="telp"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Keterangan</label>
                    <!-- <input type="text" class="form-control" value="<?php echo @$hasil_data->keterangan; ?>" name="keterangan" id="keterangan"/> -->
                    <textarea class="form-control"  name="keterangan" id="keterangan"><?php echo @$hasil_data->keterangan; ?></textarea>
                  </div>  

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Status Plasma</label>
                    <select class="form-control" id="status" name="status">
                      <option>Pilih</option>
                      <option value="1" <?php if(@$hasil_data->status=='1'){echo "selected";}?>>Aktif</option>
                      <option value="0" <?php if(@$hasil_data->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                    </select>
                  </div>
                  <div class="form-group  col-xs-12">
                    <div class="row">
                      
                      <div class="form-group  col-xs-2">
                        <label class="gedhi"><b>Jenis Bahan</label>
                        <input type="text" class="form-control" name="jenis_bahan" value="bahan setengah jadi" placeholder="bahan setengah jadi" readonly="" id="jenis_bahan" />
                      </div>



                      <div class="col-md-3">
                        <div class="form-group">
                        <label>Nama Bahan Setengah Jadi</label>
                          <?php
                          $bahan = $this->db->get_where('master_bahan_setengah_jadi',array('status' => 'sendiri','jenis_bahan' => 'plasma'));
                          $bahan = $bahan->result();
                          ?>
                          <select class="form-control select2" name="bahan" id="bahan">
                          <option selected="true" value="">--Pilih bahan--</option>
                          <?php foreach($bahan as $daftar){ ?>
                          <option value="<?php echo $daftar->kode_bahan_setengah_jadi ?>"><?php echo $daftar->nama_bahan_setengah_jadi ?></option>
                            <?php } ?>
                          </select> 
                        </div>
                      </div>
                      <div class="form-group  col-xs-2">
                        <label class="gedhi"><b>Harga</label>
                        <input type="text" class="form-control" name="harga" id="harga" />
                      </div>

                      <div class="form-group  col-xs-2" hidden>
                        <label class="gedhi"><b>Satuan</label>
                        <input readonly="" type="text" class="form-control" name="nama_satuan" id="nama_satuan" value=""/>
                        <input type="hidden" class="form-control" name="id_satuan" id="id_satuan" value=""/>
                      </div>
                      <div class="form-group  col-xs-2" hidden>
                        <label class="gedhi"><b>HPP</label>
                        <input readonly="" type="text" class="form-control" id="hpp_bb" value=""/>

                      </div>
                      <div style="margin-top: 25px;" class="form-group  col-xs-2">

                        <button type="button" id="add-item" class="btn btn-primary btn-md "><i class="fa fa-plus"></i> Add</button>
                      </div>
                      <div class="form-group  col-xs-12">
                        <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Bahan</th>
                              <th>Harga</th>

                              <th width="10%">Action</th>
                            </tr>
                          </thead>
                          <tbody id="opsi_plasma">
                          </tbody>
                        </table>
                      </div>
                    </div>

                  </div>                        
                </div>
                <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
                <br>
                <center><button type="submit" class="btn btn-primary btn-lg" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
              </form>
            </div>
          </div>
        </div>

        <!-- /.row (main row) -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'master/plasma/'; ?>";
      });
    </script>
    <div id="modal-confirm-temp" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color:grey">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
          </div>
          <div class="modal-body">
            <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data komposisi tersebut ?</span>
            <input id="id-delete" type="hidden">
          </div>
          <div class="modal-footer" style="background-color:#eee">
            <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
            <button onclick="delDataTemp()" class="btn red">Ya</button>
          </div>
        </div>
      </div>
    </div>
    <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color:grey">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
          </div>
          <div class="modal-body">
            <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data bahan jadi tersebut ?</span>
            <input id="id-delete" type="hidden">
          </div>
          <div class="modal-footer" style="background-color:#eee">
            <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
            <button onclick="actDelete()" class="btn green">Ya</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">

      $(document).ready(function(){

       <?php 
       if(!empty($uri)){ ?>
        get_opsi();
        <?php 
      }else{ ?>
        get_temp();
        <?php } ?>

        $('#kode_jabatan').on('change',function(){
          var kode_jabatan = $('#kode_jabatan').val();
          var url = "<?php echo base_url() . 'master/jabatan/get_kode' ?>";
          $.ajax({
            type: 'POST',
            url: url,
            data: {kode_jabatan:kode_jabatan},
            success: function(msg){
              if(msg == 1){
                $(".sukses").html('<div class="alert alert-warning">Kode_Telah_dipakai</div>');
                setTimeout(function(){
                  $('.sukses').html('');
                },1700);              
                $('#kode_jabatan').val('');
              }
              else{

              }
            }
          });
        });

        $(".select2").select2();
        $("#tabel_daftar").dataTable();

        $("#jenis_bahan").change(function(){
          $.ajax({
            type: 'POST',
            url: "<?php echo base_url() . 'master/plasma/get_bahan' ?>",
            data: {jenis_bahan:$(this).val()},
            success: function(data){
              $("#bahan").html(data);
            },
            error : function(data) {  
              alert('Sorry');
            }  
          });
        });

        $("#bahan").change(function(){
          jenis_bahan = $("#jenis_bahan").val();
          $.ajax({
            type: 'POST',
            url: "<?php echo base_url() . 'master/plasma/get_satuan' ?>",
            dataType: 'json',
            data: {kode_bahan_baku:$(this).val(), jenis_bahan:jenis_bahan},
            success: function(data){
              $("#id_satuan").val(data.id_satuan_stok);
              $("#nama_satuan").val(data.satuan_stok);
              $("#hpp_bb").val(data.hpp);

            },
            error : function(data) {  
              alert('Sorry');
            }  
          });
        });

      });

      $(function () {
      //jika tombol Send diklik maka kirimkan data_form ke url berikut
      $("#data_jabatan").submit( function() { 

        $.ajax( {  
          type :"post", 
          <?php 
          if (empty($uri)) {
            ?>
            //jika tidak terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/plasma/simpan_tambah_plasma'; ?>",
            <?php }
            else { ?>
            //jika terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/plasma/simpan_edit_plasma'; ?>",
            <?php }
            ?>  
            cache :false,  
            data :$(this).serialize(),
            beforeSend:function(){
              $(".tunggu").show();  
            },
            success : function(data) {  
              $(".sukses").html(data);   
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'master/plasma/' ?>";},1500);              
            },  
            error : function() {  
              alert("Data gagal dimasukkan.");  
            }  
          });
        return false;                          
      });   

      $("#add-item").click(function(){
        var kode_plasma = $("#kode_plasma").val();
        var jenis_bahan = $("#jenis_bahan").val();
        var bahan_baku = $("#bahan").val();

        var id_satuan = $("#id_satuan").val();
        var satuan = $("#nama_satuan").val();
        var hpp = $("#hpp_bb").val();
        var rumus = $("#rumus").val();
        var harga = $("#harga").val();
        if (harga == '' || bahan_baku == '') {

          alert("Silahkan Lengkapi Form.");

        }else{

          
        $.ajax({
          type: 'POST',
          <?php 
          if(!empty($uri)){ ?>
            url: "<?php echo base_url() . 'master/plasma/tambah_opsi' ?>",
            <?php 
          } else { ?>
            url: "<?php echo base_url() . 'master/plasma/tambah_temp' ?>",
            <?php
          } ?>
          data: {kode_plasma:kode_plasma, jenis_bahan:jenis_bahan, bahan_baku:bahan_baku, id_satuan:id_satuan, satuan:satuan,hpp:hpp,harga:harga},
          success: function(data){
            $("#bahan").val('');
            $("#bahan").select2("val", "");
            $("#id_satuan").val('');
            $("#nama_satuan").val('');
            $("#hpp_bb").val('');
            $("#harga").val('');
            <?php
            if(!empty($uri)){ ?>
              $("#opsi_plasma").load('<?php echo base_url().'master/plasma/table_opsi/'; ?>'+kode_plasma);
              <?php 
            }else{ ?>
              $("#opsi_plasma").load('<?php echo base_url().'master/plasma/table_temp/'; ?>'+kode_plasma);
              <?php } ?>
            },
            error : function(data) {  
              alert('Sorry');
            }  
          });
        }
      });


    });

      function get_temp(){
        kode_plasma = $("#kode_plasma").val();
        $("#opsi_plasma").load('<?php echo base_url().'master/plasma/table_temp/'; ?>'+kode_plasma);
      }
      function get_opsi(){
        kode_plasma = $("#kode_plasma").val();
        $("#opsi_plasma").load('<?php echo base_url().'master/plasma/table_opsi/'; ?>'+kode_plasma);
      }
    </script>