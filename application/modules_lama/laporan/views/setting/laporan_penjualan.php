
<script src="https://public.azurewebsites.net/js/jquery.table2excel.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
           Laporan Penjualan
         </div>
         <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none" >
            <img src="<?php echo base_url() . '/public/img/loading.gif' ?>" >
          </div>
          <form id="pencarian_form" method="post" style="margin-left: 18px;" class="form-horizontal" target="_blank">
            <div class="row">
             <div class="col-md-12" id="">
              <div class="row">
                <div class="col-md-5" id="">
                  <div class="input-group">
                    <span class="input-group-addon">Bulan</span>
                    <select class="form-control" name="bulan" id="bulan" required="">              
                      <option value="">
                        -- Bulan --
                      </option>
                      <?php
                      for ($i=1; $i <= 12 ; $i++) { 
                        ?>
                        <option value="<?php if(strlen($i)==1) {
                          echo "0".$i;   
                        }else{
                          echo $i;
                        }  ?>">
                        <?php 
                        if($i==1){
                          echo "Januari";
                        }elseif ($i==2) {
                          echo "Februari";
                        }elseif ($i==3) {
                          echo "Maret";
                        }elseif ($i==4) {
                          echo "April";
                        }elseif ($i==5) {
                          echo "Mei";
                        }elseif ($i==6) {
                          echo "Juni";
                        }elseif ($i==7) {
                          echo "Juli";
                        }elseif ($i==8) {
                          echo "Agustus";
                        }elseif ($i==9) {
                          echo "September";
                        }elseif ($i==10) {
                          echo "Oktober";
                        }elseif ($i==11) {
                          echo "November";
                        }elseif ($i==12) {
                          echo "Desember";
                        }

                        ?>
                      </option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-md-5" id="">
                <div class="input-group">
                  <span class="input-group-addon">Tahun</span>
                  <select class="form-control" name="tahun" id="tahun" required="">              
                    <option value="">
                      -- Tahun --
                    </option>
                    <?php
                    $tanggal_sekarang = date('Y');
                    $date = $tanggal_sekarang-10;
                    for ($i=$date; $i <= $tanggal_sekarang ; $i++) { 
                      ?>
                      <option value="<?php echo $i ?>">
                        <?php echo $i ?>
                      </option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>                        
              <div class="col-md-2">
                <button style="margin-left: 5px" type="button" onclick="print_penjualan()" class="btn btn-info pull-right" id="cetak"><i class="fa fa-print"></i> Print</button>
                <button style="width: 90px" type="button" class="btn btn-warning pull-right"  onclick="cari_transaksi()"><i class="fa fa-search"></i> Cari</button>
              </div>

            </div>
          </div>
        </div>
        <br>
      </form>
      <div class="row">
        <div class="col-md-12" id="">


        </div>
      </div>
      <div id="cari_penjualan">
        <div>
          <?php

          $this->db->select('*'); 
          $this->db->distinct();

          $tanggal_saiki=date("m"); 
          $where = "MONTH(tanggal_penjualan) = $tanggal_saiki"; 
          $this->db->where($where);
          $this->db->order_by('tanggal_penjualan','desc');
          $this->db->group_by('tanggal_penjualan');
          $penjualan = $this->db->get('transaksi_penjualan');
          $hasil_penjualan = $penjualan->result();

          $keuangan = 0;
          foreach($hasil_penjualan as $total){
            $keuangan += $total->grand_total;
          }

          $seluruh = '';
          foreach($hasil_penjualan as $daftar){
            $this->db->select_sum('grand_total');
            $total_jual = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
            $hasil_total = $total_jual->row();

            $seluruh += $hasil_total->grand_total;
          }

          ?>
          <label><h4><strong>Total Transaksi Penjualan : <?php echo count($hasil_penjualan); ?></strong></h4></label><br />
          <span><label><h4><strong>Total Keuangan Penjualan :<?php echo format_rupiah($seluruh); ?></strong></h4></label></span>

        </div> <br><br>
        <table id="tabel_daftar" class="table table-bordered table-striped" style="font-size:1.5em;">

          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Nominal</th>
              <th class="act">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $nomor = 1;

            foreach($hasil_penjualan as $daftar){ ?> 
            <tr>
              <td><?php echo $nomor; ?></td>
              <td><?php echo TanggalIndo(@$daftar->tanggal_penjualan);?></td>
              <td align="right">
                <?php
                $this->db->select_sum('grand_total');
                $total_jual = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
                $hasil_total = $total_jual->row();
                echo format_rupiah(@$hasil_total->grand_total); 
                ?>
              </td>
              <td><a href="<?php echo base_url().'laporan/detail_penjualan/'.$daftar->tanggal_penjualan; ?>" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle green"><i class="fa fa-search"></i> </a>
              </td>
            </tr>
            <?php $nomor++; } ?>

          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Nominal</th>
              <th class="act">Action</th>
            </tr>
          </tfoot>
        </table>
      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>

  <!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'laporan'; ?>";
  });
</script>

<script>
 jQuery(document).ready(function() {
  $("#export").click(function(){
    $("#tabel_daftar").table2excel();
  });
});

 function cari_transaksi(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  $.ajax( {  
    type :"post",  
    url : "<?php echo base_url().'laporan/search_laporan'; ?>",  
    cache :false,
    data : {bulan:bulan,tahun:tahun},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success : function(data) {
      $(".tunggu").hide(); 
      $("#cari_penjualan").html(data);
    },  
    error : function(data) {  
      alert("das");  
    }  
  });
}

function print_penjualan(){

 var bulan =$("#bulan").val();
 var tahun =$("#tahun").val();

 window.open("<?php echo base_url().'laporan/cetak_penjualan/'; ?>"+bulan+"/"+tahun);

}
</script>
