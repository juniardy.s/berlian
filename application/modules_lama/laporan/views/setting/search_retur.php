
<br>
<br>
<table id="tabel_daftar" class="table table-bordered" style="font-size: 1.5em;">
  <?php
  $param = $this->input->post();
  if(@$param['bulan'] && @$param['tahun']){
    $bulan=$param['tahun'].'-'.$param['bulan'];
    $this->db->like('tanggal_retur',$bulan);
  }
  $this->db->select('*'); 
  $this->db->distinct();
  $this->db->order_by('tanggal_retur','desc');
  $this->db->group_by('tanggal_retur');
  $get_retur_penjualan = $this->db->get('transaksi_retur_penjualan');
  $hasil_get_retur_penjualan = $get_retur_penjualan->result();

  ?>
  <thead>
   <tr>
    <th>No</th>
    <th>Tanggal</th>
    <th>Total Retur Penjualan</th>
    <th>Action</th>
  </tr>
</thead>
<tbody>
  <?php
  $nomor = 1;
  $total= 0;
  foreach($hasil_get_retur_penjualan as $daftar){ 
    ?> 
    <tr>
      <td><?php echo $nomor; ?></td>
      <td><?php echo TanggalIndo(@$daftar->tanggal_retur);?></td>
      <td align="right">
        <?php
        $this->db->select_sum('grand_total');
        $total_jual = $this->db->get_where('transaksi_retur_penjualan',array('tanggal_retur'=>$daftar->tanggal_retur));
        $hasil_total = $total_jual->row();
        $total +=@$hasil_total->grand_total;
        echo format_rupiah(@$hasil_total->grand_total); 
        ?>
      </td>
      <td align="center"><a href="<?php echo base_url().'laporan/detail_daftar_laporan_retur/'.$daftar->tanggal_retur ?>" class="btn btn-primary"><i class="fa fa-search"></i> Detail</a></td>
    </tr>
    <?php $nomor++; } ?>
  </tbody>
  <tfoot>
   <tr>
    <th colspan="2">Total</th>
    <th colspan="1" align="right" style="text-align: right;"><?php echo format_rupiah($total) ?></th>
    <th></th>

  </tr>
</tfoot>

</table>
