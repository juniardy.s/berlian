
<script src="https://public.azurewebsites.net/js/jquery.table2excel.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
  .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
           Daftar Laporan Retur Penjualan
         </div>
         <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body" style="height: 100%;">            
          <div class="sukses" ></div>
          <div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none" >
            <img src="<?php echo base_url() . '/public/img/loading.gif' ?>" >
          </div>
          <form id="pencarian_form" method="post" style="margin-left: 0px;" class="form-horizontal" target="_blank">

            <div class="row">
              <div class="col-md-12" id="">
<!--                 <div class="row">
                  <div class="col-md-5" id="">
                    <div class="input-group">
                      <span class="input-group-addon">Bulan</span>
                      <select class="form-control" name="bulan" id="bulan" required="">              
                        <option value="">
                          -- Bulan --
                        </option>
                        <?php
                        for ($i=1; $i <= 12 ; $i++) { 
                          ?>
                          <option value="<?php if(strlen($i)==1) {
                            echo "0".$i;   
                          }else{
                            echo $i;
                          }  ?>">
                          <?php 
                          if($i==1){
                            echo "Januari";
                          }elseif ($i==2) {
                            echo "Februari";
                          }elseif ($i==3) {
                            echo "Maret";
                          }elseif ($i==4) {
                            echo "April";
                          }elseif ($i==5) {
                            echo "Mei";
                          }elseif ($i==6) {
                            echo "Juni";
                          }elseif ($i==7) {
                            echo "Juli";
                          }elseif ($i==8) {
                            echo "Agustus";
                          }elseif ($i==9) {
                            echo "September";
                          }elseif ($i==10) {
                            echo "Oktober";
                          }elseif ($i==11) {
                            echo "November";
                          }elseif ($i==12) {
                            echo "Desember";
                          }

                          ?>
                        </option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-5" id="">
                  <div class="input-group">
                    <span class="input-group-addon">Tahun</span>
                    <select class="form-control" name="tahun" id="tahun" required="">              
                      <option value="">
                        -- Tahun --
                      </option>
                      <?php
                      $tanggal_sekarang = date('Y');
                      $date = $tanggal_sekarang-10;
                      for ($i=$date; $i <= $tanggal_sekarang ; $i++) { 
                        ?>
                        <option value="<?php echo $i ?>">
                          <?php echo $i ?>
                        </option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>                        
                <div class="col-md-2 pull-left">
                  <button style="width: 90px" type="button" class="btn btn-warning pull-right"  onclick="cari_transaksi()"><i class="fa fa-search"></i> Cari</button>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="col-md-1 pull-left">
                 <button style="" type="button" class="btn btn-info pull-right" id="laporan_retur"><i class="fa fa-print"></i> Print</button>
               </div>
               <div class="col-md-1 pull-left">
                 <button style="" type="button" class="btn btn-success pull-right" id="export"><i class="fa fa-table"></i> Export</button>
               </div>
             </div> -->
           </div>
         </div>
         <br>
       </form>
       <div class="row">
        <div class="col-md-12" id="">


        </div>
      </div>
      <div id="cari_transaksi">
        <div>
          <?php
          $param = $this->uri->segment(3);
          $get_retur_penjualan = $this->db->get_where('transaksi_retur_penjualan',array('tanggal_retur' => $param ));
          $hasil_get_retur_penjualan = $get_retur_penjualan->result();
          ?>

        </div> <br><br>
        <table id="tabel_daftar" class="table table-bordered" style="font-size: 1.5em;">

          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Nama</th>
              <th>Tanggal</th>
              <th>Total Retur Penjualan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $nomor = 1;
            $total= 0;
            foreach($hasil_get_retur_penjualan as $daftar){ 
              $kode_retur = $daftar->kode_retur;
              $get_opsi_retur_penjualan = $this->db->get_where('opsi_transaksi_retur_penjualan',array('kode_retur'=>$kode_retur));
              $hasil_get_opsi_retur_penjualan = $get_opsi_retur_penjualan->row();
              $total += $daftar->grand_total;

              $get_transaksi_penjualan = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>@$daftar->kode_penjualan));
              $hasil_get_transaksi_penjualan = $get_transaksi_penjualan->row();

              ?> 
              <tr>
                <td><?php echo $nomor; ?></td>
                <td><?php echo @$daftar->kode_retur; ?></td>
                <td><?php echo @$hasil_get_transaksi_penjualan->nama_member; ?></td>
                <td><?php echo TanggalIndo(@$daftar->tanggal_retur);?></td>
                <td align="right"><?php echo format_rupiah($daftar->grand_total);?></td>
                <td align="center"><a href="<?php echo base_url().'laporan/detail_retur/'.$daftar->kode_retur ?>" class="btn btn-primary"><i class="fa fa-search"></i> Detail</a></td>
              </tr>
              <?php $nomor++; } ?>
            </tbody>
            <tfoot>
             <tr>
              <th colspan="4">Total</th>
              <th colspan="1" align="right" style="text-align: right;"><?php echo format_rupiah($total) ?></th>
              <th></th>

            </tr>
          </tfoot>
        </table>



      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>

  <!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'laporan/laporan_retur/'; ?>";
  });
</script>
<script>
 $("#laporan_retur").click(function(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  if (bulan=='') {
    bulan="-";
  }
  if (tahun=='') {
    tahun="-";
  }
  window.open("<?php echo base_url().'laporan/print_laporan_retur/'; ?>"+bulan+"/"+tahun);

})
 

 function cari_transaksi(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  $.ajax( {  
    type :"post",  
    url : "<?php echo base_url().'laporan/search_retur'; ?>",  
    cache :false,
    data : {bulan:bulan,tahun:tahun},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success : function(data) {
      $(".tunggu").hide(); 
      $("#cari_transaksi").html(data);
    },  
    error : function(data) {  
      alert("das");  
    }  
  });
}

jQuery(document).ready(function() {
  $("#export").click(function(){
    $("#tabel_daftar").table2excel();
  });
});
$(document).ready(function(){

  $("#form_setting").submit(function(){
    var keterangan = "<?php echo base_url().'pembelian/keterangan'?>";
    $.ajax({
      type: "POST",
      url: keterangan,
      data: $('#form_setting').serialize(),
      success: function(msg)
      {
        $('#modal_setting').modal('hide');  
      }
    });
    return false;
  });
})
</script>

