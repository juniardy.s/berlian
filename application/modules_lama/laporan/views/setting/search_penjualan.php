<div>
  <?php

  $param = $this->input->post();
  if(@$param['bulan'] && @$param['tahun']){
    $bulan=$param['tahun'].'-'.$param['bulan'];
    $this->db->like('tanggal_penjualan',$bulan);
  }
  $this->db->select('*'); 
  $this->db->distinct();
  $this->db->order_by('tanggal_penjualan','desc');
  $this->db->group_by('tanggal_penjualan');
  $penjualan = $this->db->get('transaksi_penjualan');
  $hasil_penjualan = $penjualan->result();

  $keuangan = 0;
  foreach($hasil_penjualan as $total){
    $keuangan += $total->grand_total;
  }

  $seluruh = '';
  foreach($hasil_penjualan as $daftar){
    $this->db->select_sum('grand_total');
    $total_jual = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
    $hasil_total = $total_jual->row();

    $seluruh += $hasil_total->grand_total;
  }

  ?>
  <label><h4><strong>Total Transaksi Penjualan : <?php echo count($hasil_penjualan); ?></strong></h4></label><br />
  <span><label><h4><strong>Total Keuangan Penjualan :<?php echo format_rupiah($seluruh); ?></strong></h4></label></span>
</div>
</div> <br><br>

<table id="search_penjualan" class="table table-bordered table-striped" style="font-size:1.5em;">
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal</th>
      <th>Nominal</th>
      <th class="act">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $nomor = 1;

    foreach($hasil_penjualan as $daftar){ ?> 
    <tr>
      <td><?php echo $nomor; ?></td>
      <td><?php echo TanggalIndo(@$daftar->tanggal_penjualan);?></td>
      <td align="right">
        <?php
        $this->db->select_sum('grand_total');
        $total_jual = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
        $hasil_total = $total_jual->row();
        echo format_rupiah(@$hasil_total->grand_total); 
        ?>
      </td>
      <td><a href="<?php echo base_url().'laporan/detail_penjualan/'.$daftar->tanggal_penjualan; ?>" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle green"><i class="fa fa-search"></i> </a>
      </td>
    </tr>
    <?php $nomor++; } ?>

  </tbody>
  <tfoot>
    <tr>
      <th>No</th>
      <th>Tanggal</th>
      <th>Nominal</th>
      <th class="act">Action</th>
    </tr>
  </tfoot>
</table>
<script src="https://public.azurewebsites.net/js/jquery.table2excel.js"></script>
<script>
  jQuery(document).ready(function() {
    alert('asas');
    $("#export").click(function(){
      $("#search_penjualan").table2excel();
    });
  });
 // $("#cetak").click(function(){
 //    var tgl_awal =$("#tgl_awal").val();
 //    var tgl_akhir =$("#tgl_akhir").val();
 //     window.open("<?php echo base_url().'laporan/cetak_laporan_penjualan/'; ?>"+tgl_awal+"/"+tgl_akhir);

 //  })



</script>