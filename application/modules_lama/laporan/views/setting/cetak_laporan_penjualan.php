<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
	<?php
	$tgl_awal=$this->uri->segment(3);
	$tgl_akhir=$this->uri->segment(4);
	
	if (!empty($tgl_awal) and !empty($tgl_akhir)){
		$this->db->where('tanggal_penjualan >=',$tgl_awal);
		$this->db->where('tanggal_penjualan <=',$tgl_akhir);
	}else{
		$this->db->select('*'); 
		$this->db->distinct();


		$this->db->where('tanggal_penjualan',date('Y-m-d'));
		$this->db->order_by('kode_penjualan','desc');
		$this->db->group_by('kode_penjualan');
	}
	
	$penjualan = $this->db->get('transaksi_penjualan');
	$hasil_penjualan = $penjualan->result();
	$keuangan = 0;
	foreach($hasil_penjualan as $total){
		$keuangan += $total->grand_total-$total->nominal_retur;
	}

	?>
	<img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
<hr>
<p><h3 align="center">Laporan Penjualan</h3></p>
	<table width="100%" id="tabel_daftar" class="table" border="1" style="border-collapse: collapse;">

		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>Kode Penjualan</th>
				<!-- <th>Kode Transaksi</th> -->
				<th>Nama Member</th>
				<th>Petugas</th>
				<th>Total</th>
				<th>Pembayaran</th>
				<th>Note</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$nomor = 1;
			$total=0;
			foreach($hasil_penjualan as $daftar){ 
				$total+=@$daftar->grand_total-@$daftar->nominal_retur;
				?> 

			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo TanggalIndo(@$daftar->tanggal_penjualan);?></td>
				<td><?php echo @$daftar->kode_penjualan; ?></td>
				<!-- <td><?php echo @$daftar->kode_transaksi; ?></td> -->
				<td><?php echo @$daftar->nama_member; ?></td>
				<td><?php echo @$daftar->petugas; ?></td>

				<td><?php echo format_rupiah(@$daftar->grand_total-@$daftar->nominal_retur); ?></td>
				<td><?php echo @$daftar->jenis_transaksi; ?></td>
				<td><?php echo @$daftar->keterangan; ?></td>
			</tr>
			<?php $nomor++; } ?>
			<tr>
				<td colspan="5">Total</td>
				<td><?php echo format_rupiah($total) ?></td>
				<td></td>
				<td></td>
			</tr>

		</tbody>
		
	</table>
</body>
</html>