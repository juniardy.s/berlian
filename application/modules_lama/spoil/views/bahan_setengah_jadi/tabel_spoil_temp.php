 <?php
 $kode_default = $this->db->get('setting_gudang');
 $hasil_unit =$kode_default->row();
 $param = $hasil_unit->kode_unit;
 $kode_spoil = $this->uri->segment(4);
 $spoil =$this->db->get_where('opsi_transaksi_spoil_temp',array('kode_unit' => $param, 'kode_spoil' => $kode_spoil,'jenis_bahan' => 'bahan_setengah_jadi'));
 $list_spoil = $spoil->result();
 $nomor = 1;  

 foreach($list_spoil as $daftar){ 
  @$satuan_bahan = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>@$daftar->kode_bahan));
  @$hasil_satuan_bahan = $satuan_bahan->row();
  ?> 
  <tr>
    <td><?php echo $nomor; ?></td>
    <td><?php echo $daftar->kode_bahan; ?></td>
    <td><?php echo $daftar->nama_bahan; ?></td>
    <td>
      <?php echo @$hasil_satuan_bahan->real_stock." ".@$hasil_satuan_bahan->satuan_stok;?>
      <input name="<?php echo "stok".$daftar->kode_bahan; ?>" type="hidden" class="form-control <?php echo "stok".$daftar->kode_bahan; ?>" id="" value="<?php echo @$hasil_satuan_bahan->real_stock ?>" />
    </td>
    <td><input name="<?php echo $daftar->kode_bahan; ?>" type="number" class="form-control <?php echo "input".$daftar->kode_bahan; ?>" id="" value="" /></td>
    <td>
      <div class="<?php echo "visual".$daftar->kode_bahan; ?>"><?php echo @$hasil_satuan_bahan->real_stock." ".@$hasil_satuan_bahan->satuan_stok;?></div>
      <input name="<?php echo "sisa".$daftar->kode_bahan; ?>" type="hidden" class="form-control <?php echo "sisa".$daftar->kode_bahan; ?>" id="" value="<?php echo @$hasil_satuan_bahan->real_stock ?>" />
      <input name="<?php echo "satuan".$daftar->kode_bahan; ?>" type="hidden" class="form-control <?php echo "satuan".$daftar->kode_bahan; ?>" id="" value="<?php echo @$hasil_satuan_bahan->satuan_stok ?>" />
    </td>
    <td><input name="<?php echo "keterangan".$daftar->kode_bahan; ?>" type="text" class="form-control <?php echo "keterangan".$daftar->kode_bahan; ?>" id="" value="" /></td>
    <td><?php echo get_del_id_temp($daftar->id); ?>
    </td>
  </tr>

  <?php 
  $nomor++; 
} 
?>
<script type="text/javascript">
  <?php
  foreach($list_spoil as $daftar){ ?>
    $(".<?php echo "input".$daftar->kode_bahan; ?>").keyup(function(){
      if($(".<?php echo "input".$daftar->kode_bahan; ?>").val() < 0){
        alert("Jumlah Spoil Kurang Dari 0");
        $(".<?php echo "input".$daftar->kode_bahan; ?>").val('');
      }else{

        stok_awal = $(".<?php echo "stok".$daftar->kode_bahan; ?>").val();
        jumlah_spoil =  $(".<?php echo "input".$daftar->kode_bahan; ?>").val();
        satuan =  $(".<?php echo "satuan".$daftar->kode_bahan; ?>").val();

        hasil_akhir = stok_awal - jumlah_spoil;
        $(".<?php echo "sisa".$daftar->kode_bahan; ?>").val(hasil_akhir);
        $(".<?php echo "visual".$daftar->kode_bahan; ?>").text(hasil_akhir+" "+satuan);
      }
    }); 
    <?php 
  } ?>

  
</script>