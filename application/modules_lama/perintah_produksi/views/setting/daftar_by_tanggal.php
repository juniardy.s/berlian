 <br><br><br><br>
 <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
  <?php
  $data = $this->input->post();
  $this->db->limit(100);
  $this->db->where('status','menunggu');
  $this->db->where('tanggal_input >=',$data['tanggal_awal']);
  $this->db->where('tanggal_input <=',$data['tanggal_akhir']);
  $this->db->order_by('tanggal_input','desc');
  $perintah = $this->db->get('transaksi_perintah_produksi');
  $hasil_perintah = $perintah->result();
  ?>
  <thead>
    <tr>
      <th>No</th>
      <th>Kode Transaksi</th>
      <th>Tanggal Transaksi</th>

      <th>Petugas</th>
      <th>Jenis Perintah Produksi</th>
      <th>Action</th>


    </tr>
  </thead>
  <tbody id="posts">
    <?php
    $nomor = 1;

    foreach($hasil_perintah as $daftar){ ?> 
    <tr>
      <td><?php echo $nomor; ?></td>
      <td><?php echo $daftar->kode_transaksi; ?></td>
      <td><?php echo TanggalIndo($daftar->tanggal_input); ?></td>
      <td><?php echo $daftar->petugas; ?></td>
      <td><?php echo cek_jenis_produksi($daftar->jenis_produksi); ?></td>
      <td><?php echo get_detail_print_string($daftar->kode_transaksi); ?>
        <a onclick="actPrintString('<?php echo $daftar->kode_transaksi; ?>')" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle blue"><i class="fa fa-print"></i> </a>
      </td>

    </tr>
    <?php $nomor++; } ?>
  </tbody>
  <tfoot>
    <tr>
      <th>No</th>
      <th>Kode Transaksi</th>
      <th>Tanggal Transaksi</th>
      <th>Petugas</th>
      <th>Jenis Perintah Produksi</th>
      <th>Action</th>

    </tr>
  </tfoot>
</table>
<?php 
$this->db->where('status','menunggu');
$get_jumlah = $this->db->get('transaksi_perintah_produksi');
$jumlah = $get_jumlah->num_rows();
$jumlah = floor($jumlah/100);
?>
<input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
<input type="hidden" class="form-control pagenum" value="0">
<div class="box-footer clearfix"></div>