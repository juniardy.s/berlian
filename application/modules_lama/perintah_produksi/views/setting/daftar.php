<div class="">
  <div class="page-content">
    <div class="row">
      <div class="col-md-4">
        <a href="<?php echo base_url().'perintah_produksi/sendiri'; ?>" class="btn btn-lg btn-primary"><i class="fa fa-plus-square"></i> Tambah</a>
        <a href="<?php echo base_url().'perintah_produksi/'; ?>" class="btn btn-lg green-seagreen"><i class="fa fa-list"></i> Daftar</a>
      </div>

    </div><br />
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">Daftar Perintah Produksi</div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-4">
           <div class="input-group">
            <span class="input-group-addon">Tanggal Awal</span>
            <input type="date" class="form-control" id="tanggal_awal">
          </div>
        </div>
        <div class="col-md-4">
         <div class="input-group">
          <span class="input-group-addon">Tanggal Akhir</span>
          <input type="date" class="form-control" id="tanggal_akhir">
        </div>
      </div>
      <div class="col-md-2">
        <a style="width: 100px" class="btn btn-warning " onclick="cari_data_by_date()" ><i class="fa fa-search"></i> Cari</a>
      </div>
    </div>
    <div id="load_search"><br><br><br><br>
      <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
        <?php
        $this->db->limit(100);
        $this->db->where('status','menunggu');
        $this->db->where('MONTH(tanggal_input)',date('m'));
        $this->db->where('YEAR(tanggal_input)',date('Y'));
        $this->db->order_by('tanggal_input','desc');
        $perintah = $this->db->get('transaksi_perintah_produksi');
        $hasil_perintah = $perintah->result();  
        ?>
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Transaksi</th>
            <th>Tanggal Transaksi</th>

            <th>Petugas</th>
            <th>Jenis Perintah Produksi</th>
            <th>Action</th>


          </tr>
        </thead>
        <tbody id="posts">
          <?php
          $nomor = 1;

          foreach($hasil_perintah as $daftar){ ?> 
          <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo $daftar->kode_transaksi; ?></td>
            <td><?php echo TanggalIndo($daftar->tanggal_input); ?></td>
            <td><?php echo $daftar->petugas; ?></td>
            <td><?php echo cek_jenis_produksi($daftar->jenis_produksi); ?></td>
            <td><?php echo get_detail_print_string($daftar->kode_transaksi); ?>
              <a onclick="actPrintString('<?php echo $daftar->kode_transaksi; ?>')" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle blue"><i class="fa fa-print"></i> </a>
            </td>

          </tr>
          <?php $nomor++; } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Kode Transaksi</th>
            <th>Tanggal Transaksi</th>
            <th>Petugas</th>
            <th>Jenis Perintah Produksi</th>
            <th>Action</th>

          </tr>
        </tfoot>
      </table>
      <?php 
      $this->db->where('status','menunggu');
      $get_jumlah = $this->db->get('transaksi_perintah_produksi');
      $jumlah = $get_jumlah->num_rows();
      $jumlah = floor($jumlah/100);
      ?>
      <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
      <input type="hidden" class="form-control pagenum" value="0">


      <div class="box-footer clearfix"></div>
    </div>
  </div>
</div>
</div>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;" />
<script>
$('.btn-back').click(function(){
  window.location = "<?php echo base_url().'produksi/'; ?>";
});

$(window).scroll(function(){
  if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
    if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
      var pagenum = parseInt($(".pagenum").val()) + 1;
      $(".pagenum").val(pagenum);
      load_table(pagenum);
    }
  }
});

function load_table(page){
  var kategori =$("#kategori_produk").val();
  var nama_produk =$("#nama_produk").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url() . 'perintah_produksi/get_table' ?>",
    data: ({kategori:kategori,nama_produk:nama_produk, page:$(".pagenum").val()}),
    beforeSend: function(){
      $(".tunggu").show();  
    },
    success: function(msg)
    {
      $(".tunggu").hide();
      $("#posts").append(msg);

    }
  });
}



function actPrintString(Object){
  window.open("<?php echo base_url().'perintah_produksi/cetak/'; ?>"+Object);
}

$('#cari').click(function(){


  var nama_produk =$("#nama_produk").val();
  $.ajax( {  
    type :"post",  
    url : "<?php echo base_url().'perintah_produksi/cari_transaksi'; ?>",  
    cache :false,

    data : {kategori:kategori,nama_produk:nama_produk},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success : function(data) {
     $(".tunggu").hide();  
     $("#cari_produk").html(data);
   },  
   error : function(data) {  
     alert("das");  
   }  
 });
});
</script>

<script>
function cari_data_by_date(){
  var tanggal_awal  = $("#tanggal_awal").val();
  var tanggal_akhir = $("#tanggal_akhir").val();

  if (tanggal_akhir != '' && tanggal_awal != '') {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'perintah_produksi/get_search_daftar' ?>",
      data: ({tanggal_awal:tanggal_awal,tanggal_akhir:tanggal_akhir}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#load_search").html(msg);

      }
    });
    return false;
  }else{
    alert('Mohon Melengkapi Data.');
    $(".tunggu").hide();
  }
}
</script>