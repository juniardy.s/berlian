<?php
$get_kode = $this->uri->segment(4);
$table = $this->uri->segment(3);

$this->db->where('kode_bahan_setengah_jadi', $get_kode);
if($table == 'table_temp'){
	$get_temp = $this->db->get('opsi_master_bahan_setengah_jadi_temp');
} else{
	$get_temp = $this->db->get('opsi_master_bahan_setengah_jadi');
}
$hasil_temp = $get_temp->result();

$no = 1;
foreach ($hasil_temp as $temp) {
	?>
	<tr>
		<td><?php echo $no ?></td>
		<td><?php echo $temp->nama_bahan ?></td>
		<td><?php echo $temp->jumlah.' '.$temp->nama_satuan ?></td>
	</tr>
	<?php 
	$no++;
}
?>
<script type="text/javascript">
	function get_temp(){
		kode_bahan_setengah = $("#kode_setting").val()+'_'+$("#kode_bahan_setengah").val();
		$("#opsi_bahan_setengah").load('<?php echo base_url().'master/bahan_setengah/table_temp/'; ?>'+kode_bahan_setengah);
	}
	function get_opsi(){
		kode_bahan_setengah = $("#kode_bahan_setengah").val();
		$("#opsi_bahan_setengah").load('<?php echo base_url().'master/bahan_setengah/table_opsi/'; ?>'+kode_bahan_setengah);
	}
	function actDelete(key){
		$.ajax({
			type: 'POST',
			<?php 
			if($table == 'table_opsi'){ ?>
				url: "<?php echo base_url() . 'master/bahan_setengah/delete_opsi' ?>",
				<?php 
			} else { ?>
				url: "<?php echo base_url() . 'master/bahan_setengah/delete_temp' ?>",
				<?php
			} ?>
			data: {id:key},
			success: function(data){
				<?php 
				if($table == 'table_opsi'){ ?>
					get_opsi();
					<?php 
				} else { ?>
					get_temp();
					<?php
				} ?>
			},
			error : function(data) {  
				alert('Sorry');
			}  
		});
	}
</script>