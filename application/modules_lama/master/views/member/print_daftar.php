<!DOCTYPE html>
<html>
<head>
	<title>Print Member</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <table  >
    <tr>
      <td>
        <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
      </td>
      
    </tr>
    
  </table>
  <hr>
  <table class="" width="100%" border="0" style="border-collapse: collapse;">

    <tr>

      <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Data Member</th>
    </tr>
  </table><br>
  <table class="" width="100%" border="1" style="border-collapse: collapse;">
    <thead>
      <tr>
        <th width="50px;">No</th>
        <th>Kode Member</th>
        <th>Nama Member</th>
        <th>Alamat</th>
        <th>Telepon</th>
        <th>Jalur</th>
        <th>Keterangan</th>
        <th>Status Member</th>

      </tr>
    </thead>
    <tbody>
      <?php
      $kode_jalur=$this->uri->segment(4);
      if(!empty($kode_jalur)){
        $this->db->where('kode_jalur',$kode_jalur);
      }
      $this->db->order_by('nama_member','asc');
      $get_member = $this->db->get('master_member');
      $hasil_member =$get_member->result();
      $no = 1;
      foreach($hasil_member as $item){
        if($this->session->flashdata('message')==$item->kode_member){

          echo '<tr id="warna" style="background: #88cc99; display: none;">';
        }
        else{
          echo '<tr>';
        }
        ?>
        <td><?php echo $no;?></td>
        <td><?php echo $item->kode_member; ?></td>                  
        <td><?php echo $item->nama_member; ?></td>                  
        <td><?php echo $item->alamat_member; ?></td>
        <td><?php echo $item->telp_member; ?></td>
        <td><?php echo $item->nama_jalur; ?></td>
        <td><?php echo $item->keterangan; ?></td>
        <td><?php echo cek_status($item->status_member); ?></td>
        
      </tr>
      <?php
      $no++;
    } ?>
  </tbody>   
</table>



</html>