<!DOCTYPE html>
<html>
<head>
	<title>Print Karyawan</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <table  >
    <tr>
      <td>
        <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
      </td>
      
    </tr>
    
  </table>
  <hr>
  <table class="" width="100%" border="0" style="border-collapse: collapse;">

    <tr>
      
      <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Data Karyawan</th>
    </tr>
  </table><br>
  <table class="" width="100%" border="1" style="border-collapse: collapse;">
    <?php
    $this->db->order_by('nama_karyawan','asc');
    $karyawan = $this->db->get('master_karyawan');
    $hasil_karyawan = $karyawan->result();
    ?>
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Karyawan</th>
        <th>Jenis Karyawan</th>
        <th>Nama Karyawan</th>
        <th>Telepon</th>
        <th>Gaji Tetap</th>
        
        <th>Keterangan</th>
        <th>Status</th>
        
      </tr>
    </thead>
    <tbody>
      <?php
      $nomor = 1;

      foreach($hasil_karyawan as $daftar){ ?> 
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo $daftar->kode_karyawan; ?></td>
        <td><?php echo cek_jenis($daftar->jenis_karyawan); ?></td>
        <td><?php echo $daftar->nama_karyawan; ?></td>
        <td><?php echo $daftar->telp; ?></td>
        <td><?php echo format_rupiah($daftar->gaji_tetap); ?></td>
        
        <td><?php echo $daftar->keterangan; ?></td>
        <td><?php echo cek_status($daftar->status); ?></td>
        
      </tr>
      <?php $nomor++; } ?>
    </tbody>

    
  </table>



  </html>