<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Detail Karyawan
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>


          <div class="box-body">            
            <div class="sukses" ></div>
            <form id="data_jabatan"  method="post">         
              <div class="row">  

                <?php
                $uri = $this->uri->segment(4);
                if(!empty($uri)){
                  $data = $this->db->get_where('master_karyawan',array('kode_karyawan'=>$uri));
                  $hasil_data = $data->row();


                  ?>
                  <input type="hidden" name="kode_karyawan" value="<?php echo @$hasil_data->kode_karyawan ?>" />
                  <?php
                }

                $this->db->select_max('id');
                $get_max_member = $this->db->get('master_karyawan');
                $max_member = $get_max_member->row();

                $this->db->where('id', $max_member->id);
                $get_member = $this->db->get('master_karyawan');
                $member = $get_member->row();
                $nomor = substr(@$member->kode_karyawan, 4);
                $nomor = $nomor + 1;
                $string = strlen($nomor);
                if($string == 1){
                  $kode_karyawan ='000'.$nomor;
                } else if($string == 2){
                  $kode_karyawan ='00'.$nomor;
                } else if($string == 3){
                  $kode_karyawan ='0'.$nomor;
                } else if($string == 4){
                  $kode_karyawan =''.$nomor;
                } 

                ?>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Kode Karyawan</label>
                  <input type="hidden" name="id" value="<?php echo @$hasil_data->id ?>" />
                  <input readonly="" <?php if(!empty($uri)){ echo "readonly='true'"; } ?> type="text" class="form-control" value="<?php if(!empty($uri)){echo @$hasil_data->kode_karyawan;}else{echo "KAR_". $kode_karyawan;} ?>" name="kode_karyawan" id="kode_karyawan"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Jenis Karyawan</label>
                  <select readonly onchange="gaji()" class="form-control" id="jenis_karyawan" name="jenis_karyawan">
                    <option value="">Pilih</option>
                    <option value="borongan" <?php if(@$hasil_data->jenis_karyawan=='borongan'){echo "selected";}?>>Borongan</option>
                    <option value="tetap" <?php if(@$hasil_data->jenis_karyawan=='tetap'){echo "selected";}?>>Tetap</option>
                  </select>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Nama Karyawan</label>
                  <input readonly type="text" class="form-control" value="<?php echo @$hasil_data->nama_karyawan; ?>" name="nama_karyawan" id="nama_karyawan"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Alamat</label>
                  <input readonly type="text" class="form-control" value="<?php echo @$hasil_data->alamat; ?>" name="alamat" id="alamat"/>
                </div>

                <div class="form-group  col-xs-4">
                  <label class="gedhi">Telp</label>
                  <input readonly type="text" class="form-control" value="<?php echo @$hasil_data->telp; ?>" name="telp" id="telp"/>
                </div>




               <!--  <div class="form-group  col-xs-4">
                  <label class="gedhi">Keterangan</label>
                  <input readonly type="text" class="form-control" value="<?php echo @$hasil_data->keterangan; ?>" name="keterangan" id="keterangan"/>
                </div>    -->
                <div class="form-group  col-xs-4">
                  <label class="gedhi">Status Karyawan</label>
                  <select readonly class="form-control" id="status" name="status">
                    <option>Pilih</option>
                    <option value="1" <?php if(@$hasil_data->status=='1'){echo "selected";}?>>Aktif</option>
                    <option value="0" <?php if(@$hasil_data->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                  </select>
                </div>  

                <div class="form-group  col-xs-4">
                 <label class="gedhi tetap">Gaji Tetap</label>
                 <input readonly type="text" placeholder="Gaji Tetap" class="form-control tetap" value="<?php echo @$hasil_data->gaji_tetap; ?>" name="gaji_tetap" id="gaji_tetap"/>

                 <label class="gedhi harian">Gaji Harian</label>
                 <input readonly type="text" placeholder="Gaji Harian" class="form-control harian" value="<?php echo @$hasil_data->gaji_harian; ?>" name="gaji_harian" id="gaji_harian"/>
               </div>

              <div class="form-group  col-xs-6">
                  <label class="gedhi">Keterangan</label>
                  <!-- <input type="text" class="form-control" value="<?php #echo @$hasil_data->keterangan; ?>" name="keterangan" id="keterangan"/> -->
                  <textarea class="form-control" readonly=""  name="keterangan" id="keterangan"><?php echo @$hasil_data->keterangan; ?></textarea>
                </div>   
             
                <div class="col-md-12">
                  <a class="btn btn-primary pull-right" href="<?php echo base_url().'master/karyawan/' ?>"><i class="fa fa-back">OK</i></a>
                </div>
             </div>
             
           </form>


         </div>

         <!------------------------------------------------------------------------------------------------------>

       </div>
     </div>


     <div class="box box-info">


      <div class="box-body">            



      </section><!-- /.Left col -->      
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/karyawan/'; ?>";
  });
</script>


<script type="text/javascript">
  function gaji(){
    var jenis_karyawan=$('#jenis_karyawan').val();
    if(jenis_karyawan=='harian'){
      $(".harian").show();
      $(".tetap").hide();
    }else{
      $(".harian").hide();
      $(".tetap").show();
    }
  }
  $(document).ready(function(){
    var jenis_karyawan=$('#jenis_karyawan').val();
    if(jenis_karyawan=='harian'){
      $(".harian").show();
      $(".tetap").hide();
    }else if(jenis_karyawan=='tetap'){
      $(".harian").hide();
      $(".tetap").show();
    }else{
     $(".harian").hide();
     $(".tetap").hide();
   }


   

   $("#tabel_daftar").dataTable();
 });

  </script>