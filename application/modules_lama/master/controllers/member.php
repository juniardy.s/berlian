<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class member extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}else{
			$session = $this->session->userdata('astrosession');
			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$this->daftar();		
	}


	public function daftar()
	{
		$data['aktif'] = 'member';
		$data['kode_jalur'] = $this->input->get('jalur');
		$data['konten'] = $this->load->view('member/daftar', $data, TRUE);
		$data['halaman'] = $this->load->view('member/menu', $data, TRUE);
		$this->load->view('member/main', $data);		
	}

	public function tambah()
	{
		$data['aktif'] = 'member';
		$data['konten'] = $this->load->view('member/form', $data, TRUE);
		$data['halaman'] = $this->load->view('member/menu', $data, TRUE);
		$this->load->view('member/main', $data);			
	}

	public function ubah()
	{
		$data['aktif'] = 'member';
		$data['konten'] = $this->load->view('member/form', $data, TRUE);
		$data['halaman'] = $this->load->view('member/menu', $data, TRUE);
		$this->load->view('member/main', $data);			
	}

	public function detail()
	{
		$data['aktif'] = 'member';
		$data['konten'] = $this->load->view('member/detail', $data, TRUE);
		$data['halaman'] = $this->load->view('member/menu', $data, TRUE);
		$this->load->view('member/main', $data);				
	}

	public function print_daftar()
	{

		$this->load->view('member/print_daftar'); 
	}
	public function get_member()
	{

		$this->load->view('member/cari_member'); 
	}
	public function hapus(){
		$kode = $this->input->post("key");
		$this->db->delete( 'master_member', array('id' => $kode) );
		echo '<div class="alert alert-success">Sudah dihapus.</div>';            

	}

	public function get_temp_pembelian(){
		$id = $this->input->post('id');
		$pembelian = $this->db->get_where('opsi_transaksi_pembelian_temp',array('id'=>$id));
		$hasil_pembelian = $pembelian->row();
		echo json_encode($hasil_pembelian);
	}
	public function get_satuan_stok(){
		$param = $this->input->post();
		$satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$param['id_pembelian']));
		$hasil_satuan_stok = $satuan_stok->row();
		$dft_satuan = $this->db->get_where('master_satuan');
		$hasil_dft_satuan = $dft_satuan->result();
        #$desa = $desa->result();
		$list = '';
		foreach($hasil_dft_satuan as $daftar){
			$list .= 
			"
			<option value='$daftar->kode'>$daftar->nama</option>
			";
		}
		$opt = "<option selected='true' value=''>Pilih Satuan Stok</option>";
		echo $opt.$list;
	}

	public function simpan_tambah()

	{
		$data = $this->input->post(NULL, TRUE);

		$jalur = $this->db->get_where('master_jalur',array('kode_jalur'=>$data['kode_jalur']));
		$hasil_jalur = $jalur->row();
		$data['nama_jalur']=@$hasil_jalur->nama_jalur;

		$expedisi = $this->db->get_where('master_expedisi',array('kode_expedisi'=>$data['kode_expedisi']));
		$hasil_expedisi = $expedisi->row();
		$data['nama_expedisi']=@$hasil_expedisi->nama_expedisi;

		$this->db->insert("master_member", $data);
		echo '<div class="alert alert-success">Sudah Tersimpan.</div>';
            //echo $this->db->last_query();          
	}

	public function simpan_ubah()

	{

		$data = $this->input->post(NULL, TRUE);
		$kode = $this->input->post('kode_member');
		
		$jalur = $this->db->get_where('master_jalur',array('kode_jalur'=>$data['kode_jalur']));
		$hasil_jalur = $jalur->row();
		$data['nama_jalur']=@$hasil_jalur->nama_jalur;

		$expedisi = $this->db->get_where('master_expedisi',array('kode_expedisi'=>$data['kode_expedisi']));
		$hasil_expedisi = $expedisi->row();
		$data['nama_expedisi']=@$hasil_expedisi->nama_expedisi;

		$update = $this->db->update("master_member", $data, array('kode_member' => $data['kode_member']));
		echo '<div class="alert alert-success">Sudah Dirubah.</div>';      

		$this->session->set_flashdata('message', $kode);
		$this->session->set_flashdata('jalur', $data['kode_jalur']);
	}

	public function get_table()
	{
		$start = (50*$this->input->post('page'));
		
		$data = $this->input->post();
		$this->db->limit(50, $start);
		
		if(@$data['nama_member']){
			$produk = $data['nama_member'];
			$this->db->like('nama_member',$produk,'both');
		}
		$this->db->order_by('nama_member','asc');
		$member = $this->db->get('master_member');
		$hasil_member = $member->result();
		$nomor = $start+1;
		foreach ($hasil_member as $daftar) {
			?>   
			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo $daftar->kode_member; ?></td>                  
				<td><?php echo $daftar->nama_member; ?></td>                  
				<td><?php echo $daftar->alamat_member; ?></td>
				<td><?php echo $daftar->telp_member; ?></td>
				<td><?php echo $daftar->nama_jalur; ?></td>
				<td><?php echo $daftar->keterangan; ?></td>
				<td><?php echo cek_status($daftar->status_member); ?></td>
				<td align="center"><?php echo get_detail_edit_delete_string($daftar->id); ?></td>
			</tr>

			<?php 
			$nomor++;
		}
	}

}
