<?php
$bulan=$this->input->post('bulan');
$tahun=$this->input->post('tahun');

$bulan_tahun=$tahun.'-'.$bulan;

?>
<center><h3>Laporan Keuangan Periode Bulan <?php echo BulanIndo($bulan);?> Tahun <?php echo $tahun;?></h3></center>
<table  class="table table-striped table-hover table-bordered" id="tabel_daftarr"  style="font-size:1.5em;">

  <?php

  $transaksi_penjualan_reguler=$this->db->query("SELECT SUM(`grand_total`) AS grand_total FROM (`transaksi_penjualan`) WHERE (`jenis_transaksi` = 'tunai' or jenis_transaksi='debit') AND `tanggal_penjualan` LIKE '%$bulan_tahun%' AND `kode_penjualan` LIKE '%PEN\_%'");
  $hasil_transaksi_penjualan_reguler = $transaksi_penjualan_reguler->row();
  $omzet_reguler=$hasil_transaksi_penjualan_reguler->grand_total;

  $this->db->like('tanggal_transaksi',$bulan_tahun);
  $this->db->select_sum('nominal');
  $this->db->like('kode_referensi','PEN_');
  $omzet_hpp_reguler = $this->db->get_where('keuangan_keluar',array('kode_sub_kategori_keuangan' =>'2.6.2'));
  $hasil_omzet_hpp_reguler = $omzet_hpp_reguler->row();
  $nominal_omzet_hpp_reguler=$hasil_omzet_hpp_reguler->nominal;

  $transaksi_penjualan_spoil=$this->db->query("SELECT SUM(`grand_total`) AS grand_total FROM (`transaksi_penjualan`) WHERE (`jenis_transaksi` = 'tunai' or jenis_transaksi='debit') AND `tanggal_penjualan` LIKE '%$bulan_tahun%' AND `kode_penjualan` LIKE '%PS\_%'");
  $hasil_transaksi_penjualan_spoil = $transaksi_penjualan_spoil->row();
  $omzet_spoil=$hasil_transaksi_penjualan_spoil->grand_total;

  $this->db->like('tanggal_transaksi',$bulan_tahun);
  $this->db->select_sum('nominal');
  $this->db->like('kode_referensi','RPEN_');
  $total_retur = $this->db->get_where('keuangan_keluar',array('kode_sub_kategori_keuangan' =>'2.4.1'));
  $hasil_total_retur = $total_retur->row();
  $nominal_total_retur=$hasil_total_retur->nominal;

  $this->db->like('tanggal_transaksi',$bulan_tahun);
  $this->db->select_sum('nominal');
  $pemasukan = $this->db->get_where('keuangan_masuk',array('kode_referensi' =>'manual'));
  $hasil_pemasukan = $pemasukan->row();
  $nominal_pemasukan=$hasil_pemasukan->nominal;

  $this->db->like('tanggal_transaksi',$bulan_tahun);
  $this->db->select_sum('nominal');
  $pengeluaran = $this->db->get_where('keuangan_keluar',array('kode_referensi' =>'manual'));
  $hasil_pengeluaran = $pengeluaran->row();
  $nominal_pengeluaran=$hasil_pengeluaran->nominal;

  $this->db->like('tanggal_transaksi',$bulan_tahun);
  $this->db->select_sum('nominal');
  $pemasukan_opname = $this->db->get_where('keuangan_masuk',array('kode_sub_kategori_keuangan' =>'1.3.1'));
  $hasil_pemasukan_opname = $pemasukan_opname->row();
  $nominal_pemasukan_opname=$hasil_pemasukan_opname->nominal;

  $this->db->like('tanggal_transaksi',$bulan_tahun);
  $this->db->select_sum('nominal');
  $pengeluaran_opname = $this->db->get_where('keuangan_keluar',array('kode_sub_kategori_keuangan' =>'2.9.1'));
  $hasil_pengeluaran_opname = $pengeluaran_opname->row();
  $nominal_pengeluaran_opname=$hasil_pengeluaran_opname->nominal;
  ?>

  <thead>
    <tr width="100%">

      <th>Akun Keuangan</th>
      <th>Debet</th>
      <th>Kredit</th>
    </tr>
  </thead>
  <tbody style="width: 700px;" id="posts">
    <tr>
      <th colspan="3">Transaksi Penjualan</th>
    </tr>
    <tr>
      <td>Omzet Penjualan</td>
      <td><?php echo format_rupiah($omzet_reguler); ?></td>
      <td></td>
    </tr>
    <tr>
      <td>HPP Penjualan</td>
      <td></td>
      <td><?php echo format_rupiah($nominal_omzet_hpp_reguler); ?></td>
    </tr>
    <tr>
      <td>Retur Penjualan</td>
      <td></td>
      <td><?php echo format_rupiah($nominal_total_retur); ?></td>
    </tr>
    <?php
    $debet_penjualan = ($omzet_reguler) - ($nominal_omzet_hpp_reguler) - ($nominal_total_retur);
    $kredit_penjualan = ($nominal_total_retur)  - ($nominal_omzet_hpp_reguler) - ($omzet_reguler);
    ?>
    <tr>
      <td align="center">Sub Total</td>
      <td><?php if($debet_penjualan > 0 and $kredit_penjualan < 0){ echo @format_rupiah($debet_penjualan);}?></td>
      <td><?php if($debet_penjualan < 0 and $kredit_penjualan > 0){ echo @format_rupiah($kredit_penjualan);}?></td>
    </tr>
    <tr>
      <th colspan="3">Transaksi Lainnya</th>
    </tr>
    <tr>
      <td>Pendapatan Lain-lain</td>
      <td><?php echo format_rupiah($nominal_pemasukan); ?></td>
      <td></td>
    </tr>
    <tr>
      <td>Pengeluaran Lain-lain</td>
      <td></td>
      <td><?php echo format_rupiah($nominal_pengeluaran); ?></td>
    </tr>
    <?php
    $sub_debet_lainnya=$nominal_pemasukan-$nominal_pengeluaran;
    $sub_kredit_lainnya=$nominal_pengeluaran-$nominal_pemasukan;
    ?>
    <tr>
      <td align="center">Sub Total</td>
      <td><?php if($sub_debet_lainnya > 0 and $sub_kredit_lainnya < 0){ echo @format_rupiah($sub_debet_lainnya);}?></td>
      <td><?php if($sub_debet_lainnya < 0 and $sub_kredit_lainnya > 0){ echo @format_rupiah($sub_kredit_lainnya);}?></td>
    </tr>
    <tr>
      <th colspan="3">Penyelesaian Opname</th>
    </tr>
    <tr>
      <td>Ganti Rugi</td>
      <td><?php echo format_rupiah($nominal_pemasukan_opname); ?></td>
      <td></td>
    </tr>
    <tr>
      <td>HPP Produk Ganti Rugi</td>
      <td></td>
      <td><?php echo format_rupiah($nominal_pengeluaran_opname); ?></td>
    </tr>
    <?php
    $sub_debet_opname=$nominal_pemasukan_opname-$nominal_pengeluaran_opname;
    $sub_kredit_opname=$nominal_pengeluaran_opname-$nominal_pemasukan_opname;
    ?>
    <tr>
      <td align="center">Sub Total</td>
      <td><?php if($sub_debet_opname > 0 and $sub_kredit_opname < 0){ echo @format_rupiah($sub_debet_opname);}?></td>
      <td><?php if($sub_debet_opname < 0 and $sub_kredit_opname > 0){ echo @format_rupiah($sub_kredit_opname);}?></td>
    </tr>
    <?php
    $total_debet=$nominal_pemasukan_opname + $nominal_pemasukan + ($omzet_reguler);
    $total_kredit=$nominal_pengeluaran_opname + $nominal_pengeluaran +($nominal_omzet_hpp_reguler);

    $total_keuangan_debet=$total_debet -$total_kredit;
    $total_keuangan_kredit=$total_kredit - $total_debet;
    ?>
    <tr>
      <td align="center"><b>Total</b></td>
      <td><b><?php if($total_keuangan_debet > 0 and $total_keuangan_kredit < 0){ echo @format_rupiah($total_keuangan_debet);}?></b></td>
      <td><b><?php if($total_keuangan_debet < 0 and $total_keuangan_kredit > 0){ echo @format_rupiah($total_keuangan_kredit);}?></b>  </td>
    </tr>
  </tbody>

</table>
<label style="font-size: 17px;"><b>
  <?php if($total_keuangan_debet > 0 and $total_keuangan_kredit < 0){ echo "Laba";}?> 
  <?php if($total_keuangan_debet < 0 and $total_keuangan_kredit > 0){ echo "Rugi";}?>
  Periode Bulan <?php echo BulanIndo(date('m'));?> Tahun <?php echo date('Y');?> = 
  <?php if($total_keuangan_debet > 0 and $total_keuangan_kredit < 0){ echo @format_rupiah($total_keuangan_debet);}?>
  <?php if($total_keuangan_debet < 0 and $total_keuangan_kredit > 0){ echo @format_rupiah($total_keuangan_kredit);}?>
</b>
</label>
<br>
<?php
$this->db->like('tanggal_transaksi',$bulan_tahun);
$this->db->select_sum('sisa');
$piutang = $this->db->get('transaksi_piutang');
$hasil_piutang = $piutang->row();
$nominal_piutang=$hasil_piutang->sisa;
?>
<label style="font-size: 17px;"><b>
  Piutang Transaksi Penjualan
  Periode Bulan <?php echo BulanIndo(date('m'));?> Tahun <?php echo date('Y');?> = <?php echo format_rupiah($nominal_piutang);?>

</b>
</label>
<?php
$this->db->like('tanggal_penjualan',$bulan_tahun);
    //$this->db->like('kode_penjualan','PEN_');
$this->db->select_sum('bayar');
$this->db->where('jenis_transaksi','kredit');
$get_debit = $this->db->get('transaksi_penjualan');
$hasil_get_debit = $get_debit->row();
$hasil_debit=$hasil_get_debit->bayar;
?>
<br>
<label style="font-size: 17px;"><b>
  Uang Muka = <?php echo format_rupiah($hasil_debit)?>
</b>
</label>