<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Opname Bahan Baku
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
      <!--   <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_opname'; ?>"><i class="fa fa-list"></i> Opname Nominal </a>
        
        <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_opname_view'; ?>"><i class="fa fa-list"></i> Opname View </a>
        <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_validasi_view'; ?>"><i class="fa fa-check"></i> Validasi View </a> -->
        <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_opname'; ?>"><i class="fa fa-list"></i> Opname  </a>
        <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_validasi_nominal'; ?>"><i class="fa fa-check"></i> Validasi  </a>
        <div class="box-body">            
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">
                <div class="col-md-10 " id="">
                  <div class="col-md-5 " id="">
                    <div class="input-group">
                      <span class="input-group-addon">Filter</span>
                      <select class="form-control" id="kategori_filter">
                        <option value="">- PILIH Filter -</option>
                        <!-- <option value="kategori">Kategori Produk</option> -->
                        <option value="blok">Kategori</option>
                      </select>
                    </div>
                    <br>
                  </div>
                </div>

                <div class="col-md-10 " id="opsi_filter">
                  <div class="col-md-5 " id="">
                    <div class="input-group">
                      <span class="input-group-addon">Filter By</span>
                      <select class="form-control" id="jenis_filter">
                        <option value="">- PILIH Filter -</option>

                      </select>
                    </div>
                    <br>
                  </div>                        
                </div>
                <div class="col-md-10 " id="opsi_filter">
                  <div class="col-md-5 " id="">
                    <button style="width: 100px" type="button" class="btn btn-warning " id="cari"><i class="fa fa-search"></i> Cari</button>
                  </div>
                </div>
              </div> 

            </div>
          </div>
        </form>
        <form id="data_opname">
          <div id="cari_transaksi">
            <a style="padding:13px; margin-bottom:10px;" id="opname" class="btn btn-app green pull-right opname" ><i class="fa fa-edit"></i> Opname </a>
          </div>
          <?php

          $kode_default = $this->db->get('setting_gudang');
          $hasil_unit =$kode_default->row();
          $kode_unit = $hasil_unit->kode_unit; 
          $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
          $hasil_unite = $unit->row();

          $this->db->select_max('id');
          $this->db->where('jenis_bahan', "bahan baku");
          $get_max_po = $this->db->get('transaksi_opname');
          $max_po = $get_max_po->row();

          $this->db->where('id', $max_po->id);
          $get_po = $this->db->get('transaksi_opname');
          $po = $get_po->row();
                  // $tahun = substr(@$po->kode_spoil, 3,4);
                  // if(date('Y')==$tahun){
          $nomor = substr(@$po->kode_opname, 17);
          $nomor = $nomor + 1;
          $string = strlen($nomor);
          if($string == 1){
            $kode_trans = 'OPB_'.date('ymdhis').'_000'.$nomor;
          } else if($string == 2){
            $kode_trans = 'OPB_'.date('ymdhis').'_00'.$nomor;
          } else if($string == 3){
            $kode_trans = 'OPB_'.date('ymdhis').'_0'.$nomor;
          } else if($string == 4){
            $kode_trans = 'OPB_'.date('ymdhis').'_'.$nomor;
          }
                  // } else {
                  //   $kode_trans = 'SP_'.date('Y').'_000001';
                  // }

          ?>
          <input type="hidden" name="kode_opname" id="kode_opname" value="<?php echo $kode_trans;?>">
          <div id="cari_op"> 
            <table class="table table-striped table-hover table-bordered" id="tabel_daftarr"  style="font-size:1.5em;">
             <thead>
              <tr>
                <th>No</th>
                <th>Kode Bahan</th>
                <th>Nama Bahan</th>
                <th>Nama Kategori</th>
                <th align="right">Real Stok</th>

                <th>Action</th>
              </tr>
            </thead>
            <tbody id="daftar_list_stock">

              <?php

              $kode_default = $this->db->get('setting_gudang');
              $hasil_unit =$kode_default->row();
              $param=$hasil_unit->kode_unit;
        
              $this->db->where('status','sendiri');
              $this->db->limit(100);
              $get_stok = $this->db->get_where("master_bahan_baku", array('kode_unit' => $param));
              $hasil_stok = $get_stok->result_array();
              $no=1;
              foreach ($hasil_stok as $item) {

                $kode_bahan = $item['kode_bahan_baku']; 
                $this->db->select_max('id');                       
                $get_kode_bahan = $this->db->get_where('transaksi_stok',array('kode_bahan'=>$kode_bahan,'jenis_transaksi'=>'pembelian'));
                $hasil_hpp_bahan = $get_kode_bahan->row();
              #echo $this->db->last_query();

                $get_hpp = $this->db->get_where('transaksi_stok',array('id'=>$hasil_hpp_bahan->id));
                $hasil_get_hpp = $get_hpp->row();

                $get_stok_min = $this->db->get_where('master_bahan_baku',array('id'=>$item['id']));
                $hasil_stok_min = $get_stok_min->row();
                                  //echo count($hasil_stok_min);
                ?>   
                <tr <?php if($item['real_stock']<=$hasil_stok_min->stok_minimal){echo'class="danger"';}?>>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $item['kode_bahan_baku'];?></td>
                  <td><?php echo $item['nama_bahan_baku'];?></td>
                  <td><?php echo $item['nama_rak'];?></td>
                  <td align="right"><?php echo $item['real_stock'];?> <?php echo $item['satuan_stok'];?></td>

                  <td align="center"><input type="checkbox" id="opsi_pilihan" name="bahan_opname[]" value="<?php echo $item['kode_bahan_baku']; ?>"></td>
                </tr>

                <?php } ?>

              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kode Bahan</th>
                  <th>Nama Bahan</th>
                  <th>Nama Kategori</th>
                  <th align="right">Real Stok</th>

                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
            <a style="padding:13px; margin-bottom:10px;" id="opname" class="btn btn-app green pull-right opname" ><i class="fa fa-edit"></i> Opname </a>
          </div>
          <br><br><br><br><br><br><br><br>
          <br><br><br><br><br><br><br><br>
        </div>

      </form>
      <div class="row">
        <?php 
        $get_jumlah = $this->db->get_where("master_bahan_baku", array('kode_unit' => $param, 'status'=>'sendiri'));
        $jumlah = $get_jumlah->num_rows();
        $jumlah = floor($jumlah/100);
        ?>
        <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
        <input type="hidden" class="form-control pagenum" value="0">
      </div>  
    </div>

    <!------------------------------------------------------------------------------------------------------>

  </div>
</div>
</div><!-- /.col -->
</div>
</div>    
</div>  

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data menu tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    window.location = "<?php echo base_url().'stok/daftar_menu'; ?>";
  });
</script>
<script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});


// $('#cari').click(function(){

//   var tgl_awal =$("#tgl_awal").val();
//   var tgl_akhir =$("#tgl_akhir").val();
//   var kode_unit =$("#kode_unit").val();

//     $.ajax( {  
//       type :"post",  
//       url : "<?php echo base_url().'opname/cari_opname'; ?>",  
//       cache :false,

//       data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit},
//       beforeSend:function(){
//         $(".tunggu").show();  
//       },
//       success : function(data) {
//        $(".tunggu").hide();  
//        $("#cari_transaksi").html(data);
//      },  
//      error : function(data) {  
//          // alert("das");  
//        }  
//      });
//   }

//   $('#tgl_awal').val('');
//   $('#tgl_akhir').val('');

// });
</script>
<script>
  $(window).scroll(function(){
    if (Math.floor($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function load_table(page){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'opname/get_table_nominal' ?>",
      data: ({page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#daftar_list_stock").append(msg);

      }
    });
  }
  $(document).ready(function(){
    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": true,
      "info":     false
    });
    $('#opsi_filter').hide();
  })

  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }

  function delData() {
    var id = $('#id-delete').val();
    var url = '<?php echo base_url().'master/menu_resto/hapus_bahan_jadi'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id: id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $('#modal-confirm').modal('hide');
            // alert(id);
            window.location.reload();
          }
        });
    return false;
  }

  $('#kategori_filter').on('change',function(){
    var kategori_filter = $('#kategori_filter').val();

    var url = "<?php echo base_url() . 'opname/get_jenis_filter' ?>";
    $.ajax({
      type: 'POST',
      url: url,
      data: {kategori_filter:kategori_filter},
      success: function(msg){
        $('#opsi_filter').show();
        $('#jenis_filter').html(msg);
      }
    });
  });

  $('#cari').click(function(){

    var jenis_filter =$("#jenis_filter").val();
    var kategori_filter =$("#kategori_filter").val();
    $.ajax( {  
      type :"post",  
      url : "<?php echo base_url().'opname/cari_opname'; ?>",  
      cache :false,

      data : {jenis_filter:jenis_filter,kategori_filter:kategori_filter},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();  
       $("#cari_op").html(data);
     // $('#jenis_filter').val('');
     // $('#kategori_filter').val('');
   },  
   error : function(data) {  
         // alert("das");  
       }  
     });
  });
  $('.opname').click(function(){

    var kode_opname = $("#kode_opname").val();
    $.ajax( {  
      type :"post",  
      url : "<?php echo base_url().'opname/simpan_opname_temp_baru'; ?>",  
      cache :false,

      data :$('#data_opname').serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();  
       window.location = "<?php echo base_url() . 'opname/tambah_opname_baru/' ; ?>"+kode_opname;
     // $("#cari_transaksi").html(data);
     // // $('#jenis_filter').val('');
     // // $('#kategori_filter').val('');
   },  
   error : function(data) {  
         // alert("das");  
       }  
     });
  });
</script>