<div id="cari_transaksi">

  <table id="tabel_daftar" style="font-size:1.5em;" class="table table-striped table-bordered"> 
   <thead>
    <tr>
      <th>No</th>
      <th>Kode Opname</th>
      <th>Kode Produk</th>
      <th>Nama Produk</th>
      <th>Stok Awal</th>
      <th>Stok Akhir</th>
      <th>Selisih</th>
      <th>Status</th>

      <th>Action</th>
    </tr>
  </thead>
  <tbody id="daftar_list_stock">

    <?php
    $post=$this->input->post();
    
    $this->db->select('*, opsi_transaksi_opname.id as id_opsi, opsi_transaksi_opname.validasi as validasi_opsi');
    $this->db->from('opsi_transaksi_opname');
    if (!empty($post['tanggal_awal']) && !empty($post['tanggal_akhir'])) {
      $this->db->where('transaksi_opname.tanggal_opname >=',@$post['tanggal_awal']);
      $this->db->where('transaksi_opname.tanggal_opname <=',@$post['tanggal_akhir']);
    }else{
      $this->db->where('MONTH(transaksi_opname.tanggal_opname)', date('m'));
      $this->db->where('YEAR(transaksi_opname.tanggal_opname)', date('Y'));
    }

    $this->db->where('opsi_transaksi_opname.jenis_bahan', 'bahan jadi');
    $this->db->order_by('opsi_transaksi_opname.id','DESC');
    $this->db->join('transaksi_opname', 'transaksi_opname.kode_opname = opsi_transaksi_opname.kode_opname', 'left');
    $get_stok = $this->db->get();
    $hasil_stok = $get_stok->result_array();
    $no=1;
    foreach ($hasil_stok as $item) {

      ?>   
      <tr <?php  if($item['validasi_opsi'] =='belum divalidasi') { ?>  style="background: #f1c6c6;"  <?php } ?> >
        <td><?php echo $no++;?></td>
        <td><?php echo $item['kode_opname'];?></td>
        <td><?php echo $item['kode_bahan'];?></td>
        <td><?php echo $item['nama_bahan'];?></td>
        <td><?php echo $item['stok_awal'];?></td>
        <td><?php echo $item['stok_akhir'];?></td>
        <td><?php echo $item['selisih'];?></td>
        <td><?php echo $item['status'];?></td>


        <td align="center">
          <?php if($item['validasi_opsi']=="belum divalidasi" or empty($item['validasi_opsi'])){ ?>
          <a onclick="validasi('<?php echo $item['id_opsi']?>','<?php echo $item['status']?>')" data-toggle="tooltip" title="Validasi" class="btn btn-xs btn-primary validasi"><i class="icon-check"></i> Validasi</a>
          <?php } ?>

          <!-- <a  data-toggle="tooltip" onclick="print('<?php echo $item['kode_opname'];?>')" key="<?php echo $item['kode_opname'];?>" title="Validasi" class="btn btn-xs blue"><i class="fa fa-print"></i>  Print</a> -->
        </td>
      </tr>

      <?php } ?>

    </tbody>
    
  </table>
  <?php 
  $this->db->select('*, opsi_transaksi_opname.validasi as validasi_opsi,opsi_transaksi_opname.id as id_opsi');
  $this->db->from('opsi_transaksi_opname');
  $this->db->where('MONTH(transaksi_opname.tanggal_opname)', date('m'));
  $this->db->where('YEAR(transaksi_opname.tanggal_opname)', date('Y'));
  $this->db->where('opsi_transaksi_opname.jenis_bahan', 'bahan jadi');
  $this->db->order_by('opsi_transaksi_opname.id','DESC');
  $this->db->join('transaksi_opname', 'transaksi_opname.kode_opname = opsi_transaksi_opname.kode_opname', 'left');
  $get_jumlah = $this->db->get();
  $jumlah = $get_jumlah->num_rows();
  $jumlah = floor($jumlah/50);
  ?>
  <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
  <input type="hidden" class="form-control pagenum" value="0">
</div>
<script type="text/javascript">
  $("#tabel_daftar").dataTable({
    "ordering": true,
  });
</script>