 <script src="https://public.azurewebsites.net/js/jquery.table2excel.js"></script>
 <div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Top Produk
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
        <?php

        $get_pemasukan = $this->db->get('keuangan_masuk');
        $hasil_pemasukan = $get_pemasukan->result();
        ?>

        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row">
            <div class="col-md-5" id="trx_penjualan">
              <div class="input-group">
                <span class="input-group-addon">Bulan</span>
                <select class="form-control" name="bulan" id="bulan" required="">              
                  <option value="">
                    -- Bulan --
                  </option>
                  <?php
                  for ($i=1; $i <= 12 ; $i++) { 
                    ?>
                    <option value="<?php if(strlen($i)==1) {
                      echo "0".$i;   
                    }else{
                      echo $i;
                    }  ?>">
                    <?php 
                    if($i==1){
                      echo "Januari";
                    }elseif ($i==2) {
                      echo "Februari";
                    }elseif ($i==3) {
                      echo "Maret";
                    }elseif ($i==4) {
                      echo "April";
                    }elseif ($i==5) {
                      echo "Mei";
                    }elseif ($i==6) {
                      echo "Juni";
                    }elseif ($i==7) {
                      echo "Juli";
                    }elseif ($i==8) {
                      echo "Agustus";
                    }elseif ($i==9) {
                      echo "September";
                    }elseif ($i==10) {
                      echo "Oktober";
                    }elseif ($i==11) {
                      echo "November";
                    }elseif ($i==12) {
                      echo "Desember";
                    }

                    ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>
          <div class="col-md-5" id="trx_penjualan">
            <div class="input-group">
              <span class="input-group-addon">Tahun</span>
              <select class="form-control" name="tahun" id="tahun" required="">              
                <option value="">
                  -- Tahun --
                </option>
                <?php
                $tanggal_sekarang = date('Y');
                $date = $tanggal_sekarang-10;
                for ($i=$date; $i <= $tanggal_sekarang ; $i++) { 
                  ?>
                  <option value="<?php echo $i ?>">
                    <?php echo $i ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>

          <div class=" col-md-1">
            <div class="input-group pull-right">
              <button style="width:100%" type="button" class="btn btn-success" onclick="cari_transaksi()"><i class="fa fa-search"></i> Cari</button>

            </div>
          </div>

          <div class="col-md-1 pull-right">
           <button style="" type="button" class="btn btn-info pull-right" id="cetak"><i class="fa fa-print"></i> Print</button>
         </div>
              <!-- <div class="col-md-1 pull-right">
                 <button style="" type="button" class="btn btn-success pull-right" id="export"><i class="fa fa-table"></i> Export</button>
               </div> -->
             </div><br>
             <div id="aktifitas">
              <table class="table table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
                <thead>

                  <th>Nama Produk</th>
                  <th>Jumlah Terjual</th>

                </thead>
                
                <tbody>
                  <?php
                  $no = 1;
                  $this->db->group_by('kode_menu');
                  $this->db->like('tanggal_transaksi',date('Y-m'));
                  $this->db->order_by('nama_menu','asc');
                  $this->db->select('kode_menu,nama_menu,jumlah,nama_satuan');
                  $this->db->limit('10');
                  $top_produk = $this->db->get('opsi_transaksi_penjualan');
                    #echo $this->db->last_query();
                  $hasil_top_produk = $top_produk->result();
                  foreach($hasil_top_produk as $daftar){
                    ?>
                    <tr>

                      <td><?php echo $daftar->nama_menu; ?></td>
                      <?php
                      $this->db->select_sum('jumlah');
                      $jumlah_terjual = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu'=>$daftar->kode_menu));
                      $hasil_terjual = $jumlah_terjual->row();
                      ?>
                      <td><?php echo $hasil_terjual->jumlah." ".$daftar->nama_satuan; ?></td>
                    </tr>
                    <?php $no++; } ?>
                  </tbody>



                </table>
              </div>

            </div>

            <!------------------------------------------------------------------------------------------------------>

          </div>
        </div>
      </div><!-- /.col -->
    </div>
  </div>    
</div>  
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'analisa/menu_data'; ?>";
  });
</script>


<script>
 $("#cetak").click(function(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  window.open("<?php echo base_url().'top_produk/cetak_laporan_top_produk/'; ?>"+tahun+'/'+bulan);
  
})
 jQuery(document).ready(function() {
  $("#export").click(function(){
    $("#tabel_daftar").table2excel();
  });
});
 $(document).ready(function() {

  $('#tabel_daftar').DataTable( {
    "order": [[ 1, "desc" ]],
    "searching": false
  } );

    //$('#tabel_daftar').dataTable();
  } );

 function cari_transaksi(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  $.ajax( {  
    type :"post",  
    url : "<?php echo base_url().'top_produk/search_aktifitas'; ?>",  
    cache :false,
    data : {bulan:bulan,tahun:tahun},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success : function(data) {
     $(".tunggu").hide();
     $("#aktifitas").html(data);
   },  
   error : function(data) {  
    alert("das");  
  }  
});
}

</script>

