<table class="table table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
  <thead>

    <th>Nama Produk</th>
    <th>Jumlah Terjual</th>

  </thead>

  <tbody>
    <?php
    $no = 1;
    $post = $this->input->post();
    $date = $post['tahun'].'-'.$post['bulan'];
    $this->db->group_by('kode_menu');
    $this->db->like('tanggal_transaksi',$date);
    $this->db->order_by('nama_menu','asc');
    $this->db->select('kode_menu,nama_menu,jumlah,nama_satuan');
    $this->db->limit('10');
    $top_produk = $this->db->get('opsi_transaksi_penjualan');
                    #echo $this->db->last_query();
    $hasil_top_produk = $top_produk->result();
    foreach($hasil_top_produk as $daftar){
      ?>
      <tr>

        <td><?php echo $daftar->nama_menu; ?></td>
        <?php
        $this->db->select_sum('jumlah');
        $jumlah_terjual = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu'=>$daftar->kode_menu));
        $hasil_terjual = $jumlah_terjual->row();
        ?>
        <td><?php echo $hasil_terjual->jumlah." ".$daftar->nama_satuan; ?></td>
      </tr>
      <?php $no++; } ?>
    </tbody>



  </table>