<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/menu_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function tambah()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/tambah_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}
	
	public function daftar()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/daftar_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function detail()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/detail_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function get_produksi_temp($kode){
		$data['kode'] = $kode ;
		$this->load->view('produksi/produksi/tabel_transaksi_temp',$data);
	}
	public function get_barang_lain_temp($kode){
		$data['kode'] = $kode ;
		$this->load->view('produksi/produksi/tabel_barang_lain_temp',$data);
	}
	public function cari_produksi(){

		$this->load->view('produksi/produksi/cari_produksi');
	}
	public function get_table()
	{
		
		$start = (50*$this->input->post('page'));

		$data = $this->input->post();
		$this->db->limit(50, $start);
		if(@$data['tgl_awal'] && @$data['tgl_akhir']){
			
			$tgl_awal = $data['tgl_awal'];
			$tgl_akhir = $data['tgl_akhir'];
			
			
			$this->db->where('tanggal_produksi >=', $tgl_awal);
			$this->db->where('tanggal_produksi <=', $tgl_akhir);
			
			
		}
		$bahan_setengah = $this->db->get_where('transaksi_produksi');
		$hasil_bahan_setengah = $bahan_setengah->result();

		$nomor = $start+1;
		foreach($hasil_bahan_setengah as $daftar){
			?>
			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo TanggalIndo(@$daftar->tanggal_produksi);?></td>
				<td><?php echo @$daftar->kode_produksi; ?></td>
				<td><?php echo @$daftar->jumlah; ?></td>
				<td align="center"><?php echo get_detail($daftar->kode_produksi); ?>
					<?php 
					if(@$daftar->status == 'sesuai' and $daftar->dibayar == ''){
						?>
						<a href="<?php echo base_url().'produksi/pembayaran/'.$daftar->kode_produksi ?>" class="btn btn-warning btn-sm">Pembayaran</a>
						<?php
					}
					?>
				</td>
			</tr>
			<?php $nomor++; 
		}
	}
	public function get_bahan(){
		$jenis_bahan = $this->input->post('jenis_bahan');
		$jenis_produksi = $this->input->post('jenis_produksi');
		if($jenis_produksi=='sendiri'){
			if($jenis_bahan=='Bahan Setengah Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_setengah=$this->db->get_where('master_bahan_setengah_jadi', array('jenis_bahan' => 'sendiri'));
				$hasil_bahan_setengah=$bahan_setengah->result();
				foreach ($hasil_bahan_setengah as $bahan) {
					echo"<option value=".$bahan->kode_bahan_setengah_jadi.">".$bahan->nama_bahan_setengah_jadi."</option>";
				}
			}else if($jenis_bahan=='Bahan Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_jadi=$this->db->get('master_bahan_jadi',array('jenis_bahan' => 'sendiri'));
				$hasil_bahan_jadi=$bahan_jadi->result();
				foreach ($hasil_bahan_jadi as $bahan) {
					echo"<option value=".$bahan->kode_bahan_jadi.">".$bahan->nama_bahan_jadi."</option>";
				}
			}
		}else if($jenis_produksi=='plasma'){
			if($jenis_bahan=='Bahan Setengah Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_setengah=$this->db->get_where('master_bahan_setengah_jadi', array('status' => 'sendiri','jenis_bahan' => 'plasma'));
				$hasil_bahan_setengah=$bahan_setengah->result();
				foreach ($hasil_bahan_setengah as $bahan) {
					echo"<option value=".$bahan->kode_bahan_setengah_jadi.">".$bahan->nama_bahan_setengah_jadi."</option>";
				}
			}else if($jenis_bahan=='Bahan Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_jadi=$this->db->get('master_bahan_jadi',array('status' => 'sendiri','jenis_bahan' => 'plasma'));
				$hasil_bahan_jadi=$bahan_jadi->result();
				foreach ($hasil_bahan_jadi as $bahan) {
					echo"<option value=".$bahan->kode_bahan_jadi.">".$bahan->nama_bahan_jadi."</option>";
				}
			}
		}
		
	}
	public function get_kode_bahan(){
		$jenis_bahan = $this->input->post('jenis_bahan');
		$kode_bahan = $this->input->post('kode_bahan');
		if($jenis_bahan=='Bahan Setengah Jadi'){
			
			$bahan_setengah=$this->db->get_where('master_bahan_setengah_jadi', array('kode_bahan_setengah_jadi'=>$kode_bahan));
			$hasil_bahan=$bahan_setengah->row();
			
		}else if($jenis_bahan=='Bahan Jadi'){
			
			$bahan_jadi=$this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi'=>$kode_bahan));
			$hasil_bahan=$bahan_jadi->row();
			
		}
		echo json_encode($hasil_bahan);
	}
	public function get_kode_barang_lain(){
		
		$kode_barang_lain = $this->input->post('kode_barang_lain');
		

		$bahan_jadi=$this->db->get_where('master_bahan_baku', array('kode_bahan_baku'=>$kode_barang_lain));
		$hasil_barang=$bahan_jadi->row();

		
		echo json_encode($hasil_barang);
	}
	public function get_kode_perintah()
	{
		$kode_perintah = $this->input->post('kode_perintah');
		
		$kode_produksi = $this->input->post('kode_produksi');
		$query = $this->db->get_where('transaksi_perintah_produksi',array('kode_transaksi' => $kode_perintah,'status' =>'menunggu'));
		$data=$query->row();
		//echo $this->db->last_query();
		$jumlah = count($data);
		if($jumlah > 0){
			$cek_temp = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi));
			$hasil_cek = $cek_temp->result();
			if(count($hasil_cek)<1){
				$produksi = $this->db->get_where('opsi_transaksi_perintah_produksi',array('kode_transaksi'=>$kode_perintah));
				$list_produksi = $produksi->result();
				//echo $this->db->last_query();
				foreach($list_produksi as $daftar){ 
					$masukan['kode_produksi'] = $kode_produksi;

					$masukan['kode_bahan'] = $daftar->kode_bahan;
					$masukan['nama_bahan'] = $daftar->nama_bahan; 
					$masukan['jumlah'] = $daftar->jumlah; 
					$masukan['jenis_produksi'] = $data->jenis_produksi;
					if($daftar->kategori_bahan=='setengah_jadi'){
						$kategori_bahan='Bahan Setengah Jadi';
					}else if($daftar->kategori_bahan=='jadi'){
						$kategori_bahan='Bahan Jadi';
					}

					$masukan['kategori_bahan']=$kategori_bahan;
					$masukan['kode_karyawan']=$daftar->kode_karyawan;
					$masukan['nama_karyawan']=$daftar->nama_karyawan;
				// $masukan['kode_satuan'] = $daftar->kode_satuan;
				// $masukan['nama_satuan'] = $daftar->nama_satuan;
					$bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$daftar->kode_bahan,'nama_bahan_baku'=>$daftar->nama_bahan));
					$hasil_bahan = $bahan->row();
				//$masukan['harga_satuan'] = $hasil_bahan->hpp;
                    //$masukan['diskon_item'] = $daftar->diskon_item;
                    //$masukan['kode_supplier'] = $daftar->kode_supplier; 
                    //$masukan['nama_supplier'] = $daftar->nama_supplier; 
				//$masukan['subtotal'] = $hasil_bahan->hpp * $daftar->jumlah;

					$input = $this->db->insert('opsi_transaksi_produksi_temp',$masukan);
				}
			}
			echo "1|".$data->kode_transaksi.'|'.$data->jenis_produksi.'|'.$data->kode_karyawan.'|'.$data->nama_karyawan;
		}
		else{
			
			echo "0";
		}
	}

	public function simpan_produksi_temp(){
		$data=$this->input->post();
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		$hasil_cek_bahan = $cek_bahan->num_rows();
		$hasil_bahan = $cek_bahan->row();
		if($hasil_cek_bahan >0){
			$update_temp['jumlah']=$data['jumlah'] + $hasil_bahan->jumlah;
			$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		}else{
			$data_temp['kode_produksi']=$data['kode_produksi'];
			$data_temp['kode_bahan']=$data['kode_bahan'];
			$data_temp['nama_bahan']=$data['nama_bahan'];
			$data_temp['jumlah']=$data['jumlah'];
			$data_temp['jenis_produksi']=$data['jenis_produksi'];
			$data_temp['kategori_bahan']=$data['jenis_bahan'];
			$data_temp['tanggal_produksi']=date('Y-m-d');
			$input = $this->db->insert('opsi_transaksi_produksi_temp',$data_temp);

		}
		
	}
	public function simpan_barang_lain_temp(){
		$data=$this->input->post();
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		$hasil_cek_bahan = $cek_bahan->num_rows();
		$hasil_bahan = $cek_bahan->row();
		if($hasil_cek_bahan >0){
			$update_temp['jumlah']=$data['jumlah'] + $hasil_bahan->jumlah;
			$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		}else{
			$data_temp['kode_produksi']=$data['kode_produksi'];
			$data_temp['kode_bahan']=$data['kode_bahan'];
			$data_temp['nama_bahan']=$data['nama_bahan'];
			$data_temp['jumlah']=$data['jumlah'];
			$data_temp['jenis_produksi']=$data['jenis_produksi'];
			$data_temp['kategori_bahan']=$data['jenis_barang_lain'];
			$data_temp['status']='barang_lain';
			$data_temp['tanggal_produksi']=date('Y-m-d');
			$input = $this->db->insert('opsi_transaksi_produksi_temp',$data_temp);

		}
		
	}
	public function get_temp_produksi(){
		$id = $this->input->post('id');
		$kode_produksi = $this->input->post('kode_produksi');
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('id'=>$id,'kode_produksi'=>$kode_produksi));
		$hasil_bahan_temp = $cek_bahan->row();
		echo json_encode($hasil_bahan_temp);
	}
	public function get_temp_barang_lain(){
		$id = $this->input->post('id');
		$kode_produksi = $this->input->post('kode_produksi');
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('id'=>$id,'kode_produksi'=>$kode_produksi,'status'=>'barang_lain'));
		$hasil_bahan_temp = $cek_bahan->row();
		echo json_encode($hasil_bahan_temp);
	}
	public function update_produksi_temp(){
		$data=$this->input->post();

		$update_temp['kode_produksi']=$data['kode_produksi'];
		$update_temp['kode_bahan']=$data['kode_bahan'];
		$update_temp['nama_bahan']=$data['nama_bahan'];
		$update_temp['jumlah']=$data['jumlah'];
		$update_temp['jenis_produksi']=$data['jenis_produksi'];
		$update_temp['kategori_bahan']=$data['jenis_bahan'];
		$update_temp['tanggal_produksi']=date('Y-m-d');

		$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('id'=>$data['id'],'kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));

		
	}

	public function update_barang_lain_temp(){
		$data=$this->input->post();

		$update_temp['kode_produksi']=$data['kode_produksi'];
		$update_temp['kode_bahan']=$data['kode_bahan'];
		$update_temp['nama_bahan']=$data['nama_bahan'];
		$update_temp['jumlah']=$data['jumlah'];
		$update_temp['jenis_produksi']=$data['jenis_produksi'];
		$update_temp['kategori_bahan']=$data['jenis_barang_lain'];
		$update_temp['tanggal_produksi']=date('Y-m-d');

		$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('id'=>$data['id'],'kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		echo $this->db->last_query();
		
	}
	public function hapus_produksi_temp(){
		$id = $this->input->post('id');
		$kode_produksi = $this->input->post('kode_produksi');
		$this->db->delete('opsi_transaksi_produksi_temp',array('id'=>$id,'kode_produksi'=>$kode_produksi));
		
	}
	public function simpan_transaksi(){
		$data=$this->input->post();
		$kode_produksi=$data['kode_produksi'];
		$tanggal_produksi=$data['tanggal_produksi'];
		$kode_perintah=$data['kode_perintah'];
		$jenis_produksi=$data['jenis_produksi'];

		$user = $this->session->userdata('astrosession');
		$id_petugas = $user->id;
		$nama_petugas = $user->uname;

		$ambil_temp = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi,'jenis_produksi'=>$jenis_produksi));
		$hasil_ambil_temp = $ambil_temp->result();
		$jml_total_produksi=0;
		foreach ($hasil_ambil_temp as $data_temp) {
			
			$kategori_bahan=$data_temp->kategori_bahan;
			$kode_bahan=$data_temp->kode_bahan;
			$jumlah=$data_temp->jumlah;
			$jml_total_produksi +=$data_temp->jumlah;
			if($kategori_bahan=='Bahan Setengah Jadi'){

				$setengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));
				$hasil_setengah_jadi=$setengah_jadi->row();

				$opsi_setengah_jadi=$this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan));
				$opsi_hasil_setengah_jadi=$opsi_setengah_jadi->result();
				foreach ($opsi_hasil_setengah_jadi as $cek_sisa) {
					$sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan));
					$hasil_sisa_stok=$sisa_stok->row();
					$jumlah_produksi=$cek_sisa->jumlah * $jumlah;
					$sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;
					
					if($hasil_sisa_stok->real_stock < $jumlah_produksi){
						$jml_sisa='kurang';
					}else{
						$jml_sisa='cukup';
					}
					
				}
				//echo $jml_sisa;
				if(@$jml_sisa=='cukup'){
					foreach ($opsi_hasil_setengah_jadi as $opsi_bahan) {
						$bahan_baku=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan));
						$hasil_bahan_baku=$bahan_baku->row();
						$jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
						$real_stock['real_stock']=$hasil_bahan_baku->real_stock - $jumlah_produksi;
						
						$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));	
						
						$cek_bb_plasma=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
						$hasil_cek_bb_plasma=$cek_bb_plasma->num_rows();
						$real_stok_cek_bb_plasma=$cek_bb_plasma->row();
						
						if($hasil_cek_bb_plasma > 0 and $jenis_produksi=='plasma'){
							$real_stock_bb_plasma['real_stock']=$real_stok_cek_bb_plasma->real_stock + $jumlah_produksi;
							
							$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock_bb_plasma,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
						}else if($hasil_cek_bb_plasma == 0 and $jenis_produksi=='plasma'){
							$stok_bb_plasma['kode_bahan_baku']=$hasil_bahan_baku->kode_bahan_baku;
							$stok_bb_plasma['nama_bahan_baku']=$hasil_bahan_baku->nama_bahan_baku;
							$stok_bb_plasma['kode_unit']=@$hasil_bahan_baku->kode_unit;
							$stok_bb_plasma['nama_unit']=@$hasil_bahan_baku->nama_unit;
							$stok_bb_plasma['kode_rak']=@$hasil_bahan_baku->kode_rak;
							$stok_bb_plasma['nama_rak']=@$hasil_bahan_baku->nama_rak;
							$stok_bb_plasma['id_satuan_stok']=@$hasil_bahan_baku->id_satuan_stok;
							$stok_bb_plasma['satuan_stok']=@$hasil_bahan_baku->satuan_stok;
							$stok_bb_plasma['id_satuan_pembelian']=@$hasil_bahan_baku->id_satuan_pembelian;
							$stok_bb_plasma['satuan_pembelian']=@$hasil_bahan_baku->satuan_pembelian;
							$stok_bb_plasma['jumlah_dalam_satuan_pembelian']=@$hasil_bahan_baku->jumlah_dalam_satuan_pembelian;
							$stok_bb_plasma['stok_minimal']=@$hasil_bahan_baku->stok_minimal;
							$stok_bb_plasma['hpp']=@$hasil_bahan_baku->hpp;
							$stok_bb_plasma['kode_supplier']=@$hasil_bahan_baku->kode_supplier;
							$stok_bb_plasma['nama_supplier']=@$hasil_bahan_baku->nama_supplier;
							$stok_bb_plasma['real_stock']=$jumlah_produksi;
							$stok_bb_plasma['status']='plasma';
							$stok_bb_plasma['kode_plasma']=$data['kode_plasma'];
							$stok_bb_plasma['nama_plasma']=$data['nama_plasma'];
							$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_baku',$stok_bb_plasma);
						}


						$transaksi_stok['jenis_transaksi']='produksi';
						$transaksi_stok['kode_transaksi']=$data_temp->kode_produksi;
						$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
						$transaksi_stok['kode_bahan']=$hasil_bahan_baku->kode_bahan_baku;
						$transaksi_stok['nama_bahan']=$hasil_bahan_baku->nama_bahan_baku;
						$transaksi_stok['stok_keluar']=$jumlah_produksi;
						$transaksi_stok['id_petugas']=$id_petugas;
						$transaksi_stok['nama_petugas']=$nama_petugas;
						//$transaksi_stok['jenis_produksi']=$data_temp->jenis_produksi;
						$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);
					}
					if($jenis_produksi=='plasma'){
						$cek_bahan_s_plasma=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
						$hasil_cek_plasma=$cek_bahan_s_plasma->num_rows();
						$plasma_real_stok=$cek_bahan_s_plasma->row();
						if($hasil_cek_plasma > 0){
							$real_stock_setengah_jadi['real_stock']=$plasma_real_stok->real_stock + $jumlah;
							$update_bahan_setengah_jadi=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
						}else{
							$stok_plasma['kode_bahan_setengah_jadi']=$kode_bahan;
							$stok_plasma['nama_bahan_setengah_jadi']=$data_temp->nama_bahan;
							$stok_plasma['kode_unit']=@$hasil_setengah_jadi->kode_unit;
							$stok_plasma['nama_unit']=@$hasil_setengah_jadi->nama_unit;
							$stok_plasma['kode_rak']=@$hasil_setengah_jadi->kode_rak;
							$stok_plasma['nama_rak']=@$hasil_setengah_jadi->nama_rak;
							$stok_plasma['id_satuan_stok']=@$hasil_setengah_jadi->id_satuan_stok;
							$stok_plasma['satuan_stok']=@$hasil_setengah_jadi->satuan_stok;
							$stok_plasma['stok_minimal']=@$hasil_setengah_jadi->stok_minimal;
							$stok_plasma['hpp']=@$hasil_setengah_jadi->hpp;
							$stok_plasma['real_stock']=$jumlah;
							$stok_plasma['status']='plasma';
							$stok_plasma['kode_plasma']=$data['kode_plasma'];
							$stok_plasma['nama_plasma']=$data['nama_plasma'];
							$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_setengah_jadi',$stok_plasma,array('kode_bahan_setengah_jadi'=>$kode_bahan));
						}
					}else if ($jenis_produksi=='sendiri') {
						$real_stock_setengah_jadi['real_stock']=$hasil_setengah_jadi->real_stock + $jumlah;
						$update_bahan_setengah_jadi=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));	
					}
					
					
					$data_opsi['kode_produksi']=$data_temp->kode_produksi;
					$data_opsi['kategori_bahan']=$data_temp->kategori_bahan;
					$data_opsi['kode_bahan']=$data_temp->kode_bahan;
					$data_opsi['nama_bahan']=$data_temp->nama_bahan;
					$data_opsi['jumlah']=$data_temp->jumlah;
					$data_opsi['jenis_produksi']=$data_temp->jenis_produksi;
					$data_opsi['tanggal_produksi']=date('Y-m-d');
					$insert_opsi=$this->db->insert('opsi_transaksi_produksi',$data_opsi);

				}else{
					echo"<div class='alert alert-danger'>Stok Tidak Mencukupi</div>";
				}

			}else if($kategori_bahan=='Bahan Jadi'){
				$bahan_jadi=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));
				$hasil_bahan_jadi=$bahan_jadi->row();

				$opsi_bahan_jadi=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
				$opsi_hasil_bahan_jadi=$opsi_bahan_jadi->result();
				//echo $this->db->last_query();
				foreach ($opsi_hasil_bahan_jadi as $cek_sisa) {
					if($cek_sisa->jenis_bahan=='bahan baku'){
						$sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
						$hasil_sisa_stok=$sisa_stok->row();
					}else if ($cek_sisa->jenis_bahan=='bahan setengah jadi') {
						$sisa_stok=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
						$hasil_sisa_stok=$sisa_stok->row();
					}
					//echo $this->db->last_query();
					$jumlah_produksi=$cek_sisa->jumlah * $jumlah;
					$sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;
					if($hasil_sisa_stok->real_stock < $jumlah_produksi){
						$jml_sisa='kurang';

					}else{
						$jml_sisa='cukup';
					}
					
				}
				//echo $jml_sisa;
				if(@$jml_sisa=='cukup'){
					$opsi_bahan_jadi1=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
					$opsi_hasil_bahan_jadi1=$opsi_bahan_jadi1->result();
					foreach ($opsi_hasil_bahan_jadi1 as $opsi_bahan) {
						
						if($opsi_bahan->jenis_bahan=='bahan baku'){
							$bahan_baku=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
							$hasil_bahan_baku=$bahan_baku->row();
							$jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
							$real_stock['real_stock']=$hasil_bahan_baku->real_stock - $jumlah_produksi;

							$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));	
							
							$bj_bb_plasma=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
							$hasil_bj_bb_plasma=$bj_bb_plasma->num_rows();
							$real_stok_bj_bb_plasma=$bj_bb_plasma->row();
							if($hasil_bj_bb_plasma > 0 and $jenis_produksi=='plasma'){
								$real_stock_bb_plasma['real_stock']=$real_stok_bj_bb_plasma->real_stock + $jumlah_produksi;
								$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock_bb_plasma,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
							}else if($hasil_bj_bb_plasma == 0 and $jenis_produksi=='plasma'){
								$stok_bj_bb_plasma['kode_bahan_baku']=$hasil_bahan_baku->kode_bahan_baku;
								$stok_bj_bb_plasma['nama_bahan_baku']=$hasil_bahan_baku->nama_bahan_baku;
								$stok_bj_bb_plasma['kode_unit']=@$hasil_bahan_baku->kode_unit;
								$stok_bj_bb_plasma['nama_unit']=@$hasil_bahan_baku->nama_unit;
								$stok_bj_bb_plasma['kode_rak']=@$hasil_bahan_baku->kode_rak;
								$stok_bj_bb_plasma['nama_rak']=@$hasil_bahan_baku->nama_rak;
								$stok_bj_bb_plasma['id_satuan_stok']=@$hasil_bahan_baku->id_satuan_stok;
								$stok_bj_bb_plasma['satuan_stok']=@$hasil_bahan_baku->satuan_stok;
								$stok_bj_bb_plasma['id_satuan_pembelian']=@$hasil_bahan_baku->id_satuan_pembelian;
								$stok_bj_bb_plasma['satuan_pembelian']=@$hasil_bahan_baku->satuan_pembelian;
								$stok_bj_bb_plasma['jumlah_dalam_satuan_pembelian']=@$hasil_bahan_baku->jumlah_dalam_satuan_pembelian;
								$stok_bj_bb_plasma['stok_minimal']=@$hasil_bahan_baku->stok_minimal;
								$stok_bj_bb_plasma['hpp']=@$hasil_bahan_baku->hpp;
								$stok_bj_bb_plasma['real_stock']=$jumlah_produksi;
								$stok_bj_bb_plasma['status']='plasma';
								$stok_bj_bb_plasma['kode_plasma']=$data['kode_plasma'];
								$stok_bj_bb_plasma['nama_plasma']=$data['nama_plasma'];
								$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_baku',$stok_bj_bb_plasma);
							}


							$transaksi_stok['jenis_transaksi']='produksi';
							$transaksi_stok['kode_transaksi']=$data_temp->kode_produksi;
							$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
							$transaksi_stok['kode_bahan']=$hasil_bahan_baku->kode_bahan_baku;
							$transaksi_stok['nama_bahan']=$hasil_bahan_baku->nama_bahan_baku;
							$transaksi_stok['stok_keluar']=$jumlah_produksi;
							$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
							$transaksi_stok['id_petugas']=$id_petugas;
							$transaksi_stok['nama_petugas']=$nama_petugas;
							$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);

						}else if ($opsi_bahan->jenis_bahan=='bahan setengah jadi') {
							$bahan_stengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
							$hasil_bahan_stengah_jadi=$bahan_stengah_jadi->row();
							// echo $hasil_bahan_stengah_jadi->real_stock;
							// echo $this->db->last_query();
							$jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
							$real_stock['real_stock']=$hasil_bahan_stengah_jadi->real_stock - $jumlah_produksi;

							$update_bahan_baku=$this->db->update('master_bahan_setengah_jadi',$real_stock,array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));	
							
							$bj_bsj_plasma=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
							$hasil_bj_bsj_plasma=$bj_bsj_plasma->num_rows();
							$real_stok_bj_bsj_plasma=$bj_bsj_plasma->row();
							if($hasil_bj_bsj_plasma > 0 and $jenis_produksi=='plasma'){
								$real_stock_bsj_plasma['real_stock']=$real_stok_bj_bsj_plasma->real_stock + $jumlah_produksi;
								$update_bahan_baku=$this->db->update('master_bahan_setengah_jadi',$real_stock_bsj_plasma,array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
							}else if($hasil_bj_bsj_plasma == 0 and $jenis_produksi=='plasma'){
								$stok_bj_bsj_plasma['kode_bahan_setengah_jadi']=$hasil_bahan_stengah_jadi->kode_bahan_setengah_jadi;
								$stok_bj_bsj_plasma['nama_bahan_setengah_jadi']=$hasil_bahan_stengah_jadi->nama_bahan_setengah_jadi;
								$stok_bj_bsj_plasma['kode_unit']=@$hasil_bahan_stengah_jadi->kode_unit;
								$stok_bj_bsj_plasma['nama_unit']=@$hasil_bahan_stengah_jadi->nama_unit;
								$stok_bj_bsj_plasma['kode_rak']=@$hasil_bahan_stengah_jadi->kode_rak;
								$stok_bj_bsj_plasma['nama_rak']=@$hasil_bahan_stengah_jadi->nama_rak;
								$stok_bj_bsj_plasma['id_satuan_stok']=@$hasil_bahan_stengah_jadi->id_satuan_stok;
								$stok_bj_bsj_plasma['satuan_stok']=@$hasil_bahan_stengah_jadi->satuan_stok;
								$stok_bj_bsj_plasma['hpp']=@$hasil_bahan_stengah_jadi->hpp;
								$stok_bj_bsj_plasma['stok_minimal']=@$hasil_bahan_stengah_jadi->stok_minimal;
								$stok_bj_bsj_plasma['real_stock']=$jumlah_produksi;
								$stok_bj_bsj_plasma['status']='plasma';
								$stok_bj_bsj_plasma['kode_plasma']=$data['kode_plasma'];
								$stok_bj_bsj_plasma['nama_plasma']=$data['nama_plasma'];
								$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_setengah_jadi',$stok_bj_bsj_plasma);
							}

							$transaksi_stok['jenis_transaksi']='produksi';
							$transaksi_stok['kode_transaksi']=$data_temp->kode_produksi;
							$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
							$transaksi_stok['kode_bahan']=$hasil_bahan_stengah_jadi->kode_bahan_setengah_jadi;
							$transaksi_stok['nama_bahan']=$hasil_bahan_stengah_jadi->nama_bahan_setengah_jadi;
							$transaksi_stok['stok_keluar']=$jumlah_produksi;
							$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
							$transaksi_stok['id_petugas']=$id_petugas;
							$transaksi_stok['nama_petugas']=$nama_petugas;
							$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);
						}
						
						
					}
					if($jenis_produksi=='plasma'){
						$cek_bahan_j_plasma=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
						$hasil_cek_plasma=$cek_bahan_j_plasma->num_rows();
						$plasma_real_stok=$cek_bahan_j_plasma->row();
						if($hasil_cek_plasma > 0){
							$real_stock_jadi['real_stock']=$plasma_real_stok->real_stock + $jumlah;
							$update_bahan_jadi=$this->db->update('master_bahan_jadi',$real_stock_jadi,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));	
						}else{
							$stok_plasma['kode_bahan_jadi']=$kode_bahan;
							$stok_plasma['nama_bahan_jadi']=$data_temp->nama_bahan;
							$stok_plasma['kode_unit']=@$hasil_bahan_jadi->kode_unit;
							$stok_plasma['nama_unit']=@$hasil_bahan_jadi->nama_unit;
							$stok_plasma['kode_rak']=@$hasil_bahan_jadi->kode_rak;
							$stok_plasma['nama_rak']=@$hasil_bahan_jadi->nama_rak;
							$stok_plasma['id_satuan_stok']=@$hasil_bahan_jadi->id_satuan_stok;
							$stok_plasma['satuan_stok']=@$hasil_bahan_jadi->satuan_stok;
							$stok_plasma['hpp']=@$hasil_bahan_jadi->hpp;
							$stok_plasma['real_stock']=$jumlah;
							$stok_plasma['status']='plasma';
							$stok_plasma['kode_plasma']=$data['kode_plasma'];
							$stok_plasma['nama_plasma']=$data['nama_plasma'];
							$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_jadi',$stok_plasma,array('kode_bahan_jadi'=>$kode_bahan));
						}
					}else if ($jenis_produksi=='sendiri') {
						$real_stock_jadi['real_stock']=$hasil_bahan_jadi->real_stock + $jumlah;
						$update_bahan_jadi=$this->db->update('master_bahan_jadi',$real_stock_jadi,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));	
					}
					
					
					$data_opsi['kode_produksi']=$data_temp->kode_produksi;
					$data_opsi['kategori_bahan']=$data_temp->kategori_bahan;
					$data_opsi['kode_bahan']=$data_temp->kode_bahan;
					$data_opsi['nama_bahan']=$data_temp->nama_bahan;
					$data_opsi['jumlah']=$data_temp->jumlah;
					$data_opsi['jenis_produksi']=$data_temp->jenis_produksi;
					$data_opsi['tanggal_produksi']=date('Y-m-d');
					$insert_opsi=$this->db->insert('opsi_transaksi_produksi',$data_opsi);

				}else{
					echo"<div class='alert alert-danger'>Stok Tidak Mencukupi</div>";
				}
			}
			if(@$jml_sisa=='cukup'){
				if($jenis_produksi=='plasma'){
					$transaksi_stok_akhir['jenis_transaksi']='produksi';
					$transaksi_stok_akhir['kode_transaksi']=$data_temp->kode_produksi;
					$transaksi_stok_akhir['kategori_bahan']=$data_temp->kategori_bahan;
					$transaksi_stok_akhir['kode_bahan']=$data_temp->kode_bahan;
					$transaksi_stok_akhir['nama_bahan']=$data_temp->kode_bahan;
					$transaksi_stok_akhir['stok_keluar']=$jumlah;
				}else if ($jenis_produksi=='sendiri') {
					$transaksi_stok_akhir['jenis_transaksi']='produksi';
					$transaksi_stok_akhir['kode_transaksi']=$data_temp->kode_produksi;
					$transaksi_stok_akhir['kategori_bahan']=$data_temp->kategori_bahan;
					$transaksi_stok_akhir['kode_bahan']=$data_temp->kode_bahan;
					$transaksi_stok_akhir['nama_bahan']=$data_temp->kode_bahan;
					$transaksi_stok_akhir['stok_masuk']=$jumlah;
				}
				$transaksi_stok_akhir['id_petugas']=$id_petugas;
				$transaksi_stok_akhir['nama_petugas']=$nama_petugas;
				$transaksi_stok_akhir['tanggal_transaksi']=date('Y-m-d');
				$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok_akhir);
				
				$this->db->delete('opsi_transaksi_produksi_temp',array('kode_bahan' =>$kode_bahan,'kode_produksi'=>$kode_produksi ));
			}
			
			
			
		}
		if(@$insert_opsi){
			$status_perintah['status']='selesai';
			$this->db->update('transaksi_perintah_produksi',$status_perintah,array('kode_transaksi'=>$data['kode_perintah']));			

			$produksi['kode_produksi']=$kode_produksi;
			$produksi['kode_perintah_produksi'] = $data['kode_perintah'];
			$produksi['tanggal_produksi']=date('Y-m-d');
			$produksi['kode_plasma']=$data['kode_plasma'];
			$produksi['nama_plasma']=$data['nama_plasma'];
			$produksi['kode_karyawan']=$data['kode_karyawan'];
			$produksi['nama_karyawan']=$data['nama_karyawan'];
			$produksi['jumlah']=$jml_total_produksi;
			$produksi['jenis_produksi']=$data_temp->jenis_produksi;
			$produksi['id_petugas']=$id_petugas;
			$produksi['petugas']=$nama_petugas;
			$produksi['petugas']=$nama_petugas;
			$this->db->insert('transaksi_produksi',$produksi);

			if($jenis_produksi=='plasma' and @$nominal_piutang !=''){
				$piutang_plasma=$this->db->get_where('transaksi_piutang',array('kode_customer'=>$data['kode_plasma'],'status'=>'plasma'));
				$hasil_piutang_plasma=$piutang_plasma->num_rows();
				$jml_piutang_plasma=$piutang_plasma->row();
				if($hasil_piutang_plasma > 0){

					$pitang_update['nominal_piutang']=$jml_piutang_plasma->nominal_piutang + @$nominal_piutang;
					$pitang_update['sisa']=$jml_piutang_plasma->sisa + @$nominal_piutang;
					$this->db->update('transaksi_piutang',$pitang_update,array('kode_customer'=>$data['kode_plasma'],'status'=>'plasma'));
				}else{
					$pitang['kode_transaksi']=$kode_produksi;
					$pitang['petugas']=$nama_petugas;
					$pitang['kode_customer']=$data['kode_plasma'];
					$pitang['nama_customer']=$data['nama_plasma'];
					$pitang['nominal_piutang']=@$nominal_piutang;
					$pitang['sisa']=@$nominal_piutang;
					$pitang['status']=$jenis_produksi;
					$pitang['tanggal_transaksi']=date('Y-m-d');
					$this->db->insert('transaksi_piutang',$pitang);	
				}
				
			}
			
			echo"1|<div class='alert alert-success'>Data Berhasil Disimpan</div>";
		}
		




	}
 public function get_total(){
    $data = $this->input->post();
    $grand_total = @$data['total'] - @$data['angsuran'];
    $hasil = array("rupiah"=>format_rupiah($grand_total),'nilai'=>$grand_total);
    echo json_encode($hasil);
 }
}