
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
         History Produksi Plasma
       </div>
       <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>

    </div>

    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url() . 'produksi/history_produksi' ?>"><i class="fa fa-list"></i> History Karyawan </a>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app blue" href="<?php echo base_url() . 'produksi/history_plasma' ?>"><i class="fa fa-list"></i> History Plasma </a> 


      
      <div class="double bg-green pull-right" style="cursor:default">


        <div  style="padding-right:10px; padding-top:0px; font-size:48px; font-family:arial; font-weight:bold">


        </div>
      </div>
      <br><br
      <div class="box-body">            

        <div class="sukses" ></div>
        <br>
      <!--   <div class="row">
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tanggal Awal</span>
              <input type="text" class="form-control tgl" id="tgl_awal">
            </div>
          </div>
          
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tanggal Akhir</span>
              <input type="text" class="form-control tgl" id="tgl_akhir">
            </div>
          </div>                        
          <div class="col-md-2 pull-left">
            <button style="width: 190px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
          </div>
        </div> -->
         <div class="row">
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Plasma</span>
              <select class="form-control select2" name="kode_plasma" id="kode_plasma">
              <option value="">--Pilih plasma--</option>
              <?php 
              $filter_plasma=$this->db->get('master_plasma');
              $hasil_filter_plasma=$filter_plasma->result();
              foreach ($hasil_filter_plasma as $plasma) {
                ?>
                <option value="<?php echo $plasma->kode_plasma?>"><?php echo $plasma->nama_plasma?></option>
                <?php
              }
              ?>
              </select>
            </div>
          </div>
                           
          <div class="col-md-2 pull-left">
            <button style="width: 190px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
          </div>
        </div><br><br>
        <div id="cari_transaksi">
          <table class="table table-striped table-hover table-bordered" id="daftar_produksi"  style="font-size:1.5em;">
           <?php
         //   $this->db->order_by('id', 'desc');
         // //$this->db->where('position','gudang');
           $this->db->limit(50);
           $produksi = $this->db->get('master_plasma');
           $hasil_produksi = $produksi->result();
           ?>
           <thead>
            <tr>
              <th>No</th>
              <th>Kode Plasma</th>
              <th>Nama Plasma</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $nomor = 1;

            foreach($hasil_produksi as $daftar){ 
            //$jml_produksi=$this->db->get_where('opsi_transaksi_produksi');
              ?> 
              <tr>
                <td><?php echo $nomor; ?></td>
                <td><?php echo @$daftar->kode_plasma; ?></td>
                <td><?php echo @$daftar->nama_plasma; ?></td>
               
                <td align="center">
                    <a href="<?php echo base_url().'produksi/detail_history_plasma/'.$daftar->kode_plasma ?>" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle green"><i class="fa fa-search"></i></a>
                </td>
              </tr>
              <?php $nomor++; } ?>

            </tbody>
            <tfoot>
              <tr>
               <th>No</th>
               <th>Kode Plasma</th>
               <th>Nama Plasma</th>
               <th>Action</th>
             </tr>
           </tfoot>
         </table>
        <!--  <br><br><br><br><br><br><br><br>
         <br><br><br><br><br><br><br><br>
         <?php 
         $get_jumlah = $this->db->get_where('transaksi_produksi');
         $jumlah = $get_jumlah->num_rows();
         $jumlah = floor($jumlah/50);
         ?>
         <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
         <input type="hidden" class="form-control pagenum" value="0">
       </div> -->
       <!--  -->

     </div>

     <!------------------------------------------------------------------------------------------------------>

   </div>
 </div>
</div><!-- /.col -->
</div>
</div>    
</div>  
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>

<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;" />

<div id="modal_setting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
        <label><b><i class="fa fa-gears"></i>Setting</b></label>
      </div>

      <form id="form_setting" >
        <div class="modal-body">
          <?php
          // $setting = $this->db->get('setting_produksi');
          // $hasil_setting = $setting->row();
          ?>

          <div class="box-body">

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label>Note</label>
                  <input type="text" name="keterangan"  class="form-control" />
                </div>

              </div>
            </div>

          </div>

          <div class="modal-footer" style="background-color:#eee">
            <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
  <script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
  <link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
  <script>
  $('.btn-back').click(function(){
    window.location = "<?php echo base_url().'produksi/'; ?>";
  });

  </script>

  <script>
  $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  // function load_table(page){
  //   var tgl_awal =$("#tgl_awal").val();
  //   var tgl_akhir =$("#tgl_akhir").val();
  //   $.ajax({
  //     type: "POST",
  //     url: "<?php echo base_url() . 'produksi/get_table' ?>",
  //     data: ({tgl_awal:tgl_awal,tgl_akhir:tgl_akhir, page:$(".pagenum").val()}),
  //     beforeSend: function(){
  //       $(".tunggu").show();  
  //     },
  //     success: function(msg)
  //     {
  //       $(".tunggu").hide();
  //       $("#posts").append(msg);

  //     }
  //   });
  // }
  </script>
  <script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});


  $('#cari').click(function(){
    var kode_plasma =$("#kode_plasma").val();
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    var kode_unit =$("#kode_unit").val();
    if (tgl_awal=='' || tgl_akhir==''){ 
      alert('Masukan Tanggal Awal & Tanggal Akhir..!')
    }
    else{
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'produksi/cari_history_plasma'; ?>",  
        cache :false,
        beforeSend:function(){
          $(".tunggu").show();  
        },  
        data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit,kode_plasma:kode_plasma},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
         $(".tunggu").hide();  
         $("#cari_transaksi").html(data);
       },  
       error : function(data) {  
         // alert("das");  
       }  
     });
    }

    $('#tgl_awal').val('');
    $('#tgl_akhir').val('');

  });
  </script>
  <script>
  function setting() {
    $('#modal_setting').modal('show');
  }

  $(document).ready(function(){
    $("#daftar_produksi").dataTable({
      "paging":   false,
      "ordering": true,
      "searching": false,
      "info":     false
    });
    
$(".select2").select2();
    $("#form_setting").submit(function(){
      var keterangan = "<?php echo base_url().'produksi/keterangan'?>";
      $.ajax({
        type: "POST",
        url: keterangan,
        data: $('#form_setting').serialize(),
        success: function(msg)
        {
          $('#modal_setting').modal('hide');  
        }
      });
      return false;
    });

  });

  </script>
