
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    
                        <!-- END STYLE CUSTOMIZER -->
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                          Stok Gudang</h3>
                          <div class="page-bar">
                            <ul class="page-breadcrumb">
                              <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Home</a>
                                <i class="fa fa-angle-right"></i>
                              </li>
                              <li>
                                <a href="#">Stok Gudang</a>
                              </li>
                            </ul>
                            <div class="page-toolbar">
                              <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>
                                &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light blue-soft" id="bahan_baku">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-tasks" ></i>
                                </div>
                                <div class="details" >
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Bahan Baku
                                  </div>
                                </div>
                              </a>
                            </div>
                              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light blue-soft" id="bahan_setengah_jadi">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-tasks" ></i>
                                </div>
                                <div class="details" >
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Bahan Setengah Jadi
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light blue-soft" id="bahan_jadi">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-tasks" ></i>
                                </div>
                                <div class="details" >
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Bahan Jadi
                                  </div>
                                </div>
                              </a>
                            </div>
                            
                            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light red-soft"  id="produk">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-shopping-cart"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Produk
                                  </div>
                                </div>
                              </a>
                            </div>-->
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light green-seagreen"  id="stok">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-shopping-cart"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Stok Minimal
                                  </div>
                                </div>
                              </a>
                            </div>
                            </div>
                          <!-- END DASHBOARD STATS -->
                          <div class="clearfix">
                          </div>

                        </div>
                        <style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
            window.location = "<?php echo base_url().'stok/'; ?>";
          });
        </script>
                        <script type="text/javascript">
                          $(document).ready(function(){


                            $("#bahan_baku").click(function(){
                              window.location = "<?php echo base_url() . 'stok/daftar_stok' ?>";

                            });


                            $("#bahan_jadi").click(function(){
                              window.location = "<?php echo base_url() . 'stok/bahan_jadi' ?>";

                            });
                            $("#bahan_setengah_jadi").click(function(){
                              window.location = "<?php echo base_url() . 'stok/bahan_setengah_jadi' ?>";

                            });

                            $("#produk").click(function(){
                              window.location = "<?php echo base_url() . 'stok/produk' ?>";
                            });

                            $("#stok").click(function(){
                              window.location = "<?php echo base_url() . 'stok/daftar_stok_minimal' ?>";
                            });

                            $("#serve").click(function(){
                              window.location = "<?php echo base_url() . 'stok/serve' ?>";
                            });

                          });
                        </script>
