

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->

  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Daftar Stok Retur
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>

          <div class="box-body">            
            <div class="sukses" ></div>
            <div class="row">
          <!--   <div class="col-md-10">
                <div class="">
                  <select class="form-control" id="kode_customer">
                    <option value="">Pilih</option>
                    <?php 
                    $this->db->group_by('nama_customer');
                    $member = $this->db->get_where('transaksi_piutang',array('status'=>'member','kategori_member'=>'konsinyasi'));
                    $hasil_member = $member->result();
                    foreach ($hasil_member as $list) { ?>
                    
                    <option value="<?php echo $list->kode_customer ?>"><?php echo $list->nama_customer ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>


              <div class="col-md-2">
                <button style="width: 147px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
              </div> -->
            </div><br>
            <div id="cari_transaksi">
              <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                <?php
                $this->db->group_by('kode_produk');
                $repair = $this->db->get_where('transaksi_stok_repair',array('status'=>'proses repair'));
                $hasil_repair = $repair->result();
                ?>
                <thead>
                  <tr>
                    <th width="20px">No</th>
                    <th>Nama Produk</th>
                    <th>Stok</th>
                    <th width="200px" >Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $nomor = 1;
                  foreach($hasil_repair as $daftar){ 
                    ?> 
                    <tr>
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo @$daftar->nama_produk; ?></td>

                      <?php
                      $get_nominal_repair=$this->db->query("SELECT sum(jumlah) as jumlah from transaksi_stok_repair where kode_produk='$daftar->kode_produk' and status='proses repair' ");
                      $hasil=$get_nominal_repair->row();
                      ?>
                      <td><?php echo @$hasil->jumlah; ?></td>
                      <td align="center" width="125px">
                        <a class="btn btn-primary" href="<?php echo base_url().'stok/detail_stok_retur/'.$daftar->kode_produk ?>"><i class="fa fa-pencil"></i> Detail</a>
                        <!--    -->
                      </td>
                    </tr>
                    <?php $nomor++; } ?>

                  </tbody>
                  <tfoot>
                   <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Stok</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section> 
  </div>
</section> 
</div>
<script type="text/javascript">
  $(document).ready(function() {

   $("#tabel_daftar").dataTable({
    "paging":   false,
    "ordering": false,
    "info":     false,
    "searching":     false
  });   
 } );
</script>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'stok/'; ?>";
  });
</script>
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<script type="text/javascript">

  $('#cari').click(function(){

    var kode_customer =$("#kode_customer").val();
    $.ajax( {  
      type :"post",  
      url : "<?php echo base_url().'stok/cari_member'; ?>",  
      cache :false,

      data : {kode_customer:kode_customer},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();  
       $("#cari_transaksi").html(data);
     },  
     error : function(data) {  
         // alert("das");  
       }  
     });
  });
</script>
