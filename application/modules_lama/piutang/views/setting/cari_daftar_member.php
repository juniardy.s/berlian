<table  class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">

  <?php

  $this->db->limit(100);
  $data = $this->input->post();
  
  if(@$data['nama_customer']){
    $produk = $data['nama_customer'];
    $this->db->like('nama_customer',$produk,'both');
  }
  $bahan_baku = $this->db->get('transaksi_piutang');
  $hasil_bahan_baku = $bahan_baku->result();
  ?>





  <thead>
   <tr>
    <th width="50px;">No</th>
    <th>Customer</th>
    <th>Nominal Piutang</th>
    <th>Sisa Piutang</th>
    <th width="220px">Action</th>
  </tr>
</thead>
<tbody id="scroll_data">
  <?php

  $no = 1;
  foreach($hasil_bahan_baku as $item){
    if($this->session->flashdata('message')==$item->kode_customer){

      echo '<tr id="warna" style="background: #88cc99; display: none;">';
    }
    else{
      echo '<tr>';
    }
    ?>
    <td><?php echo $no;?></td>
    <td><?php echo $item->nama_customer ?></td>
    <td><?php echo format_rupiah($item->nominal_piutang)?></td>
    <td><?php echo format_rupiah($item->sisa)?></td>
    <td align="center"><a href="<?php echo base_url().'piutang/detail_piutang/'.$item->kode_customer?>" class="btn btn-primary btn-lg"><i class="fa fa-eye"></i> Detail</a></td>
  </tr>
  <?php
  $no++;
} ?>
</tbody>        
   <!--  <tfoot>
      <tr>
       <th>No</th>
       <th>Kode Produk</th>
       <th>Nama Produk</th>
       <th style="display:none;">Kategori Produk</th>
       <th>HPP</th>
       <th>Harga Jual 1</th>
       <th>Harga Jual 2</th>
       <th>Harga Jual 3</th>
       <th>Harga Beli</th>
       <th>Satuan Pembelian</th>
       <th>Satuan Stok</th>
       <th style="width:50px;display:none;">Konversi</th>
       <th>Stok Minimal</th>
       <th>Real Stock</th>

       <th>Action</th>
     </tr>
   </tfoot> -->
 </table>