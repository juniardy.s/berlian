<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//
	
	public function daftar()
	{
		$data['aktif'] = 'barang';
		$data['konten'] = $this->load->view('setting/daftar', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);		
		
	}
	public function daftar_member()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('setting/daftar_member', $data, true);
		$data['halaman'] = $this->load->view('setting/menu', $data, true);
		$this->load->view('main', $data);
		
		
	}
	public function get_load_member()
	{

		$start = (50*$this->input->post('page'));

		$kode = $this->input->post('kode');

		$this->db->limit(50,$start);
		$hasil=$this->db->get_where('transaksi_piutang',array('kode_customer' => $kode ));
		$hasil_ambil = $hasil->result();
		$nomor = $start+1;
		foreach ($hasil_ambil as $item) {
			if($this->session->flashdata('message')==$item->kode_customer){

				echo '<tr id="warna" style="background: #88cc99; display: none;">';
			}
			else{
				echo '<tr>';
			}
			?>
			<td><?php echo $nomor;?></td>
			<td><?php echo $no;?></td>
			<td><?php echo $item->nama_customer ?></td>
			<td><?php echo format_rupiah($item->nominal_piutang)?></td>
			<td><?php echo format_rupiah($item->sisa)?></td>
			<td align="center"><a href="<?php echo base_url().'piutang/detail_piutang/'.$item->kode_customer?>" class="btn btn-primary btn-lg"><i class="fa fa-eye"></i> Detail</a></td>
		</tr>
		<?php
		$no++;

        }//echo $this->db->last_query();
    }

    public function detail_piutang()
    {
    	$data['aktif'] = 'barang';
    	$data['konten'] = $this->load->view('setting/detail_piutang', $data, TRUE);
    	$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
    	$this->load->view('main', $data);		
    	
    }
    public function cari_daftar_piutang()
    {
    	$this->load->view('setting/cari_daftar_piutang');   	
    }
    public function cari_daftar_member()
    {
    	$this->load->view('setting/cari_daftar_member');   	
    }

    public function get_daftar_piutang()
    {

    	$start = (50*$this->input->post('page'));
    	$kode = $this->input->post('kode');

    	$this->db->limit(50,$start);
    	$pembelian = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$kode));
    	$hasil_transaksi = $pembelian->result();
    	$nomor = $start+1;
    	foreach ($hasil_transaksi as $daftar) {
    		$this->db->where('kode_customer', $daftar->kode_customer);
    		$this->db->where('sisa', $daftar->sisa);
    		$this->db->select('SUM(nominal_piutang) as piutang, SUM(sisa) as sisa');
                                  //$this->db->select('(SUM(sisa),)');
    		$get_total = $this->db->get_where('transaksi_piutang');
    		$hasil_total = $get_total->row();
    		$tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : @TanggalIndo(@$daftar->tanggal_transaksi) ;

    		?>
    		<tr class=" <?php if($daftar->sisa !='0'){ echo "danger"; } ?>">
    			<td><?php echo $nomor; ?></td>
    			<td><?php echo @$daftar->kode_transaksi; ?></td>
    			<td><?php echo tanggalindo(@$daftar->tanggal_transaksi); ?></td>
    			<td><?php echo @$daftar->nama_customer; ?></td>
    			<td><?php echo format_rupiah(@$hasil_total->piutang); ?></td>
    			<td><?php echo tanggalindo(@$daftar->jatuh_tempo); ?></td>
    			<td><?php echo cek_status_piutang(@$daftar->sisa); ?></td>
    		</tr>
    		<?php
    	}

    }


	//------------------------------------------ Proses ----------------- --------------------//

    public function get_rupiah(){
    	$angsuran = $this->input->post('angsuran');
    	$hasil = format_rupiah($angsuran);
    	
    	echo $hasil;
    	
    }


    
    
}
