<!DOCTYPE html>
<html>
<head>
   <title></title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">

   <?php
   $bulan=$this->uri->segment(3);
   $tahun=$this->uri->segment(4);
   if (!empty($bulan) and !empty($tahun)){

    $this->db->like('tanggal_penjualan', $tahun."-".$bulan);

 }
// if(@$param['petugas']){
// 	$petugas = $param['petugas'];
// 	$this->db->where('id_petugas',$petugas);
// }
 ?>
 <?php
 $this->db->group_by('tanggal_penjualan');
 $omset = $this->db->get('transaksi_penjualan');
/*$omset = $this->db->query("SELECT tanggal_transaksi,
COUNT(subtotal) AS total_transaksi, SUM(subtotal) as total_omset FROM opsi_transaksi_penjualan
WHERE status_menu = 'reguler' GROUP BY tanggal_transaksi");*/
# echo $this->db->last_query();
$totalnya = 0;
$hasil_omset = $omset->result();

?>
<?php
$bulan=$this->uri->segment(3);
$tahun=$this->uri->segment(4);
if (!empty($bulan) and !empty($tahun)){

   $this->db->like('tanggal_penjualan', $tahun."-".$bulan);

}
// if(@$param['petugas']){
// 	$petugas = $param['petugas'];
// 	$this->db->where('id_petugas',$petugas);
// }
?>
<?php
$this->db->select_sum('grand_total');
$this->db->group_by('tanggal_penjualan');
$total_omset = $this->db->get('transaksi_penjualan');
$hasil_total = $total_omset->result();
$totale = 0;
#echo $this->db->last_query();
foreach($hasil_total as $totalan){
   $totalnya += $totalan->grand_total;   

}                                                                                
?>                            

<img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
<hr>
<h3 align="center">LAPORAN OMSET</h3>
<center>
   <?php 
   if($bulan==1){
     echo "Januari";
  }elseif ($bulan==2) {
     echo "Februari";
  }elseif ($bulan==3) {
     echo "Maret";
  }elseif ($bulan==4) {
     echo "April";
  }elseif ($bulan==5) {
     echo "Mei";
  }elseif ($bulan==6) {
     echo "Juni";
  }elseif ($bulan==7) {
     echo "Juli";
  }elseif ($bulan==8) {
     echo "Agustus";
  }elseif ($bulan==9) {
     echo "September";
  }elseif ($bulan==10) {
     echo "Oktober";
  }elseif ($bulan==11) {
     echo "November";
  }elseif ($bulan==12) {
     echo "Desember";
  }

echo "&nbsp".$tahun;
  ?> 

</center><br>
<label style="font-size: 15px;"><strong>Total Omset : <?php echo format_rupiah($totalnya); ?></strong></label>
<table width="100%" id="tabel_daftar" class="table" border="1" style="border-collapse: collapse;">
   <thead>
      <tr>
         <th>No</th>
         <th>Tanggal</th>
         <th>Total Transaksi</th>
         <th>Omset</th>
      </tr>
   </thead>
   <tbody>

      <?php
      $nomor = 1;

      foreach($hasil_omset as $daftar){ ?> 
      <tr>
         <td align="center"><?php echo $nomor; ?></td>
         <td><?php echo TanggalIndo(@$daftar->tanggal_penjualan);?></td>
         <?php
         $this->db->group_by('kode_penjualan');
         $total_trx = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
         $hasil_total_trx = $total_trx->result();
         ?>                                                                                                                 
         <td align="center"><?php echo count($hasil_total_trx); ?></td>

         <?php

         $this->db->select_sum('grand_total');
         $this->db->select('kode_penjualan');
         $this->db->group_by('kode_penjualan');
         $total_omset = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
#echo $this->db->last_query();
         $hasil_total = $total_omset->result();
         $uang_omset = 0;
         foreach($hasil_total as $daftar){
            $uang_omset += $daftar->grand_total;
         }                                                                                
         ?>                                                                                                                  
         <td align="right"><?php echo format_rupiah(@$uang_omset); ?></td>
      </tr>
      <?php $nomor++; } ?>

   </tbody>
   <tfoot>
      <tr>
         <th>No</th>
         <th>Tanggal</th>
         <th>Total Transaksi</th>
         <th>Omset</th>
      </tr>
   </tfoot>
</table>
</body>
</html>