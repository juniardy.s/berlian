<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggajian extends CI_Controller {
	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

	public function index()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/menu_penggajian', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function tetap()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/daftar_penggajian_tetap', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function tambah_gaji_tetap()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/tambah_gaji_tetap', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function daftar()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/daftar_penggajian', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function detail()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/detail_penggajian', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function detail_gaji_tetap()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/detail_gaji_tetap', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function detail_withdraw()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/detail_withdraw', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function proses_penggajian()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/proses_penggajian', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function get_rupiah()
	{
		$nominal = format_rupiah($this->input->post('value'));
		echo $nominal;
	}

	public function get_gaji()
	{
		$data = $this->input->post();
		$karyawan = $this->db->get_where('master_karyawan', array('kode_karyawan' =>$data['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();
		echo json_encode($hasil_karyawan);

	}
	public function get_total_gaji()
	{
		$data = $this->input->post();
		$this->db->select_sum('total_gaji');
		$karyawan = $this->db->get_where('opsi_transaksi_penggajian', array('kode_karyawan' =>$data['kode_karyawan'],'status' =>'belum dibayar',
			'tanggal >=' =>$data['tgl_awal'],'tanggal <=' =>$data['tgl_akhir']));
		$hasil_karyawan = $karyawan->row();
		echo json_encode($hasil_karyawan);

	}
	public function get_total_kasbon()
	{
		$data = $this->input->post();
		
		$karyawan = $this->db->get_where('transaksi_kasbon', array('kode_karyawan' =>$data['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();
		echo json_encode($hasil_karyawan);

	}
	public function get_gaji_bersih()
	{
		$data = $this->input->post();
		$rupiah_kasbon=format_rupiah($data['angsuran_kasbon']);
		$gaji_bersih= @$data['total_gaji']- @$data['angsuran_kasbon'];
		$hasil=array('rupiah_kasbon'=>$rupiah_kasbon,'gaji_bersih'=>$gaji_bersih);
		echo json_encode($hasil);

	}

	public function simpan_gaji_tetap()
	{
		$input = $this->input->post();
		$user = $this->session->userdata('astrosession');

		$data['kode_karyawan']=$input['kode_karyawan'];
		$karyawan = $this->db->get_where('master_karyawan', array('kode_karyawan' =>$input['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();

		$data['nama_karyawan']=$hasil_karyawan->nama_karyawan;
		$data['total_gaji']=$input['total'];
		$data['gaji_bersih']=$input['total_gaji'];
		$data['tanggal']=$input['tanggal'];
		$data['angsuran_kasbon']=$input['angsuran_kasbon'];
		$data['jenis_karyawan']= 'tetap';
		$this->db->insert('transaksi_penggajian', $data);
		//echo $this->db->last_query();
		$keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.7.1'));
		$hasil_keuangan = $keuangan->row();

		$keluar['kode_jenis_keuangan'] = $hasil_keuangan->kode_jenis_akun;
		$keluar['nama_jenis_keuangan'] = $hasil_keuangan->nama_jenis_akun;
		$keluar['kode_kategori_keuangan'] = $hasil_keuangan->kode_kategori_akun;
		$keluar['nama_kategori_keuangan'] = $hasil_keuangan->nama_kategori_akun;
		$keluar['kode_sub_kategori_keuangan'] = $hasil_keuangan->kode_sub_kategori_akun;
		$keluar['nama_sub_kategori_keuangan'] = $hasil_keuangan->nama_sub_kategori_akun;
		$keluar['nominal'] = $input['total_gaji'];
		$keluar['keterangan'] = "Pembayaran Gaji"." ".$hasil_karyawan->nama_karyawan;
		$keluar['tanggal_transaksi'] = date("Y-m-d");
		$keluar['id_petugas'] =$user->id;
		$keluar['petugas'] = $user->uname;
		$this->db->insert('keuangan_keluar',$keluar);

		if(!empty($input['angsuran_kasbon'])  or $input['angsuran_kasbon']!='0'){

			$get_kasbon=$this->db->get_where('transaksi_kasbon',array('kode_karyawan' =>$input['kode_karyawan']));
			$hasil_kasbon=$get_kasbon->row();
			$data_kasbon['kode_karyawan']=$input['kode_karyawan'];
			$data_kasbon['nama_karyawan']=$hasil_karyawan->nama_karyawan;
			
			$data_kasbon['sisa_kasbon']=$hasil_kasbon->sisa_kasbon -$input['angsuran_kasbon'];
			$data_kasbon['angsuran_kasbon']=$input['angsuran_kasbon'];
			$data_kasbon['tanggal']=date('Y-m-d');

			$this->db->insert('opsi_transaksi_kasbon', $data_kasbon);

			
			$update_kasbon['angsuran_kasbon']=$hasil_kasbon->angsuran_kasbon + $input['angsuran_kasbon'];
			$update_kasbon['sisa_kasbon']=$hasil_kasbon->sisa_kasbon - $input['angsuran_kasbon'];
			$this->db->update('transaksi_kasbon', $update_kasbon,array('kode_karyawan' =>$input['kode_karyawan']));

			$keuangan_masuk = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'1.5.2'));
			$hasil_keuangan_masuk = $keuangan_masuk->row();
			$masuk['kode_jenis_keuangan'] = $hasil_keuangan_masuk->kode_jenis_akun;
			$masuk['nama_jenis_keuangan'] = $hasil_keuangan_masuk->nama_jenis_akun;
			$masuk['kode_kategori_keuangan'] = $hasil_keuangan_masuk->kode_kategori_akun;
			$masuk['nama_kategori_keuangan'] = $hasil_keuangan_masuk->nama_kategori_akun;
			$masuk['kode_sub_kategori_keuangan'] = $hasil_keuangan_masuk->kode_sub_kategori_akun;
			$masuk['nama_sub_kategori_keuangan'] = $hasil_keuangan_masuk->nama_sub_kategori_akun;
			$masuk['nominal'] = $input['angsuran_kasbon'];
			$masuk['keterangan'] = "Angsuran Kasbon ".@$hasil_karyawan->nama_karyawan;
			$masuk['tanggal_transaksi'] = date("Y-m-d");
			$masuk['id_petugas'] =$user->id;
			$masuk['petugas'] = $user->uname;

			$this->db->insert('keuangan_masuk',$masuk);
		}
		//echo $this->db->last_query();


		echo 'berhasil';
		
	}

	public function simpan_withdraw()
	{
		$input = $this->input->post();
		$user = $this->session->userdata('astrosession');
		
		$kode_karyawan=$input['kode_karyawan'];
		$get_gaji = $this->db->get_where('transaksi_penggajian',array('kode_karyawan'=>$kode_karyawan));
		$hasil_get_gaji = $get_gaji->row();

		$get_karyawan = $this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
		$hasil_get_karyawan = $get_karyawan->row();

			// $angsuran 		= $hasil_get_gaji->angsuran_gaji;
			// $hasil_angsuran = $angsuran + $input['angsuran_gaji'];

			// $sisa 			= $hasil_get_gaji->sisa_gaji;
			// $hasil_sisa		= $sisa - $input['angsuran_gaji'];

			// $data['angsuran_gaji']=$hasil_angsuran;
			// $data['sisa_gaji']=$hasil_sisa;
			//$this->db->update("transaksi_penggajian", $data,array('kode_karyawan'=>$kode_karyawan));
             //echo $this->db->last_query();

			// $input['sisa_gaji'] = $input['total_gaji']-$input['angsuran_gaji'];
			// $input['tanggal'] = Date('Y-m-d');
			// $this->db->insert('transaksi_penggajian', $input);

		$data_gaji['kode_karyawan']=$input['kode_karyawan'];
		$data_gaji['nama_karyawan']=$hasil_get_karyawan->nama_karyawan;
		$data_gaji['jenis_karyawan']=$hasil_get_karyawan->jenis_karyawan;
		$data_gaji['total_gaji']=$input['total_gaji'];
		$data_gaji['angsuran_kasbon']=$input['angsuran_kasbon'];
		$data_gaji['gaji_bersih']=$input['total_gaji_bersih'];
		$data_gaji['tanggal_awal']=$input['tgl_awal'];
		$data_gaji['tanggal_akhir']=$input['tgl_akhir'];
		$data_gaji['tanggal']=date('Y-m-d');

		$this->db->insert('transaksi_penggajian', $data_gaji);

		


		$keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.7.1'));
		$hasil_keuangan = $keuangan->row();

		$keluar['kode_jenis_keuangan'] = $hasil_keuangan->kode_jenis_akun;
		$keluar['nama_jenis_keuangan'] = $hasil_keuangan->nama_jenis_akun;
		$keluar['kode_kategori_keuangan'] = $hasil_keuangan->kode_kategori_akun;
		$keluar['nama_kategori_keuangan'] = $hasil_keuangan->nama_kategori_akun;
		$keluar['kode_sub_kategori_keuangan'] = $hasil_keuangan->kode_sub_kategori_akun;
		$keluar['nama_sub_kategori_keuangan'] = $hasil_keuangan->nama_sub_kategori_akun;
		$keluar['nominal'] = $input['total_gaji_bersih'];
		$keluar['keterangan'] = "Pembayaran Gaji $input[nama_karyawan]";
		$keluar['tanggal_transaksi'] = date("Y-m-d");
		$keluar['id_petugas'] =$user->id;
		$keluar['petugas'] = $user->uname;

		$this->db->insert('keuangan_keluar',$keluar);

		if(!empty($input['angsuran_kasbon'])  or $input['angsuran_kasbon']!='0'){

			$get_kasbon=$this->db->get_where('transaksi_kasbon',array('kode_karyawan' =>$input['kode_karyawan']));
			$hasil_kasbon=$get_kasbon->row();
			$data_kasbon['kode_karyawan']=$input['kode_karyawan'];
			$data_kasbon['nama_karyawan']=$input['nama_karyawan'];
			
			$data_kasbon['sisa_kasbon']=$hasil_kasbon->sisa_kasbon -$input['angsuran_kasbon'];
			$data_kasbon['angsuran_kasbon']=$input['angsuran_kasbon'];
			$data_kasbon['tanggal']=date('Y-m-d');

			$this->db->insert('opsi_transaksi_kasbon', $data_kasbon);

			
			$update_kasbon['angsuran_kasbon']=$hasil_kasbon->angsuran_kasbon + $input['angsuran_kasbon'];
			$update_kasbon['sisa_kasbon']=$hasil_kasbon->sisa_kasbon - $input['angsuran_kasbon'];
			$this->db->update('transaksi_kasbon', $update_kasbon,array('kode_karyawan' =>$input['kode_karyawan']));

			$keuangan_masuk = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'1.5.2'));
			$hasil_keuangan_masuk = $keuangan_masuk->row();
			$masuk['kode_jenis_keuangan'] = $hasil_keuangan_masuk->kode_jenis_akun;
			$masuk['nama_jenis_keuangan'] = $hasil_keuangan_masuk->nama_jenis_akun;
			$masuk['kode_kategori_keuangan'] = $hasil_keuangan_masuk->kode_kategori_akun;
			$masuk['nama_kategori_keuangan'] = $hasil_keuangan_masuk->nama_kategori_akun;
			$masuk['kode_sub_kategori_keuangan'] = $hasil_keuangan_masuk->kode_sub_kategori_akun;
			$masuk['nama_sub_kategori_keuangan'] = $hasil_keuangan_masuk->nama_sub_kategori_akun;
			$masuk['nominal'] = $input['angsuran_kasbon'];
			$masuk['keterangan'] = "Angsuran Kasbon $input[nama_karyawan]";
			$masuk['tanggal_transaksi'] = date("Y-m-d");
			$masuk['id_petugas'] =$user->id;
			$masuk['petugas'] = $user->uname;

			$this->db->insert('keuangan_masuk',$masuk);
		}

		
		

		$update_opsi['status']='selesai';
		$this->db->update('opsi_transaksi_penggajian',$update_opsi, array('kode_karyawan' =>$input['kode_karyawan'],'status' =>'belum dibayar',
			'tanggal >=' =>$input['tgl_awal'],'tanggal <=' =>$input['tgl_akhir']));

		echo 'berhasil';
		
	}
}

/* End of file penggajian.php */
/* Location: ./application/modules/penggajian/controllers/penggajian.php */