
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
      .ombo{
        width: 400px;
      } 

      </style>    
      <!-- Main content -->
      <section class="content"> 
        <?php
        $kode_karyawan = $this->uri->segment(3);
        
        $this->db->where('kode_karyawan', $kode_karyawan);
        $get_karyawan = $this->db->get('master_karyawan');
        $hasil_karyawan = $get_karyawan->row();
        //echo $this->db->last_query();
        ?>           
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Detail Gaji Karyawan <?php echo @$hasil_karyawan->nama_karyawan ?>
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body">            
                  <div class="sukses" ></div>
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <?php



                    $this->db->where('kode_karyawan', $kode_karyawan);
                    $produksi = $this->db->get('opsi_transaksi_produksi');
                    $hasil_produksi = $produksi->result();
                    ?>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Produksi</th>
                        <th>Kode Produksi</th>
                        <th>Total</th>
                        <th>Nominal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $nomor = 1;

                      foreach($hasil_produksi as $daftar){
                        if($daftar->kategori_bahan=='Bahan Jadi'){
                          $this->db->where('kode_bahan_jadi', $daftar->kode_bahan);
                          $get_bahan_jadi = $this->db->get('master_bahan_jadi');
                          $hasil_bahan_jadi = $get_bahan_jadi->row();
                          $total_gaji=$daftar->jumlah * @$hasil_bahan_jadi->tarif_borongan;
                          //echo $hasil_bahan_jadi->tarif_borongan;
                        }
                        if($daftar->kategori_bahan=='Bahan Setengah Jadi'){
                          $this->db->where('kode_bahan_setengah_jadi', $daftar->kode_bahan);
                          $get_bahan_jadi = $this->db->get('master_bahan_setengah_jadi');
                          $hasil_bahan_jadi = $get_bahan_jadi->row();
                          $total_gaji=$daftar->jumlah * @$hasil_bahan_jadi->tarif_borongan;
                          //echo $hasil_bahan_jadi->tarif_borongan;
                        }

                        //$total_gaji = $hasil_karyawan->gaji_harian * $daftar->jumlah;
                        ?>
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo TanggalIndo($daftar->tanggal_produksi); ?></td>
                          <td><?php echo $daftar->kode_produksi; ?></td>
                          <td><?php echo $daftar->jumlah; ?></td>
                          <td><?php echo format_rupiah($total_gaji); ?></td>
                        </tr>
                        <?php $nomor++; 
                      } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Produksi</th>
                        <th>Kode Produksi</th>
                        <th>Total</th>
                        <th>Nominal</th>
                      </tr>
                    </tfoot>
                  </table>


                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'penggajian/daftar'; ?>";
    });
    </script>
    


    <script>

    $(document).ready(function(){
      $("#tabel_daftar").dataTable();
    })

    </script>