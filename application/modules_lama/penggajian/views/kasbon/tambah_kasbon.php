
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content"> 

        <!-- Main row -->
        <div class="row">
          <div class="col-md-6">
            <a class="btn green" href="<?php echo base_url().'penggajian/kasbon/tambah' ?>"><i class="fa fa-money"></i> Tambah Kasbon</a>
            <a class="btn blue " href="<?php echo base_url().'penggajian/kasbon/daftar' ?>"><i class="fa fa-money"></i> Daftar Kasbon</a>
          </div><br><br><br><br>
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Form Kasbon
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body ">            
                  <div class="sukses" ></div>
                  <div class="row" >
                    <form id="form">
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Nama Karyawan</label>
                        <select class="form-control" id="kode_karyawan" name="kode_karyawan" required="">
                          <option value="">Pilih</option>
                          <?php 
                          $get_karyawan=$this->db->get_where('master_karyawan',array('status'=>'1'));
                          $hasil=$get_karyawan->result();
                          foreach ($hasil as $list) { ?>

                          <option value="<?php echo $list->kode_karyawan ?>"><?php echo $list->nama_karyawan ?></option>

                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Total Kasbon</label>
                        <input onkeyup="get_rupiah()" type="text" class="form-control" value="" required="" name="total_kasbon" id="total_kasbon"/>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Tanggal</label>
                        <input type="date" class="form-control" value="<?php echo date('Y-m-d') ?>" required="" name="tanggal" id="tanggal"/>
                      </div>
                      <div class="form-group  col-xs-12">
                        <label id="rupiah_kasbon" style="font-size:20px;" ></label>
                        <!-- <button type="submit" class="btn btn-primary pull-right">Simpan</button> -->
                        <br><br>
                        <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
                      </div>
                    </form>
                  </div>
                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'penggajian/'; ?>";
      });
    </script>

    <script>
     //  $("#kode_karyawan").change(function(){
     //   var url = "<?php echo base_url().'penggajian/get_gaji'; ?>";
     //   var kode_karyawan = $("#kode_karyawan").val();
     //   $.ajax( {
     //     type:"POST", 
     //     url : url,  
     //     cache :false,
     //     data :{kode_karyawan:kode_karyawan},
     //     dataType : 'json',
     //     success : function(data) {
     //       $("#total_gaji").val(data.gaji_tetap);
     //     },  
     //     error : function(data) {  
     //      alert(data);  
     //    }  
     //  });
     // })
     
     function get_rupiah(){
      var total_kasbon=$('#total_kasbon').val();
      if (total_kasbon < 0) {

        alert("Nominal tidak boleh kurang dari 0");
        $('#total_kasbon').val('');
        $('#kode_karyawan').val('');
        $('#tanggal').val('');


      }else{
        $.ajax({
          type: "POST",
          url: '<?php echo base_url().'penggajian/kasbon/get_rupiah'; ?>',
          data: {value:total_kasbon},
          beforeSend:function(){
          },
          success: function(msg) {
           $("#rupiah_kasbon").html(msg);
         }
       });
      }
      }


      $("#form").submit(function(){
        $.ajax({
          type: "POST",
          url: '<?php echo base_url().'penggajian/kasbon/simpan_kasbon'; ?>',
          data: $(this).serialize(),
          beforeSend:function(){
          },
          success: function(msg) {
            sukses = '<div class="alert alert-success">Sudah tersimpan.</div>';
            gagal = '<div class="alert alert-danger">Periksa Nominal Withdraw</div>';
            if(msg.length=='7'){
              $(".sukses").html(gagal);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');},1500);
            }else{
              $(".sukses").html(sukses);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'penggajian/kasbon/daftar' ?>";},1500);
            }
          }
        });
        return false;
      });

      $(document).ready(function(){
        $("#tabel_daftar").dataTable();
      })

    </script>