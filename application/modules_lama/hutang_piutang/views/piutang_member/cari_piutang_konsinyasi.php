      
<?php
  $param = $this->input->post();
    if(@$param['tgl_awal'] && @$param['tgl_akhir']){
      $tgl_awal = $param['tgl_awal'];
      $tgl_akhir = $param['tgl_akhir'];
      
      $this->db->where('tanggal_transaksi >=', $tgl_awal);
      $this->db->where('tanggal_transaksi <=', $tgl_akhir);
      $this->db->where('status', 'member');
      $this->db->where('kategori_member', 'konsinyasi');
      
    }


    $this->db->select('*');
    $this->db->from('transaksi_piutang');
    $this->db->order_by('sisa','desc');
    $this->db->group_by('nama_customer');
    $this->db->order_by('tanggal_transaksi','desc');
    $transaksi = $this->db->get();
    $hasil_transaksi = $transaksi->result();

    $total=0;
?>
    
      <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-12">
         
          <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                            <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Nominal Piutang</th>
                    <th>Sisa</th>
                    <th>Status</th>
                    <th width="200px" >Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $nomor = 1;
                  foreach($hasil_transaksi as $daftar){ 
                    ?> 
                     <tr> 
                    <!-- <tr class=" <?php if($daftar->sisa!=0){ echo "danger"; } ?>"> -->
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo @$daftar->nama_customer; ?></td>

                      <?php
                      $get_nominal_hutang=$this->db->query("SELECT sum(nominal_piutang) as nominal,sum(sisa) as sisa from transaksi_piutang where kode_customer='$daftar->kode_customer' and kategori_member='konsinyasi'");
                      $hasil=$get_nominal_hutang->row();
                      ?>
                      <td><?php echo format_rupiah(@$hasil->nominal); ?></td>
                      <td><?php echo format_rupiah(@$hasil->sisa); ?></td>
                      <td><?php if($hasil->sisa=='0'){ ?>
                      <label class="label label-primary">Lunas</label>
                      <?php }else { ?> <label class="label label-warning">Angsuran</label> <?php } ?></td>
                      <!--  <td><?php //echo format_rupiah(@$hasil->sisa); ?></td> -->
                      <!-- <td><?php// echo cek_status_piutang($daftar->sisa); ?></td> -->
                      <td align="center" width="125px">

                       <!--   <a class="btn btn-primary" href="<?php echo base_url().'hutang_piutang/piutang_member/detail_piutang/'.$daftar->kode_customer ?>"><i class="fa fa-pencil"></i> Detail</a> -->
                       <?php
                       echo get_detail_member($daftar->kode_customer);

                       ?></td>
                     </tr>
                     <?php $nomor++; } ?>

                   </tbody>
                   <tfoot>
                     <tr>
                      <th>No</th>
                      <th>Nama Member</th>
                      <th>Nominal Piutang</th>
                      <th>Sisa</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                           </table>
      <div class="row">
       <!--<div class="col-md-10" align="right">
              <label style="font-size: 20px"><b>Total :</label>
        </div>
        <div class="col-md-2 pull-right">
              <span><button style="width: 147px" type="button" class="btn btn-warning pull-right" id="cari"><i class=""></i><?php  echo format_rupiah($total); ?></button></span>
        </div>-->
        </div>
        </div>
        </div>

 <script type="text/javascript">
   $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": false,
      "info":     false
    });
 </script>