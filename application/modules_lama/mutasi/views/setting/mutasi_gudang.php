<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah Mutasi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row">
            <div class="col-md-12">


              <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                  <div class="box box-info">                 
                    <div class="box-body">           

                      <form id="data_form" action="" method="post">

                        <div class="box-body">
                          <div class="callout callout-info">

                          </div>


                          <?php
                          $tgl = date("Y-m-d");
                          $no_belakang = 0;
                          $user = $this->session->userdata('astrosession');
                          $id_user=$user->id;

                          $this->db->select_max('id');
                          $get_max_mut = $this->db->get('transaksi_mutasi');
                          $max_mut = $get_max_mut->row();

                          $this->db->where('id', $max_mut->id);
                          $get_mut = $this->db->get('transaksi_mutasi');
                          $mut = $get_mut->row();
                          
                          $tahun = substr(@$mut->kode_mutasi, 4,4);
                          
                          if(date('Y')==$tahun){
                            $nomor = substr(@$mut->kode_mutasi, 12);
                            
                            $nomor = $nomor + 1;
                            $string = strlen($nomor);
                            if($string == 1){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                            } else if($string == 2){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                            } else if($string == 3){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_000'.$nomor;
                            } else if($string == 4){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_00'.$nomor;
                            } else if($string == 5){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_0'.$nomor;
                            } else if($string == 6){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_'.$nomor;
                            }
                          } else {
                            $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_000001';
                          }
                          ?>
                          <input type="hidden" value="<?php echo @$kode_trans; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_mutasi" id="kode_mutasi" readonly/>
                          <input type="hidden" value="Petugas" class="form-control" placeholder="Kode Transaksi" name="posisi_awal" id="posisi_awal"/>                           
                          <input type="hidden" value="Petugas" class="form-control" placeholder="Kode Transaksi" name="kode_surat_jalan" id="kode_surat_jalan"/>                           
                          <div class="box-body" >

                            <br>
                          </div> 
                          <div class="row">
                            <div class="col-md-2">
                              <h3>Posisi Asal</h3>
                            </div>
                            <div class="col-md-1">
                              <h3> : </h3>
                            </div>
                            <div class="col-md-3">
                              <label>Kategori Petugas </label>
                              <input type="text" readonly class="form-control"   id="kategori_petugas" name="kategori_petugas" />
                            </div>
                            <div class="col-md-3">
                              <label>Petugas </label>
                              <input type="hidden" readonly class="form-control"   id="kode_petugas" name="kode_petugas" />
                              <input type="text" readonly class="form-control"   id="nama_petugas" name="nama_petugas" />

                            </div>
                            <div class="col-md-3 sopir">
                              <label>Sopir </label>
                              <input type="hidden" readonly class="form-control"   id="kode_sopir" name="kode_sopir" />
                              <input type="text" readonly class="form-control"   id="nama_sopir" name="nama_sopir" />

                            </div>
                          </div>
                          <div class="row kendaraan">
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-1">

                            </div>
                            <div class="col-md-3">
                             <label>Kendaraan</label>
                             <input type="text" readonly class="form-control"   id="no_kendaraan" name="no_kendaraan" />  
                             <input type="hidden" readonly class="form-control"   id="nama_kendaraan" name="nama_kendaraan" />
                           </div>
                         </div>
                       </div> 
                       <br>

                       <div class="box-body" >
                        <div class="row">

                          <div class="col-md-2">
                            <h3>Posisi Tujuan</h3>
                          </div>
                          <div class="col-md-1">
                            <h3> : </h3>
                          </div>

                          <div class="col-md-4">
                            <h3>GUDANG</h3>
                          </div>
                        </div>
                      </div> 
                      <div class="sukses" ></div>
                      <br>
                      <br>
                      <div class="gagal" ></div>
                      <div id="list_transaksi_pembelian">
                        <div class="box-body"><br>
                          <table id="tabel_daftar" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>QTY</th>
                                <th>Stok Kembali</th>
                                <th>Stok Terjual</th>
                              </tr>
                            </thead>
                            <tbody id="tabel_temp_data_mutasi">

                            </tbody>
                            <tfoot>

                            </tfoot>
                          </table>
                        </div>

                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Keterangan</label>
                              <textarea class="form-control" value="" name="keterangan" id="keterangan" required=""></textarea>
                            </div>
                          </div>
                        </div>

                      </div>
                      
                      <br>
                      <div class="box-footer ">
                        <!-- <button type="submit" class="btn btn-primary ">Simpan</button> -->
                        <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
                      </div>
                    </form>
                  </div>
                </div>
              </section><!-- /.Left col -->      
            </div>
          </div>
        </div>
      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
<div id="modal-regular" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form  method="post">
        <div class="modal-header" style="background-color:grey">

          <h4 class="modal-title" style="color:#fff;">Mutasi</h4>
        </div>
        <div class="modal-body" >
          <div class="form-body">

           <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Kode Surat Jalan</label>
                <input type="text" id="kode_surat" name="kode_surat" class="form-control" placeholder="Kode Surat Jalan" required="">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer" style="background-color:#eee">
        <a href="<?php echo base_url().'mutasi/daftar_mutasi'; ?>" class="btn blue" aria-hidden="true">Cancel</a>
        <button id="cari_mutasi" class="btn green">Cari</button>
      </div>
    </form>
  </div>
</div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'mutasi/daftar_mutasi'; ?>";
  });
</script>

<script>
  $(document).ready(function(){
  //$("#tabel_daftar").dataTable();
  $("#modal-regular").modal('show');
  $("#update").hide();
  
  $(".sopir").hide();
  $(".kendaraan").hide();
  $("#cari_mutasi").click(function(){
    var kode_surat_jalan = $('#kode_surat').val();  
    
    var url_cari = "<?php echo base_url().'mutasi/get_surat_jalan'?>";
    $.ajax({
      type: "POST",
      url: url_cari,
      data: {kode_surat_jalan:kode_surat_jalan},
      dataType:'json',
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $("#modal-regular").modal('hide');
        $(".tunggu").hide();  
        if(msg.kode_mutasi){
          $('#kode_surat_jalan').val(kode_surat_jalan);
          $('#kategori_petugas').val(msg.kategori_petugas);
          $('#kode_petugas').val(msg.kode_unit_tujuan);
          $('#nama_petugas').val(msg.nama_unit_tujuan);
          if(msg.kategori_petugas=='sales & sopir'){
            $(".sopir").show();
            $(".kendaraan").show();
            $("#kode_sopir").val(msg.kode_sopir);
            $("#nama_sopir").val(msg.nama_sopir);
            $(".kendaraan").show();
            $("#no_kendaraan").val(msg.no_kendaraan);
            $("#nama_kendaraan").val(msg.nama_kendaraan);
          } else if(msg.kategori_petugas=='sopir'){
           $(".sopir").hide();
           $("#kode_sopir").val(msg.kode_sopir);
           $("#nama_sopir").val(msg.nama_sopir);
           $(".kendaraan").show();
           $("#no_kendaraan").val(msg.no_kendaraan);
           $("#nama_kendaraan").val(msg.nama_kendaraan);
          }else{
            $(".sopir").hide();
            $(".kendaraan").hide();
            $("#no_kendaraan").val('');
            $("#nama_kendaraan").val('');
            $("#kode_sopir").val('');
            $("#nama_sopir").val('');
          }

          $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_mutasi_temp/'; ?>"+kode_surat_jalan);
          get_jenis_bahan();
        }else if(msg.sudah_tutup){
          alert('Kode Surat Jalan Sudah Di Tutup');
          window.location = "<?php echo base_url() . 'mutasi/tambah_mutasi_gudang' ?>";
        }else{
          alert('Kode Surat Jalan Tidak Ditemukan');
          window.location = "<?php echo base_url() . 'mutasi/tambah_mutasi_gudang' ?>";
        }

    }
  });
    return false;
  });

  $("#kode_rak").change(function(){
    var jenis_mutasi = $("#jenis_mutasi").val();
    var kode_petugas = $("#kode_petugas").val();
    var kode_rak = $("#kode_rak").val();
    var kategori_petugas = $("#kategori_petugas").val();
    var url = "<?php echo base_url().'mutasi/get_jenis_bahan/'?> ";
    $.ajax({
      type: "POST",
      url: url,
      data: {kode_rak:kode_rak, jenis_mutasi:jenis_mutasi,kode_petugas:kode_petugas,kategori_petugas:kategori_petugas},

      success: function(data)
      {
        $(".tunggu").hide(); 
        $("#kode_bahan").html('');
        $("#kode_bahan").html(data);
      }            
    });
  });
  $("#kategori_petugas").change(function(){

    var kategori_petugas = $("#kategori_petugas").val();
    var url = "<?php echo base_url().'mutasi/get_petugas_kategori/'?> ";
    $.ajax({
      type: "POST",
      url: url,
      data: {kategori_petugas:kategori_petugas},


      success: function(data)
      {
        $(".tunggu").hide(); 
        $("#kode_petugas").html(data);
      }            
    });
  });

  $("#kode_petugas").change(function(){
   var kategori_petugas = $("#kategori_petugas").val();
   var kode_petugas = $("#kode_petugas").val();
   var url = "<?php echo base_url().'mutasi/get_petugas/'?> ";
   $.ajax({
    type: "POST",
    url: url,
    data: {kode_petugas:kode_petugas,kategori_petugas:kategori_petugas},
    dataType:'json',

    success: function(data)
    {
      $(".tunggu").hide(); 
        //alert(data.nama_sales);
        if(data.nama_sales){
          $("#nama_petugas").val(data.nama_sales);  
        }else if(data.nama_sopir){
          $("#nama_petugas").val(data.nama_sopir);  
        }
        
      }            
    });
 });

  $('#kode_bahan').on('change',function(){

    var kode_bahan = $('#kode_bahan').val();
    var url = "<?php echo base_url() . 'mutasi/get_satuan' ?>";
    $.ajax({
      type: 'POST',
      url: url,
      dataType:'json',
      data: {kode_bahan:kode_bahan},
      success: function(msg){
        if(msg.satuan_stok){
          $('#satuan_stok').val(msg.satuan_stok);
        }

        if(msg.nama_bahan_jadi){
          $('#nama_bahan').val(msg.nama_bahan_jadi);
        }
        if(msg.real_stock){
          $('#stok_awal').val(msg.real_stock);
        }
        if(msg.hpp){
          $('#hpp').val(msg.hpp);
        }
      }
    });
  });

  $("#data_form").submit(function(){
    var simpan_mutasi = "<?php echo base_url().'mutasi/simpan_mutasi/'?>";
    $.ajax({
      type: "POST",
      url: simpan_mutasi,
      data: $('#data_form').serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
         $(".tunggu").hide();  
        if(msg=="stok kurang"){

          $(".sukses").html('<div class="alert alert-warning">Mutasi lebih kecil dari bahan pending</div>');
          setTimeout(function(){$('.sukses').html('');},1000); 
        }else{

          $(".sukses").html(msg);   

          setTimeout(function(){$('.sukses').html('');
           window.location = "<?php echo base_url() . 'mutasi/daftar_mutasi' ?>";

         },1000);  
        }

      }
    });
    return false;

  });

})

function add_item(){
  var kode_mutasi = $("#kode_mutasi").val();
  var kode_rak = $("#kode_rak").val();
  var kode_petugas = $("#kode_petugas").val();  
  var kategori_petugas = $("#kategori_petugas").val();  
  var kode_bahan = $('#kode_bahan').val();
  var nama_bahan = $('#nama_bahan').val();
  var jumlah = $('#jumlah').val();
  var jenis_mutasi = $("#jenis_mutasi").val();
  var url = "<?php echo base_url().'mutasi/simpan_item_mutasi_temp/'?> ";

  $.ajax({
    type: "POST",
    url: url,
    data: { 
      kode_mutasi:kode_mutasi,
      kode_rak:kode_rak,
      kode_petugas:kode_petugas,
      kategori_petugas:kategori_petugas,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      jumlah:jumlah,
      jenis_mutasi:jenis_mutasi
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(hasil)
    {
      $(".tunggu").hide();
      var data = hasil.split("|");
      var num = data[0];
      var pesan = data[1];
      if(num==1){
        $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_mutasi_temp/'; ?>"+kode_mutasi);
        $('#jenis_bahan').val('');
        $('#kode_bahan').val('');
        $('#jumlah').val('');
      }
      else {
        $(".gagal").html(pesan);   
        setTimeout(function(){
          $('.gagal').html('');
        },1500);
      }               
    }
  });
}

function actDelete(Object) {
  $('#id-delete').val(Object);
  $('#modal-confirm').modal('show');
}

function delData() {
  var id = $('#id-delete').val();
  var url = '<?php echo base_url().'master/menu_resto/hapus_bahan_jadi'; ?>/delete';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id: id
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
            // alert(id);
            window.location.reload();
          }
        });
  return false;
}

function actEdit(id) {
  var id = id;
  var kode_mutasi = $('#kode_mutasi').val();
  var url = "<?php echo base_url().'mutasi/get_temp_mutasi'; ?>";
  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: {id:id},
    success: function(mutasi){
      $('#jenis_bahan').val(mutasi.kategori_bahan);
      get_jenis_bahan2(mutasi.kode_bahan);
      $("#nama_bahan").val(mutasi.nama_bahan);
      $('#jumlah').val(mutasi.jumlah);
      $("#id_item_temp").val(mutasi.id);
      $("#add").hide();
      $("#update").show();
      $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_mutasi_temp/'; ?>"+kode_mutasi);
    }
  });
}

function update_item(){
  var kode_mutasi = $("#kode_mutasi").val();
  var kode_rak = $("#kode_rak").val();
  var kode_petugas = $("#kode_petugas").val();
  var kategori_petugas = $("#kategori_petugas").val();    
  var kode_bahan = $('#kode_bahan').val();
  var nama_bahan = $('#nama_bahan').val();
  var jumlah = $('#jumlah').val();
  var jenis_mutasi = $("#jenis_mutasi").val();
  var id_item_temp = $("#id_item_temp").val();
  var url = "<?php echo base_url().'mutasi/ubah_item_mutasi_temp/'?> ";

  $.ajax({
    type: "POST",
    url: url,
    data: { kode_mutasi:kode_mutasi,
      kode_rak:kode_rak,
      kode_petugas:kode_petugas,
      kategori_petugas:kategori_petugas,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      jumlah:jumlah,
      jenis_mutasi:jenis_mutasi,
      id:id_item_temp
    },
    success: function(data)
    {
      $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_mutasi_temp/'; ?>"+kode_mutasi);
      $('#kategori_bahan').val('');
      $('#kode_bahan').val('');
      $('#jumlah').val('');
      $("#nama_bahan").val('');
      $("#id_item_temp").val('');
      $("#add").show();
      $("#update").hide();

    }
  });
}

function delData() {
  var id = $('#id-delete').val();
  var kode_mutasi = $('#kode_mutasi').val();
  var url = "<?php echo base_url().'mutasi/hapus_temp'; ?>";
  $.ajax({
    type: 'POST',
    url: url,
    data: {id:id},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(pembelian){
      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
      $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_mutasi_temp/'; ?>"+kode_mutasi);
     // $('#kategori_bahan').val('');
     $('#kode_bahan').val('');
     $('#jumlah').val('');
   }
 });
}

function get_jenis_bahan(){

  var jenis_bahan = $("#jenis_bahan").val();
  var kode_petugas = $("#kode_petugas").val();
  var kode_rak = $("#kode_rak").val();
  var kategori_petugas = $("#kategori_petugas").val();
  var url = "<?php echo base_url().'mutasi/get_jenis_bahan/'?> ";
  $.ajax({
    type: "POST",
    url: url,
    data: {kode_rak:kode_rak, jenis_bahan:jenis_bahan, kode_petugas:kode_petugas,kategori_petugas:kategori_petugas},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(data)
    {
      $(".tunggu").hide(); 
      $("#kode_bahan").html('');
      $("#kode_bahan").html(data);
    }            
  });
}
function get_jenis_bahan2(Object){
  var kode_petugas = $("#kode_petugas").val();
  var jenis_mutasi = $("#jenis_mutasi").val();
  var kode_rak = $("#kode_rak").val();
  var url = "<?php echo base_url().'mutasi/get_jenis_bahan/'?> ";
  $.ajax({
    type: "POST",
    url: url,
    data: {kode_rak:kode_rak, jenis_mutasi:jenis_mutasi,kode_petugas:kode_petugas},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(data)
    {
     $(".tunggu").hide(); 
     $("#kode_bahan").html('');
     $("#kode_bahan").html(data);
     $("#kode_bahan").val(Object);
   }            
 });


}
function hitung_sisa(obj){
  mutasi = parseInt($(obj).val());
  form_jumlah = $(obj).parent().parent().find('#jumlah');
  jumlah = parseInt(form_jumlah.val());
  label_sisa = $(obj).parent().parent().find('#sisa_stok');
  sisa = jumlah - mutasi;
  label_sisa.text('');
  if(!isNaN(sisa)){
    label_sisa.text(sisa);
    
    if (sisa<0) {
      alert('jumlah mutasi tidak boleh melebihi jumlah awal maupun kurang dari 0 !')
      $(obj).val(jumlah);
      label_sisa.text('0');
    } else if (mutasi<0) {
      alert('jumlah mutasi tidak boleh melebihi jumlah awal maupun kurang dari 0 !')
      $(obj).val('0');
      label_sisa.text(jumlah);
    }
  }
}

</script>
