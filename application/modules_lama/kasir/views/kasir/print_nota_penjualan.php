<!DOCTYPE html>
<html>
<head>
	<title>PRINT NOTA PENJUALAN</title>
	<style type="text/css">
		.table1{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 0px;
			text-align: left;
			vertical-align: middle;
		}

		.table2{
			width:100%; 
			border-collapse: collapse;
		}

		.table3{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 160px;
			text-align: left;
			vertical-align: middle;
		}

		@media print {
			html, body {


				display: block;
				font-family: "Dotrice";
				font-size: auto;
			}

			@page
			{
				size: 21cm 14cm;
			}

		}

		div.page { page-break-after: always;
			position: relative;
			margin:10px 15px 0px 15px;
			padding:0px; }

		</style>
	</head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php
	$param=$this->uri->segment(3);
	$transaksi = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$param));
	$hasil_transaksi = $transaksi->row();
	?>
	<style>
		html, body {
			font-weight: 600;
			font-family: Arial, Helvetica, sans-serif !important;
		}
	</style>
	<body onload="print()">
		<table width="100%">
			<tr>
				<td colspan="2"><h4><b>BERLIAN BROOM</b></h4></td>
				<td colspan="2" rowspan="2" align="center"><b>NOTA PENJUALAN</b></td>
				<td colspan="2" rowspan="2" align="center"><b>NO. SURAT JALAN :</b></td>
			</tr>
			<tr>
				<td colspan="2">www.sapuberlian.com</td>
			</tr>
			<tr>
				<td colspan="2">(031) 7991119/ 7997597/ WA 08128322744</td>
				<td>NO. NOTA :</td>
				<td><?php echo $hasil_transaksi->kode_penjualan; ?></td>
				<td>TANGGAL :</td>
				<td><?php echo tanggalIndo($hasil_transaksi->tanggal_penjualan); ?></td>
			</tr>
			<tr>
				<td colspan="2">Jl. Gurang Anyar No 17 - 19, Cerme, Gresik</td>
				<td>NAMA SALES :</td>
				<td><?php echo @$hasil_transaksi->nama_petugas; ?></td>
				<td>TUAN/TOKO :</td>
				<td><?php echo @$hasil_transaksi->nama_member; ?></td>
			</tr>
		</table>
		<br>
		<table border="1"  width="100%" class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">KODE BARANG</th>
					<th style="text-align: center;">NAMA BARANG</th>
					<th style="text-align: center;">JUMLAH BARANG</th>
					<th style="text-align: center;">HARGA SATUAN</th>
					<th style="text-align: center;">JUMLAH HARGA</th>
					<th style="text-align: center;">KETERANGAN</th>
				</tr>
			</thead>
			<tbody>	
				<?php

				$opsi_transaksi = $this->db->get_where('opsi_transaksi_penjualan',array('kode_penjualan'=>$param));
				$hasil_opsi_transaksi = $opsi_transaksi->result();
				$no=1;
				$total = 0 ;
				foreach ($hasil_opsi_transaksi as  $value) {
					$total += $value->subtotal;
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td align="left" width="50%" ><?php echo @$value->kode_menu;?></td>
						<td align="center" width="50%" ><?php echo @$value->nama_menu;?></td>
						<td align="center" ><?php echo @$value->jumlah;?></td>
						<td align="right" style="text-align: right;"> <?php echo @format_rupiah($value->harga_satuan);?></td>
						<td align="right" style="text-align: right;"> <?php echo @format_rupiah($value->subtotal);?></td>
						<td align="left" style="text-align: center;"></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td style="text-align: right;" colspan="5"><b>TOTAL</b></td>
					<td style="text-align: right;"><?php echo @format_rupiah($total);?></td>
				</tr>
			</tbody>
		</table>
		<table width="100%">
			<tr>
				<td colspan="2" width="20%">Syarat Pembayaran</td>
				<td width="30%">: <?php echo @$hasil_transaksi->jenis_transaksi; ?></td>
				<td rowspan="7" width="10%"><img src="<?php echo base_url().'component/img/logo berlian tm.jpg' ?>" width="150px" alt="" title="" /></td>
				<td align="center">Pengirim</td>
				<td align="center">Penerima</td>
			</tr>
			<tr>
				<td colspan="2" width="20%">Jatuh Tempo</td>
				<?php
				$get_piutang = $this->db->get_where('transaksi_piutang', array('kode_transaksi' => $param));
				$hasil_piutang = $get_piutang->row();
				?>
				<td>: <?php if($hasil_transaksi->jenis_transaksi == 'kredit'){echo tanggalIndo(@$hasil_piutang->jatuh_tempo);} ?></td>
				<td rowspan="5" width="20%"></td>
				<td rowspan="5" width="20%"></td>
			</tr>
			<tr>
				<td colspan="2" width="20%">CH/BG A/N</td>
				<td width="30%">: ULIYANAH</td>
			</tr>
			<tr>
				<td colspan="3"></td>
			</tr>
			<tr>
				<td colspan="2" width="20%">BCA BENOWO SURABAYA</td>
				<td width="30%">: No. Rek 7900 7333 00</td>
			</tr>
			<tr>
				<td colspan="2" width="20%">BANK MANDIRI SURABAYA</td>
				<td width="30%">: No. Rek 14200 55555 089</td>
			</tr>
			<tr>
				<td colspan="2" width="20%">BANK BRI BENOWO SURABAYA</td>
				<td width="30%">: No. Rek 3136 0100 3116 507</td>
				<td align="center">(<?php echo @$hasil_transaksi->nama_sopir; ?>)</td>
				<td align="center">(<?php echo @$hasil_transaksi->nama_member; ?>)</td>
			</tr>
		</table>
	</body>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</html>