<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class input_plasma extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['konten'] = $this->load->view('setting/daftar', NULL, TRUE);
        $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
        $this->load->view ('main', $data);
    }

    public function tambah()
    {
      $data['konten'] = $this->load->view('setting/form_sendiri', NULL, TRUE);
      $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
      $this->load->view ('main', $data);		
  }

  public function get_transaksi($kode){
      $data['kode'] = $kode ;
      $this->load->view('input_plasma/setting/tabel_transaksi_temp',$data);
  }

  public function detail()
  {
      $data['konten'] = $this->load->view('setting/detail', NULL, TRUE);
      $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
      $this->load->view ('main', $data);		
  }

  public function get_barang(){
    $data = $this->input->post();
        #echo $data['key'];
    if($data['key']=="setengah_jadi"){
        $barang = $this->db->get_where('master_bahan_setengah_jadi',array('status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
        $hasil_barang = $barang->result();
            #echo $this->db->last_query();
        echo "<option value=''>--Pilih Produk--</option>";
        foreach($hasil_barang as $daftar){
            echo "
            <option value='$daftar->kode_bahan_setengah_jadi'>$daftar->nama_bahan_setengah_jadi</option>";
        }

    }else{
        $barang = $this->db->get_where('master_bahan_jadi',array('status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
        $hasil_barang = $barang->result();
            echo " <option value=''>--Pilih Produk--</option>";#echo $this->db->last_query();
            foreach($hasil_barang as $daftar){
                echo "
                <option value='$daftar->kode_bahan_jadi'>$daftar->nama_bahan_jadi</option>";
            }
        }
    }

    public function get_satuan(){
        $data = $this->input->post();
        $kode_bahan=$data['kode_barang_lain'];
        $barang = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan));
        $hasil_barang = $barang->row();
            //echo $this->db->last_query();
        echo (@$hasil_barang->hpp);


    }
    
    public function hapus_bahan_jenis(){
        $data = $this->input->post();
        $this->db->delete('opsi_transaksi_input_plasma_temp',array('kode_transaksi'=>$data['kode_transaksi']));
        // echo $this->db->last_query();
    }

    public function get_rupiah(){

        $nominal=$this->input->post('nominal');

        echo @format_rupiah($nominal);

    }
    public function get_piutang(){

        $kode_plasma=$this->input->post('kode_plasma');
        $piutang = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$kode_plasma));
        $list_piutang = $piutang->row();
        echo format_rupiah(@$list_piutang->sisa);

    }
    public function simpan_temp(){
        $data = $this->input->post();
        
        $nama = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$data['kode_bahan']));
        $hasil_nama = $nama->row();
        $data['nama_bahan'] = @$hasil_nama->nama_bahan_baku;
        if($data['jumlah'] > @$hasil_nama->real_stock and $data['kebutuhan']=='Barang'){
            echo "<div class='alert alert-danger'>Stok Tidak Cukup</div>";
        }else{
            $plasma = $this->db->get_where('master_plasma',array('kode_plasma'=>$data['kode_plasma']));
            $hasil_plasma = $plasma->row();
            $data['nama_plasma'] = @$hasil_plasma->nama_plasma;

            $cek = $this->db->get_where('opsi_transaksi_input_plasma_temp',array('kode_transaksi'=>$data['kode_transaksi'],
                'kode_bahan'=>$data['kode_bahan'],'kode_plasma'=>$data['kode_plasma']));

            if ($data['kebutuhan']=='Kasbon') {
                $data['sub_total'] =$data['nominal'];
                $data['nama_bahan']='Kasbon';
            }


            if(count($cek->row())<1){
                unset($data['nominal']);
                $this->db->insert('opsi_transaksi_input_plasma_temp',$data);
            }else{
                $hasil_cek = $cek->row();

                if ($data['kebutuhan']=='Kasbon') {
                    $data['sub_total'] =$hasil_cek->sub_total + $data['nominal'];
                    $data['nama_bahan']='Kasbon';
                }else{
                 $data['jumlah'] = $hasil_cek->jumlah + $data['jumlah'];
                 $data['sub_total'] = $hasil_cek->sub_total + $data['sub_total'];
             }
             unset($data['nominal']);
             $this->db->update('opsi_transaksi_input_plasma_temp',$data,array('kode_transaksi'=>$data['kode_transaksi'],
                'kode_bahan'=>$data['kode_bahan']));
         }
     }


 }
 public function update_temp(){
    $data = $this->input->post();

    $nama = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$data['kode_bahan']));
    $hasil_nama = $nama->row();
    $data['nama_bahan'] = @$hasil_nama->nama_bahan_baku;
    if($data['jumlah'] > @$hasil_nama->real_stock and $data['kebutuhan']=='Barang'){
        echo "<div class='alert alert-danger'>Stok Tidak Cukup</div>";
    }else{
        $plasma = $this->db->get_where('master_plasma',array('kode_plasma'=>$data['kode_plasma']));
        $hasil_plasma = $plasma->row();

        if ($data['kebutuhan']=='Kasbon') {
            $data['sub_total'] =$data['nominal'];
            $data['nama_bahan']='Kasbon';
        }else{
            $data['jumlah'] = $data['jumlah'];
            $data['sub_total'] = $data['jumlah'] * $data['harga_satuan'];
        }
        $data['nama_plasma'] = $hasil_plasma->nama_plasma;
        unset($data['nominal']);
        $this->db->update('opsi_transaksi_input_plasma_temp',$data,array('id'=>$data['id']));
    }

}
public function get_temp_kebutuhan(){
    $id = $this->input->post('id');

    $cek_bahan = $this->db->get_where('opsi_transaksi_input_plasma_temp',array('id'=>$id));
    $hasil_bahan_temp = $cek_bahan->row();
    echo json_encode($hasil_bahan_temp);
}
public function get_table()
{

    $start = (100*$this->input->post('page'));
    $this->db->limit(100, $start);
    if($this->input->post('kategori')!=""){
        $kategori = $this->input->post('kategori');
        $this->db->where('kode_kategori_produk', $kategori);
    }

    if($this->input->post('tgl_awal')!="" || $this->input->post('tgl_akhir')!=""  ){
        $tgl_akhir = $this->input->post('tgl_akhir');
        $tgl_awal = $this->input->post('tgl_awal');
        $this->db->where('tanggal_transaksi >=', $tgl_awal);
        $this->db->where('tanggal_transaksi <=', $tgl_akhir);
    }

    if($this->input->post('nama_produk')!=""){
        $nama_produk = $this->input->post('nama_produk');
        $this->db->like('nama_bahan_baku', $nama_produk);
    }
    $this->db->order_by('tanggal_transaksi','DESC');
    $this->db->like('tanggal_transaksi',date('m'));
    $get_bb = $this->db->get("transaksi_input_plasma");
    $hasil_bb = $get_bb->result();
    $nomor = $start+1;
    foreach ($hasil_bb as $daftar) {
        ?>   
        <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo $daftar->kode_transaksi; ?></td>
            <td><?php echo TanggalIndo($daftar->tanggal_transaksi); ?></td>
            <td><?php echo $daftar->nama_petugas; ?></td>
            <td><?php echo get_detail($daftar->kode_transaksi); ?></td>

        </tr>

        <?php 
        $nomor++;
    }
}
public function cari_transaksi(){
    $this->load->view('setting/cari_transaksi');

}
public function hapus_bahan_temp(){
    $data = $this->input->post();
    $this->db->delete('opsi_transaksi_input_plasma_temp',array('id'=>$data['id']));
}
public function simpan_transaksi(){
    $data = $this->input->post();
    $user = $this->session->userdata('astrosession');
    $transaksi['kode_transaksi'] = $data['kode_transaksi'];
    $transaksi['tanggal_transaksi'] = $data['tanggal_input'];
    $transaksi['kode_plasma'] = $data['kode_plasma'];
    $nama = $this->db->get_where('master_plasma',array('kode_plasma'=>$data['kode_plasma']));
    $hasil_nama = $nama->row();
    $transaksi['nama_plasma'] = $hasil_nama->nama_plasma;
    $transaksi['kode_petugas'] = $user->id;
    $transaksi['nama_petugas'] = $user->uname;

    $this->db->insert('transaksi_input_plasma',$transaksi);

    $get_opsi = $this->db->get_where('opsi_transaksi_input_plasma_temp',array('kode_transaksi'=>$data['kode_transaksi']));
    $hasil_opsi = $get_opsi->result();
    foreach($hasil_opsi as $daftar){
        $opsi['kode_transaksi'] = $daftar->kode_transaksi;
        $opsi['kode_bahan'] = $daftar->kode_bahan;
        $opsi['nama_bahan'] = $daftar->nama_bahan;
        $opsi['kategori_bahan'] = $daftar->kategori_bahan;
        $opsi['jumlah'] =$daftar->jumlah;
        $opsi['harga_satuan'] =$daftar->harga_satuan;
        $opsi['sub_total'] =$daftar->sub_total;
        $opsi['keterangan'] = $daftar->keterangan;
        $this->db->insert('opsi_transaksi_input_plasma',$opsi);
        if($daftar->kategori_bahan=="setengah_jadi"){
            $bahan_plasma = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan,
                'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
            $hasil_bahan_plasma = $bahan_plasma->row();

            $plasma['real_stock'] = $hasil_bahan_plasma->real_stock - $daftar->jumlah;
            $this->db->update('master_bahan_setengah_jadi',$plasma,array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan,
                'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));

            $komposisi = $this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan));
            $hasil_komposisi = $komposisi->result();
            foreach($hasil_komposisi as $list_sj){

                $bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$list_sj->kode_bahan,'kode_plasma'=>$data['kode_plasma']));
                $hasil_bahan = $bahan->row();
                $update_sj['real_stock'] = $hasil_bahan->real_stock - ($list_sj->jumlah * $daftar->jumlah);
                $this->db->update('master_bahan_baku',$update_sj,array('kode_plasma'=>$data['kode_plasma'],'kode_bahan_baku'=>$list_sj->kode_bahan));

            }

            $bahan_sendiri = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan,
                'status'=>'sendiri'));
            $hasil_bahan_sendiri = $bahan_sendiri->row();

            $sendiri['real_stock'] = $hasil_bahan_sendiri->real_stock + $daftar->jumlah;
            $this->db->update('master_bahan_setengah_jadi',$sendiri,array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan,
                'status'=>'sendiri'));

        }else{
            $bahan_plasma = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$daftar->kode_bahan,
                'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));
            $hasil_bahan_plasma = $bahan_plasma->row();

            $plasma['real_stock'] = $hasil_bahan_plasma->real_stock - $daftar->jumlah;
            $this->db->update('master_bahan_jadi',$plasma,array('kode_bahan_jadi'=>$daftar->kode_bahan,
                'status'=>'plasma','kode_plasma'=>$data['kode_plasma']));

            $komposisi = $this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$daftar->kode_bahan));
            $hasil_komposisi = $komposisi->result();
            foreach($hasil_komposisi as $list){
                if($list->jenis_bahan=="bahan baku"){
                    $bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$list->kode_bahan,'kode_plasma'=>$data['kode_plasma']));
                    $hasil_bahan = $bahan->row();
                    $update['real_stock'] = $hasil_bahan->real_stock - ($list->jumlah * $daftar->jumlah);
                    $this->db->update('master_bahan_baku',$update,array('kode_bahan_baku'=>$list->kode_bahan,'kode_plasma'=>$data['kode_plasma']));
                }else if($list->jenis_bahan=="bahan setengah jadi"){
                    $bahan = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$list->kode_bahan,'kode_plasma'=>$data['kode_plasma']));
                    $hasil_bahan = $bahan->row();
                    $update['real_stock'] = $hasil_bahan->real_stock - ($list->jumlah * $daftar->jumlah);
                    $this->db->update('master_bahan_setengah_jadi',$update,array('kode_bahan_setengah_jadi'=>$list->kode_bahan,'kode_plasma'=>$data['kode_plasma']));
                }

            }

            $bahan_sendiri = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$daftar->kode_bahan,
                'status'=>'sendiri'));
            $hasil_bahan_sendiri = $bahan_sendiri->row();

            $sendiri['real_stock'] = $hasil_bahan_sendiri->real_stock + $daftar->jumlah;
            $this->db->update('master_bahan_jadi',$sendiri,array('kode_bahan_jadi'=>$daftar->kode_bahan,
                'status'=>'sendiri'));
        }

    }



        //P. Dion

    $get_putang=$this->db->get_where('transaksi_piutang',array('kode_customer'=>$data['kode_plasma']));
    $hasil_get_piutang=$get_putang->row();

    $angsuran_lama  = $hasil_get_piutang->angsuran;
    $sisa           = $hasil_get_piutang->sisa;

    $update_piutang['angsuran']=$angsuran_lama + $data['angsuran_plasma'];
    $update_piutang['sisa']=$sisa - $data['angsuran_plasma'];

    $this->db->update('transaksi_piutang',$update_piutang,array('kode_customer'=>$data['kode_plasma']));



    $opsi_piutang['kode_transaksi']=$data['kode_transaksi'];
    $opsi_piutang['angsuran']=$data['angsuran_plasma'];
    $opsi_piutang['tanggal_angsuran']=date('Y-m-d');
    $this->db->insert('opsi_piutang',$opsi_piutang);

    $get_kategori_akun=$this->db->get_where('keuangan_kategori_akun',array('nama_kategori_akun'=>'Pembayaran Plasma'));
    $hasil_get_kategori_akun=$get_kategori_akun->row();

    $keungan_keluar['kode_jenis_keuangan']=  $hasil_get_kategori_akun->kode_jenis_akun;
    $keungan_keluar['nama_jenis_keuangan']=$hasil_get_kategori_akun->nama_jenis_akun;
    $keungan_keluar['kode_kategori_keuangan']=$hasil_get_kategori_akun->kode_kategori_akun;
    $keungan_keluar['nama_kategori_keuangan']=$hasil_get_kategori_akun->nama_kategori_akun;
    $keungan_keluar['kode_sub_kategori_keuangan']='';
    $keungan_keluar['nama_sub_kategori_keuangan']=$hasil_get_kategori_akun->nama_kategori_akun;;

    $kode_transaksi_plasma=$data['kode_transaksi'];
        //echo $data['kode_transaksi'];
    $get_total_plasma=$this->db->query("SELECT sum(sub_total) as total from opsi_transaksi_input_plasma_temp where kode_transaksi='$kode_transaksi_plasma'");
    $hasil_get_total_plasma=$get_total_plasma->row();
        //echo $this->db->last_query();
        //echo $data['angsuran_plasma']."<br>";
        //echo $hasil_get_total_plasma->total."<br>";

    $angsuran_plasma=$data['angsuran_plasma'];
    $total= $hasil_get_total_plasma->total;
        // echo "angsuran".$data['angsuran_plasma']."<br>";
        // echo "total".$total."<br>";
    $hasil=$total-$angsuran_plasma;
        //echo $hasil;
    $keungan_keluar['nominal']=$hasil;
    $keungan_keluar['keterangan']='';
    $keungan_keluar['tanggal_transaksi']=date('Y-m-d');
    $keungan_keluar['kode_referensi']='';

    $user=$this->session->userdata('astrosession');

    $keungan_keluar['id_petugas']=$user->id;
    $keungan_keluar['petugas']=$user->uname;
    $this->db->insert('keuangan_keluar',$keungan_keluar);
         //echo $this->db->last_query();
    $this->db->delete('opsi_transaksi_input_plasma_temp',array('kode_transaksi'=>$data['kode_transaksi']));
}
public function simpan_transaksi_baru(){
 $data = $this->input->post();
 $user = $this->session->userdata('astrosession');


 $user = $this->session->userdata('astrosession');
 $id_petugas = $user->id;
 $nama_petugas = $user->uname;

 $get_opsi = $this->db->get_where('opsi_transaksi_input_plasma_temp',array('kode_transaksi'=>$data['kode_transaksi']));
 $hasil_opsi = $get_opsi->result();
 foreach($hasil_opsi as $daftar){
    $kode_bahan=$daftar->kode_bahan;
    $jumlah=$daftar->jumlah;
    $bahan_baku_lain=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'status'=>'sendiri'));
    $hasil_bahan_baku_lain=$bahan_baku_lain->row();

    $real_stock_barang_lain=$hasil_bahan_baku_lain->real_stock - $jumlah;
    if($real_stock_barang_lain < 0){
        $jml_sisa='kurang';
    }else{
        $jml_sisa='cukup';
    }

    if($jml_sisa=='cukup'){

        $opsi['kode_transaksi'] = $daftar->kode_transaksi;
        $opsi['kode_bahan'] = $daftar->kode_bahan;
        $opsi['nama_bahan'] = $daftar->nama_bahan;
        $opsi['kategori_bahan'] = $daftar->kategori_bahan;
        $opsi['jumlah'] =$daftar->jumlah;
        $opsi['harga_satuan'] =$daftar->harga_satuan;
        $opsi['sub_total'] =$daftar->sub_total;
        $opsi['keterangan'] = $daftar->keterangan;
        $opsi['kode_plasma'] =$daftar->kode_plasma;
        $opsi['nama_plasma'] = $daftar->nama_plasma;
        $opsi['kebutuhan'] = $daftar->kebutuhan;
        $this->db->insert('opsi_transaksi_input_plasma',$opsi);

        $real_stock['real_stock']=$real_stock_barang_lain;
        $update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$kode_bahan,'status'=>'sendiri'));


        $transaksi_stok['jenis_transaksi']='kebutuhan plasma';
        $transaksi_stok['kode_transaksi']=$data['kode_transaksi'];
            //$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
        $transaksi_stok['hpp'] =$daftar->harga_satuan;
        $transaksi_stok['kode_bahan']=$hasil_bahan_baku_lain->kode_bahan_baku;
        $transaksi_stok['nama_bahan']=$hasil_bahan_baku_lain->nama_bahan_baku;
        $transaksi_stok['stok_keluar']=$jumlah;
        $transaksi_stok['tanggal_transaksi']=date('Y-m-d');
        $transaksi_stok['id_petugas']=$id_petugas;
        $transaksi_stok['nama_petugas']=$nama_petugas;
        $insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);
        if($daftar->kebutuhan=='Kasbon'){
            $nominal_piutang=$daftar->sub_total;
        }elseif($daftar->kebutuhan=='Barang'){
            $nominal_piutang=$jumlah * $daftar->harga_satuan;
        }
        
        $piutang_plasma=$this->db->get_where('transaksi_piutang',array('kode_customer'=>$daftar->kode_plasma,'status'=>'plasma'));
        $hasil_piutang_plasma=$piutang_plasma->num_rows();
        $jml_piutang_plasma=$piutang_plasma->row();
        if($hasil_piutang_plasma > 0){

            $pitang_update['nominal_piutang']=$jml_piutang_plasma->nominal_piutang + @$nominal_piutang;
            $pitang_update['sisa']=$jml_piutang_plasma->sisa + @$nominal_piutang;
            $this->db->update('transaksi_piutang',$pitang_update,array('kode_customer'=>$daftar->kode_plasma,'status'=>'plasma'));
        }else{
            $pitang['kode_transaksi']=$data['kode_transaksi'];
            $pitang['petugas']=$nama_petugas;
            $pitang['kode_customer']=$daftar->kode_plasma;
            $pitang['nama_customer']=$daftar->nama_plasma;
            $pitang['nominal_piutang']=@$nominal_piutang;
            $pitang['sisa']=@$nominal_piutang;
            $pitang['status']='plasma';
            $pitang['tanggal_transaksi']=date('Y-m-d');
            $this->db->insert('transaksi_piutang',$pitang); 
        }

    }else{
        echo"<div class='alert alert-danger'>Stok Barang Lain Tidak Mencukupi</div>";
    }
}
if(@$jml_sisa=='cukup'){
    $transaksi['kode_transaksi'] = $data['kode_transaksi'];
    $transaksi['tanggal_transaksi'] = $data['tanggal_input'];
    // $transaksi['kode_plasma'] = $data['kode_plasma'];
    // $nama = $this->db->get_where('master_plasma',array('kode_plasma'=>$data['kode_plasma']));
    // $hasil_nama = $nama->row();
    // $transaksi['nama_plasma'] = $hasil_nama->nama_plasma;
    $transaksi['kode_petugas'] = $user->id;
    $transaksi['nama_petugas'] = $user->uname;

    $this->db->insert('transaksi_input_plasma',$transaksi);
    $this->db->delete('opsi_transaksi_input_plasma_temp',array('kode_transaksi'=>$data['kode_transaksi']));
}
}
}
