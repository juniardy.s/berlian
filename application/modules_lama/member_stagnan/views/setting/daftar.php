<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Member Stagnan (1 Minggu Tidak Order)
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>

      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
        <?php
        $get_member = $this->db->get_where('master_member',array('status_member '=>1));
        $hasil_member = $get_member->result();
        ?>
        <div class="box-body">
          <div class="col-md-1 pull-right">
           <button style="" type="button" class="btn btn-info pull-right" id="cetak"><i class="fa fa-print"></i> Print</button>
         </div>          
         <div class="sukses" ></div>
         <div id="cari_transaksi">
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
                <th width="50px;">No</th>
                <th>Kode Member</th>
                <th>Nama Member</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;

              foreach($hasil_member as $item){ 

                $tgl1 = date('Y-m-d');
                $tgl2 = date('Y-m-d', strtotime('-7 days', strtotime($tgl1))); 
                $this->db->where('tanggal_penjualan >=',$tgl2);
                $this->db->where('tanggal_penjualan <=',$tgl1);
                $this->db->where('kode_member',$item->kode_member);

                $get_penjualan = $this->db->get('transaksi_penjualan');
                $hasil_penjualan = $get_penjualan->row();

                if(count($hasil_penjualan) == 0){
                  ?> 
                  <td><?php echo $no; ?></td> 
                  <td><?php echo $item->kode_member; ?></td>
                  <td><?php echo $item->nama_member; ?></td>                  
                </tr>
                <?php 
                $no++;
              }  }?>
            </tbody>                
          </table>
        </div>
      </div>
    </div>
  </div>
</div><!-- /.col -->
</div>

</div>    
</div>  
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<script type="text/javascript">
  $("#cetak").click(function(){
    window.open("<?php echo base_url().'member_stagnan/cetak_member_stagnan/'; ?>");

  })
  $('.tgl').Zebra_DatePicker({});
  $('#cari').click(function(){
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    var kode_unit =$("#kode_unit").val();
    if (tgl_awal=='' || tgl_akhir==''){ 
      alert('Masukan Tanggal Awal & Tanggal Akhir..!')
    }
    else{
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'pemasukan/cari_pemasukan'; ?>",  
        cache :false,

        data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
         $(".tunggu").hide();  
         $("#cari_transaksi").html(data);
       },  
       error : function(data) {  
         // alert("das");  
       }  
     });
    }

    $('#tgl_awal').val('');
    $('#tgl_akhir').val('');

  });
</script>

<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'analisa/menu_data'; ?>";
  });
</script>


<script>
  $(document).ready(function() {
    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": false,
      "info":     false
    });
    setTimeout(function(){
      $("#warna").fadeIn('slow');
    }, 1000);

    $('#tabel_daftar').dataTable();
  } );
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);

</script>

