
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content"> 
        <?php
        $kode_karyawan = $this->uri->segment(3);
        $this->db->where('kode_petugas', $kode_karyawan);
        $get_gaji = $this->db->get('transaksi_withdraw');
        $hasil_penggajian = $get_gaji->row();
        ?>           
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Detail Pengambilan Petugas <?php echo $hasil_penggajian->nama_petugas ?>
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body">            
                  <div class="sukses" ></div>
                                    <div class="row" >
                    <form id="form_withdraw">
                      <div class="form-group  col-xs-6">
                        <label class="gedhi">Kode</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_penggajian->kode_petugas;?>" name="kode_karyawan" id="kode_karyawan"/>
                      </div>

                      <div class="form-group  col-xs-6">
                        <label class="gedhi">Nama</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_penggajian->nama_petugas; ?>" name="nama_karyawan" id="nama_karyawan"/>
                      </div>
                      <div class="form-group  col-xs-3">
                        <label class="gedhi">Total withdraw</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($hasil_penggajian->total_withdraw) ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                      <div class="form-group  col-xs-3">
                        <label class="gedhi">Total Pengambilan</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($hasil_penggajian->pengambilan) ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                       <div class="form-group  col-xs-6">
                        <label class="gedhi">Sisa</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($hasil_penggajian->sisa) ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                    </form>
                  </div>
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <?php
                    $this->db->where('kode_petugas', $kode_karyawan);
                    $penggajian = $this->db->get('opsi_transaksi_withdraw');
                    $hasil_penggajian = $penggajian->result();
                    ?>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Nominal Diambil</th>
                        <th>Sisa</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $nomor = 1;
                      foreach($hasil_penggajian as $daftar){
                        ?>
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo TanggalIndo($daftar->tanggal_transaksi); ?></td>
                          <td><?php echo $daftar->nama_petugas; ?></td>
                          <td><?php echo format_rupiah($daftar->pengambilan); ?></td>
                          <td><?php echo format_rupiah($daftar->sisa); ?></td>
                        </tr>
                        <?php $nomor++; 
                      } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama Karyawan</th>
                        <th>Nominal Diambil</th>
                        <th>Sisa</th>
                      </tr>
                    </tfoot>
                  </table>


                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'withdraw/'; ?>";
      });
    </script>
    <script>
      $(document).ready(function(){
        $("#tabel_daftar").dataTable();
      })

    </script>