
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content"> 

        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Detail Penggajian Karyawan Tetap
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>

                <?php 
                $kode_karyawan=$this->uri->segment(3);
                $get_karyawan=$this->db->get_where('transaksi_penggajian',array('kode_karyawan'=>$kode_karyawan));
                $hasil=$get_karyawan->row();
                ?>
                <div class="box-body ">            
                  <div class="sukses" ></div>
                  <div class="row" >
                    <form id="form">
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Nama Karyawan</label>
                        <select readonly class="form-control" id="kode_karyawan" name="kode_karyawan">
                          <option value=""><?php echo $hasil->nama_karyawan ?></option>
                        </select>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Total Gaji</label>
                        <input readonly  type="text" class="form-control" value="<?php echo $hasil->total_gaji ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Tanggal</label>
                        <input readonly type="text" class="form-control" value="<?php echo tanggalIndo($hasil->tanggal) ?>" name="tanggal" id="tanggal"/>
                      </div>
                      <div class="form-group  col-xs-12">
                      <a class="btn btn-primary pull-right" href="<?php echo base_url().'penggajian/tetap' ?>">OK</a>
                      </div>
                    </form>
                  </div>
                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'penggajian/tetap'; ?>";
      });
    </script>

    <script>
      $("#kode_karyawan").change(function(){
       var url = "<?php echo base_url().'penggajian/get_gaji'; ?>";
       var kode_karyawan = $("#kode_karyawan").val();
       $.ajax( {
         type:"POST", 
         url : url,  
         cache :false,
         data :{kode_karyawan:kode_karyawan},
         dataType : 'json',
         success : function(data) {
           $("#total_gaji").val(data.gaji_tetap);
         },  
         error : function(data) {  
          alert(data);  
        }  
      });
     })




      $("#form").submit(function(){
        $.ajax({
          type: "POST",
          url: '<?php echo base_url().'penggajian/simpan_gaji_tetap'; ?>',
          data: $(this).serialize(),
          beforeSend:function(){
          },
          success: function(msg) {
            sukses = '<div class="alert alert-success">Sudah tersimpan.</div>';
            gagal = '<div class="alert alert-danger">Periksa Nominal Withdraw</div>';
            if(msg.length=='7'){
              $(".sukses").html(gagal);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');},1500);
            }else{
              $(".sukses").html(sukses);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'penggajian/tetap' ?>";},1500);
            }
          }
        });
        return false;
      });

      $(document).ready(function(){
        $("#tabel_daftar").dataTable();
      })

    </script>