
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
      .ombo{
        width: 400px;
      } 

    </style>    
    <!-- Main content -->
    <section class="content"> 
      <?php
      $kode_karyawan=$this->uri->segment(3);
      $this->db->where('kode_petugas', $kode_karyawan);
      $get_penggajian = $this->db->get('transaksi_withdraw');
      $hasil_penggajian = $get_penggajian->row();

      ?>           
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                Form Withdraw <?php echo $hasil_penggajian->nama_petugas ?>
              </div>
              <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
                <a href="javascript:;" class="reload">
                </a>

              </div>
            </div>
            <div class="portlet-body">
              <!------------------------------------------------------------------------------------------------------>


              <div class="box-body ">            
                <div class="sukses" ></div>
                <div class="row" >
                  <form id="form_withdraw">
                    <div class="form-group  col-xs-6">
                      <label class="gedhi">Kode</label>
                      <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_penggajian->kode_petugas;?>" name="kode_karyawan" id="kode_karyawan"/>
                    </div>

                    <div class="form-group  col-xs-6">
                      <label class="gedhi">Nama</label>
                      <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_penggajian->nama_petugas; ?>" name="nama_karyawan" id="nama_karyawan"/>
                    </div>
                     <!--  <div class="form-group  col-xs-2">
                        <label class="gedhi">Total withdraw</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($hasil_penggajian->total_withdraw) ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                      <div class="form-group  col-xs-2">
                        <label class="gedhi">Total Pengambilan</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($hasil_penggajian->pengambilan) ?>" name="total_gaji" id="total_gaji"/>
                      </div> -->
                      <div class="form-group  col-xs-6">
                        <label class="gedhi">Sisa</label>
                        <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($hasil_penggajian->sisa) ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                      <div class="form-group  col-xs-6">
                        <label class="gedhi">Nominal Withdraw</label>
                        <input type="text" class="form-control" value="" name="angsuran_gaji" id="angsuran_gaji"/>
                        <div id="nominal_withdraw">
                        </div>
                      </div>

                      <div class="form-group  col-xs-12">
                        <br><br>
                        <!--  <button type="submit" class="btn btn-primary pull-right">Simpan</button> -->
                        <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
                      </div>
                    </form>
                  </div>
                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'withdraw/'; ?>";
    });
  </script>

  <script>

    function actDelete(Object) {
      $('#id-delete').val(Object);
      $('#modal-confirm').modal('show');
    }

    $("#angsuran_gaji").keyup(function(){
      $.ajax({
        type: "POST",
        url: '<?php echo base_url().'withdraw/get_rupiah'; ?>',
        data: {
          value: $(this).val()
        },
        success: function(msg) {
          var withdraw = $('#angsuran_gaji').val();
          var sisa = $('#total_gaji').val();
          if(withdraw < 0)
          {
            alert('Nominal Withdraw Tidak Boleh Kurang Dari 0');
            $('#angsuran_gaji').val('');
          }else if(withdraw > <?php echo $hasil_penggajian->sisa ?>){
             alert('Nominal Withdraw Tidak Boleh Lebih Dari Sisa');
            $('#angsuran_gaji').val('');
          }else{
            $('#nominal_withdraw').html('<h2 class="alert alert-danger"><b>'+msg+'</b></h2>');
          }
        }
      });
      return false;
    });

    $("#form_withdraw").submit(function(){
      $.ajax({
        type: "POST",
        url: '<?php echo base_url().'withdraw/simpan_withdraw'; ?>',
        data: $(this).serialize(),
        beforeSend:function(){
        },
        success: function(msg) {
          sukses = '<div class="alert alert-success">Sudah tersimpan.</div>';
          gagal = '<div class="alert alert-danger">Periksa Nominal Withdraw</div>';
          if(msg.length=='7'){
            $(".sukses").html(gagal);
            $(".tunggu").hide();
            setTimeout(function(){$('.sukses').html('');},1500);
          }else{
            $(".sukses").html(sukses);
            $(".tunggu").hide();
            setTimeout(function(){$('.sukses').html('');
              window.location = "<?php echo base_url() . 'withdraw/' ?>";},1500);
          }
        }
      });
      return false;
    });

    // $("#angsuran_gaji").keyup(function(){
    //   var withdraw = $('#angsuran_gaji').val();
    //   if(withdraw < 0)
    //   {
    //     alert('Nominal Withdraw Tidak Boleh Kurang Dari 0');
    //     $('#angsuran_gaji').val('');
    //     $('#nominal_withdraw').html();
    //   }
    // });

    $(document).ready(function(){
      $("#tabel_daftar").dataTable();
    })

  </script>