<?php 
$tgl_awal=$this->input->post('tgl_awal');
$tgl_akhir=$this->input->post('tgl_akhir');

?>

<div class="col-md-12">

</div>
<div class="col-md-12" id="container" >

</div>

<!------------------------------------------------------------------------------------------------------>

<script type="text/javascript">

  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Grafik Top Produk'
    },
    subtitle: {
      text: '<?php echo @TanggalIndo($tgl_awal)." "."-"." ".@TanggalIndo($tgl_akhir)  ?>'
    },
    xAxis: {
     categories: [

     <?php 

     if (!empty($tgl_awal) && !empty($tgl_akhir)) {
      $this->db->where('tanggal_transaksi >=', $tgl_awal);
      $this->db->where('tanggal_transaksi <=', $tgl_akhir);
      $this->db->group_by('kode_menu');
      $this->db->order_by('jumlah','desc');
      $this->db->limit(5,0);
      $nama_produk = $this->db->get_where('opsi_transaksi_penjualan');
    } else {
      $this->db->group_by('kode_menu');
      $this->db->order_by('jumlah','desc');
      $this->db->limit(5,0);
      $nama_produk = $this->db->get_where('opsi_transaksi_penjualan');
    }

    $get_nama_produk =$nama_produk->result();
    foreach($get_nama_produk as $item){
      echo "'$item->nama_menu',";
    } ?>

    ]
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Nominal '
    }
  },
  plotOptions: {
    column: {
      pointPadding: 0,
      borderWidth: 0
    }
  },
  series: [{
    name: ' ',
    data: [

    <?php
    foreach($get_nama_produk as $item){
      $this->db->select_sum('jumlah');
      if(@$data['tgl_awal'] && @$data['tgl_akhir']){

        $tgl_awal = $data['tgl_awal'];
        $tgl_akhir = $data['tgl_akhir'];
        $this->db->where('tanggal_transaksi >=', $tgl_awal);
        $this->db->where('tanggal_transaksi <=', $tgl_akhir);
      }
      $jumlah = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu'=>$item->kode_menu));
      $hasil_jumlah = $jumlah->row();
      echo $hasil_jumlah->jumlah.",";
    }
    ?>
    ],

  }]
});


</script>