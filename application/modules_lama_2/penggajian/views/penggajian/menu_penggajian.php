
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    
                        <!-- END STYLE CUSTOMIZER -->
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                          Penggajian</h3>
                          <div class="page-bar">
                            <ul class="page-breadcrumb">
                              <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Home</a>
                                <i class="fa fa-angle-right"></i>
                              </li>
                              <li>
                                <a href="#">Penggajian</a>
                              </li>
                            </ul>
                            <div class="page-toolbar">
                              <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>
                                &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light blue-soft" id="withdraw">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-tasks" ></i>
                                </div>
                                <div class="details" >
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Withdraw
                                  </div>
                                </div>
                              </a>
                            </div>
                           
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light green-soft"  id="penggajian">
                                <div class="visual">
                                  <i class="fa fa-money"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Penggajian
                                  </div>
                                </div>
                              </a>
                            </div>
                             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light red-soft"  id="kasbon">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-retweet"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Kasbon
                                  </div>
                                </div>
                              </a>
                            </div>
                             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light purple-soft"  id="komisi">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-usd"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Komisi
                                  </div>
                                </div>
                              </a>
                            </div>
                            
                            </div>
                          <!-- END DASHBOARD STATS -->
                          <div class="clearfix">
                          </div>

                        </div>
                        <script type="text/javascript">
                          $(document).ready(function(){


                            $("#withdraw").click(function(){
                              window.location = "<?php echo base_url().'withdraw/' ?>";
                            });

                            $("#penggajian").click(function(){
                              window.location = "<?php echo base_url() . 'penggajian/daftar' ?>";
                            });

                            $("#kasbon").click(function(){
                              window.location = "<?php echo base_url() . 'penggajian/kasbon' ?>";
                            });

                            $("#komisi").click(function(){
                              window.location = "<?php echo base_url() . 'komisi/' ?>";
                            });

                           

                          });
                        </script>
