
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content"> 
        <?php
        $kode_karyawan = $this->uri->segment(3);
        $this->db->where('kode_karyawan', $kode_karyawan);
        $get_gaji = $this->db->get('master_karyawan');
        $hasil_gaji = $get_gaji->row();
        ?>           
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Detail Pembayaran Gaji Karyawan <?php echo $hasil_gaji->nama_karyawan ?>
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body">            
                  <div class="sukses" ></div>
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <?php
                    $this->db->where('kode_karyawan', $kode_karyawan);
                    $penggajian = $this->db->get('transaksi_penggajian');
                    $hasil_penggajian = $penggajian->result();
                    ?>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama Karyawan</th>
                        <th>Nominal Diambil</th>
                        <th>Sisa</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $nomor = 1;

                      foreach($hasil_penggajian as $daftar){
                        ?>
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo TanggalIndo($daftar->tanggal); ?></td>
                          <td><?php echo $daftar->nama_karyawan; ?></td>
                          <td><?php echo format_rupiah($daftar->angsuran_gaji); ?></td>
                          <td><?php echo format_rupiah($daftar->sisa_gaji); ?></td>
                        </tr>
                        <?php $nomor++; 
                      } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama Karyawan</th>
                        <th>Nominal Diambil</th>
                        <th>Sisa</th>
                      </tr>
                    </tfoot>
                  </table>


                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'penggajian/'; ?>";
      });
    </script>
    <script>
      $(document).ready(function(){
        $("#tabel_daftar").dataTable();
      })

    </script>