
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content"> 

        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Detail Kasbon
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>

                <?php 
                $kode_karyawan=$this->uri->segment(4);
                $get_karyawan=$this->db->get_where('transaksi_kasbon',array('kode_karyawan'=>$kode_karyawan));
                $hasil=$get_karyawan->row();
                ?>
                <div class="box-body ">            
                  <div class="sukses" ></div>
                  <div class="row" >
                    <form id="form">
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Nama Karyawan</label>
                        <select readonly class="form-control" id="kode_karyawan" name="kode_karyawan">
                          <option value=""><?php echo $hasil->nama_karyawan ?></option>
                        </select>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Total Kasbon</label>
                        <input readonly  type="text" class="form-control" value="<?php echo @format_rupiah($hasil->total_kasbon) ?>" name="total_kasbon" id="total_kasbon"/>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Tanggal</label>
                        <input readonly type="text" class="form-control" value="<?php echo @tanggalIndo($hasil->tanggal) ?>" name="tanggal" id="tanggal"/>
                      </div>
                      
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Sisa Kasbon</label>
                        <input readonly  type="text" class="form-control" value="<?php echo @format_rupiah($hasil->sisa_kasbon) ?>" name="total_kasbon" id="total_kasbon"/>
                      </div>
                    </form>
                  </div>
                </div>

              <div id="list_transaksi_angsuran">
                <div class="box-body">
                  
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Angsuran</th>
                        <th>Angsuran</th>
                       
                      </tr>
                    </thead>
                    <tbody >

                      <?php
                      
                      $opsi_piutang = $this->db->get_where('opsi_transaksi_kasbon',array('kode_karyawan'=>$kode_karyawan));
                      $list_opsi_piutang = $opsi_piutang->result();
                      $no = 1;  

                      foreach($list_opsi_piutang as $value){ 
                        ?> 
                        <tr>
                          <td><?php echo $no; ?></td>
                          <td><?php echo @TanggalIndo($value->tanggal); ?></td>
                         
                          <td><?php echo @format_rupiah($value->angsuran_kasbon); ?></td>
                          
                        </tr>
                        <?php 
                        
                        $no++; 
                      } 
                      ?>

                     
                    </tbody>
                    <tfoot>

                    </tfoot>
                  </table>
                  <br><br>
                   <div class="row" >
                      <div class="form-group  col-xs-12">
                      <label>Keterangan :</label>
                      <h1><?php if($hasil->total_kasbon==$hasil->angsuran_kasbon){ ?>
                          <label class="label label-info">Lunas</label>
                            <?php }else{
                              ?>
                              <label class="label label-warning">Belum Lunas</label>
                              <?php
                              }?></h1>
                      <a class="btn btn-primary pull-right" href="<?php echo base_url().'penggajian/kasbon/' ?>">OK</a>
                      </div>
                    </div>
                </div>
              </div>
                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'penggajian/kasbon/'; ?>";
      });
    </script>

    <script>
      $("#kode_karyawan").change(function(){
       var url = "<?php echo base_url().'penggajian/get_gaji'; ?>";
       var kode_karyawan = $("#kode_karyawan").val();
       $.ajax( {
         type:"POST", 
         url : url,  
         cache :false,
         data :{kode_karyawan:kode_karyawan},
         dataType : 'json',
         success : function(data) {
           $("#total_gaji").val(data.gaji_tetap);
         },  
         error : function(data) {  
          alert(data);  
        }  
      });
     })




      $("#form").submit(function(){
        $.ajax({
          type: "POST",
          url: '<?php echo base_url().'penggajian/simpan_gaji_tetap'; ?>',
          data: $(this).serialize(),
          beforeSend:function(){
          },
          success: function(msg) {
            sukses = '<div class="alert alert-success">Sudah tersimpan.</div>';
            gagal = '<div class="alert alert-danger">Periksa Nominal Withdraw</div>';
            if(msg.length=='7'){
              $(".sukses").html(gagal);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');},1500);
            }else{
              $(".sukses").html(sukses);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'penggajian/tetap' ?>";},1500);
            }
          }
        });
        return false;
      });

      $(document).ready(function(){
        $("#tabel_daftar").dataTable();
      })

    </script>