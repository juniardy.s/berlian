<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kasbon extends CI_Controller {
	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

	public function index()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/kasbon/daftar_kasbon', $data, TRUE);
		$this->load->view('main', $data);	
	}

	

	public function tambah()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/kasbon/tambah_kasbon', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function daftar()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/kasbon/daftar_kasbon', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function detail()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/kasbon/detail_kasbon', $data, TRUE);
		$this->load->view('main', $data);	
	}

	
	public function get_rupiah()
	{
		$nominal = format_rupiah($this->input->post('value'));
		echo $nominal;
	}

	public function get_gaji()
	{
		$data = $this->input->post();
		$karyawan = $this->db->get_where('master_karyawan', array('kode_karyawan' =>$data['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();
		echo json_encode($hasil_karyawan);

	}

	public function simpan_kasbon()
	{
		$input = $this->input->post();
		$user = $this->session->userdata('astrosession');

		$data['kode_karyawan']=$input['kode_karyawan'];
		$karyawan = $this->db->get_where('master_karyawan', array('kode_karyawan' =>$input['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();

		$cek_kasbon=$this->db->get_where('transaksi_kasbon', array('kode_karyawan' =>$input['kode_karyawan']));
		$hasil_kasbon=$cek_kasbon->row();
		if(count($hasil_kasbon) > 0){
			$update['total_kasbon']=@$hasil_kasbon->total_kasbon + $input['total_kasbon'];
			$update['sisa_kasbon']=@$hasil_kasbon->sisa_kasbon + $input['total_kasbon'];
			$update['tanggal']=$input['tanggal'];
			$this->db->update('transaksi_kasbon',$update,array('kode_karyawan' =>$input['kode_karyawan']));
		}else{
			$data['nama_karyawan']=$hasil_karyawan->nama_karyawan;
			$data['total_kasbon']=$input['total_kasbon'];
			$data['sisa_kasbon']=$input['total_kasbon'];
			$data['tanggal']=$input['tanggal'];
			$data['jenis_karyawan']= $hasil_karyawan->jenis_karyawan;
			$this->db->insert('transaksi_kasbon', $data);
		}
		
		//echo $this->db->last_query();
		$keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.7.1'));
		$hasil_keuangan = $keuangan->row();

		$keluar['kode_jenis_keuangan'] = $hasil_keuangan->kode_jenis_akun;
		$keluar['nama_jenis_keuangan'] = $hasil_keuangan->nama_jenis_akun;
		$keluar['kode_kategori_keuangan'] = $hasil_keuangan->kode_kategori_akun;
		$keluar['nama_kategori_keuangan'] = $hasil_keuangan->nama_kategori_akun;
		$keluar['kode_sub_kategori_keuangan'] = $hasil_keuangan->kode_sub_kategori_akun;
		$keluar['nama_sub_kategori_keuangan'] = $hasil_keuangan->nama_sub_kategori_akun;
		$keluar['nominal'] = $input['total_kasbon'];
		$keluar['keterangan'] = "Kasbon Gaji"." ".$hasil_karyawan->nama_karyawan;
		$keluar['tanggal_transaksi'] = date("Y-m-d");
		$keluar['id_petugas'] =$user->id;
		$keluar['petugas'] = $user->uname;
		$this->db->insert('keuangan_keluar',$keluar);
		echo $this->db->last_query();


		echo 'berhasil';
		
	}

	
}

/* End of file penggajian.php */
/* Location: ./application/modules/penggajian/controllers/penggajian.php */