
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
   
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Stok Plasma 
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="col-md-4">
            <div class="form-group">
                
                <select class="form-control" id="bahan">
                    <option value="" selected="true">--Pilih Bahan--</option>
                    <option value="baku">Bahan Baku</option>
                    <option value="setengah">Bahan Setengah Jadi</option>
                    <!--<option value="jadi">Bahan Jadi</option>-->
                </select>
            </div>
          </div>
          <div class="col-md-4">
            <a onclick="cari_stok()" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>
          </div>
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
               <th>No.</th>
              <th>Kode Bahan</th>
              <th>Nama Bahan</th>
              <th align="right">Real Stok</th>
            </tr>
            </thead>
            
            <tbody id="stok">
            
            </tbody>
                          
        </table>


      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
</div>    
</div>  
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
            window.location = "<?php echo base_url().'stok/daftar_plasma'; ?>";
          });
        </script>

<script>
  $(document).ready(function() {
    
  } );
    $("#gudang").click(function(){
      $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/gudang' ?>";

                            });

                            $("#bar").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/bar' ?>";
                            });

                            $("#kitchen").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/kitchen' ?>";
                            });

                            $("#serve").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/server' ?>";
                            });

    setTimeout(function(){
      $("#warna").fadeIn('slow');
    }, 1000);
    $("a#hapus").click( function() {    
      var r =confirm("Anda yakin ingin menghapus data ini ?");
      if (r==true)  
      {
        $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'master/barang/hapus' ?>",  
          cache :false,  
          data :({key:$(this).attr('key')}),
          beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
            $(".sukses").html(data);   
            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/barang/daftar' ?>";},1500);              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
        return false;
      }
      else {}        
    });

                           
  
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);


function cari_stok(){
    var jenis_bahan = $("#bahan").val();
    var kode_plasma = "<?php echo $this->uri->segment(3); ?>";
    $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'stok/get_stok_plasma' ?>",  
          cache :false,  
          data :{jenis_bahan:jenis_bahan,kode_plasma:kode_plasma},
          beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
            $("#stok").html(data);   
            $(".tunggu").hide();              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
    
}
</script>

