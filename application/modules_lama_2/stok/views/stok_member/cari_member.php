<table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                <?php
                $param=$this->input->post('kode_customer');
                $this->db->group_by('nama_customer');
                $hutang = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$param,'kategori_member'=>'konsinyasi'));
                $hasil_hutang = $hutang->result();
                ?>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Nominal</th>
                    <th width="200px" >Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $nomor = 1;
                  foreach($hasil_hutang as $daftar){ 
                    ?> 
                    <tr>
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo @$daftar->nama_customer; ?></td>

                      <?php
                      $get_nominal_hutang=$this->db->query("SELECT sum(nominal_piutang) as nominal,sum(sisa) as sisa from transaksi_piutang where kode_customer='$daftar->kode_customer' and kategori_member='konsinyasi'");
                      $hasil=$get_nominal_hutang->row();
                      ?>
                      <td><?php echo format_rupiah(@$hasil->sisa); ?></td>
                      <td align="center" width="125px">
                        <a class="btn btn-primary" href="<?php echo base_url().'stok/detail_stok_member/'.$daftar->kode_customer ?>"><i class="fa fa-pencil"></i> Detail</a>
                        <!--    -->
                      </td>
                    </tr>
                    <?php $nomor++; } ?>

                  </tbody>
                  <tfoot>
                   <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Nominal</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>