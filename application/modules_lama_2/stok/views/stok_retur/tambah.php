<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah Retur
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row">
            <div class="col-md-12">


              <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                  <div class="box box-info">                 
                    <div class="box-body">           

                      <form id="data_form" action="" method="post">

                        <div class="box-body">
                          <div class="callout callout-info">

                          </div>


                          <?php
                          $tgl = date("Y-m-d");
                          $no_belakang = 0;
                          $user = $this->session->userdata('astrosession');
                          $id_user=$user->id;

                          $this->db->select_max('id');
                          $get_max_mut = $this->db->get('transaksi_mutasi');
                          $max_mut = $get_max_mut->row();

                          $this->db->where('id', $max_mut->id);
                          $get_mut = $this->db->get('transaksi_mutasi');
                          $mut = $get_mut->row();
                          
                          $tahun = substr(@$mut->kode_mutasi, 4,4);
                          
                          if(date('Y')==$tahun){
                            $nomor = substr(@$mut->kode_mutasi, 12);
                            
                            $nomor = $nomor + 1;
                            $string = strlen($nomor);
                            if($string == 1){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                            } else if($string == 2){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                            } else if($string == 3){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_000'.$nomor;
                            } else if($string == 4){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_00'.$nomor;
                            } else if($string == 5){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_0'.$nomor;
                            } else if($string == 6){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_'.$nomor;
                            }
                          } else {
                            $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_000001';
                          }
                          ?>
                          <input type="hidden" value="<?php echo @$kode_trans; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_mutasi" id="kode_mutasi" readonly/>
                          <input type="hidden" value="Petugas" class="form-control" placeholder="Kode Transaksi" name="posisi_awal" id="posisi_awal"/>                           
                          <input type="hidden" value="Petugas" class="form-control" placeholder="Kode Transaksi" name="kode_surat_jalan" id="kode_surat_jalan"/>                           
                          <div class="box-body" >

                            <br>
                          </div> 
                          <div class="row">
                            <!-- <div class="col-md-2">
                              <h3>Posisi Asal</h3>
                            </div> --><!-- 
                            <div class="col-md-1">
                              <h3> : </h3>
                            </div> -->
                            <div class="col-md-3">
                              <label>Kode Kasir</label>
                              <input type="text" readonly class="form-control"   id="kd_kasir" name="kategori_petugas" />
                            </div>
                            <div class="col-md-3">
                              <label>Kode Penjualan  </label>
                              <input type="hidden" readonly class="form-control"   id="kode_petugas" name="kode_petugas" />
                              <input type="text" readonly class="form-control"   id="kd_pen" name="nama_petugas" />

                            </div>
                            <div class="col-md-3">
                              <label> Nama Customer Member </label>
                              <input type="hidden" readonly class="form-control"   id="kode_petugas" name="kode_petugas" />
                              <input type="text" readonly class="form-control"   id="nama_cs" name="nama_petugas" />

                            </div>
                            <!-- <div class="col-md-3 sopir">
                              <label>Sopir </label>
                              <input type="hidden" readonly class="form-control"   id="kode_sopir" name="kode_sopir" />
                              <input type="text" readonly class="form-control"   id="nama_sopir" name="nama_sopir" />

                            </div> -->
                          </div>
                         <!--  <div class="row kendaraan">
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-1">

                            </div>
                            <div class="col-md-3">
                             <label>Kendaraan</label>
                             <input type="text" readonly class="form-control"   id="no_kendaraan" name="no_kendaraan" />  
                             <input type="hidden" readonly class="form-control"   id="nama_kendaraan" name="nama_kendaraan" />
                           </div>
                         </div> -->
                       </div> 
                       <br>

                       <div class="box-body" >
                       <!--  <div class="row">

                          <div class="col-md-2">
                            <h3>Posisi Tujuan</h3>
                          </div>
                          <div class="col-md-1">
                            <h3> : </h3>
                          </div>

                          <div class="col-md-4">
                            <h3>GUDANG</h3>
                          </div>
                        </div> -->
                      </div> 
                      <div class="sukses" ></div>
                      <br>
                      <br>
                      <div class="gagal" ></div>
                      <div id="list_transaksi_pembelian">
                        <div class="box-body"><br>
                          <table id="tabel_daftar" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Stok Beli</th>
                                <th>Stok Ingin di Retur</th>
                              </tr>
                            </thead>
                            <tbody id="table_load_penjualan">

                            </tbody>
                            <tfoot>

                            </tfoot>
                          </table>
                        </div>

                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <label>Keterangan</label>
                              <textarea class="form-control" value="" name="keterangan" id="keterangan" required=""></textarea>
                            </div>
                          </div>
                        </div>

                      </div>
                      
                      <br>
                      <div class="box-footer ">
                        <!-- <button type="submit" class="btn btn-primary ">Simpan</button> -->
                        <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
                      </div>
                    </form>
                  </div>
                </div>
              </section><!-- /.Left col -->      
            </div>
          </div>
        </div>
      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
<div id="modal-regular" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form  method="post">
        <div class="modal-header" style="background-color:grey">

          <h4 class="modal-title" style="color:#fff;">Retur</h4>
        </div>
        <div class="modal-body" >
          <div class="form-body">

           <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Kode Penjualan</label>
                <input type="text" id="kode_surat" name="kode_surat" class="form-control" placeholder="Kode Penjualan" required="">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer" style="background-color:#eee">
        <a href="<?php echo base_url().'stok/stok_retur'; ?>" class="btn blue" aria-hidden="true">Cancel</a>
        <button id="cari_mutasi" class="btn green">Cari</button>
      </div>
    </form>
  </div>
</div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'mutasi/daftar_mutasi'; ?>";
  });
</script>

<script>
 $(document).ready(function(){
  //$("#tabel_daftar").dataTable();
  $("#modal-regular").modal('show');
  $("#update").hide();
  
  $(".sopir").hide();
  $(".kendaraan").hide();
  $("#cari_mutasi").click(function(){
    var kode_penjualan = $('#kode_surat').val();  
    
    var url_cari = "<?php echo base_url().'stok/get_retur/'?>";
    $.ajax({
      type: "POST",
      url: url_cari,
      data: {kode_penjualan:kode_penjualan},
      dataType:'json',
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $("#modal-regular").modal('hide');
        $(".tunggu").hide();
        if (msg[0].kode_penjualan) {
          $(".tunggu").hide();
          $(".modal_regular").modal('hide');
          $("#kd_kasir").val(msg[0].kode_kasir);
          $("#kd_pen").val(msg[0].kode_penjualan);
          $("#nama_cs").val(msg[0].nama_member);
          load_stok_penjualan(msg[0].kode_penjualan);
        } 

    }
  });
    return false;
  });
});
function load_stok_penjualan(kode_penjualan){
  $.ajax({
    url: '<?php echo base_url("stok/get_detail_penjualan/") ?>'+'/'+kode_penjualan,
    method : 'post',
    dataType: 'html',
    success: function(html){
      $("#table_load_penjualan").html(html);
    }
  });
}



</script>
