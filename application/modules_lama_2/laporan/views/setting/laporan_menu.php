
<script src="https://public.azurewebsites.net/js/jquery.table2excel.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<style type="text/css">
	.ombo{
		width: 400px;
	} 

</style>    
<!-- Main content -->
<section class="content">             
	<!-- Main row -->
	<div class="row">
		<!-- Left col -->
		<section class="col-lg-12 connectedSortable">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						Laporan Produk
					</div>
					<div class="tools">
						<a href="javascript:;" class="collapse">
						</a>
						<a href="javascript:;" class="reload">
						</a>

					</div>
				</div>
				<div class="portlet-body">
					<!------------------------------------------------------------------------------------------------------>


					<div class="box-body">            
						<div class="sukses" ></div>
						<div class="row">
							<div class="col-md-5" id="trx_penjualan">
								<div class="input-group">
									<span class="input-group-addon">Bulan</span>
									<select class="form-control" name="bulan" id="bulan" required="">              
										<option value="">
											-- Bulan --
										</option>
										<?php
										for ($i=1; $i <= 12 ; $i++) { 
											?>
											<option value="<?php if(strlen($i)==1) {
												echo "0".$i;   
											}else{
												echo $i;
											}  ?>">
											<?php 
											if($i==1){
												echo "Januari";
											}elseif ($i==2) {
												echo "Februari";
											}elseif ($i==3) {
												echo "Maret";
											}elseif ($i==4) {
												echo "April";
											}elseif ($i==5) {
												echo "Mei";
											}elseif ($i==6) {
												echo "Juni";
											}elseif ($i==7) {
												echo "Juli";
											}elseif ($i==8) {
												echo "Agustus";
											}elseif ($i==9) {
												echo "September";
											}elseif ($i==10) {
												echo "Oktober";
											}elseif ($i==11) {
												echo "November";
											}elseif ($i==12) {
												echo "Desember";
											}

											?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-5" id="trx_penjualan">
							<div class="input-group">
								<span class="input-group-addon">Tahun</span>
								<select class="form-control" name="tahun" id="tahun" required="">              
									<option value="">
										-- Tahun --
									</option>
									<?php
									$tanggal_sekarang = date('Y');
									$date = $tanggal_sekarang-10;
									for ($i=$date; $i <= $tanggal_sekarang ; $i++) { 
										?>
										<option value="<?php echo $i ?>">
											<?php echo $i ?>
										</option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class=" col-md-1">
							<div class="input-group pull-right">
								<button style="width:100%" type="button" class="btn btn-success" onclick="cari_transaksi()"><i class="fa fa-search"></i> Cari</button>

							</div>
						</div>
						<div class="col-md-1">
							<button style="" type="button" class="btn btn-info pull-right" id="cetak"><i class="fa fa-print"></i> Print</button>
						</div>
					</div>
					<div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none" >
						<img src="<?php echo base_url() . '/public/img/loading.gif' ?>" >
					</div>
					<form id="pencarian_form" method="post" style="margin-left: 18px;" class="form-horizontal" target="_blank">



						<br>


					</form>
					<div class="row">
							<!-- <div class="col-md-1 pull-right">
								<button style="" type="button" class="btn btn-info pull-right" id="cetak"><i class="fa fa-print"></i> Print</button>
							</div> -->
							<!-- <div class="col-md-1 pull-right">
								<button style="" type="button" class="btn btn-warning pull-right" id="export"><i class="fa fa-table"></i> Export</button>
							</div><br> -->
						</div>

						<div id="cari_menu">
							<div>
								<?php

								$this->db->select('*'); 
								$this->db->distinct();
								$this->db->like('tanggal_transaksi',date('Y-m'));
								// $this->db->where('tanggal_transaksi',date('Y-m-d')) ;

								$this->db->group_by('kode_menu');
								$penjualan = $this->db->get('opsi_transaksi_penjualan');
                //echo $this->db->last_query();
								$hasil_penjualan = $penjualan->result();
								$keuangan = 0;
								foreach($hasil_penjualan as $total){
									@$keuangan += $total->grand_total;

								}

								?>
								<!-- <label><h4><strong>Total Transaksi Menu : <?php echo @count($hasil_penjualan); ?></strong></h4></label><br /> -->
								<!-- <span><label><h4><strong>Total Keuangan Penjualan :<?php echo @format_rupiah($keuangan); ?></strong></h4></label></span> -->
							</div><hr>
							<table id="tabel_daftar" class="table table-bordered table-striped" style="font-size:1.5em;">

								<thead>
									<tr>
										<th width="20">No</th>

										<th>Nama Produk</th>
										<th>Total</th>

									</tr>
								</thead>
								<tbody>
									<?php
									$nomor = 1;

									foreach($hasil_penjualan as $daftar){
										if(!empty($tgl_awal) and !empty($tgl_akhir)){
											$jumlah_menu = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu' => $daftar->kode_menu,'tanggal_transaksi >=' => @$tgl_awal,'tanggal_transaksi <=' => @$tgl_akhir));   
										}else{
											$jumlah_menu = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu' => $daftar->kode_menu));    
										}

                    //echo $this->db->last_query();
										$hasil_jumlah = $jumlah_menu->result();
										$total_menu=0;

										foreach ($hasil_jumlah as $value) {
											$total_menu+=$value->jumlah;

										}
										@$total+=$total_menu;
										?> 
										<tr>
											<td><?php echo $nomor; ?></td>

											<td><?php echo @$daftar->nama_menu; ?></td>
											<td align="center"><?php echo @$total_menu; ?></td>

										</tr>
										<?php $nomor++; } ?>

									</tbody>
									<tfoot>
										<tr>
											<th>No</th>

											<th>Nama Produk</th>
											<th>Total</th>

										</tr>
									</tfoot>
								</table>
							</div>

							<!------------------------------------------------------------------------------------------------------>

						</div>
					</div>

					<!-- /.row (main row) -->
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->
			<style type="text/css" media="screen">
			.btn-back
			{
				position: fixed;
				bottom: 10px;
				left: 10px;
				z-index: 999999999999999;
				vertical-align: middle;
				cursor:pointer
			}
		</style>
		<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

		<script>
			$('.btn-back').click(function(){
				$(".tunggu").show();
				window.location = "<?php echo base_url().'laporan'; ?>";
			});
		</script>

		<script>
			$("#cetak").click(function(){
				var bulan =$("#bulan").val();
				var tahun =$("#tahun").val();
				window.open("<?php echo base_url().'laporan/cetak_laporan_menu/'; ?>"+bulan+"/"+tahun);

			})
			jQuery(document).ready(function() {
				$("#export").click(function(){
					$("#tabel_daftar").table2excel();
				});
			});
			function cari_transaksi(){
				var bulan =$("#bulan").val();
				var tahun =$("#tahun").val();
				var petugas = $("#nama_petugas").val();
				$.ajax( {  
					type :"post",  
					url : "<?php echo base_url().'laporan/search_laporan_menu'; ?>",  
					cache :false,
					data : {bulan:bulan,tahun:tahun},
					beforeSend:function(){
						$(".tunggu").show();  
					},
					success : function(data) {
						$("#cari_menu").html(data);
						$(".tunggu").hide();  
					},  
					error : function(data) {  
						alert("Kosong");  
						$(".tunggu").hide();  
					}  
				});
			}
		</script>
