                           
<?php $this->db->truncate('opsi_transaksi_penjualan_temp'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  
  <style type="text/css">
    .ombo{
      width: 600px;
    } 
  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->


    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box grey-steel">
          <div class="portlet-title">
            <div class="caption">
              <span style="font-size: large; color: black;" class="pull-left">Menu Kasir</span>

            </div>
            <table id="col_manual" class="table table-striped table-bordered table-advance table-hover">
              <tbody >
                <form >
                  <tr style="background-color:#229fcd;">
                    <td width="100px">
                      <select onchange="cek_jenis()" class="form-control" id="jenis">
                        <!-- <option value="non member">Non Member</option> -->
                        <option value="member">Member</option>
                        <option value="sales">Sales</option>
                        <option value="sopir">Sopir</option>
                        <option value="sales & sopir">Sales & Sopir</option>
                      </select>
                    </td>
                    <td width="200px">
                     <div>
                      <span class="jenis_member">
                        <select  onchange="get_member()" class="form-control select2 inputClear" id="kode_member" name="kode_member">
                         <option value="">Pilih Member</option>
                         <?php 
                         $get_member=$this->db->get('master_member');
                         $hasil=$get_member->result();
                         foreach ($hasil as $list) {
                          ?>
                          <option value="<?php echo $list->kode_member;  ?>"><?php echo $list->kode_member."  ". $list->nama_member;  ?></option>
                          <?php } ?>
                        </select>
                        <!-- <input onkeyup="get_member()" type="text" class="form-control" name="kode_member" id="kode_member" /> -->
                      </span>
                      <span class="jenis_sales">
                        <input onkeyup="get_no_surat();" type="text" class="form-control inputClear" placeholder="No. Surat Jalan" name="no_surat" id="no_surat" />
                        <select  onchange="get_member()" class="form-control select2" id="kode_member_sales" name="kode_member">
                         <option value="">Pilih Member</option>
                         <?php 
                         $get_member=$this->db->get('master_member');
                         $hasil=$get_member->result();
                         foreach ($hasil as $list) {
                          ?>
                          <option value="<?php echo $list->kode_member;  ?>"><?php echo $list->kode_member." "."-"." ". $list->nama_member;;  ?></option>
                          <?php } ?>
                        </select>
                      </span>

                    </div>
                  </td>
                  <td width="300px">
                    <div class="">
                      <div class="">
                        <span class="jenis_member">
                          <input readonly="true" type="text" class="form-control inputClear" placeholder="Nama Member" name="nama_member" id="nama_member" />
                          <input readonly="true" type="hidden" class="form-control inputClear" name="" id="kat_member" />
                        </span>
                        <span class="jenis_sales">
                          <input readonly="true" type="text" class="form-control inputClear" placeholder="Nama Petugas" name="nama_petugas" id="nama_petugas" />
                          <input readonly="true" type="text" class="form-control inputClear" placeholder="Nama Member" name="nama_member" id="nama_member_sales" />
                          <input readonly="true" type="hidden" class="form-control inputClear" name="kode_petugas" id="kode_petugas" />
                        </span>
                        <span class="jenis_sales_sopir">
                          <input readonly="true" type="text" class="form-control inputClear" placeholder="Nama Sopir" name="nama_sopir" id="nama_sopir" />
                          <input readonly="true" type="hidden" class="form-control inputClear" name="kode_sopir" id="kode_sopir" />
                        </span>

                      </div>
                    </div>
                  </td>
                  <td width="100px" class="text-center">
                    <div>
                      <a class="btn purple" id="btn_simpan" onclick="show_modal_next()">NEXT</a>
                      <a class="btn red" id="btn_cancel" onclick="show_modal_cancel()">CANCEL</a>
                    </div>
                  </td>
                </tr>
              </form>
            </tbody>
          </table>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>
          <?php
          if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
          else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
          else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
          else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
          else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
         else if(getenv('REMOTE_ADDR'))
          $ipaddress = getenv('REMOTE_ADDR');
        else
          $ipaddress = 'UNKNOWN';

        $get_kasir = $this->db->get_where('master_kasir',array('ip'=>$ipaddress));
        $hasil_kasir = $get_kasir->row();
                                      #echo $this->db->last_query();
        $nomor_kasir = $hasil_kasir->kode_kasir;

        $user = $this->session->userdata('astrosession');
        $tgl = date("Y-m-d");
        $no_belakang = 0;
        $this->db->select_max('kode_penjualan');
        $kode = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$tgl));
        $hasil_kode = $kode->row();
                                        #$pecah_kode = explode("_",$hasil_kode_pembelian->kode_pembelian);
                                        #echo $pecah_kode[0];
                                        #echo $pecah_kode[2];
        $this->db->select('kode_penjualan');
        $kode_penjualan = $this->db->get('master_setting');
        $hasil_kode_penjualan = $kode_penjualan->row();
        if(count($hasil_kode)==0){
          $no_belakang = 1;
        }
        else{
          $pecah_kode = explode("_",$hasil_kode->kode_penjualan);
          $no_belakang = @$pecah_kode[4]+1;
        }
        $this->db->select_max('kode_transaksi');
        $kasir = $this->db->get_where('transaksi_kasir',array('tanggal'=>$tgl,'status'=>"open",
          'kode_kasir'=>$nomor_kasir));
        $hasil_cek_kasir = $kasir->row();
                                        #echo $this->db->last_query();

        $ipaddress = '';

        $this->db->select_max('id');
        $get_max_po = $this->db->get('transaksi_penjualan');
        $max_po = $get_max_po->row();

        $this->db->where('id', $max_po->id);
        $get_po = $this->db->get('transaksi_penjualan');
        $po = $get_po->row();

        $user = $this->session->userdata('astrosession');
        $id_user=$user->id;
        $tahun = substr(@$po->kode_penjualan, 4,4);
        if(date('Y')==$tahun){
          $nomor = substr(@$po->kode_penjualan, 12);
            //echo $nomor;
          $nomor = $nomor + 1;
          $string = strlen($nomor);
          if($string == 1){
            $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_00000'.$nomor;
          } else if($string == 2){
            $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_0000'.$nomor;
          } else if($string == 3){
            $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_000'.$nomor;
          } else if($string == 4){
            $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_00'.$nomor;
          } else if($string == 5){
            $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_0'.$nomor;
          } else if($string == 6){
            $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_'.$nomor;
          }
        } else {
          $kode_trans = 'PEN_'.date('Y').'_'.$id_user.'_000001';
        }
        ?>
        <input type="hidden" value="<?php echo @$kode_trans ?>" placeholder="Kode Transaksi" name="kode_penjualan" id="kode_penjualan" />
        <input type="hidden" value="<?php echo @$kode_trans ?>" placeholder="Kode Transaksi" name="kode_penjualan_baru" id="kode_penjualan_baru" />

        <input type="hidden" value="<?php echo TanggalIndo(date("Y-m-d")); ?>" readonly="true"  placeholder="Tanggal Transaksi" name="tanggal_pembelian" id="tanggal_penjualan"/>


        <div class="box-body">            

          <div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none" >
            <img src="<?php echo base_url() . '/public/images/loading1.gif' ?>" >
          </div>
          <div class="sukses"></div>
          <div class="row">
           <div class="col-md-8">

        <!--<table id="col_auto" class="table table-striped table-bordered table-advance table-hover" width="40%;">
                    <tbody >
                      <form >
                        <tr>
                         <td width="100px" style="background-color:#229fcd;">
                          <input autofocus type="text" name="barcode" id="barcode" class="form-control" placeholder="" />

                        </td>
                        <td align="center" valign="bottom" width="100px" style="background-color:#229fcd;color: white;font-size: large;font-weight: bold;">
                          Mode Barcode Auto
                          <input type="hidden" name="barcode" id="harga_barcode_auto" class="form-control" placeholder="" />
                          </td>
                          
                        </tr>
                      </form>
                    </tbody>
                  </table>-->




                  <table id="too_show" class="table table-striped table-bordered table-advance table-hover">
                    <tbody>
                      <form id="panelForm">
                        <tr>
                          <td width="250px" style="background-color:#229fcd;">
                            <input type="text" name="id_penjualan" id="id_penjualan" value="" hidden/>
                            <?php
                            $this->db->where('real_stock >','0');
                            $this->db->where('status', 'sendiri');
                            $menu_resto = $this->db->get('master_bahan_jadi');
                            $hasil_menu = $menu_resto->result();

                            
                          // echo $this->db->last_query();
                            ?>
                            <select name="menu" id="menu" class="form-control select2" onchange="clear_Oldstep()">
                             <option value="" selected="true">--Pilih Produk--</option>
                             <?php
                             foreach($hasil_menu as $daftar){
                               ?>
                               <option value="<?php echo $daftar->kode_bahan_jadi; ?>"><?php echo $daftar->nama_bahan_jadi; ?></option>
                               <?php } ?>
                             </select>
                           </td>
                      <!--    <td width="105px" style="background-color:#229fcd;">
                          <select name="satuan_penjualan"  id="satuan_penjualan" class="form-control">
                           <option value="Item" >Item</option>
                           <option value="Lusin" >Lusin</option>

                         </select> 


                       </td> -->

                       <td width="100px" style="background-color:#229fcd;">

                        <input type="text" name="qty" onclick="this.select();" onkeyup="get_kategori_harga()" id="qty" class="form-control" placeholder="jumlah" />
                        <input type="hidden" id="kode_kasir" value="<?php echo $hasil_cek_kasir->kode_transaksi; ?>" />
                        <input type="hidden" name="qty" id="qty2" class="form-control" placeholder="jumlah">
                      </td>
                      <td width="100px" style="background-color:#229fcd;">
                        <input readonly="true" type="text" name="harga" id="harga" class="form-control" placeholder="harga">
                      </td>
                      <td width="150px" style="background-color:#229fcd;">
                        <select hidden name="jenis_diskon" id="jenis_diskon" onchange="clear_diskon()" class="form-control">
                          <option value="persen" selected="true">Persen</option>
                          <option value="rupiah">Rupiah</option>
                        </select>
                      </td>
                      <td width="100px" style="background-color:#229fcd;">
                        <div class="input-icon right" id="form_diskon_item">
                          <i class="fa">%</i>
                          <input type="text" name="diskon" onclick="this.select();" onkeyup="diskon_persen_item()" id="diskon_item" class="form-control" placeholder="Diskon Persen" value="0">
                        </div>
                        <div class="input-icon right" id="form_diskon_rupiah">
                          <i class="fa">Rp.</i>
                          <input type="text" name="diskon_rupiah" onclick="this.select();" onkeyup="diskon_rupiah_item()" id="diskon_rupiah" class="form-control" placeholder=" Diskon Rupiah" value="0">
                        </div>
                        <input type="hidden" name="kode_edit_penjualan" id="kode_edit_penjualan" />
                      </td>
                      <td width="75px" style="background-color:#229fcd;" >
                        <div onclick="simpan_pesanan_temp()" class="btn purple">Add</div>
                        <div id="update" onclick="editData()" class="btn purple">Update</div>
                      </td>
                    </tr>
                  </form>
                </tbody>
              </table>
            </div>
            <div class="col-md-8">
              <table style="white-space: nowrap; font-size: 1.5em;" id="data" class="table table-bordered  table-hover">
                <thead>
                  <tr>
                    <th style="background-color:#229fcd; color:white" class="text-center" width="50px">No.</th>
                    <th style="background-color:#229fcd; color:white" class="text-center">Nama Produk</th>

                    <th style="background-color:#229fcd; color:white" class="text-center" width="50px">Qty</th>
                    <th style="background-color:#229fcd; color:white" class="text-center" width="125px">Harga</th>
                    <th style="background-color:#229fcd; color:white" class="text-center" width="125px">Subtotal</th>
                    <th style="background-color:#229fcd; color:white" class="text-center" width="100px">Diskon</th>
                    <th style="background-color:#229fcd; color:white" class="text-center" width="170px">Action</th>
                  </tr>
                </thead>
                <tbody id="tb_pesan_temp">
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
              <div class="row">
                <div class="form-group col-md-4"  >
                 <div class="input-group">
                   <span class="input-group-addon">All Diskon</span>
                   <select hidden name="jenis_diskon_all" id="jenis_diskon_all" onchange="change_diskon_all()" class="form-control">
                    <option value="persen" selected="true">Persen</option>
                    <option value="rupiah">Rupiah</option>
                  </select>
                </div>
              </div>

              <div class="form-group col-md-5"  >
               <div class="input-icon right" id="persen_all">
                <i class="fa">%</i>
                <input type="text" name="persen" onclick="this.select();" onkeyup="diskon_persen()" id="persen" class="form-control" placeholder="Diskon Persen" value="0">
              </div>
              <div class="input-icon right" id="rupiah_all">
                <i class="fa">Rp.</i>
                <input type="text" name="rupiah" onclick="this.select();" onkeyup="diskon_rupiah()" id="rupiah" class="form-control" placeholder=" Diskon Rupiah" value="0">
              </div>
            </div>

                <!-- <div class="form-group col-md-6">
                  <div class="input-group">
                    <span class="input-group-addon">Diskon(%)</span>
                    <span id="dibayar">
                      <input type="text" onkeyup="diskon_persen()" onclick="this.select();" value="0" class="form-control" name="persen" id="persen" />
                    </span>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div class="input-group">
                    <span class="input-group-addon">Diskon(Rp)</span>
                    <span id="dibayar">
                      <input type="text" onkeyup="diskon_rupiah()" onclick="this.select();" class="form-control" value="0" name="rupiah" id="rupiah" />
                    </span>
                  </div>
                </div> -->

                <div class="form-group col-md-12"  >
                  <div class="input-group">
                    <span class="input-group-addon">Note</span>   
                    <span id="dibayar">                   
                      <textarea class="form-control" name="note" id="note"></textarea>
                    </span>
                  </div>
                </div>
              </div>


            </div>

            <div style="margin-top: -65px;" class="col-md-4">
              <div class="bg-yellow" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                <span style="font-size:22px; " class="pull-right" id="total_pesanan"></span>
                <input type="hidden" id="total_pesanan_hidden">
                <p style="font-size: 18px;">Total Pesanan</p>
              </div>
              <div class="bg-red" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                <span style="font-size:22px; " class="pull-right" id="diskon_all">Rp 0</span>
                <i style="font-size:56px; margin-top:5px"></i>
                <p style="font-size: 18px;">Discount</p>
              </div>
              <div class="bg-blue" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                <span style="font-size:22px; " class="pull-right" id="grand_total">Rp 0</span>
                <i style="font-size:56px; margin-top:5px"></i>
                <p style="font-size: 18px;">Grand Total</p>
              </div>



              <div style="height:60px; margin-top:5px">
                <div style="height: 40px;" class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">Jenis Transaksi</span>
                    <span id="golongan">
                      <select class="form-control" id="jenis_transaksi" name="jenis_transaksi">
                        <option selected="" value="tunai">Tunai</option>
                        <option value="kredit">Kredit</option>
                        <option value="konsinyasi">Konsinyasi</option>
                      </select>
                    </span>
                  </div>
                </div>
              </div>


              <div id="div_jatuh_tempo" style="height:60px; margin-top:-20px">
                <div style="height: 40px;" class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">Jatuh Tempo &nbsp;&nbsp;&nbsp;</span>
                    <span id="golongan">
                     <input type="date" class="form-control" name="jatuh_tempo" id="jatuh_tempo" value="<?php echo date('Y-m-d') ?>">
                   </span>
                 </div>
               </div>
             </div>


             <div style="height:60px;margin-bottom: 5px;margin-top: -20px;">
              <div style="height: 40px;" class="form-group">
                <div class="input-group">
                  <span class="input-group-addon">Ongkos Kirim &nbsp;&nbsp;&nbsp;</span>
                  <span id="golongan">
                    <input onkeyup="ongkos_kirim()" onclick="this.select();" type="text" class="form-control" id="ongkos_kirim" />

                  </span>
                </div>
              </div>
            </div>
            <div style="height:60px; margin-top: -20px;">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon" style="font-size: x-large;font-weight: bolder;width: 100px"><strong >Dibayar &nbsp;</strong></span>
                  <span id="dibayar">
                    <input type="hidden" id="total_no" />
                    <input type="hidden" id="total2" />
                    <input type="hidden" id="kembalian2" />
                    <input style="font-size: 30px;" onkeyup="kembalian()" type="text" class="form-control input-lg" name="bayar" id="bayar" />
                  </span>
                </div>
              </div>
            </div>

            <div class="bg-purple" style="height:40px; padding: 0px 10px 0px 10px; margin-top:-5px">
              <span style="font-size:26px; " class="pull-right totalDiskon" id="kembalian">Rp 0</span>
              <i style="font-size:56px; margin-top:5px"></i>
              <p id="text_dibayar" style="font-size: 18px;">Kembalian</p>
            </div>







                        <!--<div class="bg-green" style="height:55px; padding: 10px 10px 0px 10px; margin-top:35px">
                            <button class="btn btn-large btn-default" style="width:100%" onclick="list_suspend()">LIST PESANAN</button>
                          </div>-->
                        </div><br />
                        <div id="rupiah_bayar" style="padding-left: 500px;margin-top:50px;font-size:35px" class="pull-right col-md-12">

                        </div>
                      </div>
                      <div class="row">

                        <div class="col-md-12">
                          <input type="hidden" id="id_meja" value="<?php echo $this->uri->segment(3); ?>" />
                          
                          
                          
                          
                          
                          <input type="hidden" id="hasil_meja" />
                          
                        </div>
                        
                        
                      </div>
                      <br /><br />
                      
                      <!-- <div id="data_pengiriman" class="row" style=" margin-top: -10px;">
                        <fieldset>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Nama Penerima</label>
                              <input placeholder="Nama" type="text" class="form-control" id="penerima" />
                            </div>
                            
                            <div class="form-group">
                              <label>Jam Pengiriman</label>
                              <input placeholder="No. Telp" type="text" class="form-control timepicker" id="jam_kirim" />
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>No. Telp</label>
                              <input placeholder="No. Telp" type="text" class="form-control" id="no_telp" />
                            </div>
                            
                            <div class="form-group">
                              <label>Tanggal Pengiriman</label>
                              <input placeholder="Tanggal" type="date" class="form-control" id="tgl_kirim" />
                            </div>
                            
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>Alamat</label>
                              <textarea id="alamat" class="form-control" placeholder="Alamat"></textarea>
                            </div>
                            
                          </div>
                        </fieldset>
                        
                      </div> -->
                      
                      <div class="row">


                        <div class="col-lg-12">
                          <a style="text-decoration: none;" onclick="konfirm_bayar()"  class="bg-green btn col-md-12">
                            <center><span style="font-size:35px; font-weight: bold; "><i style="font-size: 35px;" class="fa fa-money"></i> Bayar</span></center>	
                          </a>
                        </div>
                        
                      </div><br /><br />
                      
                      
                      
                      
                      
                      
                      
                      <!------------------------------------------------------------------------------------------------------>

                    </div>
                  </div>
                  
                  <!-- /.row (main row) -->
                </section><!-- /.content -->
              </div>
              
              
            </div>  
          </div>
        </section><!-- /.Left col -->      
        
        
        
      </div><!-- /.row (main row) -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <div id="modal-confirm-bayar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Pembayaran</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan dengan nominal pembayaran tersebut ?</span>
          <input id="no" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button id="ya" onclick="bayar()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <!-------------------------------------- modal next -------------------------------------->
  <div id="show_modal_next" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:12pt">Apakah anda yakin Data tersebut ?</span>
          <input id="no" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button id="ya" onclick="next_step()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <!-------------------------------------- modal cancel -------------------------------------->
  <div id="show_modal_cancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:12pt">Apakah anda yakin Membatalkan Data Ini ?</span>
          <input id="no" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button id="ya" onclick="clear_temp()()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>


  <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus pesanan tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="delData()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>
  

  <script>
    function next_step(){
      $('#show_modal_next').modal('hide');
      var kode_member       = $('#kode_member').val();
      var no_surat          = $('#no_surat').val();
      var nama_petugas      = $('#nama_petugas').val();
      var nama_member_sales = $('#nama_member_sales').val();
      var nama_sopir        = $('#nama_sopir').val();
      var kode_member_sales = $('#kode_member_sales').val();
      var jenis             = $('#jenis').val();
      var nama_petugas       = $('#nama_petugas').val();
      if (jenis == 'member' && kode_member == '') {
        alert('Harap Mengisi Semua Field.');
      }else if(jenis == 'sales' && (no_surat == '' || kode_member_sales =='' || nama_member_sales == '')){
        alert('Harap Mengisi Semua Field atau Data Salah');
      }else if(jenis == 'sopir' && (no_surat == ''|| nama_petugas=='' || nama_member_sales == '')){
        alert('Harap Mengisi Semua Field atau Data Salah');
      }else if(jenis == 'sales & sopir'&& (no_surat == '' || nama_petugas =='' || nama_member_sales == '')){
        alert('Harap Mengisi Semua Field atau Data Salah');
      }else {

        if (jenis != 'member') {
          var url = "<?php echo base_url().'kasir/kasir/cek_produk_nonmember'; ?>";
          $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {kode:no_surat},
            success: function(data){
              $("#menu").html('<option value="">--Pilih Produk--</option>');
              $.each(data, function(index, field) {
                $("#menu").append('<option value="'+field.kode_bahan+'">'+field.nama_bahan+'</option>');
              });
            }
          });
        }
        $(".portlet-body").show();
        $("#btn_simpan").hide();
        $("#btn_cancel").show();
        $('#jenis').attr('disabled',true);
        $('#kode_member').attr('readonly',true);
        $('#no_surat').attr('readonly',true);
        $('#kode_member_sales').attr('readonly',true);
      }
    }

    function change_diskon_all(){
      var jenis_diskon_all = $('#jenis_diskon_all').val();
      $('#persen').val('0');
      $('#rupiah').val('0');
      if (jenis_diskon_all == 'rupiah') {
        $('#persen_all').hide();
        $('#rupiah_all').show();
      }else{
        $('#rupiah_all').hide();
        $('#persen_all').show();
      }

      
      
    }

    function clear_temp(){
      var url = "<?php echo base_url().'kasir/kasir/clear_temp_kasir'; ?>";
      $.ajax({
        type: 'POST',
        url: url,
        success: function(data){
          location.reload();
        }
      });
    }

    function clear_Oldstep(){
      $("#qty").val('0');
      $("#diskon_item").val('0');
      $("#diskon_rupiah").val('0');
    }

    function clear_diskon(){
      $("#diskon_item").val('0');
      $("#diskon_rupiah").val('0');
    }

    function diskon_persen_item(){
      var nilai = $("#diskon_item").val();
      if (nilai < 0 || nilai > 100) {
        alert('Diskon(%) Salah.');
        $("#diskon_item").val('0');
      }
    }

    function diskon_rupiah_item(){
      var nilai = $("#diskon_rupiah").val();
      var qty   = $("#qty").val();
      var harga = $("#harga").val();
      var total = qty * harga;
      if (nilai < 0 || nilai > total) {
        alert('Diskon(Rp) Salah.');
        $("#diskon_rupiah").val('0');
      }
    }

    function setting() {
      $('#modal_setting').modal('show');
    }

    function konfirm_bayar(){
      $("#modal-confirm-bayar").modal('show');
    }

    function show_modal_next(){
      $("#show_modal_next").modal('show');
    }

    function show_modal_cancel(){
      $('#show_modal_cancel').modal('show');
    }

    function taking_order(){
      $('#modal-taking-order').modal('show');
      $("#nota_to").focus();
    }
    $("#jenis_diskon").change(function(){
      if($("#jenis_diskon").val()=='rupiah'){
        $("#form_diskon_rupiah").show();
        $("#form_diskon_item").hide();
      } else {
        $("#form_diskon_rupiah").hide();
        $("#form_diskon_item").show();
      }
    });

    $("#jenis_transaksi").change(function(){
      var kat_member=$('#kat_member').val();
      var jenis_transaksi=$('#jenis_transaksi').val();
      if (/*kat_member!='konsinyasi'  && */ jenis_transaksi=='kredit' || jenis_transaksi=='konsinyasi') {
       $("#div_jatuh_tempo").show();
       $("#div_note").show();
       $("#text_dibayar").html('Hutang&nbsp;&nbsp;');
     }else{
      $("#div_jatuh_tempo").hide();
      $("#note").val('');
      $("#div_note").hide();
      $("#text_dibayar").html('Kembalian&nbsp;&nbsp;');
    }
  });


    $(document).ready(function(){
      $("#btn_cancel").hide();
      $(".portlet-body").hide();
      $('#rupiah_all').hide();
      $("#form_diskon_rupiah").hide();
      $("#div_jatuh_tempo").hide();
      var kode_kasir = $("#kode_kasir").val();
      $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>'+kode_kasir);
      $("#form_setting").submit(function(){
        var keterangan = "<?php echo base_url().'kasir/keterangan'?>";
        $.ajax({
          type: "POST",
          url: keterangan,
          data: $('#form_setting').serialize(),
          success: function(msg)
          {
            $('#modal_setting').modal('hide');  
          }
        });
        return false;
      });
      $('#col_auto').hide();
      $('#col_manual').show();
      $('#barcode_manual').focus();
      $("#data_pengiriman").hide();
      /*$('input.timepicker').timepicker({ 
       timeFormat: 'HH:mm',
       interval: 30,
       scrollbar:true 
     });*/

//$("#no_surat").hide();
$('.jenis_sales').hide();
$('.jenis_sales_sopir').hide();


});
                           function tutupmeja(){
                            var url = "<?php echo base_url().'kasir/kasir/tutup_meja'; ?>";
                            var id_meja = $("#id_meja").val();
                            $.ajax( {
                             type:"POST", 
                             url : url,  
                             cache :false,  
                             data :{id_meja:id_meja},

                             beforeSend:function(){
                              $(".tunggu").show();  
                            },
                            success : function(data) {
                             $(".tunggu").hide(); 
                             $(".sukses").html(data);
                             setTimeout(function(){$('.sukses').html('');},1500); 
                           },  
                           error : function(data) {  
                            alert(data);  
                          }  
                        });
                          }
                          function bukameja(){
                            var url = "<?php echo base_url().'kasir/kasir/buka_meja'; ?>";
                            var id_meja = $("#id_meja").val();
                            $.ajax( {
                             type:"POST", 
                             url : url,  
                             cache :false,  
                             data :{id_meja:id_meja},
                             beforeSend:function(){
                              $(".tunggu").show();  
                            },
                            success : function(data) {  
                              $(".tunggu").hide(); 
                              window.location.reload();
                            },  
                            error : function(data) {  
                              alert(data);  
                            }  
                          });
                          }

                          function simpan_pesanan_temp(){
                            var url = "<?php echo base_url().'kasir/simpan_pesanan_temp'; ?>";
                            var kode_penjualan = $("#kode_penjualan").val();
                            $('#id_penjualan').val('--Pilih Produk--  ');
                            var tanggal_penjualan = $("#tanggal_penjualan").val();
                            var nomor_nota =$("#nomor_nota").val();
                            var kode_meja = $("#kode_meja").val();
                            var menu = $("#menu").val();
                            var jumlah = $("#qty").val();
                            var harga = $("#harga").val();
                            var diskon = $("#diskon_item").val();
                            var kode_kasir = $("#kode_kasir").val();
                            var jenis_diskon = $("#jenis_diskon").val();
                            var diskon_rupiah = $("#diskon_rupiah").val();
                            var jenis = $("#jenis").val();
                            var no_surat = $("#no_surat").val();
                            var kode_petugas = $("#kode_petugas").val();


                            if(jumlah < 1 || menu==""){
                              $(".sukses").html("<div class='alert alert-warning'>Jumlah Barang Salah</div>");
                              setTimeout(function(){$('.sukses').html('');},1500); 

                            }else if(parseInt(diskon) < 0 || parseInt(diskon) >100){
                             $(".sukses").html("<div class='alert alert-warning'>Jumlah Diskon Item Salah</div>");
                             setTimeout(function(){$('.sukses').html('');},1500); 
                           }else{

                            $.ajax({
                              type: "POST",
                              url: url,
                              cache: false,
                              data: {
                                  no_surat: no_surat,
                                  kode_penjualan: kode_penjualan,
                                  kode_meja: kode_meja,
                                  kode_menu: menu,
                                  jumlah: jumlah,
                                  jenis: jenis,
                                  kode_petugas: kode_petugas,
                                  diskon_item: diskon,
                                  harga_satuan: harga,
                                  kode_kasir: kode_kasir,
                                  jenis_diskon: jenis_diskon,
                                  diskon_rupiah: diskon_rupiah
                              },
                             beforeSend:function(){
                              $(".tunggu").show();  
                            },
                            success : function(data) {
                              pesan=data.split("|");
                              $(".tunggu").hide(); 
                              if(pesan[0]==1){
                                $(".sukses").html(pesan[1]);
                                setTimeout(function(){
                                 $(".sukses").html('');
                               },1700);     
                              }else{
                                $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>'+kode_kasir);
                                $("#menu").select2().select2('val', '');
                                $("#qty").val('');
                                $("#diskon_item").val('0');
                                $("#harga").val('');
                                $("#keterangan").val('');
                                $("#barcode_manual").focus();
                                $("#jenis_diskon").html('');
                                $("#jenis_diskon").html('<option value="persen" selected="true">Persen</option><option value="rupiah">Rupiah</option>');
                                totalan();
                                grand_total();
                                cek_status();
                                $("#form_diskon_rupiah").hide();
                                $("#form_diskon_item").show();
                              }


                            },  
                            error : function(data) {  
                              pesan=data.split("|");
                              alert(pesan[1]);  
                            }  
                          });
                         }

                       }

                       function simpan_pesanan_otomatis(){
                        var kode_penjualan = $("#kode_penjualan").val();
                        var tanggal_penjualan = $("#tanggal_penjualan").val();
                        var nomor_nota =$("#nomor_nota").val();
                        var menu = $("#barcode").val();
                        var jumlah = '1';
                        var diskon ='0';
                        var kode_meja = $("#kode_meja").val();
                        var kode_kasir = $("#kode_kasir").val();
                        var harga = $("#harga_barcode_auto").val();
                        var kode_kasir = $("#kode_kasir").val();
                        var jenis = $("#jenis").val();
                        var kode_petugas = $("#kode_petugas").val();
                        var no_surat = $("#no_surat").val();

                        var url = "<?php echo base_url().'kasir/simpan_pesanan_temp'; ?>";

                        $.ajax({
                          type: "POST",
                          url: url,
                          data: {no_surat:no_surat,kode_penjualan:kode_penjualan,kode_meja:kode_meja,kode_menu:menu,jumlah:jumlah,jenis:jenis,kode_petugas:kode_petugas,
                            diskon_item:diskon,harga_satuan:harga,kode_kasir:kode_kasir

                          },
                          success: function(data)
                          {
                            pesan=data.split("|");
                            $(".tunggu").hide(); 
                            if(pesan[0]==1){
                              $(".sukses").html(pesan[1]);
                              setTimeout(function(){
                               $(".sukses").html('');
                             },1700);   
                            }else{
                              $("#tb_pesan_temp").load("<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>"+kode_kasir);
                              $("#nama_produk").val('');
                              $('#nama_produk').select2().trigger('change');
                              $("#qty").val('');
                              $("#diskon_item").val('');
                              totalan();
                              grand_total();
                              cek_status();
                            }
                          }
                        });
                         }

                         function get_harga(){
                          var url = "<?php echo base_url().'kasir/kasir/get_harga'; ?>";
                          var id_menu = $("#menu").val();
                          var qty = $("#qty").val();

                          $.ajax( {
                            type:"POST", 
                            url : url, 
                            cache :false,  
                            data :{id_menu:id_menu,qty:qty},
                            success : function(data) {   
                              $("#harga").val(data);
                            },  
                            error : function(data) {  
                              alert(data);  
                            }  
                          });
                        }
                        function actEdit(id) {
                         var kode_kasir = $("#kode_kasir").val();
                         var id = id;
                         var url = "<?php echo base_url().'kasir/kasir/get_pesanan_temp'; ?>";
                         $.ajax({
                          type: 'POST',
                          url: url,
                          dataType: 'json',
                          data: {id:id},
                          success: function(kasir){
                            $("#menu").val(kasir.kode_menu);
                            $("#qty").val(kasir.jumlah);
                            $("#qty2").val(kasir.jumlah);
                            $("#diskon_item").val(kasir.diskon_item);
                            $("#harga").val(kasir.harga_satuan);
                            $("#keterangan").val(kasir.keterangan);
                            $("#kode_edit_penjualan").val(kasir.kode_penjualan);
                            $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>'+kode_kasir);
                          }
                        });
                         $("#update").show();
                         $("#tambah").hide();
                       }

                       function editData(){

                        var url = "<?php echo base_url().'kasir/kasir/simpan_ubah_pesanan_temp'; ?>";
    //var kode_penjualan = $("#kode_penjualan").val();
    var kode_meja = $("#kode_meja").val();
    var menu = $("#menu").val();
    var jumlah_awal = $("#qty2").val();
    var jumlah = $("#qty").val();
    var harga = $("#harga").val();
    var diskon = $("#diskon_item").val();
    var keterangan = $("#keterangan").val();
    kode_penjualan = $("#kode_edit_penjualan").val();
    $.ajax( {
     type:"POST", 
     url : url,  
     cache :false,  
     data :{kode_penjualan:kode_penjualan,kode_meja:kode_meja,kode_menu:menu,jumlah:jumlah,
      diskon_item:diskon,harga_satuan:harga,jumlah_awal:jumlah_awal,keterangan:keterangan
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success : function(data) {
      $(".tunggu").hide(); 
      $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'.$this->uri->segment(3); ?>');
      $("#menu").val('');
      $("#qty").val('');
      $("#diskon_item").val('0');
      $("#harga").val('');
      $("#keterangan").val('');
      totalan();
      grand_total();
      cek_status();
      
    },  
    error : function(data) {  
      alert(data);  
    }  
  });
    $("#tambah").show();
    $("#update").hide();
  }
  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }
  function delData() {
    var id = $('#id-delete').val();
    var url = '<?php echo base_url().'kasir/kasir/hapus_pesanan_temp'; ?>/delete';
    


    $.ajax({
      type: "POST",
      url: url,
      data: {
        id:id
      },
      success: function(msg) {
        var kode_kasir = $("#kode_kasir").val()
        $('#modal-confirm').modal('hide');
        $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>'+kode_kasir);
        totalan();
        grand_total();
      }
    });
    return false;
  }
  function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
      rev2  += rev[i];
      if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
        rev2 += '.';
      }
    }
    return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
  }
  function ongkos_kirim(){

    var ongkos = $('#ongkos_kirim').val();
    if(ongkos < 0){
      alert('Ongkos Kirim Salah.');
      $('#ongkos_kirim').val('0');
    }else if(ongkos > 0){

      var ongkos = parseInt($('#ongkos_kirim').val());
      var diskon = parseInt($("#rupiah").val());
      var grand_tot=parseInt($('#total2').val());
      var a;
      a= Math.round(grand_tot + ongkos - diskon );
      $('#total_no').val(a);
      $('#grand_total').text(toRp(a));
    }

    if(ongkos == ''){
      var ongkos = parseInt(0);
      
    } else{
      var ongkos = parseInt($('#ongkos_kirim').val());
      var diskon = parseInt($("#rupiah").val());
      if (diskon == ''){
        diskon = 0;
      }
      if (ongkos == ''){
        $('#ongkos_kirim').val('0')
      }
    }
    /*var grand_tot=parseInt($('#total2').val());
    var a;
    a= Math.round(grand_tot + ongkos - diskon );
    $('#total_no').val(a);
    $('#grand_total').text(toRp(a));*/


  }
  function diskon_persen(){
    $("#ongkos_kirim").val('0');
    var no_meja = $("#kode_meja").val();
    var kode_kasir = $("#kode_kasir").val();
    var diskon_persen = $("#persen").val();
    var url = "<?php echo base_url().'kasir/kasir/diskon_persen'; ?>";
    if(parseInt(diskon_persen) < 0 || parseInt(diskon_persen) >100){
      alert('Jumlah Diskon(%) Salah');
      $("#persen").val('0');
      $("#rupiah").val('0');
      $("#ongkos_kirim").val('0');
      diskon_all();
      grand_total();
    }else{
      $.ajax({
        type: 'POST',
        url: url,
        data: {no_meja:no_meja,diskon_persen:diskon_persen,kode_kasir:kode_kasir},
        success: function(total){
          var rupiah = Math.round((diskon_persen/100 )*total);
          $("#rupiah").val(rupiah);
          diskon_all();
          grand_total();
        }
      });
    }

  }
  function diskon_rupiah(){
    $("#ongkos_kirim").val('0');
    var no_meja = $("#kode_meja").val();
    var diskon_rupiah = $("#rupiah").val();
    var kode_kasir = $("#kode_kasir").val();
    var total_pesanan_hidden = $("#total_pesanan_hidden").val();
    var url = "<?php echo base_url().'kasir/kasir/diskon_persen'; ?>";
    if(parseInt(diskon_rupiah) < 0 || parseInt(diskon_rupiah) > parseInt(total_pesanan_hidden)){
      alert('Jumlah Diskon(Rp) Salah');
      $("#persen").val('0');
      $("#rupiah").val('0');
      $("#ongkos_kirim").val('0');
      diskon_all();
      grand_total();
    }else{
      $.ajax({
        type: 'POST',
        url: url,
        data: {no_meja:no_meja,diskon_rupiah:diskon_rupiah,kode_kasir:kode_kasir},
        success: function(total){
            // var persen = Math.round(diskon_rupiah/total*100);
            // $("#persen").val(persen);
            diskon_all();
            grand_total();
          }
        });
    }
  }
  function totalan() {
    var no_meja = $("#kode_meja").val();
    var kode_kasir = $("#kode_kasir").val();
    var url = "<?php echo base_url().'kasir/kasir/get_total_temp'; ?>";
    $.ajax({
      type: 'POST',
      url: url,
      dataType:'json',
      data: {no_meja:no_meja,kode_kasir:kode_kasir},
      success: function(kasir){
        $("#total_pesanan").text(kasir.total);
        $("#total_pesanan_hidden").val(kasir.total2);
        $("#grand_total").text(kasir.total);
        $("#total2").val(kasir.total2);
      }
    });
  }
  function diskon_all(){
    var url = "<?php echo base_url().'kasir/kasir/diskon_all'; ?>";
    var rupiah = $("#rupiah").val();
    $.ajax({
      type: 'POST',
      url: url,
      data: {rupiah:rupiah},
      success: function(rupiah){
        $("#diskon_all").text(rupiah);
      }
    });
  }
  function grand_total(){
   var url = "<?php echo base_url().'kasir/kasir/grand_total'; ?>";
   var rupiah = $("#rupiah").val();
   var kode_kasir = $("#kode_kasir").val();
   var kode_penjualan = $("#kode_penjualan").val();
   $.ajax({
    type: 'POST',
    url: url,
    dataType:'json',
    data: {rupiah:rupiah,kode_penjualan:kode_penjualan,kode_kasir:kode_kasir},
    success: function(rupiah){
      $("#grand_total").text(rupiah.total_grand);
      $("#total_no").val(rupiah.total_no);
    }
  });
 }

 function kembalian(){
  var url = "<?php echo base_url().'kasir/kasir/kembalian'; ?>";
  var dibayar = $("#bayar").val();
  var total = $("#total_no").val();
  var jenis_transaksi = $("#jenis_transaksi").val();
  if (jenis_transaksi == 'kredit' && parseInt(dibayar) >= parseInt(total)) {
    $("#bayar").val('0');
    alert('Pembayaran Kredit Tidak Boleh Melebihi Total, Atau Pilih Pembayaran Tunai.');
  }else if (parseInt(dibayar) < 0) {
    $("#bayar").val('0');
    alert('Pembayaran Tidak Boleh Kurang dari Nol.');
    $("#kembalian").text('Rp 0');
  }else{
    $.ajax({
      type: 'POST',
      url: url,
      dataType:'json',
      data: {total:total,dibayar:dibayar,jenis_transaksi:jenis_transaksi},
      success: function(rupiah){
        $("#kembalian").text(rupiah.kembalian1);
        $("#kembalian2").val(rupiah.kembalian2);
        $("#rupiah_bayar").text(rupiah.dibayar);
      }
    });
  }
}
function bayar(){
  var url = "<?php echo base_url().'kasir/kasir/simpan_pembayaran'; ?>";
  var kode_meja = $("#kode_meja").val();
  var kode_penjualan = $("#kode_penjualan").val();
  var total_pesanan = $("#total2").val();
  var persen = $("#persen").val();
  var rupiah = $("#rupiah").val();
  var grand_total = $("#total_no").val();
  var jenis_transaksi = $("#jenis_transaksi").val();
  var kembalian = $("#kembalian2").val();
  var bayar = $("#bayar").val();
  var kode_member = $("#kode_member").val();
  var kode_member_sales = $("#kode_member_sales").val();
  var nama_member = $("#nama_member").val();
  var kode_penjualan_baru = $("#kode_penjualan_baru").val();
  var waiter = $("#waiter").val();
  var jenis_penerimaan = $("#jenis_penerimaan").val();
  var penerima = $("#penerima").val();
  var no_telp = $("#no_telp").val();
  var alamat = $("#alamat").val();
  var jam_kirim = $("#jam_kirim").val();
  var tgl_kirim = $("#tgl_kirim").val();
  var ongkos = $("#ongkos_kirim").val();
  var jenis = $("#jenis").val();
  var kode_petugas = $("#kode_petugas").val();
  var nama_petugas = $("#nama_petugas").val();
  var kode_sopir = $("#kode_sopir").val();
  var nama_sopir = $("#nama_sopir").val();
  var jatuh_tempo = $("#jatuh_tempo").val();
  var note = $("#note").val();
  var no_surat = $("#no_surat").val();
  if(bayar==""){
    $("#modal-confirm-bayar").modal('hide');
    $("#modal-confirm-bayar").modal('hide');
    alert('Belum Melakukan Pembayaran.');
  }else{
    if (jenis_transaksi == 'tunai' && bayar < grand_total) {
      $("#modal-confirm-bayar").modal('hide');
      alert('Pembayaran Tunai Tidak Boleh Kurang, Atau Pilih Kredit.');
    }else if(jenis_transaksi == 'kredit' && bayar >= grand_total) {
     $("#modal-confirm-bayar").modal('hide');
     alert('Pembayaran Kredit Tidak Boleh Melebihi Total, Atau Pilih Pembayaran Tunai.');
   }else{
    if(nama_member=="" && jenis_transaksi=="kredit"){
      $(".sukses").html('<div class="alert alert-warning">Pembayaran kredit hanya digunakan untuk member</div>');
    }else{

              $.ajax({
                  type: 'POST',
                  url: url,
                  data: {
                      no_surat: no_surat,
                      kode_meja: kode_meja,
                      kode_penjualan: kode_penjualan,
                      bayar: bayar,
                      kembalian: kembalian,
                      total_pesanan: total_pesanan,
                      persen: persen,
                      rupiah: rupiah,
                      grand_total: grand_total,
                      jenis_transaksi: jenis_transaksi,
                      kode_member: kode_member,
                      nama_member: nama_member,
                      waiter: waiter,
                      kode_penjualan_baru: kode_penjualan_baru,
                      jenis_penerimaan: jenis_penerimaan,
                      penerima: penerima,
                      no_telp: no_telp,
                      alamat: alamat,
                      jenis: jenis,
                      kode_petugas: kode_petugas,
                      nama_petugas: nama_petugas,
                      kode_sopir: kode_sopir,
                      nama_sopir: nama_sopir,
                      jatuh_tempo: jatuh_tempo,
                      note: note,
                      jam_kirim: jam_kirim,
                      tgl_kirim: tgl_kirim,
                      ongkos: ongkos,
                      kode_member_sales: kode_member_sales
                  },
                  success: function(hasil) {
                      $("#modal-confirm-bayar").modal('hide');

                      $(".sukses").html('<div style="font-size:1.5em" class="alert alert-success">Pembayaran Produk Berhasil</div>');
                      var link = "<?php echo base_url('kasir/kasir/cetak_pembayaran'); ?>";

                      $.ajax({
                          type: "POST",
                          url: link,
                          data: {
                              kode_meja: kode_meja,
                              kode_penjualan_baru: kode_penjualan_baru,
                              jenis_transaksi: jenis_transaksi,
                              ongkos: ongkos
                          },
                          success: function(msg) {}
                      });
                      setTimeout(function() {
                          $('.sukses').html('');
                          window.location = "<?php echo base_url() . 'kasir/' ?>";
                          window.open("<?php echo base_url() . 'kasir/print_penjualan/' ?>" + kode_penjualan);
                      }, 1000);
                  }
              });
                         }
                       }
                     }

                   }


                   function simpan_pindah_meja(){
                    var url = "<?php echo base_url().'kasir/kasir/pindah_meja'; ?>";
                    var meja_asal = $("#meja_asal").val();
                    var meja_akhir = $("#meja_akhir").val();
                    $.ajax({
                      type: 'POST',
                      url: url,
                      data: {meja_asal:meja_asal,meja_akhir:meja_akhir},
                      success: function(data){
                        $(".sukses").html('<div class="alert alert-success">Berhasil Pindah Meja</div>');
                        setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'kasir/menu_kasir/' ?>"+meja_akhir;},1000);  
                      }
                    });
                  }


                  function cek_status(){
                    var kode_meja = $("#id_meja").val();
                    var url = "<?php echo base_url().'kasir/kasir/cek_status'; ?>";
                    $.ajax({
                      type: 'POST',
                      url: url,
                      data: {kode_meja:kode_meja},
                      success: function(hasil){
                        var data = $("#hasil_meja").val(hasil);
                        if($("#hasil_meja").val(hasil)=="aktif"){
                //alert("oke");
                $("#buka").show();
                $("#tutup").hide();
              }else{
               // alert("jos");
               $("#buka").hide();
               $("#tutup").show();
             }
           }
         });
                  }

                  function print() {
                    var url = "<?php echo base_url('kasir/kasir/cetak_bill'); ?>";
                    var kode_meja = $("#kode_meja").val();
                    $.ajax({
                     type: "POST",
                     url: url,
                     data:{
                      kode_meja:kode_meja
                    },
                    success: function(msg)
                    {
           //alert(msg);
           
         }
       });
                  }
                  function get_no_surat() {
                    var url = "<?php echo base_url('kasir/get_no_surat'); ?>";
                    var no_surat = $("#no_surat").val();
                    var jenis = $("#jenis").val();
                    $.ajax({
                     type: "POST",
                     url: url,
                     data:{
                      no_surat:no_surat,jenis:jenis
                    },
                    dataType:'json',  
                    success: function(msg)
                    {
                     $("#nama_petugas").val(msg.nama_unit_tujuan);
                     $("#kode_petugas").val(msg.kode_unit_tujuan);
                     $("#kode_sopir").val(msg.kode_sopir);
                     $("#nama_sopir").val(msg.nama_sopir);
                     get_produk_petugas();
                   }
                 });
                  }

                  function cetak_pesanan() {
                    var url = "<?php echo base_url('kasir/kasir/cetak_pesanan'); ?>";
                    var kode_meja = $("#kode_meja").val();
                    $.ajax({
                     type: "POST",
                     url: url,
                     data:{
                      kode_meja:kode_meja
                    },
                    success: function(msg)
                    {
           //alert(msg);
           
         }
       });
                  }

/*function diskon_per_item(){
    var harga = $("#harga").val();
    var diskon = $("#diskon_item").val();
    var url = "<?php #echo base_url().'kasir/diskon_per_item'; ?>";
    $.ajax( {
       type:"POST", 
        url : url,  
        cache :false,  
        data :{harga:harga,diskon:diskon},
        beforeSend: function(){
         $(".loading").show(); 
       },
        beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) {
            
           
            
        },  
      error : function(data) {  
        alert(data);  
      }  
    });
                         }*/
                         function tutup_kasir(){
                          var kasir = $("#kasir").val();
                          var url = "<?php echo base_url().'kasir/kasir/tutup_kasir'; ?>";
                          $.ajax( {
                           type:"POST", 
                           url : url,  
                           cache :false,  
                           data :{kasir:kasir},
                           beforeSend:function(){
                            $(".tunggu").show();  
                          },
                          success : function(data) {  
                            $(".tunggu").hide(); 
                            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'kasir/kasir/tutup_kasir/' ?>"+kasir;},1000);
                          },  
                          error : function(data) {  
                            alert(data);  
                          }  
                        });
                        }
                        function get_member(){
                          var kode_member = $("#kode_member").val();
                          var kode_member_sales = $("#kode_member_sales").val();
                          var jenis = $("#jenis").val();
                          var url = "<?php echo base_url().'kasir/kasir/get_member'; ?>"
                          $.ajax( {
                           type:"POST", 
                           url : url,  
                           cache :false,
                           dataType:'json',  
                           data :{kode_member:kode_member,kode_member_sales:kode_member_sales,jenis:jenis},
                           success : function(data) {  
    //  $(".tunggu").hide(); 
    $("#nama_member").val(data.nama_member);
    $("#nama_member_sales").val(data.nama_member);
    $("#member").text(data.nama_member);

    $("#kat_member").val(data.kategori_member);

    
    if(data.kategori_member=='konsinyasi'){
      $("#jenis_transaksi").html("<option value='konsinyasi' selected>Konsinyasi</option>");
      $("#div_jatuh_tempo").hide();
      $("#ongkos_kirim").prop('readonly', true);
      $("#bayar").val('0');
      $("#bayar").prop('readonly', true);
      $("#div_note").hide();
    }else if(data.kategori_member=='member'){
      $("#jenis_transaksi").html("<option value='kredit' selected>kredit</option><option value='tunai'>Tunai</option><option value='konsinyasi'>Konsinyasi</option>");
      $("#div_jatuh_tempo").show();
      $("#div_note").show();
      $("#text_dibayar").html('Hutang&nbsp;&nbsp;');
      $("#ongkos_kirim").prop('readonly', false);
      $("#bayar").prop('readonly', false);
    }else{
      $("#jenis_transaksi").html("<option value='konsinyasi' >Konsinyasi</option><option value='tunai'>Tunai</option><option value='kredit'>Kredit</option>");
      $("#div_jatuh_tempo").hide();
      $("#div_note").hide();
      $("#text_dibayar").html('Kembalian&nbsp;&nbsp;');
      $("#ongkos_kirim").prop('readonly', false);
      $("#bayar").prop('readonly', false);
    }
  },  
  error : function(data) {  
    alert(data);  
  }  
});
                         }

                         function cek_jenis(){
  // var jenis=$('#jenis').val();
  // if (jenis=='member'){
  //   $("#kode_member").prop('disabled', false);
  // }else{
  //   $("#kode_member").prop('disabled', true);
  //   $("#kode_member").select2("val", "");
  //   $("#nama_member").val('');

  // }
  $('.inputClear').val('');

  var jenis=$('#jenis').val();
  var jenis_t = "<?php echo base_url().'kasir/get_jenis'?>";
  if(jenis=='member'){
    $("#kode_member").prop('disabled', false);
    $('.jenis_sales').hide();
    $('.jenis_member').show();
    $('.jenis_sales_sopir').hide();
  }else if(jenis=='sales' || jenis=='sopir'){
    $("#kode_member").prop('disabled', true);
    $('.jenis_sales').show();
    $('.jenis_member').hide();
    $('.jenis_sales_sopir').hide();

  }else if(jenis=='sales & sopir'){
    $("#kode_member").prop('disabled', true);
    $('.jenis_sales').show();
    $('.jenis_sales_sopir').show();
    $('.jenis_member').hide();
    $('#nama_member_sales').hide();
    
  }

}
function get_produk_petugas(){
  var jenis = $("#jenis").val();
  var kode_petugas = $("#kode_petugas").val();
  var url = "<?php echo base_url().'kasir/get_produk_petugas'; ?>";
  $.ajax( {
   type:"POST", 
   url : url,  
   cache :false,  
   data :{jenis:jenis,kode_petugas:kode_petugas},

   success : function(data) {  
    $("#menu").html(data);
  }
});
}
function get_kategori_harga(){
 var url = "<?php echo base_url().'kasir/get_kategori_harga'; ?>";
 var qty = $("#qty").val();
 var id_menu = $("#menu").val();

 
 var jenis=$('#jenis').val();

 if(jenis=='member'){
  var kode_member = $("#kode_member").val();
}else{
 var kode_member = $("#kode_member_sales").val();
}
$.ajax( {
 type:"POST", 
 url : url,  
 cache :false,
 data :{id_menu:id_menu,qty:qty,kode_member:kode_member},
 dataType : 'json',
 success : function(data) {
  if(data==''){
   $("#harga").val('0');
 }else{
   $("#harga").val(data.harga);
 }
},  

});
}
$(document).ready(function(){
 //$("#kode_member").prop('disabled', true);
    //diskon_per_item();
    totalan();
    grand_total();
    cek_status();
    var kode_kasir = $("#kode_kasir").val();
    $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>'+kode_kasir);
    $("#update").hide();
    $(".select2").select2();
    
    
    $("#cari_to").click(function(){
      var kode_kasir = $("#kode_kasir").val();
      var kode_transaksi = $("#nota_to").val();
      var kode_penjualan = $("#kode_penjualan").val();
      var url = "<?php echo base_url().'kasir/kasir/get_taking_order/'; ?>"+kode_kasir;
      $.ajax( {
       type:"POST", 
       url : url,  
       cache :false,
       dataType:'json',  
       data :{kode_transaksi:kode_transaksi,kode_kasir:kode_kasir,kode_penjualan:kode_penjualan},
       
       beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {  
        $(".tunggu").hide();
        if(data.status_penerimaan=="dikirim"){
          $("#data_pengiriman").fadeIn(1000);
        }else{
          $("#data_pengiriman").hide();
        }
        $("#modal-taking-order").modal('hide'); 
        $("#tb_pesan_temp").load('<?php echo base_url().'kasir/kasir/pesanan_temp/'; ?>'+kode_kasir);
        totalan();
        grand_total();
        cek_status();
        $("#penerima").val(data.nama_penerima);
        $("#no_telp").val(data.no_telp);
        $("#alamat").val(data.alamat);
        $("#jam_kirim").val(data.waktu_pengiriman);
        $("#tgl_kirim").val(data.tanggal_pengiriman);
        $("#jenis_penerimaan").val(data.status_penerimaan);         
      },  
      error : function(data) {  
        alert(data);  
      }  
    });

                         });


          /*    $('#barcode').on('keydown',function(event) {

               if (event.which == 17) {
                event.preventDefault();
               //alert('asdasd');
               $('#col_auto').hide();
               $('#col_manual').show();
               $('#barcode_manual').focus();

             }
           });
              $('#barcode_manual').on('keydown',function(event) {

               if (event.which == 17) {
                event.preventDefault();
               //alert('asdasd');
               $('#col_auto').show();
               $('#col_manual').hide();
               $('#barcode').focus();

             }
           }); 
       
       
      $('#barcode').on('keydown', function(event) {

               if (event.which == 13) {
                event.preventDefault();
                var url = "<?php #echo base_url().'kasir/get_harga'; ?>";
                var id_menu = $("#barcode").val();
                $.ajax( {
                 type:"POST", 
                 url : url,  
                 cache :false,
                 dataType: "json",  
                 data :{id_menu:id_menu},
                 beforeSend: function(){
                   $(".loading").show(); 
                 },
                 success : function(data) {  
                 
                 $("#harga_barcode_auto").val(data);

                  
                  simpan_pesanan_otomatis();
                  $("#barcode").val('');
                  $("#barcode").focus();
                  $(".loading").hide(); 
                },  
                error : function(data) {  
                  alert(data);  
                }  
              });
              } else if (event.which == 66) {
                event.preventDefault();
                $("#bayar").focus();
              }
            }); */

                           $('#barcode_manual').on('keydown', function(event) {

                             if (event.which == 13) {
                               var cek = $("#barcode_manual").val();
                               if(cek==""){
                                event.preventDefault();
                                $("#persen").focus();

                              }else{
                               event.preventDefault();
                               var url = "<?php echo base_url().'kasir/get_produk_manual'; ?>";
                               var id_menu = $("#barcode_manual").val();
                               $.ajax( {
                                 type:"POST", 
                                 url : url,  
                                 cache :false,  
                                 dataType : "json",
                                 data :{id_menu:id_menu},

                                 success : function(data) {
                                  $(".loading").hide(); 
                                  $("#qty").focus();
                                  $("#menu").select2().select2('val',data.kode_produk);
                                  $("#barcode_manual").val('');
                                  $("#harga").val(data.harga_jual);
                          // simpan_pesanan_manual();
                        },  
                        error : function(data) {  
                          alert(data);  
                        }  
                      });
                             }                                 

                           }else if (event.which == 66) {
                            event.preventDefault();
                            $("#bayar").focus();
                          }
                        }); 


                           $("#barcode_manual").change(function(){
                            var url = "<?php echo base_url().'kasir/get_satuan_stok'; ?>";
                            var id_menu = $("#barcode_manual").val();
                            $.ajax( {
                             type:"POST", 
                             url : url,  
                             cache :false,
                             data :{id_menu:id_menu},

                             success : function(data) {
                               $("#satuan_stok").html('');
                               $("#satuan_stok").html(data);
                               $("#satuan_stok").focus();
                             },  
                             error : function(data) {  
                              alert(data);  
                            }  
                          });
                          })

                           $("#menu").change(function(){
                            var url = "<?php echo base_url().'kasir/get_satuan_stok'; ?>";
                            var id_menu = $("#menu").val();
                            $.ajax( {
                             type:"POST", 
                             url : url,  
                             cache :false,
                             data :{id_menu:id_menu},
                             dataType : 'json',
                             success : function(data) {
                 //$("#harga").val(data.harga_jual);
                 get_kategori_harga();
               },  
               error : function(data) {  
                alert(data);  
              }  
            });
                          })

                           $('#menu').on('keydown', function(event) {

                             if (event.which == 13) {
               // alert("siap");
               $("#satuan_stok").focus();
             }else if(event.which == 27){
              // alert("siap");
              $("#persen").focus();
            }
          });

                           $('#satuan_stok').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                $("#qty").focus();
              }
            });

                           $('#qty').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                $("#diskon_item").focus();
              }
            });

                           $('#diskon_item').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                simpan_pesanan_temp();
                $("#menu").focus();
              }
            });

                           $('#persen').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#kode_member").focus();
              }
            });

                           $('#rupiah').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#kode_member").focus();
              }
            });

                           $('#kode_member').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#jenis_transaksi").focus();
              }
            });

                           $('#jenis_penerimaan').on('keydown', function(event) {
                            var penerimaan = $("#jenis_penerimaan").val();
                            if (event.which == 13) {
                //alert("siap");
                if(penerimaan=="dikirim"){
                  $("#penerima").focus();
                }else{
                  $("#ongkos_kirim").focus();
                } 
                
              }
            });

                           $("#jenis_penerimaan").change(function(){
       // alert("oke");
       var penerimaan = $("#jenis_penerimaan").val();
       if(penerimaan=="dikirim"){
        $("#data_pengiriman").fadeIn(500);
      }else{
        $("#data_pengiriman").fadeOut(500);
        $("#penerima").val('');
        $("#no_telp").val('');
        $("#alamat").val('');
        $("#jam_kirim").val('');
        $("#tgl_kirim").val('');
      } 
    });

                           $('#penerima').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#no_telp").focus();
              }
            });

                           $('#no_telp').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#alamat").focus();
              }
            });

                           $('#alamat').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#jam_kirim").focus();
              }
            });

                           $('#jam_kirim').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#tgl_kirim").focus();
              }
            });

                           $('#tgl_kirim').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#bayar").focus();
              }
            });

                           $('#ongkos_kirim').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#bayar").focus();
              }
            });

                           $('#jenis_transaksi').on('keydown', function(event) {

                             if (event.which == 13) {
                //alert("siap");
                
                $("#jenis_penerimaan").focus();
              }
            });

                           $('#bayar').on('keydown', function(event) {

                             if (event.which == 13) {
               // alert("siap");
               $("#modal-confirm-bayar").modal('show');

             }
           });



   /*  $('#no').on('keydown', function(event) {

             if (event.which == 84) {
               // alert("siap");
              $("#modal-confirm-bayar").modal('hide');
            }
          });

     $('#ya').on('keydown', function(event) {

             if (event.which == 89) {
               // alert("siap");
             bayar();
            }
          }); */

  /*  $('#bayar').on('keydown', function(event) {

             if (event.which == 13) {
                //alert("siap");
             
               bayar();
            }
          });*/


                         });
                         </script>
