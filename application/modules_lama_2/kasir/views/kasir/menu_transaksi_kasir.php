
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    
                        <!-- END STYLE CUSTOMIZER -->
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                          Transaksi Kasir</h3>
                          <div class="page-bar">
                            <ul class="page-breadcrumb">
                              <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Home</a>
                                <i class="fa fa-angle-right"></i>
                              </li>
                              <li>
                                <a href="#">Transaksi Kasir</a>
                              </li>
                            </ul>
                            <div class="page-toolbar">
                              <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>
                                &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light blue-soft" id="kasir_transaksi">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-shopping-cart" ></i>
                                </div>
                                <div class="details" >
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Kasir
                                  </div>
                                </div>
                              </a>
                            </div>
                           
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light green-soft"  id="retur">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-retweet"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Retur Penjualan
                                  </div>
                                </div>
                              </a>
                            </div>
                            
                            </div>
                          <!-- END DASHBOARD STATS -->
                          <div class="clearfix">
                          </div>

                        </div>
                        <script type="text/javascript">
                          $(document).ready(function(){


                            $("#kasir_transaksi").click(function(){
                              window.location = "<?php echo base_url().'kasir' ?>";
                            });

                            $("#retur").click(function(){
                              window.location = "<?php echo base_url() . 'retur_penjualan/' ?>";
                            });

                           

                          });
                        </script>
