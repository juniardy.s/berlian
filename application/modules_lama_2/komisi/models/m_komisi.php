<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_komisi extends CI_Model {
	public function hitung_total_komisi($kode) {
		$total = 0;
		$get_komisi = $this->db->get_where('transaksi_komisi', array('kode_penerima_komisi' => $kode))->result();
		foreach (@$get_komisi as $item) {
			if ($item->status_komisi == 'masuk') {
				$total += $item->total_komisi;
			} else if ($item->status_komisi == 'withdraw') {
				$total -= $item->total_komisi;
			}
		}
		return ($total >= 0)? $total : 0;
	}

	public function hitung_jumlah_barang($kode) {
		$total = 0;
		$get_komisi = $this->db->get_where('transaksi_komisi', array('kode_penerima_komisi' => $kode))->result();
		foreach (@$get_komisi as $item) {
			if ($item->status_komisi == 'masuk') {
				$total += $item->total_barang_komisi;
			}
		}
		return ($total >= 0)? $total : 0;
	}

	public function total_withdraw($kode) {
		$total = 0;
		$get_komisi = $this->db->get_where('transaksi_komisi', array('kode_penerima_komisi' => $kode))->result();
		foreach (@$get_komisi as $item) {
			if ($item->status_komisi == 'withdraw') {
				$total -= $item->total_komisi;
			}
		}
		return ($total >= 0)? $total : 0;
	}

	public function total_masuk($kode) {
		$total = 0;
		$get_komisi = $this->db->get_where('transaksi_komisi', array('kode_penerima_komisi' => $kode))->result();
		foreach (@$get_komisi as $item) {
			if ($item->status_komisi == 'masuk') {
				$total += $item->total_komisi;
			}
		}
		return ($total >= 0)? $total : 0;
	}
}