

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">             
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Detail Komisi <?php echo $petugas->nama ?>
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row" >
            <form>
              <div class="form-group  col-xs-6">
                <label class="gedhi">Kode</label>
                <input readonly="" type="text" class="form-control" value="<?php echo $petugas->kode ?>" name="kode_petugas"/>
              </div>

              <div class="form-group  col-xs-6">
                <label class="gedhi">Nama</label>
                <input readonly="" type="text" class="form-control" value="<?php echo $petugas->nama ?>" name="nama_petugas"/>
              </div>
              <div class="form-group  col-xs-3">
                <label class="gedhi">Total Withdraw</label>
                <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($this->m_komisi->total_withdraw($petugas->kode)) ?>"/>
              </div>
              <div class="form-group  col-xs-3">
                <label class="gedhi">Total Pendapatan</label>
                <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($this->m_komisi->total_masuk($petugas->kode)) ?>"/>
              </div>
               <div class="form-group  col-xs-6">
                <label class="gedhi">Sisa</label>
                <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($this->m_komisi->hitung_total_komisi($petugas->kode)) ?>"/>
              </div>
            </form>
          </div>

          <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                             <thead>
                              <tr>
                                <th>Tanggal</th>
                                <th>Asal</th>
                                <th>Jumlah</th>
                                <th>Komisi</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php foreach($riwayat_komisi as $daftar){ ?> 
                                  <?php if ($daftar->status_komisi == 'masuk') { ?>
                                    <tr>
                                      <td><?php echo TanggalIndo(substr($daftar->tanggal_komisi, 0, 10)); ?></td>
                                      <td><?php echo strtoupper($daftar->asal_komisi); ?></td>
                                      <td><?php echo $daftar->total_barang_komisi; ?></td>
                                      <td><?php echo format_rupiah($daftar->total_komisi); ?></td>
                                      <td align="center"><a href="javascript:;" data-toggle="tooltip" data-kode="<?php echo $daftar->id ?>" title="Detail" class="btn btn-icon-only btn-circle blue btn-detail"><i class="fa fa-search"></i></a></td>
                                    </tr>
                                  <?php } else if ($daftar->status_komisi == 'withdraw') { ?>
                                    <tr style="background-color: yellow;">
                                      <td><?php echo TanggalIndo(substr($daftar->tanggal_komisi, 0, 10)); ?></td>
                                      <td colspan="2">Withdraw</td>
                                      <td colspan="2"><?php echo "- ".format_rupiah($daftar->total_komisi); ?></td>
                                    </tr>
                                  <?php } ?>
                                <?php } ?>
                            </tbody>
                           </table>


         </div>

       </div>
     </div>
            
            
                <div class="box box-info">
                    
                    
                    <div class="box-body">            
                        
                        

            </section><!-- /.Left col -->      
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:grey">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="color:#fff;">Detail Riwayat Komisi</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="background-color:#eee">
                <button class="btn red" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
$(".tunggu").show();
            window.location = "<?php echo base_url().'komisi/'; ?>";
          });
        </script>


<script>
$(document).ready(function(){
  $("#tabel_daftar").dataTable();

  $(document).on('click', '.btn-detail', function(){
    $kode = $(this).data('kode');
    $.get("<?php echo base_url('komisi/get_detail_riwayat_komisi') ?>/"+$kode, function( data ) {
      $('#modal-detail .modal-body').html(data);
      $('#modal-detail').modal('show');
    });
  });

  $("#modal-detail").on('hidden.bs.modal', function(){
      $('#modal-detail .modal-body').html('');
  });
})
   
</script>