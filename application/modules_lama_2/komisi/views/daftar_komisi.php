

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">             
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Komisi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <div class="box-body">            
          <div class="sukses" ></div>
          <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                             <thead>
                              <tr>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Jumlah</th>
                                <th>Komisi</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php foreach($daftar_orang as $daftar){ ?> 
                                    <tr>
                                      <td><?php echo $daftar->kode; ?></td>
                                      <td><?php echo $daftar->nama; ?></td>
                                      <td><?php echo $this->m_komisi->hitung_jumlah_barang($daftar->kode); ?></td>
                                      <td><?php echo format_rupiah($this->m_komisi->hitung_total_komisi($daftar->kode)); ?></td>
                                      <td align="center"><?php echo get_detail_komisi($daftar->kode); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                           </table>


         </div>

       </div>
     </div>
            
            
                <div class="box box-info">
                    
                    
                    <div class="box-body">            
                        
                        

            </section><!-- /.Left col -->      
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
$(".tunggu").show();
            window.location = "<?php echo base_url().'penggajian/'; ?>";
          });
        </script>


<script>
$(document).ready(function(){
  $("#tabel_daftar").dataTable();
})
   
</script>