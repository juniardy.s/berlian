 <script src="https://public.azurewebsites.net/js/jquery.table2excel.js"></script>
 <div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Omset
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
        <?php

        $get_pemasukan = $this->db->get('keuangan_keluar');
        $hasil_pemasukan = $get_pemasukan->result();
        ?>

        <div class="box-body">            
          <div class="sukses" ></div>
          <form id="pencarian_form" method="post" style="margin-left: 18px;" class="form-horizontal" target="_blank">
           <div class="row">
            <div class="col-md-5" id="">
              <div class="input-group">
                <span class="input-group-addon">Bulan</span>
                <select class="form-control" name="bulan" id="bulan" required="">              
                  <option value="">
                    -- Bulan --
                  </option>
                  <?php
                  for ($i=1; $i <= 12 ; $i++) { 
                    ?>
                    <option value="<?php if(strlen($i)==1) {
                      echo "0".$i;   
                    }else{
                      echo $i;
                    }  ?>">
                    <?php 
                    if($i==1){
                      echo "Januari";
                    }elseif ($i==2) {
                      echo "Februari";
                    }elseif ($i==3) {
                      echo "Maret";
                    }elseif ($i==4) {
                      echo "April";
                    }elseif ($i==5) {
                      echo "Mei";
                    }elseif ($i==6) {
                      echo "Juni";
                    }elseif ($i==7) {
                      echo "Juli";
                    }elseif ($i==8) {
                      echo "Agustus";
                    }elseif ($i==9) {
                      echo "September";
                    }elseif ($i==10) {
                      echo "Oktober";
                    }elseif ($i==11) {
                      echo "November";
                    }elseif ($i==12) {
                      echo "Desember";
                    }

                    ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>

          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tahun</span>
              <select class="form-control" name="tahun" id="tahun" required="">              
                <option value="">
                  -- Tahun --
                </option>
                <?php
                $tanggal_sekarang = date('Y');
                $date = $tanggal_sekarang-10;
                for ($i=$date; $i <= $tanggal_sekarang ; $i++) { 
                  ?>
                  <option value="<?php echo $i ?>">
                    <?php echo $i ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>                        
          <div class="col-md-2 pull-left">
           <a class="btn btn-success" onclick="cari_omset()"><i class="fa fa-search"></i> Cari</a>
         </div>
       </div>






       <br>


     </form>
     <div class="row">
       <div class="col-md-1 pull-right">
         <button style="" type="button" class="btn btn-info pull-right" id="cetak"><i class="fa fa-print"></i> Print</button>
       </div>
     </div><br>
     <div id="omset">
      <?php

      $tahun = date("Y");
      $bulan_sebelum_sekarang = date("m")-1;

      // echo($bulan_sebelum_sekarang);
      $this->db->where('tanggal_penjualan >=', $tahun."-01-01");
      $this->db->where('tanggal_penjualan <=', $tahun."-".$bulan_sebelum_sekarang."-31");
      // $this->db->where('tanggal_penjualan >=', "2017-01-01");
      // $this->db->where('tanggal_penjualan <=', '2017-09-31');

      $this->db->select_sum('grand_total');
      $this->db->group_by('tanggal_penjualan');
      $total_omset = $this->db->get('transaksi_penjualan');
      $hasil_total = $total_omset->result();
      
      $totalnya = 0;
#echo $this->db->last_query();
      foreach($hasil_total as $totalan){
        $totalnya += $totalan->grand_total;   

      }                                                                                
      ?>                            
      <label style="font-size: x-large;"><strong>Total Omset : <?php echo format_rupiah($totalnya); ?></strong></label>

      <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Total Transaksi</th>
            <th>Omset</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $this->db->group_by('tanggal_penjualan');
          $this->db->like('tanggal_penjualan', date("Y-m"));
          $omset = $this->db->get('transaksi_penjualan');


          $hasil_omset = $omset->result();
          $nomor = 1;

          foreach($hasil_omset as $daftar){ ?> 
          <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo TanggalIndo(@$daftar->tanggal_penjualan);?></td>
            <?php
            $this->db->group_by('kode_penjualan');
            $total_trx = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
            $hasil_total_trx = $total_trx->result();
            ?>                                                                                                                 
            <td><?php echo count($hasil_total_trx); ?></td>

            <?php

            $this->db->select_sum('grand_total');
            $this->db->select('kode_penjualan');
            $this->db->group_by('kode_penjualan');
            $total_omset = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$daftar->tanggal_penjualan));
#echo $this->db->last_query();
            $hasil_total = $total_omset->result();
            $uang_omset = 0;
            foreach($hasil_total as $daftar){
              $uang_omset += $daftar->grand_total;
            }                                                                                
            ?>                                                                                                                  
            <td><?php echo format_rupiah(@$uang_omset); ?></td>
          </tr>
          <?php $nomor++; } ?>

        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Total Transaksi</th>
            <th>Omset</th>
          </tr>
        </tfoot>
      </table>
    </div>


  </div>

  <!------------------------------------------------------------------------------------------------------>

</div>
</div>
</div><!-- /.col -->
</div>
</div>    
</div>  
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'analisa/menu_data'; ?>";
  });
</script>

<script>
 $("#cetak").click(function(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  window.open("<?php echo base_url().'omset/cetak_laporan_omset/'; ?>"+bulan+"/"+tahun);

})
 jQuery(document).ready(function() {
  $("#export").click(function(){
    $("#tabel_daftar").table2excel();
  });
});
 function cari_omset(){
  var bulan =$("#bulan").val();
  var tahun =$("#tahun").val();
  var petugas = $("#nama_petugas").val();
  $.ajax( {  
    type :"post",  
    url : "<?php echo base_url().'omset/search_omset'; ?>",  
    cache :false,
    data : {bulan:bulan,tahun:tahun,petugas:petugas},
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success : function(data) {
     $("#omset").html(data);
     $(".tunggu").hide();  
   },  
   error : function(data) {  
    alert("error");
  }  
});
  $('#cetak').show();
  $('#export').show();
}


$(document).ready(function(){
  $('#tabel_daftar').dataTable({
    "paging":   false,
    "searching":   false,
    "info":     false
  });

  $('.tgl').Zebra_DatePicker({});

})

</script>

