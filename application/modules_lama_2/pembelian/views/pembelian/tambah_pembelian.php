<div class="row">      
  <style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'pembelian/daftar_pembelian'; ?>";
  });
</script>
<div class="col-xs-12">
  <!-- /.box -->
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        Pembelian

      </div>
      <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>
    </div>
    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url() . 'pembelian/tambah' ?>"><i class="fa fa-edit"></i> Tambah </a>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app blue" href="<?php echo base_url() . 'pembelian/daftar_pembelian' ?>"><i class="fa fa-list"></i> Daftar </a> 
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green pull-right" href="<?php echo base_url() . 'pembelian/tambah_supplier' ?>"><i class="fa fa-edit"></i> Tambah Supplier </a> 

      <?php
      $param = $this->uri->segment(4);
      if(!empty($param)){
        $bahan_baku = $this->db->get_where('master_bahan_baku',array('id'=>$param));
        $hasil_bahan_baku = $bahan_baku->row();
      }    

      ?>
      <div class="box-body">                   
        <div class="sukses" ></div>
        <form id="data_form" action="" method="post">
          <div class="box-body">
            <div class="notif_nota" ></div>
            <label><h3><b>Transaksi Pembelian</b></h3></label>
            <hr>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Kode Transaksi</label>
                  <?php
                  $tgl = date("Y-m-d");
                  $no_belakang = 0;
                  $this->db->select_max('kode_pembelian');
                  $kode = $this->db->get_where('transaksi_pembelian',array('tanggal_pembelian'=>$tgl));
                  $hasil_kode = $kode->row();
#$pecah_kode = explode("_",$hasil_kode_pembelian->kode_pembelian);
#echo $pecah_kode[0];
#echo $pecah_kode[2];
                  $this->db->select('kode_pembelian');
                  $kode_pembelian = $this->db->get('master_setting');
                  $hasil_kode_pembelian = $kode_pembelian->row();

                  if(count($hasil_kode)==0){
                    $no_belakang = 1;
                  }
                  else{
                    $pecah_kode = explode("_",$hasil_kode->kode_pembelian);
                    $no_belakang = @$pecah_kode[2]+1;
                  }

                  $user = $this->session->userdata('astrosession');
                  $id_user=$user->id;

                  $this->db->select_max('id');
                  $get_max_po = $this->db->get('transaksi_pembelian');
                  $max_po = $get_max_po->row();

                  $this->db->where('id', $max_po->id);
                  $get_po = $this->db->get('transaksi_pembelian');
                  $po = $get_po->row();
                  $tahun = substr(@$po->kode_pembelian, 4,4);
                  if(date('Y')==$tahun){
                    $nomor = substr(@$po->kode_pembelian, 12);

                    $nomor = $nomor + 1;
                    $string = strlen($nomor);
                    if($string == 1){
                      $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                    } else if($string == 2){
                      $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                    } else if($string == 3){
                      $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_000'.$nomor;
                    } else if($string == 4){
                      $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_00'.$nomor;
                    } else if($string == 5){
                      $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_0'.$nomor;
                    } else if($string == 6){
                      $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_'.$nomor;
                    }
                  } else {
                    $kode_trans = 'PEM_'.date('Y').'_'.$id_user.'_000001';
                  }

                  ?>
                  <?php @$hasil_kode_pembelian->kode_pembelian."_".date("dmyHis")."_".$no_belakang ?>
                  <input readonly="true" type="text" value="<?php echo $kode_trans ?>" class="form-control" placeholder="Kode Transaksi" name="kode_pembelian" id="kode_pembelian" />
                  <input type="hidden" id="hasil_kode_po" name="kode_po2" >
                </div>

                <div class="form-group">
                  <label>Supplier</label>
                  <?php
                  $supplier = $this->db->get_where('master_supplier',array('status_supplier' => '1'));
                  $supplier = $supplier->result();
                  ?>
                  <select disabled="" required class="form-control select2" name="kode_supplier_awal" id="kode_supplier_awal" required="">
                    <option selected="true" value="">--Pilih Supplier--</option>
                    <?php foreach($supplier as $daftar){ ?>
                    <option value="<?php echo $daftar->kode_supplier ?>"><?php echo $daftar->nama_supplier ?></option>
                    <?php } ?>
                  </select> 
                  <input type="hidden" name="kode_supplier" id="kode_supplier">
                </div>

              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label class="gedhi">Tanggal Transaksi</label>
                  <input type="text" value="<?php echo TanggalIndo(date("Y-m-d")); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_pembelian" id="tanggal_pembelian"/>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">  
                  <label>Kode PO</label>
                  <input type="text" class="form-control" placeholder="Kode Po" name="kode_po" id="kode_po2" required="" readonly="true" />
                </div>

              </div>

              <div class="col-md-3">
                <div class="form-group">  
                  <label>Nota Referensi</label>
                  <input required type="text" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" required=""/>
                </div>

              </div>

              <div class="col-md-6" hidden="">
                <div class="form-group">  
                  <label>Keterangan</label>
                  <textarea class="form-control" placeholder="Keterangan" name="keterangan" id="keterangan" required=""></textarea>
                </div>

              </div>



            </div>
          </div> 
          <hr>
          <div class="sukses" ></div>
          <div style="width: 100%; float: left; padding-bottom: 30px; margin-top:15px;" class="form_pembelian">
            <!-- LEFT SIDE -->
            <div class="col-md-9" style="padding: 0;" data-mh="xxx">
              <div class="col-md-12">
                <div style=" float: left; padding: 0 0 14px 5px ; background: #dadad9; margin-bottom: 15px;">
                  <div class="col-xs-2" id="bahan_baku">
                    <label>Nama Bahan</label>
                    <select id="kode_bahan" name="kode_bahan" class="form-control select2">
                      <option value="">Pilih Produk</option>
                      <?php 
//$this->db->where('status_produk','produk');
                      $ambil_data = $this->db->get('master_bahan_baku');
                      $hasil_ambil_data = $ambil_data->result();
                      foreach ($hasil_ambil_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value->kode_bahan_baku ;?>"><?php echo $value->nama_bahan_baku ;?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                  <input type="hidden" id="nama_bahan" name="nama_bahan" />
                  <div class="col-xs-1">
                    <label>Jumlah</label>
                    <input type="text" class="form-control" placeholder="0" name="jumlah" id="jumlah"/>
                    <input disabled="" type="hidden" name="jumlah_awal" id="jumlah_awal"/>
                  </div>
                  <div class="col-xs-1">
                    <label>Satuan</label>
                    <input readonly type="text" class="form-control" placeholder="Satuan" name="satuan" id="satuan_stok"/>
                  </div>
                  <div class="col-xs-2">
                    <label>Harga </label>
                    <input type="text" class="form-control" placeholder="0" name="harga" id="harga" />
                    <input disabled="" type="hidden" name="harga_awal" id="harga_awal" />
                    <input type="hidden" name="kode_satuan" id="kode_satuan" />
                  </div>
                  <div class="col-xs-2" style="width: 120px">
                    <label>Jenis Diskon</label>

                    <select class="form-control" name="jenis_diskon_item" id="jenis_diskon_item">
                      <option value="Persen">Persen</option>
                      <option value="Rupiah">Rupiah</option>
                    </select>

                  </div>
                  <div class="col-xs-2" style="width: 110px">
                    <label>Diskon</label>
                    <div class="input-icon right">
                      <i class="fa icon_diskon_item" >%</i>
                      <input type="text" class="form-control" placeholder="0" name="diskon_item" id="diskon_item" />
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <label>Subtotal</label>
                    <input type="text" readonly="true" class="form-control" placeholder="Sub Total" name="sub_total" id="sub_total" />
                    <input disabled="" type="hidden" name="sub_total_awal" id="sub_total_awal" />
                    <input type="hidden" name="id_item" id="id_item" />
                  </div>
                  <div class="col-xs-1" style="padding:auto 0px;">
                    <label>&nbsp;</label>
                    <a onclick="add_item()" id="add"  class="btn btn-primary">Add</a>
                    <a onclick="update_item()" id="update" style="padding-left: 5px ;padding-right: 5px;" class="btn btn-primary pull-left">Update</a>
                  </div>
                </div>
              </div>
              <!-- TABLE LIST -->
              <div id="list_transaksi_pembelian" class="col-xs-12">
                <div class="box-body">
                  <label><strong>List Produk</strong></label>
                  <table id="tabel_daftar" class="table table-bordered table-striped" style="font-size:1.5em;">
                    <thead>
                      <tr>
                        <th>No</th>

                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Diskon</th>
                        <th>Subtotal</th>
                        <!-- <th>Exp.Date</th> -->
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="tabel_temp_data_transaksi">

                    </tbody>
                    <tfoot>

                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="col-md-6">
                <label>Diskon</label>
                <table width="100%">
                  <tr>
                    <td width="100px;">
                      <select class="form-control" id="jenis_diskon" name="jenis_diskon">
                        <option value="Persen">Persen</option>
                        <option value="Rupiah">Rupiah</option>
                      </select>
                    </td>
                    <td>
                      <div class="input-icon right">
                        <i class="fa icon_diskon" >%</i>
                        <input type="text" class="form-control" placeholder="0" name="diskon" id="diskon">
                      </div>
                    </td>
                  </tr>
                </table>
                <!-- <p class="help-block pull-right"><i class="fa fa-exclamation"></i> Pilih jenis diskon</p> -->
              </div>
              <div class="col-md-12"><strong><div id="nilai_uang_muka" class="pull-right" style="font-size: 20px"></div></strong></div>
              <!-- //TABLE LIST -->
            </div>
            <!-- //LEFT SIDE -->


            <!-- Right SIDE -->
            <div class="col-md-3" style="background: #c49f47;color: #fff; margin-bottom: 5px;" data-mh="xxx">
              <div class="col-md-12">
                <label style="font-size: 15px">Total Pembelian</label>
              </div>
              <div class="col-md-12">
                <label class="pull-right nilai_pembelian" style="font-size: 20px">Rp.0</label>
                <input type="hidden" name="total_pembelian" id="total_pembelian" value="0">
              </div>
            </div>
            <div class="col-md-3" style="background: #cb5a5e;color: #fff; margin-bottom: 5px" data-mh="xxx">
              <div class="col-md-12">
                <label style="font-size: 15px">Diskon</label>
              </div>
              <div class="col-md-12">
                <label class="pull-right nilai_diskon" style="font-size: 20px">Rp.0</label>

              </div>
            </div>
            <div class="col-md-3" style="background: #3598dc;color: #fff; margin-bottom: 5px" data-mh="xxx">
              <div class="col-md-12">
                <label style="font-size: 15px">Grand Total</label>
              </div>
              <div class="col-md-12">
                <label class="pull-right nilai_grand_total" style="font-size: 20px">Rp.0</label>
                <input type="hidden" name="grand_total" id="grand_total">
              </div>
            </div>
            <div class="col-md-3" style="background: #66b82f; padding-top: 20px; margin-bottom: 30px;" data-mh="xxx">




              <div class="col-md-12">
                <label>Jenis Pembayaran</label>
                <div class="form-group">
                  <select class="form-control" id="jenis_pembayaran" name="proses_pembayaran" >
                    <option value="cash">Cash</option>
                    <option value="kredit">Kredit</option>
                  </select>
                </div>
              </div>
              <hr>
              <div class="col-md-12" id="form_jatuh_tempo">
                <label>Jatuh Tempo</label>
                <div class="form-group">
                  <input type="date" class="form-control" name="tanggal_jatuh_tempo" id="jatuh_tempo" />
                </div>
              </div>
              <div class="col-md-12">
                <label id="label_uang_muka">Dibayar</label>
                <div class="form-group">
                  <input type="text" class="form-control" onkeyup="get_kembali()" name="uang_muka" id="uang_muka" />

                </div>
              </div>




              <div class="col-md-12" style="background: #8e5fa2; color:#FFF; padding:10px; border-radius: 8px !important; box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.38); margin-bottom: -27px;">
                <div class="input-group text-center" style="width: 100%;">
                  <h5><div id="label_kembalian">Kembalian</div></h5>
                  <h3><div id="nilai_dibayar">Rp. 0</div></h3>
                  <input type="hidden" class="form-control"  name="kembalian" id="kembalian" />
                  <input type="hidden" class="form-control"  name="kode_sub" id="kode_sub" />
                </div>
              </div>
            </div>
            <!-- //Right SIDE -->
          </div>

          <br>
          <br>
          <br>
          <center>
            <a onclick="confirm_bayar()"  class="btn btn-success btn-lg form_pembelian" style="width:500px;"><i class="fa fa-save"></i> Simpan</a></center>
            <div class="box-footer clearfix">

            </div>
          </form>
          <!--  -->
        </div>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------------------------------------------------------------>
<!-- Content Wrapper. Contains page content -->
<!-- /.content-wrapper -->
<div id="modal-regular" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="cari_po" method="post">
        <div class="modal-header" style="background-color:grey">

          <h4 class="modal-title" style="color:#fff;">Transaksi Pembelian</h4>
        </div>
        <div class="modal-body" >
          <div class="form-body">

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Kode PO</label>
                  <input type="text" id="kode_po" name="kode_po" class="form-control" placeholder="Kode PO" required="">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer" style="background-color:#eee">
          <button onclick="cancel()" class="btn blue" data-dismiss="modal" aria-hidden="true">Cancel</button>
          <button type="submit" class="btn green">Cari</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">

        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus pembelian Produk tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()"   class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>

<div id="modal-confirm-bayar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Pembayaran</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan Pembelian Tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button id="simpan_transaksi"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<div id="modal-notif" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Informasi</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Gagal, Nomor Referensi harus di isi !<span id="bayare"></span></span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">OK</button>
        <!-- <button id="simpan_transaksi"  class="btn green">Ya</button> -->
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-dibayar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Bayar</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Nominal Pembayaran Kurang !</span>
        <input id="id-delete" type="hidden">
        <input id="bahan-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">OK</button>

      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-uangmuka" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Uang Muka</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Periksa Kembali Nominal Uang Muka !</span>
        <input id="id-delete" type="hidden">
        <input id="bahan-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">OK</button>

      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-tanggal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Jatuh Tempo</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Silahkan Pilih Tanggal Jatuh Tempo !</span>
        <input id="id-delete" type="hidden">
        <input id="bahan-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">OK</button>

      </div>
    </div>
  </div>
</div>
<div id="modal_penyesuaian" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
        <label>Penyesuaian Hpp</label>
      </div>
      <div class="modal-body">
        <table  class="table table-bordered table-striped" style="font-size:1.5em;">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Harga Satuan Awal</th>
              <th>Harga Satuan Baru</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="tabel_data_penyesuaian">

          </tbody>
          <tfoot>

          </tfoot>
        </table>

      </div>
      <div class="modal-footer" style="background-color:#eee">

        <button id="selesai_penyesuaian"  onclick="selesai_penyesuaian()" class="btn blue">Selesai</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(".form_pembelian").hide();

  function setting() {
    $('#modal_setting').modal('show');
  }

  function simpan_supplier() {
    var kode_supplier = $("#kode_supplier").val();
    if (kode_supplier == '') {
      alert('Nama Supplier Harus Diisi.');
    }else{
      $('#modal-supplier').modal('show');
    };
  }
  function update_supplier() {
    $('#modal-supplier-update').modal('show');
  }

  function save_supplier(){
    document.getElementById('kode_supplier').disabled = true;
    document.getElementById('nomor_nota').readOnly = true;
    $('#modal-supplier').modal('hide');
    $("#update_supplier").show();
    $("#simpan_supplier").hide();
    $(".form_pembelian").show();
  }
  function delete_supplier(){
    document.getElementById('kode_supplier').disabled = false;
    document.getElementById('nomor_nota').readOnly = false;
    $('#modal-supplier-update').modal('hide');
    $("#update_supplier").hide();
    $("#simpan_supplier").show();
    $(".form_pembelian").hide();

    var kode_pembelian = $('#kode_pembelian').val();
    var kode_supplier = $('#kode_supplier').val();
    var url = '<?php echo base_url().'pembelian/hapus_temp'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        kode_pembelian:kode_pembelian,kode_supplier:kode_supplier
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {

        $(".tunggu").hide();
        $('#modal-confirm').modal('hide');
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);
        $('#kategori_bahan').val('bahan baku');
        get_grandtotal();

      }
    });
    return false;

  }

  function confirm_bayar(){

    var uang = $("#nilai_dibayar").text();
    var uang_muka = $("#uang_muka").val();
    var grand_total = $("#grand_total").val();
    var proses_pembayaran = $("#jenis_pembayaran").val();
    var jatuh_tempo = $("#jatuh_tempo").val();
    if(proses_pembayaran=='kredit' && jatuh_tempo==''){
      $("#modal-confirm-tanggal").modal('show');
    }else if(( parseInt(uang_muka) < 0 || parseInt(grand_total) <= parseInt(uang_muka) || uang_muka=='') && proses_pembayaran=='kredit'){
      $("#modal-confirm-uangmuka").modal('show');
    }else if((parseInt(grand_total) > parseInt(uang_muka) || uang_muka=='') && proses_pembayaran=='cash'){
      $("#modal-confirm-dibayar").modal('show');
    }else{
      $("#bayare").text(uang);
      $("#modal-confirm-bayar").modal('show');
    }

  }
  $("#cari_po").submit(function(){
    var kode_pembelian = $('#kode_pembelian').val();  
    var kode_po = $('#kode_po').val();  
    var keterangan = "<?php echo base_url().'pembelian/get_kode_po'?>";
    $('#kode_po2').val(kode_po);
    $.ajax({
      type: "POST",
      url: keterangan,
      data: {kode_po:kode_po,kode_pembelian:kode_pembelian},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        var data = msg.split("|");
        if(data[0] == 1){  

          $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);
          $('#modal-regular').modal('hide');
          $("#hasil_kode_po").val(data[1]);
          $("#kode_supplier_awal").select2().select2('val', data[2]);
          $("#kode_supplier").val(data[2]);
          $(".tunggu").hide();
          $(".form_pembelian").show();
          get_grandtotal();
        }
        else{
          alert('Nota Tidak Ditemukan');            
          $('#kode_supplier_awal').val('');
          $('#nota').val('');
          $('#tanggal_pembelian_awal').val('');
          $(".tunggu").hide();

        }  

      }
    });
    return false;
  });

  $('#jenis_diskon_item').on('change',function(){
    var jenis_diskon_item = $("#jenis_diskon_item").val();
    var jumlah = $('#jumlah').val();
    var harga = $('#harga').val();
    var pembelian=(jumlah * harga);
    if(jenis_diskon_item == 'Persen')
    { 
      $(".icon_diskon_item").text('%');
    }else if(jenis_diskon_item == 'Rupiah')
    {
      $(".icon_diskon_item").text('Rp');
    }
    $("#diskon_item").val('');
    $("#sub_total").val(pembelian);
  });
  $('#jenis_diskon').on('change',function(){
    var jenis_diskon = $("#jenis_diskon").val();
    if(jenis_diskon == 'Persen')
    { 
      $(".icon_diskon").text('%');
    }else if(jenis_diskon == 'Rupiah')
    {
      $(".icon_diskon").text('Rp');
    }
    $("#diskon").val('');
    get_grandtotal();
  });

  $("#diskon_item").keyup(function(){
    var jenis_diskon_item = $("#jenis_diskon_item").val();
    var diskon_item = parseInt($("#diskon_item").val());
    var jumlah = $('#jumlah').val();
    var harga = $('#harga').val();
    var pembelian=(jumlah * harga);
    if(jenis_diskon_item == 'Persen'){ 
      if(diskon_item < 0 || diskon_item > 100){
        alert('Jumlah Diskon Salah');
        $("#diskon_item").val('');
        $("#sub_total").val(pembelian);
      }else{

        var nilai_diskon = ( pembelian * diskon_item) /100;
        var total = ( pembelian - nilai_diskon);
        $("#sub_total").val(total);
      }
    }else if(jenis_diskon_item == 'Rupiah'){
      if(diskon_item < 0 ||diskon_item > pembelian){
        alert('Jumlah Diskon Salah');
        $("#diskon_item").val('');
        $("#sub_total").val(pembelian);
      }else{
        var total = ( pembelian - diskon_item);
        $("#sub_total").val(total);
      }
    }

  });
  $("#diskon").keyup(function(){
    var jenis_diskon = $("#jenis_diskon").val();
    var diskon = parseInt($("#diskon").val());
    var pembelian=$('#total_pembelian').val();
    if(jenis_diskon == 'Persen'){ 
      if(diskon < 0 || diskon > 100){
        alert('Jumlah Diskon Salah');
        $("#diskon").val('');
      }
    }else if(jenis_diskon == 'Rupiah'){
      if(diskon < 0 ||diskon > pembelian){
        alert('Jumlah Diskon Salah');
        $("#diskon").val('');
      }
    }
    get_grandtotal();
  });

  $('#jenis_pembayaran').on('change',function(){
    var jenis_pembayaran = $("#jenis_pembayaran").val();
    if(jenis_pembayaran == 'cash')
    { 
      $("#form_jatuh_tempo").hide();

      $("#label_uang_muka").text('Dibayar');
      $("#label_kembalian").text('Kembalian');
    }else if(jenis_pembayaran == 'kredit')
    {
      $("#form_jatuh_tempo").show();
      $("#uang_muka").val('');
      $("#label_uang_muka").text('Uang Muka');
      $("#label_kembalian").text('Hutang');
    }
    get_grandtotal();
  });

  var jenis_pembayaran = $("#jenis_pembayaran").val();
  if(jenis_pembayaran == 'cash')
  {
    $("#form_jatuh_tempo").hide();
    var total = $("#tabel_temp_data_transaksi #tb_grand_total").text();
    $("#uang_muka").val(total);
  }else if(jenis_pembayaran == 'kredit')
  {
    $("#form_jatuh_tempo").show();

  }


  $("#jumlah").keyup(function(){

    var jumlah = parseInt($('#jumlah').val());
    if(jumlah < 0  ){
      var jumlah_awal = $('#jumlah_awal').val();
      var sub_total_awal = $('#sub_total_awal').val();
      $('#jumlah').val(jumlah_awal);
      $('#sub_total').val(sub_total_awal);



      alert("Jumlah Salah");
    }else{
      var jenis_diskon_item = $("#jenis_diskon_item").val();
      var diskon_item = parseInt($("#diskon_item").val()); 
      var harga = $('#harga').val();
      var pembelian=(jumlah * harga);
      if(jenis_diskon_item == 'Persen' && $("#diskon_item").val() !=''){ 
        if(diskon_item < 0 || diskon_item > 100){
          alert('Jumlah Diskon Salah');
          $("#diskon_item").val('');
          $("#sub_total").val('');
        }else{

          var nilai_diskon = ( pembelian * diskon_item) /100;
          var total = ( pembelian - nilai_diskon);
          $("#sub_total").val(total);
        }
      }else if(jenis_diskon_item == 'Rupiah' && $("#diskon_item").val() !=''){
        if(diskon_item < 0 ||diskon_item > pembelian){
          alert('Jumlah Diskon Salah');
          $("#diskon_item").val('');
          $("#sub_total").val('');
        }else{
          var total = ( pembelian - diskon_item);
          $("#sub_total").val(total);
        }
      }else{
        $("#sub_total").val(pembelian);
      }
    }
  });



  $("#harga").keyup(function(){
    var harga = parseInt($('#harga').val());
    if(harga < 0  ){
      var harga_awal = $('#harga_awal').val();
      var sub_total_awal = $('#sub_total_awal').val();

      $('#harga').val(harga_awal);
      $('#sub_total').val(sub_total_awal);

      alert("Jumlah Salah");
    }else{
      var jenis_diskon_item = $("#jenis_diskon_item").val();
      var diskon_item = parseInt($("#diskon_item").val()); 
      var jumlah = parseInt($('#jumlah').val());
      var pembelian=(jumlah * harga);
      if(jenis_diskon_item == 'Persen' && $("#diskon_item").val() !=''){ 
        if(diskon_item < 0 || diskon_item > 100){
          alert('Jumlah Diskon Salah');
          $("#diskon_item").val('');
          $("#sub_total").val('');
        }else{
          var nilai_diskon = (pembelian * diskon_item) /100;
          var total = ( pembelian - nilai_diskon);
          $("#sub_total").val(total);
        }

      }else if(jenis_diskon_item == 'Rupiah' && $("#diskon_item").val() !=''){
        if(diskon_item < 0 ||diskon_item > pembelian){
          alert('Jumlah Diskon Salah');
          $("#diskon_item").val('');
          $("#sub_total").val('');
        }else{
          var total = ( pembelian - diskon_item);
          $("#sub_total").val(total);
        }
      }else{
        $("#sub_total").val(pembelian);

      }
    }

  });


  $(document).ready(function(){
    $("#tanggal").hide();
    $("#update").hide();
    $("#update_supplier").hide();
    $("#peralatan").hide();
    $("#form_expired").hide();
    $("#modal-regular").modal('show');


    // var kode_pembelian = $('#kode_pembelian').val() ;  
    // $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);

    $(".tgl").datepicker();

    $('#nomor_nota').on('change',function(){
      var nomor_nota = $('#nomor_nota').val();
      var url = "<?php echo base_url() . 'pembelian/get_kode_nota' ?>";
      $.ajax({
        type: 'POST',
        url: url,
        data: {nomor_nota:nomor_nota},
        success: function(msg){
          if(msg == 1){
            $(".notif_nota").html('<div class="alert alert-warning">Kode_Telah_dipakai</div>');
            setTimeout(function(){
              $('.notif_nota').html('');
            },1700);              
            $('#nomor_nota').val('');
          }
          else{

          }
        }
      });
    });


    $("#kode_sub").val('2.1.1');

    $("#proses_pembayaran").change(function(){
      var proses_pembayaran = $("#proses_pembayaran").val();
      if(proses_pembayaran== 'cash'){
        $("#kode_sub").val('2.1.1');
      }
      else if(proses_pembayaran== 'debet'){
        $("#kode_sub").val('2.1.2');

      }
      else{
        $("#kode_sub").val('2.1.3');
      }
      if(proses=="kredit"){
        $("#tanggal").show();
      }else{
        $("#tanggal").hide();
      }
    });

    $('#kode_bahan').on('change',function(){
      var kode_bahan = $('#kode_bahan').val();
      var url = "<?php echo base_url() . 'pembelian/get_satuan' ?>";
      $.ajax({
        type: 'POST',
        url: url,
        dataType:'json',
        data: {kode_bahan:kode_bahan},
        success: function(msg){



          if(msg.satuan_pembelian){
            $('#satuan_stok').val(msg.satuan_pembelian);
          }

          if(msg.id_satuan_pembelian){
            $("#kode_satuan").val(msg.id_satuan_pembelian);
          }

          if(msg.nama_bahan_baku){
            $("#nama_bahan").val(msg.nama_bahan_baku);
          }
          if(msg.nama_perlengkapan){
            $("#nama_bahan").val(msg.nama_perlengkapan);
          }
          $('#jumlah').val('');
          $('#diskon').val('');
          $('#sub_total').val('');

        }
      });
    });


    $("#dibayar").keyup(function(){
      var dibayar = $("#dibayar").val();
      var kode_pembelian = $('#kode_pembelian').val() ;
      var grand = $("#tb_grand_total").text();
      var proses_pembayaran = $('#proses_pembayaran').val() ;
      var url = "<?php echo base_url().'pembelian/get_rupiah'; ?>";
      var url_kredit = "<?php echo base_url().'pembelian/get_rupiah_kredit'; ?>";
      if(proses_pembayaran==''){
        alert('Pembayaran Harus Diisi');
      }
      else{
        if(proses_pembayaran=='cash' || proses_pembayaran=='debet'){
          $.ajax({
            type: "POST",
            url: url,
            data: {dibayar:dibayar,kode_pembelian:kode_pembelian,grand:grand},
            success: function(msg) {            
              var data = msg.split("|");  
              var nilai_dibayar = data[1];
              var nilai_kembali = data[0];
              $("#nilai_dibayar").html(nilai_dibayar);


            }
          });
        }
        else if(proses_pembayaran=='credit'){
          $.ajax({
            type: "POST",
            url: url_kredit,
            data: {dibayar:dibayar,kode_pembelian:kode_pembelian,grand:grand},
            success: function(msg) {            
              var data = msg.split("|");  
              var nilai_dibayar = data[1];
              var nilai_kembali = data[0];
              $("#nilai_dibayar").html(nilai_dibayar);
              $("#nilai_kembali").html(nilai_kembali);
            }
          });
        }
      }

    })

    $("#simpan_transaksi").click(function(){
      var simpan_transaksi = "<?php echo base_url().'pembelian/simpan_transaksi'?>";
      var kode_pembelian = $('#kode_pembelian').val() ;
      $.ajax({
        type: "POST",
        url: simpan_transaksi,
        data: $('#data_form').serialize(),
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success: function(msg)
        {
          $(".tunggu").hide();  
          $("#modal-confirm-bayar").modal('hide');
          var data = msg.split("|");
          var num = parseInt(data[0]);
          var pesan = data[1];

          if(num > 0){  
            kode = $("#kode_sub").val();
            setTimeout(function(){$('.sukses').html(msg);
              window.location = "<?php echo base_url() . 'pembelian/daftar_pembelian' ?>";
              //window.open("<?php echo base_url() . 'pembelian/print_pembelian/' ?>"+kode_pembelian);
            },1500);  
          }
          else{
            $(".sukses").html(pesan);   
            setTimeout(function(){$('.sukses').html('');
          },1500); 
          }     
        }
      });
      return false;

    });

  });

function add_item(){
  var kategori_bahan = $('#kategori_bahan').val();
  var kode_pembelian = $('#kode_pembelian').val();
  var nomor_nota = $('#nomor_nota').val();
  var kode_supplier = $('#kode_supplier').val();
  var kategori_bahan = $('#kategori_bahan').val();
  var kode_bahan = $('#kode_bahan').val();
  var diskon = $('#diskon_item').val();
  var jenis_diskon = $('#jenis_diskon_item').val();
  var jumlah = $('#jumlah').val();
  var satuan_stok = $('#satuan_stok').val();
  var kode_satuan = $('#kode_satuan').val();
  var nama_satuan = $("#nama_satuan").val();
  var harga_satuan = $('#harga').val();
  var nama_bahan = $("#nama_bahan").val();
  var subtotal = $("#sub_total").val();

  var url = "<?php echo base_url().'pembelian/simpan_item_temp/'?> ";
  if(nomor_nota == '' && kode_supplier == ''){
    $(".sukses").html('<div class="alert alert-danger">Nomor Nota dan Supplier harus diisi.</div><span aria-hidden="true">&times;</span>');   
    setTimeout(function(){
      $('.sukses').html('');     
    },3000);
  }else if(kode_bahan == '' || jumlah == ''){
    $(".sukses").html('<div class="alert alert-danger">Silahkan Lengkapi Form</div>');   
    setTimeout(function(){
      $('.sukses').html('');     
    },3000);
  }
  else{
    $.ajax({
      type: "POST",
      url: url,
      data: { 
        kode_pembelian:kode_pembelian,
        kode_bahan:kode_bahan,
        nama_bahan:nama_bahan,
        jumlah:jumlah,
        kode_satuan:kode_satuan,
        nama_satuan:nama_satuan,
        harga_satuan:harga_satuan,
        kode_supplier:kode_supplier,
        diskon_item:diskon,
        nama_satuan:satuan_stok,
        jenis_diskon:jenis_diskon,subtotal:subtotal,
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(data)
      {

        $(".tunggu").hide();

        if(data==1){
          $(".sukses").html('<div class="alert alert-danger">Item Telah Tersedia</div>');

          setTimeout(function(){$('.sukses').html('');},1800); 
        }else{
          $('.sukses').html('');     
          $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);
          $('#kode_bahan').val('');
          $('#kode_perlengkapan').val('');
          $('#kategori_bahan').val('');
          $('#jumlah').val('');
          $('#expired_date').val('');
          $("#harga").val('');
          $("#satuan_stok").val('');
          $("#diskon_item").val('');

          $('#sub_total').val('');
          $("#nama_bahan").val('');
          $("#kode_satuan").val(''); 
          $("#nama_satuan").val(''); 
          get_grandtotal();
        }
      }
    });
  }
}

function actDelete(id) {
  $('#id-delete').val(id);
  $('#modal-confirm').modal('show');
}

function actEdit(id) {
  var id = id;
  var kode_pembelian = $('#kode_pembelian').val();
  var url = "<?php echo base_url().'pembelian/get_temp_pembelian'; ?>";
  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: {id:id},
    success: function(pembelian){
      $('#kategori_bahan').val(pembelian.kategori_bahan);
      $("#kode_bahan").val(pembelian.kode_bahan);
      $("#kode_perlengkapan").val(pembelian.kode_bahan);
      $("#kategori_bahan").val(pembelian.kategori_bahan);
      $("#nama_bahan").val(pembelian.nama_bahan);
      $('#jumlah').val(pembelian.jumlah);
      $('#jumlah_awal').val(pembelian.jumlah);
      $('#kode_satuan').val(pembelian.kode_satuan);
      $("#nama_satuan").val(pembelian.nama_satuan);
      $('#harga').val(pembelian.harga_satuan);
      $('#harga_awal').val(pembelian.harga_satuan);
      $('#diskon_item').val(pembelian.diskon_item);
      if(pembelian.jenis_diskon == '' || pembelian.jenis_diskon  == null){
        $('#jenis_diskon_item').val("Persen");
      }else{
        $('#jenis_diskon_item').val(pembelian.jenis_diskon);
      }
      $('#sub_total').val(pembelian.subtotal);
      $('#sub_total_awal').val(pembelian.subtotal);
      $('#expired_date').val(pembelian.expired_date);
      $('#satuan_stok').val(pembelian.nama_satuan);
      $('#kode_supplier').val(pembelian.kode_supplier);
      $('#sub_total').val(pembelian.subtotal);
      $("#id_item").val(pembelian.id);
      $("#add").hide();
      $("#update").show();
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);
      document.getElementById('kode_bahan').disabled = true;

    }
  });
}

function update_item(){
  var kode_pembelian = $('#kode_pembelian').val();
  var kategori_bahan = $('#kategori_bahan').val();
  var diskon = $('#diskon_item').val();
  var kode_bahan = $('#kode_bahan').val();
  var jumlah = $('#jumlah').val();
  var expired_date = $('#expired_date').val();
  var kode_satuan = $('#kode_satuan').val();
  var nama_satuan = $("#nama_satuan").val();
  var harga_satuan = $('#harga').val();
  var nama_bahan = $("#nama_bahan").val();
  var subtotal = $("#sub_total").val();
  var jenis_diskon = $("#jenis_diskon_item").val();
  var id_item = $("#id_item").val();
  var url = "<?php echo base_url().'pembelian/update_item_temp/'?> ";

  $.ajax({
    type: "POST",
    url: url,
    data: { 
      expired_date:expired_date,
      kode_pembelian:kode_pembelian,
      kategori_bahan:kategori_bahan,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      jumlah:jumlah,
      kode_satuan:kode_satuan,
      nama_satuan:nama_satuan,
      harga_satuan:harga_satuan,
      diskon_item:diskon,
      jenis_diskon:jenis_diskon,subtotal:subtotal,
      id:id_item
    },
    success: function(data)
    {
      document.getElementById('kode_bahan').disabled = false;
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);
      $('#kategori_bahan').val('');
      $('#kode_bahan').val('');
      $('#jumlah').val('');
      $("#nama_satuan").val('');
      $('#harga_satuan').val('');
      $("#nama_bahan").val('');
      $("#kode_satuan").val('');
      $("#id_item").val('');
      $("#harga").val('');
      $("#harga_awal").val('');
      $("#jumlah_awal").val('');
      $("#sub_total").val('');
      $("#sub_total_awal").val('');
      $("#expired_date").val('');
      $("#satuan_stok").val('');
      $("#jenis_diskon_item").val('Persen');
      $("#diskon_item").val('');
      $("#add").show();
      $("#update").hide();
      $("#bahan_baku").show();
      $("#peralatan").hide();
      $("#form_expired").hide();
      $("#bahan").show();
      get_grandtotal();

    }
  });
}

function delData() {

  var id = $('#id-delete').val();
  var kode_pembelian = $('#kode_pembelian').val();
  var url = '<?php echo base_url().'pembelian/hapus_bahan_temp'; ?>/delete';
  $.ajax({
    type: "POST",
    url: url,

    data: {
      id:id
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {

      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pembelian/get_pembelian/'; ?>"+kode_pembelian);
      $('#kategori_bahan').val('bahan baku');
      get_grandtotal();

    }
  });
  return false;
}
function get_grandtotal() {
  var kode_pembelian = $('#kode_pembelian').val();
  var kode_supplier = $('#kode_supplier').val();
  var diskon = $('#diskon').val();
  var jenis_diskon = $('#jenis_diskon').val();
  var jenis_pembayaran = $('#jenis_pembayaran').val();
  var url = '<?php echo base_url().'pembelian/get_grandtotal'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    dataType: 'json',
    data: {
      kode_pembelian:kode_pembelian,diskon:diskon,jenis_diskon:jenis_diskon,kode_supplier:kode_supplier
    },
    success: function(msg) {
      $('#grand_total').val(msg.grand_total);

      $('.nilai_diskon').text(msg.nilai_diskon);
      $('.nilai_grand_total').text(msg.nilai_grand_total);
      $('.nilai_pembelian').text(msg.nilai_pembelian);
      $('#total_pembelian').val(msg.total_pembelian);
      if(jenis_pembayaran=='cash'){
//$('#uang_muka').val(msg.grand_total);

$('#kembalian').val('');
}else{
  $('#uang_muka').val('');
  $('#kembalian').val(msg.grand_total);
  $('#nilai_dibayar').text(msg.nilai_grand_total);
}
}
});
  return false;
}
function get_kembali() {
  var uang_muka = $('#uang_muka').val();
  var grand_total = $('#grand_total').val();
  var jenis_pembayaran = $('#jenis_pembayaran').val();
  var url = '<?php echo base_url().'pembelian/get_kembali'; ?>';
  if((parseInt(uang_muka) < 0 || uang_muka=='-') && jenis_pembayaran=='cash'){
    alert("Nominal Salah");
    $('#uang_muka').val('');
    get_grandtotal();
  }else if((parseInt(uang_muka) < 0 || uang_muka=='-' || parseInt(uang_muka) >= parseInt(grand_total)) && jenis_pembayaran=='kredit'){
    alert("Nominal Salah");
    $('#uang_muka').val('');
    get_grandtotal();
    $('#nilai_uang_muka').text('Rp. 0');
  }else{
    $.ajax({
      type: "POST",
      url: url,
      dataType: 'json',
      data: {
        grand_total:grand_total,uang_muka:uang_muka,jenis_pembayaran:jenis_pembayaran
      },
      success: function(msg) {
        $('#nilai_dibayar').text(msg.nilai_kembali);
        $('#kembalian').val(msg.kembali);
        $('#nilai_uang_muka').text(msg.nilai_uang_muka);
      }
    });
    return false;
  }

}

</script>


