<?php 
	
	$kode 	=	$this->uri->segment(3);

	$hasil 	=	$this->db->get_where('opsi_transaksi_penjualan' , ['kode_penjualan' => $kode])->result();
	$no 	=	1;
	foreach($hasil as $res){

		$retur 	=	$res->jumlah_retur == null ? 0 : $res->jumlah_retur; 
 ?>
 <tr>
 	<td><?php echo $no++; ?></td>
 	<td><?php echo '<b>'.$res->kode_menu .'</b> - '.$res->nama_menu ?></td>
 	<td><?php echo format_rupiah($res->harga_satuan) ?></td>
 	<td><?php echo $res->jumlah.' '.$res->nama_satuan ?></td>
 	<td><?php echo $retur.' '.$res->nama_satuan ?></td>
 	<td><?php echo format_rupiah($res->subtotal) ?></td>
 </tr>
<?php } ?>