
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
	<!--<style type="text/css">
	@page  
	{ 
		width: 12cm;
		height: 14cm   	  
	} 

	body  
	{ 
		margin-left: 0.5cm;
		margin-right: 0.5cm;
	} 
	.table tr td{

	}



	@media print {
		html, body {

			/*display: block;*/
			/*font-family: "Dotrice";*/
			font-size: 12px;
		}

		@page
		{
			size: 12cm 14cm;
		}

	}
	div.page { page-break-after: always;
		/*position: relative;*/
		/*margin:10px 15px 0px 15px;*/
		padding:0px; }
		</style>-->

		<style type="text/css">
	body{


	}
	.table1{
		border-collapse: collapse;
		width:100%;
		position: absolute;
		top: 0px;
		text-align: left;
		vertical-align: middle;
	}
	.right{
		text-align: right;
		margin-right: 5px;
	}
	.left{
		text-align: left;
	}
	.table2{
		width:100%; 
		text-align:center;
		border-collapse: collapse;
	}
	.table2 tr th{
		border: 0.5px solid #222;
	}
	.table2 tr td{
		border: 0.5px solid #222;
	}
	@media print {
		html, body {


			display: block;
			font-family: "Dotrice";
			font-size: auto;
		}

		@page
		{
			size: 21cm 14cm;
		}

	}
	div.page { page-break-after: always;
		position: relative;
		margin:10px 15px 0px 15px;
		padding:0px; }
	</style>
		<?php
		$kode_mutasi=$this->uri->segment(3);
		$get_transaksi=$this->db->get_where('transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
		$hasil_transaksi=$get_transaksi->row();
		if($hasil_transaksi->kategori_petugas=='sales' or $hasil_transaksi->kategori_petugas=='sales & sopir'){
			$get_petugas=$this->db->get_where('master_sales',array('kode_sales' => $hasil_transaksi->kode_unit_tujuan ));		
		}else{
			$get_petugas=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_unit_tujuan ));		
		}
		$hasil_petugas=$get_petugas->row();
		?>

		<table width="100%" border="0" style="border-collapse: collapse;">
			<tr>
				<td >
					<img src="<?php echo base_url().'component/img/logo berlian tm.jpg' ?>" width="180px" alt="" title="" /><br>
					
					</td>
					<td width="50%" colspan="2">......................................., <?php echo @TanggalIndo($hasil_transaksi->tanggal_transaksi);?> </td>
				</tr>

				<!-- <tr>
					<td width="50%"></td>
					<td width="50%" colspan="2">......................................., <?php echo @TanggalIndo($hasil_transaksi->tanggal_transaksi);?> </td>
				</tr> -->
				<tr>
					<td width="50%" rowspan="3"><center style="font-size:12px;">
						<i>
							Produksi Alat - Alat Kebersihan<br>
							<b>Sapu, Sikat, Penebah, Kaset, Dll</b></i><br>
							Jl. Raya Gurang Anyar 17 - 19<br>
							Cerme - Gresik<br>
						</center></td>
					<td width="10%" >Kepada : </td>
					<td width="30%"> <?php echo $hasil_transaksi->nama_unit_tujuan;?>
					</td>
				</tr>
				<tr>
					<td width="10%" ></td>
					<td width="30%"> <?php if($hasil_transaksi->kategori_petugas=='sales & sopir'){echo $hasil_transaksi->nama_sopir;}?>
					</td>
				</tr>
				<tr>
					<td width="10%" ></td>
					<td width="30%"><?php echo $hasil_transaksi->keterangan;?>
					</td>
				</tr>

			</table>
			<br>

			<table width="100%" border="0" style="border-collapse: collapse;">
				<tr>

					<td width="100px"> SURAT JALAN NO </td>
					<td> : </td>
					<td> <?php echo @$hasil_transaksi->kode_surat_jalan;?> </td>

				</tr>
				<tr>

					<td width="100px"> KENDARAAN NO </td>
					<td> : </td>
					<td> <?php echo @$hasil_transaksi->no_kendaraan;?></td>

				</tr>

			</table>
			<br>
			<table width="100%" border="1" style="border-collapse: collapse;">
				<tr>
					<th width="15px">No.</th>
					<th width="100px">Jumlah</th>
					<th width="300px">Jenis Barang</th>

				</tr>
				<?php
				$get_opsi=$this->db->get_where('opsi_transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
				$hasil_opsi=$get_opsi->result();
				$no=1;
				foreach ($hasil_opsi as  $value) {
					?>
					<tr>
						<td align="center"><?php echo $no++; ?></td>
						<td align="center"><?php echo $value->jumlah; ?></td>
						<td><?php echo $value->nama_bahan; ?></td>

					</tr>
					<?php
				}
				?>

			</table>



		</body>
		</html>

		<script type="text/javascript">
