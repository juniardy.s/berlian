

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
    .ombo{
      width: 400px;
    } 

  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Proses Pembayaran Piutang
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>
            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>


            <div class="box-body">            

              <form id="data_form" action="" method="post">
                <div class="box-body">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Kode Transaksi</label>
                          <?php
                          $kode = $this->uri->segment(4);
                          $transaksi_penjualan = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$kode));
                          $hasil_transaksi_penjualan = $transaksi_penjualan->row();
                          ?>
                          <input readonly="true" type="text" value="<?php echo @$hasil_transaksi_penjualan->kode_penjualan; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_pembelian" id="kode_penjualan" />
                        </div>

                        <div class="form-group">
                          <label class="gedhi">Tanggal Transaksi</label>
                          <input type="text" value="<?php echo TanggalIndo($hasil_transaksi_penjualan->tanggal_penjualan); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_pembelian" id="tanggal_pembelian"/>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Kode Member</label>
                          <input readonly="true" type="text" value="<?php echo $hasil_transaksi_penjualan->kode_member ?>" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" />
                        </div>

                        <div class="form-group">
                          <label>Nama Member</label>
                          <input readonly="true" type="text" value="<?php echo $hasil_transaksi_penjualan->nama_member ?>" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" />
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label>Grand Total</label>
                        <input readonly="true" type="text" value="<?php echo format_rupiah($hasil_transaksi_penjualan->grand_total) ?>" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" />
                      </div>
                      <div class="form-group col-md-3">
                        <?php
                        $kode_pen = $this->uri->segment(4);
                        $transaksi_piutang = $this->db->get_where('transaksi_piutang',array('kode_transaksi'=>$kode_pen));
                        $hasil_transaksi_piutang = $transaksi_piutang->row();
                        ?>
                        <label>Jatuh Tempo</label>
                        <input readonly="true" type="text" value="<?php echo @TanggalIndo($hasil_transaksi_piutang->jatuh_tempo) ?>" class="form-control" placeholder="Jatuh Tempo" name="" id="" />
                      </div>
                      <div class="form-group col-md-3">
                        <label>Petugas</label>
                        <input readonly="true" type="text" value="<?php echo ($hasil_transaksi_penjualan->petugas) ?>" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" />
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="sukses" ></div>

                <div id="list_transaksi_pembelian">
                  <div class="box-body">
                    <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Menu</th>
                          <th>QTY</th>
                          <th>Harga Satuan</th>
                          <th>Diskon Item</th>
                          <th>Subtotal</th>
                        </tr>
                      </thead>
                      <tbody id="tabel_temp_data_transaksi">

                        <?php
                        $kode = $this->uri->segment(4);
                        $penjualan = $this->db->get_where('opsi_transaksi_penjualan',array('kode_penjualan'=>$kode));
                        $list_penjualan = $penjualan->result();
                        $nomor = 1;  $total = 0;

                        foreach($list_penjualan as $daftar){ 
                          ?> 
                          <tr>
                            <td><?php echo $nomor; ?></td>
                            <td><?php echo $daftar->nama_menu; ?></td>
                            <td><?php echo $daftar->jumlah; ?></td>
                            <td><?php echo format_rupiah($daftar->harga_satuan); ?></td>
                            <td><?php echo $daftar->diskon_item; ?></td>
                            <td><?php echo format_rupiah($daftar->subtotal); ?></td>
                          </tr>
                          <?php 
                          $total = $total + $daftar->subtotal;
                          $nomor++; 
                        } 
                        ?>

                        <tr>
                          <td colspan="4"></td>
                          <td style="font-weight:bold;">Total</td>
                          <td><?php echo format_rupiah($total); ?></td>
                        </tr>

                        <tr>
                          <td colspan="4"></td>
                          <td style="font-weight:bold;">Diskon (%)</td>
                          <td id="tb_diskon"><?php echo $hasil_transaksi_penjualan->diskon_persen; ?></td></td>

                        </tr>

                        <tr>
                          <td colspan="4"></td>
                          <td style="font-weight:bold;">Diskon (Rp)</td>
                          <td id="tb_diskon_rupiah"><?php echo format_rupiah($hasil_transaksi_penjualan->diskon_rupiah); ?></td>

                        </tr>

                        <tr>
                          <td colspan="4"></td>
                          <td style="font-weight:bold;">Grand Total</td>
                          <td id="tb_grand_total"><?php echo format_rupiah($total-$hasil_transaksi_penjualan->diskon_rupiah); ?></td>

                        </tr>
                      </tbody>
                      <tfoot>

                      </tfoot>
                    </table>
                  </div>
                </div>



                <div class="box-body">
                  <div class="row">
                    <?php
                    $transaksi_piutang= $this->db->get_where('transaksi_piutang',array('kode_transaksi'=>$kode));
                    $hasil_transaksi_piutang = $transaksi_piutang->row();
                    $sisa = $hasil_transaksi_piutang->sisa ;
                    ?>

                    <div class="col-md-2">
                      <label><b>Sisa Piutang</b></label>
                      <?php if($sisa==0){ ?>
                      <div class="input-group">
                        <a href="<?php echo base_url().'hutang_piutang/piutang/daftar_piutang'; ?>"><div style="text-decoration: none;" class="btn red"><i class="fa fa-tags"></i> <?php echo ($sisa==0) ? 'LUNAS' :  format_rupiah($sisa) ; ?></div></a>
                      </div>
                      <?php 
                    }
                    else{
                      ?>
                      <div class="input-group">
                        <div style="text-decoration: none;" class="btn green"><i class="fa fa-tags"></i> <?php echo ($sisa==0) ? 'LUNAS' :  format_rupiah($sisa) ; ?></div>
                        <input type="hidden" id="nilai_sisa" value="<?php echo $sisa; ?>">
                      </div>
                      <?php 
                    }
                    ?>
                  </div>
<!--                 <div class="col-md-3" style="">
                  <label><b>Jumlah Hutang</b></label>
                  <?php if($sisa==0){ ?>
                  <div class="input-group">
                    <a href="<?php echo base_url().'hutang_piutang/piutang/daftar_piutang'; ?>"><div style="text-decoration: none;" class="btn red"><i class="fa fa-tags"></i> <?php echo ($sisa==0) ? 'LUNAS' :  format_rupiah($sisa) ; ?></div></a>
                  </div>
                  <?php 
                }
                else{
                  ?>
                  <div class="input-group">
                    <div style="text-decoration: none;" class="btn blue"><i class="fa fa-tags"></i> <?php echo ($sisa==0) ? 'LUNAS' :  format_rupiah($hasil_transaksi_piutang->nominal_piutang) ; ?></div>

                  </div>
                  <?php 
                }
                ?>
              </div> -->
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="row col-md-12" <?php if($sisa==0) {echo 'style="display:none;"'; } else{} ?> >


                <div class="col-md-3" id="div_dibayar">
                  <label><b>Jenis Transaksi</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    <select class="form-control" name="jenis_transaksi" id="jenis_transaksi">
                      <option value="">--Pilih Pembayaran--</option>
                      <option value="Angsuran">Angsuran</option>
                      <option value="Lunas">Lunas</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3" id="div_dibayar">
                  <label><b>Dibayar</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    <input type="text" class="form-control" onkeyup="get_angsuran()" style="margin-right: 5px;" placeholder="Angsuran" name="angsuran" id="angsuran">
                    <span class="input-group-addon" id="nilai_dibayar">Rp 0,00</span>
                  </div>
                </div>

                <div class="col-md-3" id="tempo">
                  <label><b>Jatuh Tempo</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    <input type="date" class="form-control" placeholder="jatuh_tempo" name="jatuh_tempo" id="jatuh_tempo">
                  </div>
                </div>
                <div class="col-md-2" style="padding:25px;">
                  <div onclick="actbayar()" id="proses_hutang"  class="btn btn-success">Bayar</div>
                </div>



              </div>

            </div>

          </div>

        </form>


      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>

</section><!-- /.Left col -->      
</div><!-- /.row (main row) -->


</section><!-- /.Left col -->      
</div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Pembayaran</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:11pt">Apakah anda yakin akan membayar piutang tersebut sebesar <span id="utang"></span> ?</span>

      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="proses_piutang()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    var kode=$('#nomor_nota').val();
    window.location = "<?php echo base_url().'hutang_piutang/kredit_member/detail/'; ?>"+kode;
  });
</script>
<script>
  $(document).ready(function(){
  //$("#tabel_daftar").dataTable();
  $("#angsuran").keyup(function(){
    var angsuran = $('#angsuran').val();
    var nilai_sisa = $('#nilai_sisa').val();
    var url = "<?php echo base_url().'hutang_piutang/piutang/get_rupiah'; ?>";

    if (angsuran < 0) {
      alert("Angsuran tidak boleh kurang dari 0 !");
      $("#angsuran").val("");

    }
    else if (parseInt(angsuran) >= parseInt(nilai_sisa) ) {
      alert('Angsuran Melebihi Atau Sama Dengan Sisa Hutang !, Silahkan Pilih Jenis Pembayaran Lunas.');
      $('#angsuran').val(" ");
    }
    else{
      $.ajax({
        type: "POST",
        url: url,
        data: {angsuran:angsuran},
        success: function(rupiah) {              
         $("#nilai_dibayar").html(rupiah);

       }
     });
    }
  });



})
  function actbayar() {
    var nilai_dibayar = $("#nilai_dibayar").text();
    $("#utang").text(nilai_dibayar);
    $('#modal-confirm').modal('show');
  }

  function get_angsuran(){
    var angsuran = $('#angsuran').val();
    var nilai_sisa = $('#nilai_sisa').val();
    var jumlah = parseInt(angsuran);
    var url = "<?php echo base_url().'hutang_piutang/piutang/get_rupiah'; ?>";
    if (jumlah < 0) {
      alert("Angsuran tidak boleh kurang dari 0 !");
      $("#angsuran").val("");

    }
    else{
      $.ajax({
        type: "POST",
        url: url,
        data: {angsuran:angsuran},
        success: function(rupiah) {              
         $("#nilai_dibayar").html(rupiah);

       }
     });
    }
  }

  function proses_piutang(){
    var kode_transaksi = $('#kode_penjualan').val();
    var sisa = $("#nilai_sisa").val();
    var jenis_transaksi = $("#jenis_transaksi").val();
    var jatuh_tempo = $("#jatuh_tempo").val();
    var angsuran = $("#angsuran").val();    
    var url = "<?php echo base_url().'hutang_piutang/kredit_member/simpan_piutang'?> ";

    if (jenis_transaksi == '') {
      alert("Pilih Jenis Transaksi Terlebih Dahulu !");
      $('#modal-confirm').modal('hide');
    }

    else if (angsuran == '') {
      alert("Angsuran harap di isi !");
      $('#modal-confirm').modal('hide');
    }
    else if(jenis_transaksi == 'Angsuran' && jatuh_tempo == ''){
      alert('Field Jatuh Tempo wajib di isi !');
      $('#modal-confirm').modal('hide');
    }

    else{
      $.ajax({
        type: "POST",
        url: url,
        data: { 
          kode_transaksi:kode_transaksi,
          sisa:sisa,
          jenis_transaksi:jenis_transaksi,
          angsuran:angsuran,
          jatuh_tempo:jatuh_tempo
        },
        success: function(hasil)
        {

          if(hasil==1){
            $('#modal-confirm').modal('hide');
            $(".sukses").html('<div style="font-size:1.5em" class="alert alert-success">Berhasil Melakukan Pembayaran Piutang.</div>');   
            //setTimeout(function(){
            //  $('.sukses').html('');
            //  window.location = '<?php echo base_url().'hutang_piutang/kredit_member/daftar_kredit'; ?>';                     
            //},2000);     

          }else{
            $('#modal-confirm').modal('hide');
            $(".sukses").html(hasil);
            setTimeout(function(){
              $('.sukses').html('');                    
            },2000);
          }






        }
      });
    }
  }



  $(document).ready(function(){
   $("#tempo").hide();

   $("#jenis_transaksi").change(function(){
    var pilih = $("#jenis_transaksi").val();
    var sisa = $("#nilai_sisa").val();
    if(pilih=="Lunas"){
      $("#tempo").hide(100);
      $("#angsuran").val(sisa);
      $("#angsuran").attr('readonly',true);
      get_angsuran();
      $("#nilai_dibayar").show();
    }else if(pilih=="Angsuran"){
      $("#tempo").show(100);
      $("#angsuran").val("");
      $("#nilai_dibayar").val("");
      $("#angsuran").attr('readonly',false);
      $("#nilai_dibayar").show();
      get_angsuran();
    }else if(pilih==""){
      $("#tempo").hide(100);
      $("#angsuran").attr('readonly',true);
      $("#angsuran").val();
      $("#nilai_dibayar").hide();
      $("#angsuran").val("");
      get_angsuran();
    }else{
      $("#tempo").hide(100);
      $("#angsuran").attr('readonly',true);
      $("#angsuran").val();
      $("#nilai_dibayar").hide();
      $("#angsuran").val("");
      get_angsuran();

    }
  })
 }) 
</script>