<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_member extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function daftar_piutang()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/daftar_piutang', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}

	public function daftar_piutang_member()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/list_member', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}
	public function cari_piutang()
	{
		$this->load->view('hutang_piutang/piutang_member/cari_piutang');   	
	}

	public function cari_detail_piutang_konsinyasi()
	{
		$this->load->view('hutang_piutang/piutang_member/cari_detail_piutang_konsinyasi');   	
	}

	public function cari_piutang_konsinyasi()
	{
		$this->load->view('hutang_piutang/piutang_member/cari_piutang_konsinyasi');   	
	}
	
	public function daftar_piutang_belum_lunas()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('hutang_piutang/piutang/daftar_piutang', NULL, TRUE);
		$this->load->view ('main', $data);		
	}

	public function detail($kode)
	{
		$data['aktif'] = 'hutang_piutang';
		$data['kode'] = $kode;
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/daftar_piutang', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}
	public function detail_angsuran($kode)
	{
		$data['aktif'] = 'hutang_piutang';
		$data['kode'] = $kode;
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/daftar_angsuran', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}
	public function detail_piutang()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/detail_piutang_all', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}

	public function detail_lunas()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/detail_lunas', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}

	public function proses()
	{
		$data['aktif'] = 'hutang_piutang';
		$data['konten'] = $this->load->view('hutang_piutang/piutang_member/proses_piutang', $data, true);
		$data['halaman'] = $this->load->view('hutang_piutang/piutang_member/menu', $data, true);
		$this->load->view('main', $data);


	}


	//------------------------------------------ Proses ----------------- --------------------//

	public function get_rupiah(){
		$angsuran = $this->input->post('angsuran');
		$hasil = format_rupiah($angsuran);

		echo $hasil;

	}
	public function get_satuan()
	{
		$kode_bahan = $this->input->post('kode_bahan');
		$kode_transaksi = $this->input->post('kode_transaksi');
		// $jenis_bahan = $this->input->post('jenis_bahan');
		
		$nama_bahan = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu' => $kode_bahan,'kode_penjualan' => $kode_transaksi));
		$hasil_bahan = $nama_bahan->row();
                #$bahan = $hasil_bahan->satuan_pembelian;
		
		echo json_encode($hasil_bahan);

	}

	public function get_bayar_piutang_member($kode){
		$data['kode'] = $kode ;
		$this->load->view('piutang_member/table_bayar_piutang_temp',$data);
	}
	public function update_subtotal(){
		$id = $this->input->post('id');
		$jumlah = $this->input->post('jumlah');

		$this->db->where('id', $id);
		$get = $this->db->get('opsi_bayar_piutang_member_temp');		
		$hasil_get = $get->row();

		$sub_total = $hasil_get->harga_satuan*$jumlah;

		$data['jumlah'] = $jumlah;
		$data['sub_total'] = $sub_total;
		$this->db->update('opsi_bayar_piutang_member_temp',$data,array('id'=>$id));
	}

	public function move_table(){
		$kode = $this->input->post('kode_transaksi');
		
		$this->db->where('kode_transaksi', $kode);
		$cek = $this->db->get('opsi_bayar_piutang_member_temp');
		$hasil_cek = $cek->result();

		if(empty($hasil_cek)){


			$this->db->where('kode_penjualan', $kode);
			$get = $this->db->get('opsi_transaksi_penjualan');
			$hasil = $get->result();

			foreach ($hasil as $item) {

				$data['kode_transaksi'] = $item->kode_penjualan;
				$data['kode_bahan'] = $item->kode_menu;
				$data['nama_bahan'] = $item->nama_menu;
				$data['qty_beli'] = $item->jumlah;
				$data['harga_satuan'] = $item->harga_satuan;

				$this->db->insert('opsi_bayar_piutang_member_temp', $data);
			}

		}

	}

	public function simpan_item_temp()
	{
		$masukan = $this->input->post();
		$get_real_stock= $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$masukan['kode_bahan'],'kode_member'=>$masukan['kode_member']));
		$hasil_real_stock=$get_real_stock->row();
		// echo "real Stok =".$hasil_real_stock->real_stock;
		// echo "QTY =".$masukan['jumlah'];

		//echo $this->db->last_query();

		if(@$hasil_real_stock->real_stock >= $masukan['jumlah']){
			unset($masukan['kode_member']);
			$this->db->insert('opsi_bayar_piutang_member_temp',$masukan);
			echo '<div  class="alert alert-success">Berhasil Disimpan</div>';
		}else{
			echo '<div class="alert alert-danger">Stok Tidak mencukupi</div>';
		}

	}

	public function get_temp_bayar_piutang(){
		$id = $this->input->post('id');
		$item = $this->db->get_where('opsi_bayar_piutang_member_temp',array('id'=>$id));
		$hasil_item = $item->row();
		echo json_encode($hasil_item);
	}



	public function hapus_item_temp(){
		$id = $this->input->post('id');
		$this->db->delete('opsi_bayar_piutang_member_temp',array('id'=>$id));
	}

	public function update_item_temp(){
		$update = $this->input->post();
		$data_update['jumlah']=$update['jumlah'];
		$data_update['keterangan']=$update['keterangan'];
		$this->db->update('opsi_bayar_piutang_member_temp',$data_update,array('id'=>$update['id'],'kode_bahan'=>$update['kode_bahan']));
		
		echo "sukses";
	}
	public function simpan_bayar_member(){
		$kode_transaksi=$this->input->post('kode_transaksi');
		$kode_member=$this->input->post('kode_member');

		$get_bayar_member_temp		=$this->db->get_where('opsi_bayar_piutang_member_temp',array('kode_transaksi'=>$kode_transaksi));
		$hasil_bayar_member_temp	=$get_bayar_member_temp->result();
		$total=0;
		$sub_total=0;
		foreach ($hasil_bayar_member_temp as $list) {
			$sub_total += $list->sub_total;
			$total = $sub_total;

			$get_stock = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$list->kode_bahan,'kode_member'=>$kode_member));
			$hasil_stock = $get_stock->row();
			$stock =$hasil_stock->real_stock;
			// echo $list->jumlah;
			$update_stok['real_stock']	= $stock - $list->jumlah;
			$update = $this->db->update('master_bahan_jadi', $update_stok, array('kode_bahan_jadi'=>$list->kode_bahan,'kode_member'=>$kode_member) );
			//echo $this->db->last_query();

			$insert['kode_transaksi']=$list->kode_transaksi;
			$insert['kode_bahan']=$list->kode_bahan;
			$insert['jumlah']=$list->jumlah;
			$insert['keterangan']=$list->keterangan;
			$insert['nama_bahan']=$list->nama_bahan;
			$insert['harga_satuan']=$list->harga_satuan;
			$insert['sub_total']=$list->sub_total;
			$insert_opsi=$this->db->insert('opsi_bayar_piutang_member',$insert);

			$delete=$this->db->delete('opsi_bayar_piutang_member_temp',array('kode_transaksi'=>$list->kode_transaksi));

		}

		$nominal_hutang=$this->input->post('nominal_hutang');
		echo "nominal Hutang".$nominal_hutang;
		$total 						= $sub_total;
		echo "Total Bayar". $total;

		$sisa_angsuran=$nominal_hutang - $total;
		echo "sisa". $sisa_angsuran;
		$data['angsuran']			= $total;
		$data['sisa']			    = $sisa_angsuran;
		$update_nominal_piutang 	= $this->db->update('transaksi_piutang', $data, array('kode_transaksi'=>$kode_transaksi) );
		//echo $this->db->last_query();

		$insert_opsi_angsuran['kode_transaksi']=$kode_transaksi;
		$insert_opsi_angsuran['angsuran']=$total;
		$insert_opsi_angsuran['tanggal_angsuran']=date('Y-m-d');

		$insert_opsi_piutang=$this->db->insert('opsi_piutang',$insert_opsi_angsuran);


		$query_akun = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=> '1.5.1'))->row();
		$kode_sub = $query_akun->kode_sub_kategori_akun;
		$nama_sub = $query_akun->nama_sub_kategori_akun;
		$kode_kategori = $query_akun->kode_kategori_akun;
		$nama_kategori = $query_akun->nama_kategori_akun;
		$kode_jenis = $query_akun->kode_jenis_akun;
		$nama_jenis = $query_akun->nama_jenis_akun;

		$insert_keuangan_masuk['kode_jenis_keuangan']=$kode_jenis;
		$insert_keuangan_masuk['nama_jenis_keuangan']=$nama_jenis;
		$insert_keuangan_masuk['kode_kategori_keuangan']=$kode_kategori;
		$insert_keuangan_masuk['nama_kategori_keuangan']=$nama_kategori;

		$insert_keuangan_masuk['kode_sub_kategori_keuangan']=$kode_sub;
		$insert_keuangan_masuk['nama_sub_kategori_keuangan']=$nama_sub;
		$insert_keuangan_masuk['nominal']=$total;
		$insert_keuangan_masuk['keterangan']='Angsuran Member';
		$insert_keuangan_masuk['tanggal_transaksi']=date('Y-m-d');
		$insert_keuangan_masuk['kode_referensi']=$kode_transaksi;
		$user=$this->session->userdata('astrosession');
		$insert_keuangan_masuk['id_petugas']=$user->id;
		$insert_keuangan_masuk['petugas']=$user->uname;
		$insert_masuk=$this->db->insert('keuangan_masuk',$insert_keuangan_masuk);
		echo $this->db->last_query();





		

	}

	public function simpan_piutang()
	{
		$kode_transaksi = $this->input->post('kode_transaksi');
		$sisa = $this->input->post('sisa');
		$angsuran = $this->input->post('angsuran');
		if($angsuran > $sisa){
			echo '<div style="font-size:1.5em" class="alert alert-danger">Angsuran Lebih Besar Dari Sisa.</div>';
		}else{
			$query = $this->db->get_where('transaksi_piutang',array('kode_transaksi' => $kode_transaksi) )->row();
			$cek_angsuran = $query->angsuran;
			$cek_nominal = $query->nominal_piutang;

			$opsi['kode_transaksi'] = $kode_transaksi ;
			$opsi['angsuran'] = $angsuran ;
			$opsi['tanggal_angsuran'] = date("Y-m-d") ;
			$this->db->insert('opsi_piutang',$opsi);

			$this->db->select('*') ;
			$query_akun = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=> '1.1.4'))->row();
			$kode_sub = $query_akun->kode_sub_kategori_akun;
			$nama_sub = $query_akun->nama_sub_kategori_akun;
			$kode_kategori = $query_akun->kode_kategori_akun;
			$nama_kategori = $query_akun->nama_kategori_akun;
			$kode_jenis = $query_akun->kode_jenis_akun;
			$nama_jenis = $query_akun->nama_jenis_akun;

			$get_id_petugas = $this->session->userdata('astrosession');
			$id_petugas = $get_id_petugas->id;
			$nama_petugas = $get_id_petugas->uname;

			if(empty($cek_angsuran)){
				$update_hutang['angsuran'] = $angsuran;
				$update_hutang['sisa'] = $cek_nominal - $angsuran ;
				$sukses = $this->db->update('transaksi_piutang', $update_hutang, array('kode_transaksi'=>$kode_transaksi) );

				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_transaksi ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'angsuran penjualan' ;
				$data_keu['nominal'] = $angsuran;
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_masuk", $data_keu);

			}
			else{
				$update_angsuran['angsuran'] = $cek_angsuran + $angsuran;
				$this->db->update('transaksi_piutang', $update_angsuran,  array('kode_transaksi'=>$kode_transaksi) );

				$query_angsuran = $this->db->get_where('transaksi_piutang',array('kode_transaksi' => $kode_transaksi) )->row()->angsuran;
				$update_sisa['sisa'] = $cek_nominal - $query_angsuran ;
				$sukses = $this->db->update('transaksi_piutang', $update_sisa,  array('kode_transaksi'=>$kode_transaksi) );

				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_transaksi ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'angsuran penjualan' ;
				$data_keu['nominal'] = $angsuran;
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_masuk", $data_keu);
			}

			if($sukses){
				echo 1;
			} 		
		}

	}

	
	
}
