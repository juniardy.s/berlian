      
<?php

$param = $this->input->post();
@$transaksi_hutang=$param['kode_transaksi'];
if(@$param['tgl_awal'] && @$param['tgl_akhir']){
  $tgl_awal = $param['tgl_awal'];
  $tgl_akhir = $param['tgl_akhir'];

  $this->db->where('tanggal_transaksi >=', $tgl_awal);
  $this->db->where('tanggal_transaksi <=', $tgl_akhir);

}

$this->db->limit(50,0);
$this->db->select('*');
$this->db->from('transaksi_hutang');
$this->db->where('kode_supplier',$transaksi_hutang);
// $this->db->where('sisa !=',0);
$this->db->order_by('sisa','asc');
$this->db->order_by('tanggal_transaksi','asc');
$transaksi = $this->db->get();
$hasil_transaksi = $transaksi->result();
$total=0;
// echo $this->db->last_query();
?>
<table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">

 <tr>
  <th>No</th>
  <th>Kode Transaksi</th>
  <th>Tanggal Transaksi</th>
  <th>Supplier</th>
  <th>Hutang</th>
  <th>Jatuh Tempo</th>
  <th>Status</th>
</tr>
</thead>
<tbody id="scroll_data">
  <?php
  $nomor = 1;
  $total=0;
  foreach($hasil_transaksi as $daftar){ 
    $total +=$daftar->nominal_hutang;
    $tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : TanggalIndo(@$daftar->tanggal_transaksi) ;
    ?> 
    <tr class="<?php if(@$daftar->sisa > 0){ echo "warning"; }?>">
      <td><?php echo $nomor; ?></td>
      <td><?php echo @$daftar->kode_transaksi; ?></td>
      <td><?php echo $tgl; ?></td>
      <td><?php echo @$daftar->nama_supplier; ?></td>
      <td><?php echo format_rupiah(@$daftar->sisa); ?></td>
      <td><?php if($daftar->tanggal_jatuh_tempo=='0000-00-00' || empty($daftar->tanggal_jatuh_tempo)){echo '-';}else{ echo @TanggalIndo(@$daftar->tanggal_jatuh_tempo); }?></td>
      <td><?php echo cek_status_piutang($daftar->sisa); ?></td>
    </tr>
    <?php $nomor++; } ?>

  </tbody>
  <tfoot>
    <tr>
      <th>No</th>
      <th>Kode Transaksi</th>
      <th>Tanggal Transaksi</th>
      <th>Supplier</th>
      <th>Hutang</th>
      <th>Jatuh Tempo</th>
      <th>Status</th>
    </tr>
  </tfoot>
</table>
<?php 
$get_jumlah = $this->db->get_where('transaksi_hutang',array('kode_supplier'=>$transaksi_hutang));
$jumlah = $get_jumlah->num_rows();
$jumlah = floor($jumlah/50);
?>
<div class="text-center">
  <a id="ngeload" class="btn btn-success btn-lg ngeload"><i class="fa fa-refresh"></i> Load More</a>
</div>
<input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
<input type="hidden" class="form-control pagenum " value="0">
<input type="hidden" class="form-control  " value="<?php echo $transaksi_hutang ?>" id="kode_supplier">
<script>
  $(".ngeload").click(function(){
    if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
      var pagenum = parseInt($(".pagenum").val()) + 1;
      $(".pagenum").val(pagenum);
      load_table(pagenum);
    }
  })

  function load_table(page){
    var kode = $("#kode_supplier").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'hutang/get_daftar_hutang' ?>",
      data: ({  page:$(".pagenum").val(),kode:kode}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#scroll_data").append(msg);

      }
    });
  }