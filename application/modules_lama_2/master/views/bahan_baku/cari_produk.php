<table  class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">

  <?php
  $kode_default = $this->db->get('setting_gudang');
  $hasil_unit =$kode_default->row();
  $param=$hasil_unit->kode_unit;
  $this->db->limit(50);
  $data = $this->input->post();
  if(@$data['kategori']){
    $kategori = $data['kategori'];
    $this->db->where('kode_kategori_produk',$kategori);
  }
  if(@$data['nama_produk']){
    $produk = $data['nama_produk'];
    $this->db->like('nama_bahan_baku',$produk,'both');
  }
  $this->db->order_by('nama_bahan_baku','asc');
  $bahan_baku = $this->db->get_where('master_bahan_baku',array('kode_unit' => $param,'status'=>'sendiri'));
  $hasil_bahan_baku = $bahan_baku->result();
  ?>

  <thead>
    <tr width="100%">
      <th>No</th>
      <th>Kode Bahan</th>
      <th>Nama Bahan Baku</th>
      <th>Unit</th>
      <th>Kategori</th>
      <th>Satuan Pembelian</th>
      <th>Satuan Penggunaan</th>
      <th style="width:50px">Isi Dalam 1 <br>(Satuan Pembelian)</th>
      <th>Stok Minimal</th>
      <th>HPP</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $nomor=1;
    foreach($hasil_bahan_baku as $daftar){
      ?>
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo $daftar->kode_bahan_baku; ?></td>
        <td><?php echo $daftar->nama_bahan_baku; ?></td>
        
        <td><?php echo $daftar->nama_unit; ?></td>
        <td><?php echo $daftar->nama_rak; ?></td>
        <td><?php echo $daftar->satuan_pembelian; ?></td>
        <td><?php echo $daftar->satuan_stok; ?></td>
        <td><?php echo $daftar->jumlah_dalam_satuan_pembelian; ?></td>
        <td><?php echo $daftar->stok_minimal; ?></td>
        <td><?php echo format_rupiah($daftar->stok_minimal); ?></td>
        <td><?php echo get_detail_edit_delete_string($daftar->id); ?></td>
      </tr>
      <?php $nomor++; } ?>
    </tbody>
    <tfoot>
      <tr>
       <th>No</th>
       <th>Kode Bahan Baku</th>
       <th>Nama Bahan Baku</th>
       
       <th>Unit</th>
       <th>Kategori</th>
       <th>Satuan Pembelian</th>
       <th>Satuan Penggunaan</th>
       <th style="width:50px">Isi Dalam 1<br> (Satuan Pembelian)</th>
       <th>Stok Minimal</th>
       <th>HPP</th>
       <th>Action</th>
     </tr>
   </tfoot>
 </table>
 <br><br><br><br><br><br><br><br>
 <br><br><br><br><br><br><br><br>
 <?php 
 
 if(@$data['nama_produk']){
  $produk = $data['nama_produk'];
  $this->db->like('nama_bahan_baku',$produk,'both');
}
$get_jumlah = $this->db->get_where('master_bahan_baku', array('kode_unit' => $param));
$jumlah = $get_jumlah->num_rows();
$jumlah = floor($jumlah/100);
?>
<input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
<input type="hidden" class="form-control pagenum" value="0">