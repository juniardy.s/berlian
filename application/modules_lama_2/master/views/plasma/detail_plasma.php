

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
  .ombo{
    width: 400px;
  } 

  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Detail Plasma
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>

            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>

            <div class="box-body">                   
              <div class="sukses" ></div>
              <form id="data_jabatan"  method="post">         
                <div class="row">  

                  <?php
                  $uri = $this->uri->segment(4);
                  if(!empty($uri)){
                    $data = $this->db->get_where('master_plasma',array('kode_plasma'=>$uri));
                    $hasil_data = $data->row();
                    ?>
                    <?php
                  }
                  ?>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Kode Plasma</label>
                    <input type="hidden" name="id" value="<?php echo @$hasil_data->id ?>" />
                    <input readonly="" <?php if(!empty($uri)){ echo "readonly='true'"; } ?> type="text" class="form-control" value="<?php echo @$hasil_data->kode_plasma; ?>" name="kode_plasma" id="kode_plasma"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Nama Plasma</label>
                    <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_data->nama_plasma; ?>" name="nama_plasma" id="nama_plasma"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Alamat</label>
                    <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_data->alamat; ?>" name="alamat" id="alamat"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Telepon</label>
                    <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_data->telp; ?>" name="telp" id="telp"/>
                  </div>

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Keterangan</label>
                   <!--  <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_data->keterangan; ?>" name="keterangan" id="keterangan"/> -->
                   <textarea class="form-control" readonly="" name="keterangan" id="keterangan"><?php echo @$hasil_data->keterangan; ?></textarea>
                  </div>  

                  <div class="form-group  col-xs-6">
                    <label class="gedhi">Status Plasma</label>
                    <select readonly="" class="form-control" id="status" name="status">
                      <option>Pilih</option>
                      <option value="1" <?php if(@$hasil_data->status=='1'){echo "selected";}?>>Aktif</option>
                      <option value="0" <?php if(@$hasil_data->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                    </select>
                  </div>   
                  <div class="form-group  col-xs-12">
                    <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Bahan</th>
                          <th>Harga</th>


                        </tr>
                      </thead>
                      <tbody id="opsi_plasma">
                        <?php
                        $this->db->where('kode_plasma', $uri);
                        $get_temp = $this->db->get('opsi_bahan_plasma');

                        $hasil_temp = $get_temp->result();
                        $total = 0;
                        $no = 1;
                        foreach ($hasil_temp as $temp) {
                          ?>
                          <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $temp->nama_bahan ?></td>
                            <td><?php echo format_rupiah($temp->harga) ?></td>


                          </tr>
                          <?php 
                          $total += $temp->harga;
                          $no++;
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>                     
                </div>

              </form>
            </div>
          </div>
        </div>

        <!-- /.row (main row) -->
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'master/plasma/'; ?>";
    });
    </script>
    <div id="modal-confirm-temp" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color:grey">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
          </div>
          <div class="modal-body">
            <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data komposisi tersebut ?</span>
            <input id="id-delete" type="hidden">
          </div>
          <div class="modal-footer" style="background-color:#eee">
            <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
            <button onclick="delDataTemp()" class="btn red">Ya</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">

    $(document).ready(function(){



      $(".select2").select2();
      $("#tabel_daftar").dataTable();
    });


    </script>