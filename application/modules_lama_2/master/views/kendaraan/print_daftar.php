<!DOCTYPE html>
<html>
<head>
	<title>Print Kendaraan</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <table  >
    <tr>
      <td>
        <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
      </td>
      
    </tr>
    
  </table>
  <hr>
  <table class="" width="100%" border="0" style="border-collapse: collapse;">

    <tr>

      <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Data Kendaraan</th>
    </tr>
  </table><br>
  <table class="" width="100%" border="1" style="border-collapse: collapse;">
    <?php
    $this->db->order_by('nama_kendaraan','asc');
    $kendaraan = $this->db->get('master_kendaraan');
    $hasil_kendaraan = $kendaraan->result();
    ?>
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Kendaraan</th>
        <th>Nama Kendaraan</th>
        <th>No. Kendaraan</th>
        <th>STNK</th>

        <th>Status</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $nomor = 1;

      foreach($hasil_kendaraan as $daftar){ ?> 
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo @$daftar->kode_kendaraan; ?></td>
        <td><?php echo @$daftar->nama_kendaraan; ?></td>
        <td><?php echo @$daftar->no_kendaraan; ?></td>
        <td><?php echo @$daftar->stnk; ?></td>

        <td><?php echo cek_status(@$daftar->status); ?></td>
        <td><?php echo @$daftar->keterangan; ?></td>
      </tr>
      <?php $nomor++; } ?>
    </tbody>

  </table>



  </html>