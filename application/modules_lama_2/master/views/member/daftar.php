<?php 
$post=$this->input->post();
$hasil_data_member = $this->db->get('master_member')->result();

?>
<style>
.a {
  background-color: #4c6aa5;
  padding: 5px;
  border: none;
  color: white;
  border-left: 3px solid #26a69a;
  box-shadow: 0px 2px 12px #23232347;
}
</style>
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Member
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <div id="hasil_cari">
          <!------------------------------------------------------------------------------------------------------>

          <div class="row">
            <div class="col-md-3">
             <label>Cari Data </label>
             <select  name="kode_data" id="kode_data" class="form-control" onchange="cari_data()">
              <option value="">--Pilih Data--</option>
              <option value="jalur">Jalur</option>
              <option value="member">Member</option>
            </select>
          </div>
          <div class="col-md-3">
            <div id="jalur">
              <div class="form-group">
                <label>Nama Jalur</label>
                <?php
                $kode_unit = @$hasil_unit->kode_unit;
                $get_jalur = $this->db->get_where('master_jalur',array('status'=>'1'));
                $hasil_get_jalur = $get_jalur->result();

                ?>
                <select  name="kode_jalur" id="kode_jalur" class="form-control select2">
                  <option selected="true" value="">--Pilih Jalur--</option>
                  <?php 
                  foreach($hasil_get_jalur as $daftar){    
                    ?>
                    <option <?php if(@$hasil_member->kode_jalur==$daftar->kode_jalur){ echo "selected"; } ?> value="<?php echo $daftar->kode_jalur; ?>"><?php echo $daftar->nama_jalur; ?></option>
                    <?php 
                  }
                  ?>
                </select>
              </div>
            </div>
            <div id="member">
              <div class="form-group">
                <label>Nama Member</label>
                <input type="text" id="nama_member1" name="nama_member1" class="form-control" placeholder="Nama Member">
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <a onclick="cari_member()" style="margin-top: 25px;" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>
          </div>
          <div class="col-md-3" >
            <label>Total Member</label>
            <div class="a" align="center"><h3><?php echo count($hasil_data_member); ?></h3></div>
            <br>
          </div>

        </div>
        <br>
        <br>
        <br>      
        <div class="row">
          <div class="col-md-12">
            <a onclick="print_daftar()" style="margin-top: 0px;" class="btn btn-lg blue pull-right "><i class="fa fa-print"></i> Print</a>

          </div>
        </div>
        <br>
        <div class="box-body">            
          <div class="sukses" ></div>
          
          <table class="table table-striped table-hover table-bordered" id=""  style="font-size:1.5em;">
            <thead>
              <tr>
                <th width="50px;">No</th>
                <th>Kode Member</th>
                <th>Nama Member</th>
                <th>Alamat</th>
                <th>Telepon</th>
                <th>Jalur</th>
                <th>Keterangan</th>
                <th>Status Member</th>
                <th width="220px">Action</th>
              </tr>
            </thead>
            <tbody id="daftar_member">
              <?php
              $session = $this->session->userdata('astrosession');
              $this->db->limit(50);
              $this->db->order_by('nama_member','asc');
              $get_member = $this->db->get('master_member');
              $hasil_member =$get_member->result();
              $no = 1;
              foreach($hasil_member as $item){
                if($this->session->flashdata('message')==$item->kode_member){
                  echo '<tr class="highlight" id="warna" style="background: #88cc99; display: none;">';
                }
                else{
                  echo '<tr>';
                }
                ?>
                <td><?php echo $no;?></td>
                <td><?php echo $item->kode_member; ?></td>                  
                <td><?php echo $item->nama_member; ?></td>                  
                <td><?php echo $item->alamat_member; ?></td>
                <td><?php echo $item->telp_member; ?></td>
                <td><?php echo $item->nama_jalur; ?></td>
                <td><?php echo $item->keterangan; ?></td>
                <td><?php echo cek_status($item->status_member); ?></td>
                <td align="center"><?php echo get_detail_edit_delete_string($item->id); ?></td>
              </tr>
              <?php
              $no++;
            } ?>
          </tbody>                
        </table>
        <?php 
        $get_jumlah = $this->db->get('master_member');
        $jumlah = $get_jumlah->num_rows();
        $jumlah = floor($jumlah/50);
        ?>
        <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
        <input type="hidden" class="form-control pagenum" value="0">
      </div>

    </div>

    <!------------------------------------------------------------------------------------------------------>

  </div>
</div>
</div><!-- /.col -->
</div>
</div>    
</div>  

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data member tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>



  $('#jalur').hide();
  $('#member').hide();

  function cari_data(){
    kode_data = $('#kode_data').val();

    if (kode_data =='jalur') {
      $('#jalur').show();
      $('#member').hide();
      $('#nama_member1').val('');

    }else if(kode_data =='member'){

      $('#jalur').hide();
      $('#member').show();
      $('#kode_jalur').val('');
    }else{
     $('#jalur').hide();
     $('#member').hide();
     $('#nama_member1').val('');
     $('#kode_jalur').val('');
   }
 }

 $('.btn-back').click(function(){
  $(".tunggu").show();
  window.location = "<?php echo base_url().'master/daftar'; ?>";
});
</script>

<script>
  $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function load_table(page){
    var nama_member = $("#nama_member").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'master/member/get_table' ?>",
      data: ({nama_member:nama_member, page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#daftar_member").append(msg);


      }
    });
  }
</script>

<script>
  $(document).ready(function() {

    setTimeout(function(){
      $("#warna").fadeIn('slow');
    }, 1000);
    $("a#hapus").click( function() {    
      var r =confirm("Anda yakin ingin menghapus data ini ?");
      if (r==true)  
      {
        $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'master/member/hapus' ?>",  
          cache :false,  
          data :({key:$(this).attr('key')}),
          beforeSend:function(){
            $(".tunggu").show();  
          },
          success : function(data) { 
            $(".sukses").html(data);   
            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/member/daftar' ?>";},1500);              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
        return false;
      }
      else {}        
    });


    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": true,
      "info":     false
    });
  } );
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);
  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }
  function print_daftar(){
    var kode_jalur = $("#kode_jalur").val();
    window.open('<?php echo base_url().'master/member/print_daftar'; ?>/'+kode_jalur);
  }
  function cari_member(jalur = null){

    var kode_jalur = jalur || $("#kode_jalur").val();
    var nama_member = $("#nama_member1").val();
    var url = '<?php echo base_url().'master/member/get_member'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        kode_jalur:kode_jalur,nama_member:nama_member
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $("#hasil_cari").html(msg);
        $(".tunggu").hide();
        $('#kode_data').val('');
        $('#kode_jalur').val('');
        $('#nama_member1').val('');
        $('#jalur').hide();
        $('#member').hide();
      }
    });

  }
  function delData() {
    var key = $('#id-delete').val();
    var url = '<?php echo base_url().'master/member/hapus'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        key: key
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $('#modal-confirm').modal('hide');
            // alert(id);
            window.location.reload();
          }
        });
    return false;
  }
  var jalur = "<?php echo $kode_jalur ?>";
  if (jalur)
    cari_member(jalur);

</script>
<script type="text/javascript">
 $(document).ready(function() {
  $(".select2").select2();
  // $("label").hide();

});
</script>

