<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bahan_jadi extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at h  ttp://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();      
        if ($this->session->userdata('astrosession') == FALSE) {
            redirect(base_url('authenticate'));         
        }
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_jadi/daftar_bahan_jadi', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_jadi/menu', $data, TRUE);
        $this->load->view('bahan_jadi/main', $data);    

    }

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_jadi/menu', $data, TRUE);
        $this->load->view('bahan_jadi/main', $data);        
    }

    public function tambah()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_jadi/tambah_bahan_jadi', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_jadi/menu', $data, TRUE);
        $this->load->view('bahan_jadi/main', $data);        
    }
    
    public function detail()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_jadi/detail_bahan_jadi', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_jadi/menu', $data, TRUE);
        $this->load->view('bahan_jadi/main', $data);        
    }
    public function table_temp()
    {
        $this->load->view('bahan_jadi/table_temp');        
    }

    public function table_opsi()
    {
        $this->load->view('bahan_jadi/table_temp');        
    }

    public function table_temp_harga()
    {
        $this->load->view('bahan_jadi/table_temp_harga');        
    }

    public function table_opsi_harga()
    {
        $this->load->view('bahan_jadi/table_temp_harga');        
    }

    public function table_detail()
    {
        $this->load->view('bahan_jadi/table_detail');        
    }

    public function get_satuan()
    {
        $bahan_baku = $this->input->post('kode_bahan_baku');
        $jenis = $this->input->post('jenis_bahan');
        
        if($jenis == 'bahan baku'){
            $this->db->where('kode_bahan_baku', $bahan_baku);
            $get_bahan = $this->db->get('master_bahan_baku');
        } else{
            $this->db->where('kode_bahan_setengah_jadi', $bahan_baku);
            $get_bahan = $this->db->get('master_bahan_setengah_jadi');
        }
        $hasil_bb = $get_bahan->row();
        echo json_encode($hasil_bb);
    }
    public function get_bahan()
    {
        $jenis = $this->input->post('jenis_bahan');
        if($jenis == 'bahan baku'){
            $this->db->where('status','sendiri');
            $get_bahan = $this->db->get('master_bahan_baku');
        } else{
            $this->db->where('status','sendiri');
            $get_bahan = $this->db->get('master_bahan_setengah_jadi');
        }
        $hasil_bahan = $get_bahan->result();
        echo '<option selected="true" value="">--Pilih Bahan--</option>';
        foreach ($hasil_bahan as $bahan ) {
            if($jenis == 'bahan baku'){
                echo'<option value="'.$bahan->kode_bahan_baku.'">'.$bahan->nama_bahan_baku.'</option>';
            } else{
                echo'<option value="'.$bahan->kode_bahan_setengah_jadi.'">'.$bahan->nama_bahan_setengah_jadi.'</option>';
            }
        }
        
    }

    public function tambah_temp()
    {
        $input = $this->input->post();
        $get_bahan_temp=$this->db->get_where('opsi_master_bahan_jadi_temp',array('kode_bahan'=>$input['bahan_baku'],'kode_bahan_jadi'=>$input['kode_bahan_jadi']));
        $hasil_get_bahan_temp=$get_bahan_temp->row();
        //echo count($hasil_get_bahan_temp);
        if (count($hasil_get_bahan_temp)=='1') {
            // $jumlah_awal=$hasil_get_bahan_temp->jumlah;
            // $data_update['jumlah']=$jumlah_awal + $input['jumlah'];

            // $this->db->update("opsi_master_bahan_jadi_temp", $data_update, array('kode_bahan' => $input['bahan_baku']));
            // //echo $this->db->last_query();
            echo "1";
        }else{
           $data['kode_bahan_jadi'] = $input['kode_bahan_jadi'];
           $data['kode_bahan'] = $input['bahan_baku'];
           $jenis = $this->input->post('jenis_bahan');

           if($jenis == 'bahan baku'){
            $this->db->where('kode_bahan_baku', $data['kode_bahan']);
            $get_bahan = $this->db->get('master_bahan_baku');
            $hasil_bb = $get_bahan->row();
            $data['nama_bahan'] = $hasil_bb->nama_bahan_baku;
        } else{
            $this->db->where('kode_bahan_setengah_jadi', $data['kode_bahan']);
            $get_bahan = $this->db->get('master_bahan_setengah_jadi');
            $hasil_bb = $get_bahan->row();
            $data['nama_bahan'] = $hasil_bb->nama_bahan_setengah_jadi;
        }
        $data['jenis_bahan'] = $input['jenis_bahan'];
        $data['jumlah'] = $input['jumlah'];
        $data['id_satuan'] = $input['id_satuan'];
        $data['nama_satuan'] = $input['satuan'];
        $data['hpp'] = $input['hpp'];
        $data['rumus'] = $input['rumus'];
        $data['harga'] = $input['harga'];

        $this->db->insert('opsi_master_bahan_jadi_temp', $data);
    }

}

public function tambah_temp_harga()
{
    $input = $this->input->post();


    $data['kode_bahan_jadi'] = $input['kode_bahan_jadi'];
    $data['kategori'] = $input['kategori_member'];
    $data['qty_min'] = $input['qty_min'];
    $data['qty_min'] = $input['qty_min'];
    if($input['qty_max']==""){
        $data['qty_max']= null;
    }else{

        $data['qty_max'] = $input['qty_max'];
    }
    $data['harga'] = $input['harga'];

    $this->db->insert('opsi_harga_bahan_jadi_temp', $data);
}
public function tambah_opsi_harga()
{
    $input = $this->input->post();


    $data['kode_bahan_jadi'] = $input['kode_bahan_jadi'];
    $data['kategori'] = $input['kategori_member'];
    $data['qty_min'] = $input['qty_min'];
    if($input['qty_max']==""){
        $data['qty_max']= null;
    }else{

        $data['qty_max'] = $input['qty_max'];
    }
    $data['harga'] = $input['harga'];

    $this->db->insert('opsi_harga_bahan_jadi', $data);
}

public function tambah_opsi()
{
    $input = $this->input->post();
    $data['kode_bahan_jadi'] = $input['kode_bahan_jadi'];
    $data['kode_bahan'] = $input['bahan_baku'];
    $jenis = $this->input->post('jenis_bahan');

    $get_bahan=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan'=>$input['bahan_baku'],'kode_bahan_jadi'=>$input['kode_bahan_jadi']));
    $hasil_get_bahan=$get_bahan->row();
        //echo count($hasil_get_bahan);
    if (count($hasil_get_bahan)=='1') {
        echo "1";
    }else{
        if($jenis == 'bahan baku'){
            $this->db->where('kode_bahan_baku', $data['kode_bahan']);
            $get_bahan = $this->db->get('master_bahan_baku');
            $hasil_bb = $get_bahan->row();
            $data['nama_bahan'] = $hasil_bb->nama_bahan_baku;
        } else{
            $this->db->where('kode_bahan_setengah_jadi', $data['kode_bahan']);
            $get_bahan = $this->db->get('master_bahan_setengah_jadi');
            $hasil_bb = $get_bahan->row();
            $data['nama_bahan'] = $hasil_bb->nama_bahan_setengah_jadi;
        }
        $data['jenis_bahan'] = $input['jenis_bahan'];
        $data['jumlah'] = $input['jumlah'];
        $data['id_satuan'] = $input['id_satuan'];
        $data['nama_satuan'] = $input['satuan'];
        $data['hpp'] = $input['hpp'];
        $data['rumus'] = $input['rumus'];
        $data['harga'] = $input['harga'];
        
        $this->db->insert('opsi_master_bahan_jadi', $data);
    }
    
}
public function delete_temp()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_master_bahan_jadi_temp', array('id' => $id));
}
public function delete_opsi()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_master_bahan_jadi', array('id' => $id));
}

public function delete_temp_harga()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_harga_bahan_jadi_temp', array('id' => $id));
}
public function delete_opsi_harga()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_harga_bahan_jadi', array('id' => $id));
}

public function simpan()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_bahan_jadi', 'Kode bahan baku', 'required');
    $this->form_validation->set_rules('nama_bahan_jadi', 'Nama bahan baku', 'required');
    $this->form_validation->set_rules('kode_rak', 'Kode Rak', 'required');
    $this->form_validation->set_rules('id_satuan_stok', 'Satuan', 'required');    
    $this->form_validation->set_rules('stok_minimal', 'Stok Minimal', 'required');      
        //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo warn_msg(validation_errors());

    } 
        //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $unit = $this->db->get_where('master_unit',array('kode_unit'=>$data['kode_unit']));
        $hasil_unit = $unit->row();
        $rak = $this->db->get_where('master_rak',array('kode_rak'=>$data['kode_rak']));
        $hasil_rak = $rak->row();
        $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_stok']));
        $this->db->select('kode_bahan_jadi');
        $bb = $this->db->get('master_setting');
        $hasil_bb = $bb->row();
        $hasil_satuan_stok = $satuan_stok->row();
        $data['kode_bahan_jadi'] = $hasil_bb->kode_bahan_jadi.'_'.$data['kode_bahan_jadi'];
        unset($data['jenis_bahan']);
        unset($data['bahan']);
        unset($data['jumlah']);
        unset($data['id_satuan']);
        unset($data['nama_satuan']);
        $data['satuan_stok'] = $hasil_satuan_stok->nama;
        $data['nama_unit'] = @$hasil_unit->nama_unit;
        $data['nama_rak'] = $hasil_rak->nama_rak;
        $data['real_stock'] = 0;
        $data['status'] = 'sendiri';
        $data['jenis_bahan'] = $data['jenis_produksi'];
        unset($data['jenis_produksi']);

        $this->db->where('kode_bahan_jadi', $data['kode_bahan_jadi']);
        $get_harga_temp = $this->db->get('opsi_harga_bahan_jadi_temp');
        $hasil_harga_temp = $get_harga_temp->result();
        $jumlah_harga_temp = count($hasil_harga_temp);
        if($jumlah_harga_temp > 0){
            foreach ($hasil_harga_temp as $harga_temp) {
                $opsi_harga['kode_bahan_jadi'] = $harga_temp->kode_bahan_jadi;
                $opsi_harga['kategori'] = $harga_temp->kategori;
                $opsi_harga['qty_min'] = $harga_temp->qty_min;
                $opsi_harga['qty_max'] = $harga_temp->qty_max;
                $opsi_harga['harga'] = $harga_temp->harga;

                $this->db->insert("opsi_harga_bahan_jadi", $opsi_harga);
            }

            $delete = $this->db->delete('opsi_harga_bahan_jadi_temp', array('kode_bahan_jadi' => $data['kode_bahan_jadi']));

        }

        $this->db->where('kode_bahan_jadi', $data['kode_bahan_jadi']);
        $get_temp = $this->db->get('opsi_master_bahan_jadi_temp');
        $hasil_temp = $get_temp->result();
        $jumlah_temp = count($hasil_temp);
        if($jumlah_temp > 0){
            foreach ($hasil_temp as $temp) {
                $opsi['kode_bahan_jadi'] = $temp->kode_bahan_jadi;
                $opsi['kode_bahan'] = $temp->kode_bahan;
                $opsi['nama_bahan'] = $temp->nama_bahan;
                $opsi['jenis_bahan'] = $temp->jenis_bahan;
                $opsi['jumlah'] = $temp->jumlah;
                $opsi['id_satuan'] = $temp->id_satuan;
                $opsi['nama_satuan'] = $temp->nama_satuan;
                $opsi['hpp'] = $temp->hpp;
                $opsi['rumus'] = $temp->rumus;
                $opsi['harga'] = $temp->harga;

                $this->db->insert("opsi_master_bahan_jadi", $opsi);
            }

            $delete = $this->db->delete('opsi_master_bahan_jadi_temp', array('kode_bahan_jadi' => $data['kode_bahan_jadi']));

            $this->db->insert("master_bahan_jadi", $data);
            echo json_encode(array('hasil' => 'berhasil'));
        }
        else{
            echo json_encode(array('hasil' => 'opsi'));
        }

    }
}

public function simpan_edit()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_bahan_jadi', 'Kode bahan baku', 'required');
    $this->form_validation->set_rules('nama_bahan_jadi', 'Nama bahan baku', 'required');
    $this->form_validation->set_rules('kode_rak', 'Kode Rak', 'required');
    $this->form_validation->set_rules('id_satuan_stok', 'Satuan', 'required');    
    $this->form_validation->set_rules('stok_minimal', 'Stok Minimal', 'required');      
        //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo warn_msg(validation_errors());

    } 
        //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $unit = $this->db->get_where('master_unit',array('kode_unit'=>$data['kode_unit']));
        $hasil_unit = $unit->row();
        $rak = $this->db->get_where('master_rak',array('kode_rak'=>$data['kode_rak']));
        $hasil_rak = $rak->row();
        $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_stok']));
        $hasil_satuan_stok = $satuan_stok->row();
        $data['kode_bahan_jadi'] = $data['kode_bahan_jadi'];
        unset($data['jenis_bahan']);
        unset($data['bahan']);
        unset($data['jumlah']);
        unset($data['id_satuan']);
        unset($data['nama_satuan']);
        $data['satuan_stok'] = $hasil_satuan_stok->nama;
        $data['nama_unit'] = $hasil_unit->nama_unit;
        $data['nama_rak'] = $hasil_rak->nama_rak;
        $data['real_stock'] = 0;
        $data['jenis_bahan'] = $data['jenis_produksi'];
        unset($data['jenis_produksi']);
        $this->db->where('kode_bahan_jadi', $data['kode_bahan_jadi']);
        $get_temp = $this->db->get('opsi_master_bahan_jadi');
        $hasil_temp = $get_temp->result();
        $jumlah_temp = count($hasil_temp);
        if($jumlah_temp > 0){
            $this->db->update("master_bahan_jadi", $data, array('kode_bahan_jadi' => $data['kode_bahan_jadi'], 'status'=>'sendiri'));
            echo json_encode(array('hasil' => 'berhasil'));
        }
        else{
            echo json_encode(array('hasil' => 'opsi'));
        }

    }
}   


public function hapus(){
    $kode = $this->input->post('id');
    $get_hapus = $this->db->get_where('master_bahan_jadi',array('id'=>$kode));
    $hasil_get = $get_hapus->row();
    $this->db->delete('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$hasil_get->kode_bahan_jadi));
    $this->db->delete('master_bahan_jadi',array('kode_bahan_jadi'=>$hasil_get->kode_bahan_jadi,
        'kode_unit'=>$hasil_get->kode_unit,'kode_rak'=>$hasil_get->kode_rak));
}

public function get_satuan_stok(){
    $param = $this->input->post();
    $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$param['id_pembelian']));
    $hasil_satuan_stok = $satuan_stok->row();
    $dft_satuan = $this->db->get_where('master_satuan');
    $hasil_dft_satuan = $dft_satuan->result();
        #$desa = $desa->result();
    $list = '';
    foreach($hasil_dft_satuan as $daftar){
      $list .= 
      "
      <option value='$daftar->kode'>$daftar->nama</option>
      ";
  }
  $opt = "<option selected='true' value=''>Pilih Satuan Stok</option>";
  echo $opt.$list;
}

public function get_rak()
{
    $kode_unit = $this->input->post('kode_unit');
    $opt = "<option selected='true' value=''>--Pilih Rak--</option>";
    $data = $this->db->get_where('master_rak',array('kode_unit' => $kode_unit));
    foreach ($data->result() as $key => $value) {
        $opt .= "<option value=".$value->kode_rak.">".$value->nama_rak."</option>";
    }
    echo $opt;
}

public function get_kode()
{
    $kode_bahan_jadi = $this->input->post('kode_bahan_jadi');
    $query = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi' => $kode_bahan_jadi))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}
public function get_rupiah()
{
    $hpp = $this->input->post('hpp');
    echo format_rupiah($hpp);
}
public function get_hpp()
{

    $get_kode = $this->input->post('kode_bahan_jadi');
    $table = $this->input->post('table');

    $this->db->where('kode_bahan_jadi', $get_kode);
    if($table == 'table_temp'){
        $get_temp = $this->db->get('opsi_master_bahan_jadi_temp');
    } else{
        $get_temp = $this->db->get('opsi_master_bahan_jadi');
    }
    $hasil_temp = $get_temp->result();
    $total = 0;
    $no = 1;
    foreach ($hasil_temp as $temp) {

        $total += $temp->harga;
        $no++;
    }

    $tambahan = $this->db->get('master_tambahan');
    $hasil_tambahan = $tambahan->result();
    $total_tambahan = 0;
    foreach($hasil_tambahan as $daftar){

       $total_tambahan += $daftar->harga; 
   } 
   if($total !=0){
       echo ($total * 10 / 100) + ($total + $total_tambahan);
   }else{ 
    echo "0";
}

}

public function get_table()
{
    $kode_default = $this->db->get('setting_gudang');
    $hasil_unit =$kode_default->row();
    $param=$hasil_unit->kode_unit;
    $start = (50*$this->input->post('page'));

    $data = $this->input->post();
    $this->db->limit(50, $start);
    if(@$data['nama_produk']){
        $produk = $data['nama_produk'];
        $this->db->like('nama_bahan_jadi',$produk,'both');
    }
    $bahan_jadi = $this->db->get_where('master_bahan_jadi',array('kode_unit' => $param, 'status' => 'sendiri'));
    $hasil_bahan_jadi = $bahan_jadi->result();

    $nomor = $start+1;
    foreach($hasil_bahan_jadi as $daftar){
        ?>
        <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo $daftar->kode_bahan_jadi; ?></td>
            <td><?php echo $daftar->nama_bahan_jadi; ?></td>

            <td><?php echo $daftar->nama_unit; ?></td>
            <td><?php echo $daftar->nama_rak; ?></td>
            <td><?php echo $daftar->satuan_stok; ?></td>
            <td><?php echo $daftar->stok_minimal; ?></td>
            <td><?php echo format_rupiah($daftar->hpp); ?></td>
            <td><?php echo get_detail_edit_delete_string($daftar->id); ?></td>
        </tr>
        <?php $nomor++; 
    }
}

public function get_produk(){
    $this->load->view('bahan_jadi/cari_produk');
}
public function print_produk(){
    $this->load->view('bahan_jadi/print_bahan_jadi');
}



}
