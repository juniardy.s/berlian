
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            Widget settings form goes here
          </div>
          <div class="modal-footer">
            <button type="button" class="btn blue">Save changes</button>
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="#">Home</a>
          <i class="fa fa-angle-right"></i>
        </li>
        <li>
          <a href="#">Record</a>
        </li>
      </ul>
      <div class="page-toolbar">
        <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
          <i class="icon-calendar"></i>
          &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light blue-soft" id="konsinyasi">
          <div class="visual">
            <i class="glyphicon glyphicon-tasks" ></i>
          </div>
          <div class="details" >
            <div class="number">

            </div>
            <div class="desc">
              Piutang Konsinyasi
            </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light red-soft"  id="member">
          <div class="visual">
            <i class="glyphicon glyphicon-shopping-cart"></i>
          </div>
          <div class="details">
            <div class="number">

            </div>
            <div class="desc">
              Piutang Member
            </div>
          </div>
        </a>
      </div>
                           <!--  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light green-soft"  id="asset">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-shopping-cart"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Asset
                                  </div>
                                </div>
                              </a>
                            </div> -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light purple-soft"  id="buku_besar">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-shopping-cart"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Buku Besar
                                  </div>
                                </div>
                              </a>
                            </div> -->

                          </div>
                          <!-- END DASHBOARD STATS -->
                          <div class="clearfix">
                          </div>

                        </div>
                        <script type="text/javascript">
                          $(document).ready(function(){


                            $("#konsinyasi").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'konsinyasi/' ?>";
                            });


                            $("#member").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'hutang_piutang/kredit_member/daftar_kredit' ?>";
                            });

                          });
                        </script>
