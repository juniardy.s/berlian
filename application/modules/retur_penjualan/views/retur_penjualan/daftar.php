 <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url() . 'retur_penjualan/tambah' ?>"><i class="fa fa-edit"></i> Tambah </a>
 <a style="padding:13px; margin-bottom:10px;" class="btn btn-app blue" href="<?php echo base_url() . 'retur_penjualan/' ?>"><i class="fa fa-list"></i> Daftar </a> 

 <div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
         Retur Penjualan
       </div>
       <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>
    </div>

    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>




      
      <div class="double bg-green pull-right" style="cursor:default">


        <div  style="padding-right:10px; padding-top:0px; font-size:48px; font-family:arial; font-weight:bold">


        </div>
      </div>

      <div class="box-body">            

        <div class="sukses" ></div>
        <br>
        <div class="row">
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tanggal Awal</span>
              <input type="text" class="form-control tgl" id="tgl_awal">
            </div>
          </div>
          
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tanggal Akhir</span>
              <input type="text" class="form-control tgl" id="tgl_akhir">
              <input type="hidden" class="form-control tgl" value="<?php echo $this->uri->segment(3)?>" id="kode_unit">
            </div>
          </div>                        
          <div class="col-md-2 pull-left">
            <button style="width: 190px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
          </div>
        </div>
        <br><br>
        <div id="cari_transaksi">

         <table class="table table-striped table-hover table-bordered" id="daftar_pembelian"  style="font-size:1.5em;">
          <?php
          $tanggal = date('Y-m');
          $this->db->like('tanggal_retur',$tanggal);
          $this->db->order_by('id','desc');
          $get_retur_penjualan = $this->db->get('transaksi_retur_penjualan');
          $hasil_get_retur_penjualan = $get_retur_penjualan->result();
          ?>
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Retur</th>
              <th>Kode Penjualan</th>
              <th>Tanggal</th>
              <!-- <th>Nominal Retur Penjualan</th> -->
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $nomor = 1;
            $total= 0;
            foreach($hasil_get_retur_penjualan as $daftar){ 
             ?>
             <tr>
              <td><?php echo $nomor; ?></td>
              <td><?php echo @$daftar->kode_retur; ?></td>
              <td><?php echo @$daftar->kode_penjualan; ?></td>
              <td><?php echo TanggalIndo(@$daftar->tanggal_retur);?></td>
              <!--<td><?php #echo @$daftar->nama_member; ?></td>-->
              <!-- <td align="right"><?php #echo format_rupiah($daftar->nominal_retur);?></td> -->
              <td align="center"><?php echo get_detail($daftar->kode_retur); ?></td>
            </tr>
            <?php $nomor++; } ?>
          </tbody>

        </table>
      </div>
      <!--  -->

    </div>

    <!------------------------------------------------------------------------------------------------------>

  </div>
</div>
</div><!-- /.col -->
</div>
</div>    
</div>  

<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'kasir/menu_transaksi_kasir'; ?>";
  });
</script>
<div id="modal_setting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
        <label><b><i class="fa fa-gears"></i>Setting</b></label>
      </div>

      <form id="form_setting" >
        <div class="modal-body">
          <?php
          // $setting = $this->db->get('setting_pembelian');
          // $hasil_setting = $setting->row();
          ?>

          <div class="box-body">

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label>Note</label>
                  <input type="text" name="keterangan"  class="form-control" />
                </div>

              </div>
            </div>

          </div>

          <div class="modal-footer" style="background-color:#eee">
            <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
  <script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
  <link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
  <script type="text/javascript">

    $('.tgl').Zebra_DatePicker({});


    $('#cari').click(function(){
      var tgl_awal =$("#tgl_awal").val();
      var tgl_akhir =$("#tgl_akhir").val();
      if (tgl_awal=='' || tgl_akhir==''){ 
        alert('Masukan Tanggal Awal & Tanggal Akhir..!')
      }
      else{
        $.ajax( {  
          type :"post",  
          url : "<?php echo base_url().'retur_penjualan/cari_retur_penjualan'; ?>",  
          cache :false,
          beforeSend:function(){
            $(".tunggu").show();  
          },  
          data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir},
          beforeSend:function(){
            $(".tunggu").show();  
          },
          success : function(data) {
           $(".tunggu").hide();  
           $("#cari_transaksi").html(data);
         },  
         error : function(data) {  
         // alert("das");  
       }  
     });
      }

      $('#tgl_awal').val('');
      $('#tgl_akhir').val('');

    });
  </script>
  <script>
    function setting() {
      $('#modal_setting').modal('show');
    }

    $(document).ready(function(){
      $("#daftar_pembelian").dataTable({
        "paging":   false,
        "ordering": true,
        "searching": false,
        "info":     false
      });


      $("#form_setting").submit(function(){
        var keterangan = "<?php echo base_url().'pembelian/keterangan'?>";
        $.ajax({
          type: "POST",
          url: keterangan,
          data: $('#form_setting').serialize(),
          success: function(msg)
          {
            $('#modal_setting').modal('hide');  
          }
        });
        return false;
      });

    });

  </script>
