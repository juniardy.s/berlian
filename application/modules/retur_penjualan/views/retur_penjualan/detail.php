 <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url() . 'retur_penjualan/tambah' ?>"><i class="fa fa-edit"></i> Tambah </a>
 <a style="padding:13px; margin-bottom:10px;" class="btn btn-app blue" href="<?php echo base_url() . 'retur_penjualan/' ?>"><i class="fa fa-list"></i> Daftar </a> 

 <div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
         Retur Penjualan
       </div>
       <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>
    </div>

    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>




      
      <div class="double bg-green pull-right" style="cursor:default">


        <div  style="padding-right:10px; padding-top:0px; font-size:48px; font-family:arial; font-weight:bold">


        </div>
      </div>

      <div class="box-body">            
<form id="data_form" action="" method="post">
          <div class="box-body">
            <label><h3><b>Detail Transaksi Retur Penjualan</b></h3></label>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kode Retur</label>
                  <?php
                  $kode = $this->uri->segment(3);
                  $transaksi_retur_penjualan = $this->db->get_where('transaksi_retur_penjualan',array('kode_retur'=>$kode));
                  $hasil_transaksi_retur_penjualan = $transaksi_retur_penjualan->row();
                  ?>
                  <input readonly="true" type="text" value="<?php echo @$hasil_transaksi_retur_penjualan->kode_retur; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_retur" id="kode_retur" />
                </div>
                
                <div class="form-group">
                  <label class="gedhi">Tanggal Retur</label>
                  <input type="text" value="<?php echo @TanggalIndo($hasil_transaksi_retur_penjualan->tanggal_retur); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_retur" id="tanggal_retur"/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Total Retur Penjualan</label>
                  <input type="text" value="<?php echo format_rupiah($hasil_transaksi_retur_penjualan->grand_total); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_retur" id="tanggal_retur"/>
                </div>
              </div>
              <div class="col-md-6" style="display: none;">
                <div class="form-group">
                  <label>Customer</label>
                  <input type="text" value="<?php echo $hasil_transaksi_retur_penjualan->nama_member; ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_retur" id="tanggal_retur"/>
                </div>
              </div>
              <!--<?php
              // $kode_penjualan = $hasil_transaksi_retur_penjualan->kode_penjualan;
              // $get_penjualan = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$kode_penjualan));
              // $hasil_get_penjualan = $get_penjualan->row();
              ?>-->
             <!--  <div class="col-md-6">
                <div class="form-group">
                  <label>Nominal Retur</label>
                  <input type="text" value="<?php #echo format_rupiah($hasil_transaksi_retur_penjualan->nominal_retur) ?>" readonly="true" class="form-control" placeholder="Nominal Retur" name="tanggal_retur" id="tanggal_retur"/>
                </div>
              </div>
 -->
            </div>
          </div> 
          <div id="list_transaksi_retur_penjualan">
            <div class="box-body">
              <table id="tabel_daftar" class="table table-bordered table-striped" style="font-size: 1.5em;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>QTY</th>
                    <th>Harga Satuan</th>
                    <th>Diskon (%)</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody id="tabel_temp_data_transaksi">
                  <?php
                  $kode = $this->uri->segment(3);
                  $pembelian = $this->db->get_where('opsi_transaksi_retur_penjualan',array('kode_retur'=>$kode,'status'=>'retur'));
                  $list_pembelian = $pembelian->result();
                  #echo $this->db->last_query();
                  $nomor = 1;  $total = 0;
                  foreach($list_pembelian as $daftar){ 
                    ?> 
                    <tr>
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo $daftar->nama_produk; ?></td>
                      <td><?php echo $daftar->jumlah; ?></td>
                      <td align="right"><?php echo format_rupiah($daftar->harga_satuan); ?></td>
                      <td align="center"><?php echo $daftar->diskon_item; ?></td>
                      <td align="right"><?php echo format_rupiah($daftar->subtotal); ?></td>
                    </tr>
                    <?php 
                    $total = $total + $daftar->subtotal;
                    $nomor++; 
                  } 
                  ?>
                  <tr>
                    <td colspan="4"></td>
                    <td style="font-weight:bold;">Total</td>
                    <td align="right" style="font-weight:bold;"><?php echo format_rupiah($total); ?></td>
                  </tr>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div> 
  </div>
 
</div>
</form>
<!--  -->

</div>

<!------------------------------------------------------------------------------------------------------>

</div>
</div>
</div><!-- /.col -->
</div>
</div>    
</div>  


<div id="modal_setting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
        <label><b><i class="fa fa-gears"></i>Setting</b></label>
      </div>

      <form id="form_setting" >
        <div class="modal-body">
          <?php
          // $setting = $this->db->get('setting_pembelian');
          // $hasil_setting = $setting->row();
          ?>

          <div class="box-body">

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label>Note</label>
                  <input type="text" name="keterangan"  class="form-control" />
                </div>

              </div>
            </div>

          </div>

          <div class="modal-footer" style="background-color:#eee">
            <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
  <script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
  <link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
  <script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});


  $('#cari').click(function(){
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    var kode_unit =$("#kode_unit").val();
    if (tgl_awal=='' || tgl_akhir==''){ 
      alert('Masukan Tanggal Awal & Tanggal Akhir..!')
    }
    else{
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'pembelian/cari_pembelian'; ?>",  
        cache :false,
        beforeSend:function(){
          $(".tunggu").show();  
        },  
        data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
         $(".tunggu").hide();  
         $("#cari_transaksi").html(data);
       },  
       error : function(data) {  
         // alert("das");  
       }  
     });
    }

    $('#tgl_awal').val('');
    $('#tgl_akhir').val('');

  });
  </script>
  <script>
  function setting() {
    $('#modal_setting').modal('show');
  }

  $(document).ready(function(){
    $("#daftar_pembelian").dataTable({
      "paging":   false,
      "ordering": true,
      "searching": false,
      "info":     false
    });
    

    $("#form_setting").submit(function(){
      var keterangan = "<?php echo base_url().'pembelian/keterangan'?>";
      $.ajax({
        type: "POST",
        url: keterangan,
        data: $('#form_setting').serialize(),
        success: function(msg)
        {
          $('#modal_setting').modal('hide');  
        }
      });
      return false;
    });

  });

  </script>
