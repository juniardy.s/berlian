<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class retur_penjualan extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//
	public function index()
	{
		$data['aktif']='retur_penjualan';
		$data['konten'] = $this->load->view('retur_penjualan/retur_penjualan/daftar', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function daftar_retur()
	{
		$data['aktif']='retur_penjualan';
		$data['konten'] = $this->load->view('retur_penjualan/retur_penjualan/daftar', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function tambah()
	{
		$data['aktif']='retur_penjualan';
		$data['konten'] = $this->load->view('retur_penjualan/retur_penjualan/tambah_retur', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function cari_retur_penjualan()
	{
		$this->load->view('retur_penjualan/retur_penjualan/cari_retur_penjualan');	
	}

	public function cek_jumlah()
	{
		$data = $this->input->post();

		$cek_penjualan = $this->db->get_where('opsi_transaksi_penjualan',array('id'=>$data['id']));
		$hasil_penjualan = $cek_penjualan->row();

		echo $hasil_penjualan->jumlah;		
	}

	public function detail()
	{
		$data['aktif']='retur_penjualan';
		$data['konten'] = $this->load->view('retur_penjualan/retur_penjualan/detail', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}
	public function get_transaksi_penjualan(){
		$data = $this->input->post();
		$kode_penjualan = $data['penjualan'];

		$cek_penjualan = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$kode_penjualan));
		$hasil_penjualan = $cek_penjualan->row();

		echo TanggalIndo($hasil_penjualan->tanggal_penjualan);	
	}
	public function get_nominal(){
		$nominal = $this->input->post('nominal_retur');
		

		echo format_rupiah($nominal);
	}
	public function cari_penjualan(){
		$data = $this->input->post();
		$kode_penjualan = $data['kode_penjualan'];
		$this->db->group_by('kode_penjualan');
		$cek_penjualan = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$kode_penjualan));
		$hasil_penjualan = $cek_penjualan->row();

		echo json_encode($hasil_penjualan);
	}
	public function simpan_item_temp()
	{
		$masukan = $this->input->post();
		$nama_suplier = $this->db->get_where('master_supplier',array('kode_supplier'=>$masukan['kode_supplier']));
		$hasil_nama_supplier = $nama_suplier->row();
		$masukan['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
		$subtotal = $masukan['jumlah']*$masukan['harga_satuan'];
		$masukan['subtotal'] = $subtotal;
		$this->db->insert('opsi_transaksi_retur_penjualan_temp',$masukan);
		echo "sukses";


	}
	public function update_item_temp(){
		$update = $this->input->post();
  #==================================INSERT DI OPSI RETUR TEMP===============#
		$opsi_retur['kode_retur'] = $update['kode_retur'];
		$opsi_retur['kode_produk'] = $update['kode_menu'];
		$opsi_retur['nama_produk'] = $update['nama_menu'];
		$opsi_retur['jumlah'] = $update['jumlah'];

		$harga = $this->db->get_where('opsi_transaksi_penjualan',array('kode_penjualan'=>$update['kode_penjualan'],'kode_menu'=>$update['kode_menu']));
		$hasil_harga = $harga->row();
		$opsi_retur['harga_satuan'] = $hasil_harga->harga_satuan;
		$opsi_retur['diskon_item'] = $hasil_harga->diskon_item;
		$opsi_retur['kode_satuan'] = $hasil_harga->kode_satuan;
		$opsi_retur['nama_satuan'] = $hasil_harga->nama_satuan;
		$opsi_retur['subtotal'] = $opsi_retur['harga_satuan'] * $opsi_retur['jumlah'];
		$opsi_retur['status'] = 'retur';
		$opsi_retur['kode_penjualan'] = $update['kode_penjualan']; 
		$opsi_retur['kode_kasir'] = $update['kode_kasir'];
		

		$cek_retur = $this->db->get_where('opsi_transaksi_retur_penjualan_temp',array('kode_retur'=>$update['kode_retur'],
			'kode_produk'=>$update['kode_menu']));
		$hasil_cek = $cek_retur->result();

		if(count($hasil_cek)<1){
			$this->db->insert('opsi_transaksi_retur_penjualan_temp',$opsi_retur);
		}else{
			$this->db->update('opsi_transaksi_retur_penjualan_temp',$opsi_retur,array('kode_retur'=>$update['kode_retur'],
				'kode_produk'=>$update['kode_menu']));
		}
  #=====================================================#
		$cek_retur = $this->db->get_where('opsi_transaksi_retur_penjualan_temp',array('kode_retur'=>$update['kode_retur'],
			'kode_produk'=>$update['kode_menu']));
		$hasil_cek = $cek_retur->result();
  #=======================UPDATE OPSI TRANSAKSI PENJUALAN=========#
		$get_produk = $this->db->get_where('opsi_transaksi_penjualan',array('kode_penjualan'=>$update['kode_penjualan'],
			'kode_menu'=>$update['kode_menu']));
		$hasil_produk = $get_produk->row();

		$opsi_penjualan['jumlah_retur'] = $hasil_produk->jumlah_retur + $update['jumlah'];
		$opsi_penjualan['jumlah'] = $hasil_produk->jumlah - $update['jumlah'];
		$opsi_penjualan['subtotal'] = $hasil_produk->harga_satuan * $opsi_penjualan['jumlah'];

		$this->db->update('opsi_transaksi_penjualan',$opsi_penjualan,array('kode_penjualan'=>$update['kode_penjualan'],
			'kode_menu'=>$update['kode_menu']));
  #==============================================================================#
		$this->db->select_sum('subtotal');
		$get_subtotal = $this->db->get_where('opsi_transaksi_retur_penjualan_temp',array('kode_retur'=>$update['kode_retur']));
		$hasil_subtotal = $get_subtotal->row();

		echo json_encode($hasil_subtotal);
	}
	public function get_retur_penjualan(){
		@$kode = $this->uri->segment(3);
		@$data['kode'] = @$kode ;
		$this->load->view('retur_penjualan/retur_penjualan/tabel_transaksi_temp',$data);
	}
	public function get_form(){
		@$id = $this->input->post('id');
		@$data['id'] = @$id ;
		$this->load->view('retur_penjualan/retur_penjualan/form_penyesuaian',$data);
	}
	public function batal_retur(){
		$batal = $this->input->post();

		$cek_produk = $this->db->get_where('opsi_transaksi_retur_penjualan_temp',array('kode_produk'=>$batal['kode_produk'],
			'kode_penjualan'=>$batal['kode_penjualan']));
		$hasil_cek = $cek_produk->row();

		$cek_produk_penjualan = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu'=>$batal['kode_produk'],
			'kode_penjualan'=>$batal['kode_penjualan']));
		$hasil_cek_penjualan = $cek_produk_penjualan->row();
		
		$opsi_penjualan['jumlah_retur'] = @$hasil_cek_penjualan->jumlah_retur - $hasil_cek->jumlah;
		$opsi_penjualan['jumlah'] = $hasil_cek->jumlah + $hasil_cek_penjualan->jumlah;
		$opsi_penjualan['subtotal'] = $hasil_cek_penjualan->harga_satuan * $opsi_penjualan['jumlah'];
		$this->db->update('opsi_transaksi_penjualan',$opsi_penjualan,array('kode_penjualan'=>$batal['kode_penjualan'],
			'kode_menu'=>$batal['kode_produk']));

		$this->db->delete('opsi_transaksi_retur_penjualan_temp',array('kode_produk'=>$batal['kode_produk'],
			'kode_penjualan'=>$batal['kode_penjualan']));        
	}
	public function batal_retur_penjualan(){
		$kode = $this->input->post('kode_penjualan');
		$retur_temp = $this->db->get_where('opsi_transaksi_retur_penjualan_temp',array('kode_penjualan'=>$kode));
		$hasil_temp = $retur_temp->result();


		$this->db->delete('opsi_transaksi_retur_penjualan_temp',array('kode_penjualan'=>$kode));
	}
	public function simpan_retur_penjualan(){
		$masukan = $this->input->post();

		$get_id_petugas = $this->session->userdata('astrosession');
		$id_petugas = $get_id_petugas->id;
		$nama_petugas = $get_id_petugas->uname;

		$retur_produk = $this->db->get_where('opsi_transaksi_retur_penjualan_temp',array('kode_penjualan'=>$masukan['kode_penjualan']));
		$hasil_retur_produk = $retur_produk->result();

		$total_retur_penjualan = 0;
		foreach ($hasil_retur_produk as $daftar) {
			$opsi_retur['kode_retur'] = $masukan['kode_retur'];
			$opsi_retur['kode_produk'] = $daftar->kode_produk;
			$opsi_retur['nama_produk'] = $daftar->nama_produk;
			$opsi_retur['jumlah'] = $daftar->jumlah;
			$opsi_retur['harga_satuan'] = $daftar->harga_satuan;
			$opsi_retur['diskon_item'] = $daftar->diskon_item;
			$opsi_retur['subtotal'] = $daftar->subtotal;
			$opsi_retur['status'] = $daftar->status;
			$opsi_retur['kode_penjualan'] = $daftar->kode_penjualan;
			
			$total_retur_penjualan += $daftar->subtotal;

			$tabel_opsi_retur = $this->db->insert('opsi_transaksi_retur_penjualan',$opsi_retur);

			$penjualan_cek = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$daftar->kode_penjualan));
			$hasil_penjualan_cek = $penjualan_cek->row();
			if($hasil_penjualan_cek->kategori_penjualan=='sales' || $hasil_penjualan_cek->kategori_penjualan=='sales & sopir'){
				$penjualan_opsi = $this->db->get_where('opsi_transaksi_penjualan',array('kode_penjualan'=>$daftar->kode_penjualan,'kode_menu'=>$daftar->kode_produk));
				$hasil_penjualan_opsi = $penjualan_opsi->row();

				$retur_bonus=$hasil_penjualan_opsi->komisi_sales * $daftar->jumlah;

				
				$withdraw = $this->db->get_where('transaksi_withdraw',array('kode_transaksi'=>$daftar->kode_penjualan,'kategori_petugas'=>'sales'));
				$hasil_withdraw = $withdraw->row();

				$data_witdraw['total_withdraw']=@$hasil_withdraw->total_withdraw - @$retur_bonus;
				$data_witdraw['sisa']=@$hasil_withdraw->total_withdraw - @$retur_bonus;
				$this->db->update('transaksi_withdraw',$data_witdraw,array('kode_transaksi'=>$daftar->kode_penjualan,'kategori_petugas'=>'sales'));
				
				if ($hasil_penjualan_cek->kategori_penjualan=='sales & sopir') {
					$retur_bonus_sopir=$hasil_penjualan_opsi->komisi_sopir * $daftar->jumlah;

					$withdraw_sopir = $this->db->get_where('transaksi_withdraw',array('kode_transaksi'=>$daftar->kode_penjualan,'kategori_petugas'=>'sopir'));
					$hasil_withdraw_sopir = $withdraw_sopir->row();

					$data_withdraw_sopir['total_withdraw']=@$hasil_withdraw_sopir->total_withdraw - @$retur_bonus_sopir;
					$data_withdraw_sopir['sisa']=@$hasil_withdraw_sopir->total_withdraw - @$retur_bonus_sopir;
					$this->db->update('transaksi_withdraw',$data_withdraw_sopir,array('kode_transaksi'=>$daftar->kode_penjualan,'kategori_petugas'=>'sopir'));
				}
			}

			$jumlah=$daftar->jumlah;
			for($ulang=1;$ulang <=$jumlah;$ulang++){

				$user = $this->session->userdata('astrosession');
				$id_user=$user->id;

				$this->db->select_max('id');
				$get_max_repair = $this->db->get('transaksi_stok_repair');
				$max_repair = $get_max_repair->row();

				$this->db->where('id', $max_repair->id);
				$get_repair = $this->db->get('transaksi_stok_repair');
				$repair = $get_repair->row();
				$tahun = substr(@$repair->kode_repair, 4,4);
				if(date('Y')==$tahun){
					$nomor = substr(@$repair->kode_repair, 12);
                      //echo $nomor;
					$nomor = $nomor + 1;
					$string = strlen($nomor);
					if($string == 1){
						$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_00000'.$nomor;
					} else if($string == 2){
						$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_0000'.$nomor;
					} else if($string == 3){
						$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_000'.$nomor;
					} else if($string == 4){
						$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_00'.$nomor;
					} else if($string == 5){
						$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_0'.$nomor;
					} else if($string == 6){
						$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_'.$nomor;
					}
				} else {
					$kode_repair = 'REP_'.date('Y').'_'.$id_user.'_000001';
				}
				
				$data_repair['kode_repair'] = $kode_repair;
				$data_repair['kode_retur'] = $masukan['kode_retur'];
				$data_repair['kode_penjualan'] = $daftar->kode_penjualan;
				$data_repair['tanggal_retur'] = date('Y-m-d');
				$data_repair['kode_produk'] = $daftar->kode_produk;
				$data_repair['nama_produk'] = $daftar->nama_produk;
				$data_repair['jumlah'] =1;
				$data_repair['status'] ='proses repair';
				

				$this->db->insert('transaksi_stok_repair',$data_repair);
			}
			//echo $this->db->last_query();
		}

		$penjualan_tran = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$masukan['kode_penjualan']));
		$hasil_penjualan_tran = $penjualan_tran->row();

		if($hasil_penjualan_tran->jenis_transaksi=='kredit' or $hasil_penjualan_tran->jenis_transaksi=='konsinyasi'){
			if($hasil_penjualan_tran->jenis_transaksi=='konsinyasi'){
				$kategori_member='konsinyasi';
			}else{
				$kategori_member='member';
			}
			$piutang_tran = $this->db->get_where('transaksi_piutang',array('kode_customer'=>@$hasil_penjualan_tran->kode_member,'kategori_member'=>$kategori_member,'kode_transaksi'=>$masukan['kode_penjualan']));
			$hasil_piutang_tran = $piutang_tran->row();

			$update_piutang['nominal_piutang']=@$hasil_piutang_tran->nominal_piutang-$total_retur_penjualan;
			$update_piutang['sisa']=@$hasil_piutang_tran->sisa-$total_retur_penjualan;
			
			$this->db->update('transaksi_piutang',$update_piutang,array('kode_customer'=>@$hasil_penjualan_tran->kode_member,'kategori_member'=>$kategori_member,'kode_transaksi'=>$masukan['kode_penjualan']));
			//echo $this->db->last_query();
		}

		if (count($hasil_retur_produk) > 0) {

			foreach ($hasil_retur_produk as $item) {
				$produk = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$item->kode_produk));
				$hasil_produk = $produk->row();
				// $stok['real_stock'] = $hasil_produk->real_stock + $item->jumlah;
				// $this->db->update('master_bahan_jadi',$stok,array('kode_bahan_jadi'=>$item->kode_produk,'status'=>'sendiri'));

				if($hasil_penjualan_tran->jenis_transaksi=='konsinyasi'){

					$produk_meber = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$item->kode_produk,'status'=>'member','kode_member'=>@$hasil_penjualan_tran->kode_member));
					@$hasil_produk_meber = $produk_meber->row();
					$stok_meber['real_stock'] = @$hasil_produk_meber->real_stock - $item->jumlah;
					$this->db->update('master_bahan_jadi',$stok_meber,array('kode_bahan_jadi'=>$item->kode_produk,'status'=>'member','kode_member'=>@$hasil_penjualan_tran->kode_member));
				}
				// $hpp_produk = $this->db->get_where('transaksi_stok',array('kode_bahan'=>$item->kode_produk));
				// $hasil_hpp_produk = $hpp_produk->row();

				$transaksi_stok['jenis_transaksi'] = 'retur penjualan';
				$transaksi_stok['kode_transaksi'] = $masukan['kode_retur'];
				$transaksi_stok['kategori_bahan'] = 'produk';
				$transaksi_stok['kode_bahan'] = $item->kode_produk;
				$transaksi_stok['nama_bahan'] = $item->nama_produk;
				$transaksi_stok['stok_masuk'] = $item->jumlah;
				$transaksi_stok['sisa_stok'] = $item->jumlah;
				$transaksi_stok['hpp'] = @$hasil_produk->hpp;
				$transaksi_stok['id_petugas'] = $id_petugas;
				$transaksi_stok['nama_petugas'] = $nama_petugas;
				$transaksi_stok['tanggal_transaksi'] = date("Y-m-d");
				$transaksi_stok['posisi_awal'] = 'supplier';
				$transaksi_stok['posisi_akhir'] = 'gudang';
				$transaksi_stok['status'] = 'keluar';

				$this->db->insert('transaksi_stok',$transaksi_stok);



			}
		}

		if($hasil_penjualan_tran->jenis_transaksi=='tunai'){
			$kategori = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.4.1'));
			$hasil_kategori = $kategori->row();

		  	$nominal_retur['kode_jenis_keuangan'] =$hasil_kategori->kode_jenis_akun;// '2';
		  	$nominal_retur['nama_jenis_keuangan'] = $hasil_kategori->nama_jenis_akun;//'Pengeluaran';
		  	$nominal_retur['kode_kategori_keuangan'] =$hasil_kategori->kode_kategori_akun;// '2.4';
		  	$nominal_retur['nama_kategori_keuangan'] = $hasil_kategori->nama_kategori_akun;//'Retur Penjualan';
		  	$nominal_retur['kode_sub_kategori_keuangan'] =$hasil_kategori->kode_sub_kategori_akun;// '2.4.1';
		  	//$this->db->select('nama_sub_kategori_akun');

		  	$nominal_retur['nama_sub_kategori_keuangan'] = $hasil_kategori->nama_sub_kategori_akun;
		  	$nominal_retur['nominal'] = $total_retur_penjualan;
		  	$nominal_retur['tanggal_transaksi'] = date('Y-m-d');
		  	$nominal_retur['id_petugas'] = $id_petugas;
		  	$nominal_retur['petugas'] = $nama_petugas;
		  	$nominal_retur['kode_referensi'] = $masukan['kode_retur'];

		  	$this->db->insert('keuangan_keluar',$nominal_retur);
		  }

		  $transaksi['kode_kasir'] = $masukan['kode_kasir'];
		  $transaksi['kode_retur'] = $masukan['kode_retur'];
		  $transaksi['kode_penjualan'] = $masukan['kode_penjualan'];
		  $transaksi['tanggal_retur'] = date("Y-m-d");
  			//$transaksi['kode_member'] = $masukan['kode_member'];
  			//$transaksi['nama_member'] = $masukan['nama_member'];
		  $transaksi['total_nominal'] = $total_retur_penjualan;
		  $transaksi['grand_total'] = $total_retur_penjualan;
  			//$transaksi['nominal_retur'] = $masukan['nominal_retur'];
		  $transaksi['id_petugas'] = $id_petugas;
		  $transaksi['petugas'] = $nama_petugas;
		  $transaksi['keterangan'] = '';


		  $this->db->select_max('urut');    
		  $result = $this->db->get_where('transaksi_retur_penjualan');
		  $hasil = @$result->result();
		  if($result->num_rows()) $no = ($hasil[0]->urut)+1;
		  else $no = 1;

		  if($no<10)$no = '0000'.$no;
		  else if($no<100)$no = '000'.$no;
		  else if($no<1000)$no = '00'.$no;
		  else if($no<10000)$no = '0'.$no;
		  else if($no<10000)$no = ''.$no;
                  //else if($no<100000)$no = $no;
		  $code = $no;
		  $transaksi['kode_transaksi'] = "RPEN_".$code;
		  $transaksi['urut'] = $code;
		  $this->db->insert('transaksi_retur_penjualan',$transaksi);
		  // echo $this->db->last_query();

		  $kode_keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.4.1'));
		  $hasil_kode = $kode_keuangan->row();



		  $this->db->delete('opsi_transaksi_retur_penjualan_temp',array('kode_penjualan'=>$masukan['kode_penjualan']));


		}

	}

