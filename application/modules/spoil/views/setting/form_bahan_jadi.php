<div class="row">      
  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah Spoil
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>


      <div class="portlet-body">

        <!------------------------------------------------------------------------------------------------------>

        <?php
        $kode_spoil = $this->uri->segment(3);
        ?>
        

        <div class="box-body">    

          <div class="sukses" ></div>
          <form id="data_form" action="" method="post">
            <div class="box-body">
              <div class="row">


                <div class="col-md-4">
                  <div class="box-body">
                    <div class="btn btn-app blue" style="display: block;">
                      <span style="font-weight:bold;"><i class="fa fa-barcode"></i>&nbsp;&nbsp;&nbsp; Kode Spoil &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;</span>
                      <span style="text-align:right;"><?php echo $kode_spoil ?></span>
                      <input readonly="true" type="hidden" value="<?php echo $kode_spoil ?>" class="form-control" placeholder="Kode Transaksi" name="kode_spoil" id="kode_spoil" />
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                  <div class="box-body">
                    <div class="btn btn-app blue"  style="display: block;">
                      <span style="font-weight:bold;"><i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp; Tanggal Spoil &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;</span>
                      <span style="text-align:right;" id="tanggal_spoil"><?php echo TanggalIndo(date("Y-m-d")); ?></span>
                    </div>
                  </div>
                </div>

              </div>
            </div> 
            <br><br>
            <div id="list_transaksi_pembelian">
              <div class="box-body">
                <table id="tabel_daftar" class="table table-bordered table-striped" style="font-size: 1.5em;"> 
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Produk</th>
                      <th>Nama Produk</th>
                      <th>Jumlah</th>
                      <th style="width:10%">Jumlah Spoil</th>
                      <th>Sisa</th>
                      <th>Keterangan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="tabel_spoil_temp">

                  </tbody>
                  <tfoot>

                  </tfoot>
                </table>
              </div>
            </div>
            
            <center><button type="submit" class="btn btn-success btn-lg " style="width:200px;"><i class="fa fa-save"></i> Simpan </button></center>

            <div class="box-footer clearfix"></div>

          </form>

          <!------------------------------------------------------------------------------------------------------>
        </div>
      </div><!-- /.col -->
    </div>
  </div>
  <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data spoil tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="delData()" class="btn red">Ya</button>
        </div>
      </div>
    </div>
  </div>
  <div id="modal-notif-input" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Notifikasi</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:14pt" id="text-notif">Apakah anda yakin akan menghapus data spoil tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn green" data-dismiss="modal" aria-hidden="true">OK</button>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">
<script>
  $('.btn-back').click(function(){
    window.location = "<?php echo base_url().'spoil/daftar_spoil_bahan_jadi'; ?>";
  });
</script>

<script type="text/javascript">
 $(document).ready(function(){
  var kode_spoil = $("#kode_spoil").val();
  $("#tabel_spoil_temp").load("<?php echo base_url().'spoil/get_input_spoil_bj/'; ?>"+kode_spoil);
});
 function actDelete(Object) {
  $('#id-delete').val(Object);
  $('#modal-confirm').modal('show');
}

function delData() {
  var id = $('#id-delete').val();
  var kode_spoil = $('#kode_spoil').val();
  var url = '<?php echo base_url().'spoil/hapus_bahan_temp'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id:id
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
      $("#tabel_spoil_temp").load("<?php echo base_url().'spoil/get_input_spoil_bj/'; ?>"+kode_spoil);
    }
  });
  return false;
}
<?php
$kode_default = $this->db->get('setting_gudang');
$hasil_unit =$kode_default->row();
$param =$hasil_unit->kode_unit;
$spoil =$this->db->get_where('opsi_transaksi_spoil_temp',array('kode_unit' => $param, 'kode_spoil' => $kode_spoil,'jenis_bahan' => 'sendiri'));
$list_spoil = $spoil->result();
?>
$("#data_form").submit(function(){
  notif = 0;
  <?php
  foreach($list_spoil as $daftar){ ?>
    if($(".<?php echo "input".$daftar->kode_bahan; ?>").val()==""){
      $("#text-notif").text("Masukkan jumlah produk <?php echo $daftar->nama_bahan; ?> yang akan di spoil !");
      $("#modal-notif-input").modal("show");
      notif = 1;
    }
    <?php 
  } ?>
  if(notif == 0){
    var simpan_spoil = "<?php echo base_url().'spoil/spoil/simpan_spoil_bahan_jadi'?>";
    $.ajax({
      type: "POST",
      url: simpan_spoil,
      data: $('#data_form').serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $(".sukses").html(msg);   
        setTimeout(function(){$('.sukses').html('');
         window.location = "<?php echo base_url() . 'spoil/daftar_spoil_bahan_jadi'; ?>";
       },1500);        
      }
    });
  }
  return false;
});
</script>