<?php 
  
  $code   = $this->uri->segment(3);

  $tipe   = substr($code, 0, 3);

 
    
    $query  = 'select sum(nominal_withdraw) as jml_with from transaksi_komisi_withdraw where kode_sales = "$code" group by kode_sales';
    $cari_withdraw = $this->db->query($query)->result_array();

    $withdraw      = count($cari_withdraw) > 0 ?  $cari_withdraw[0]['jml_with'] : 0;

    $query2  =  "select sum(nominal_komisi) as komisi from transaksi_komisi_revisi where kode_sales = '$code' && status_penjualan = 'lunas' group by kode_sales";
    $komisi  = $this->db->query($query2)->result_array();
    $komisi  = count($komisi) > 0 ? $komisi[0]['komisi'] : 0;

    $sisa    = $komisi - $withdraw; 

  // } else {

  //   $query  = 'select sum(nominal_withdraw) as jml_with from transaksi_komisi_withdraw where kode_sales = "$code" group by kode_sales';
  //   $cari_withdraw = $this->db->query($query)->result_array();

  //   $withdraw      = count($cari_withdraw) > 0 ?  $cari_withdraw[0]['jml_with'] : 0;

  //   $query2  =  "select sum(nominal_komisi) as komisi from transaksi_komisi_revisi where kode_sales = '$code' group by kode_sales";
  //   $komisi  = $this->db->query($query2)->result_array();
  //   $komisi  = count($komisi) > 0 ? $komisi[0]['komisi'] : 0;

  //   $sisa    = $komisi - $withdraw; 

  // }


 ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">             
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Detail Komisi <?php echo $petugas->nama ?>
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
        <?php 

          $potongan   = 0;

            // $this->db->select( 'a.* , b.* ')
            //          ->from('barang_hilang a')
            //          ->join('opsi_harga_bahan_jadi b' , 'a.kode_barang = b.kode_bahan_jadi' ,'left')
            //          ->get()->row();

          if ($tipe == 'SA_') {
              $q = $this->db->get_where('barang_hilang' , ['kode_petugas1' => $code] )->result();
            
          } else {
              $q = $this->db->where('kode_petugas1 = "'.$code.'" or kode_petugas2 = "'.$code.'" ')->get('barang_hilang')->result();
            
          }

                     // echo $this->db->last_query();
             $totalutang  = 0;
             if (count($q) < 1) {
                $totalutang = 0; 
             } else {

             
             foreach ($q as $val) {
                if ($val->kode_petugas2 != null) {
                    $potongan = 2;
                } else {
                    $potongan = 1;
                }

                $where = [
                  'kode_bahan_jadi' => $val->kode_barang 
                ];

                $get = $this->db->get_where('opsi_harga_bahan_jadi' , $where)->row();

                $harga_potongan   = $get->harga / $potongan;

                $total_sementara   = $val->jumlah_hilang * $harga_potongan ;

                $totalutang += $total_sementara;
             }
           }


         ?>

      </div>
      <div class="portlet-body">
        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row" >
            <form>
              <div class="form-group  col-xs-6">
                <label class="gedhi">Kode</label>
                <input readonly="" type="text" class="form-control" value="<?php echo $petugas->kode ?>" name="kode_petugas"/>
              </div>

              <div class="form-group  col-xs-6">
                <label class="gedhi">Nama</label>
                <input readonly="" type="text" class="form-control" value="<?php echo $petugas->nama ?>" name="nama_petugas"/>
              </div>
              <div class="form-group  col-xs-3">
                <label class="gedhi">Total Withdraw</label>
                <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($withdraw) ?>"/>
              </div>
              <div class="form-group  col-xs-3">
                <label class="gedhi">Total Pendapatan</label>
                <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($komisi) ?>"/>
              </div>
               <div class="form-group  col-xs-3">
                <label class="gedhi">Sisa</label>
                <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($sisa) ?>"/>
              </div>
              <div class="form-group  col-xs-3">
                <label style="color:red"><b>Potongan</b> </label>
                <input style="border: 2px solid red; " readonly="" type="text" class="form-control" value="<?php echo format_rupiah($totalutang) ?>"/>
              </div>
            </form>
          </div>

          <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                             <thead>
                              <tr>
                                <th>Tanggal</th>
                                <th>Asal</th>
                                <th>Jumlah Pemasukan</th>
                                <th>Besar Komisi <?php $tipe == 'SA_' ? '(%)' : 'Pieces' ?> </th>
                                <th>Komisi</th>
                                <th>Keterangan</th>
                                <th>Status Komisi</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php 
                                  $riwayat_komisi = $this->db->get_where('transaksi_komisi_revisi' , ['kode_sales' => $code ] )->result();
                                  foreach($riwayat_komisi as $daftar){ 
                                ?> 
                                  <tr>
                                    <td><?php echo $daftar->tanggal ?></td>
                                    <td>
                                        
                                        <a target="_blank" href="<?php echo base_url('kasir/print_penjualan').'/'.$daftar->kode_transaksi_asal_komisi ?>">
                                          <?php echo $daftar->kode_transaksi_asal_komisi  ?>
                                        </a>

                                    </td>
                                    <td><?php echo number_format($daftar->nominal_terbayar) ?></td>
                                    <td>
                                      <?php $format = $tipe == 'SA_' ? ' %' : ' @ Pieces'; ?>
                                      <?php echo $daftar->besar_komisi.$format ?>
                                    </td>
                                    <td> <?php echo format_rupiah($daftar->nominal_komisi)  ?></td>
                                    <td><?php echo $daftar->keterangan ?></td>
                                    <td><?php echo $daftar->status_penjualan ?></td>
                                    <!-- <td></td> -->
                                  </tr>
                               <?php } ?>
                               
                            </tbody>
                           </table>


         </div>   <!-- 
                                  <?php if ($daftar->status_komisi == 'masuk') { ?>
                                    <tr>
                                      <td><?php echo TanggalIndo(substr($daftar->tanggal_komisi, 0, 10)); ?></td>
                                      <td><?php echo strtoupper($daftar->asal_komisi); ?></td>
                                      <td><?php echo $daftar->total_barang_komisi; ?></td>
                                      <td><?php echo format_rupiah($daftar->total_komisi); ?></td>
                                      <td align="center"><a href="javascript:;" data-toggle="tooltip" data-kode="<?php echo $daftar->id ?>" title="Detail" class="btn btn-icon-only btn-circle blue btn-detail"><i class="fa fa-search"></i></a></td>
                                    </tr>
                                  <?php } else if ($daftar->status_komisi == 'withdraw') { ?>
                                    <tr style="background-color: yellow;">
                                      <td><?php echo TanggalIndo(substr($daftar->tanggal_komisi, 0, 10)); ?></td>
                                      <td colspan="2">Withdraw</td>
                                      <td colspan="2"><?php echo "- ".format_rupiah($daftar->total_komisi); ?></td>
                                    </tr>
                                  <?php } ?>
                                                              -->

       </div>
     </div>
            
            
                <div class="box box-info">
                    
                    
                    <div class="box-body">            
                        
                        

            </section><!-- /.Left col -->      
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:grey">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="color:#fff;">Detail Riwayat Komisi</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="background-color:#eee">
                <button class="btn red" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
$(".tunggu").show();
            window.location = "<?php echo base_url().'komisi/'; ?>";
          });
        </script>


<script>
$(document).ready(function(){
  $("#tabel_daftar").dataTable();

  $(document).on('click', '.btn-detail', function(){
    $kode = $(this).data('kode');
    $.get("<?php echo base_url('komisi/get_detail_riwayat_komisi') ?>/"+$kode, function( data ) {
      $('#modal-detail .modal-body').html(data);
      $('#modal-detail').modal('show');
    });
  });

  $("#modal-detail").on('hidden.bs.modal', function(){
      $('#modal-detail .modal-body').html('');
  });
})
   
</script>