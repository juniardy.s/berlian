
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content">             
        <!-- Main row -->
        <div class="row">
        <div class="col-md-6">
          <a class="btn green" href="<?php echo base_url().'penggajian/kasbon/tambah' ?>"><i class="fa fa-money"></i> Tambah Kasbon</a>
          <a class="btn blue " href="<?php echo base_url().'penggajian/kasbon/daftar' ?>"><i class="fa fa-money"></i> Daftar Kasbon</a>
        </div><br><br><br><br>
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Daftar kasbon
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>

               
                <div class="box-body">            
                  <div class="sukses" ></div>
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <?php
                    
                    $karyawan = $this->db->get('transaksi_kasbon');
                    $hasil_karyawan = $karyawan->result();
                    ?>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode Karyawan</th>
                        <th>Nama Karyawan</th>
                         <th>Tanggal Penggajian</th>
                        <th>Total Kasbon</th>
                        <th>Keterangan</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $nomor = 1;

                      foreach($hasil_karyawan as $daftar){
                        ?>
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo $daftar->kode_karyawan; ?></td>
                          <td><?php echo $daftar->nama_karyawan; ?></td>
                          <td><?php echo tanggalIndo($daftar->tanggal); ?></td>
                          <td><?php echo  format_rupiah($daftar->sisa_kasbon); ?></td>
                          <td><?php if($daftar->total_kasbon==$daftar->angsuran_kasbon){ ?>
                          <label class="label label-info">Lunas</label>
                            <?php }else{
                              ?>
                              <label class="label label-warning">Belum Lunas</label>
                              <?php
                              }?>
                          </td>
                          <td align="center">
                            <div class="btn-group">
                              <a href="<?php echo base_url().'penggajian/kasbon/detail/'.$daftar->kode_karyawan?>" id="detail" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle blue"><i class="fa fa-search"></i></a>
                            </div>
                          </td>
                        </tr>
                        <?php $nomor++; 
                      } ?>
                    </tbody>
                    
                  </table>


                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'penggajian/'; ?>";
      });
    </script>
    <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color:grey">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
          </div>
          <div class="modal-body">
            <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data jabatan tersebut ?</span>
            <input id="id-delete" type="hidden">
          </div>
          <div class="modal-footer" style="background-color:#eee">
            <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
            <button onclick="delData()" class="btn red">Ya</button>
          </div>
        </div>
      </div>
    </div>


    <script>

      function actDelete(Object) {
        $('#id-delete').val(Object);
        $('#modal-confirm').modal('show');
      }

      function delData() {
        var id = $('#id-delete').val();
        var url = '<?php echo base_url().'master/karyawan/hapus'; ?>';
        $.ajax({
          type: "POST",
          url: url,
          data: {
            id: id
          },
          beforeSend:function(){
            $(".tunggu").show();  
          },
          success: function(msg) {
            $('#modal-confirm').modal('hide');
            window.location.reload();
          }
        });
        return false;
      }

      $(document).ready(function(){
        $("#tabel_daftar").dataTable({
          "ordering": false,
          "paging": false
        });
      })

    </script>