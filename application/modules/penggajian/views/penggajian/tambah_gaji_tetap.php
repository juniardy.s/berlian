
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
      .ombo{
        width: 400px;
      } 

      </style>    
      <!-- Main content -->
      <section class="content"> 

        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Form Penggajian Karyawan Tetap
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body ">            
                  <div class="sukses" ></div>
                  
                  <form id="form">
                    <disv class="row" >
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Nama Karyawan</label>
                        <select class="form-control" id="kode_karyawan" name="kode_karyawan">
                          <option value="">Pilih</option>
                          <?php 
                          $get_karyawan=$this->db->get_where('master_karyawan',array('jenis_karyawan'=>'tetap'));
                          $hasil=$get_karyawan->result();
                          foreach ($hasil as $list) { ?>

                          <option value="<?php echo $list->kode_karyawan ?>"><?php echo $list->nama_karyawan ?></option>

                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Total Gaji</label>
                        <input readonly=""  type="text" class="form-control" value="" name="total" id="total"/>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Tanggal</label>
                        <input type="date" class="form-control" value="<?php echo date('Y-m-d') ?>" name="tanggal" id="tanggal"/>
                      </div>
                    </div>
                    <div class="row" >
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Total Kasbon</label>
                        <input readonly type="text" class="form-control" value="" name="total_kasbon" id="total_kasbon"/>
                      </div>
                      <div class="form-group  col-xs-4">
                        <label class="gedhi">Angsuran Kasbon</label>
                        <input  type="text" class="form-control" value="0" name="angsuran_kasbon" id="angsuran_kasbon"/>
                        <label id="rupiah_kasbon" class="pull-right" style="font-size:20px"></label>
                      </div>
                        <div class="form-group  col-xs-4">
                        <label class="gedhi">Total Gaji Bersih</label>
                        <input readonly=""  type="text" class="form-control" value="0" name="total_gaji" id="total_gaji"/>
                        <input  type="hidden" class="form-control" value=""  id="nominal_gaji"/>

                      </div>
                      <div class="form-group  col-xs-12">
                        <!-- <button type="submit" class="btn btn-primary pull-right">Simpan</button> -->
                        <br><br>
                        <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
                      </div>
                    </div>
                  </form>
                  
                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'penggajian/tetap'; ?>";
    });
    </script>

    <script>
    $("#kode_karyawan").change(function(){
     var url = "<?php echo base_url().'penggajian/get_gaji'; ?>";
     var kode_karyawan = $("#kode_karyawan").val();
     $.ajax( {
       type:"POST", 
       url : url,  
       cache :false,
       data :{kode_karyawan:kode_karyawan},
       dataType : 'json',
       success : function(data) {
         $("#total").val(data.gaji_tetap);
         $("#nominal_gaji").val(data.gaji_tetap);
         get_total_kasbon();
       },  
       error : function(data) {  
        alert(data);  
      }  
    });
   });

$("#angsuran_kasbon").keyup(function(){
        var angsuran_kasbon=$("#angsuran_kasbon").val();
        var total_kasbon=$("#total_kasbon").val();
        var total_gaji=$("#nominal_gaji").val();
        var angsuran_gaji=parseInt(angsuran_kasbon);

        if (angsuran_kasbon < 0) {
          alert('Nominal tidak boleh kurang dari 0');
          $('#angsuran_kasbon').val('');
          
        }else if (angsuran_gaji > total_kasbon) {
          alert('Angsuran tidak boleh melebihi Total Kasbon')
          $('#angsuran_kasbon').val('');
          
        }else{


        $.ajax({
          type: "POST",
          url: '<?php echo base_url().'penggajian/get_gaji_bersih'; ?>',
          data: {
            angsuran_kasbon:angsuran_kasbon,
           total_gaji:total_gaji
          },
          dataType : 'json',
          success: function(msg) {
            if(msg.rupiah_kasbon){
              $("#rupiah_kasbon").html(msg.rupiah_kasbon);
            }
            if(msg.gaji_bersih){
              $("#total_gaji").val(msg.gaji_bersih);
            }
          }
        });
      }
        return false;
      });



    $("#form").submit(function(){
        var angsuran_kasbon=$("#angsuran_kasbon").val();
        var total_kasbon=$("#total_kasbon").val();
        var tanggal=$("#tanggal").val();
        var total_gaji=$("#total_gaji").val();
        var total=$("#total").val();
        var kode_karyawan=$("#kode_karyawan").val();


      if (angsuran_kasbon == '' || total_kasbon == '' || tanggal == '' || total_gaji == '' || kode_karyawan == '' || total == '') {
        alert('lengkapi form');
      }else{

      $.ajax({
        type: "POST",
        url: '<?php echo base_url().'penggajian/simpan_gaji_tetap'; ?>',
        data: $(this).serialize(),
        beforeSend:function(){
        },
        success: function(msg) {
          sukses = '<div class="alert alert-success">Sudah tersimpan.</div>';
          gagal = '<div class="alert alert-danger">Periksa Nominal Withdraw</div>';
          if(msg.length=='7'){
            $(".sukses").html(gagal);
            $(".tunggu").hide();
            setTimeout(function(){$('.sukses').html('');},1500);
          }else{
            $(".sukses").html(sukses);
            $(".tunggu").hide();
            setTimeout(function(){$('.sukses').html('');
              window.location = "<?php echo base_url() . 'penggajian/tetap' ?>";},1500);
          }
        }
      });
      }
      
      return false;
    });
    function get_total_kasbon(){
      var url = "<?php echo base_url().'penggajian/get_total_kasbon'; ?>";
      var kode_karyawan = $("#kode_karyawan").val();
      $.ajax( {
       type:"POST", 
       url : url,  
       cache :false,
       data :{kode_karyawan:kode_karyawan},
       dataType : 'json',
       success : function(data) {
        if (data == '') {
        $('#total_kasbon').val('0');
        }else{
         $("#total_kasbon").val(data.sisa_kasbon);
        }
       },  
       error : function(data) {  
        alert(data);  
      }  
    });
    }
    $(document).ready(function(){
      $("#tabel_daftar").dataTable();
    })

    </script>