
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
      .ombo{
        width: 400px;
      } 

    </style>    
    <!-- Main content -->
    <section class="content"> 
      <?php
      $kode_karyawan=$this->uri->segment(3);
      $this->db->where('kode_karyawan', $kode_karyawan);
      $get_penggajian = $this->db->get('master_karyawan');
      $hasil_penggajian = $get_penggajian->row();

      $this->db->where('kode_karyawan', $kode_karyawan);
      $get_kasbon = $this->db->get('transaksi_kasbon');
      $hasil_kasbon = $get_kasbon->row();

      $this->db->select_sum('total_gaji');
      $karyawan = $this->db->get_where('opsi_transaksi_penggajian', array('kode_karyawan' =>$kode_karyawan,'status' =>'belum dibayar'));
      $hasil_karyawan = $karyawan->row();

      ?>           
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                Form Penggajian Karyawan <?php echo $hasil_penggajian->nama_karyawan ?>
              </div>
              <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
                <a href="javascript:;" class="reload">
                </a>

              </div>
            </div>
            <div class="portlet-body">
              <!------------------------------------------------------------------------------------------------------>


              <div class="box-body ">            
                <div class="sukses" ></div>
                <form id="form_withdraw">
                  <div class="row" >

                    <div class="form-group  col-md-4">
                      <label class="gedhi">Tanggal Awal</label>
                      <input  type="date" class="form-control" value="" name="tgl_awal" id="tgl_awal"/>
                    </div>
                    <div class="form-group  col-md-4">
                      <label class="gedhi">Tanggal Akhir</label>
                      <input  type="date" class="form-control" value="" name="tgl_akhir" id="tgl_akhir"/>
                    </div>
                    <div class="form-group  col-md-1">
                      <label class="gedhi"></label>
                      <button style="margin-top: 26px" id="cari_gaji" class="btn btn-warning pull-right"><i class="fa fa-search"></i>    Cari</button>
                    </div>


                  </div>
                  <div class="row" >
                    <div class="form-group  col-md-6">
                      <label class="gedhi">Kode Karyawan</label>
                      <input readonly="" type="text" class="form-control" value="<?php echo $kode_karyawan;?>" name="kode_karyawan" id="kode_karyawan"/>
                    </div>

                    <div class="form-group  col-md-6">
                      <label class="gedhi">Nama Karyawan</label>
                      <input readonly="" type="text" class="form-control" value="<?php echo @$hasil_penggajian->nama_karyawan; ?>" name="nama_karyawan" id="nama_karyawan"/>
                    </div>
                    <div class="form-group  col-md-6">
                      <label class="gedhi">Total Gaji</label>
                      <input readonly="" type="text" class="form-control" value="<?php if(!empty($hasil_karyawan->total_gaji)){
                        echo  @$hasil_karyawan->total_gaji; } else{ echo '0' ;} ?>" name="total_gaji" id="total_gaji"/>
                      </div>
                      <div class="form-group  col-md-6">
                        <label class="gedhi">Total Kasbon</label>
                        <input readonly="" type="text" class="form-control" value="<?php if(!empty($hasil_kasbon->sisa_kasbon)){echo @$hasil_kasbon->sisa_kasbon;}else{echo '0';} ?>" name="total_kasbon" id="total_gaji"/>
                      </div>
                      <div class="form-group  col-md-6">
                        <label class="gedhi">Angsuran Kasbon</label>
                        <input  type="text" class="form-control" value="0" onkeyup="get_gaji()" name="angsuran_kasbon" id="angsuran_kasbon"/>
                        <label id="rupiah_kasbon" style="font-size:20px"></label>
                      </div>
                      <div class="form-group  col-md-6">
                        <label class="gedhi">Total Gaji Bersih</label>
                        <input readonly="" type="text" class="form-control" value="<?php if(!empty($hasil_karyawan->total_gaji)){
                          echo  @$hasil_karyawan->total_gaji; } else{ echo '0' ;} ?>" name="total_gaji_bersih" id="total_gaji_bersih"/>
                        </div>

                        <div class="form-group  col-md-12">
                          <!-- <button type="submit" class="btn btn-primary pull-right">Simpan</button> -->
                          <br><br>
                          <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'penggajian/daftar'; ?>";
    });
  </script>

  <script>

    function actDelete(Object) {
      $('#id-delete').val(Object);
      $('#modal-confirm').modal('show');
    }

    function get_rupiah(){

      var angsuran_kasbon=$("#angsuran_kasbon").val();
      var total_gaji=$("#total_gaji").val();

      $.ajax({
        type: "POST",
        url: '<?php echo base_url().'penggajian/get_gaji_bersih'; ?>',
        data: {
          angsuran_kasbon:angsuran_kasbon,
          total_gaji:total_gaji
        },
        dataType : 'json',
        success: function(msg) {
          var angsuran = $('#angsuran_kasbon').val();

          var angsuran_gaji = parseInt(angsuran);

          if(angsuran < 0)
          {
            alert('Angsuran karbon Tidak Boleh Kurang Dari 0');
            $('#angsuran_kasbon').val('');
            get_rupiah();
          }
          else if(angsuran_gaji >= total_gaji)
          {
            alert('Angsuran karbon Tidak Boleh Lebih Dari Total Kasbon');
            $('#angsuran_kasbon').val('');
            get_rupiah();
          }else{
            if(msg.rupiah_kasbon){
              $("#rupiah_kasbon").html(msg.rupiah_kasbon);
            }
            if(msg.gaji_bersih){
              $("#total_gaji_bersih").val(msg.gaji_bersih);
            }
            if(msg.total_gaji){
              $("#total_gaji_bersih").val(msg.total_gaji);
            }
          }
        }
      });
      return false;
    }  


    $("#angsuran_kasbon").keyup(function(){
      var angsuran_kasbon=$("#angsuran_kasbon").val();
      var total_gaji=$("#total_gaji").val();

      $.ajax({
        type: "POST",
        url: '<?php echo base_url().'penggajian/get_gaji_bersih'; ?>',
        data: {
          angsuran_kasbon:angsuran_kasbon,
          total_gaji:total_gaji
        },
        dataType : 'json',
        success: function(msg) {
          var angsuran = $('#angsuran_kasbon').val();

          var angsuran_gaji = parseInt(angsuran);

          if(angsuran < 0)
          {
            alert('Angsuran karbon Tidak Boleh Kurang Dari 0');
            $('#angsuran_kasbon').val('');
            get_rupiah();
          }
          else if(angsuran_gaji >= total_gaji)
          {
            alert('Angsuran karbon Tidak Boleh Lebih Dari Total Kasbon');
            $('#angsuran_kasbon').val('');
            get_rupiah();
            
          }else{
            if(msg.rupiah_kasbon){
              $("#rupiah_kasbon").html(msg.rupiah_kasbon);
            }
            if(msg.gaji_bersih){
              $("#total_gaji_bersih").val(msg.gaji_bersih);
            }
            if(msg.total_gaji){
              $("#total_gaji_bersih").val(msg.total_gaji);
            }
          }
        }
      });
      return false;
    });

    $("#cari_gaji").click(function(){
      var tgl_awal=$("#tgl_awal").val();
      var tgl_akhir=$("#tgl_akhir").val();
      var kode_karyawan=$("#kode_karyawan").val();
      var angsuran_kasbon=parseInt($("#angsuran_kasbon").val());
      $.ajax({
        type: "POST",
        url: '<?php echo base_url().'penggajian/get_total_gaji'; ?>',
        data: {
         tgl_awal:tgl_awal,
         tgl_akhir:tgl_akhir,
         kode_karyawan:kode_karyawan
       },
       dataType : 'json',
       success: function(msg) {
           // alert(msg.total_gaji);

           // if(msg.total_gaji == '0' || msg.total_gaji == '' || msg.total_gaji == 'null'){
           // 	$("#total_gaji").val('0');
           // }else{	
           // $("#total_gaji").val(msg.total_gaji);
           // alert(msg.total_gaji);
           // }

           if(msg.total_gaji=='' || msg.total_gaji=='0' || msg.total_gaji=='null'){
            $("#total_gaji_bersih").val('0');
            $("#total_gaji").val('0');
          }else{
            var gaji_bersih=msg.total_gaji - angsuran_kasbon;
            $("#total_gaji_bersih").val(gaji_bersih);
           if(msg.total_gaji == null ){var tot = 0}else{  var tot =  msg.total_gaji };
            $("#total_gaji").val(tot);
          }

          //  if(angsuran_kasbon=='' || angsuran_kasbon=='0'){
          //   $("#total_gaji_bersih").val(msg.total_gaji);
          // }else{
          //   var gaji_bersih=msg.total_gaji - angsuran_kasbon;
          //   $("#total_gaji_bersih").val(gaji_bersih);
          // }

        }
      });
      return false;
    });

    $("#form_withdraw").submit(function(){

      var tgl_awal=$("#tgl_awal").val();
      var tgl_akhir=$("#tgl_akhir").val();

      if (tgl_awal == ''|| tgl_akhir == '') {
        alert("Silahkan Input Tanggal Awal dan Tanggal Akhirnya !");
      }
      else{
        $.ajax({
          type: "POST",
          url: '<?php echo base_url().'penggajian/simpan_withdraw'; ?>',
          data: $(this).serialize(),
          beforeSend:function(){
          },
          success: function(msg) {
            // sukses = '<div class="alert alert-success">Sudah tersimpan.</div>';
            // gagal = '<div class="alert alert-danger">Periksa Nominal Withdraw</div>';
            // if(msg.length=='7'){
            //   $(".sukses").html(gagal);
            //   $(".tunggu").hide();
            //   setTimeout(function(){$('.sukses').html('');},1500);
            // }else{
              $(".sukses").html(msg);
              $(".tunggu").hide();
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'penggajian/daftar' ?>";},1500);
            //}
          }
        });
      }
      return false;
    });

    $(document).ready(function(){
      $("#tabel_daftar").dataTable();
    })

  </script>