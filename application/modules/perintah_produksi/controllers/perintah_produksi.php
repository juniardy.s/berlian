<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class perintah_produksi extends MY_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
* 		http://example.com/index.php/welcome
*	- or -
* 		http://example.com/index.php/welcome/index
*	- or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see http://codeigniter.com/user_guide/general/urls.html
*/

public function __construct()
{
    parent::__construct();		
    if ($this->session->userdata('astrosession') == FALSE) {
        redirect(base_url('authenticate'));			
    }
    $this->load->library('form_validation');
    $this->load->library('session');
}	

//------------------------------------------ View Data Table----------------- --------------------//

public function index()
{
    $data['konten'] = $this->load->view('setting/daftar', NULL, TRUE);
    $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
    $this->load->view ('main', $data);
}

public function sendiri()
{
    $data['konten'] = $this->load->view('setting/form_sendiri', NULL, TRUE);
    $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
    $this->load->view ('main', $data);		
}

public function get_transaksi($kode){
    $data['kode'] = $kode ;
    $this->load->view('perintah_produksi/setting/tabel_transaksi_temp',$data);
}

public function detail()
{
    $data['konten'] = $this->load->view('setting/detail', NULL, TRUE);
    $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
    $this->load->view ('main', $data);		
}

public function get_barang(){
    $data = $this->input->post();
// echo $data['key'];
// echo $data['jenis_produksi'];
    if($data['jenis_produksi']=='sendiri'){
        if($data['key']=="setengah_jadi"){
            $this->db->where('jenis_bahan',$data['jenis_produksi']);
            $this->db->where('status','sendiri');
            $barang = $this->db->get('master_bahan_setengah_jadi');
            $hasil_barang = $barang->result();
#echo $this->db->last_query();
            echo '<option value="">Pilih Produk</option>';
            foreach($hasil_barang as $daftar){
                echo "<option value='$daftar->kode_bahan_setengah_jadi'>$daftar->nama_bahan_setengah_jadi</option>";
            }

        }else{
//$this->db->where('jenis_bahan',$data['jenis_produksi']);
            $this->db->where('jenis_bahan',$data['jenis_produksi']);
            $this->db->where('status','sendiri');
            $barang = $this->db->get('master_bahan_jadi');
            $hasil_barang = $barang->result();
#echo $this->db->last_query();
            echo '<option value="">Pilih Produk</option>';  
            foreach($hasil_barang as $daftar){
                echo "<option value='$daftar->kode_bahan_jadi'>$daftar->nama_bahan_jadi</option>";
            }
        }
    }else{
        if($data['key']=="setengah_jadi"){
            $this->db->where('kode_plasma',$data['kode_plasma']);
            $this->db->where('jenis_bahan','bahan setengah jadi');
            $barang = $this->db->get('opsi_bahan_plasma');
            $hasil_barang = $barang->result();
#echo $this->db->last_query();
            echo '<option value="">Pilih Produk</option>';
            foreach($hasil_barang as $daftar){
                echo "<option value='$daftar->kode_bahan'>$daftar->nama_bahan</option>";
            }

        }else{
            $this->db->where('kode_plasma',$data['kode_plasma']);
            $this->db->where('jenis_bahan','bahan jadi');
            $barang = $this->db->get('opsi_bahan_plasma');
            $hasil_barang = $barang->result();
#echo $this->db->last_query();
            echo '<option value="">Pilih Produk</option>';
            foreach($hasil_barang as $daftar){
                echo "<option value='$daftar->kode_bahan'>$daftar->nama_bahan</option>";
            }
        }
    }

}

public function simpan_temp(){
    $data = $this->input->post();
    if($data['perintah_produksi']=='plasma'){
        $data['kode_karyawan']=$data['kode_plasma'];
        $plasma = $this->db->get_where('master_plasma',array('kode_plasma'=>$data['kode_plasma']));
        $hasil_plasma = $plasma->row();
        $data['nama_karyawan']=$hasil_plasma->nama_plasma;
    }else{
        $karyawan = $this->db->get_where('master_karyawan',array('kode_karyawan'=>$data['kode_karyawan']));
        $hasil_karyawan = $karyawan->row();
        $data['nama_karyawan']=$hasil_karyawan->nama_karyawan;
    }
    unset($data['kode_plasma']);
    unset($data['perintah_produksi']);
    if($data['kategori_bahan']=="setengah_jadi"){
        $nama = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$data['kode_bahan'],'status'=>'sendiri'));
        $hasil_nama = $nama->row();
        $data['nama_bahan'] = $hasil_nama->nama_bahan_setengah_jadi;
    }else{
        $nama = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$data['kode_bahan'],'status'=>'sendiri'));
        $hasil_nama = $nama->row();
        $data['nama_bahan'] = $hasil_nama->nama_bahan_jadi;
    }
    $kode_bahan=$data['kode_bahan'];
    $kategori_bahan=$data['kategori_bahan'];
    $jumlah=intval($data['jumlah']);
    $afkir=intval($data['afkir']);
    $cek_stok='';

    if($kategori_bahan=='setengah_jadi'){

        $setengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));
        $hasil_setengah_jadi=$setengah_jadi->row();

        if ($hasil_setengah_jadi->real_stock < $afkir) {
            echo "1|<div class='alert alert-danger'>Stok Afkir Tidak Mencukupi</div>";
            exit();
        }

        $opsi_setengah_jadi=$this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan));
        $opsi_hasil_setengah_jadi=$opsi_setengah_jadi->result();
        $jml_sisa='cukup';
        $jml_sisa_stok='kurang';
        foreach ($opsi_hasil_setengah_jadi as $cek_sisa) {
            $sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
            $hasil_sisa_stok=$sisa_stok->row();
            $jumlah_produksi=$cek_sisa->jumlah * $jumlah;
            $sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;

            if($hasil_sisa_stok->real_stock < $jumlah_produksi){
                $jml_sisa='kurang';
            }else{
                $jml_sisa_stok='cukup';
            }
        }

        if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
            $cek_stok='cukup';
        }else{
            $cek_stok='tidakcukup';

        }
    }else if($kategori_bahan=='jadi'){
        $bahan_jadi=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));
        $hasil_bahan_jadi=$bahan_jadi->row();

        if ($hasil_bahan_jadi->real_stock < $afkir) {
            echo "1|<div class='alert alert-danger'>Stok Afkir Tidak Mencukupi</div>";
            exit();
        }

        $opsi_bahan_jadi=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
        $opsi_hasil_bahan_jadi=$opsi_bahan_jadi->result();

        $jml_sisa='cukup';
        $jml_sisa_stok='kurang';
        foreach ($opsi_hasil_bahan_jadi as $cek_sisa) {
            if($cek_sisa->jenis_bahan=='bahan baku'){
                $sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
                $hasil_sisa_stok=$sisa_stok->row();
            }else if ($cek_sisa->jenis_bahan=='bahan setengah jadi') {
                $sisa_stok=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
                $hasil_sisa_stok=$sisa_stok->row();
            }

            $jumlah_produksi=$cek_sisa->jumlah * $jumlah;
            $sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;
            if($hasil_sisa_stok->real_stock < $jumlah_produksi){
                $jml_sisa='kurang';

            }else{
                $jml_sisa_stok='cukup';
            }

        }
        if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
            $cek_stok='cukup';
        }else{

            $cek_stok='tidakcukup';
        }
    }

    if($cek_stok=='cukup'){
        $cek = $this->db->get_where('opsi_transaksi_perintah_produksi_temp',array('kode_transaksi'=>$data['kode_transaksi'],
            'kode_bahan'=>$data['kode_bahan'],'kode_karyawan'=>$data['kode_karyawan']));
        if(count($cek->row())<1){
            $this->db->insert('opsi_transaksi_perintah_produksi_temp',$data);
        }else{
            $this->db->update('opsi_transaksi_perintah_produksi_temp',$data,array('kode_transaksi'=>$data['kode_transaksi'],
                'kode_bahan'=>$data['kode_bahan']));
        }
        echo "0|";
    }else{
        echo"1|<div class='alert alert-danger'>Stok Tidak Mencukupi</div>";
    }
    

}
public function get_table()
{

    $start = (100*$this->input->post('page'));
    $this->db->limit(100, $start);
    if($this->input->post('kategori')!=""){
        $kategori = $this->input->post('kategori');
        $this->db->where('kode_kategori_produk', $kategori);
    }
    if($this->input->post('nama_produk')!=""){
        $nama_produk = $this->input->post('nama_produk');
        $this->db->like('nama_bahan_baku', $nama_produk);
    }
    $get_bb = $this->db->get_where("transaksi_perintah_produksi", array('status' => 'menunggu'));
    $hasil_bb = $get_bb->result();
    $nomor = $start+1;
    foreach ($hasil_bb as $daftar) {
        ?>   
        <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo $daftar->kode_transaksi; ?></td>
            <td><?php echo TanggalIndo($daftar->tanggal_input); ?></td>
            <td><?php echo $daftar->petugas; ?></td>
            <td><?php echo cek_jenis_produksi($daftar->jenis_produksi); ?></td>
            <td><?php echo get_detail_print_string($daftar->kode_transaksi); ?>
              <a onclick="actPrintString('<?php echo $daftar->kode_transaksi; ?>')" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle blue"><i class="fa fa-print"></i> </a>
          </td>                   
      </tr>

      <?php 
      $nomor++;
  }
}
public function cari_perintah_produksi(){
    $this->load->view('bahan_baku/cari_perintah_produksi');

}
public function cetak_perintah(){
    $this->load->view('setting/cetak_perintah');

}
public function cetak(){
    $this->load->view('setting/cetak_perintah');

}
public function hapus_bahan_jenis(){
    $data = $this->input->post();
    $this->db->delete('opsi_transaksi_perintah_produksi_temp',array('kode_transaksi'=>$data['kode_transaksi']));
}
public function hapus_bahan_temp(){
    $data = $this->input->post();
    $this->db->delete('opsi_transaksi_perintah_produksi_temp',array('id'=>$data['id']));
}
public function simpan_transaksi(){
    $data = $this->input->post();
    $user = $this->session->userdata('astrosession');

    if($data['perintah_produksi']=='sendiri'){
        $transaksi['kode_transaksi'] = $data['kode_transaksi'];
        $transaksi['tanggal_input'] = $data['tanggal_input'];
        $transaksi['petugas'] = $user->uname;
        $transaksi['status'] = "menunggu";
        $transaksi['jenis_produksi'] = $data['perintah_produksi'];

        $this->db->insert('transaksi_perintah_produksi',$transaksi);
        echo"1|<div class='alert alert-success'>Sukses Simpan Data</div>";
    }
    $get_opsi = $this->db->get_where('opsi_transaksi_perintah_produksi_temp',array('kode_transaksi'=>$data['kode_transaksi']));
    $hasil_opsi = $get_opsi->result();
    foreach($hasil_opsi as $daftar){

        if($data['perintah_produksi']=='sendiri'){
            $opsi['kode_transaksi'] = $daftar->kode_transaksi;
            $opsi['kode_bahan'] = $daftar->kode_bahan;
            $opsi['nama_bahan'] = $daftar->nama_bahan;
            $opsi['kode_karyawan'] = $daftar->kode_karyawan;
            $opsi['nama_karyawan'] = $daftar->nama_karyawan;
            $opsi['kategori_bahan'] = $daftar->kategori_bahan;
            $opsi['jumlah'] =$daftar->jumlah;
            $opsi['jumlah_sisa'] =$daftar->jumlah;
            $opsi['afkir'] =$daftar->afkir;
            $opsi['keterangan'] = $daftar->keterangan;
            $this->db->insert('opsi_transaksi_perintah_produksi',$opsi);


        }else{ 
//==================================== produksi plasma ====================
            $kategori_bahan=$daftar->kategori_bahan;
            $kode_bahan=$daftar->kode_bahan;
            $kode_plasma=$daftar->kode_karyawan;
            $nama_plasma=$daftar->nama_karyawan;
            $jumlah=$daftar->jumlah;
            $afkir=$daftar->afkir;
            if($kategori_bahan=='setengah_jadi'){

                $setengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));
                $hasil_setengah_jadi=$setengah_jadi->row();

                $opsi_setengah_jadi=$this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan));
                $opsi_hasil_setengah_jadi=$opsi_setengah_jadi->result();
                $jml_sisa='cukup';
                $jml_sisa_stok='kurang';
                foreach ($opsi_hasil_setengah_jadi as $cek_sisa) {
                    $sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
                    $hasil_sisa_stok=$sisa_stok->row();
                    $jumlah_produksi=$cek_sisa->jumlah * $jumlah;
                    $sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;

                    if($hasil_sisa_stok->real_stock < $jumlah_produksi){
                        $jml_sisa='kurang';
                    }else{
                        $jml_sisa_stok='cukup';
                    }
                }

                if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
                    foreach ($opsi_hasil_setengah_jadi as $opsi_bahan) {
                        $bahan_baku=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
                        $hasil_bahan_baku=$bahan_baku->row();
                        $jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
                        $real_stock['real_stock']=$hasil_bahan_baku->real_stock - $jumlah_produksi;

                        $update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));    

                        $cek_bb_plasma=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));
                        $hasil_cek_bb_plasma=$cek_bb_plasma->num_rows();
                        $real_stok_cek_bb_plasma=$cek_bb_plasma->row();

                        if($hasil_cek_bb_plasma > 0){
                            $real_stock_bb_plasma['real_stock']=$real_stok_cek_bb_plasma->real_stock + $jumlah_produksi;

                            $update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock_bb_plasma,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));

                        }else if($hasil_cek_bb_plasma == 0){
                            $stok_bb_plasma['kode_bahan_baku']=$hasil_bahan_baku->kode_bahan_baku;
                            $stok_bb_plasma['nama_bahan_baku']=$hasil_bahan_baku->nama_bahan_baku;
                            $stok_bb_plasma['kode_unit']=@$hasil_bahan_baku->kode_unit;
                            $stok_bb_plasma['nama_unit']=@$hasil_bahan_baku->nama_unit;
                            $stok_bb_plasma['kode_rak']=@$hasil_bahan_baku->kode_rak;
                            $stok_bb_plasma['nama_rak']=@$hasil_bahan_baku->nama_rak;
                            $stok_bb_plasma['id_satuan_stok']=@$hasil_bahan_baku->id_satuan_stok;
                            $stok_bb_plasma['satuan_stok']=@$hasil_bahan_baku->satuan_stok;
                            $stok_bb_plasma['id_satuan_pembelian']=@$hasil_bahan_baku->id_satuan_pembelian;
                            $stok_bb_plasma['satuan_pembelian']=@$hasil_bahan_baku->satuan_pembelian;
                            $stok_bb_plasma['jumlah_dalam_satuan_pembelian']=@$hasil_bahan_baku->jumlah_dalam_satuan_pembelian;
                            $stok_bb_plasma['stok_minimal']=@$hasil_bahan_baku->stok_minimal;
                            $stok_bb_plasma['hpp']=@$hasil_bahan_baku->hpp;
                            $stok_bb_plasma['kode_supplier']=@$hasil_bahan_baku->kode_supplier;
                            $stok_bb_plasma['nama_supplier']=@$hasil_bahan_baku->nama_supplier;
                            $stok_bb_plasma['real_stock']=$jumlah_produksi;
                            $stok_bb_plasma['status']='plasma';
                            $stok_bb_plasma['kode_plasma']=$kode_plasma;
                            $stok_bb_plasma['nama_plasma']=$nama_plasma;
                            $insert_bahan_setengah_jadi=$this->db->insert('master_bahan_baku',$stok_bb_plasma);
                        }


                    }

                    $cek_bahan_s_plasma=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));
                    $hasil_cek_plasma=$cek_bahan_s_plasma->num_rows();
                    $plasma_real_stok=$cek_bahan_s_plasma->row();

                    $real_stock_setengah_jadi_s['real_stock']=$hasil_setengah_jadi->real_stock - $afkir;
                    $update_bahan_setengah_jadi_s=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi_s,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));	
                    $hasil_setengah_jadi->real_stock=$real_stock_setengah_jadi_s['real_stock'];


                    if($hasil_cek_plasma > 0){
                        $real_stock_setengah_jadi['real_stock']=$plasma_real_stok->real_stock + $jumlah;
                        $real_stock_setengah_jadi['afkir']=$plasma_real_stok->afkir + $afkir;
                        $update_bahan_setengah_jadi=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));

                    }else{
                        $stok_plasma['kode_bahan_setengah_jadi']=$kode_bahan;
                        $stok_plasma['nama_bahan_setengah_jadi']=$daftar->nama_bahan;
                        $stok_plasma['kode_unit']=@$hasil_setengah_jadi->kode_unit;
                        $stok_plasma['nama_unit']=@$hasil_setengah_jadi->nama_unit;
                        $stok_plasma['kode_rak']=@$hasil_setengah_jadi->kode_rak;
                        $stok_plasma['nama_rak']=@$hasil_setengah_jadi->nama_rak;
                        $stok_plasma['id_satuan_stok']=@$hasil_setengah_jadi->id_satuan_stok;
                        $stok_plasma['satuan_stok']=@$hasil_setengah_jadi->satuan_stok;
                        $stok_plasma['stok_minimal']=@$hasil_setengah_jadi->stok_minimal;
                        $stok_plasma['hpp']=@$hasil_setengah_jadi->hpp;
                        $stok_plasma['real_stock']=$jumlah;
                        $stok_plasma['afkir']=$afkir;
                        $stok_plasma['status']='plasma';
                        $stok_plasma['kode_plasma']=$kode_plasma;
                        $stok_plasma['nama_plasma']=$nama_plasma;
                        $insert_bahan_setengah_jadi=$this->db->insert('master_bahan_setengah_jadi',$stok_plasma,array('kode_bahan_setengah_jadi'=>$kode_bahan));
                    }

                }else{
                    echo"0|<div class='alert alert-danger'>Stok Tidak Mencukupi</div>";
                }

            }else if($kategori_bahan=='jadi'){
                $bahan_jadi=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));
                $hasil_bahan_jadi=$bahan_jadi->row();

                $opsi_bahan_jadi=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
                $opsi_hasil_bahan_jadi=$opsi_bahan_jadi->result();

                $jml_sisa='cukup';
                $jml_sisa_stok='kurang';
                foreach ($opsi_hasil_bahan_jadi as $cek_sisa) {
                    if($cek_sisa->jenis_bahan=='bahan baku'){
                        $sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
                        $hasil_sisa_stok=$sisa_stok->row();
                    }else if ($cek_sisa->jenis_bahan=='bahan setengah jadi') {
                        $sisa_stok=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
                        $hasil_sisa_stok=$sisa_stok->row();
                    }

                    $jumlah_produksi=$cek_sisa->jumlah * $jumlah;
                    $sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;
                    if($hasil_sisa_stok->real_stock < $jumlah_produksi){
                        $jml_sisa='kurang';

                    }else{
                        $jml_sisa_stok='cukup';
                    }

                }
                if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
                    $opsi_bahan_jadi1=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
                    $opsi_hasil_bahan_jadi1=$opsi_bahan_jadi1->result();
                    foreach ($opsi_hasil_bahan_jadi1 as $opsi_bahan) {

                        if($opsi_bahan->jenis_bahan=='bahan baku'){
                            $bahan_baku=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
                            $hasil_bahan_baku=$bahan_baku->row();
                            $jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
                            $real_stock['real_stock']=$hasil_bahan_baku->real_stock - $jumlah_produksi;

                            $update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));    

                            $bj_bb_plasma=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));
                            $hasil_bj_bb_plasma=$bj_bb_plasma->num_rows();
                            $real_stok_bj_bb_plasma=$bj_bb_plasma->row();
                            if($hasil_bj_bb_plasma > 0 ){
                                $real_stock_bb_plasma['real_stock']=$real_stok_bj_bb_plasma->real_stock + $jumlah_produksi;
                                $update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock_bb_plasma,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));

                            }else if($hasil_bj_bb_plasma == 0 ){
                                $stok_bj_bb_plasma['kode_bahan_baku']=$hasil_bahan_baku->kode_bahan_baku;
                                $stok_bj_bb_plasma['nama_bahan_baku']=$hasil_bahan_baku->nama_bahan_baku;
                                $stok_bj_bb_plasma['kode_unit']=@$hasil_bahan_baku->kode_unit;
                                $stok_bj_bb_plasma['nama_unit']=@$hasil_bahan_baku->nama_unit;
                                $stok_bj_bb_plasma['kode_rak']=@$hasil_bahan_baku->kode_rak;
                                $stok_bj_bb_plasma['nama_rak']=@$hasil_bahan_baku->nama_rak;
                                $stok_bj_bb_plasma['id_satuan_stok']=@$hasil_bahan_baku->id_satuan_stok;
                                $stok_bj_bb_plasma['satuan_stok']=@$hasil_bahan_baku->satuan_stok;
                                $stok_bj_bb_plasma['id_satuan_pembelian']=@$hasil_bahan_baku->id_satuan_pembelian;
                                $stok_bj_bb_plasma['satuan_pembelian']=@$hasil_bahan_baku->satuan_pembelian;
                                $stok_bj_bb_plasma['jumlah_dalam_satuan_pembelian']=@$hasil_bahan_baku->jumlah_dalam_satuan_pembelian;
                                $stok_bj_bb_plasma['stok_minimal']=@$hasil_bahan_baku->stok_minimal;
                                $stok_bj_bb_plasma['hpp']=@$hasil_bahan_baku->hpp;
                                $stok_bj_bb_plasma['real_stock']=$jumlah_produksi;
                                $stok_bj_bb_plasma['status']='plasma';
                                $stok_bj_bb_plasma['kode_plasma']=$kode_plasma;
                                $stok_bj_bb_plasma['nama_plasma']=$nama_plasma;
                                $insert_bahan_setengah_jadi=$this->db->insert('master_bahan_baku',$stok_bj_bb_plasma);
                            }



                        }else if ($opsi_bahan->jenis_bahan=='bahan setengah jadi') {
                            $bahan_stengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
                            $hasil_bahan_stengah_jadi=$bahan_stengah_jadi->row();
                            $jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
                            $real_stock['real_stock']=$hasil_bahan_stengah_jadi->real_stock - $jumlah_produksi;

                            $update_bahan_baku=$this->db->update('master_bahan_setengah_jadi',$real_stock,array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));  

                            $bj_bsj_plasma=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));
                            $hasil_bj_bsj_plasma=$bj_bsj_plasma->num_rows();
                            $real_stok_bj_bsj_plasma=$bj_bsj_plasma->row();
                            if($hasil_bj_bsj_plasma > 0  ){
                                $real_stock_bsj_plasma['real_stock']=$real_stok_bj_bsj_plasma->real_stock + $jumlah_produksi;
                                $update_bahan_baku=$this->db->update('master_bahan_setengah_jadi',$real_stock_bsj_plasma,array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));
                            }else if($hasil_bj_bsj_plasma == 0 ){
                                $stok_bj_bsj_plasma['kode_bahan_setengah_jadi']=$hasil_bahan_stengah_jadi->kode_bahan_setengah_jadi;
                                $stok_bj_bsj_plasma['nama_bahan_setengah_jadi']=$hasil_bahan_stengah_jadi->nama_bahan_setengah_jadi;
                                $stok_bj_bsj_plasma['kode_unit']=@$hasil_bahan_stengah_jadi->kode_unit;
                                $stok_bj_bsj_plasma['nama_unit']=@$hasil_bahan_stengah_jadi->nama_unit;
                                $stok_bj_bsj_plasma['kode_rak']=@$hasil_bahan_stengah_jadi->kode_rak;
                                $stok_bj_bsj_plasma['nama_rak']=@$hasil_bahan_stengah_jadi->nama_rak;
                                $stok_bj_bsj_plasma['id_satuan_stok']=@$hasil_bahan_stengah_jadi->id_satuan_stok;
                                $stok_bj_bsj_plasma['satuan_stok']=@$hasil_bahan_stengah_jadi->satuan_stok;
                                $stok_bj_bsj_plasma['hpp']=@$hasil_bahan_stengah_jadi->hpp;
                                $stok_bj_bsj_plasma['stok_minimal']=@$hasil_bahan_stengah_jadi->stok_minimal;
                                $stok_bj_bsj_plasma['real_stock']=$jumlah_produksi;
                                $stok_bj_bsj_plasma['status']='plasma';
                                $stok_bj_bsj_plasma['kode_plasma']=$kode_plasma;
                                $stok_bj_bsj_plasma['nama_plasma']=$nama_plasma;
                                $insert_bahan_setengah_jadi=$this->db->insert('master_bahan_setengah_jadi',$stok_bj_bsj_plasma);
                            }


                        }


                    }

                    $cek_bahan_j_plasma=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));
                    $hasil_cek_plasma=$cek_bahan_j_plasma->num_rows();
                    $plasma_real_stok=$cek_bahan_j_plasma->row();

                    $real_stock_jadi_s['real_stock']=$hasil_bahan_jadi->real_stock - $afkir;
                    $update_bahan_jadi_s=$this->db->update('master_bahan_jadi',$real_stock_jadi_s,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));	
                    $hasil_bahan_jadi->real_stock=$real_stock_jadi_s['real_stock'];

                    if($hasil_cek_plasma > 0){
                        $real_stock_jadi['real_stock']=$plasma_real_stok->real_stock + $jumlah;
                        $real_stock_jadi['afkir']=$plasma_real_stok->afkir + $afkir;
                        $update_bahan_jadi=$this->db->update('master_bahan_jadi',$real_stock_jadi,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_plasma));    
                    }else{
                        $stok_plasma['kode_bahan_jadi']=$kode_bahan;
                        $stok_plasma['nama_bahan_jadi']=$daftar->nama_bahan;
                        $stok_plasma['kode_unit']=@$hasil_bahan_jadi->kode_unit;
                        $stok_plasma['nama_unit']=@$hasil_bahan_jadi->nama_unit;
                        $stok_plasma['kode_rak']=@$hasil_bahan_jadi->kode_rak;
                        $stok_plasma['nama_rak']=@$hasil_bahan_jadi->nama_rak;
                        $stok_plasma['id_satuan_stok']=@$hasil_bahan_jadi->id_satuan_stok;
                        $stok_plasma['satuan_stok']=@$hasil_bahan_jadi->satuan_stok;
                        $stok_plasma['hpp']=@$hasil_bahan_jadi->hpp;
                        $stok_plasma['real_stock']=$jumlah;
                        $stok_plasma['status']='plasma';
                        $stok_plasma['kode_plasma']=$kode_plasma;
                        $stok_plasma['nama_plasma']=$nama_plasma;
                        $insert_bahan_setengah_jadi=$this->db->insert('master_bahan_jadi',$stok_plasma,array('kode_bahan_jadi'=>$kode_bahan));
                    }


                }else{
                    echo"0|<div class='alert alert-danger'>Stok Tidak Mencukupi</div>";
                }
            }

        }
        if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
            $opsi['kode_transaksi'] = $daftar->kode_transaksi;
            $opsi['kode_bahan'] = $daftar->kode_bahan;
            $opsi['nama_bahan'] = $daftar->nama_bahan;
            $opsi['kode_karyawan'] = $daftar->kode_karyawan;
            $opsi['nama_karyawan'] = $daftar->nama_karyawan;
            $opsi['kategori_bahan'] = $daftar->kategori_bahan;
            $opsi['jumlah'] =$daftar->jumlah;
            $opsi['afkir'] =$daftar->afkir;
            $opsi['jumlah_sisa'] =$daftar->jumlah;
            $opsi['keterangan'] = $daftar->keterangan;
            $this->db->insert('opsi_transaksi_perintah_produksi',$opsi);
        }
    }
    if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
        $transaksi['kode_transaksi'] = $data['kode_transaksi'];
        $transaksi['tanggal_input'] = $data['tanggal_input'];
        $transaksi['petugas'] = $user->uname;
        $transaksi['status'] = "menunggu";
        $transaksi['jenis_produksi'] = $data['perintah_produksi'];

        $this->db->insert('transaksi_perintah_produksi',$transaksi);
        echo"1|<div class='alert alert-success'>Sukses Simpan Data</div>";
    }
    $this->db->delete('opsi_transaksi_perintah_produksi_temp',array('kode_transaksi'=>$data['kode_transaksi']));

}

public function get_search_daftar(){
    $this->load->view('setting/daftar_by_tanggal');
}
}
