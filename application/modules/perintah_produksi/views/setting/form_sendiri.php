<div class="">
  <div class="page-content">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">Perintah Produksi</div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <form id="data_form">
          <table class="table">
            <tr>
              <?php
              $this->db->select_max('id');
              $get_max_po = $this->db->get('transaksi_perintah_produksi');
              $max_po = $get_max_po->row();

              $user = $this->session->userdata('astrosession');
              $id_user=$user->id;

              $this->db->where('id', $max_po->id);
              $get_po = $this->db->get('transaksi_perintah_produksi');
              $po = $get_po->row();
              $tahun = substr(@$po->kode_transaksi, 3,4);
              if(date('Y')==$tahun){
                $nomor = substr(@$po->kode_transaksi, 11);
//echo $nomor;
                $nomor = $nomor + 1;
                $string = strlen($nomor);
                if($string == 1){
                  $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                } else if($string == 2){
                  $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                } else if($string == 3){
                  $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_000'.$nomor;
                } else if($string == 4){
                  $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_00'.$nomor;
                } else if($string == 5){
                  $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_0'.$nomor;
                } else if($string == 6){
                  $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_'.$nomor;
                }
              } else {
                $kode_trans = 'PP_'.date('Y').'_'.$id_user.'_000001';
              }

              ?>
              <td><label>Kode Transaksi</label></td>
              <td><input type="text" name="kode_transaksi" id="kode_transaksi" readonly value="<?php echo $kode_trans;?>" class='form-control' required /></td>
            </tr>
            <tr>
              <td><label>Tanggal</label></td>
              <td><input type="text" name="tanggal_input"  readonly value="<?php echo date('Y-m-d');?>" class='form-control' required /></td>
            </tr>
            <tr>
              <td><label>Perintah Produksi</label></td>
              <td>
                <select name="perintah_produksi"  id="perintah_produksi" required class="form-control select2">
                  <option value="">--Pilih Perintah Produksi--</option>
                  <option value="sendiri">Factory</option>
                  <option value="plasma">Plasma</option>
                </select>
                <input type="text" id="view_jenis"   readonly value="" class='form-control'  />
              </td>
            </tr>
            <tr id="opsi">
              <td><label id="pilih">Pilih Karyawan</label></td>
              <td>
                <select  name="kode_karyawan"  id="karyawan" required class="form-control select2">
                  <option value="">--Pilih Karyawan--</option>
                  <?php
                  $karyawan = $this->db->query(" SELECT * FROM master_karyawan where status='1' and jenis_karyawan='borongan'");
                  $hasil_karyawan = $karyawan->result();
                  foreach ($hasil_karyawan as $data) {
                    ?>
                    <option value="<?php echo $data->kode_karyawan; ?>"><?php echo $data->nama_karyawan; ?></option>
                    <?php
                  }
                  ?>
                </select>
                <select  name="kode_plasma"  id="kode_plasma" required class="form-control select2">
                  <option value="">--Pilih Plasma--</option>
                  <?php
                  $plasma = $this->db->query(" SELECT * FROM master_plasma where status='1'");
                  $hasil_plasma = $plasma->result();
                  foreach ($hasil_plasma as $data) {
                    ?>
                    <option value="<?php echo $data->kode_plasma; ?>"><?php echo $data->nama_plasma; ?></option>
                    <?php
                  }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td></td>
              <td><div onclick="simpan_jenis()" id="simpan_jenis"   class="btn btn-primary btn-lg pull-right"><i class="fa fa-save"></i> Simpan</div>

              </td>
            </tr>

          </table>
        </form>
        <div class="box-body list_perintah">
          <div class="row">
            <div class="">

              <!-- <div class="col-md-2" id="sendiri">
                <label>Pilih Karyawan</label>
                <select  name="kode_karyawan"  id="karyawan" required class="form-control select2">
                  <option value="">--Pilih Karyawan--</option>
                  <?php
                  $karyawan = $this->db->query(" SELECT * FROM master_karyawan where status='1' and jenis_karyawan='borongan'");
                  $hasil_karyawan = $karyawan->result();
                  foreach ($hasil_karyawan as $data) {
                    ?>
                    <option value="<?php echo $data->kode_karyawan; ?>"><?php echo $data->nama_karyawan; ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div> -->
              <!-- <div class="col-md-2" id="plasma">
                <label>Pilih Plasma</label>
                <select  name="kode_plasma"  id="kode_plasma" required class="form-control select2">
                  <option value="">--Pilih Plasma--</option>
                  <?php
                  $plasma = $this->db->query(" SELECT * FROM master_plasma where status='1'");
                  $hasil_plasma = $plasma->result();
                  foreach ($hasil_plasma as $data) {
                    ?>
                    <option value="<?php echo $data->kode_plasma; ?>"><?php echo $data->nama_plasma; ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div> -->

              <div class="col-md-2">
                <label>Jenis Bahan</label>
                <select onchange="get_barang()" name="kategori_bahan" id="kategori_bahan" class="form-control" tabindex="-1" aria-hidden="true">
                  <option value="" selected="true" >--Pilih Jenis Bahan--</option>
                  <option value="setengah_jadi">Setengah Jadi</option>                     
                  <option value="jadi" id="bahan_jadi">Jadi</option> 
                </select>


              </div>

              <div class="col-md-2">
                <label>Nama Produk</label>
                <select id="kode_bahan" name="kode_bahan" class="form-control select2" >
                  <option value="">Pilih Produk</option>
                </select>
              </div>

              <div class="col-md-2">
                <label>QTY</label>
                <input type="text" class="form-control" placeholder="QTY" name="jumlah" onkeyup="cek_jumlah()" id="jumlah" />
              </div>

              <div class="col-md-2">
                <label>Afkir</label>
                <input type="text" class="form-control" placeholder="Afkir" name="afkir" id="afkir" />
              </div>

              <div class="col-md-3">
                <label>Keterangan</label>
                <input type="text" class="form-control" placeholder="KET" name="keterangan" id="keterangan" />
                <!-- <textarea class="form-control" placeholder="KET" name="keterangan" id="keterangan" ></textarea> -->
                <input type="hidden" name="id_item" id="id_item" />
                <?php
                $query=$this->db->query("SELECT kode_unit from setting_gudang");
                $kode_unit=$query->row();


                ?>
                <input type="hidden" id="nama_bahan" name="nama_bahan" />

                <input type="hidden" name="kode_unit" id="kode_unit" value="<?php echo $kode_unit->kode_unit;?>" />
              </div>


<!--<div class="col-md-2">
<label>Satuan</label>
<input type="text" readonly="true" class="form-control" placeholder="Satuan Pembelian" name="nama_satuan" id="nama_satuan" />
<input type="hidden" name="kode_satuan" id="kode_satuan" />
</div>
<div class="col-md-2">
<label>Harga Satuan</label>
<input type="text" class="form-control" placeholder="Harga Satuan" name="harga_satuan" id="harga_satuan" />
<input type="hidden" name="id_item" id="id_item" />
</div>-->
<div class="col-md-1" style="padding-top:26px; padding-left:0px;" >
  <div onclick="add_item()" id="add"  class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Add</div>

</div>
</div>
</div>
</div>
<div id="list_transaksi_pembelian" class="list_perintah">
  <div class="box-body"><br>
    <div calss="sukses" id="sukses"></div>
    <table id="tabel_daftar" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Nama bahan</th>
          <th>QTY</th>
          <th>Afkir</th>
          <th>Keterangan</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody id="tabel_temp_data_transaksi">

      </tbody>
      <tfoot>

      </tfoot>
    </table>
  </div>
</div>
<div class="box-footer clearfix">
  <br><br>
  <center>
   <div onclick="batal_jenis()"  id="batal_jenis" class="btn btn-danger btn-lg"><i class="fa fa-cancel"></i> Batal</div>
   <a id="simpan" class="btn btn-lg green-seagreen list_perintah" style="width:200px;"><i class="fa fa-save"></i> Simpan</a></center>
 </div>
</div>
</div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus produk tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-batal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Batal Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="hapus_jenis()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-simpan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Simpan Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menyimpan data tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="simpan_perintah()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">
<script>
  $('.btn-back').click(function(){
    window.location = "<?php echo base_url().'perintah_produksi/'; ?>";
  });
</script>
<script>
  $(document).ready(function(){
    $("#batal_jenis").hide();
    $("#opsi").hide();
    $(".list_perintah").hide();
    $("#sendiri").hide();
    $("#plasma").hide();
    $("#view_jenis").hide();
    var kode_transaksi = $("#kode_transaksi").val();
    $("#tabel_temp_data_transaksi").load("<?php echo base_url().'perintah_produksi/get_transaksi/'; ?>"+kode_transaksi); 
    $("#perintah_produksi").change(function(){
      var perintah = $("#perintah_produksi").val();
      $("#opsi").show();
      if(perintah=="sendiri"){
        $("#sendiri").show();
        $("#karyawan").show();
        $("#kode_plasma").hide();
        $("#plasma").hide();
        $("#sendiri").val('');
        $("#plasma").val('');        
        $("#pilih").text('Pilih Karyawan');
        $("#bahan_jadi").show();
      }else if(perintah=="plasma"){
        $("#bahan_jadi").hide();
        $("#sendiri").hide();
        $("#karyawan").hide();
        $("#kode_plasma").show();
        $("#pilih").text('Pilih Plasma');
        $("#plasma").show();$("#sendiri").val('');$("#plasma").val('');
      }else{
        $("#hide").show();
        $("#karyawan").hide();
        $("#kode_plasma").hide();
        $("#sendiri").hide();$("#sendiri").val('');$("#plasma").val('');
        $("#plasma").hide();
      }

    });

    $("#simpan").click(function(){
      $('#modal-confirm-simpan').modal('show');
    })
  });

  function batal_jenis() {
    $('#modal-confirm-batal').modal('show');
  }
  function simpan_jenis() {

    var perintah_produksi=$("#perintah_produksi").val();
    var karyawan=$("#karyawan").val();
    var kode_plasma=$("#kode_plasma").val();
    if(perintah_produksi=='sendiri' && karyawan !=''){
     $("#simpan_jenis").hide();
     $("#batal_jenis").show();
     $(".list_perintah").show();
     $("#perintah_produksi").hide();
     document.getElementById("karyawan").disabled = true;
     document.getElementById("kode_plasma").disabled = true;
     $("#view_jenis").val('Factory');
     $("#view_jenis").show();
   }else if(perintah_produksi=='plasma' && kode_plasma !=''){
     $("#simpan_jenis").hide();
     $("#batal_jenis").show();
     $(".list_perintah").show();
     $("#perintah_produksi").hide();
     document.getElementById("karyawan").disabled = true;
     document.getElementById("kode_plasma").disabled = true;
     $("#view_jenis").val('Plasma');
     $("#view_jenis").show();
   }else{
    alert('Silahkan Lengkapi Form ..!');
  }


}
function cek_jumlah() {
 var jumlah = $("#jumlah").val();
 if(jumlah < 0){
  alert('jumlah Salah');
  $("#jumlah").val('');
}
}
function hapus_jenis() {

  var kode_transaksi = $("#kode_transaksi").val();
  var url = '<?php echo base_url().'perintah_produksi/hapus_bahan_jenis'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      kode_transaksi:kode_transaksi
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $("#simpan_jenis").show();
      $("#batal_jenis").hide();
      $(".list_perintah").hide();
      $("#perintah_produksi").show();
      $(".tunggu").hide();  
      $("#view_jenis").hide();
      $("#kode_plasma").val('');
      $("#kategori_bahan").val('');
      $("#kode_plasma").val('');
      $("#kode_bahan").val('');
      $("#jumlah").val('');
      $("#opsi").hide();
      $("#keterangan").val('');
      $("#perintah_produksi").val('');
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'perintah_produksi/get_transaksi/'; ?>"+kode_transaksi); 
      $('#modal-confirm-batal').modal('hide');
      document.getElementById("karyawan").disabled = false;
      document.getElementById("kode_plasma").disabled = false;
    }
  });
  return false;

}

function get_barang(){
  var key = $("#kategori_bahan").val();
  var jenis_produksi=$('#perintah_produksi').val();
  var kode_plasma=$('#kode_plasma').val();

  var url = "<?php echo base_url().'perintah_produksi/get_barang' ;?>";

  $.ajax({
    type: 'POST',
    url: url,
    cache:false,
    data: {key:key,jenis_produksi:jenis_produksi,kode_plasma:kode_plasma},
    success: function(msg){
      $("#kode_bahan").html('');
      $("#kode_bahan").html(msg);
    }
  });
}

function add_item(){
  var kode_transaksi = $("#kode_transaksi").val();
  var kategori_bahan = $("#kategori_bahan").val();
  var kode_bahan = $("#kode_bahan").val();
  var jumlah = $("#jumlah").val();
  var afkir = $("#afkir").val();
  var keterangan = $("#keterangan").val();
  var kode_karyawan = $("#karyawan").val();
  var kode_plasma = $("#kode_plasma").val();
  var perintah_produksi = $("#perintah_produksi").val();
  if(kategori_bahan=='' || kode_bahan=='' || jumlah==''){
    alert("Lengkapi Form !");
  }else{
    var url = "<?php echo base_url().'perintah_produksi/simpan_temp' ;?>";
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {kategori_bahan:kategori_bahan,kode_bahan:kode_bahan,kode_transaksi:kode_transaksi,kode_karyawan:kode_karyawan,
        kode_plasma:kode_plasma,perintah_produksi:perintah_produksi,
        jumlah:jumlah,afkir:afkir,keterangan:keterangan},
        success: function(msg){
          var pesan=msg.split('|');
          if(pesan[0]==0){
           $("#kategori_bahan").val('');
           $("#kode_bahan").val('').trigger('change');
           $("#jumlah").val('');
           $("#afkir").val('');
           $("#keterangan").val('');
           $("#tabel_temp_data_transaksi").load("<?php echo base_url().'perintah_produksi/get_transaksi/'; ?>"+kode_transaksi); 
           $("#sukses").html("");
         }else{
           $("#sukses").html(pesan[1]);
         }        
       }
     }); 
  }

}
function actDelete(Object) {
  $('#id-delete').val(Object);
  $('#modal-confirm').modal('show');
}
function delData() {
  var id = $('#id-delete').val();
  var kode_transaksi = $("#kode_transaksi").val();
  var url = '<?php echo base_url().'perintah_produksi/hapus_bahan_temp'; ?>/delete';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id:id
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'perintah_produksi/get_transaksi/'; ?>"+kode_transaksi); 

    }
  });
  return false;
}
function simpan_perintah(){
  var url = '<?php echo base_url().'perintah_produksi/simpan_transaksi'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    data: $("#data_form").serialize(),
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();
      $('#modal-confirm-simpan').modal('hide');
//$("#sukses").html('<div class="alert alert-success">Sukses Simpan Data</div>');
pesan=msg.split("|");
//alert(pesan[0]);
if(pesan[0]==1){
  $("#sukses").html("<div class='alert alert-success'>Sukses Simpan Data</div>");
  var kode_transaksi = $("#kode_transaksi").val();
  window.open("<?php echo base_url().'perintah_produksi/cetak_perintah/' ?>"+kode_transaksi);   
  setTimeout(function(){$('.sukses').html('');

    window.location = "<?php echo base_url() . 'perintah_produksi/sendiri' ?>";
  },1500);   

}else{
  $("#sukses").html(pesan[1]);
}

}
});
}
</script>