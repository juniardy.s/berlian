<div class="">
  <div class="page-content">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">Kebutuhan Plasma</div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <form id="data_form">
          <table class="table">
            <tr>
             <?php
             $this->db->select_max('id');
             $get_max_po = $this->db->get('transaksi_input_plasma');
             $max_po = $get_max_po->row();

             $this->db->where('id', $max_po->id);
             $get_po = $this->db->get('transaksi_input_plasma');
             $po = $get_po->row();
             $tahun = substr(@$po->kode_transaksi, 3,4);
             if(date('Y')==$tahun){
              $nomor = substr(@$po->kode_transaksi, 8);
              $nomor = $nomor + 1;
              $string = strlen($nomor);
              if($string == 1){
                $kode_trans = 'IP_'.date('Y').'_00000'.$nomor;
              } else if($string == 2){
                $kode_trans = 'IP_'.date('Y').'_0000'.$nomor;
              } else if($string == 3){
                $kode_trans = 'IP_'.date('Y').'_000'.$nomor;
              } else if($string == 4){
                $kode_trans = 'IP_'.date('Y').'_00'.$nomor;
              } else if($string == 5){
                $kode_trans = 'IP_'.date('Y').'_0'.$nomor;
              } else if($string == 6){
                $kode_trans = 'IP_'.date('Y').'_'.$nomor;
              }
            } else {
              $kode_trans = 'IP_'.date('Y').'_000001';
            }

            ?>
            <td><label>Kode Transaksi</label></td>
            <td><input type="text" name="kode_transaksi" id="kode_transaksi" readonly value="<?php echo $kode_trans;?>" class='form-control' required /></td>
          </tr>
          <tr>
            <td><label>Tanggal</label></td>
            <td><input type="text" name="tanggal_input"  readonly value="<?php echo date('Y-m-d');?>" class='form-control' required /></td>
          </tr>


          <tr id="plasma">
            <td><label>Pilih Plasma</label></td>
            <td>
              <select onchange="get_piutang()"  name="kode_plasma"  id="kode_plasma" required class="form-control select2">
                <option value="">--Pilih Plasma--</option>
                <?php
                $plasma = $this->db->query(" SELECT * FROM master_plasma where status='1'");
                $hasil_plasma = $plasma->result();
                foreach ($hasil_plasma as $data) {
                  ?>
                  <option value="<?php echo $data->kode_plasma; ?>"><?php echo $data->nama_plasma; ?></option>
                  <?php
                }
                ?>
              </select>
            </td>
          </tr>

        </table>
      
      <div class="box-body">
        <div class="row">
          <div class="">
           <div class="col-md-2">
            <label>Jenis Bahan</label>
            <select onchange="get_barang()" name="kategori_bahan" id="kategori_bahan" class="form-control" tabindex="-1" aria-hidden="true">
              <option value="" selected="true" >--Pilih Jenis Bahan--</option>
              <option value="setengah_jadi">Setengah Jadi</option>                     
              <option value="jadi">Jadi</option> 
            </select>
          </div>

          <div class="col-md-3">
            <label>Nama Produk</label>
            <select onchange="get_satuan()" id="kode_bahan" name="kode_bahan" class="form-control select2" >
             <option value="">--Pilih Produk--</option>
           </select>
         </div>

         <div class="col-md-3">
          <label>QTY</label>
          <input type="text" class="form-control" placeholder="QTY" name="jumlah" id="jumlah" />
        </div>
        <div class="col-md-3">
          <label>Harga</label>
          <input readonly="" type="text" class="form-control" placeholder="Rp. 0" name="harga_satuan" id="harga_satuan" />
        </div>

        <div hidden class="col-md-3">
          <label>Keterangan</label>
          <input type="text" class="form-control" placeholder="KET" name="keterangan" id="keterangan" />
          <input type="hidden" name="id_item" id="id_item" />
          <?php
          $query=$this->db->query("SELECT kode_unit from setting_gudang");
          $kode_unit=$query->row();


          ?>
          <input type="hidden" id="nama_bahan" name="nama_bahan" />

          <input type="hidden" name="kode_unit" id="kode_unit" value="<?php echo $kode_unit->kode_unit;?>" />
        </div>


                                  <!--<div class="col-md-2">
                                    <label>Satuan</label>
                                    <input type="text" readonly="true" class="form-control" placeholder="Satuan Pembelian" name="nama_satuan" id="nama_satuan" />
                                    <input type="hidden" name="kode_satuan" id="kode_satuan" />
                                  </div>
                                  <div class="col-md-2">
                                    <label>Harga Satuan</label>
                                    <input type="text" class="form-control" placeholder="Harga Satuan" name="harga_satuan" id="harga_satuan" />
                                    <input type="hidden" name="id_item" id="id_item" />
                                  </div>-->
                                  <div class="col-md-1" style="padding-top:26px; padding-left:0px;" >
                                    <div onclick="add_item()" id="add"  class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Add</div>
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div id="list_transaksi_pembelian">
                              <div class="box-body"><br>
                                <table id="tabel_daftar" class="table table-bordered table-striped">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Nama bahan</th>
                                      <th>Kategori Bahan</th>
                                      <th>QTY</th>
                                      <th>Harga Satuan</th>
                                      <th>Sub Total</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tabel_temp_data_transaksi">

                                  </tbody>
                                  <tfoot>
                                   </tr>

                                  <td style="font-size: 15px" align="right" colspan="5">Piutang Plasma</td>
                                  <td  style="font-size: 15px" align="right"><input readonly="" type="text" class="form-control" name="piutang_plasma" id="piutang_plasma"></td>
                                  <td></td>
                                </tr>
                                  </tr>

                                  <td style="font-size: 15px" align="right" colspan="5">Angsuran Plasma</td>
                                  <td  style="font-size: 15px" align="right"><input type="text" class="form-control" name="angsuran_plasma"></td>
                                  <td></td>
                                </tr>
                              </tfoot>
                            </table>
                            </form>
                          </div>
                        </div>
                        <div class="box-footer clearfix">
                          <a id="simpan" class="btn btn-lg green-seagreen pull-right"><i class="fa fa-save"></i> Simpan</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header" style="background-color:grey">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
                        </div>
                        <div class="modal-body">
                          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus produk tersebut ?</span>
                          <input id="id-delete" type="hidden">
                        </div>
                        <div class="modal-footer" style="background-color:#eee">
                          <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
                          <button onclick="delData()"  class="btn green">Ya</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="modal-confirm-simpan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header" style="background-color:grey">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title" style="color:#fff;">Konfirmasi Simpan Data</h4>
                        </div>
                        <div class="modal-body">
                          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menyimpan data tersebut ?</span>
                          <input id="id-delete" type="hidden">
                        </div>
                        <div class="modal-footer" style="background-color:#eee">
                          <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
                          <button onclick="simpan_perintah()"  class="btn green">Ya</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <style type="text/css" media="screen">
                    .btn-back
                    {
                      position: fixed;
                      bottom: 10px;
                      left: 10px;
                      z-index: 999999999999999;
                      vertical-align: middle;
                      cursor:pointer
                    }
                  </style>
                  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">
                  <script>
                    $('.btn-back').click(function(){
                      window.location = "<?php echo base_url().'input_plasma/'; ?>";
                    });
                  </script>
                  <script>
                    $(document).ready(function(){

                      var kode_transaksi = $("#kode_transaksi").val();
                      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi); 


                      $("#simpan").click(function(){
                        $('#modal-confirm-simpan').modal('show');
                      })
                    });
                    function get_barang(){
                      var key = $("#kategori_bahan").val();
                      var kode_plasma = $("#kode_plasma").val();
                      var url = "<?php echo base_url().'input_plasma/get_barang' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {key:key,kode_plasma:kode_plasma},
      success: function(msg){
        $("#kode_bahan").html('');
        $("#kode_bahan").html(msg);
      }
    });

  }

  function get_satuan(){
    var kode_bahan = $("#kode_bahan").val();
    var kode_plasma = $("#kode_plasma").val();

    var key = $("#kategori_bahan").val();
    //alert(key);
    var url = "<?php echo base_url().'input_plasma/get_satuan' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {key:key,kode_plasma:kode_plasma,kode_bahan:kode_bahan},
      success: function(msg){
        $("#harga_satuan").html('');
        $("#harga_satuan").val(msg);
      }
    });
  }

  function get_piutang(){

    var kode_plasma = $("#kode_plasma").val();
    var url = "<?php echo base_url().'input_plasma/get_piutang' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {kode_plasma:kode_plasma},
      success: function(msg){
        $("#piutang_plasma").html('');
        $("#piutang_plasma").val(msg);
      }
    });
  }
  function add_item(){
    var kode_transaksi = $("#kode_transaksi").val();
    var kategori_bahan = $("#kategori_bahan").val();
    var kode_bahan = $("#kode_bahan").val();
    var jumlah = $("#jumlah").val();
    var harga_satuan=$('#harga_satuan').val();
    var sub_total= jumlah * harga_satuan;
    var kode_plasma = $("#kode_plasma").val();
    var keterangan = $("#keterangan").val();
    var url = "<?php echo base_url().'input_plasma/simpan_temp' ;?>";
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {sub_total:sub_total,harga_satuan:harga_satuan,kategori_bahan:kategori_bahan,kode_bahan:kode_bahan,kode_transaksi:kode_transaksi,
        jumlah:jumlah,keterangan:keterangan,kode_plasma:kode_plasma},
        success: function(msg){

          $("#kategori_bahan").val('');
          $("#kode_bahan").val('');
          $("#jumlah").val('');
          $("#harga_satuan").val('');
          $("#keterangan").val('');
          $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi +"/"+kode_plasma); 
        }
      }); 
  }
  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }
  function delData() {
    var id = $('#id-delete').val();
    var kode_transaksi = $("#kode_transaksi").val();
    var url = '<?php echo base_url().'input_plasma/hapus_bahan_temp'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id:id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
        $('#modal-confirm').modal('hide');
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi); 

      }
    });
    return false;
  }
  function simpan_perintah(){
    var url = '<?php echo base_url().'input_plasma/simpan_transaksi'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: $("#data_form").serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
        $('#modal-confirm').modal('hide');
        $(".sukses").html('<div class="alert alert-success">Sukses Simpan Data</div>');   
        setTimeout(function(){$('.sukses').html('');
        window.location = "<?php echo base_url() . 'input_plasma/' ?>";
                },1500);   

      }
    });
  }
</script>