<div class="">
  <div class="page-content">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">Kebutuhan Plasma</div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <form id="data_form">
          <table class="table">
            <tr>
             <?php
             $this->db->select_max('id');
             $get_max_po = $this->db->get('transaksi_input_plasma');
             $max_po = $get_max_po->row();

             $user = $this->session->userdata('astrosession');
             $id_user=$user->id;
             $this->db->where('id', $max_po->id);
             $get_po = $this->db->get('transaksi_input_plasma');
             $po = $get_po->row();
             $tahun = substr(@$po->kode_transaksi, 3,4);
             if(date('Y')==$tahun){
              $nomor = substr(@$po->kode_transaksi, 11);
              //echo $nomor;
              $nomor = $nomor + 1;
              $string = strlen($nomor);
              if($string == 1){
                $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_00000'.$nomor;
              } else if($string == 2){
                $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_0000'.$nomor;
              } else if($string == 3){
                $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_000'.$nomor;
              } else if($string == 4){
                $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_00'.$nomor;
              } else if($string == 5){
                $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_0'.$nomor;
              } else if($string == 6){
                $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_'.$nomor;
              }
            } else {
              $kode_trans = 'IP_'.date('Y').'_'.$id_user.'_000001';
            }

            ?>
            <td><label>Kode Transaksi</label></td>
            <td><input type="text" name="kode_transaksi" id="kode_transaksi" readonly value="<?php echo $kode_trans;?>" class='form-control' required /></td>
          </tr>
          <tr>
            <td><label>Tanggal</label></td>
            <td>
              <input type="text" name="tanggal_input"  readonly value="<?php echo date('Y-m-d');?>" class='form-control' required />
            </td>
          </tr>


          <tr id="plasma">
            <td><label>Pilih Plasma</label></td>
            <td>
              <select onchange="get_piutang()"  name="kode_plasma"  id="kode_plasma" required class="form-control select2">
                <option value="">--Pilih Plasma--</option>
                <?php
                $plasma = $this->db->query(" SELECT * FROM master_plasma where status='1'");
                $hasil_plasma = $plasma->result();
                foreach ($hasil_plasma as $data) {
                  ?>
                  <option value="<?php echo $data->kode_plasma; ?>"><?php echo $data->nama_plasma; ?></option>
                  <?php
                }
                ?>
              </select>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <div style="" onclick="form_simpan()" id="form_temp" class="btn btn-primary btn-lg pull-right"><i class="fa fa-save"></i> Simpan</div>
            </td>
          </tr>

        </table>
        <div class="box-body">
          <div class="row">
            <div class="" id="input_temp">  
              <div class="col-md-2">
                <label>Pilih Kebutuhan</label>
                <select name="kebutuhan" onchange="get_kebutuhan()"  id="kebutuhan" required class="form-control select2">
                  <option value="">-- Pilih Kebutuhan --</option>
                  <option value="Barang">Barang</option>
                  <option value="Kasbon">Kasbon</option>
                </select>
              </div>
              <div class="col-md-2 kasbon">
                <label>Nominal</label>
                <input type="text" onkeyup="get_rupiah()" class="form-control" placeholder="Nominal" name="nominal" id="nominal" />
              </div>
              <div class="col-md-3 barang">
               <label>Nama Barang</label>
               <select onchange="get_satuan()" id="kode_barang_lain" name="kode_barang_lain" class="form-control select2">
                <option value="">--Pilih Barang--</option>
                <?php 
                $this->db->where('status','sendiri');
                $bahan_baku=$this->db->get('master_bahan_baku');
                $hasil_bahan_baku=$bahan_baku->result();
                foreach ($hasil_bahan_baku as $barang_lain) {
                 ?>
                 <option value="<?php echo $barang_lain->kode_bahan_baku?>"><?php echo $barang_lain->nama_bahan_baku?></option>
                 <?php
               }
               ?>
             </select>
           </div>

           <div class="col-md-2 barang">
            <label>QTY</label>
            <input type="text" class="form-control" placeholder="QTY" name="jumlah" id="jumlah" />
          </div>
          <div class="col-md-2 barang">
            <label>Harga</label>
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input type="text" class="form-control" placeholder="" name="harga_satuan" id="harga_satuan" />
            </div>
          </div>

          <div hidden class="col-md-3">
            <label>Keterangan</label>
            <input type="text" class="form-control" placeholder="KET" name="keterangan" id="keterangan" />
            <input type="hidden" name="id_item" id="id_item" />
            <?php
            $query=$this->db->query("SELECT kode_unit from setting_gudang");
            $kode_unit=$query->row();


            ?>
            <input type="hidden" id="nama_bahan" name="nama_bahan" />

            <input type="hidden" name="kode_unit" id="kode_unit" value="<?php echo $kode_unit->kode_unit;?>" />
          </div>

          <div class="col-md-1" style="padding-top:26px; padding-left:0px;" >
            <div onclick="add_item()" id="add"  class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Add</div>
            <div onclick="update_item()" id="update"  class="btn btn-primary pull-right">Update</div>
          </div>
          <div hidden class="col-md-3 kasbon">
            <br><br><b>
              <label style="font-size: 20px;" id="rupiah_nominal"></label></b>
            </div>
          </div>
        </div>
      </div>
      <div id="list_transaksi_pembelian">
        <div class="sukses"></div>
        <div class="box-body"><br>
          <table id="tabel_daftar" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Plasma</th>
                <th>Nama bahan</th>
                <th>QTY</th>
                <th>Harga Satuan</th>
                <th>Sub Total</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="tabel_temp_data_transaksi">

            </tbody>
            <tfoot>

            </tfoot>
          </table>
        </form>
      </div>
    </div>
    <div class="box-footer clearfix">
      <br><br>
      <center>
       <div onclick="batal_jenis()"  id="batal_jenis" class="btn btn-danger btn-lg"><i class="fa fa-cancel"></i> Batal</div>
       <a id="simpan" class="btn btn-lg green-seagreen " style="width:200px;"><i class="fa fa-save"></i> Simpan</a>
     </center>
   </div>
 </div>
</div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus produk tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-simpan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Simpan Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menyimpan data tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="simpan_perintah()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm-batal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Batal Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="hapus_jenis()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">
<script>
  $('.btn-back').click(function(){
    window.location = "<?php echo base_url().'input_plasma/'; ?>";
  });
</script>
<script>
  $(document).ready(function(){
    $("#update").hide();
    $("#simpan").hide();
    $("#input_temp").hide();
    $("#batal_jenis").hide();
    $("#list_transaksi_pembelian").hide();
    $(".kasbon").hide();
    var kode_transaksi = $("#kode_transaksi").val();
    $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi); 


    $("#simpan").click(function(){
      $('#modal-confirm-simpan').modal('show');
    })
  });

  function batal_jenis() {
    $('#modal-confirm-batal').modal('show');
  }

  function get_barang(){
    var key = $("#kategori_bahan").val();
    var kode_plasma = $("#kode_plasma").val();
    var url = "<?php echo base_url().'input_plasma/get_barang' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {key:key,kode_plasma:kode_plasma},
      success: function(msg){
        $("#kode_bahan").html('');
        $("#kode_bahan").html(msg);
      }
    });

  }


  $("#jumlah").keyup(function(){

   var jumlah = $('#jumlah').val();

   if (jumlah <  0 ){
    alert("Silahkan cek inputan quantitynya !");
    $('#jumlah').val('');
  }

});


  function get_kebutuhan(){
    var kebutuhan = $("#kebutuhan").val();
    if(kebutuhan=='Barang'){
      $(".barang").show();
      $(".kasbon").hide();
    }else if(kebutuhan=='Kasbon'){
      $(".barang").hide();
      $(".kasbon").show();
    }
  }

  function form_input(){
    $("#kode_plasma").val('');
    $("#input_temp").hide();
    $("#list_transaksi_pembelian").hide();
    $("#form_temp").show();
    $("#batal_jenis").hide();
    $("#simpan").hide();
    document.getElementById('kode_plasma').disabled = false;
  }

  function form_simpan(){
    var kode_plasma = $("#kode_plasma").val();
    var kode_transaksi = $("#kode_transaksi").val();
    if(kode_plasma !=''){
      $("#input_temp").show();
      $("#simpan").show();
      $("#batal_jenis").show();
      $("#form_temp").hide();
      $("#list_transaksi_pembelian").show();
      document.getElementById('kode_plasma').setAttribute("disabled","disabled");
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi +"/"+kode_plasma); 
    }else{
      $("#input_temp").hide();
      $("#list_transaksi_pembelian").hide();
      $("#form_temp").show();
      $("#batal_jenis").hide();
      $("#simpan").hide();
      alert("Silahkan cek Plasmanya !");
    }
  }

  function get_rupiah(){
    var nominal = $("#nominal").val();
    var url = "<?php echo base_url().'input_plasma/get_rupiah' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {nominal:nominal},
      success: function(msg){
        $("#rupiah_nominal").html(msg);
        
      }
    });
  }


  $("#nominal").keyup(function(){

    var nominal = $('#nominal').val();
    rupiah_nominal = parseInt(nominal);
    if (rupiah_nominal <  0 ){
      alert("Silahkan cek inputan nominalnya !");
      $('#nominal').val('');
      get_rupiah();
    }

  });


  function get_satuan(){
    var kode_barang_lain = $("#kode_barang_lain").val();
    var kode_plasma = $("#kode_plasma").val();

    var key = $("#kategori_bahan").val();
    //alert(key);
    var url = "<?php echo base_url().'input_plasma/get_satuan' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {key:key,kode_plasma:kode_plasma,kode_barang_lain:kode_barang_lain},
      success: function(msg){
        $("#harga_satuan").html(' ');
        $("#harga_satuan").val(msg);
      }
    });
  }

  function hapus_jenis() {

    var kode_transaksi = $("#kode_transaksi").val();
    var url = '<?php echo base_url().'input_plasma/hapus_bahan_jenis'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        kode_transaksi:kode_transaksi
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
        $("#kode_plasma").val('');
        $('#modal-confirm-batal').modal('hide');
        form_input();
      }
    });
    return false;

  }

  function get_piutang(){

    var kode_plasma = $("#kode_plasma").val();
    var url = "<?php echo base_url().'input_plasma/get_piutang' ;?>";
    //alert(key);
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {kode_plasma:kode_plasma},
      success: function(msg){
        $("#piutang_plasma").html('');
        $("#piutang_plasma").val(msg);
      }
    });
  }
  function add_item(){
    var kode_transaksi = $("#kode_transaksi").val();
    var kategori_bahan = $("#kategori_bahan").val();
    var kode_bahan = $("#kode_barang_lain").val();
    var jumlah = $("#jumlah").val();
    var harga_satuan=$('#harga_satuan').val();
    var sub_total= jumlah * harga_satuan;
    var kode_plasma = $("#kode_plasma").val();
    var keterangan = $("#keterangan").val();
    var nominal = $("#nominal").val();
    var kebutuhan = $("#kebutuhan").val();
    var url = "<?php echo base_url().'input_plasma/simpan_temp' ;?>";
    if (kebutuhan == '') {
      alert("Silahkan Lengkapi Formnya");
    }
    else if (kebutuhan =='Barang' && kode_bahan=='') {
      alert("Silahkan Lengkapi Formnya");
    }
    else if (kebutuhan =='Barang' && jumlah =='') {
      alert("Silahkan Lengkapi Formnya");
    }
    else if(kebutuhan =='Kasbon' && nominal ==''){
      alert("Silahkan Lengkapi Formnya");
    }
    else{
      $.ajax({
        type: 'POST',
        url: url,
        cache:false,
        data: {sub_total:sub_total,harga_satuan:harga_satuan,kategori_bahan:kategori_bahan,kode_bahan:kode_bahan,kode_transaksi:kode_transaksi,
          jumlah:jumlah,keterangan:keterangan,kode_plasma:kode_plasma,kebutuhan:kebutuhan,nominal:nominal},
          success: function(msg){
            setTimeout(function(){$('.sukses').html('');
              $(".sukses").html(msg);
            },1500);  
          // $("#kode_plasma").val('');
          $("#nominal").val('');
          $("#rupiah_nominal").html('');
          $("#kebutuhan").val('');
          $("#kategori_bahan").val('');
          $("#kode_barang_lain").val('');
          $("#jumlah").val('');
          $("#harga_satuan").val('');
          $("#keterangan").val('');
          $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi +"/"+kode_plasma); 
        }
      }); 
    }
  }
  function actEdit(id) {
    var id = id;
    var kode_produksi =$('#kode_produksi').val();
    var url = "<?php echo base_url().'input_plasma/get_temp_kebutuhan'; ?>";
    $.ajax({
      type: 'POST',
      url: url,
      dataType: 'json',
      data: {id:id,kode_produksi:kode_produksi},
      success: function(produksi){

        $("#kebutuhan").val(produksi.kebutuhan);
        if(produksi.kebutuhan=='Kasbon'){
         $("#nominal").val(produksi.sub_total);
       }

       $("#kode_barang_lain").val(produksi.kode_bahan);
       $("#jumlah").val(produksi.jumlah);
       $('#harga_satuan').val(produksi.harga_satuan);

       $("#kode_plasma").val(produksi.kode_plasma);

       $("#id_item").val(produksi.id);
       get_kebutuhan();
       get_rupiah();
       $("#add").hide();
       $("#update").show();

     }
   });
  }
  function update_item(){
    var id= $("#id_item").val();
    var kode_transaksi = $("#kode_transaksi").val();
    var kategori_bahan = $("#kategori_bahan").val();
    var kode_bahan = $("#kode_barang_lain").val();
    var jumlah = $("#jumlah").val();
    var harga_satuan=$('#harga_satuan').val();
    var sub_total= jumlah * harga_satuan;
    var kode_plasma = $("#kode_plasma").val();
    var keterangan = $("#keterangan").val();
    var nominal = $("#nominal").val();
    var kebutuhan = $("#kebutuhan").val();
    var url = "<?php echo base_url().'input_plasma/update_temp' ;?>";
    $.ajax({
      type: 'POST',
      url: url,
      cache:false,
      data: {id:id,sub_total:sub_total,harga_satuan:harga_satuan,kategori_bahan:kategori_bahan,kode_bahan:kode_bahan,kode_transaksi:kode_transaksi,
        jumlah:jumlah,keterangan:keterangan,kode_plasma:kode_plasma,kebutuhan:kebutuhan,nominal:nominal},
        success: function(msg){
         setTimeout(function(){$('.sukses').html('');
          $(".sukses").html(msg);
        },1500);   
         // $("#kode_plasma").val('');
         $("#nominal").val('');
         $("#rupiah_nominal").html('');
         $("#kebutuhan").val('');
         $("#kategori_bahan").val('');
         $("#kode_barang_lain").val('');
         $("#jumlah").val('');
         $("#harga_satuan").val('');
         $("#keterangan").val('');
         $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi +"/"+kode_plasma); 
         $("#update").hide();
         $("#add").show();
       }
     }); 
  }
  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }
  function delData() {
    var id = $('#id-delete').val();
    var kode_transaksi = $("#kode_transaksi").val();
    var url = '<?php echo base_url().'input_plasma/hapus_bahan_temp'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id:id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
        $('#modal-confirm').modal('hide');
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'input_plasma/get_transaksi/'; ?>"+kode_transaksi); 

      }
    });
    return false;
  }
  function simpan_perintah(){
    var url = '<?php echo base_url().'input_plasma/simpan_transaksi_baru'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: $("#data_form").serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
        $('#modal-confirm').modal('hide');
        $(".sukses").html('<div class="alert alert-success">Sukses Simpan Data</div>');   
        setTimeout(function(){$('.sukses').html('');
          window.location = "<?php echo base_url() . 'input_plasma/' ?>";
        },1500);   

      }
    });
  }
</script>