<div class="">
  <div class="page-content">
    <div class="row">
      <div class="col-md-4">
        <a href="<?php echo base_url().'input_plasma/tambah'; ?>" class="btn btn-lg btn-primary"><i class="fa fa-plus-square"></i> Tambah</a>
        <a href="<?php echo base_url().'input_plasma/'; ?>" class="btn btn-lg green-seagreen"><i class="fa fa-list"></i> Daftar</a>
      </div>

    </div><br />
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">Daftar Kebutuhan Plasma</div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tanggal Awal</span>
              <input type="date" class="form-control tgl" id="tgl_awal">
            </div>
          </div>
          
          <div class="col-md-5" id="">
            <div class="input-group">
              <span class="input-group-addon">Tanggal Akhir</span>
              <input type="date" class="form-control tgl" id="tgl_akhir">
            </div>
          </div>                        
          <div class="col-md-2 pull-left">
            <button style="width: 190px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
          </div>
        </div>
        <br>
        <div id="transaksi">
          <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
            <?php
            $this->db->limit(100);
            $this->db->order_by('id','DESC');
            $this->db->like('tanggal_transaksi',date('m'));
            $input_plasma = $this->db->get('transaksi_input_plasma');
            $hasil_input_plasma = $input_plasma->result();
            ?>
            <thead>
              <tr>
                <th>No</th>
                <th>Kode Transaksi</th>
                <th>Tanggal Transaksi</th>

                <th>Petugas</th>
                <th>Action</th>


              </tr>
            </thead>
            <tbody id="posts">
              <?php
              $nomor = 1;

              foreach($hasil_input_plasma as $daftar){ ?> 
              <tr>
                <td><?php echo $nomor; ?></td>
                <td><?php echo $daftar->kode_transaksi; ?></td>
                <td><?php echo TanggalIndo($daftar->tanggal_transaksi); ?></td>
                <td><?php echo $daftar->nama_petugas; ?></td>
                <td><?php echo get_detail($daftar->kode_transaksi); ?></td>

              </tr>
              <?php $nomor++; } ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Kode Transaksi</th>
                <th>Tanggal Transaksi</th>

                <th>Petugas</th>
                <th>Action</th>


              </tr>
            </tfoot>
          </table>
          <?php 
          $get_jumlah = $this->db->get('transaksi_input_plasma');
          $jumlah = $get_jumlah->num_rows();
          $jumlah = floor($jumlah/100);
          ?>
          <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
          <input type="hidden" class="form-control pagenum" value="0">
        </div>

        <div class="box-footer clearfix"></div>
      </div>
    </div>
  </div>
  <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script type="text/javascript">
  // $('.tgl').Zebra_DatePicker({});


  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'produksi/'; ?>";
    
  });


  $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function load_table(page){
    var kategori =$("#kategori_produk").val();
    var nama_produk =$("#nama_produk").val();
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'input_plasma/get_table' ?>",
      data: ({kategori:kategori,nama_produk:nama_produk,tgl_awal:tgl_awal,tgl_akhir:tgl_akhir, page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#posts").append(msg);

      }
    });
  }

  $('#cari').click(function(){


    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    $.ajax( {  
      type :"post",  
      url : "<?php echo base_url().'input_plasma/cari_transaksi'; ?>",  
      cache :false,

      data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();  
       $("#transaksi").html(data);
     },  
     error : function(data) {  
       alert("das");  
     }  
   });
  });



 //  $(document).ready(function(){
 // });
</script>

