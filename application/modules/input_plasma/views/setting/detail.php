<div class="">
  <div class="page-content">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">Detail Kebutuhan  Plasma</div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>
        </div>
      </div>
      <div class="portlet-body">
      <?php
        $kode = $this->uri->segment(3);
        $get_perintah = $this->db->get_where('transaksi_input_plasma',array('kode_transaksi'=>$kode));
        $hasil_perintah = $get_perintah->row();
      ?>
        <form id="data_form">
          <table class="table">
            <tr>
             
              <td><label>Kode Transaksi</label></td>
              <td><input type="text" name="kode_transaksi" id="kode_transaksi" readonly value="<?php echo $kode;?>" class='form-control' required /></td>
            </tr>
            <tr>
              <td><label>Tanggal Transaksi</label></td>
              <td><input type="text" name="tanggal_input"  readonly value="<?php echo TanggalIndo($hasil_perintah->tanggal_transaksi);?>" class='form-control' required /></td>
            </tr>
            
           <!--  <tr id="plasma">
              <td><label>Pilih Plasma</label></td>
              <td>
                <select disabled="true"  name="kode_plasma"  id="kode_plasma" required class="form-control select2">
                  <option value="">--Nama Plasma--</option>
                  <?php
                  $plasma = $this->db->query(" SELECT * FROM master_plasma where status='1'");
                  $hasil_plasma = $plasma->result();
                  foreach ($hasil_plasma as $data) {
                    ?>
                    <option <?php if($hasil_perintah->kode_plasma==$data->kode_plasma){ echo "selected='true'"; } ?> value="<?php echo $data->kode_plasma; ?>"><?php echo $data->nama_plasma; ?></option>
                    <?php
                  }
                  ?>
                </select>
              </td>
            </tr>
             -->
            
      </table>
    </form>
    
    <div id="list_transaksi_pembelian">
                              <div class="box-body"><br>
                                <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Nama Plasma</th>
                                      <th>Nama bahan</th>
                                      <th>QTY</th>
                                      <th>Harga Satuan</th>
                                      <th>Subtotal</th>
                                      
                                    </tr>
                                  </thead>
                                  <tbody id="tabel_temp_data_transaksi">
                                  <?php
                                    $opsi = $this->db->get_where('opsi_transaksi_input_plasma',array('kode_transaksi'=>$kode));
                                    $hasil_opsi = $opsi->result();
                                    $no = 1;
                                    foreach($hasil_opsi as $daftar){
                                  ?>
                                  <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $daftar->nama_plasma; ?></td>
                                    <td><?php echo $daftar->nama_bahan; ?></td>
                                    <?php
                                         
                                            $query=$this->db->query("SELECT satuan_stok from master_bahan_baku where kode_bahan_baku='$daftar->kode_bahan'");
                                            $hasil_satuan=$query->row();
                                         
                                          
                                       ?>
                                    <td><?php echo $daftar->jumlah. " " . @$hasil_satuan->satuan_stok; ?></td>
                                    <td><?php echo format_rupiah($daftar->harga_satuan); ?></td>
                                    <td><?php echo format_rupiah($daftar->sub_total); ?></td>
                                    
                                  </tr>
                                  <?php $no++;} ?>

                                  </tbody>
                                  <tfoot>

                                  </tfoot>
                                </table>
                              </div>
                            </div>
    
     
    
  </div>
</div>
</div>


<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">
<script>
$('.btn-back').click(function(){
  window.location = "<?php echo base_url().'input_plasma/'; ?>";
});
</script>
