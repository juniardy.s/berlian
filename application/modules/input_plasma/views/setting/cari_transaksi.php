<table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
  <?php
  $this->db->limit(100);
  $this->db->order_by('tanggal_transaksi','DESC');
  $param = $this->input->post();
  if(@$param['tgl_awal'] && @$param['tgl_akhir']){

    $tgl_awal = $param['tgl_awal'];
    $tgl_akhir = $param['tgl_akhir'];
              //$kode_unit = $param['kode_unit'];

    $this->db->where('tanggal_transaksi >=', $tgl_awal);
    $this->db->where('tanggal_transaksi <=', $tgl_akhir);
            //$this->db->where('position =', 'gudang');

  }

  // $this->db->like('tanggal_transaksi',date('m'));
  $input_plasma = $this->db->get('transaksi_input_plasma');
  $hasil_input_plasma = $input_plasma->result();
  ?>
  <thead>
    <tr>
      <th>No</th>
      <th>Kode Transaksi</th>
      <th>Tanggal Transaksi</th>

      <th>Petugas</th>
      <th>Action</th>


    </tr>
  </thead>
  <tbody>
    <?php
    $nomor = 1;

    foreach($hasil_input_plasma as $daftar){ ?> 
    <tr>
      <td><?php echo $nomor; ?></td>
      <td><?php echo $daftar->kode_transaksi; ?></td>
      <td><?php echo TanggalIndo($daftar->tanggal_transaksi); ?></td>

      <td><?php echo $daftar->nama_petugas; ?></td>
      <td><?php echo get_detail($daftar->kode_transaksi); ?></td>

    </tr>
    <?php $nomor++; } ?>
  </tbody>
  <tfoot>
    <tr>
      <th>No</th>
      <th>Kode Transaksi</th>
      <th>Tanggal Transaksi</th>

      <th>Petugas</th>
      <th>Action</th>


    </tr>
  </tfoot>
</table>
<?php 
$get_jumlah = $this->db->get('transaksi_input_plasma');
$jumlah = $get_jumlah->num_rows();
$jumlah = floor($jumlah/100);
?>
<input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
<input type="hidden" class="form-control pagenum" value="0">