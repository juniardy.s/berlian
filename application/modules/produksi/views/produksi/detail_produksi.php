<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Detail Produksi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>



        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" action="" method="post">
            <div class="box-body">
              <label><h3><b>Detail Transaksi Produksi</b></h3></label>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Kode Transaksi</label>
                    <?php
                    $kode = $this->uri->segment(3);
                    
                    $transaksi_produksi = $this->db->get_where('transaksi_produksi',array('kode_produksi'=>$kode));
                    $hasil_transaksi_produksi = $transaksi_produksi->row();
                    ?>
                    
                    <input readonly="true" type="text" value="<?php echo @$hasil_transaksi_produksi->kode_produksi; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_produksi" id="kode_produksi" />
                  </div>
                  
                  <div class="form-group">
                    <label class="gedhi">Tanggal Transaksi</label>
                    <input type="text" value="<?php echo TanggalIndo($hasil_transaksi_produksi->tanggal_produksi); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_produksi" id="tanggal_produksi"/>
                  </div>
                 
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                    <label class="gedhi">Jenis Transaksi</label>
                    <input type="text" value="<?php echo $hasil_transaksi_produksi->jenis_produksi; ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_produksi" id="tanggal_produksi"/>
                  </div>
                  </div>
              </div>
            </div>

            <div class="box-body">
              <table id="tabel_daftar" class="table table-bordered table-striped" style="font-size:1.5em;">
                <thead>
                  <tr>
                    <th width="12px">No</th>
                    <th width="50%">Nama bahan</th>
                    <th width="<?php echo ($hasil_transaksi_produksi->jenis_produksi == 'plasma')? '20s%':'50%' ; ?>">QTY</th>
                  <?php if ($hasil_transaksi_produksi->jenis_produksi == 'plasma') { ?>
                    <th width="20%">Afkir Kembali</th>
                  <?php } ?>
                    
                  </tr>
                </thead>
                <tbody id="tabel_temp_data_transaksi">

                  <?php
                  $kode = $this->uri->segment(3);
                  $produksi = $this->db->get_where('opsi_transaksi_produksi',array('kode_produksi'=>$kode));
                  $list_produksi = $produksi->result();
                  $nomor = 1;  $total = 0; $afkir = 0; $sisa = 0;

                  foreach($list_produksi as $daftar){ 
                    @$satuan_bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>@$daftar->kode_bahan));
                    @$hasil_satuan_bahan = $satuan_bahan->row();
                    
                    ?> 
                    <tr>
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo $daftar->nama_bahan; ?></td>
                      <td><?php echo $daftar->jumlah; ?> <?php echo @$hasil_satuan_bahan->satuan_produksi;?></td>
                    <?php if ($hasil_transaksi_produksi->jenis_produksi == 'plasma') { ?>
                      <td><?php echo $daftar->afkir; ?> <?php echo @$hasil_satuan_bahan->satuan_produksi;?></td>
                    <?php } ?>
                     
                    </tr>
                    <?php 
                    $total = $total + (@$daftar->jumlah);
                    $afkir = $afkir + (@$daftar->afkir);
                    $nomor++; 
                  } 
                  ?>
                  
                  <tr>
                    <td colspan="1"></td>
                    <td style="font-weight:bold;">Total</td>
                    <td align="left"><?php echo $total; ?></td>
                  <?php if ($hasil_transaksi_produksi->jenis_produksi == 'plasma') { ?>
                    <td align="left"><?php echo $afkir; ?></td>
                  <?php } ?>
                  </tr>

                  
                </tbody>
                <tfoot>

                </tfoot>
              </table>
              <br>
              
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
$('.btn-back').click(function(){
  $(".tunggu").show();
  window.location = "<?php echo base_url().'produksi/daftar'; ?>";
});
</script>

