
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
         Detail History Karyawan
       </div>
       <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>

    </div>

    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>
   <!--    <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url() . 'produksi/history_karyawan' ?>"><i class="fa fa-list"></i> History Karyawan </a>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app blue" href="<?php echo base_url() . 'produksi/history_plasma' ?>"><i class="fa fa-list"></i> History Plasma </a> 

    -->
    <?php
    $kode_karyawan=$this->uri->segment(3);
    $tanggal = date('Y-m');
    $this->db->where('kode_karyawan',$kode_karyawan);
    $get_plasma=$this->db->get('master_karyawan');
    $hasil_plasma=$get_plasma->row();
     //echo $this->db->last_query();
    ?>
    <a style="padding:13px; margin-bottom:10px;width: 190px" class="btn btn-info blue "><?php echo $hasil_plasma->nama_karyawan; ?></a> 

    <div class="double bg-green pull-right" style="cursor:default">


      <div  style="padding-right:10px; padding-top:0px; font-size:48px; font-family:arial; font-weight:bold">


      </div>
    </div>
    <br><br
    <div class="box-body">            

      <div class="sukses" ></div>
      <br>
      <div class="row">
        <div class="col-md-5" id="">
          <div class="input-group">
            <span class="input-group-addon">Tanggal Awal</span>
            <input type="text" class="form-control tgl" id="tgl_awal">
          </div>
        </div>

        <div class="col-md-5" id="">
          <div class="input-group">
            <span class="input-group-addon">Tanggal Akhir</span>
            <input type="text" class="form-control tgl" id="tgl_akhir">
            <input type="hidden" class="form-control tgl" value="<?php echo $this->uri->segment(3)?>" id="kode_unit">
          </div>
        </div>                        
        <div class="col-md-2 pull-left">
          <button style="width: 190px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
        </div>
      </div>
      <div id="cari_transaksi">
        <table class="table table-striped table-hover table-bordered" id="daftar_produksi"  style="font-size:1.5em;">
         <?php
         //   $this->db->order_by('id', 'desc');
         // //$this->db->where('position','gudang');
         $this->db->limit(50);

         $this->db->like('tanggal_produksi',$tanggal);
         $this->db->where('kode_karyawan',$hasil_plasma->kode_karyawan);
         $produksi = $this->db->get('opsi_transaksi_produksi');
         $hasil_produksi = $produksi->result();
         ?>
         <thead>
          <tr>
            <th>No</th>
            <th>Tanggal Produksi</th>
            <th>Kode Produksi</th>
            <th>Target</th>
            <th>Jumlah Produksi</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody id="posts">
          <?php
          $nomor = 1;

          foreach($hasil_produksi as $daftar){ 
            $this->db->where('kode_produksi',$daftar->kode_produksi);
            $trasnsaksi_produksi = $this->db->get('transaksi_produksi');
            $hasil_trasnsaksi_produksi = $trasnsaksi_produksi->row();

            $this->db->where('kode_transaksi',$hasil_trasnsaksi_produksi->kode_perintah_produksi);
            $this->db->where('kode_bahan',$daftar->kode_bahan);
            $this->db->where('kode_karyawan',$daftar->kode_karyawan);
            $trasnsaksi_produksi = $this->db->get('opsi_transaksi_perintah_produksi');
            $hasil_trasnsaksi_produksi = $trasnsaksi_produksi->row();

            ?> 
            <tr>
              <td><?php echo $nomor; ?></td>
              <td><?php echo TanggalIndo(@$daftar->tanggal_produksi);?></td>
              <td><?php echo @$daftar->kode_produksi; ?></td>
              <td><?php echo @$hasil_trasnsaksi_produksi->jumlah;?></td>
              <td><?php echo @$daftar->jumlah; ?></td>
              <td align="center">
                <?php
                if(@$hasil_trasnsaksi_produksi->jumlah==@$daftar->jumlah){
                  ?>
                  <a class="btn btn-info  ">Sesuai</a> 
                  <?php
                }else{
                  ?>
                  <a class="btn btn-danger ">Tidak Sesuai</a> 
                  <?php
                }
                ?>
              </td>
            </tr>
            <?php $nomor++; } ?>

          </tbody>
          <tfoot>
            <tr>
             <th>No</th>
             <th>Tanggal Produksi</th>
             <th>Kode Produksi</th>
             <th>Target</th>
             <th>Jumlah Produksi</th>
             <th>Status</th>
           </tr>
         </tfoot>
       </table>
       <br><br><br><br><br><br><br><br>
       <br><br><br><br><br><br><br><br>
       <?php
       $this->db->where('kode_karyawan',$hasil_plasma->kode_karyawan); 
       $get_jumlah = $this->db->get('opsi_transaksi_produksi');
       $jumlah = $get_jumlah->num_rows();
       $jumlah = floor($jumlah/50);
       ?>
       <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
       <input type="hidden" class="form-control pagenum" value="0">
     </div>
     <!--  -->

   </div>

   <!------------------------------------------------------------------------------------------------------>

 </div>
</div>
</div><!-- /.col -->
</div>
</div>    
</div>  
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>

<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;" />

<div id="modal_setting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
        <label><b><i class="fa fa-gears"></i>Setting</b></label>
      </div>

      <form id="form_setting" >
        <div class="modal-body">
          <?php
          // $setting = $this->db->get('setting_produksi');
          // $hasil_setting = $setting->row();
          ?>

          <div class="box-body">

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label>Note</label>
                  <input type="text" name="keterangan"  class="form-control" />
                </div>

              </div>
            </div>

          </div>

          <div class="modal-footer" style="background-color:#eee">
            <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
  <script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
  <link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
  <script>
    $('.btn-back').click(function(){
      window.location = "<?php echo base_url().'produksi/history_produksi'; ?>";
    });

  </script>

  <script>
    $(window).scroll(function(){
      if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
        if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
          var pagenum = parseInt($(".pagenum").val()) + 1;
          $(".pagenum").val(pagenum);
          load_table(pagenum);
        }
      }
    });

    function load_table(page){
      var tgl_awal =$("#tgl_awal").val();
      var tgl_akhir =$("#tgl_akhir").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() . 'produksi/get_table_detail_history' ?>",
        data: ({tgl_awal:tgl_awal,tgl_akhir:tgl_akhir, page:$(".pagenum").val()}),
        beforeSend: function(){
          $(".tunggu").show();  
        },
        success: function(msg)
        {
          $(".tunggu").hide();
          $("#posts").append(msg);

        }
      });
    }
  </script>
  <script type="text/javascript">

    $('.tgl').Zebra_DatePicker({});


    $('#cari').click(function(){
      var tgl_awal =$("#tgl_awal").val();
      var tgl_akhir =$("#tgl_akhir").val();
      var kode_unit =$("#kode_unit").val();
      if (tgl_awal=='' || tgl_akhir==''){ 
        alert('Masukan Tanggal Awal & Tanggal Akhir..!')
      }
      else{
        $.ajax( {  
          type :"post",  
          url : "<?php echo base_url().'produksi/cari_detail_produksi'; ?>",  
          cache :false,
          beforeSend:function(){
            $(".tunggu").show();  
          },  
          data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit},
          beforeSend:function(){
            $(".tunggu").show();  
          },
          success : function(data) {
           $(".tunggu").hide();  
           $("#cari_transaksi").html(data);
         },  
         error : function(data) {  
         // alert("das");  
       }  
     });
      }

      $('#tgl_awal').val('');
      $('#tgl_akhir').val('');

    });
  </script>
  <script>
    function setting() {
      $('#modal_setting').modal('show');
    }

    $(document).ready(function(){
      $("#daftar_produksi").dataTable({
        "paging":   false,
        "ordering": true,
        "searching": false,
        "info":     false
      });


      $("#form_setting").submit(function(){
        var keterangan = "<?php echo base_url().'produksi/keterangan'?>";
        $.ajax({
          type: "POST",
          url: keterangan,
          data: $('#form_setting').serialize(),
          success: function(msg)
          {
            $('#modal_setting').modal('hide');  
          }
        });
        return false;
      });

    });

  </script>
