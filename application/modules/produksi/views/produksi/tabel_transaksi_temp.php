<?php
if(@$kode){
  $this->db->group_by('kode_karyawan');
  $group = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode,'status'=>''));
  $list_group = $group->result();
  foreach ($list_group as $list_group) {


    $pembelian = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode,'status'=>'','kode_karyawan'=>$list_group->kode_karyawan ));

    $list_pembelian = $pembelian->result();
    $list_pembelian = $pembelian->result();
    $nomor = 1;  $total = 0;
    $grand_total = 0;
    foreach($list_pembelian as $daftar){
      if($daftar->kategori_bahan=="Bahan Setengah Jadi"){
        @$satuan_bahan = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>@$daftar->kode_bahan));
        @$hasil_satuan_bahan = $satuan_bahan->row();
      }else if($daftar->kategori_bahan=="Bahan Jadi"){
        @$satuan_bahan = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>@$daftar->kode_bahan));
        @$hasil_satuan_bahan = $satuan_bahan->row();
      }else{
        @$satuan_bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>@$daftar->kode_bahan));
        @$hasil_satuan_bahan = $satuan_bahan->row();
      }

      if($daftar->jenis_produksi=="plasma"){
        @$harga_bahan = $this->db->get_where('opsi_bahan_plasma',array('kode_bahan'=>@$daftar->kode_bahan,'kode_plasma'=>@$daftar->kode_karyawan));
        @$hasil_harga_bahan = $harga_bahan->row();
      }
      ?> 
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo @$daftar->nama_karyawan; ?></td>
        <td><?php echo @$daftar->nama_bahan; ?></td>
        <td><?php echo @format_angka($daftar->jumlah); ?> <?php echo @$hasil_satuan_bahan->satuan_stok?></td>
        <td class="plasma"><?php echo @format_angka($daftar->afkir); ?> <?php echo @$hasil_satuan_bahan->satuan_stok?></td>
        <td><?php if($daftar->jenis_produksi=="plasma"){
          echo format_rupiah(@$hasil_harga_bahan->harga );
        }else{ 
          echo format_rupiah(@$hasil_satuan_bahan->tarif_borongan );
        }?></td>
        <td><?php  if($daftar->jenis_produksi=="plasma"){
          echo format_rupiah(@$hasil_harga_bahan->harga * @$daftar->jumlah);
        }else{ 
          echo format_rupiah(@$hasil_satuan_bahan->tarif_borongan * @$daftar->jumlah);
        } ?></td>

        <td width="100px"><?php echo get_edit_del_id(@$daftar->id); ?></td>
      </tr>
      <?php 
      @$total = $total + @$daftar->subtotal;
      $grand_total += @$hasil_harga_bahan->harga * @$daftar->jumlah;
      $nomor++; 
      $jenis_produksi=$daftar->jenis_produksi;
    }
    ?>
    <tr class="plasma">
      <td colspan="5"></td>
      <td>Piutang Plasma</td>
      <td width="300px">
        <?php
        $piutang = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$daftar->kode_karyawan));
        $hasil_piutang = $piutang->row();
        ?>
        <input readonly type="text" value="<?php echo format_rupiah(@$hasil_piutang->sisa); ?>" class="form-control" placeholder="Piutang Plasma" name="piutang_plasma" id="piutang_plasma" required=""/>
        <input type="hidden" value="<?php echo (@$hasil_piutang->sisa); ?>" id="piutang_<?php echo @$daftar->kode_karyawan; ?>" />
      </td>
      <td></td>
    </tr>
    <tr class="plasma">
      <td colspan="5"></td>
      <td>Angsuran Plasma</td>
      <td width="300px"><input type="text" class="form-control angsuran jumlah_data" kode="<?php echo @$daftar->kode_karyawan; ?>" placeholder="Angsuran" name="angsuran" id="angsuran_<?php echo @$daftar->kode_karyawan; ?>" required=""/></td>
      <td></td>
    </tr>
    <tr class="plasma">
      <td colspan="5"></td>
      <td>Grand Total</td>
      <td width="300px">

        <input readonly type="text" id="total_<?php echo @$daftar->kode_karyawan; ?>" value="<?php echo format_rupiah($grand_total); ?>" class="form-control" placeholder="Piutang Plasma" name="piutang_plasma" required=""/>
        <input type="hidden" value="<?php echo ($grand_total); ?>" id="grand_<?php echo @$daftar->kode_karyawan; ?>" />
        <input type="hidden" value="<?php echo ($grand_total); ?>" id="kunci_<?php echo @$daftar->kode_karyawan; ?>" />
      </td>
      <td></td>
    </tr>
    <tr>
      <td style="background-color: #bcbebf;height: 5px;" colspan="7"></td>
    </tr>
    <?php 
  }}
  ?>
  <script type="text/javascript">
    <?php
    if($jenis_produksi=='plasma'){
      ?>
      $(".plasma").show();
      <?php
    }else{
      ?>

      $(".plasma").hide();
      <?php
    }
    ?>
    $(".angsuran").keyup(function(){
     var kode_plasma = $(this).attr('kode');
     var kode_produksi =  "<?php echo $kode; ?>";
     var total = $("#kunci_"+kode_plasma).val();
     var angsuran = $("#angsuran_"+kode_plasma).val();

     var jumlah = parseInt(angsuran);

     var piutang = $("#piutang_"+kode_plasma).val();
  // alert(angsuran);
  var url = "<?php echo base_url().'produksi/get_total' ?>";
  if (jumlah > piutang || angsuran < 0){
    alert("Silahkan cek inputan quantitynya !");
    $("#angsuran_"+kode_plasma).val('');
  }else{
    $.ajax({
      type: "POST",
      url: url,
      dataType:'json',
      data: {kode_plasma:kode_plasma,kode_produksi:kode_produksi,total:total,angsuran:angsuran,piutang:piutang},
      success: function(hasil) {    
        $("#total_"+kode_plasma).val(hasil.rupiah);
        $("#grand_"+kode_plasma).val(hasil.nilai);

      }
    });
  }
});

</script>