<table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                            <?php
                              $this->db->limit(100);
                              if($this->input->post('nama_produk')!=""){
                                $nama_produk = $this->input->post('nama_produk');
                                $this->db->like('nama_bahan_baku', $nama_produk);
                              }
                              $this->db->where('status','menunggu');
                              $perintah = $this->db->get('transaksi_perintah_produksi');
                              $hasil_perintah = $perintah->result();
                            ?>
                             <thead>
                              <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Tanggal Transaksi</th>
                                <th>Nama</th>
                                <th>Petugas</th>
                                <th>Jenis Perintah Produksi</th>
                                <th>Status</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $nomor = 1;

                                    foreach($hasil_perintah as $daftar){ ?> 
                                    <tr>
                                      <td><?php echo $nomor; ?></td>
                                      <td><?php echo $daftar->kode_transaksi; ?></td>
                                      <td><?php echo TanggalIndo($daftar->tanggal_input); ?></td>
                                      <td><?php echo $daftar->nama_karyawan; ?></td>
                                      <td><?php echo $daftar->petugas; ?></td>
                                      <td><?php echo $daftar->jenis_produksi; ?></td>
                                      <td><?php echo cek_status_retur($daftar->status); ?></td>
                                      
                                    </tr>
                                <?php $nomor++; } ?>
                            </tbody>
                              <tfoot>
                                <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Tanggal Transaksi</th>
                                <th>Nama</th>
                                <th>Petugas</th>
                                <th>Jenis Perintah Produksi</th>
                                <th>Status</th>
                                
                              </tr>
                             </tfoot>
                           </table>
                           <?php 
             $this->db->where('status','menunggu');
             $get_jumlah = $this->db->get('transaksi_perintah_produksi');
             $jumlah = $get_jumlah->num_rows();
             $jumlah = floor($jumlah/100);
             ?>