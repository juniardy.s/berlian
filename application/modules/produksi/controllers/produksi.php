<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/menu_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function tambah()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/tambah_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}
	
	public function daftar()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/daftar_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function history_produksi()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/history_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function detail_history_produksi()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/detail_history_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function history_plasma()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/history_plasma', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function detail_history_plasma()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/detail_history_plasma', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function detail()
	{
		$data['aktif']='produksi';
		$data['konten'] = $this->load->view('produksi/produksi/detail_produksi', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function get_produksi_temp($kode){
		$data['kode'] = $kode ;
		$this->load->view('produksi/produksi/tabel_transaksi_temp',$data);
	}
	public function get_barang_lain_temp($kode){
		$data['kode'] = $kode ;
		$this->load->view('produksi/produksi/tabel_barang_lain_temp',$data);
	}
	public function cari_produksi(){

		$this->load->view('produksi/produksi/cari_produksi');
	}

	public function cari_detail_produksi(){

		$this->load->view('produksi/produksi/cari_detail_produksi');
	}

	public function cari_detail_plasma(){

		$this->load->view('produksi/produksi/cari_detail_plasma');
	}

	public function cari_history_karyawan(){

		$this->load->view('produksi/produksi/cari_history_karyawan');
	}
	public function cari_history_plasma(){

		$this->load->view('produksi/produksi/cari_history_plasma');
	}
	public function get_table()
	{
		
		$start = (50*$this->input->post('page'));

		$data = $this->input->post();
		$this->db->limit(50, $start);
		if(@$data['tgl_awal'] && @$data['tgl_akhir']){
			
			$tgl_awal = $data['tgl_awal'];
			$tgl_akhir = $data['tgl_akhir'];
			
			
			$this->db->where('tanggal_produksi >=', $tgl_awal);
			$this->db->where('tanggal_produksi <=', $tgl_akhir);
			
			
		}
		$bahan_setengah = $this->db->get('transaksi_produksi');
		$hasil_bahan_setengah = $bahan_setengah->result();

		$nomor = $start+1;
		foreach($hasil_bahan_setengah as $daftar){
			?>
			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo TanggalIndo(@$daftar->tanggal_produksi);?></td>
				<td><?php echo @$daftar->kode_produksi; ?></td>
				<td><?php echo @$daftar->kode_perintah_produksi; ?></td>
				<td><?php echo @$daftar->jumlah; ?></td>

				<td align="center"><?php echo get_detail($daftar->kode_produksi); ?>
					<?php 
					if(@$daftar->status == 'sesuai' and $daftar->dibayar == ''){
						?>
						<a href="<?php echo base_url().'produksi/pembayaran/'.$daftar->kode_produksi ?>" class="btn btn-warning btn-sm">Pembayaran</a>
						<?php
					}
					?>


				</td>
			</tr>
			<?php $nomor++; 
		}
	}

	public function get_table_history()
	{

		$start = (50*$this->input->post('page'));

		$data = $this->input->post();
		$this->db->limit(50, $start);
		$produksi = $this->db->get('master_karyawan');
		$hasil_produksi = $produksi->result();

		$nomor = $start+1;
		foreach($hasil_produksi as $daftar){
			?>
			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo @$daftar->kode_karyawan; ?></td>
				<td><?php echo @$daftar->nama_karyawan; ?></td>

				<td align="center">
					<a href="<?php echo base_url().'produksi/detail_history_produksi/'.$daftar->kode_karyawan ?>" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle green"><i class="fa fa-search"></i></a>
				</td>
			</tr>
			<?php $nomor++; 
		}
	}

	public function get_table_detail_history()
	{

		$start = (50*$this->input->post('page'));

		$data = $this->input->post();
		$this->db->limit(50, $start);
		$this->db->where('kode_karyawan',$hasil_plasma->kode_karyawan); 
		$produksi = $this->db->get('opsi_transaksi_produksi');
		$hasil_produksi = $produksi->result();

		$nomor = $start+1;
		foreach($hasil_produksi as $daftar){
			$this->db->where('kode_produksi',$daftar->kode_produksi);
			$trasnsaksi_produksi = $this->db->get('transaksi_produksi');
			$hasil_trasnsaksi_produksi = $trasnsaksi_produksi->row();

			$this->db->where('kode_transaksi',$hasil_trasnsaksi_produksi->kode_perintah_produksi);
			$this->db->where('kode_bahan',$daftar->kode_bahan);
			$this->db->where('kode_karyawan',$daftar->kode_karyawan);
			$trasnsaksi_produksi = $this->db->get('opsi_transaksi_perintah_produksi');
			$hasil_trasnsaksi_produksi = $trasnsaksi_produksi->row();
			?>
			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo TanggalIndo(@$daftar->tanggal_produksi);?></td>
				<td><?php echo @$daftar->kode_produksi; ?></td>
				<td><?php echo @$hasil_trasnsaksi_produksi->jumlah;?></td>
				<td><?php echo @$daftar->jumlah; ?></td>
				<td align="center">
					<?php
					if(@$hasil_trasnsaksi_produksi->jumlah==@$daftar->jumlah){
						?>
						<a class="btn btn-info  ">Sesuai</a> 
						<?php
					}else{
						?>
						<a class="btn btn-danger ">Tidak Sesuai</a> 
						<?php
					}
					?>
				</td>
			</tr>
			<?php $nomor++; 
		}
	}

	public function get_bahan(){
		$jenis_bahan = $this->input->post('jenis_bahan');
		$jenis_produksi = $this->input->post('jenis_produksi');
		if($jenis_produksi=='sendiri'){
			if($jenis_bahan=='Bahan Setengah Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_setengah=$this->db->get_where('master_bahan_setengah_jadi', array('jenis_bahan' => 'sendiri'));
				$hasil_bahan_setengah=$bahan_setengah->result();
				foreach ($hasil_bahan_setengah as $bahan) {
					echo"<option value=".$bahan->kode_bahan_setengah_jadi.">".$bahan->nama_bahan_setengah_jadi."</option>";
				}
			}else if($jenis_bahan=='Bahan Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_jadi=$this->db->get('master_bahan_jadi',array('jenis_bahan' => 'sendiri'));
				$hasil_bahan_jadi=$bahan_jadi->result();
				foreach ($hasil_bahan_jadi as $bahan) {
					echo"<option value=".$bahan->kode_bahan_jadi.">".$bahan->nama_bahan_jadi."</option>";
				}
			}
		}else if($jenis_produksi=='plasma'){
			if($jenis_bahan=='Bahan Setengah Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_setengah=$this->db->get_where('master_bahan_setengah_jadi', array('status' => 'sendiri','jenis_bahan' => 'plasma'));
				$hasil_bahan_setengah=$bahan_setengah->result();
				foreach ($hasil_bahan_setengah as $bahan) {
					echo"<option value=".$bahan->kode_bahan_setengah_jadi.">".$bahan->nama_bahan_setengah_jadi."</option>";
				}
			}else if($jenis_bahan=='Bahan Jadi'){
				echo"<option>--Pilih Bahan--</option>";
				$bahan_jadi=$this->db->get('master_bahan_jadi',array('status' => 'sendiri','jenis_bahan' => 'plasma'));
				$hasil_bahan_jadi=$bahan_jadi->result();
				foreach ($hasil_bahan_jadi as $bahan) {
					echo"<option value=".$bahan->kode_bahan_jadi.">".$bahan->nama_bahan_jadi."</option>";
				}
			}
		}

	}
	public function get_kode_bahan(){
		$jenis_bahan = $this->input->post('jenis_bahan');
		$kode_bahan = $this->input->post('kode_bahan');
		if($jenis_bahan=='Bahan Setengah Jadi'){

			$bahan_setengah=$this->db->get_where('master_bahan_setengah_jadi', array('kode_bahan_setengah_jadi'=>$kode_bahan));
			$hasil_bahan=$bahan_setengah->row();

		}else if($jenis_bahan=='Bahan Jadi'){

			$bahan_jadi=$this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi'=>$kode_bahan));
			$hasil_bahan=$bahan_jadi->row();

		}
		echo json_encode($hasil_bahan);
	}
	public function get_kode_barang_lain(){

		$kode_barang_lain = $this->input->post('kode_barang_lain');


		$bahan_jadi=$this->db->get_where('master_bahan_baku', array('kode_bahan_baku'=>$kode_barang_lain));
		$hasil_barang=$bahan_jadi->row();


		echo json_encode($hasil_barang);
	}
	public function get_kode_perintah()
	{
		$kode_perintah = $this->input->post('kode_perintah');

		$kode_produksi = $this->input->post('kode_produksi');
		$query = $this->db->get_where('transaksi_perintah_produksi',array('kode_transaksi' => $kode_perintah,'status' =>'menunggu'));
		$data=$query->row();
		//echo $this->db->last_query();
		$jumlah = count($data);
		if($jumlah > 0){
			$cek_temp = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi));
			$hasil_cek = $cek_temp->num_rows();
			if($hasil_cek<1){
				$produksi = $this->db->get_where('opsi_transaksi_perintah_produksi',array('kode_transaksi'=>$kode_perintah));
				$list_produksi = $produksi->result();
				//echo $this->db->last_query();
				foreach($list_produksi as $daftar){ 
					$masukan['kode_produksi'] = $kode_produksi;

					$masukan['kode_bahan'] = $daftar->kode_bahan;
					$masukan['nama_bahan'] = $daftar->nama_bahan; 
					$masukan['jumlah'] = $daftar->jumlah_sisa; 
					// $masukan['afkir'] = $daftar->afkir; 
					$masukan['jenis_produksi'] = $data->jenis_produksi;
					if($daftar->kategori_bahan=='setengah_jadi'){
						$kategori_bahan='Bahan Setengah Jadi';
					}else if($daftar->kategori_bahan=='jadi'){
						$kategori_bahan='Bahan Jadi';
					}

					$masukan['kategori_bahan']=$kategori_bahan;
					$masukan['kode_karyawan']=$daftar->kode_karyawan;
					$masukan['nama_karyawan']=$daftar->nama_karyawan;

					$bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$daftar->kode_bahan,'nama_bahan_baku'=>$daftar->nama_bahan));
					$hasil_bahan = $bahan->row();
					
					if ($data->jenis_produksi == 'plasma') {
						$bahan_setengah_jadi = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan,'status'=>'plasma','kode_plasma'=>$daftar->kode_karyawan));
						$hasil_bahan_setengah_jadi = $bahan_setengah_jadi->row();
					}
					
					if ($daftar->jumlah > 0 || @$hasil_bahan_setengah_jadi->afkir > 0) {
						$input = $this->db->insert('opsi_transaksi_produksi_temp',$masukan);
					}
				}
			}else{
				$this->db->delete('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi));
				$produksi = $this->db->get_where('opsi_transaksi_perintah_produksi',array('kode_transaksi'=>$kode_perintah));
				$list_produksi = $produksi->result();
				//echo $this->db->last_query();
				foreach($list_produksi as $daftar){ 
					$masukan['kode_produksi'] = $kode_produksi;

					$masukan['kode_bahan'] = $daftar->kode_bahan;
					$masukan['nama_bahan'] = $daftar->nama_bahan; 
					$masukan['jumlah'] = $daftar->jumlah_sisa; 
					// $masukan['afkir'] = $daftar->afkir; 
					$masukan['jenis_produksi'] = $data->jenis_produksi;
					if($daftar->kategori_bahan=='setengah_jadi'){
						$kategori_bahan='Bahan Setengah Jadi';
					}else if($daftar->kategori_bahan=='jadi'){
						$kategori_bahan='Bahan Jadi';
					}

					$masukan['kategori_bahan']=$kategori_bahan;
					$masukan['kode_karyawan']=$daftar->kode_karyawan;
					$masukan['nama_karyawan']=$daftar->nama_karyawan;
					
					if ($data->jenis_produksi == 'plasma') {
						$bahan_setengah_jadi = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$daftar->kode_bahan,'status'=>'plasma','kode_plasma'=>$daftar->kode_karyawan));
						$hasil_bahan_setengah_jadi = $bahan_setengah_jadi->row();
					}
					
					if ($daftar->jumlah > 0 || @$hasil_bahan_setengah_jadi->afkir > 0) {
						$input = $this->db->insert('opsi_transaksi_produksi_temp',$masukan);
					}
				}
			}
			echo "1|".$data->kode_transaksi.'|'.$data->jenis_produksi.'|'.$data->kode_karyawan.'|'.$data->nama_karyawan;
		}
		else{

			echo "0";
		}
	}

	public function simpan_produksi_temp(){
		$data=$this->input->post();
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		$hasil_cek_bahan = $cek_bahan->num_rows();
		$hasil_bahan = $cek_bahan->row();
		if($hasil_cek_bahan >0){
			$update_temp['jumlah']=$data['jumlah'] + $hasil_bahan->jumlah;
			$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		}else{
			$data_temp['kode_produksi']=$data['kode_produksi'];
			$data_temp['kode_bahan']=$data['kode_bahan'];
			$data_temp['nama_bahan']=$data['nama_bahan'];
			$data_temp['jumlah']=$data['jumlah'];
			$data_temp['jenis_produksi']=$data['jenis_produksi'];
			$data_temp['kategori_bahan']=$data['jenis_bahan'];
			$data_temp['tanggal_produksi']=date('Y-m-d');
			$input = $this->db->insert('opsi_transaksi_produksi_temp',$data_temp);

		}

	}
	public function simpan_barang_lain_temp(){
		$data=$this->input->post();
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		$hasil_cek_bahan = $cek_bahan->num_rows();
		$hasil_bahan = $cek_bahan->row();
		if($hasil_cek_bahan >0){
			$update_temp['jumlah']=$data['jumlah'] + $hasil_bahan->jumlah;
			$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		}else{
			$data_temp['kode_produksi']=$data['kode_produksi'];
			$data_temp['kode_bahan']=$data['kode_bahan'];
			$data_temp['nama_bahan']=$data['nama_bahan'];
			$data_temp['jumlah']=$data['jumlah'];
			$data_temp['jenis_produksi']=$data['jenis_produksi'];
			$data_temp['kategori_bahan']=$data['jenis_barang_lain'];
			$data_temp['status']='barang_lain';
			$data_temp['tanggal_produksi']=date('Y-m-d');
			$input = $this->db->insert('opsi_transaksi_produksi_temp',$data_temp);

		}

	}
	public function get_temp_produksi(){
		$id = $this->input->post('id');
		$kode_produksi = $this->input->post('kode_produksi');
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('id'=>$id,'kode_produksi'=>$kode_produksi));
		$hasil_bahan_temp = $cek_bahan->row();
		echo json_encode($hasil_bahan_temp);
	}
	public function get_temp_barang_lain(){
		$id = $this->input->post('id');
		$kode_produksi = $this->input->post('kode_produksi');
		$cek_bahan = $this->db->get_where('opsi_transaksi_produksi_temp',array('id'=>$id,'kode_produksi'=>$kode_produksi,'status'=>'barang_lain'));
		$hasil_bahan_temp = $cek_bahan->row();
		echo json_encode($hasil_bahan_temp);
	}
	public function update_produksi_temp(){
		$data=$this->input->post();

		$cek_opsi = $this->db->get_where('opsi_transaksi_perintah_produksi',array('kode_transaksi'=>$data['kode_perintah'],'kode_bahan'=>$data['kode_bahan']));
		$hasil_opsi_temp = $cek_opsi->row();
		//echo $this->db->last_query();

		// Cek Quantity
		if (@$hasil_opsi_temp->jumlah_sisa < $data['jumlah']) {
			echo"<div class='alert alert-danger'>Quantity Lebih dari Perintah Produksi</div>";
			exit();
		}

		// Cek Afkir
		if (substr(@$hasil_opsi_temp->kode_karyawan, 0, 3) == 'PLM'){
			if ($hasil_opsi_temp->kategori_bahan == 'jadi') {
				$bahan_jadi = $this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi' => $hasil_opsi_temp->kode_bahan, 'status' => 'plasma', 'kode_plasma' => $hasil_opsi_temp->kode_karyawan))->row();
				if ($bahan_jadi->afkir < $data['afkir']) {
					echo"<div class='alert alert-danger'>Afkir Lebih dari yang ada di Plasma</div>";
					exit();
				}
			} else if ($hasil_opsi_temp->kategori_bahan == 'setengah_jadi') {
				$bahan_setengah_jadi = $this->db->get_where('master_bahan_setengah_jadi', array('kode_bahan_setengah_jadi' => $hasil_opsi_temp->kode_bahan, 'status' => 'plasma', 'kode_plasma' => $hasil_opsi_temp->kode_karyawan))->row();
				if ($bahan_setengah_jadi->afkir < $data['afkir']) {
					echo"<div class='alert alert-danger'>Afkir Lebih dari yang ada di Plasma</div>";
					exit();
				}
			}
		}

		if (@$hasil_opsi_temp->jumlah_sisa < $data['jumlah']) {
			echo"<div class='alert alert-danger'>Quantity Lebih dari Perintah Produksi</div>";
			exit();
		}

		if($data['jumlah'] > 0 || $data['afkir'] > 0){
			$update_temp['kode_produksi']=$data['kode_produksi'];
			$update_temp['kode_bahan']=$data['kode_bahan'];
			$update_temp['nama_bahan']=$data['nama_bahan'];
			$update_temp['jumlah']=$data['jumlah'];
			$update_temp['afkir']=intval($data['afkir']);
			$update_temp['jenis_produksi']=$data['jenis_produksi'];
			$update_temp['kategori_bahan']=$data['jenis_bahan'];
			$update_temp['tanggal_produksi']=date('Y-m-d');

			$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('id'=>$data['id'],'kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		}else{
			echo"<div class='alert alert-danger'>Terjadi Kesalahan</div>";
		}



	}

	public function update_barang_lain_temp(){
		$data=$this->input->post();

		$update_temp['kode_produksi']=$data['kode_produksi'];
		$update_temp['kode_bahan']=$data['kode_bahan'];
		$update_temp['nama_bahan']=$data['nama_bahan'];
		$update_temp['jumlah']=$data['jumlah'];
		$update_temp['jenis_produksi']=$data['jenis_produksi'];
		$update_temp['kategori_bahan']=$data['jenis_barang_lain'];
		$update_temp['tanggal_produksi']=date('Y-m-d');

		$this->db->update('opsi_transaksi_produksi_temp',$update_temp,array('id'=>$data['id'],'kode_bahan'=>$data['kode_bahan'],'kode_produksi'=>$data['kode_produksi']));
		echo $this->db->last_query();

	}
	public function hapus_produksi_temp(){
		$id = $this->input->post('id');
		$kode_produksi = $this->input->post('kode_produksi');
		$this->db->delete('opsi_transaksi_produksi_temp',array('id'=>$id,'kode_produksi'=>$kode_produksi));

	}
	public function simpan_transaksi(){
		$data=$this->input->post();
		$kode_produksi=$data['kode_produksi'];
		$tanggal_produksi=$data['tanggal_produksi'];
		$kode_perintah=$data['kode_perintah'];
		$jenis_produksi=$data['jenis_produksi'];

		$user = $this->session->userdata('astrosession');
		$id_petugas = $user->id;
		$nama_petugas = $user->uname;

		$ambil_temp = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi,'jenis_produksi'=>$jenis_produksi));
		$hasil_ambil_temp = $ambil_temp->result();
		$jml_total_produksi=0;
		$jml_total_afkir=0;
		$jml_total_sisa=0;
		foreach ($hasil_ambil_temp as $data_temp) {
			if(!($data_temp->jumlah > 0)) continue;
			$kode_karyawan=$data_temp->kode_karyawan;
			$nama_karyawan=$data_temp->nama_karyawan;
			$kategori_bahan=$data_temp->kategori_bahan;
			$kode_bahan=$data_temp->kode_bahan;
			$jumlah=$data_temp->jumlah;
			$afkir=$data_temp->afkir;
			$jml_total_produksi +=$data_temp->jumlah;
			$jml_total_afkir +=$data_temp->afkir;
			if($kategori_bahan=='Bahan Setengah Jadi'){

				$setengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));
				$hasil_setengah_jadi=$setengah_jadi->row();

				$opsi_setengah_jadi=$this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan));
				$opsi_hasil_setengah_jadi=$opsi_setengah_jadi->result();
				$jml_sisa='cukup';
				$jml_sisa_stok='kurang';
				foreach ($opsi_hasil_setengah_jadi as $cek_sisa) {
					$sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan));
					$hasil_sisa_stok=$sisa_stok->row();
					$jumlah_produksi=$cek_sisa->jumlah * $jumlah;
					$sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;

					if($hasil_sisa_stok->real_stock < $jumlah_produksi){
						$jml_sisa='kurang';
					}else{
						$jml_sisa_stok='cukup';
					}

				}
				//echo $jml_sisa;
				if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
					foreach ($opsi_hasil_setengah_jadi as $opsi_bahan) {
						$bahan_baku=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan));
						$hasil_bahan_baku=$bahan_baku->row();
						$jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
						if($jenis_produksi=="sendiri"){
							$real_stock['real_stock']=$hasil_bahan_baku->real_stock - $jumlah_produksi;
							$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
						}


						$cek_bb_plasma=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
						$hasil_cek_bb_plasma=$cek_bb_plasma->num_rows();
						$real_stok_cek_bb_plasma=$cek_bb_plasma->row();

						if($hasil_cek_bb_plasma > 0 and $jenis_produksi=='plasma'){
							$real_stock_bb_plasma['real_stock']=$real_stok_cek_bb_plasma->real_stock - $jumlah_produksi;

							$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock_bb_plasma,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
						}


						$transaksi_stok['jenis_transaksi']='produksi';
						$transaksi_stok['kode_transaksi']=$data_temp->kode_produksi;
						$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
						$transaksi_stok['kode_bahan']=$hasil_bahan_baku->kode_bahan_baku;
						$transaksi_stok['nama_bahan']=$hasil_bahan_baku->nama_bahan_baku;
						$transaksi_stok['stok_keluar']=$jumlah_produksi;
						$transaksi_stok['id_petugas']=$id_petugas;
						$transaksi_stok['nama_petugas']=$nama_petugas;
						$transaksi_stok['posisi_awal']='Gudang';
						$transaksi_stok['kode_unit_asal']=@$hasil_bahan_baku->kode_unit;
						$transaksi_stok['posisi_akhir']='Gudang';
						$transaksi_stok['kode_unit_tujuan']=@$hasil_bahan_baku->kode_unit;
						$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
						$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);
					}
					if($jenis_produksi=='plasma'){
						$cek_bahan_s_plasma=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
						$hasil_cek_plasma=$cek_bahan_s_plasma->num_rows();
						$plasma_real_stok=$cek_bahan_s_plasma->row();
						if($hasil_cek_plasma > 0){
							$real_stock_setengah_jadi['real_stock']=$plasma_real_stok->real_stock - $jumlah;
							$real_stock_setengah_jadi['afkir']=$plasma_real_stok->afkir - $afkir;
							$update_bahan_setengah_jadi=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));

							$real_stock_setengah_jadi_s['real_stock']=$hasil_setengah_jadi->real_stock + $jumlah + $afkir;
							$update_bahan_setengah_jadi=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi_s,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));	
						}
					}else if ($jenis_produksi=='sendiri') {
						$real_stock_setengah_jadi['real_stock']=$hasil_setengah_jadi->real_stock + $jumlah;
						$update_bahan_setengah_jadi=$this->db->update('master_bahan_setengah_jadi',$real_stock_setengah_jadi,array('kode_bahan_setengah_jadi'=>$kode_bahan,'status'=>'sendiri'));	

						$cek_penggajian=$this->db->get_where('transaksi_penggajian',array('kode_karyawan'=>$kode_karyawan));
						$hasil_cek_penggajian=$cek_penggajian->row();
						if(!empty($hasil_cek_penggajian)){
							$get_karyawan=$this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
							$hasil_get_karyawan=$get_karyawan->row();
							$update_gaji['total_gaji']=@$hasil_cek_penggajian->total_gaji + ($jumlah * @$hasil_setengah_jadi->tarif_borongan);
							$update_gaji['sisa_gaji']=@$hasil_cek_penggajian->sisa_gaji + ($jumlah * @$hasil_setengah_jadi->tarif_borongan);
							$update_gaji['tanggal']=date('Y-m-d');
							//$this->db->update('transaksi_penggajian',$update_gaji,array('kode_karyawan'=>$kode_karyawan));
						}else{
							$data_gaji['kode_karyawan']=$kode_karyawan;
							$data_gaji['nama_karyawan']=$nama_karyawan;
							$get_karyawan=$this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
							$hasil_get_karyawan=$get_karyawan->row();
							$data_gaji['total_gaji']=$jumlah * @$hasil_setengah_jadi->tarif_borongan;
							$data_gaji['sisa_gaji']=$jumlah * @$hasil_setengah_jadi->tarif_borongan;
							$data_gaji['tanggal']=date('Y-m-d');
							//$this->db->insert('transaksi_penggajian',$data_gaji);
						}
						$data_gaji_opsi['kode_transaksi']=$kode_produksi;
						$data_gaji_opsi['kode_karyawan']=$kode_karyawan;
						$data_gaji_opsi['nama_karyawan']=$nama_karyawan;
						$get_karyawan=$this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
						$hasil_get_karyawan=$get_karyawan->row();
						$data_gaji['jenis_karyawan']=$hasil_get_karyawan->jenis_karyawan;
						$data_gaji_opsi['jumlah']= $jumlah;
						$data_gaji_opsi['tarif_borongan']= @$hasil_setengah_jadi->tarif_borongan;
						$data_gaji_opsi['total_gaji']=$jumlah * @$hasil_setengah_jadi->tarif_borongan;
						$data_gaji_opsi['tanggal']=date('Y-m-d');
						$data_gaji_opsi['status']='belum dibayar';
						$this->db->insert('opsi_transaksi_penggajian',$data_gaji_opsi);
					}

					$data_opsi['kode_karyawan']=$kode_karyawan;
					$data_opsi['nama_karyawan']=$nama_karyawan;
					$data_opsi['kode_produksi']=$data_temp->kode_produksi;
					$data_opsi['kategori_bahan']=$data_temp->kategori_bahan;
					$data_opsi['kode_bahan']=$data_temp->kode_bahan;
					$data_opsi['nama_bahan']=$data_temp->nama_bahan;
					$data_opsi['jumlah']=$data_temp->jumlah;
					$data_opsi['afkir']=$data_temp->afkir;
					$data_opsi['jenis_produksi']=$data_temp->jenis_produksi;
					$data_opsi['tanggal_produksi']=date('Y-m-d');
					$insert_opsi=$this->db->insert('opsi_transaksi_produksi',$data_opsi);
					if ($insert_opsi) {
						$get_opsi_perintah = $this->db->get_where('opsi_transaksi_perintah_produksi', array('kode_transaksi' => $data['kode_perintah'], 'kode_bahan' => $data_temp->kode_bahan))->row();
						$opsi_perintah['jumlah_sisa'] = $get_opsi_perintah->jumlah_sisa - $data_temp->jumlah;
						$this->db->update('opsi_transaksi_perintah_produksi', $opsi_perintah, array('kode_transaksi' => $data['kode_perintah'], 'kode_bahan' => $data_temp->kode_bahan));
					}
				}else{
					echo"<div class='alert alert-danger'>Stok ".$data_temp->nama_bahan." Tidak Mencukupi</div>";
					exit();
				}

			}else if($kategori_bahan=='Bahan Jadi'){
				$bahan_jadi=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));
				$hasil_bahan_jadi=$bahan_jadi->row();

				$opsi_bahan_jadi=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
				$opsi_hasil_bahan_jadi=$opsi_bahan_jadi->result();
				//echo $this->db->last_query();
				$jml_sisa='cukup';
				$jml_sisa_stok='kurang';
				foreach ($opsi_hasil_bahan_jadi as $cek_sisa) {
					if($cek_sisa->jenis_bahan=='bahan baku'){
						$sisa_stok=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
						$hasil_sisa_stok=$sisa_stok->row();
					}else if ($cek_sisa->jenis_bahan=='bahan setengah jadi') {
						$sisa_stok=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$cek_sisa->kode_bahan,'status'=>'sendiri'));
						$hasil_sisa_stok=$sisa_stok->row();
					}
					//echo $this->db->last_query();

					$jumlah_produksi=$cek_sisa->jumlah * $jumlah;
					$sisa_real_stok=$hasil_sisa_stok->real_stock - $jumlah_produksi;
					if($hasil_sisa_stok->real_stock < $jumlah_produksi){
						$jml_sisa='kurang';

					}else{
						$jml_sisa_stok='cukup';
					}

				}
				//echo $jml_sisa;
				if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
					$opsi_bahan_jadi1=$this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
					$opsi_hasil_bahan_jadi1=$opsi_bahan_jadi1->result();
					foreach ($opsi_hasil_bahan_jadi1 as $opsi_bahan) {

						if($opsi_bahan->jenis_bahan=='bahan baku'){

							$bahan_baku=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
							$hasil_bahan_baku=$bahan_baku->row();
							$jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
							if($jenis_produksi=='sendiri'){
								$real_stock['real_stock']=$hasil_bahan_baku->real_stock - $jumlah_produksi;

								$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));	

							}

							$bj_bb_plasma=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
							$hasil_bj_bb_plasma=$bj_bb_plasma->num_rows();
							$real_stok_bj_bb_plasma=$bj_bb_plasma->row();
							if($hasil_bj_bb_plasma > 0 and $jenis_produksi=='plasma'){
								$real_stock_bb_plasma['real_stock']=$real_stok_bj_bb_plasma->real_stock - $jumlah_produksi;
								$update_bahan_baku=$this->db->update('master_bahan_baku',$real_stock_bb_plasma,array('kode_bahan_baku'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
							}
							// else if($hasil_bj_bb_plasma == 0 and $jenis_produksi=='plasma'){
							// 	$stok_bj_bb_plasma['kode_bahan_baku']=$hasil_bahan_baku->kode_bahan_baku;
							// 	$stok_bj_bb_plasma['nama_bahan_baku']=$hasil_bahan_baku->nama_bahan_baku;
							// 	$stok_bj_bb_plasma['kode_unit']=@$hasil_bahan_baku->kode_unit;
							// 	$stok_bj_bb_plasma['nama_unit']=@$hasil_bahan_baku->nama_unit;
							// 	$stok_bj_bb_plasma['kode_rak']=@$hasil_bahan_baku->kode_rak;
							// 	$stok_bj_bb_plasma['nama_rak']=@$hasil_bahan_baku->nama_rak;
							// 	$stok_bj_bb_plasma['id_satuan_stok']=@$hasil_bahan_baku->id_satuan_stok;
							// 	$stok_bj_bb_plasma['satuan_stok']=@$hasil_bahan_baku->satuan_stok;
							// 	$stok_bj_bb_plasma['id_satuan_pembelian']=@$hasil_bahan_baku->id_satuan_pembelian;
							// 	$stok_bj_bb_plasma['satuan_pembelian']=@$hasil_bahan_baku->satuan_pembelian;
							// 	$stok_bj_bb_plasma['jumlah_dalam_satuan_pembelian']=@$hasil_bahan_baku->jumlah_dalam_satuan_pembelian;
							// 	$stok_bj_bb_plasma['stok_minimal']=@$hasil_bahan_baku->stok_minimal;
							// 	$stok_bj_bb_plasma['hpp']=@$hasil_bahan_baku->hpp;
							// 	$stok_bj_bb_plasma['real_stock']=$jumlah_produksi;
							// 	$stok_bj_bb_plasma['status']='plasma';
							// 	$stok_bj_bb_plasma['kode_plasma']=$kode_karyawan;
							// 	$stok_bj_bb_plasma['nama_plasma']=$nama_karyawan;
							// 	$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_baku',$stok_bj_bb_plasma);
							// }


							$transaksi_stok['jenis_transaksi']='produksi';
							$transaksi_stok['kode_transaksi']=$data_temp->kode_produksi;
							$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
							$transaksi_stok['kode_bahan']=$hasil_bahan_baku->kode_bahan_baku;
							$transaksi_stok['nama_bahan']=$hasil_bahan_baku->nama_bahan_baku;
							$transaksi_stok['stok_keluar']=$jumlah_produksi;
							$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
							$transaksi_stok['id_petugas']=$id_petugas;
							$transaksi_stok['nama_petugas']=$nama_petugas;
							$transaksi_stok['posisi_awal']='Gudang';
							$transaksi_stok['kode_unit_asal']=@$hasil_bahan_baku->kode_unit;
							$transaksi_stok['posisi_akhir']='Gudang';
							$transaksi_stok['kode_unit_tujuan']=@$hasil_bahan_baku->kode_unit;
							$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
							$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);

						}else if ($opsi_bahan->jenis_bahan=='bahan setengah jadi') {
							$bahan_stengah_jadi=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));
							$hasil_bahan_stengah_jadi=$bahan_stengah_jadi->row();
							// echo $hasil_bahan_stengah_jadi->real_stock;
							// echo $this->db->last_query();
							$jumlah_produksi=$opsi_bahan->jumlah * $jumlah;
							if($jenis_produksi=='sendiri'){
								$real_stock['real_stock']=$hasil_bahan_stengah_jadi->real_stock - $jumlah_produksi;

								$update_bahan_baku=$this->db->update('master_bahan_setengah_jadi',$real_stock,array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'sendiri'));	

							}

							$bj_bsj_plasma=$this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
							$hasil_bj_bsj_plasma=$bj_bsj_plasma->num_rows();
							$real_stok_bj_bsj_plasma=$bj_bsj_plasma->row();
							if($hasil_bj_bsj_plasma > 0 and $jenis_produksi=='plasma'){
								$real_stock_bsj_plasma['real_stock']=$real_stok_bj_bsj_plasma->real_stock - $jumlah_produksi;
								$update_bahan_baku=$this->db->update('master_bahan_setengah_jadi',$real_stock_bsj_plasma,array('kode_bahan_setengah_jadi'=>$opsi_bahan->kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
							}
							// else if($hasil_bj_bsj_plasma == 0 and $jenis_produksi=='plasma'){
							// 	$stok_bj_bsj_plasma['kode_bahan_setengah_jadi']=$hasil_bahan_stengah_jadi->kode_bahan_setengah_jadi;
							// 	$stok_bj_bsj_plasma['nama_bahan_setengah_jadi']=$hasil_bahan_stengah_jadi->nama_bahan_setengah_jadi;
							// 	$stok_bj_bsj_plasma['kode_unit']=@$hasil_bahan_stengah_jadi->kode_unit;
							// 	$stok_bj_bsj_plasma['nama_unit']=@$hasil_bahan_stengah_jadi->nama_unit;
							// 	$stok_bj_bsj_plasma['kode_rak']=@$hasil_bahan_stengah_jadi->kode_rak;
							// 	$stok_bj_bsj_plasma['nama_rak']=@$hasil_bahan_stengah_jadi->nama_rak;
							// 	$stok_bj_bsj_plasma['id_satuan_stok']=@$hasil_bahan_stengah_jadi->id_satuan_stok;
							// 	$stok_bj_bsj_plasma['satuan_stok']=@$hasil_bahan_stengah_jadi->satuan_stok;
							// 	$stok_bj_bsj_plasma['hpp']=@$hasil_bahan_stengah_jadi->hpp;
							// 	$stok_bj_bsj_plasma['stok_minimal']=@$hasil_bahan_stengah_jadi->stok_minimal;
							// 	$stok_bj_bsj_plasma['real_stock']=$jumlah_produksi;
							// 	$stok_bj_bsj_plasma['status']='plasma';
							// 	$stok_bj_bsj_plasma['kode_plasma']=$kode_karyawan;
							// 	$stok_bj_bsj_plasma['nama_plasma']=$nama_karyawan;
							// 	$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_setengah_jadi',$stok_bj_bsj_plasma);
							// }

							$transaksi_stok['jenis_transaksi']='produksi';
							$transaksi_stok['kode_transaksi']=$data_temp->kode_produksi;
							$transaksi_stok['kategori_bahan']=$data_temp->kategori_bahan;
							$transaksi_stok['kode_bahan']=$hasil_bahan_stengah_jadi->kode_bahan_setengah_jadi;
							$transaksi_stok['nama_bahan']=$hasil_bahan_stengah_jadi->nama_bahan_setengah_jadi;
							$transaksi_stok['stok_keluar']=$jumlah_produksi;
							$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
							$transaksi_stok['id_petugas']=$id_petugas;
							$transaksi_stok['nama_petugas']=$nama_petugas;
							$transaksi_stok['posisi_awal']='Gudang';
							$transaksi_stok['kode_unit_asal']=@$hasil_bahan_stengah_jadi->kode_unit;
							$transaksi_stok['posisi_akhir']='Gudang';
							$transaksi_stok['kode_unit_tujuan']=@$hasil_bahan_stengah_jadi->kode_unit;
							$transaksi_stok['tanggal_transaksi']=date('Y-m-d');
							$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok);
						}


					}
					if($jenis_produksi=='plasma'){
						$cek_bahan_j_plasma=$this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));
						$hasil_cek_plasma=$cek_bahan_j_plasma->num_rows();
						$plasma_real_stok=$cek_bahan_j_plasma->row();
						if($hasil_cek_plasma > 0){
							$real_stock_jadi['real_stock']=$plasma_real_stok->real_stock - $jumlah;
							$real_stock_jadi['afkir']=$plasma_real_stok->afkir - $afkir;
							$update_bahan_jadi=$this->db->update('master_bahan_jadi',$real_stock_jadi,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'plasma','kode_plasma'=>$kode_karyawan));	

							$real_stock_jadi_s['real_stock']=$hasil_bahan_jadi->real_stock + $jumlah + $afkir;
							$update_bahan_jadi_s=$this->db->update('master_bahan_jadi',$real_stock_jadi_s,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));	
						}
						// else{
						// 	$stok_plasma['kode_bahan_jadi']=$kode_bahan;
						// 	$stok_plasma['nama_bahan_jadi']=$data_temp->nama_bahan;
						// 	$stok_plasma['kode_unit']=@$hasil_bahan_jadi->kode_unit;
						// 	$stok_plasma['nama_unit']=@$hasil_bahan_jadi->nama_unit;
						// 	$stok_plasma['kode_rak']=@$hasil_bahan_jadi->kode_rak;
						// 	$stok_plasma['nama_rak']=@$hasil_bahan_jadi->nama_rak;
						// 	$stok_plasma['id_satuan_stok']=@$hasil_bahan_jadi->id_satuan_stok;
						// 	$stok_plasma['satuan_stok']=@$hasil_bahan_jadi->satuan_stok;
						// 	$stok_plasma['hpp']=@$hasil_bahan_jadi->hpp;
						// 	$stok_plasma['real_stock']=$jumlah;
						// 	$stok_plasma['status']='plasma';
						// 	$stok_plasma['kode_plasma']=$kode_karyawan;
						// 	$stok_plasma['nama_plasma']=$nama_karyawan;
						// 	$insert_bahan_setengah_jadi=$this->db->insert('master_bahan_jadi',$stok_plasma,array('kode_bahan_jadi'=>$kode_bahan));
						// }
					}else if ($jenis_produksi=='sendiri') {
						$real_stock_jadi['real_stock']=$hasil_bahan_jadi->real_stock + $jumlah;
						$update_bahan_jadi=$this->db->update('master_bahan_jadi',$real_stock_jadi,array('kode_bahan_jadi'=>$kode_bahan,'status'=>'sendiri'));

						$cek_penggajian=$this->db->get_where('transaksi_penggajian',array('kode_karyawan'=>$kode_karyawan));
						$hasil_cek_penggajian=$cek_penggajian->row();
						if(!empty($hasil_cek_penggajian)){
							$get_karyawan=$this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
							$hasil_get_karyawan=$get_karyawan->row();
							$update_gaji['total_gaji']=@$hasil_cek_penggajian->total_gaji + ($jumlah * @$hasil_bahan_jadi->tarif_borongan);
							$update_gaji['sisa_gaji']=@$hasil_cek_penggajian->sisa_gaji + ($jumlah * @$hasil_bahan_jadi->tarif_borongan);
							$update_gaji['tanggal']=date('Y-m-d');
							//$this->db->update('transaksi_penggajian',$update_gaji,array('kode_karyawan'=>$kode_karyawan));
						}else{
							$data_gaji['kode_karyawan']=$kode_karyawan;
							$data_gaji['nama_karyawan']=$nama_karyawan;
							$get_karyawan=$this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
							$hasil_get_karyawan=$get_karyawan->row();
							$data_gaji['total_gaji']=$jumlah * @$hasil_bahan_jadi->tarif_borongan;
							$data_gaji['sisa_gaji']=$jumlah * @$hasil_bahan_jadi->tarif_borongan;
							$data_gaji['tanggal']=date('Y-m-d');
							//$this->db->insert('transaksi_penggajian',$data_gaji);
						}
						$data_gaji_opsi['kode_transaksi']=$kode_produksi;
						$data_gaji_opsi['kode_karyawan']=$kode_karyawan;
						$data_gaji_opsi['nama_karyawan']=$nama_karyawan;
						$get_karyawan=$this->db->get_where('master_karyawan',array('kode_karyawan'=>$kode_karyawan));
						$hasil_get_karyawan=$get_karyawan->row();
						$data_gaji['jenis_karyawan']=$hasil_get_karyawan->jenis_karyawan;
						$data_gaji_opsi['jumlah']= $jumlah;
						$data_gaji_opsi['tarif_borongan']= @$hasil_bahan_jadi->tarif_borongan;
						$data_gaji_opsi['total_gaji']=$jumlah * @$hasil_bahan_jadi->tarif_borongan;
						$data_gaji_opsi['tanggal']=date('Y-m-d');
						$data_gaji_opsi['status']='belum dibayar';
						$this->db->insert('opsi_transaksi_penggajian',$data_gaji_opsi);
					}

					$data_opsi['kode_karyawan']=$kode_karyawan;
					$data_opsi['nama_karyawan']=$nama_karyawan;
					$data_opsi['kode_produksi']=$data_temp->kode_produksi;
					$data_opsi['kategori_bahan']=$data_temp->kategori_bahan;
					$data_opsi['kode_bahan']=$data_temp->kode_bahan;
					$data_opsi['nama_bahan']=$data_temp->nama_bahan;
					$data_opsi['jumlah']=$data_temp->jumlah;
					$data_opsi['jenis_produksi']=$data_temp->jenis_produksi;
					$data_opsi['tanggal_produksi']=date('Y-m-d');
					$insert_opsi=$this->db->insert('opsi_transaksi_produksi',$data_opsi);
					if ($insert_opsi) {
						$get_opsi_perintah = $this->db->get_where('opsi_transaksi_perintah_produksi', array('kode_transaksi' => $data['kode_perintah'], 'kode_bahan' => $data_temp->kode_bahan))->row();
						$opsi_perintah['jumlah_sisa'] = $get_opsi_perintah->jumlah_sisa - $data_temp->jumlah;
						$this->db->update('opsi_transaksi_perintah_produksi', $opsi_perintah, array('kode_transaksi' => $data['kode_perintah'], 'kode_bahan' => $data_temp->kode_bahan));
					}

				}else{
					echo"<div class='alert alert-danger'>Stok ".$data_temp->nama_bahan." Tidak Mencukupi</div>";
					exit();
				}
			}
			if(@$jml_sisa=='cukup' and @$jml_sisa_stok=='cukup'){
				if($jenis_produksi=='plasma'){
					$transaksi_stok_akhir['jenis_transaksi']='produksi';
					$transaksi_stok_akhir['kode_transaksi']=$data_temp->kode_produksi;
					$transaksi_stok_akhir['kategori_bahan']=$data_temp->kategori_bahan;
					$transaksi_stok_akhir['kode_bahan']=$data_temp->kode_bahan;
					$transaksi_stok_akhir['nama_bahan']=$data_temp->nama_bahan;
					$transaksi_stok_akhir['stok_keluar']=$jumlah;
				}else if ($jenis_produksi=='sendiri') {
					$transaksi_stok_akhir['jenis_transaksi']='produksi';
					$transaksi_stok_akhir['kode_transaksi']=$data_temp->kode_produksi;
					$transaksi_stok_akhir['kategori_bahan']=$data_temp->kategori_bahan;
					$transaksi_stok_akhir['kode_bahan']=$data_temp->kode_bahan;
					$transaksi_stok_akhir['nama_bahan']=$data_temp->nama_bahan;
					$transaksi_stok_akhir['stok_masuk']=$jumlah;
				}

				$transaksi_stok_akhir['id_petugas']=$id_petugas;
				$transaksi_stok_akhir['nama_petugas']=$nama_petugas;
				$transaksi_stok_akhir['tanggal_transaksi']=date('Y-m-d');
				$transaksi_stok_akhir['posisi_awal']='Gudang';
				$transaksi_stok_akhir['kode_unit_asal']='U001';
				$transaksi_stok_akhir['posisi_akhir']='Gudang';
				$transaksi_stok_akhir['kode_unit_tujuan']='U001';
				$insert_transaksi_stok=$this->db->insert('transaksi_stok',$transaksi_stok_akhir);

				$this->db->delete('opsi_transaksi_produksi_temp',array('kode_bahan' =>$kode_bahan,'kode_produksi'=>$kode_produksi ));
			}



		}
		if(@$insert_opsi){
			$cek_perintah_selesai = true;
			$get_opsi_perintah = $this->db->get_where('opsi_transaksi_perintah_produksi', array('kode_transaksi'=>$data['kode_perintah']))->result();
			foreach (@$get_opsi_perintah as $row_perintah) {
				if ($row_perintah->jumlah_sisa > 0) $cek_perintah_selesai = false;
			}
			if ($cek_perintah_selesai) {
				$status_perintah['status']='selesai';
				$this->db->update('transaksi_perintah_produksi',$status_perintah,array('kode_transaksi'=>$data['kode_perintah']));			
			}

			$produksi['kode_produksi']=$kode_produksi;
			$produksi['kode_perintah_produksi'] = $data['kode_perintah'];
			$produksi['tanggal_produksi']=date('Y-m-d');
			// if($jenis_produksi=='plasma'){
			// 	$produksi['kode_plasma']=$kode_karyawan;
			// 	$produksi['nama_plasma']=$nama_karyawan;
			// 	$produksi['kode_karyawan']='';
			// 	$produksi['nama_karyawan']='';
			// }else{
			// 	$produksi['kode_plasma']='';
			// 	$produksi['nama_plasma']='';
			// 	$produksi['kode_karyawan']=$kode_karyawan;
			// 	$produksi['nama_karyawan']=$nama_karyawan;
			// }

			$produksi['jumlah']=$jml_total_produksi;
			$produksi['afkir']=$jml_total_afkir;
			$produksi['jenis_produksi']=$data_temp->jenis_produksi;
			$produksi['id_petugas']=$id_petugas;
			$produksi['petugas']=$nama_petugas;
			$produksi['petugas']=$nama_petugas;
			$this->db->insert('transaksi_produksi',$produksi);

			echo"1|<div class='alert alert-success'>Data Berhasil Disimpan</div>";
		} else {
			echo"<div class='alert alert-danger'>Periksa Data Anda</div>";
		}





	}
	public function get_total(){
		$data = $this->input->post();
		$grand_total = @$data['total'] - @$data['angsuran'];
		$hasil = array("rupiah"=>format_rupiah($grand_total),'nilai'=>$grand_total);

		$update_piutang_temp['piutang_plasma']=$data['piutang'];
		$update_piutang_temp['angsuran']=$data['angsuran'];
		$update_piutang_temp['grand_total_plasma']=$grand_total;
		$this->db->update('opsi_transaksi_produksi_temp',$update_piutang_temp,array('kode_karyawan' =>$data['kode_plasma'], 'kode_produksi'=>$data['kode_produksi']));
		echo json_encode($hasil);
	}

	public function angsuran_piutang(){
		$kode_produksi=$this->input->post('kode_produksi');
		$jenis_produksi=$this->input->post('jenis_produksi');
		if($jenis_produksi=="plasma"){
			$this->db->group_by('kode_karyawan');
			$ambil_temp = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi,'jenis_produksi'=>$jenis_produksi));
			$hasil_ambil_temp = $ambil_temp->result();
			foreach ($hasil_ambil_temp as $data_temp) {
				if(!empty($data_temp->angsuran) and $data_temp->angsuran !=0){
					$ambil_piutang = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$data_temp->kode_karyawan,'status'=>'plasma'));
					$hasil_ambil_piutang = $ambil_piutang->row();

					$update_piutang['sisa']=@$hasil_ambil_piutang->sisa - $data_temp->angsuran;
					$update_piutang['angsuran']=@$hasil_ambil_piutang->angsuran + $data_temp->angsuran;
			//$update_piutang['grand_total_plasma']=$grand_total;
					$this->db->update('transaksi_piutang',$update_piutang,array('kode_customer'=>$data_temp->kode_karyawan,'status'=>'plasma'));

					$opsi_piutang['kode_transaksi']=$kode_produksi;
					$opsi_piutang['jenis_transaksi']='produksi';
					$opsi_piutang['kode_customer']=$data_temp->kode_karyawan;
					$opsi_piutang['nama_customer']=$data_temp->nama_karyawan;
					$opsi_piutang['angsuran']=$data_temp->angsuran;
					$opsi_piutang['tanggal_angsuran']=date('Y-m-d');
					$opsi_piutang['status']='plasma';
					$this->db->insert('opsi_piutang',$opsi_piutang);
			//echo $this->db->last_query();

					$get_kategori_akun=$this->db->get_where('keuangan_kategori_akun',array('nama_kategori_akun'=>'Pembayaran Plasma'));
					$hasil_get_kategori_akun=$get_kategori_akun->row();
					$keungan_keluar['kode_jenis_keuangan']=  $hasil_get_kategori_akun->kode_jenis_akun;
					$keungan_keluar['nama_jenis_keuangan']=$hasil_get_kategori_akun->nama_jenis_akun;
					$keungan_keluar['kode_kategori_keuangan']=$hasil_get_kategori_akun->kode_kategori_akun;
					$keungan_keluar['nama_kategori_keuangan']=$hasil_get_kategori_akun->nama_kategori_akun;
					$keungan_keluar['kode_sub_kategori_keuangan']='';
					$keungan_keluar['nama_sub_kategori_keuangan']=$hasil_get_kategori_akun->nama_kategori_akun;

					$keungan_keluar['nominal']=$data_temp->grand_total_plasma;
					$keungan_keluar['keterangan']='';
					$keungan_keluar['tanggal_transaksi']=date('Y-m-d');
					$keungan_keluar['kode_referensi']='';

					$user=$this->session->userdata('astrosession');

					$keungan_keluar['id_petugas']=$user->id;
					$keungan_keluar['petugas']=$user->uname;
					$this->db->insert('keuangan_keluar',$keungan_keluar);
			//echo $this->db->last_query();
				}else{
			// 	$ambil_piutang = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$data_temp->kode_karyawan,'status'=>'plasma'));
			// 	$hasil_ambil_piutang = $ambil_piutang->row();

			// 	$update_piutang['sisa']=@$hasil_ambil_piutang->sisa - $data_temp->angsuran;
			// 	$update_piutang['angsuran']=@$hasil_ambil_piutang->angsuran + $data_temp->angsuran;
			// //$update_piutang['grand_total_plasma']=$grand_total;
			// 	$this->db->update('transaksi_piutang',$update_piutang,array('kode_customer'=>$data_temp->kode_karyawan,'status'=>'plasma'));

			// 	$opsi_piutang['kode_transaksi']=$kode_produksi;
			// 	$opsi_piutang['angsuran']=$data_temp->angsuran;
			// 	$opsi_piutang['tanggal_angsuran']=date('Y-m-d');
			// 	$this->db->insert('opsi_piutang',$opsi_piutang);
			// //echo $this->db->last_query();

					$pembelian = $this->db->get_where('opsi_transaksi_produksi_temp',array('kode_produksi'=>$kode_produksi,'status'=>'','kode_karyawan'=>$data_temp->kode_karyawan));

					$list_pembelian = $pembelian->result();

					$grand_total = 0;
					foreach($list_pembelian as $daftar){
						if($daftar->kategori_bahan=="Bahan Setengah Jadi"){
							@$satuan_bahan = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>@$daftar->kode_bahan));
							@$hasil_satuan_bahan = $satuan_bahan->row();
						}else if($daftar->kategori_bahan=="Bahan Jadi"){
							@$satuan_bahan = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>@$daftar->kode_bahan));
							@$hasil_satuan_bahan = $satuan_bahan->row();
						}else{
							@$satuan_bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>@$daftar->kode_bahan));
							@$hasil_satuan_bahan = $satuan_bahan->row();
						}
						@$harga_bahan = $this->db->get_where('opsi_bahan_plasma',array('kode_bahan'=>@$daftar->kode_bahan,'kode_plasma'=>@$daftar->kode_karyawan));
						@$hasil_harga_bahan = $harga_bahan->row();

						$grand_total += @$hasil_harga_bahan->harga * @$daftar->jumlah;
					}

					$get_kategori_akun=$this->db->get_where('keuangan_kategori_akun',array('nama_kategori_akun'=>'Pembayaran Plasma'));
					$hasil_get_kategori_akun=$get_kategori_akun->row();
					$keungan_keluar['kode_jenis_keuangan']=  $hasil_get_kategori_akun->kode_jenis_akun;
					$keungan_keluar['nama_jenis_keuangan']=$hasil_get_kategori_akun->nama_jenis_akun;
					$keungan_keluar['kode_kategori_keuangan']=$hasil_get_kategori_akun->kode_kategori_akun;
					$keungan_keluar['nama_kategori_keuangan']=$hasil_get_kategori_akun->nama_kategori_akun;
					$keungan_keluar['kode_sub_kategori_keuangan']='';
					$keungan_keluar['nama_sub_kategori_keuangan']=$hasil_get_kategori_akun->nama_kategori_akun;

					$keungan_keluar['nominal']=$grand_total;
					$keungan_keluar['keterangan']='';
					$keungan_keluar['tanggal_transaksi']=date('Y-m-d');
					$keungan_keluar['kode_referensi']='';

					$user=$this->session->userdata('astrosession');

					$keungan_keluar['id_petugas']=$user->id;
					$keungan_keluar['petugas']=$user->uname;
					$this->db->insert('keuangan_keluar',$keungan_keluar);
				}
			}
		}
	}
}