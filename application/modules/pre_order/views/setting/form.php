<div class="row">      
  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah PO
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">                   
          <div class="" ></div>
          <section class="content">             
            <!-- Main row -->


            <div class="row">
              <!-- Left col -->
              <section class="col-lg-12 connectedSortable">



                <div class="box box-info">
                  <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools"></div><!-- /. tools -->
                  </div>

                  <form id="data_form" action="" method="post">

                    <div class="box-body">
                      <div class="sukses" ></div>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Kode Transaksi</label>
                            <?php
                            $this->db->select_max('id');
                            $get_max_po = $this->db->get('transaksi_po');
                            $max_po = $get_max_po->row();

                            $user = $this->session->userdata('astrosession');
                            $id_user=$user->id;

                            $this->db->where('id', $max_po->id);
                            $get_po = $this->db->get('transaksi_po');
                            $po = $get_po->row();
                            $tahun = substr(@$po->kode_transaksi, 3,4);
                            if(date('Y')==$tahun){
                              $nomor = substr(@$po->kode_transaksi, 11);

                              $nomor = $nomor + 1;
                              $string = strlen($nomor);
                              if($string == 1){
                                $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                              } else if($string == 2){
                                $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                              } else if($string == 3){
                                $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_000'.$nomor;
                              } else if($string == 4){
                                $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_00'.$nomor;
                              } else if($string == 5){
                                $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_0'.$nomor;
                              } else if($string == 6){
                                $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_'.$nomor;
                              }
                            } else {
                              $kode_trans = 'PO_'.date('Y').'_'.$id_user.'_000001';
                            }

                            ?>
                            <input readonly="true" type="text" value="<?php echo $kode_trans; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_po" id="kode_po" />
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="gedhi">Tanggal Transaksi</label>
                            <input type="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_input" id="tanggal_input"/>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label>Suplier</label>
                          <select class="form-control select2" name="supplier" id="kode_supplier">
                            <option value="">Pilih Suplier</option>
                            <?php
                            $this->db->order_by('nama_supplier','asc');
                            $this->db->where('status_supplier', '1');
                            $get_suplier=$this->db->get('master_supplier');
                            $hasil_supplier=$get_suplier->result();
                            foreach ($hasil_supplier as $list) {
                              ?>
                              <option value="<?php echo $list->kode_supplier; ?>"><?php echo $list->nama_supplier; ?></option>
                              <?php 
                            } ?>
                          </select>
                        </div>
                        <?php
                        $get_ses = $this->session->userdata('astrosession');
                        ?>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="gedhi">Operator</label>
                            <input type="text" readonly="" value="<?php echo $get_ses->uname; ?>" class="form-control" placeholder="Tanggal Transaksi" name="operator" id="operator"/>
                          </div>
                        </div>

                        <div class="col-md-1" style="padding-top:26px; padding-left:0px;" >
                          <div onclick="add_supplier()" id="add_supplier"  class="btn btn-primary btn-block">Simpan</div>
                          <div onclick="hapus_supplier()" id="hapus_supplier" class="btn btn-danger btn-block">Batal</div>
                        </div>
                      </div>
                    </div> 


                    <div class="box-body list_item">
                      <div class="row">
                        <div class="">

                          <div class="col-md-2">
                            <label>Nama Produk</label>
                            <select id="kode_bahan" onchange="get_satuan()" name="kode_bahan" class="form-control select2" >
                              <option value="">Pilih Produk</option>
                            </select>
                          </div>

                          <div class="col-md-2">
                            <label>QTY</label>
                            <input type="text" class="form-control" placeholder="QTY" name="jumlah" id="jumlah" onkeyup="cek_jumlah()" />
                          </div>
                          <div class="col-md-2">
                            <label>Satuan</label>
                            <input type="text" readonly="true" class="form-control" placeholder="Satuan Pembelian" name="nama_satuan" id="nama_satuan" />
                            <input type="hidden" name="kode_satuan" id="kode_satuan" />
                          </div>
                          <div class="col-md-3">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="KET" name="keterangan" id="keterangan" />
                            <input type="hidden" name="id_item" id="id_item" />
                            <?php
                            $query=$this->db->query("SELECT kode_unit from setting_gudang");
                            $kode_unit=$query->row();


                            ?>
                            <input type="hidden" id="nama_bahan" name="nama_bahan" />

                            <input type="hidden" name="kode_unit" id="kode_unit" value="<?php echo $kode_unit->kode_unit;?>" />
                          </div>



                          <div class="col-md-1" style="padding-top:26px; padding-left:0px;" >
                            <div onclick="add_item()" id="add"  class="btn btn-primary btn-block">Add</div>
                            <div onclick="update_item()" id="update" class="btn btn-primary btn-block">Edit</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div id="list_transaksi_pembelian" class="list_item">
                      <div class="box-body"><br>
                        <table id="tabel_daftar" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Nama bahan</th>
                              <th>QTY</th>
                              <th>Keterangan</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody id="tabel_temp_data_transaksi">

                          </tbody>
                          <tfoot>

                          </tfoot>
                        </table>
                      </div>
                    </div>



                    <br>
                    <!-- <div class="box-footer"> <button type="submit" class="btn btn-success pull-right">Simpan</button> -->
                    </div>
                  </form>
                  <center><a onclick="confirm_bayar();" class="btn btn-success btn-md list_item" style="width:200px;"><i class="fa fa-save"></i> Simpan</a></center>




                </div>        
              </div>  



            </div>
          </section><!-- /.Left col -->      
        </div><!-- /.row (main row) -->
        <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color:grey">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
              </div>
              <div class="modal-body">
                <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus produk tersebut ?</span>
                <input id="id-delete" type="hidden">
              </div>
              <div class="modal-footer" style="background-color:#eee">
                <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
                <button onclick="delData()"  class="btn green">Ya</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal-simpan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color:grey">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="color:#fff;">Konfirmasi Simpan Data</h4>
              </div>
              <div class="modal-body">
                <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menyimpan data tersebut ?</span>
                <input id="id-delete" type="hidden">
              </div>
              <div class="modal-footer" style="background-color:#eee">
                <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
                <button onclick="simpan_besar()"  class="btn green">Ya</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal-confirm-bayar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color:grey">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="color:#fff;">Konfirmasi PO</h4>
              </div>
              <div class="modal-body">
                <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan PO ini <span id="bayare"></span> ?</span>
                <input id="id-delete" type="hidden">
              </div>
              <div class="modal-footer" style="background-color:#eee">
                <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
                <button id="ok" onclick="simpan_po()" class="btn green">Ya</button>
              </div>
            </div>
          </div>
        </div>
        <div id="modal-confirm-supplier" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color:grey">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" style="color:#fff;">Konfirmasi Supplier</h4>
              </div>
              <div class="modal-body">
                <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data tersebut ?</span>
                <input id="id-delete" type="hidden">
              </div>
              <div class="modal-footer" style="background-color:#eee">
                <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
                <button id="ok" onclick="delDataSupplier()" class="btn green">Ya</button>
              </div>
            </div>
          </div>
        </div>
      </section><!-- /.content -->
      <div class="box-footer clearfix">


      </div>


    </div>

  </div>
</div>
</div>


<!------------------------------------------------------------------------------------------------------>

<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'pre_order/daftar'; ?>";
  });

  $(document).ready(function() {
    $(".select2").select2();
  });
</script>


<script>
  function setting() {
    $('#modal_setting').modal('show');
  }
  function confirm_bayar(){
    $("#modal-confirm-bayar").modal('show');
  }


  $(document).ready(function(){
    $("#update").hide();
    $(".list_item").hide();
    $("#hapus_supplier").hide();
    get_bahan() ;
    var kode_po = $('#kode_po').val() ;  

    $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pre_order/get_po/'; ?>"+kode_po);



    $(".tgl").datepicker();
    $(".select2").select2();

    $('#ok').on('click',function(){


    });
    $('#nomor_nota').on('change',function(){
      var nomor_nota = $('#nomor_nota').val();
      var url = "<?php echo base_url() . 'pre_order/get_kode_nota' ?>";
      $.ajax({
        type: 'POST',
        url: url,
        data: {nomor_nota:nomor_nota},
        success: function(msg){
          if(msg == 1){
            $(".notif_nota").html('<div class="alert alert-warning">Kode_Telah_dipakai</div>');
            setTimeout(function(){
              $('.notif_nota').html('');
            },1700);              
            $('#nomor_nota').val('');
          }
          else{

          }
        }
      });
    });


    var jenis_bahan = $("#kategori_bahan").val();

    var url = "<?php echo base_url().'pre_order/get_bahan'; ?>";
    $.ajax({
      type: "POST",
      url: url,
      data: {jenis_bahan:jenis_bahan},
      success: function(pilihan) {              
        $("#kode_bahan").html(pilihan);
      }
    });


    $('#kode_bahan').on('change',function(){


    });

    $("#data_form").submit(function(){
      var simpan_transaksi = "<?php echo base_url().'pre_order/simpan_transaksi/'?>";
      $.ajax({
        type: "POST",
        url: simpan_transaksi,
        data: $('#data_form').serialize(),
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success: function(msg)
        {
          $(".tunggu").hide();
          var data = msg.split("|");
          var num = data[0];
          var pesan = data[1];

          if(num == 0){  
            $(".sukses").html(pesan);   
            setTimeout(function(){$('.sukses').html('');
              window.location = "<?php echo base_url() . 'pre_order/daftar' ?>";
            },1500);   

          }
          else{
            $(".sukses").html(pesan);   
            setTimeout(function(){$('.sukses').html('');
          },1500); 
          }     
        }
      });
      return false;

    });

  });
  function simpan_po() {
    var simpan_transaksi = "<?php echo base_url().'pre_order/simpan_transaksi/'?>";
    $.ajax({
      type: "POST",
      url: simpan_transaksi,
      data: $('#data_form').serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {

        $(".tunggu").hide();
        var data = msg.split("|");
        var num = data[0];
        var pesan = data[1];

        if(num == 0){  
          $(".sukses").html(pesan);   
          setTimeout(function(){$('.sukses').html('');
            window.location = "<?php echo base_url() . 'pre_order/daftar' ?>";
          },1500);   

        }
        else{
          $(".sukses").html(pesan);   
          setTimeout(function(){$('.sukses').html('');
            window.location = "<?php echo base_url() . 'pre_order/daftar' ?>";
          },1500); 
        }  

        $("#modal-confirm-bayar").modal('hide');   
      }
    });
    return false;
  }
  function get_bahan() {
    var jenis_bahan = $("#kategori_bahan").val();

    var url = "<?php echo base_url().'pre_order/get_bahan'; ?>";
    $.ajax({
      type: "POST",
      url: url,
      data: {jenis_bahan:jenis_bahan},
      success: function(pilihan) {              
        $("#kode_bahan").html(pilihan);
      }
    });
  }
  function add_supplier() {
    var kode_supplier = $("#kode_supplier").val();
    var tanggal_input = $("#tanggal_input").val();
    if(kode_supplier=='' || tanggal_input==''){
      alert('Pilih Tanggal Dan  Supplier');
    }else{
      $('#modal-simpan').modal('show');
     /* $("#kode_supplier").attr('readonly',true);
      $("#tanggal_input").attr('readonly',true);
      $(".list_item").show();
      $("#hapus_supplier").show();
      $("#add_supplier").hide();*/
    }

  }
  function simpan_besar(){
    $('#modal-simpan').modal('hide');
      $("#kode_supplier").attr('readonly',true);
      $("#tanggal_input").attr('readonly',true);
      $(".list_item").show();
      $("#hapus_supplier").show();
      $("#add_supplier").hide();
  }
  function hapus_supplier() {
   $("#modal-confirm-supplier").modal('show');
 }
 function delDataSupplier() {
  $("#kode_supplier").attr('readonly',false);
  $("#tanggal_input").attr('readonly',false);
  $(".list_item").hide();
  $("#hapus_supplier").hide();
  $("#add_supplier").show();

  var kode_po = $('#kode_po').val();
  var url = '<?php echo base_url().'pre_order/hapus_bahan_supplier'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      kode_po:kode_po
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();  
      $("#modal-confirm-supplier").modal('hide');
      $(".tunggu").hide(); 
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pre_order/get_po/'; ?>"+kode_po);
    }
  });
  return false;
}
function cek_jumlah() {
  var jumlah = $("#jumlah").val();

  if(parseInt(jumlah) < 0){
    alert('Jumlah Pembelian Salah');
    $("#jumlah").val('');
  }

}
function get_satuan() {
  var jenis_bahan = $('#kategori_bahan').val();
  var kode_bahan = $('#kode_bahan').val();
  var url = "<?php echo base_url() . 'pre_order/get_satuan' ?>";
  $.ajax({
    type: 'POST',
    url: url,
    dataType:'json',
    data: {kode_bahan:kode_bahan,jenis_bahan:jenis_bahan},
    success: function(msg){
      if(msg.satuan_pembelian){
        $('#nama_satuan').val(msg.satuan_pembelian);
      }else if(msg.satuan_stok){
        $('#nama_satuan').val(msg.satuan_stok);
      }
      if(msg.id_satuan_pembelian){
        $("#kode_satuan").val(msg.id_satuan_pembelian);
      }else if(msg.kode_satuan_stok){
        $("#kode_satuan").val(msg.kode_satuan_stok);
      }
      if(msg.nama_bahan_baku){
        $("#nama_bahan").val(msg.nama_bahan_baku);
      }else if(msg.nama_barang){
        $("#nama_bahan").val(msg.nama_barang);
      }

    }
  });
}

function add_item(){
  var kode_po = $('#kode_po').val();
  var kategori_bahan = $('#kategori_bahan').val();
  var kode_bahan = $('#kode_bahan').val();
  var jumlah = $('#jumlah').val();
  var nama_bahan = $("#nama_bahan").val();
  var keterangan = $("#keterangan").val();
  var position = $("#kode_unit").val();
  var url = "<?php echo base_url().'pre_order/simpan_item_temp/'?> ";
  if(kode_bahan=='' || jumlah==''){
    alert('Silahkan Lengkapi Form !');
  }else{
    $.ajax({
      type: "POST",
      url: url,
      data: { kode_po:kode_po,
        kategori_bahan:kategori_bahan,
        kode_bahan:kode_bahan,
        nama_bahan:nama_bahan,
        keterangan:keterangan,
        jumlah:jumlah,position:position    
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(data)
      {
        $(".tunggu").hide(); 
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pre_order/get_po/'; ?>"+kode_po);

        $('#kode_bahan').val('');
        $('#jumlah').val('');
        $('#nama_satuan').val('');      
        $("#keterangan").val('');             
      }
    });
  }
}

function actDelete(Object) {
  $('#id-delete').val(Object);
  $('#modal-confirm').modal('show');
}

function actEdit(id) {
  var id = id;
  var kode_po = $('#kode_po').val();
  var url = "<?php echo base_url().'pre_order/get_temp_po'; ?>";
  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: {id:id},
    success: function(pembelian){
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url().'pre_order/get_nama_satuan'; ?>",
        data: {kode_bahan:pembelian.kode_bahan},
        success: function(data){
          $('#nama_satuan').val(data);
        }
      });

      $('#kategori_bahan').val(pembelian.kategori_bahan);

      $("#kode_bahan").select2().select2('val', pembelian.kode_bahan);

      $('#jumlah').val(pembelian.jumlah);
      $("#id_item").val(pembelian.id);
      $("#keterangan").val(pembelian.keterangan);
      $("#add").hide();
      $("#update").show();
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pre_order/get_po/'; ?>"+kode_po);
    }
  });
}

function update_item(){
  var kode_po = $('#kode_po').val();
  var kategori_bahan = $('#kategori_bahan').val();
  var kode_bahan = $('#kode_bahan').val();
  var jumlah = $('#jumlah').val();
  var nama_bahan = $("#nama_bahan").val();
  var keterangan = $("#keterangan").val();
  var id_item = $("#id_item").val();
  var url = "<?php echo base_url().'pre_order/update_item_temp/'?> ";

  $.ajax({
    type: "POST",
    url: url,
    data: { kode_po:kode_po,
      kategori_bahan:kategori_bahan,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      keterangan:keterangan,
      jumlah:jumlah,
      id:id_item
    },
    success: function(data)
    {
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pre_order/get_po/'; ?>"+kode_po);

      $("#kode_bahan").select2().select2('val','');
      $('#jumlah').val('');
      $("#nama_bahan").val('');
      $("#keterangan").val('');
      $("#nama_satuan").val('');
      $("#id_item").val('');
      $("#add").show();
      $("#update").hide();
    }
  });
}

function delData() {
  var id = $('#id-delete').val();
  var kode_po = $('#kode_po').val();
  var url = '<?php echo base_url().'pre_order/hapus_bahan_temp'; ?>/delete';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id:id
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'pre_order/get_po/'; ?>"+kode_po);

    }
  });
  return false;
}
</script>

