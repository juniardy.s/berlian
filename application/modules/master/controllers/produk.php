<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class produk extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['aktif'] = 'produk';
		$data['konten'] = $this->load->view('produk/daftar', $data, TRUE);
		$data['halaman'] = $this->load->view('produk/menu', $data, TRUE);
		$this->load->view('produk/main', $data);		
	}


	public function daftar()
	{
		$data['aktif'] = 'produk';
		$data['konten'] = $this->load->view('produk/daftar', $data, TRUE);
		$data['halaman'] = $this->load->view('produk/menu', $data, TRUE);
		$this->load->view('produk/main', $data);		
	}

	public function tambah()
	{
		$data['aktif'] = 'produk';
		$data['konten'] = $this->load->view('produk/form', $data, TRUE);
		$data['halaman'] = $this->load->view('produk/menu', $data, TRUE);
		$this->load->view('produk/main', $data);			
	}

	public function ubah()
	{
		$data['aktif'] = 'produk';
		$data['konten'] = $this->load->view('produk/form', $data, TRUE);
		$data['halaman'] = $this->load->view('produk/menu', $data, TRUE);
		$this->load->view('produk/main', $data);			
	}

	public function detail()
	{
		$data['aktif'] = 'produk';
		$data['konten'] = $this->load->view('produk/detail', $data, TRUE);
		$data['halaman'] = $this->load->view('produk/menu', $data, TRUE);
		$this->load->view('produk/main', $data);				
	}


	public function hapus(){
		$kode = $this->input->post("key");
		$this->db->delete( 'master_produk', array('id' => $kode) );
		echo '<div class="alert alert-success">Sudah dihapus.</div>';            

	}

	public function get_temp_pembelian(){
        $id = $this->input->post('id');
        $pembelian = $this->db->get_where('opsi_transaksi_pembelian_temp',array('id'=>$id));
        $hasil_pembelian = $pembelian->row();
        echo json_encode($hasil_pembelian);
    }
    public function get_satuan_stok(){
        $param = $this->input->post();
        $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$param['id_pembelian']));
        $hasil_satuan_stok = $satuan_stok->row();
        $dft_satuan = $this->db->get_where('master_satuan');
        $hasil_dft_satuan = $dft_satuan->result();
        #$desa = $desa->result();
        $list = '';
        foreach($hasil_dft_satuan as $daftar){
          $list .= 
          "
          <option value='$daftar->kode'>$daftar->nama</option>
          ";
        }
        $opt = "<option selected='true' value=''>Pilih Satuan Stok</option>";
        echo $opt.$list;
    }

	public function simpan_tambah()

		{
			$data = $this->input->post(NULL, TRUE);
            $unit = $this->db->get_where('master_unit',array('kode_unit'=>$data['position']));
            $hasil_unit = $unit->row();
            $rak = $this->db->get_where('master_rak',array('kode_rak'=>$data['kode_rak']));
            $hasil_rak = $rak->row();
            $satuan_pembelian = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_pembelian']));
            $hasil_satuan_pembelian = $satuan_pembelian->row();
            $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_stok']));
           
            $hasil_satuan_stok = $satuan_stok->row();
            $data['kode_produk'] = $data['kode_produk'];
            $data['kode_satuan_stok'] = $hasil_satuan_stok->kode;
            $data['nama_satuan_stok'] = $hasil_satuan_stok->nama;
            
            $data['nama_rak'] = $hasil_rak->nama_rak;
            $data['real_stok'] = 0;
            unset($data['id_satuan_stok']);
            $this->db->insert("master_produk", $data);
            echo 'sukses';            
		
	}

	public function simpan_ubah()

	{

		$data = $this->input->post(NULL, TRUE);
            $unit = $this->db->get_where('master_unit',array('kode_unit'=>$data['position']));
            $hasil_unit = $unit->row();
            $rak = $this->db->get_where('master_rak',array('kode_rak'=>$data['kode_rak']));
            $hasil_rak = $rak->row();
            $satuan_pembelian = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_pembelian']));
            $hasil_satuan_pembelian = $satuan_pembelian->row();
            $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_stok']));
           
            $hasil_satuan_stok = $satuan_stok->row();
            $data['kode_produk'] = $data['kode_produk'];
             $data['kode_satuan_stok'] = $hasil_satuan_stok->kode;
            $data['nama_satuan_stok'] = $hasil_satuan_stok->nama;
            
            unset($data['id_satuan_stok']);
            $data['nama_rak'] = $hasil_rak->nama_rak;
           $update = $this->db->update("master_produk", $data, array('kode_produk' => $data['kode_produk']));
            echo 'sukses';            

	}
	public function get_kode()
    {
        $kode_produk = $this->input->post('kode_produk');
        $query = $this->db->get_where('master_produk',array('kode_produk' => $kode_produk))->num_rows();

        if($query > 0){
            echo "1";
        }
        else{
            echo "0";
        }
    }
     public function get_rupiah()
    {
        $hpp = $this->input->post('hpp');
        echo format_rupiah($hpp);
    }
    
    public function get_table()
    {
        $kode_default = $this->db->get('setting_gudang');
        $hasil_unit =$kode_default->row();
        $param =$hasil_unit->kode_unit;
        $start = (50*$this->input->post('page'));
        
        $data = $this->input->post();
        $this->db->limit(50, $start);
        
    if(@$data['nama_produk']){
        $produk = $data['nama_produk'];
        $this->db->like('nama_produk',$produk,'both');
    }
        $get_bb = $this->db->get_where("master_produk", array('kode_unit' => $param));
        $hasil_bb = $get_bb->result();
        $nomor = $start+1;
        foreach ($hasil_bb as $daftar) {
            if($this->session->flashdata('message')==$daftar->kode_produk){

                  echo '<tr id="warna" style="background: #88cc99; display: none;">';
                }
                else{
                  echo '<tr>';
                }
            ?>   
            
                <td><?php echo $no;?></td>
                <td><?php echo $daftar->kode_produk; ?></td>                  
                <td><?php echo $daftar->nama_produk; ?></td>
                <td><?php echo format_rupiah($daftar->hpp); ?></td>
                <td><?php echo format_rupiah($daftar->harga_jual); ?></td>                  
                <td><?php echo cek_status($daftar->status); ?></td>
                <td align="center"><?php echo get_detail_edit_delete_string($daftar->id); ?></td>
                </tr>

          <?php 
          $nomor++;
      }
  }
  
  public function get_produk(){
        $this->load->view('produk/cari_produk');
    }        
}
