<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plasma extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('plasma/daftar_plasma', NULL, TRUE);
        $data['halaman'] = $this->load->view('plasma/menu', $data, TRUE);
        $this->load->view('plasma/main', $data);  

    }	



    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }

    public function tambah()
    {
       $data['aktif'] = 'master';
       $data['konten'] = $this->load->view('plasma/tambah_plasma', NULL, TRUE);
       $data['halaman'] = $this->load->view('plasma/menu', $data, TRUE);
       $this->load->view('plasma/main', $data);  


   }

   public function detail()
   {
    $data['aktif'] = 'master';
    $data['konten'] = $this->load->view('plasma/detail_plasma', NULL, TRUE);
    $data['halaman'] = $this->load->view('plasma/menu', $data, TRUE);
    $this->load->view('plasma/main', $data);  
}
public function print_daftar()
{
    
    $this->load->view('plasma/print_daftar'); 
}
public function get_kode()
{
    $kode_plasma = $this->input->post('kode_plasma');
    $query = $this->db->get_where('master_plasma',array('kode_plasma' => $kode_plasma))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}




public function simpan_tambah_plasma()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_plasma', 'kode_plasma', 'required');
    $this->form_validation->set_rules('nama_plasma', 'nama_plasma', 'required');
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    $this->form_validation->set_rules('telp', 'telp', 'required');
    $this->form_validation->set_rules('status', 'status', 'required'); 


    if ($this->form_validation->run() == FALSE) {
        echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
    } 

    else {
        $data = $this->input->post(NULL, TRUE);
        $kode_plasma=$data['kode_plasma'];
        $get_bahan_temp=$this->db->get_where('opsi_bahan_plasma_temp',array('kode_plasma'=>$kode_plasma));
        $hasil_get_bahan_temp=$get_bahan_temp->result();
        foreach ($hasil_get_bahan_temp as $value) {
            $data_opsi['kode_plasma']=$value->kode_plasma;
            $data_opsi['kode_bahan']=$value->kode_bahan;
            $data_opsi['nama_bahan']=$value->nama_bahan;
            $data_opsi['jenis_bahan']=$value->jenis_bahan;
            $data_opsi['id_satuan']=$value->id_satuan;
            $data_opsi['nama_satuan']=$value->nama_satuan;
            $data_opsi['hpp']=$value->hpp;
            $data_opsi['harga']=$value->harga;

            $insert = $this->db->insert("opsi_bahan_plasma",$data_opsi);
             //echo $this->db->last_query();
        }
        unset($data['jenis_bahan']);
        unset($data['bahan']);
        unset($data['harga']);
        unset($data['id_satuan']);
        unset($data['nama_satuan']);

        $insert = $this->db->insert("master_plasma",$data);
        //echo $this->db->last_query();
        $this->db->delete('opsi_bahan_plasma_temp', array('kode_plasma' => $kode_plasma));
        echo '<div class="alert alert-success">Sudah tersimpan.</div>'; 

    }
}



public function simpan_edit_plasma()
{
  $this->load->library('form_validation');
  $this->form_validation->set_rules('kode_plasma', 'kode_plasma', 'required');
  $this->form_validation->set_rules('nama_plasma', 'nama_plasma', 'required');
  $this->form_validation->set_rules('alamat', 'alamat', 'required');
  $this->form_validation->set_rules('telp', 'telp', 'required');
  $this->form_validation->set_rules('status', 'status', 'required'); 
                //jika form validasi berjalan salah maka tampilkan GAGAL
  if ($this->form_validation->run() == FALSE) {
    echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
} 
                    //jika form validasi berjalan benar maka inputkan data
else {
    $data = $this->input->post(NULL, TRUE);
    unset($data['jenis_bahan']);
    unset($data['bahan']);
    unset($data['harga']);
    unset($data['id_satuan']);
    unset($data['nama_satuan']);

    $update = $this->db->update("master_plasma", $data, array('id' => $data['id']));
    echo '<div class="alert alert-success">Sudah Tersimpan.</div>';  
            //echo $this->db->last_query();  
}  
}




public function hapus(){
    $id = $this->input->post('id');
    $this->db->delete('master_plasma',array('kode_plasma'=>$id));

    $this->db->delete('opsi_bahan_plasma', array('kode_plasma' => $id));
}
public function tambah_temp()
{
    $input = $this->input->post();
    $get_bahan_temp=$this->db->get_where('opsi_bahan_plasma_temp',array('kode_bahan'=>$input['bahan_baku'],'kode_plasma'=>$input['kode_plasma']));
    $hasil_get_bahan_temp=$get_bahan_temp->row();
    echo count($hasil_get_bahan_temp);
    if (count($hasil_get_bahan_temp)=='1') {
        $jumlah_awal=$hasil_get_bahan_temp->jumlah;
        $data_update['jumlah']=$jumlah_awal + $input['jumlah'];

        $this->db->update("opsi_bahan_plasma_temp", $data_update, array('kode_bahan' => $input['bahan_baku'],'kode_plasma'=>$input['kode_plasma']));
            //echo $this->db->last_query();
    }else{
    // $data['kode_bahan_jadi'] = $input['kode_bahan_jadi'];
     $data['kode_plasma'] = $input['kode_plasma'];
     $data['kode_bahan'] = $input['bahan_baku'];
     $jenis = $this->input->post('jenis_bahan');

     if($jenis == 'bahan jadi'){
        $this->db->where('kode_bahan_jadi', $data['kode_bahan']);
        $get_bahan = $this->db->get('master_bahan_jadi');
        $hasil_bb = $get_bahan->row();
        $data['nama_bahan'] = $hasil_bb->nama_bahan_jadi;
    } else{
        $this->db->where('kode_bahan_setengah_jadi', $data['kode_bahan']);
        $get_bahan = $this->db->get('master_bahan_setengah_jadi');
        $hasil_bb = $get_bahan->row();
        $data['nama_bahan'] = $hasil_bb->nama_bahan_setengah_jadi;
    }
    $data['jenis_bahan'] = $input['jenis_bahan'];

    $data['id_satuan'] = $input['id_satuan'];
    $data['nama_satuan'] = $input['satuan'];
    $data['hpp'] = $input['hpp'];

    $data['harga'] = $input['harga'];

    $this->db->insert('opsi_bahan_plasma_temp', $data);
}

}
public function tambah_opsi()
{
    $input = $this->input->post();
    $get_bahan_temp=$this->db->get_where('opsi_bahan_plasma',array('kode_bahan'=>$input['bahan_baku'],'kode_plasma'=>$input['kode_plasma']));
    $hasil_get_bahan_temp=$get_bahan_temp->row();
    echo count($hasil_get_bahan_temp);
    if (count($hasil_get_bahan_temp)=='1') {
        $jumlah_awal=$hasil_get_bahan_temp->jumlah;
        $data_update['jumlah']=$jumlah_awal + $input['jumlah'];

        $this->db->update("opsi_bahan_plasma", $data_update, array('kode_bahan' => $input['bahan_baku'],'kode_plasma'=>$input['kode_plasma']));
            //echo $this->db->last_query();
    }else{
    // $data['kode_bahan_jadi'] = $input['kode_bahan_jadi'];
     $data['kode_plasma'] = $input['kode_plasma'];
     $data['kode_bahan'] = $input['bahan_baku'];
     $jenis = $this->input->post('jenis_bahan');

     if($jenis == 'bahan jadi'){
        $this->db->where('kode_bahan_jadi', $data['kode_bahan']);
        $get_bahan = $this->db->get('master_bahan_jadi');
        $hasil_bb = $get_bahan->row();
        $data['nama_bahan'] = $hasil_bb->nama_bahan_jadi;
    } else{
        $this->db->where('kode_bahan_setengah_jadi', $data['kode_bahan']);
        $get_bahan = $this->db->get('master_bahan_setengah_jadi');
        $hasil_bb = $get_bahan->row();
        $data['nama_bahan'] = $hasil_bb->nama_bahan_setengah_jadi;
    }
    $data['jenis_bahan'] = $input['jenis_bahan'];

    $data['id_satuan'] = $input['id_satuan'];
    $data['nama_satuan'] = $input['satuan'];
    $data['hpp'] = $input['hpp'];

    $data['harga'] = $input['harga'];

    $this->db->insert('opsi_bahan_plasma', $data);
}

}

public function get_table()
{
    $start = (50*$this->input->post('page'));
    
    $data = $this->input->post();
    $this->db->limit(50, $start);
    
    if(@$data['nama_plasma']){
        $produk = $data['nama_plasma'];
        $this->db->like('nama_plasma',$produk,'both');
    }
    $this->db->order_by('nama_plasma','asc');
    $plasma = $this->db->get('master_plasma');
    $hasil_plasma = $plasma->result();
    $nomor = $start+1;
    foreach ($hasil_plasma as $daftar) {
        ?>   
        <tr>
          <td><?php echo $nomor; ?></td>
          <td><?php echo $daftar->kode_plasma; ?></td>
          <td><?php echo $daftar->nama_plasma; ?></td>
          <td><?php echo $daftar->alamat; ?></td>
          <td><?php echo $daftar->telp; ?></td>
          <td><?php echo $daftar->keterangan; ?></td>
          <td><?php echo cek_status($daftar->status); ?></td>
          <td align="center"><?php echo get_detail_edit_delete($daftar->kode_plasma); ?></td>
      </tr>

      <?php 
      $nomor++;
  }
}

public function get_satuan()
{
    $bahan_baku = $this->input->post('kode_bahan_baku');
    $jenis = $this->input->post('jenis_bahan');

    if($jenis == 'bahan jadi'){
        $this->db->where('kode_bahan_jadi', $bahan_baku);
        $get_bahan = $this->db->get('master_bahan_jadi');
    } else{
        $this->db->where('kode_bahan_setengah_jadi', $bahan_baku);
        $get_bahan = $this->db->get('master_bahan_setengah_jadi');
    }
    $hasil_bb = $get_bahan->row();
    echo json_encode($hasil_bb);
}
public function get_bahan()
{
    $jenis = $this->input->post('jenis_bahan');
    if($jenis == 'bahan jadi'){
        $this->db->where('jenis_bahan','plasma');
        $get_bahan = $this->db->get('master_bahan_jadi');
    } else{
        $this->db->where('jenis_bahan','plasma');
        $get_bahan = $this->db->get('master_bahan_setengah_jadi');
    }
    $hasil_bahan = $get_bahan->result();
    echo '<option selected="true" value="">--Pilih Bahan--</option>';
    foreach ($hasil_bahan as $bahan ) {
        if($jenis == 'bahan jadi'){
            echo'<option value="'.$bahan->kode_bahan_jadi.'">'.$bahan->nama_bahan_jadi.'</option>';
        } else{
            echo'<option value="'.$bahan->kode_bahan_setengah_jadi.'">'.$bahan->nama_bahan_setengah_jadi.'</option>';
        }
    }

}
public function table_temp()
{
    $this->load->view('plasma/table_temp');        
}
public function table_opsi()
{
    $this->load->view('plasma/table_temp');        
}
public function delete_temp()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_bahan_plasma_temp', array('id' => $id));
}
public function delete_opsi()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_bahan_plasma', array('id' => $id));
}




}
