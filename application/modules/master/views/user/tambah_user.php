

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Tambah User
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>

          <?php
          $param = $this->uri->segment(4);
          if(!empty($param)){
            $bahan_baku = $this->db->get_where('master_bahan_baku',array('id'=>$param));
            $hasil_bahan_baku = $bahan_baku->row();
          }    

          ?>
          <div class="box-body">                   
            <div class="sukses" ></div>
            <form id="data_form"  method="post">
              <div class="box-body">            
                <div class="row">
                 
                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Karyawan</label>
                    <select id="kode_user" name="kode_user" style="width: 100%" class="form-control select2" required=""> 
                      <option value="">-- Pilih Karyawan --</option>
                      <?php 
                      $this->db->where('status ','1');
                      $karyawan = $this->db->get('master_karyawan');
                      $hasil_karyawan = $karyawan->result();
                      foreach ($hasil_karyawan as $value) {
                        ?>
                        <option value="<?php echo $value->kode_karyawan ;?>"><?php echo $value->nama_karyawan ;?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>

                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Username</label>
                    <input type="text" class="form-control" required="" id="uname" name="uname" required="">
                  </div>

                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Password</label>
                    <input type="password" class="form-control" required="" id="upass" name="upass"  required="">
                  </div>

                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Jabatan</label>
                    <select id="kode_jabatan" name="kode_jabatan" style="width: 100%" class="form-control " required=""> 
                      <option value="">-- Pilih Jabatan --</option>
                      <?php 
                      $this->db->where('kode_jabatan !=','001');
                      $ambil_data = $this->db->get('master_jabatan');
                      $hasil_ambil_data = $ambil_data->result();
                      foreach ($hasil_ambil_data as $value) {
                        ?>
                        <option value="<?php echo $value->kode_jabatan ;?>"><?php echo $value->nama_jabatan ;?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Status</label>
                    <select class="form-control" id="status" required="" name="status" required="">
                      <option selected value="" >Pilih</option>
                      <option  value="1">Aktif</option>
                      <option  value="0">Tidak Aktif</option>
                    </select>
                  </div>
                </div>

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- /.row (main row) -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'master/user/'; ?>";
    });
  </script>

  <script type="text/javascript">

    $("#data_form").submit( function() {  
      kode_user = $('#kode_user').val(); 
      nama_user = $("#kode_user option:selected").text();
      uname = $('#uname').val();   
      upass = $('#upass').val(); 
      kode_jabatan = $('#kode_jabatan').val();   
      status = $('#status').val(); 

      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url() . 'master/user/simpan' ?>",  
        cache :false,  
        data :{kode_user:kode_user,nama_user:nama_user,uname:uname,upass:upass,kode_jabatan:kode_jabatan,status:status},
        dataType: 'Json',
        beforeSend:function(){
          $(".tunggu").show();   
        },
        success : function(data) { 
          if (data.response == 'sukses') {
            $(".tunggu").hide();   
            $(".alert_berhasil").show();   
           window.location="<?php echo base_url('master/user/');?>";
          }else{
            alert('Gagal Menyimpan data');
            setInterval(function(){ location.reload() }, 2000);
          }
        },  
        error : function() {
          alert("Data gagal dimasukkan.");  
        }  
      });
      return false;   

    });   
  </script>