

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <style type="text/css">
  .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Detail User
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>

          <?php
          $param = $this->uri->segment(4);
          if(!empty($param)){
            $bahan_baku = $this->db->get_where('master_bahan_baku',array('id'=>$param));
            $hasil_bahan_baku = $bahan_baku->row();
          }    

          ?>
          <div class="box-body">                   
            <div class="sukses" ></div>
            <form id="data_form"  method="post">
              <div class="box-body">            
                <div class="row">
                  <?php 
                  $kode_user=$this->uri->segment(4);
                  $this->db->where('kode_user',$kode_user);
                  $get_user = $this->db->get('master_user')->row();
                  ?>

                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Nama</label>
                    <input type="text" readonly="" value="<?php echo $get_user->nama_user?>" class="form-control" name="">
                  </div>


                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Username</label>
                    <input type="text" readonly="" value="<?php echo $get_user->uname?>" class="form-control"  name="">
                  </div>

                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Password</label>
                    <input type="password" class="form-control" readonly="" value="<?php echo paramDecrypt($get_user->upass); ?>" id="upass" name="upass">
                  </div>

                  <div class="form-group  col-xs-5">
                    <label class="gedhi">Jabatan</label>
                    <select id="kode_jabatan" name="kode_jabatan" disabled="" style="width: 100%" class="form-control "> 
                      <option value="" selected="">-- Pilih Jabatan --</option>
                      <?php 
                      $this->db->where('kode_jabatan !=','001');
                      $ambil_data = $this->db->get('master_jabatan');
                      $hasil_ambil_data = $ambil_data->result();
                      foreach ($hasil_ambil_data as $value) {
                        ?>
                        <option <?php if($get_user->kode_jabatan==$value->kode_jabatan){echo "selected";}?> value="<?php echo $value->kode_jabatan ;?>"><?php echo $value->nama_jabatan ;?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>

                            <!--<div class="form-group  col-xs-5">
                              <label class="gedhi">Hak Akses</label>
                              <input type="text" class="form-control" value="<?php #echo @$hasil_data->modul; ?>" name="modul" />
                            </div>-->

                            <div class="form-group  col-xs-5">
                              <label class="gedhi">Status</label>
                              <select class="form-control select2" name="status" id="status" readonly disabled="">
                                <option selected="" value="">--Pilih Status--</option>
                                <option  <?php if($get_user->status=='1'){echo "selected";}?>  value="1">Aktif</option>
                                <option  <?php if($get_user->status=='0'){echo "selected";}?> value="0">Tidak Aktif</option>
                              </select>
                            </div>
                            <br>
                            <div id="modul">
                                <!--<div class="form-group  col-xs-9">
                                    <label class="gedhi"><h3><b>Hak Akses</b></h3></label>
                                </div>
                                <div class="form-group col-xs-9">
                                      <table border="1" class="table table-bordered table-striped">
                                          <thead >
                                              <tr>
                                                  <th width="35px" ><center>No</center></th>
                                                  <th width="250px"><center>Nama Modul</center></th>
                                                  <th width="300px"><center>Checklist Modul yang Dipilih</center></th>
                                              </tr>
                                          </thead>
                                          <?php 

                                              $arr_modul = array();
                                              if(!empty($data))
                                              {
                                                  $arr_modul = explode('|', $hasil_data->modul);
                                              }

                                              $modul = $this->db->get('master_modul');
                                              $hasil_modul = $modul->result();
                                              if(!empty($hasil_modul)){
                                                  $no=1;
                                                  foreach ($hasil_modul as $key => $value){ ?>
                                                      <tr>
                                                          <td align="center"><?php echo $no;?></td>
                                                          <td align="center"><?php echo $value->modul;?></td>
                                                          <td><center><input readonly type="checkbox" name="modul[]" value="<?php echo $value->modul;?>" <?php echo (in_array($value->modul, $arr_modul)) ? 'checked':'';?> ></center></td>
                                                      </tr>                            
                                                      <?php $no++; 
                                                  } 
                                              }
                                              else{
                                                  echo '<tr><td colspan="3">Belum ada data</td></tr>';
                                              }
                                          ?>
                                      </table> 
                                    </div>-->
                                  </div>

                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <!-- /.row (main row) -->
                    </section><!-- /.content -->
                  </div><!-- /.content-wrapper -->
                  <style type="text/css" media="screen">
                  .btn-back
                  {
                    position: fixed;
                    bottom: 10px;
                    left: 10px;
                    z-index: 999999999999999;
                    vertical-align: middle;
                    cursor:pointer
                  }
                </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

                <script>
                  $('.btn-back').click(function(){
                    $(".tunggu").show();
                    window.location = "<?php echo base_url().'master/user/'; ?>";
                  });
                </script>
                <script type="text/javascript">

                  $(document).ready(function(){
                    $(".select2").select2();
                    $('#modul').hide();
                  });

                  $(function () {

                    $('#jabatan').change(function(){

                      var jabatan = $('#jabatan').val();
                      if(jabatan == ''){
                       $('#modul').hide();
                     }
                     else{
                      $('#modul').show();
                    }
                  });

      //jika tombol Send diklik maka kirimkan data_form ke url berikut
      $("#data_form").submit( function() { 

        $.ajax( {  
          type :"post", 
          <?php 
          if (empty($uri)) {
            ?>
            //jika tidak terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/user/simpan_tambah_user'; ?>",
            <?php }
            else { ?>
            //jika terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/user/simpan_edit_user'; ?>",
            <?php }
            ?>  
            cache :false,  
            data :$(this).serialize(),
            beforeSend:function(){
              $(".tunggu").show();  
            },
            success : function(data) {  
              $(".sukses").html(data);   
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'master/user/' ?>";
              },1000);      
            },  
            error : function() {  
              alert("Data gagal dimasukkan.");  
            }  
            return false;                          
          });   

      });

    </script>