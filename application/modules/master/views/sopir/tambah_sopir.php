

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Form Sopir
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>

          <div class="box-body">                   
            <div class="sukses" ></div>
            <form id="data_jabatan"  method="post">         
              <div class="row">  

                <?php
                $uri = $this->uri->segment(4);
                if(!empty($uri)){
                  $data = $this->db->get_where('master_sopir',array('kode_sopir'=>$uri));
                  $hasil_data = $data->row();
                  ?>
                  <?php
                }

                $this->db->select_max('id');
                $get_max_member = $this->db->get('master_sopir');
                $max_member = $get_max_member->row();

                $this->db->where('id', $max_member->id);
                $get_member = $this->db->get('master_sopir');
                $member = $get_member->row();
                $nomor = substr(@$member->kode_sopir, 4);
                $nomor = $nomor + 1;
                $string = strlen($nomor);
                if($string == 1){
                  $kode_sopir ='000'.$nomor;
                } else if($string == 2){
                  $kode_sopir ='00'.$nomor;
                } else if($string == 3){
                  $kode_sopir ='0'.$nomor;
                } else if($string == 4){
                  $kode_sopir =''.$nomor;
                } 
                ?>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Kode Sopir</label>
                  <input type="hidden" name="id" value="<?php echo @$hasil_data->id ?>" />
                  <input readonly="" <?php if(!empty($uri)){ echo "readonly='true'"; } ?> type="text" class="form-control" value="<?php if(!empty($uri)){echo @$hasil_data->kode_sopir;}else{ echo "SO_".$kode_sopir;} ?>" name="kode_sopir" id="kode_sopir"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Nama Sopir</label>
                  <input type="text" class="form-control" value="<?php echo @$hasil_data->nama_sopir; ?>" name="nama_sopir" id="nama_sopir"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Alamat</label>
                  <input type="text" class="form-control" value="<?php echo @$hasil_data->alamat; ?>" name="alamat" id="alamat"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Telepon</label>
                  <input type="text" class="form-control" value="<?php echo @$hasil_data->telp; ?>" name="telp" id="telp"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Keterangan</label>
                  <!-- <input type="text" class="form-control" value="<?php echo @$hasil_data->keterangan; ?>" name="keterangan" id="keterangan"/> -->
                  <textarea class="form-control"  name="keterangan" id="keterangan"><?php echo @$hasil_data->keterangan; ?></textarea>
                </div>  

                 <div class="form-group  col-xs-6">
                  <label class="gedhi">Status Sopir</label>
                  <select class="form-control" id="status" name="status">
                    <option>Pilih</option>
                    <option value="1" <?php if(@$hasil_data->status=='1'){echo "selected";}?>>Aktif</option>
                    <option value="0" <?php if(@$hasil_data->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                  </select>
                </div>                        
              </div>
              <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
              <br>
              <center><button type="submit" class="btn btn-primary btn-lg" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
            </form>
          </div>
        </div>
      </div>

      <!-- /.row (main row) -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'master/sopir/'; ?>";
    });
  </script>
  <div id="modal-confirm-temp" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data komposisi tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="delDataTemp()" class="btn red">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">

    $(document).ready(function(){

      $('#kode_jabatan').on('change',function(){
        var kode_jabatan = $('#kode_jabatan').val();
        var url = "<?php echo base_url() . 'master/jabatan/get_kode' ?>";
        $.ajax({
          type: 'POST',
          url: url,
          data: {kode_jabatan:kode_jabatan},
          success: function(msg){
            if(msg == 1){
              $(".sukses").html('<div class="alert alert-warning">Kode_Telah_dipakai</div>');
              setTimeout(function(){
                $('.sukses').html('');
              },1700);              
              $('#kode_jabatan').val('');
            }
            else{

            }
          }
        });
      });

      $(".select2").select2();
      $("#tabel_daftar").dataTable();
    });

    $(function () {
      //jika tombol Send diklik maka kirimkan data_form ke url berikut
      $("#data_jabatan").submit( function() { 

        $.ajax( {  
          type :"post", 
          <?php 
          if (empty($uri)) {
            ?>
            //jika tidak terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/sopir/simpan_tambah_sopir'; ?>",
            <?php }
            else { ?>
            //jika terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/sopir/simpan_edit_sopir'; ?>",
            <?php }
            ?>  
            cache :false,  
            data :$(this).serialize(),
            beforeSend:function(){
              $(".tunggu").show();  
            },
            success : function(data) {  
              $(".sukses").html(data);   
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'master/sopir/' ?>";},1500);              
            },  
            error : function() {  
              alert("Data gagal dimasukkan.");  
            }  
          });
        return false;                          
      });   

      
    });

  </script>