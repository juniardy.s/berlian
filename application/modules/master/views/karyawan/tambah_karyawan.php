<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Tambah Karyawan
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>


          <div class="box-body">            
            <div class="sukses" ></div>
            <form id="data_jabatan"  method="post">         
              <div class="row">  

                <?php
                $uri = $this->uri->segment(4);
                if(!empty($uri)){
                  $data = $this->db->get_where('master_karyawan',array('kode_karyawan'=>$uri));
                  $hasil_data = $data->row();


                  ?>
                  <input type="hidden" name="kode_karyawan" value="<?php echo @$hasil_data->kode_karyawan ?>" />
                  <?php
                }

                $this->db->select_max('id');
                $get_max_member = $this->db->get('master_karyawan');
                $max_member = $get_max_member->row();

                $this->db->where('id', $max_member->id);
                $get_member = $this->db->get('master_karyawan');
                $member = $get_member->row();
                $nomor = substr(@$member->kode_karyawan, 4);
                $nomor = $nomor + 1;
                $string = strlen($nomor);
                if($string == 1){
                  $kode_karyawan ='000'.$nomor;
                } else if($string == 2){
                  $kode_karyawan ='00'.$nomor;
                } else if($string == 3){
                  $kode_karyawan ='0'.$nomor;
                } else if($string == 4){
                  $kode_karyawan =''.$nomor;
                } 

                ?>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Kode Karyawan</label>
                  <input type="hidden" name="id" value="<?php echo @$hasil_data->id ?>" />
                  <input readonly="" required="" <?php if(!empty($uri)){ echo "readonly='true'"; } ?> type="text" class="form-control" value="<?php if(!empty($uri)){echo @$hasil_data->kode_karyawan;}else{echo "KAR_". $kode_karyawan;} ?>" name="kode_karyawan" id="kode_karyawan"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Jenis Karyawan</label>
                  <select onchange="gaji()" class="form-control" required="" id="jenis_karyawan" name="jenis_karyawan">
                    <option value="">Pilih</option>
                    <option value="borongan" <?php if(@$hasil_data->jenis_karyawan=='borongan'){echo "selected";}?>>Borongan</option>
                    <option value="tetap" <?php if(@$hasil_data->jenis_karyawan=='tetap'){echo "selected";}?>>Tetap</option>
                  </select>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Nama Karyawan</label>
                  <input type="text" required="" class="form-control" value="<?php echo @$hasil_data->nama_karyawan; ?>" name="nama_karyawan" id="nama_karyawan"/>
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi">Alamat</label>
                  <input type="text" required="" class="form-control" value="<?php echo @$hasil_data->alamat; ?>" name="alamat" id="alamat"/>
                </div>

                <div class="form-group  col-xs-4">
                  <label class="gedhi">Telp</label>
                  <input type="text" required="" class="form-control" value="<?php echo @$hasil_data->telp; ?>" name="telp" id="telp"/>
                </div>




                <div class="form-group  col-xs-4">
                  <label class="gedhi">Status Karyawan</label>
                  <select class="form-control" id="status" name="status" required="">
                    <option>Pilih</option>
                    <option value="1" <?php if(@$hasil_data->status=='1'){echo "selected";}?>>Aktif</option>
                    <option value="0" <?php if(@$hasil_data->status=='0'){echo "selected";}?>>Tidak Aktif</option>
                  </select>
                </div>  

               
                <div class="form-group  col-xs-4">
                 <label class="gedhi tetap">Gaji Tetap</label>
                 <label class="gedhi pull-right tetap" id="hasil_rupiah"></label>
                 <input type="text" onkeyup="rupiah()"  placeholder="Gaji Tetap" class="form-control tetap" value="<?php echo @$hasil_data->gaji_tetap; ?>" name="gaji_tetap" id="gaji_tetap"/>

                <!--  <label class="gedhi harian">Gaji Harian</label>
                 <input type="text" placeholder="Gaji Harian" class="form-control harian" value="<?php echo @$hasil_data->gaji_harian; ?>" name="gaji_harian" id="gaji_harian"/> -->
               </div>

                
             </div>
             <div class="row"> 
              <div class="form-group  col-xs-6">
                  <label class="gedhi">Keterangan</label>
                  <!-- <input type="text" class="form-control" value="<?php #echo @$hasil_data->keterangan; ?>" name="keterangan" id="keterangan"/> -->
                  <textarea class="form-control" required="" name="keterangan" id="keterangan"><?php echo @$hasil_data->keterangan; ?></textarea>
                </div>   
             </div>
             <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
             <br>
              <center><button type="submit" class="btn btn-primary btn-lg" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
           </form>


         </div>

         <!------------------------------------------------------------------------------------------------------>

       </div>
     </div>


     <div class="box box-info">


      <div class="box-body">            



      </section><!-- /.Left col -->      
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/karyawan/'; ?>";
  });
</script>


<script type="text/javascript">
function rupiah(){
    var url = "<?php echo base_url().'master/karyawan/rupiah'; ?>";
    var gaji_tetap = $("#gaji_tetap").val();
    $.ajax({
      type: 'POST',
      url: url,
      data: {gaji_tetap:gaji_tetap},
      success: function(rupiah){
        $("#hasil_rupiah").text(rupiah);
      }
    });
  }
  function gaji(){
    var jenis_karyawan=$('#jenis_karyawan').val();
    if(jenis_karyawan=='tetap'){
     
      $(".tetap").show();
    }else{
      
      $(".tetap").hide();
    }
  }
  $(document).ready(function(){
    var jenis_karyawan=$('#jenis_karyawan').val();
    if(jenis_karyawan=='tetap'){
     
      $(".tetap").show();
    }else if(jenis_karyawan=='tetap'){
      
      $(".tetap").show();
    }else{
     $(".harian").hide();
     $(".tetap").hide();
   }


   

   $("#tabel_daftar").dataTable();
 });

  $(function () {
      //jika tombol Send diklik maka kirimkan data_form ke url berikut
      $("#data_jabatan").submit( function() { 
        $.ajax( {  
          type :"post", 
          <?php 
          if (empty($uri)) {
            ?>
            //jika tidak terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/karyawan/simpan_tambah_karyawan'; ?>",
            <?php }
            else { ?>
            //jika terdapat segmen maka simpan di url berikut
            url : "<?php echo base_url() . 'master/karyawan/simpan_edit_karyawan'; ?>",
            <?php }
            ?>  
            cache :false,  
            data :$(this).serialize(),
            beforeSend:function(){
              $(".tunggu").show();  
            },
            success : function(data) {  
              $(".sukses").html(data);   
              setTimeout(function(){$('.sukses').html('');
              window.location = "<?php echo base_url() . 'master/karyawan/' ?>";},500);             
            },  
            error : function() {  
              alert("Data gagal dimasukkan.");  
            }  
          });
        return false;                          
      });   

      
    });


  </script>