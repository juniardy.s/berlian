<?php 

$kode_jalur=$this->input->post('kode_jalur');
$nama_member=$this->input->post('nama_member');

if(!empty($kode_jalur)){
  $this->db->where('kode_jalur',$kode_jalur);
}
if(!empty($nama_member)){
  $this->db->like('nama_member',$nama_member);
}
$hasil_data_member = $this->db->get('master_member')->result();

?>
<div class="row">
  <div class="col-md-3">
   <label>Cari Data </label>
   <select  name="kode_data" id="kode_data" class="form-control" onchange="cari_data()">
    <option value="">--Pilih Data--</option>
    <option value="jalur">Jalur</option>
    <option value="member">Member</option>
  </select>
</div>
<div class="col-md-3">
  <div id="jalur">
    <div class="form-group">
      <label>Nama Jalur</label>
      <?php
      $kode_unit = @$hasil_unit->kode_unit;
      $get_jalur = $this->db->get_where('master_jalur',array('status'=>'1'));
      $hasil_get_jalur = $get_jalur->result();

      ?>
      <select  name="kode_jalur" id="kode_jalur" class="form-control select2">
        <option selected="true" value="">--Pilih Jalur--</option>
        <?php 
        foreach($hasil_get_jalur as $daftar){    
          ?>
          <option <?php if(@$hasil_member->kode_jalur==$daftar->kode_jalur){ echo "selected"; } ?> value="<?php echo $daftar->kode_jalur; ?>"><?php echo $daftar->nama_jalur; ?></option>
          <?php 
        }
        ?>
      </select>
    </div>
  </div>
  <div id="member">
    <div class="form-group">
      <label>Nama Member</label>
      <input type="text" id="nama_member1" name="nama_member1" class="form-control" placeholder="Nama Member">
    </div>
  </div>
</div>
<div class="col-md-3">
  <a onclick="cari_member()" style="margin-top: 25px;" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>
</div>
<div class="col-md-3" >
  <label>Total Member</label>
  <div class="a" align="center"><h3><?php echo count($hasil_data_member); ?></h3></div>
  <br>
</div>

</div>
<br>
<br>
<br>      
<div class="row">
  <div class="col-md-12">
    <a onclick="print_daftar()" style="margin-top: 0px;" class="btn btn-lg blue pull-right "><i class="fa fa-print"></i> Print</a>

  </div>
</div>
<br>
<div class="box-body">            
  <div class="sukses" ></div>
  <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
    <thead>
      <tr>
        <th width="50px;">No</th>
        <th>Kode Member</th>
        <th>Nama Member</th>
        <th>Alamat</th>
        <th>Telepon</th>
        <th>Jalur</th>
        <th>Keterangan</th>
        <th>Status Member</th>
        <th width="220px">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $kode_jalur=$this->input->post('kode_jalur');
      $nama_member=$this->input->post('nama_member');
      
      if(!empty($kode_jalur)){
        $this->db->where('kode_jalur',$kode_jalur);
      }
      if(!empty($nama_member)){
        $this->db->like('nama_member',$nama_member);
      }
      $this->db->order_by('nama_member','asc');
      $get_member = $this->db->get('master_member');
      $hasil_member =$get_member->result();
      $no = 1;
      foreach($hasil_member as $item){
        if($this->session->flashdata('message')==$item->kode_member){

          echo '<tr id="warna" style="background: #88cc99; display: none;">';
        }
        else{
          echo '<tr>';
        }
        ?>
        <td><?php echo $no;?></td>
        <td><?php echo $item->kode_member; ?></td>                  
        <td><?php echo $item->nama_member; ?></td>                  
        <td><?php echo $item->alamat_member; ?></td>
        <td><?php echo $item->telp_member; ?></td>
        <td><?php echo $item->nama_jalur; ?></td>
        <td><?php echo $item->keterangan; ?></td>
        <td><?php echo cek_status($item->status_member); ?></td>
        <td align="center">
          <div class="btn-group">
            <a href="detail/<?php echo $item->id ?>" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle green"><i class="fa fa-search"></i> </a>
            <a href="tambah/<?php echo $item->id ?>" onclick="actEdit($(this), event)" data-toggle="tooltip" title="Edit" class="btn btn-icon-only btn-circle yellow"><i class="fa fa-pencil"></i> </a>
            <a style="padding:3.5px;" onclick="actDelete('<?php echo $item->id ?>')" data-toggle="tooltip" title="Delete" class="btn btn-icon-only btn-circle red"><i class="fa fa-trash"> </i></a>
          </div>
        </td>
      </tr>
      <?php
      $no++;
    } ?>
  </tbody>                
</table>
</div>