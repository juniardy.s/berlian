<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Form Bahan Setengah Jadi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>

        <?php
        $param = $this->uri->segment(4);
        if(!empty($param)){
          $bahan_setengah = $this->db->get_where('master_bahan_setengah_jadi',array('id'=>$param));
          $hasil_bahan_setengah = $bahan_setengah->row();
        }    

        ?>
        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">
                <div class="form-group  col-xs-5">
                  <label><b>Kode Bahan Setengah Jadi</label>

                  <div class="input-group">
                    <?php
                    $this->db->select('kode_bahan_setengah_jadi');
                    $bb = $this->db->get('master_setting');
                    $hasil_bb = $bb->row();
                    ?>
                    <span class="input-group-addon"><?php if(empty($param)){ echo $hasil_bb->kode_bahan_setengah_jadi.'_'; } ?></span>
                    <input type="hidden" id="kode_setting" value="<?php echo @$hasil_bb->kode_bahan_setengah_jadi ?>"></input>
                    <input required name="kode_bahan_setengah" value="<?php echo @$hasil_bahan_setengah->kode_bahan_setengah_jadi ?>"  <?php if(!empty($param)){ echo "readonly='true'"; } ?> type="text" class="form-control" id="kode_bahan_setengah" />
                  </div>
                </div>
                <div class="form-group  col-xs-5">
                  <label class="gedhi"><b>Nama Bahan Setengah Jadi</label>
                  <input required value="<?php echo @$hasil_bahan_setengah->nama_bahan_setengah_jadi; ?>" type="text" class="form-control" name="nama_bahan_setengah_jadi" />
                </div>

                <div class="form-group  col-xs-5">
                  <label class="gedhi"><b>Unit</label>
                  <?php
                  $kode_default = $this->db->get('setting_gudang');
                  $hasil_unit =$kode_default->row();
                  $kode_unit=$hasil_unit->kode_unit;

                  $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
                  $hasil_unit = $unit->row();
                  ?>
                  <input required value="<?php echo @$hasil_unit->nama_unit; ?>" type="text" class="form-control" readonly />
                  <input value="<?php echo @$hasil_unit->kode_unit; ?>" type="hidden" class="form-control" name="kode_unit" />
                </div>

                <div class="form-group  col-xs-5">
                  <label class="gedhi"><b>Kategori</label>
                  <?php

                  $kode_unit = @$hasil_unit->kode_unit;
                  $get_rak = $this->db->get_where('master_rak',array('kode_unit'=>$kode_unit,'status'=>'1'));
                  $hasil_get_rak = $get_rak->result();

                  ?>
                  <select required name="kode_rak" id="kode_rak" class="form-control">
                    <option selected="true" value="">--Pilih Kategori--</option>
                    <?php 
                    foreach($hasil_get_rak as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_setengah->kode_rak==$daftar->kode_rak){ echo "selected"; } ?> value="<?php echo $daftar->kode_rak; ?>"><?php echo $daftar->nama_rak; ?></option>
                      <?php  
                    } ?>
                  </select>
                </div>

                <div class="form-group  col-xs-5">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Satuan Stok</label>
                  <select required class="form-control stok select2" name="id_satuan_stok">
                    <option selected="true" value="">--Pilih satuan stok--</option>
                    <?php 
                    foreach($hasil_dft_satuan as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_setengah->id_satuan_stok==$daftar->kode){ echo "selected"; } ?> value="<?php echo $daftar->kode; ?>"><?php echo $daftar->nama; ?></option>
                      <?php 
                    } ?>
                  </select> 
                </div>
                <div class="form-group  col-xs-5">
                  <label class="gedhi"><b>Stok Minimal</label>
                  <input required type="text" class="form-control" name="stok_minimal" value="<?php echo @$hasil_bahan_setengah->stok_minimal ?>" />
                </div>
                <div class="form-group  col-xs-5">
                  <label class="gedhi"><b>Total HPP</label>
                  <input required type="text" class="form-control" name="hpp" id="hpp" value="<?php echo @$hasil_bahan_setengah->hpp ?>" />
                  <h3><div id="nilai_hpp"></div></h3>
                </div>
                <div class="form-group  col-xs-5">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Jenis Bahan</label>
                  <select required class="form-control" name="jenis_bahan">
                    <option selected="true" value="">--Pilih jenis bahan--</option>
                        <option value="sendiri">Factory</option>
                        <option value="plasma">Plasma</option>
                  </select> 
                </div>
               </div>
               
                  <div class="row">
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Bahan Baku</label>
                      <?php
                      $get_bb = $this->db->get('master_bahan_baku');
                      $hasil_get_bb = $get_bb->result();

                      ?>
                      <select name="bahan" id="bahan" class="form-control">
                        <option selected="true" value="">--Pilih Bahan Baku--</option>
                        <?php 
                        foreach($hasil_get_bb as $daftar){    
                          ?>
                          <option value="<?php echo $daftar->kode_bahan_baku; ?>"><?php echo $daftar->nama_bahan_baku; ?></option>
                          <?php  
                        } ?>
                      </select>
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Jumlah</label>
                      <input type="text" class="form-control" name="jumlah" id="jumlah" />
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Satuan</label>
                      <input readonly="" type="text" class="form-control" name="nama_satuan" id="nama_satuan" value=""/>
                      <input type="hidden" class="form-control" name="id_satuan" id="id_satuan" value=""/>
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>HPP</label>
                      <input readonly="" type="text" class="form-control" id="hpp_bb" value=""/>
                      
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Rumus</label>
                      <div class="input-group">
                    
                    <input type="text" class="form-control" value="10" id="rumus" />
                    <span class="input-group-addon">%</span>
                  </div>
                      
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Harga</label>
                      <input type="text" class="form-control" id="harga" value=""/>
                      
                    </div>
                    <div class="form-group  col-xs-12">
                      
                      <button type="button" id="add-item" class="btn btn-primary btn-md pull-right"><i class="fa fa-plus"></i> Add</button>
                    </div>
                  </div>
               
                <div class="form-group  col-lg-12">
                  <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Bahan</th>
                        <th>Jumlah</th>
                        <th>HPP</th>
                        <th>Rumus</th>
                        <th>Harga</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody id="opsi_bahan_setengah">
                    
                    </tbody>
                  </table>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-lg green-seagreen"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data bahan setengah jadi tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="actDelete()" class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------------------------------------------------------------>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  function get_temp(){
    kode_bahan_setengah = $("#kode_setting").val()+'_'+$("#kode_bahan_setengah").val();
    $("#opsi_bahan_setengah").load('<?php echo base_url().'master/bahan_setengah/table_temp/'; ?>'+kode_bahan_setengah);
    
  }
  function get_opsi(){
    kode_bahan_setengah = $("#kode_bahan_setengah").val();
    $("#opsi_bahan_setengah").load('<?php echo base_url().'master/bahan_setengah/table_opsi/'; ?>'+kode_bahan_setengah);
  }
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/bahan_setengah/'; ?>";
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    
    $(".select2").select2();
    <?php 
    if(!empty($param)){ ?>
      get_opsi();
      <?php 
    }else{ ?>
    get_temp();
    <?php } ?>
  });
  $(function(){
    $('#kode_bahan_setengah').on('change',function(){
      var kode_input = $('#kode_bahan_setengah').val();
      var kode_setting = $('#kode_setting').val();
      var kode_bahan_setengah = kode_setting + "_" + kode_input ;
      var url = "<?php echo base_url() . 'master/bahan_setengah/get_kode' ?>";
      $.ajax({
        type: 'POST',
        url: url,
        data: {kode_bahan_setengah:kode_bahan_setengah},
        success: function(msg){
          if(msg == 1){
            $(".sukses").html('<div class="alert alert-warning">Kode Telah Dipakai</div>');
            setTimeout(function(){
              $('.sukses').html('');
            },1700);              
            $('#kode_bahan_setengah').val('');

          }
          else{

          }
        }
      });
    });

    $("#bahan").change(function(){
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'master/bahan_setengah/get_satuan' ?>",
        dataType: 'json',
        data: {kode_bahan_baku:$(this).val()},
        success: function(data){
          $("#id_satuan").val(data.id_satuan_stok);
          $("#nama_satuan").val(data.satuan_stok);
          $("#hpp_bb").val(data.hpp);
          var hpp = $("#hpp_bb").val();
          var rumus = $("#rumus").val();
          var harga = (hpp*rumus/100)+parseInt(hpp);
          $("#harga").val(harga);
        },
        error : function(data) {  
          alert('Sorry');
        }  
      });
    });
    
    
    $("#rumus").keyup(function(){
         var hpp = $("#hpp_bb").val();
          var rumus = $("#rumus").val();
          var harga = (hpp*rumus/100)+parseInt(hpp);
          $("#harga").val(harga);
    });
    
    $("#add-item").click(function(){
      <?php 
      if(!empty($param)){ ?>
        kode_bahan_setengah = $("#kode_bahan_setengah").val();
        <?php 
      } else { ?>
        kode_bahan_setengah = $("#kode_setting").val()+'_'+$("#kode_bahan_setengah").val();
        <?php
      } ?>
      bahan_baku = $("#bahan").val();
      jumlah = $("#jumlah").val();
      id_satuan = $("#id_satuan").val();
      satuan = $("#nama_satuan").val();
      var hpp = $("#hpp_bb").val();
      var rumus = $("#rumus").val();
      var harga = $("#harga").val();
      $.ajax({
        type: 'POST',
        <?php 
        if(!empty($param)){ ?>
          url: "<?php echo base_url() . 'master/bahan_setengah/tambah_opsi' ?>",
          <?php 
        } else { ?>
          url: "<?php echo base_url() . 'master/bahan_setengah/tambah_temp' ?>",
          <?php
        } ?>
        data: {kode_bahan_setengah:kode_bahan_setengah, bahan_baku:bahan_baku, jumlah:jumlah, 
        id_satuan:id_satuan, satuan:satuan,hpp:hpp,rumus:rumus,harga:harga},
        success: function(data){
          $("#bahan").val('');
          $("#jumlah").val('');
          $("#id_satuan").val('');
          $("#nama_satuan").val('');
          $("#hpp_bb").val('');
          $("#harga").val('');
          <?php 
          if(!empty($param)){ ?>
            get_opsi();
            <?php 
          } else { ?>
            get_temp();
            <?php
          } ?>
          
        },
        error : function(data) {  
          alert('Sorry');
        }  
      });
    });

    $("#hpp").keyup(function(){
      var temp_data = "<?php echo base_url().'master/bahan_setengah/get_rupiah/'?>";
      var hpp = $('#hpp').val() ;
      $.ajax({
        type: "POST",
        url: temp_data,
        data: {hpp:hpp},
        success: function(hasil) {              
          $("#nilai_hpp").html(hasil);
        }
      });

    });

    $("#data_form").submit( function() {
      <?php if(!empty($param)){ ?>
        var url = "<?php echo base_url(). 'master/bahan_setengah/simpan_edit'; ?>";  
        <?php 
      }else{ ?>
        var url = "<?php echo base_url(). 'master/bahan_setengah/simpan'; ?>";
        <?php 
      } ?>
      $.ajax( {
        type:"POST", 
        url : url,  
        cache :false,  
        dataType : 'json',
        data :$(this).serialize(),

        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
          if(data.hasil=="opsi"){
            $(".sukses").html('<div class="alert alert-danger">Opsi Tidak Ada!!!</div>');
            setTimeout(function(){$('.sukses').html('');},1000);
            $(".tunggu").hide();
          }
          else{
            $(".sukses").html('<div class="alert alert-success">Data Berhasil Disimpan</div>');
            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/bahan_setengah/' ?>";},1000);  
          }
          $(".loading").hide();   

        },  
        error : function(data) {  
          $(".sukses").html('<div class="alert alert-success">Data Gagal Disimpan</div>');
        }  
      });
      return false;                    
    }); 
  })
</script>