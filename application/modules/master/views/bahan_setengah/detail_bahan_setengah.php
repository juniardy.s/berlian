<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Detail Bahan Setengah Jadi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>

        <?php
        $param = $this->uri->segment(4);
        if(!empty($param)){
          $bahan_setengah = $this->db->get_where('master_bahan_setengah_jadi',array('id'=>$param));
          $hasil_bahan_setengah = $bahan_setengah->row();
        }    

        ?>
        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">
                <div class="form-group  col-xs-6">
                  <label><b>Kode Bahan Setengah Jadi</label>

                  <div class="input-group">
                    <?php
                    $this->db->select('kode_bahan_setengah_jadi');
                    $bb = $this->db->get('master_setting');
                    $hasil_bb = $bb->row();
                    ?>
                    <span class="input-group-addon"><?php if(empty($param)){ echo $hasil_bb->kode_bahan_setengah_jadi.'_'; } ?></span>
                    <input type="hidden" id="kode_setting" value="<?php echo @$hasil_bb->kode_bahan_setengah_jadi ?>"></input>
                    <input required name="kode_bahan_setengah" value="<?php echo @$hasil_bahan_setengah->kode_bahan_setengah_jadi ?>"  <?php if(!empty($param)){ echo "readonly='true'"; } ?> type="text" class="form-control" id="kode_bahan_setengah" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Nama Bahan Setengah Jadi</label>
                  <input disabled="" required value="<?php echo @$hasil_bahan_setengah->nama_bahan_setengah_jadi; ?>" type="text" class="form-control" name="nama_bahan_setengah_jadi" />
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Unit</label>
                  <?php
                  $kode_default = $this->db->get('setting_gudang');
                  $hasil_unit =$kode_default->row();
                  $kode_unit=$hasil_unit->kode_unit;

                  $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
                  $hasil_unit = $unit->row();
                  ?>
                  <input disabled required value="<?php echo @$hasil_unit->nama_unit; ?>" type="text" class="form-control" readonly />
                  <input value="<?php echo @$hasil_unit->kode_unit; ?>" type="hidden" class="form-control" name="kode_unit" />
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Kategori</label>
                  <?php

                  $kode_unit = @$hasil_unit->kode_unit;
                  $get_rak = $this->db->get_where('master_rak',array('kode_unit'=>$kode_unit,'status'=>'1'));
                  $hasil_get_rak = $get_rak->result();

                  ?>
                  <select disabled required name="kode_rak" id="kode_rak" class="form-control">
                    <option selected="true" value="">--Pilih Kategori--</option>
                    <?php 
                    foreach($hasil_get_rak as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_setengah->kode_rak==$daftar->kode_rak){ echo "selected"; } ?> value="<?php echo $daftar->kode_rak; ?>"><?php echo $daftar->nama_rak; ?></option>
                      <?php  
                    } ?>
                  </select>
                </div>

                <div class="form-group  col-xs-6">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Satuan Stok</label>
                  <select disabled required class="form-control stok select2" name="id_satuan_stok">
                    <option selected="true" value="">--Pilih satuan stok--</option>
                    <?php 
                    foreach($hasil_dft_satuan as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_setengah->id_satuan_stok==$daftar->kode){ echo "selected"; } ?> value="<?php echo $daftar->kode; ?>"><?php echo $daftar->nama; ?></option>
                      <?php 
                    } ?>
                  </select> 
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Stok Minimal</label>
                  <input disabled required type="text" class="form-control" name="stok_minimal" value="<?php echo @$hasil_bahan_setengah->stok_minimal ?>" />
                </div>
                <div class="form-group  col-xs-4">
                  <label class="gedhi"><b>HPP</label>
                  <input readonly="true" required type="text" class="form-control" name="hpp" id="hpp" value="<?php echo @$hasil_bahan_setengah->hpp ?>" />
                  <h3><div id="nilai_hpp"></div></h3>
                </div>

                <div class="form-group  col-xs-4">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Jenis Bahan</label>
                  <select disabled required class="form-control" name="jenis_bahan">
                    <option  selected="true" value="">--Pilih jenis bahan--</option>
                        <option <?php if ($hasil_bahan_setengah->jenis_bahan=='sendiri') {
                          echo "selected";
                        }?> value="sendiri">Factory</option>
                        <option <?php if ($hasil_bahan_setengah->jenis_bahan=='plasma') {
                          echo "selected";
                        }?> value="plasma">Plasma</option>
                  </select> 
                </div>
                <div class="form-group  col-xs-4">
                  <label class="gedhi"><b>Tarif Borongan</label>
                  <input readonly="true" required type="text" class="form-control" name="tarif_borongan" id="tarif_borongan" value="<?php echo @$hasil_bahan_setengah->tarif_borongan ?>" />
                 
                </div>
               </div>
               
               
               
                <div class="form-group">
                  <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Bahan</th>
                        <th>Jumlah</th>
                        <th>HPP</th>
                        <!--<th>Rumus</th>-->
                        <th>Harga</th>
                        
                      </tr>
                    </thead>
                    <tbody id="opsi_bahan_setengah">
                    <?php
$get_kode =  @$hasil_bahan_setengah->kode_bahan_setengah_jadi;

$this->db->where('kode_bahan_setengah_jadi', $get_kode);
$get_temp = $this->db->get('opsi_master_bahan_setengah_jadi');
$hasil_temp = $get_temp->result();

$no = 1;
$total = 0;
foreach ($hasil_temp as $temp) {
	?>
	<tr>
		<td><?php echo $no ?></td>
		<td><?php echo $temp->nama_bahan ?></td>
		<td><?php echo $temp->jumlah.' '.$temp->nama_satuan ?></td>
        <td><?php echo format_rupiah($temp->hpp) ?></td>
        <!--<td><?php echo $temp->rumus." %"; ?></td>-->
        <td><?php echo format_rupiah($temp->harga) ?></td>
		
	</tr>
	<?php 
	$no++;
    $total += $temp->harga;
}
?>
<?php

?>
<tr>
    <td align="right" colspan="4">Total</td>
    <td colspan="2"><?php echo format_rupiah($total); ?>
        <input type="hidden" id="total" value="<?php echo $total; ?>" />
    </td>
    
</tr>
<?php 
        $tambahan = $this->db->get('master_tambahan');
        $hasil_tambahan = $tambahan->result();
        $total_tambahan = 0;
        foreach($hasil_tambahan as $daftar){
    ?>
    <tr hidden>
        <td align="right" colspan="4"><?php echo $daftar->item; ?></td>
        <td colspan="2"><?php echo format_rupiah($daftar->harga); ?></td>
    </tr>
    <?php $total_tambahan += $daftar->harga; } ?>
    <tr hidden>
        <td align="right" colspan="4">Sales Komisi</td>
        <td colspan="2"><?php echo format_rupiah($total * 10 / 100); ?></td>
    </tr>
    <tr hidden="">
        <td align="right" colspan="4">Total HPP</td>
       
        <td colspan="2">
         <?php if($total !=0){ ?>
        <?php echo format_rupiah(($total * 10 / 100) + ($total + $total_tambahan) ); ?>
       <!--  <input type="hidden" value="<?php #echo ($total * 10 / 100) + ($total + $total_tambahan); ?>" id="total_hpp" /> -->
        <?php }else{ ?>
        <input type="hidden" value="0" id="total_hpp" />
        <?php  } ?>
        </td>
    </tr>
    
   
                    </tbody>
                  </table>
                </div>
              
              <div class="box-footer">
                <a href="<?php echo base_url().'master/bahan_setengah/' ?>" class="btn btn-lg green-seagreen"> OK</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------------------------------------------------------------>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/bahan_setengah/'; ?>?" + window.location.search.substring(1);
  });
</script>
