<?php
$get_kode = $this->uri->segment(4);
$table = $this->uri->segment(3);

$this->db->where('kode_bahan_setengah_jadi', $get_kode);
if($table == 'table_temp'){
	$get_temp = $this->db->get('opsi_master_bahan_setengah_jadi_temp');
} else{
	$get_temp = $this->db->get('opsi_master_bahan_setengah_jadi');
}
$hasil_temp = $get_temp->result();

$no = 1;
$total = 0;
foreach ($hasil_temp as $temp) {
	?>
	<tr>
		<td><?php echo $no ?></td>
		<td><?php echo $temp->nama_bahan ?></td>
		<td><?php echo $temp->jumlah.' '.$temp->nama_satuan ?></td>
		<td><?php echo format_rupiah($temp->hpp) ?></td>
		<!--<td><?php echo $temp->rumus." %"; ?></td>-->
		<td><?php echo format_rupiah($temp->harga) ?></td>
		<td width="10%">
			<a style="padding:3.5px;" onclick="modal(<?php echo $temp->id ?>)" data-toggle="tooltip" title="Delete" class="btn btn-icon-only btn-circle red"><i class="fa fa-trash"></i></a>
		</td>
	</tr>
	<?php 
	$no++;
	$total += $temp->harga;
}
?>
<?php

?>
<tr>
	<td align="right" colspan="4">Total</td>
	<td colspan="2"><?php echo format_rupiah($total); ?>
		<input type="hidden" id="total" value="<?php echo $total; ?>" />
	</td>

</tr>
<?php 
$tambahan = $this->db->get('master_tambahan');
$hasil_tambahan = $tambahan->result();
$total_tambahan = 0;
foreach($hasil_tambahan as $daftar){
	?>
	<tr >
		<td align="right" colspan="4"><?php echo $daftar->item; ?></td>
		<td colspan="2"><?php echo format_rupiah($daftar->harga); ?></td>
		<?php if($daftar->item=='Komisi Supir'){?>
		<td hidden ><input type="hidden" value="<?php echo @$daftar->harga; ?>" id="komisi_supir" name="komisi_supir" /></td> 

	</tr>
	<?php } $total_tambahan += $daftar->harga; } ?>
	<tr >
		<td align="right" colspan="4">Sales Komisi</td>
		<td colspan="2"><?php echo format_rupiah($total * 10 / 100); ?></td>
	<td hidden><input type="hidden" value="<?php echo @$total * 10 / 100; ?>" id="sales_komisi" name="sales_komisi" /></td> 
	</tr>
	<tr >
		<td align="right" colspan="4">Total HPP</td>

		<td colspan="2">
			<?php if($total !=0){ ?>
			<?php echo format_rupiah(($total * 10 / 100) + ($total + $total_tambahan) ); ?>
			<input type="hidden" value="<?php echo ($total * 10 / 100) + ($total + $total_tambahan); ?>" id="total_hpp" /> 
			<?php }else{ ?>
			<input type="hidden" value="0" id="total_hpp" />
			<?php  } ?>
		</td>
	</tr>


	<script type="text/javascript">
		function modal(Object){
			$('#id-delete').val(Object);
			$('#modal-confirm').modal('show');
		}




		function actDelete(){
			var id =$('#id-delete').val();
			$.ajax({
				type: 'POST',
				<?php 
				if($table == 'table_opsi'){ ?>
					url: "<?php echo base_url() . 'master/bahan_setengah/delete_opsi' ?>",
					<?php 
				} else { ?>
					url: "<?php echo base_url() . 'master/bahan_setengah/delete_temp' ?>",
					<?php
				} ?>
				data: {id:id},
				success: function(data){
					<?php 
					if($table == 'table_opsi'){ ?>
						get_opsi();
						<?php 
					} else { ?>
						get_temp();
						<?php
					} ?>
					get_hpp();
					$('#modal-confirm').modal('hide');

				},
				error : function(data) {  
					alert('Sorry');
				}  
			});
		}
	</script>