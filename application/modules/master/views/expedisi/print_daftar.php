<!DOCTYPE html>
<html>
<head>
	<title>Print Expedisi</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <table  >
    <tr>
      <td>
        <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
      </td>

    </tr>

  </table>
  <hr>
  <table class="" width="100%" border="0" style="border-collapse: collapse;">

    <tr>

      <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Data Expedisi</th>
    </tr>
  </table><br>
  <table class="" width="100%" border="1" style="border-collapse: collapse;">
    <?php
    $this->db->order_by('nama_expedisi','asc');
    $expedisi = $this->db->get('master_expedisi');
    $hasil_expedisi = $expedisi->result();
    ?>
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Expedisi</th>
        <th>Nama Expedisi</th>
        <th>Alamat</th>
        <th>Kode Pos</th>
        <th>Telepon</th>

        <th>Status</th>
        <th>Keterangan</th>

      </tr>
    </thead>
    <tbody>
      <?php
      $nomor = 1;

      foreach($hasil_expedisi as $daftar){ ?> 
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo @$daftar->kode_expedisi; ?></td>
        <td><?php echo @$daftar->nama_expedisi; ?></td>
        <td><?php echo @$daftar->alamat; ?></td>
        <td><?php echo @$daftar->kode_pos; ?></td>
        <td><?php echo @$daftar->telp; ?></td>
        <td><?php echo cek_status(@$daftar->status); ?></td>
        <td><?php echo @$daftar->keterangan; ?></td>

      </tr>
      <?php $nomor++; } ?>
    </tbody>

  </table>



  </html>