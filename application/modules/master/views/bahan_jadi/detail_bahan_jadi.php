<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Detail Bahan Jadi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>

        <?php
        $param = $this->uri->segment(4);
        if(!empty($param)){
          $bahan_jadi = $this->db->get_where('master_bahan_jadi',array('id'=>$param));
          $hasil_bahan_jadi = $bahan_jadi->row();
        }    

        ?>
        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">
                <div class="form-group  col-xs-6">
                  <label><b>Kode Bahan Jadi</label>

                  <div class="input-group">
                    <?php
                    $this->db->select('kode_bahan_jadi');
                    $bb = $this->db->get('master_setting');
                    $hasil_bb = $bb->row();
                    ?>
                    <span class="input-group-addon"><?php if(empty($param)){ echo $hasil_bb->kode_bahan_jadi.'_'; } ?></span>
                    <input type="hidden" id="kode_setting" value="<?php echo @$hasil_bb->kode_bahan_jadi ?>" readonly></input>
                    <input required readonly name="kode_bahan_jadi" value="<?php echo @$hasil_bahan_jadi->kode_bahan_jadi ?>"  <?php if(!empty($param)){ echo "readonly='true'"; } ?> type="text" class="form-control" id="kode_bahan_jadi" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Nama Bahan Jadi</label>
                  <input readonly required value="<?php echo @$hasil_bahan_jadi->nama_bahan_jadi; ?>" type="text" class="form-control" name="nama_bahan_jadi" />
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Unit</label>
                  <?php
                  $kode_default = $this->db->get('setting_gudang');
                  $hasil_unit =$kode_default->row();
                  $kode_unit=$hasil_unit->kode_unit;

                  $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
                  $hasil_unit = $unit->row();
                  ?>
                  <input required value="<?php echo @$hasil_unit->nama_unit; ?>" type="text" class="form-control" readonly />
                  <input value="<?php echo @$hasil_unit->kode_unit; ?>" type="hidden" class="form-control" name="kode_unit" />
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Kategori</label>
                  <?php

                  $kode_unit = @$hasil_unit->kode_unit;
                  $get_rak = $this->db->get_where('master_rak',array('status'=>'1'));
                  $hasil_get_rak = $get_rak->result();

                  ?>
                  <select required disabled="" name="kode_rak" id="kode_rak" class="form-control">
                    <option selected="true" value="">--Pilih Kategori--</option>
                    <?php 
                    foreach($hasil_get_rak as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_jadi->kode_rak==$daftar->kode_rak){ echo "selected"; } ?> value="<?php echo $daftar->kode_rak; ?>"><?php echo $daftar->nama_rak; ?></option>
                      <?php  
                    } ?>
                  </select>
                </div>

                <div class="form-group  col-xs-6">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Satuan Stok</label>
                  <select required disabled="" class="form-control stok select2" name="id_satuan_stok">
                    <option selected="true" value="">--Pilih satuan stok--</option>
                    <?php 
                    foreach($hasil_dft_satuan as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_jadi->id_satuan_stok==$daftar->kode){ echo "selected"; } ?> value="<?php echo $daftar->kode; ?>"><?php echo $daftar->nama; ?></option>
                      <?php 
                    } ?>
                  </select> 
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Stok Minimal</label>
                  <input required readonly type="text" class="form-control" name="stok_minimal" value="<?php echo @$hasil_bahan_jadi->stok_minimal ?>" />
                </div>
                <div class="form-group  col-xs-6">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Jenis</label>
                  <select disabled="" required class="form-control" name="jenis_bahan">
                    <option selected="true" value="">--Pilih jenis--</option>
                    <option <?php if ($hasil_bahan_jadi->jenis_bahan=='sendiri'){echo 'selected';} ?> value="sendiri">Factory</option>
                    <option  <?php if ($hasil_bahan_jadi->jenis_bahan=='plasma'){echo 'selected';} ?> value="plasma">Plasma</option>
                  </select> 
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>HPP</label>
                  <input readonly="true" required type="text" class="form-control" name="hpp" id="hpp" value="<?php echo @$hasil_bahan_jadi->hpp ?>" />
                  <h3><div id="nilai_hpp"></div></h3>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Harga Jual</label>
                  <input required readonly type="text" class="form-control" name="harga_jual" id="harga_jual" value="<?php echo @$hasil_bahan_jadi->harga_jual ?>" />
                  <h3><div id="nilai_harga_jual"></div></h3>
                </div>
               <!--  <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Isi Per Lusin</label>
                  <input required readonly type="text" class="form-control" name="harga_jual" id="harga_jual" value="<?php echo @$hasil_bahan_jadi->isi_per_lusin ?>" />
                </div> -->
               <!--  <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Tarif Borongan</label>
                  <input required readonly type="text" class="form-control" name="tarif_borongan" id="tarif_borongan" value="<?php echo @$hasil_bahan_jadi->tarif_borongan ?>" />
                  <h3><div id="nilai_tarif_borongan"></div></h3>
                </div> -->

                <div class="form-group  col-xs-12">


                </div>
                <div class="form-group  col-xs-12">
                  <div class="portlet box blue">
                    <div class="portlet-title">
                      <div class="caption">
                        Komposisi Bahan Jadi
                      </div>
                    </div>
                  </div>

                </div>
                <div class="form-group  col-xs-12">
                  <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Bahan</th>
                        <th>Jumlah</th>
                        <th>HPP</th>
                        <th>Rumus</th>
                        <th>Harga</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $get_kode = @$hasil_bahan_jadi->kode_bahan_jadi;
                      $table = $this->uri->segment(3);

                      $this->db->where('kode_bahan_jadi', $get_kode);
                      if($table == 'table_temp'){
                        $get_temp = $this->db->get('opsi_master_bahan_jadi_temp');
                      } else{
                        $get_temp = $this->db->get('opsi_master_bahan_jadi');
                      }
                      $hasil_temp = $get_temp->result();
                      $total = 0;
                      $no = 1;
                      foreach ($hasil_temp as $temp) {
                        ?>
                        <tr>
                          <td><?php echo $no ?></td>
                          <td><?php echo $temp->nama_bahan ?></td>
                          <td align="center"><?php echo $temp->jumlah.' '.$temp->nama_satuan ?></td>
                          <td align="right"><?php echo format_rupiah($temp->hpp) ?></td>
                          <td align="center"><?php echo $temp->rumus." %"; ?></td>
                          <td align="right"><?php echo format_rupiah($temp->harga) ?></td>
                        </tr>
                        <?php 
                        $total += $temp->harga;
                        $no++;
                      }
                      ?>
                      <tr>
                        <td align="right" colspan="5">Total</td>
                        <td align="right" colspan="1"><?php echo format_rupiah($total); ?>
                          <input type="hidden" id="total" value="<?php echo $total; ?>" />
                        </td>

                      </tr>
                      <?php 
                      $tambahan = $this->db->get('master_tambahan');
                      $hasil_tambahan = $tambahan->result();
                      $total_tambahan = 0;
                      foreach($hasil_tambahan as $daftar){
                        ?>
                        <tr>
                          <td align="right" colspan="5"><?php echo $daftar->item; ?></td>
                          <td align="right" colspan="1"><?php echo format_rupiah($daftar->harga); ?></td>
                        </tr>
                        <?php $total_tambahan += $daftar->harga; } ?>
                        <tr>
                          <td align="right" colspan="5">Sales Komisi</td>
                          <td align="right" colspan="1"><?php echo format_rupiah($total * 10 / 100); ?></td>
                        </tr>
                        <tr>
                          <td align="right" colspan="5">Total HPP</td>

                          <td align="right" colspan="1">
                            <?php if($total !=0){ ?>
                            <?php echo format_rupiah(($total * 10 / 100) + ($total + $total_tambahan) ); ?>
                            <input type="hidden" value="<?php echo ($total * 10 / 100) + ($total + $total_tambahan); ?>" id="total_hpp" />
                            <?php }else{ ?>
                            <input type="hidden" value="0" id="total_hpp" />
                            <?php } ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="form-group  col-xs-12">
                    <div class="portlet box blue">
                      <div class="portlet-title">
                        <div class="caption">
                          Harga Bahan Jadi 
                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="form-group  col-xs-12">
                    <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kategori Member</th>
                          <th>Qty Min </th>
                          <th>Qty Max </th>
                          <th>Harga</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $get_kode = @$hasil_bahan_jadi->kode_bahan_jadi;
                        $table = $this->uri->segment(3);

                        $this->db->where('kode_bahan_jadi', $get_kode);
                        if($table == 'table_temp_harga'){
                          $get_temp = $this->db->get('opsi_harga_bahan_jadi_temp');
                        } else{
                          $get_temp = $this->db->get('opsi_harga_bahan_jadi');
                        }
                        $hasil_temp = $get_temp->result();
                        $total = 0;
                        $no = 1;
                        foreach ($hasil_temp as $temp) {
                          ?>
                          <tr>
                            <td><?php echo $no++;?></td>
                            <td><?php echo $temp->kategori ?></td>
                            <td><?php echo $temp->qty_min ?></td>
                            <td><?php echo $temp->qty_max ?></td>
                            <td><?php echo format_rupiah($temp->harga);?></td>

                          </tr>

                          <?php }
                          ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="box-footer">
                    <a href="<?php echo base_url().'master/bahan_jadi/' ?>" class="btn btn-lg green-seagreen"><i class="fa fa-save"></i> OK</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!------------------------------------------------------------------------------------------------------>
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      function get_temp(){
        kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
        $("#opsi_bahan_jadi").load('<?php echo base_url().'master/bahan_jadi/table_temp/'; ?>'+kode_bahan_jadi);
      }
      function get_opsi(){
        kode_bahan_jadi = $("#kode_bahan_jadi").val();
        $("#opsi_bahan_jadi").load('<?php echo base_url().'master/bahan_jadi/table_opsi/'; ?>'+kode_bahan_jadi);
      }
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'master/bahan_jadi/'; ?>?"+window.location.search.substring(1);
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){

        $(".select2").select2();
        <?php 
        if(!empty($param)){ ?>
          get_opsi();
          <?php 
        }else{ ?>
          get_temp();
          <?php } ?>
        });
      $(function(){
        $('#kode_bahan_jadi').on('change',function(){
          var kode_input = $('#kode_bahan_jadi').val();
          var kode_setting = $('#kode_setting').val();
          var kode_bahan_jadi = kode_setting + "_" + kode_input ;
          var url = "<?php echo base_url() . 'master/bahan_jadi/get_kode' ?>";
          $.ajax({
            type: 'POST',
            url: url,
            data: {kode_bahan_jadi:kode_bahan_jadi},
            success: function(msg){
              if(msg == 1){
                $(".sukses").html('<div class="alert alert-warning">Kode Telah Dipakai</div>');
                setTimeout(function(){
                  $('.sukses').html('');
                },1700);              
                $('#kode_bahan_jadi').val('');

              }
              else{

              }
            }
          });
        });

        $("#jenis_bahan").change(function(){
          $.ajax({
            type: 'POST',
            url: "<?php echo base_url() . 'master/bahan_jadi/get_bahan' ?>",
            data: {jenis_bahan:$(this).val()},
            success: function(data){
              $("#bahan").html(data);
            },
            error : function(data) {  
              alert('Sorry');
            }  
          });
        });

        $("#bahan").change(function(){
          jenis_bahan = $("#jenis_bahan").val();
          $.ajax({
            type: 'POST',
            url: "<?php echo base_url() . 'master/bahan_jadi/get_satuan' ?>",
            dataType: 'json',
            data: {kode_bahan_baku:$(this).val(), jenis_bahan:jenis_bahan},
            success: function(data){
              $("#id_satuan").val(data.id_satuan_stok);
              $("#nama_satuan").val(data.satuan_stok);
              $("#hpp_bb").val(data.hpp);
              var hpp = $("#hpp_bb").val();
              var rumus = $("#rumus").val();
              var harga = (hpp*rumus/100)+parseInt(hpp);
              $("#harga").val(harga);
            },
            error : function(data) {  
              alert('Sorry');
            }  
          });
        });

        $("#rumus").keyup(function(){
          var hpp = $("#hpp_bb").val();
          var rumus = $("#rumus").val();
          var harga = (hpp*rumus/100)+parseInt(hpp);
          $("#harga").val(harga);
        });

        $("#add-item").click(function(){
          <?php 
          if(!empty($param)){ ?>
            kode_bahan_jadi = $("#kode_bahan_jadi").val();
            <?php 
          } else { ?>
            kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
            <?php
          } ?>
          jenis_bahan = $("#jenis_bahan").val();
          bahan_baku = $("#bahan").val();
          jumlah = $("#jumlah").val();
          id_satuan = $("#id_satuan").val();
          satuan = $("#nama_satuan").val();
          var hpp = $("#hpp_bb").val();
          var rumus = $("#rumus").val();
          var harga = $("#harga").val();
          $.ajax({
            type: 'POST',
            <?php 
            if(!empty($param)){ ?>
              url: "<?php echo base_url() . 'master/bahan_jadi/tambah_opsi' ?>",
              <?php 
            } else { ?>
              url: "<?php echo base_url() . 'master/bahan_jadi/tambah_temp' ?>",
              <?php
            } ?>
            data: {kode_bahan_jadi:kode_bahan_jadi, jenis_bahan:jenis_bahan, bahan_baku:bahan_baku, 
              jumlah:jumlah, id_satuan:id_satuan, satuan:satuan,hpp:hpp,rumus:rumus,harga:harga},
              success: function(data){
                $("#bahan").val('');
                $("#jumlah").val('');
                $("#id_satuan").val('');
                $("#nama_satuan").val('');
                $("#hpp_bb").val('');
                $("#harga").val('');
                <?php 
                if(!empty($param)){ ?>
                  get_opsi();
                  <?php 
                } else { ?>
                  get_temp();
                  <?php
                } ?>
              },
              error : function(data) {  
                alert('Sorry');
              }  
            });
        });

        $("#hpp").keyup(function(){
          var temp_data = "<?php echo base_url().'master/bahan_jadi/get_rupiah/'?>";
          var hpp = $('#hpp').val() ;
          $.ajax({
            type: "POST",
            url: temp_data,
            data: {hpp:hpp},
            success: function(hasil) {              
              $("#nilai_hpp").html(hasil);
            }
          });

        });
        $("#harga_jual").keyup(function(){
          var temp_data = "<?php echo base_url().'master/bahan_jadi/get_rupiah/'?>";
          var hpp = $('#harga_jual').val() ;
          $.ajax({
            type: "POST",
            url: temp_data,
            data: {hpp:hpp},
            success: function(hasil) {              
              $("#nilai_harga_jual").html(hasil);
            }
          });

        });

        $("#data_form").submit( function() {
          <?php if(!empty($param)){ ?>
            var url = "<?php echo base_url(). 'master/bahan_jadi/simpan_edit'; ?>";  
            <?php 
          }else{ ?>
            var url = "<?php echo base_url(). 'master/bahan_jadi/simpan'; ?>";
            <?php 
          } ?>
          $.ajax( {
            type:"POST", 
            url : url,  
            cache :false,  
            dataType : 'json',
            data :$(this).serialize(),

            beforeSend:function(){
              $(".tunggu").show();  
            },
            success : function(data) {
              if(data.hasil=="opsi"){
                $(".sukses").html('<div class="alert alert-danger">Opsi Tidak Ada!!!</div>');
                setTimeout(function(){$('.sukses').html('');},1000);
                $(".tunggu").hide();
              }
              else{
                $(".sukses").html('<div class="alert alert-success">Data Berhasil Disimpan</div>');
                setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/bahan_jadi/' ?>";},1000);  
              }
              $(".loading").hide();   

            },  
            error : function(data) {  
              $(".sukses").html('<div class="alert alert-success">Data Gagal Disimpan</div>');
            }  
          });
          return false;                    
        }); 
      })
    </script>