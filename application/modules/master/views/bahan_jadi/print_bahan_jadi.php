<!DOCTYPE html>
<html>
<head>
	<title>Print Bahan Jadi</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
<table  >
    <tr>
      <td>
    <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
    </td>
    
    </tr>
  
  </table>
<hr>
<table class="" width="100%" border="0" style="border-collapse: collapse;">

  <tr>
    
    <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Data Bahan Jadi</th>
  </tr>
</table><br>
<table class="" width="100%" border="1" style="border-collapse: collapse;">

 <?php
  $kode_default = $this->db->get('setting_gudang');
  $hasil_unit =$kode_default->row();
  $param=$hasil_unit->kode_unit;
 
if(@$nama_produk){
  $this->db->like('nama_bahan_jadi',$nama_produk,'both');
}
  $this->db->order_by('nama_bahan_jadi','ASC');
  $bahan_jadi = $this->db->get_where('master_bahan_jadi',array('kode_unit' => $param, 'status' => 'sendiri'));
  $hasil_bahan_jadi = $bahan_jadi->result();
  ?>

  <thead>
    <tr width="100%">
      <th>No</th>
      <th>Kode Bahan</th>
      <th>Nama Bahan Jadi</th>

      <th>Unit</th>
      <th>Kategori</th>
      <th>Satuan Stok</th>
      <th>Stok Minimal</th>
      <th>HPP</th>
     
    </tr>
  </thead>
  <tbody>
    <?php
    $nomor=1;
    foreach($hasil_bahan_jadi as $daftar){
      ?>
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo $daftar->kode_bahan_jadi; ?></td>
        <td><?php echo $daftar->nama_bahan_jadi; ?></td>

        <td><?php echo $daftar->nama_unit; ?></td>
        <td><?php echo $daftar->nama_rak; ?></td>
        <td><?php echo $daftar->satuan_stok; ?></td>
        <td><?php echo $daftar->stok_minimal; ?></td>
        <td><?php echo format_rupiah($daftar->hpp); ?></td>
       
      </tr>
      <?php $nomor++; 
    } ?>
  </tbody>
  
</table>



</html>