<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stok extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['halaman'] = $this->load->view('setting/menu_stok', NULL, TRUE);
		$this->load->view ('main', $data);
	}

	public function stok_member()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_member/daftar_stok_member', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_member/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}
	public function stok_retur()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_retur/daftar', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_retur/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}
	public function tambah_stok_retur()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_retur/tambah', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_retur/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}

	//==retur==//

	public function get_retur(){

		$kode_penjualan =	$_POST['kode_penjualan'] ;

		$result 	=	$this->db->get_where('transaksi_penjualan' , [ 'kode_penjualan' => $kode_penjualan])->result();

		echo json_encode($result);
	}

	public function get_detail_penjualan($kode_penjualan){

		$result 	=	$this->db->get_where('opsi_transaksi_penjualan' , [ 'kode_penjualan' => $kode_penjualan])->result();

		$no =	1;
		foreach ($result as $val) {


			$jumlah_retur  = $val->jumlah_retur == null ? 0 : $val->jumlah_retur;

			echo "<tr>";
			 echo "<td>".$no++."</td>";
			 echo "<td>".$val->nama_menu."</td>";
			 echo "<td>".$val->jumlah." ".$val->nama_satuan."</td>";
			 echo "<td> <input type='number' name='".$val->id."' value=".$jumlah_retur." ></td>";
			echo "</tr>";

		}

	}



	public function detail_stok_retur()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_retur/detail', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_retur/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}
	public function repair()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_retur/repair', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_retur/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}

	public function detail_tranksaki_stok_member()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_member/detail_transaksi', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_member/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}

	public function detail_stok_member()
	{
		$data['aktif'] = 'stok';
		$data['konten'] = $this->load->view('stok_member/detail_stok_member', $data, TRUE);
		$data['halaman'] = $this->load->view('stok_member/menu', $data, TRUE);
		$this->load->view ('main', $data);

	}

	public function cari_member()
	{
		$this->load->view ('stok_member/cari_member');
	}

	public function cari_detail_transaksi()
	{
		$this->load->view ('stok_member/cari_detail_transaksi');
	}

	public function list_stock(){
		$kode_unit = $this->input->post('kode_unit');
		$kode_rak = $this->input->post('kode_rak');

		$get_stok = $this->db->query("SELECT * from master_bahan_baku where kode_rak='$kode_rak'");

		#echo $this->db->last_query();
		$table = '';
		foreach ($get_stok->result() as $key => $value) {
			$kode_bahan = $value->kode_bahan_baku;
			$this->db->select('*, min(id) id');
			$get_kode_bahan = $this->db->get_where('transaksi_stok',array('kode_bahan'=>$kode_bahan));
			$hasil_hpp_bahan = $get_kode_bahan->row();
			$get_stok_min = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan));
			$hasil_stok_min = $get_stok_min->row();
			if ($value->real_stock <= $hasil_stok_min->stok_minimal){
				$table.='<tr class="danger">';
			}else{
				$table.='<tr>';
			}
			$table .=
			'<td>'.$value->kode_bahan_baku.'</td>
			<td>'.$value->nama_bahan_baku.'</td>
			<td>'.$value->kode_rak.'</td>
			<td>'.$value->nama_rak.'</td>
			<td>'.$value->real_stock.'</td>
			<td>'.$value->satuan_stok.'</td>
			<td>'.format_rupiah($hasil_hpp_bahan->hpp).'</td>
			<td>'.format_rupiah($hasil_hpp_bahan->hpp * $value->real_stock).'</td>
			<td>'.get_detail_stok($kode_unit,$value->kode_rak,$value->kode_bahan_baku).'</td>
		</tr>';
	}
	echo $table;
}

public function get_jenis_filter()
{
	$kategori_filter = $this->input->post('kategori_filter');

	if($kategori_filter=='kategori'){
		$jenis_filter = $this->db->get('master_kategori_menu');
		$hasil_jenis_filter = $jenis_filter->result();
		echo "<option value=''>Pilih Kategori Produk</option>";
		foreach ($hasil_jenis_filter as  $value) {
			echo "<option value=".$value->kode_kategori_menu.">".$value->nama_kategori_menu."</option>";
		}

	}elseif($kategori_filter=='blok'){
		$jenis_filter = $this->db->get('master_rak');
		$hasil_jenis_filter = $jenis_filter->result();
		echo "<option value=''>Pilih Blok</option>";
		foreach ($hasil_jenis_filter as  $value) {
			echo "<option value=".$value->kode_rak.">".$value->nama_rak."</option>";
		}
	}


}

public function cari_stok(){

	$this->load->view('stok/daftar_stok/cari_stok');

}

public function simpan_po_stokmin_temp()
{
	$input=$this->input->post('bahan_stokmin');
	$this->db->select_max('id');
	$get_max_po = $this->db->get('transaksi_po');
	$max_po = $get_max_po->row();

	$this->db->where('id', $max_po->id);
	$get_po = $this->db->get('transaksi_po');
	$po = $get_po->row();
	$tahun = substr(@$po->kode_transaksi, 3,4);
	if(date('Y')==$tahun){
		$nomor = substr(@$po->kode_transaksi, 8);
		$nomor = $nomor + 1;
		$string = strlen($nomor);
		if($string == 1){
			$kode_trans = 'PO_'.date('Y').'_00000'.$nomor;
		} else if($string == 2){
			$kode_trans = 'PO_'.date('Y').'_0000'.$nomor;
		} else if($string == 3){
			$kode_trans = 'PO_'.date('Y').'_000'.$nomor;
		} else if($string == 4){
			$kode_trans = 'PO_'.date('Y').'_00'.$nomor;
		} else if($string == 5){
			$kode_trans = 'PO_'.date('Y').'_0'.$nomor;
		} else if($string == 6){
			$kode_trans = 'PO_'.date('Y').'_'.$nomor;
		}
	} else {
		$kode_trans = 'PO_'.date('Y').'_000001';
	}

	// $tgl = date("Y-m-d");
	// $no_belakang = 0;
	// $this->db->select_max('kode_po');
	// $kode = $this->db->get_where('transaksi_po',array('tanggal_input'=>$tgl));
	// $hasil_kode = $kode->row();

	// $this->db->select('kode_po');
	// $get_kode_ro = $this->db->get('master_setting');
	// $hasil_kode_ro = $get_kode_ro->row();

	// if(count($hasil_kode)==0){
	// 	$no_belakang = 1;
	// }
	// else{
	// $kode_default = $this->db->get('setting_gudang');
	// $hasil_unit =$kode_default->row();
	// $param=$hasil_unit->kode_unit;
	// $ko_ro_baru = @$hasil_kode_ro->kode_po."_".date("dmyHis")."_".$param."_".$no_belakang;
	// 	$pecah_kode = explode("_",$hasil_kode->kode_po);
	// 	$no_belakang = @$pecah_kode[3]+1;
	// }

	foreach ($input as $value) {
			//echo $value;
		$bahan=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$value));
		$hasil_bahan=$bahan->row();
		$data['kode_po']=$kode_trans;
		$data['kode_bahan']=$hasil_bahan->kode_bahan_baku;
		$data['nama_bahan']=$hasil_bahan->nama_bahan_baku;
		$data['kategori_bahan']=$hasil_bahan->jenis_bahan;
		$data['position']=$hasil_bahan->kode_unit;
		$data['jumlah']=0;

		$this->db->insert('opsi_transaksi_po_temp',$data);

	}
}

public function daftar()
{
	$data['halaman'] = $this->load->view('setting/daftar', NULL, TRUE);
	$this->load->view ('main', $data);
}

public function daftar_stok_minimal()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/daftar_stok_minimal', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu_stok_minimal', $data, TRUE);
	$this->load->view ('main', $data);
}
public function daftar_stok()
{
	$data['aktif'] = 'stok';
	$data['nama_bahan'] = $this->input->get('nama_bahan');
	$data['nama_kategori'] = $this->input->get('nama_kategori');
	$data['konten'] = $this->load->view('daftar_stok/daftar', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu', $data, TRUE);
	$this->load->view ('main', $data);
}
public function print_daftar()
{
	$data['nama_bahan'] = $this->input->get('nama_bahan');
	$data['nama_kategori'] = $this->input->get('nama_kategori');
	$this->load->view('daftar_stok/print_daftar', $data);
}
public function print_stok_min()
{

	$this->load->view('daftar_stok/print_stok_min');
}
public function bahan_jadi()
{
	$data['aktif'] = 'stok';
	$data['nama_bahan'] = $this->input->get('nama_bahan');
	$data['nama_kategori'] = $this->input->get('nama_kategori');
	$data['konten'] = $this->load->view('bahan_jadi/daftar', $data, TRUE);
	$data['halaman'] = $this->load->view('bahan_jadi/menu', $data, TRUE);
	$this->load->view ('main', $data);
}
public function print_daftar_j()
{
	$data['nama_bahan'] = $this->input->get('nama_bahan');
	$data['nama_kategori'] = $this->input->get('nama_kategori');
	$this->load->view('bahan_jadi/print_daftar', $data);
}
public function bahan_setengah_jadi()
{
	$data['aktif'] = 'stok';
	$data['nama_bahan'] = $this->input->get('nama_bahan');
	$data['nama_kategori'] = $this->input->get('nama_kategori');
	$data['konten'] = $this->load->view('bahan_setengah_jadi/daftar', $data, TRUE);
	$data['halaman'] = $this->load->view('bahan_setengah_jadi/menu', $data, TRUE);
	$this->load->view ('main', $data);
}
public function print_daftar_sj()
{
	$data['nama_bahan'] = $this->input->get('nama_bahan');
	$data['nama_kategori'] = $this->input->get('nama_kategori');
	$this->load->view('bahan_setengah_jadi/print_daftar', $data);
}
public function plasma()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/daftar_stok_plasma', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu_plasma', $data, TRUE);
	$this->load->view ('main', $data);
}

public function daftar_plasma()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/daftar_plasma', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu_plasma', $data, TRUE);
	$this->load->view ('main', $data);
}

public function detail_stok_plasma()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/detail_stok_plasma', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu_plasma', $data, TRUE);
	$this->load->view ('main', $data);
}

public function get_stok_plasma(){
	$this->load->view('daftar_stok/tabel_stok_plasma');
}


public function daftar_menu()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('setting/daftar', $data, TRUE);
	$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
	$this->load->view ('main', $data);
}

public function produk()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/daftar_produk', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu_produk', $data, TRUE);
	$this->load->view ('main', $data);
}

public function tambah()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/form', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu', $data, TRUE);
	$this->load->view('main', $data);
}

public function ubah()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/form', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu', $data, TRUE);
	$this->load->view('main', $data);
}

public function detail()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/detail_stok_gudang', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu', $data, TRUE);
	$this->load->view('main', $data);
}

public function detail_bahan_jadi()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('bahan_jadi/detail_stok_gudang', $data, TRUE);
	$data['halaman'] = $this->load->view('bahan_jadi/menu', $data, TRUE);
	$this->load->view('main', $data);
}
public function detail_bahan_setengah_jadi()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('bahan_setengah_jadi/detail_stok_gudang', $data, TRUE);
	$data['halaman'] = $this->load->view('bahan_setengah_jadi/menu', $data, TRUE);
	$this->load->view('main', $data);
}
public function detail_produk()
{
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/detail_produk', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu', $data, TRUE);
	$this->load->view('main', $data);
}


public function hapus(){
	$kode = $this->input->post("key");
	$this->db->delete( 'master_stok', array('kode_stok' => $kode) );
	echo '<div class="alert alert-success">Sudah dihapus.</div>';

}

public function detail_stok_gudang(){
	$data['aktif'] = 'stok';
	$data['konten'] = $this->load->view('daftar_stok/detail_stok_gudang', $data, TRUE);
	$data['halaman'] = $this->load->view('daftar_stok/menu', $data, TRUE);
	$this->load->view('main', $data);
}
public function get_bahan()
{
	$param = $this->input->post();
	$kode_unit = $this->uri->segment(4);
        //echo $kode_unit;
	$jenis = $param['jenis_bahan'];

	if($jenis == 'bahan baku'){
		$opt = '';
		$query = $this->db->get_where('master_bahan_baku',array('kode_unit' => $kode_unit));
		$opt = '<option value="">--Pilih Bahan Baku--</option>';
		foreach ($query->result() as $key => $value) {
			$opt .= '<option value="'.$value->kode_bahan_baku.'">'.$value->nama_bahan_baku.'</option>';
		}
		echo $opt;

	}else if ($jenis == 'bahan jadi') {
		$opt = '';
		$query = $this->db->get_where('master_bahan_jadi',array('kode_unit' => $kode_unit));
		$opt = '<option value="">--Pilih Bahan Jadi--</option>';
		foreach ($query->result() as $key => $value) {
			$opt .= '<option value="'.$value->kode_bahan_jadi.'">'.$value->nama_bahan_jadi.'</option>';
		}
		echo $opt;
	}
}
public function get_temp_pembelian(){
	$id = $this->input->post('id');
	$pembelian = $this->db->get_where('opsi_transaksi_pembelian_temp',array('id'=>$id));
	$hasil_pembelian = $pembelian->row();
	echo json_encode($hasil_pembelian);
}

public function simpan_tambah()

{
	$data['kode_stok'] = $this->input->post("kode_stok");
	$data['nama_stok'] = $this->input->post("nama_stok");
	$data['kode_supplier'] = $this->input->post("supplier");
	$parameter = $this->input->post("supplier");
	$get_master_supplier = $this->db->get_where('master_supplier', array('kode_supplier' => $parameter));
	$hasil_master_supplier = $get_master_supplier->row();
	$item = $hasil_master_supplier;
	$data['nama_supplier'] = $item->nama_supplier;

	$data['stok'] = $this->input->post("stok");
	$data['position'] = $this->input->post("position");

	$insert = $this->db->insert("master_stok", $data);
	echo '<div class="alert alert-success">Sudah tersimpan.</div>';
	$this->session->set_flashdata('message', $data['kode_stok']);
}

public function simpan_ubah()

{
	$data['kode_stok'] = $this->input->post("kode_stok");
	$data['nama_stok'] = $this->input->post("nama_stok");
	$data['kode_supplier'] = $this->input->post("supplier");
	$parameter = $this->input->post("supplier");
	$get_master_supplier = $this->db->get_where('master_supplier', array('kode_supplier' => $parameter));
	$hasil_master_supplier = $get_master_supplier->row();
	$item = $hasil_master_supplier;
	$data['nama_supplier'] = $item->nama_supplier;

	$data['stok'] = $this->input->post("stok");
	$data['position'] = $this->input->post("position");

	$update = $this->db->update("master_stok", $data, array('kode_stok' => $data['kode_stok']));
	echo '<div class="alert alert-success">Sudah tersimpan.</div>';
	$this->session->set_flashdata('message', $data['kode_stok']);
}
public function simpan_repair()

{
	$kode_repair = $this->input->post("kode_repair");
	$kode_retur = $this->input->post("kode_retur");
	$kode_produk = $this->input->post("kode_produk");
	$kode_bahan = $this->input->post("kode_bahan");
	$status = $this->input->post("status");

	$user = $this->session->userdata('astrosession');
	$id_user=$user->id;
	$nama_user=$user->uname;

	$get_produk = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_produk,'status'=>'sendiri'));
	$hasil_produk = $get_produk->row();

	$bahan = implode(",", $kode_bahan);

	$kode_bahan=explode(",", $bahan);

	foreach ($kode_bahan as $opsi_bahan) {

		if(!empty($opsi_bahan)){
			$get_opsi = $this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$kode_produk,'kode_bahan'=>$opsi_bahan));
			$hasil_opsi = $get_opsi->row();

			if($hasil_opsi->jenis_bahan='bahan baku'){

				$get_bahan_baku = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$opsi_bahan,'status'=>'sendiri'));
				$hasil_bahan_baku = $get_bahan_baku->row();

				$bahan_baku['real_stock']=$hasil_bahan_baku->real_stock - $hasil_opsi->jumlah;
				$this->db->update("master_bahan_baku", $bahan_baku, array('kode_bahan_baku'=>$opsi_bahan,'status'=>'sendiri'));

			}else{
				$get_bahan_setangah = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$opsi_bahan,'status'=>'sendiri'));
				$hasil_bahan_setangah = $get_bahan_setangah->row();

				$bahan_setangah['real_stock']=$hasil_bahan_setangah->real_stock - $hasil_opsi->jumlah;
				$this->db->update("master_bahan_setengah_jadi", $bahan_setangah, array('kode_bahan_setengah_jadi'=>$opsi_bahan,'status'=>'sendiri'));

			}
			$opsi_repair['kode_repair']=$kode_repair;
			$opsi_repair['tanggal_repair']=date('Y-m-d');
			$opsi_repair['kode_produk']=$kode_produk;
			$opsi_repair['nama_produk']=$hasil_produk->nama_bahan_jadi;
			$opsi_repair['kode_bahan']=$opsi_bahan;
			$opsi_repair['nama_bahan']=$hasil_opsi->nama_bahan;
			$opsi_repair['jumlah']=$hasil_opsi->jumlah;
			$opsi_repair['jenis_bahan']=$hasil_opsi->jenis_bahan;

			$this->db->insert("opsi_transaksi_repair", $opsi_repair);

			$stok_bahan['jenis_transaksi']='repair';
			$stok_bahan['kode_transaksi']=$kode_repair;
			$stok_bahan['tanggal_transaksi']=date('Y-m-d');
			$stok_bahan['kode_bahan']=$opsi_bahan;
			$stok_bahan['nama_bahan']=$hasil_opsi->nama_bahan;
			$stok_bahan['stok_keluar']=$hasil_opsi->jumlah;
			$stok_bahan['hpp']=$hasil_opsi->hpp;
			$stok_bahan['kategori_bahan']=$hasil_opsi->jenis_bahan;
			$stok_bahan['status']='keluar';
			$stok_bahan['id_petugas']=$id_user;
			$stok_bahan['nama_petugas']=$nama_user;

			$this->db->insert("transaksi_stok", $stok_bahan);


		}
	}


	if($status=='selesai'){
		$stok_produk['real_stock']=$hasil_produk->real_stock + 1;
		$this->db->update("master_bahan_jadi", $stok_produk, array('kode_bahan_jadi'=>$kode_produk,'status'=>'sendiri'));
	}

	$produk_transaksi['jenis_transaksi']='repair';
	$produk_transaksi['kode_transaksi']=$kode_repair;
	$produk_transaksi['tanggal_transaksi']=date('Y-m-d');
	$produk_transaksi['kode_bahan']=$kode_produk;
	$produk_transaksi['nama_bahan']=$hasil_produk->nama_bahan_jadi;
	$produk_transaksi['stok_masuk']=1;
	$produk_transaksi['hpp']=$hasil_produk->hpp;
	$produk_transaksi['kategori_bahan']='Bahan Jadi';
	$produk_transaksi['status']='masuk';
	$produk_transaksi['posisi_akhir']='gudang';
	$produk_transaksi['id_petugas']=$id_user;
	$produk_transaksi['nama_petugas']=$nama_user;

	$this->db->insert("transaksi_stok", $produk_transaksi);

	$data_repair['status']=$status;
	$data_repair['tanggal_repair']=date('Y-m-d');
	$data_repair['id_petugas']=$id_user;
	$data_repair['nama_petugas']=$nama_user;
	$update = $this->db->update("transaksi_stok_repair", $data_repair, array('kode_repair' => $kode_repair,'kode_retur' => $kode_retur));
	echo '<div class="alert alert-success">Sudah tersimpan.</div>';

}

	public function save_opname_plasma() {
		$data = $this->input->post();
		if (!$data) {
			echo json_encode(['error'=> true, 'msg' => 'Tidak ada data']);
			exit();
		}

		if (!$data['id'] || !$data['jenis_bahan'] || !$data['kode_plasma']) {
			echo json_encode(['error'=> true, 'msg' => 'Data Tidak Lengkap']);
			exit();
		}

		$dtUpdate['real_stock'] = intval(@$data['stock']);
		$dtUpdate['afkir'] = intval(@$data['afkir']);
		$dtWhere['id'] = $data['id'];
		$dtWhere['status'] = 'plasma';
		$dtWhere['kode_plasma'] = $data['kode_plasma'];
		$dtWhere['nama_unit'] = 'Gudang';

		if ($data['jenis_bahan'] == 'baku') {
			$save = $this->db->update('master_bahan_baku', $dtUpdate, $dtWhere);
		} else if ($data['jenis_bahan'] == 'setengah') {
			$save = $this->db->update('master_bahan_setengah_jadi', $dtUpdate, $dtWhere);
		}
		if (@$save) {
			echo json_encode(['error' => false, 'msg' => 'Berhasil Menyimpan Data']);
			exit();
		} else {
			echo json_encode(['error' => true, 'msg' => 'Gagal Menyimpan Data']);
			exit();
		}
	}
}
