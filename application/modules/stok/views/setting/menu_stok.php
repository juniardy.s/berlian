
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    
                        <!-- END STYLE CUSTOMIZER -->
                        <!-- BEGIN PAGE HEADER-->
                        <h3 class="page-title">
                          Stok</h3>
                          <div class="page-bar">
                            <ul class="page-breadcrumb">
                              <li>
                                <i class="fa fa-home"></i>
                                <a href="#">Home</a>
                                <i class="fa fa-angle-right"></i>
                              </li>
                              <li>
                                <a href="#">Stok</a>
                              </li>
                            </ul>
                            <div class="page-toolbar">
                              <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>
                                &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light blue-soft" id="bahan_baku">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-tasks" ></i>
                                </div>
                                <div class="details" >
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Gudang
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light red-soft"  id="produk">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-shopping-cart"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Plasma
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light green-soft"  id="mutasi">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-retweet"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Mutasi
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-light purple-soft"  id="stok_retur">
                                <div class="visual">
                                  <i class="glyphicon glyphicon-retweet"></i>
                                </div>
                                <div class="details">
                                  <div class="number">

                                  </div>
                                  <div class="desc">
                                    Stok Retur
                                  </div>
                                </div>
                              </a>
                            </div>
                            
                            </div>
                          <!-- END DASHBOARD STATS -->
                          <div class="clearfix">
                          </div>

                        </div>
                        <script type="text/javascript">
                          $(document).ready(function(){


                            $("#bahan_baku").click(function(){
                              window.location = "<?php echo base_url() . 'stok/daftar_menu' ?>";

                            });

                            $("#produk").click(function(){
                              window.location = "<?php echo base_url() . 'stok/daftar_plasma' ?>";
                            });

                            $("#stok").click(function(){
                              window.location = "<?php echo base_url() . 'stok/daftar_stok_minimal' ?>";
                            });

                            $("#serve").click(function(){
                              window.location = "<?php echo base_url() . 'stok/serve' ?>";
                            });

                            $("#mutasi").click(function(){
                              window.location = "<?php echo base_url() . 'mutasi/daftar_mutasi' ?>";
                            });
                            
                            $("#stok_retur").click(function(){
                              window.location = "<?php echo base_url() . 'stok/stok_retur' ?>";
                            });


                          });
                        </script>
