<?php 
$get_position = $this->uri->segment(2);
$position = ucwords($get_position);
?>
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
   
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Stok Plasma 
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
               <th>No.</th>
              <th>Kode Bahan</th>
              <th>Nama Bahan</th>
              <th>Nama Rak</th>
              <th align="right">Real Stok</th>
              <th align="right">Stok Min</th>
              <th>HPP</th>
              <th>Aset</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              <?php
              
              $get_master_barang = $this->db->get_where('master_bahan_baku',array('nama_unit'=>'Gudang'));
              $hasil_master_barang = $get_master_barang->result();

              $no = 1;
              foreach($hasil_master_barang as $item){
                if($this->session->flashdata('message')==$item->kode_bahan_baku){

                  echo '<tr id="warna" style="background: #88cc99; display: none;">';
                }
                else{
                  echo '<tr>';
                }
                ?>
                <td><?php echo $no;?></td>
                <td><?php echo $item->kode_bahan_baku; ?></td>                  
                <td><?php echo $item->nama_bahan_baku; ?></td>   
                <td><?php echo $item->nama_rak; ?></td>
                <td><?php echo $item->real_stock; ?></td>   
                <td><?php echo $item->stok_minimal." ",$item->satuan_stok; ?></td>                
                <td><?php 
                $kode_bahan = $item->kode_bahan_baku; 
              $this->db->select_max('id');                       
              $get_kode_bahan = $this->db->get_where('transaksi_stok',array('kode_bahan'=>$kode_bahan,'jenis_transaksi'=>'pembelian'));
              $hasil_hpp_bahan = $get_kode_bahan->row();
              #echo $this->db->last_query();

              $get_hpp = $this->db->get_where('transaksi_stok',array('id'=>$hasil_hpp_bahan->id));
              $hasil_get_hpp = $get_hpp->row();

              $get_stok_min = $this->db->get_where('master_bahan_baku',array('id'=>$item->id));
              $hasil_stok_min = $get_stok_min->row();
                  echo format_rupiah(@$hasil_get_hpp->hpp);
                ?>
                </td>
                <td>
                <?php echo format_rupiah((@$item->real_stock <= 0) ? (@$hasil_get_hpp->hpp * 0) : (@$hasil_get_hpp->hpp * $item->real_stock));?>
                  
                </td>
               
               
                
                <td align="center"><?php echo get_detail($item->kode_bahan_baku); ?></td>
              </tr>
              <?php
              $no++;
            } ?>
          </tbody>                
        </table>


      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
</div>    
</div>  
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
            window.location = "<?php echo base_url().'stok/daftar_menu'; ?>";
          });
        </script>

<script>
  $(document).ready(function() {
    $("#tabel_daftar").dataTable({
    "paging":   false,
    "ordering": false,
    "info":     false
  });
  } );
    $("#gudang").click(function(){
      $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/gudang' ?>";

                            });

                            $("#bar").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/bar' ?>";
                            });

                            $("#kitchen").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/kitchen' ?>";
                            });

                            $("#serve").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/server' ?>";
                            });

    setTimeout(function(){
      $("#warna").fadeIn('slow');
    }, 1000);
    $("a#hapus").click( function() {    
      var r =confirm("Anda yakin ingin menghapus data ini ?");
      if (r==true)  
      {
        $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'master/barang/hapus' ?>",  
          cache :false,  
          data :({key:$(this).attr('key')}),
          beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
            $(".sukses").html(data);   
            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/barang/daftar' ?>";},1500);              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
        return false;
      }
      else {}        
    });

                           
  
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);

</script>

