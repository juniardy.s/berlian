

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
    .ombo{
      width: 400px;
    } 

  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Detail Stok Retur
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>

            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>


            <div class="box-body">            
              <div class="sukses" ></div>
              <form id="data_form" action="" method="post">
                <div class="box-body">
                  <div class="row">
                   <?php
                   $kode_retur = $this->uri->segment(4);
                   $kode_produk = $this->uri->segment(3);
                   $get_produk = $this->db->get_where('transaksi_stok_repair',array('kode_retur'=>$kode_retur,'kode_produk'=>$kode_produk));
                   $list_get_produk = $get_produk->row();
                   ?>

                   <div class="col-md-6">
                    <div class="form-group">
                      <label>Kode Retur</label>
                      <input readonly="true" type="text" value="<?php echo $kode_retur ?>" class="form-control" placeholder="kode produk" name="kode_retur" id="kode_retur" />
                      <input type="hidden" value="<?php echo $list_get_produk->kode_produk ?>" class="form-control" placeholder="kode produk" name="kode_produk" id="kode_produk" />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal Retur</label>
                      <input readonly="true" type="text" value="<?php echo $list_get_produk->tanggal_retur ?>" class="form-control" placeholder="nama produk" name="nama_produk" id="nama_produk" />
                    </div>
                  </div>

                </div>
              </div> 

              <div class="sukses" ></div>

              <div id="list_transaksi_pembelian">
                <div class="box-body">
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th >Nama Produk</th>
                        <th>Status</th>
                        <th width="60%">Bahan Rusak</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="tabel_temp_data_transaksi">

                      <?php
                      
                      $produk = $this->db->get_where('transaksi_stok_repair',array('kode_retur'=>$kode_retur,'status'=>'proses repair' ,'kode_produk'=>$kode_produk));
                      $list_produk = $produk->result();
                      $nomor = 1;  


                      foreach($list_produk as $daftar){

                        $get_jumlah = $this->db->get_where('transaksi_stok_repair',array('kode_retur'=>$daftar->kode_retur,'kode_produk'=>$kode_produk,'status'=>'proses repair'));
                        $hasil_jumlah = $get_jumlah->row();
                        ?> 
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo $daftar->nama_produk; ?></td>
                          <td><select class="form-control select2 status" key="<?php echo $daftar->kode_repair?>" id="status_<?php echo $daftar->kode_repair?>">
                            <option value="selesai">Diperbaiki</option>
                            <option value="tidak diperbaiki">Tidak Diperbaiki</option>
                          </select></td>
                          <td>
                            <select class="form-control select2 " name="kode_bahan[]" id="kode_bahan_<?php echo $daftar->kode_repair?>" multiple>
                              <?php 
                              $get_opsi = $this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$daftar->kode_produk));
                              $hasil_opsi = $get_opsi->result();
                              foreach ($hasil_opsi as  $opsi) {
                                ?>
                                <option value="<?php echo $opsi->kode_bahan;?>"><?php echo $opsi->nama_bahan;?></option>
                                <?php
                              }
                              ?>
                              
                            </select>
                          </td>


                          <td align="center" width="125px">
                            <button class="btn btn-success simpan" id="simpan_<?php echo $daftar->kode_repair?>" kode="<?php echo $daftar->kode_repair?>" ><i class="fa fa-save "></i> Simpan</button>
                            <!--    -->
                          </td>
                        </tr>
                        <?php 
                        
                        $nomor++; 
                      } 
                      ?>


                    </tbody>
                    <tfoot>

                    </tfoot>
                  </table>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <a  href="<?php echo base_url().'stok/detail_stok_retur/'.@$list_get_produk->kode_produk ?>" class="btn blue  pull-right"  >Selesai</a>
                  </div>
                </div>
              </div>

              <br>


            </form>


          </div>

          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>

    </section><!-- /.Left col -->      
  </div><!-- /.row (main row) -->

  <div class="row">
    <!-- Left col -->
    <!-- /.Left col -->      
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    var kode_member=$('#nomor_nota').val();
    window.location = "<?php echo base_url().'stok/detail_stok_retur/'.@$list_get_produk->kode_produk; ?>";
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
  });

  $(".simpan").click(function(){
   var kode_repair = $(this).attr('kode');
   var kode_bahan = $("#kode_bahan_"+kode_repair).val();
   var kode_retur = $("#kode_retur").val();
   var kode_produk = $("#kode_produk").val();
   var status = $("#status_"+kode_repair).val();
   var url = "<?php echo base_url().'stok/simpan_repair' ?>";
   $.ajax({
    type: "POST",
    url: url,
    data: {kode_repair:kode_repair,kode_bahan:kode_bahan,kode_retur:kode_retur,kode_produk:kode_produk,status:status},
    success: function(hasil) {    
     $("#simpan_"+kode_repair).hide();
     document.getElementById("kode_bahan_"+kode_repair).disabled = true;
     document.getElementById("status_"+kode_repair).disabled = true;

   }
 });
   return false;
 });

  $(".status").change(function(){
   var kode_repair = $(this).attr('key');
   var kode_bahan = $("#kode_bahan_"+kode_repair).val();
   var status = $("#status_"+kode_repair).val();
   
   if(status=='selesai'){
    document.getElementById("kode_bahan_"+kode_repair).disabled = false;
  }else{
    
    $("#kode_bahan_"+kode_repair).select2("val", "");
    document.getElementById("kode_bahan_"+kode_repair).disabled = true;
  }

});
</script>