

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
    .ombo{
      width: 400px;
    } 

  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Detail Stok Retur
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>

            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>


            <div class="box-body">            
              <div class="sukses" ></div>
              <form id="data_form" action="" method="post">
                <div class="box-body">
                  <div class="row">
                   <?php
                   $kode_produk = $this->uri->segment(3);
                   $get_produk = $this->db->get_where('transaksi_stok_repair',array('kode_produk'=>$kode_produk));
                   $list_get_produk = $get_produk->row();
                   ?>

                   <div class="col-md-6">
                    <div class="form-group">
                      <label>Kode Produk</label>
                      <input readonly="true" type="text" value="<?php echo $kode_produk ?>" class="form-control" placeholder="kode produk" name="kode_produk" id="kode_produk" />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama Produk</label>
                      <input readonly="true" type="text" value="<?php echo $list_get_produk->nama_produk ?>" class="form-control" placeholder="nama produk" name="nama_produk" id="nama_produk" />
                    </div>
                  </div>

                </div>
              </div> 

              <div class="sukses" ></div>

              <div id="list_transaksi_pembelian">
                <div class="box-body">
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode Retur</th>
                        <th>Tanggal Retur</th>
                        <th>QTY</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="tabel_temp_data_transaksi">

                      <?php
                      $this->db->group_by('kode_retur');
                      $produk = $this->db->get_where('transaksi_stok_repair',array('kode_produk'=>$kode_produk));
                      $list_produk = $produk->result();
                      $nomor = 1;  


                      foreach($list_produk as $daftar){
                        $this->db->select_sum('jumlah');
                        $get_jumlah = $this->db->get_where('transaksi_stok_repair',array('kode_retur'=>$daftar->kode_retur,'status'=>'proses repair','kode_produk'=>$kode_produk));
                        $hasil_jumlah = $get_jumlah->row();

                        $get_proses = $this->db->get_where('transaksi_stok_repair',array('kode_retur'=>$daftar->kode_retur,'status'=>'proses repair','kode_produk'=>$kode_produk));
                        $hasil_proses = $get_proses->row();

                        ?> 
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo $daftar->kode_retur; ?></td>
                          <td><?php echo $daftar->tanggal_retur; ?></td>
                          <td><?php echo $hasil_jumlah->jumlah; ?></td>
                          <td><?php if(count($hasil_proses) > 0){?><div class='btn btn-warning'>Proses Repair</div> 
                            <?php 
                          }elseif ($daftar->status=='tidak diperbaiki') {
                            ?>
                            <div class='btn btn-info'>Tidak DIperbaiki</div> 
                            <?php
                          }else{ 
                            ?>
                            <div class='btn blue'>Selesai</div> 
                            <?php } ?></td>
                            <td align="center" width="125px">
                              <?php if(count($hasil_proses) > 0){?>
                              <a class="btn btn-success" href="<?php echo base_url().'stok/repair/'.$daftar->kode_produk.'/'.$daftar->kode_retur ?>"><i class="fa fa-pencil"></i> Repair</a>
                              <?php 
                            }
                            ?>
                          </td>
                        </tr>
                        <?php 

                        $nomor++; 
                      } 
                      ?>


                    </tbody>
                    <tfoot>

                    </tfoot>
                  </table>
                </div>
              </div>

              <br>


            </form>


          </div>

          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>

    </section><!-- /.Left col -->      
  </div><!-- /.row (main row) -->

  <div class="row">
    <!-- Left col -->
    <!-- /.Left col -->      
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    var kode_member=$('#nomor_nota').val();
    window.location = "<?php echo base_url().'stok/stok_retur/'; ?>";
  });
</script>
