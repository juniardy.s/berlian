
<table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
  <?php
  $param=$this->input->post();
  $tgl_awal=$param['tgl_awal'];
  $tgl_akhir=$param['tgl_akhir'];
  $kode_member=$param['kode_member'];
  if (!empty($tgl_awal) && !empty($tgl_akhir)) {
    $this->db->where('kode_customer',$kode_member);
    $this->db->where('tanggal_transaksi >=',$tgl_awal);
    $this->db->where('tanggal_transaksi <=',$tgl_akhir);
  }
  $hutang = $this->db->get('transaksi_piutang');
  $hasil_hutang = $hutang->result();
  //echo $this->db->last_query();
  ?>
  <thead>
    <tr>
      <th>No</th>
      <th>Kode Transaksi</th>
      <th>Nama Member</th>
      <th>Nominal</th>
      <th>Tanggal Transaksi</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $nomor = 1;
    foreach($hasil_hutang as $daftar){ 
      $tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : TanggalIndo(@$daftar->tanggal_transaksi) ;
      ?> 
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo @$daftar->kode_transaksi; ?></td>
        <td><?php echo @$daftar->nama_customer; ?></td>
        <td><?php echo format_rupiah(@$daftar->nominal_piutang); ?></td>
        <td><?php echo $tgl; ?></td>
        <td align="center">
          <a class="btn btn-primary" href="<?php echo base_url().'stok/detail_stok_member/'.$daftar->kode_customer."/".$daftar->kode_transaksi;  ?>"><i class="fa fa-pencil"></i> Detail</a>
        </td>
      </tr>
      <?php $nomor++; } ?>

    </tbody>
    <tfoot>
      <tr>
        <th>No</th>
        <th>Kode Transaksi</th>
        <th>Nama Member</th>
        <th>Nominal</th>
        <th>Tanggal Transaksi</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>