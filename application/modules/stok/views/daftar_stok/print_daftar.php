<!DOCTYPE html>
<html>
<head>
	<title>Print Stok Bahan Baku</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
<hr>
  <table class="" width="100%" border="0" style="border-collapse: collapse;">

    <tr>

      <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Stok Bahan Baku</th>
    </tr>
  </table><br>
  <table class="" width="100%" border="1" style="border-collapse: collapse;">
    <thead>
              <tr>
               <th>No.</th>
               <th>Kode Bahan</th>
               <th>Nama Bahan</th>
               <th>Nama Kategori</th>
               <th >Real Stok</th>
               <th >Stok Min</th>
               <th>HPP</th>
               <th>Aset</th>

             </tr>
           </thead>
           <tbody>
            <?php
            if (@$nama_bahan) $this->db->like('nama_bahan_baku', $nama_bahan);
            if (@$nama_kategori) $this->db->like('nama_rak', $nama_kategori);
            $this->db->where('status','sendiri');
            $get_master_barang = $this->db->get_where('master_bahan_baku',array('nama_unit'=>'Gudang'));
            $hasil_master_barang = $get_master_barang->result();

            $no = 1;
            foreach($hasil_master_barang as $item){
              if($this->session->flashdata('message')==$item->kode_bahan_baku){

                echo '<tr id="warna" style="background: #88cc99; display: none;">';
              }
              else{
                echo '<tr>';
              }
              ?>
              <td><?php echo $no;?></td>
              <td><?php echo $item->kode_bahan_baku; ?></td>
              <td><?php echo $item->nama_bahan_baku; ?></td>
              <td><?php echo $item->nama_rak; ?></td>
              <td><?php echo $item->real_stock; ?></td>
              <td><?php echo $item->stok_minimal." ",$item->satuan_stok; ?></td>
              <td align="right"><?php echo format_rupiah($item->hpp); ?></td>
              <td align="right"><?php echo format_rupiah($item->real_stock*$item->hpp); ?></td>
                <!--<td>
                <?php echo format_rupiah((@$item->real_stock <= 0) ? (@$hasil_get_hpp->hpp * 0) : (@$hasil_get_hpp->hpp * $item->real_stock));?>

              </td>-->




            </tr>
            <?php
            $no++;
          } ?>
        </tbody>
  </table>



  </html>
