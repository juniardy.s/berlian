<?php 
$get_position = $this->uri->segment(2);
$position = ucwords($get_position);
?>
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
   
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Nama Plasma 
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
               <th>No.</th>
               <th>Kode Plasma</th>
               <th>Nama Plasma</th>
               <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $this->db->group_by('kode_plasma');
                $plasma = $this->db->get_where('master_plasma',array('status'=>'1'));
                $no = 1;
                foreach($plasma->result() as $daftar){
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $daftar->kode_plasma; ?></td>
                <td><?php echo $daftar->nama_plasma; ?></td>
                <td><a href="<?php echo base_url().'stok/detail_stok_plasma/'.$daftar->kode_plasma; ?>" class="btn btn-circle green btn-icon-only" ><i class="fa fa-search"></i></a></td>
            </tr>
            <?php $no++; } ?>
            </tbody>                
        </table>


      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
</div>    
</div>  
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
            window.location = "<?php echo base_url().'stok/'; ?>";
          });
        </script>

<script>
  $(document).ready(function() {
    $("#tabel_daftar").dataTable({
    "paging":   false,
    "ordering": false,
    "info":     false
  });
  } );
    $("#gudang").click(function(){
      $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/gudang' ?>";

                            });

                            $("#bar").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/bar' ?>";
                            });

                            $("#kitchen").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/kitchen' ?>";
                            });

                            $("#serve").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/server' ?>";
                            });

    setTimeout(function(){
      $("#warna").fadeIn('slow');
    }, 1000);
    $("a#hapus").click( function() {    
      var r =confirm("Anda yakin ingin menghapus data ini ?");
      if (r==true)  
      {
        $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'master/barang/hapus' ?>",  
          cache :false,  
          data :({key:$(this).attr('key')}),
          beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
            $(".sukses").html(data);   
            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/barang/daftar' ?>";},1500);              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
        return false;
      }
      else {}        
    });

                           
  
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);

</script>

