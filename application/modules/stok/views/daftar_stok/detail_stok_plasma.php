
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
   
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Stok Plasma 
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="col-md-4">
            <div class="form-group">
                
                <select class="form-control" id="bahan">
                    <option value="" selected="true">--Pilih Bahan--</option>
                    <option value="baku">Bahan Baku</option>
                    <option value="setengah">Bahan Setengah Jadi</option>
                    <!-- <option value="jadi">Bahan Jadi</option> -->
                </select>
            </div>
          </div>
          <div class="col-md-4">
            <a onclick="cari_stok()" id="btn_cari" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>
          </div>
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
               <th>No.</th>
              <th>Kode Bahan</th>
              <th>Nama Bahan</th>
              <th align="right">Real Stok</th>
              <th align="right">Afkir</th>
              <th align="center">Opname</th>
            </tr>
            </thead>
            
            <tbody id="stok">
            
            </tbody>
                          
        </table>


      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
</div>    
</div>  

<!-- Modal -->
<div id="modal_opname" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <form action="#">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Opname <span class="nama_bahan"></span></h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <span class="control-label">Stock Sebelumnya</span>
            <input type="number" disabled id="stock_sebelumnya" class="form-control">
          </div>
          <div class="form-group">
            <span class="control-label">Afkir Sebelumnya</span>
            <input type="number" disabled id="afkir_sebelumnya" class="form-control">
          </div>
          <div class="form-group">
            <span class="control-label">Real Stock</span>
            <input type="number" name="stock" class="form-control">
          </div>
          <div class="form-group">
            <span class="control-label">Afkir</span>
            <input type="number" name="afkir" class="form-control">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="id">
        <input type="hidden" name="jenis_bahan">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="saveOpname()">Validasi</button>
      </div>
      </form>
    </div>

  </div>
</div>
<style type="text/css" media="screen">
        .btn-back
          {
            position: fixed;
            bottom: 10px;
             left: 10px;
            z-index: 999999999999999;
                vertical-align: middle;
                cursor:pointer
          }
        </style>
                <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

        <script>
          $('.btn-back').click(function(){
            window.location = "<?php echo base_url().'stok/daftar_plasma'; ?>";
          });
        </script>

<script>
  var ajaxSaveOpname = null;

  $(document).ready(function() {
    
  } );
    $("#gudang").click(function(){
      $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/gudang' ?>";

                            });

                            $("#bar").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/bar' ?>";
                            });

                            $("#kitchen").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/kitchen' ?>";
                            });

                            $("#serve").click(function(){
                              $('.tunggu').show();
                              window.location = "<?php echo base_url() . 'stok/server' ?>";
                            });

    setTimeout(function(){
      $("#warna").fadeIn('slow');
    }, 1000);
    $("a#hapus").click( function() {    
      var r =confirm("Anda yakin ingin menghapus data ini ?");
      if (r==true)  
      {
        $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'master/barang/hapus' ?>",  
          cache :false,  
          data :({key:$(this).attr('key')}),
          beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
            $(".sukses").html(data);   
            setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/barang/daftar' ?>";},1500);              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
        return false;
      }
      else {}        
    });

                           
  
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);


function cari_stok(){
    var jenis_bahan = $("#bahan").val();
    var kode_plasma = "<?php echo $this->uri->segment(3); ?>";
    $.ajax( {  
          type :"post",  
          url :"<?php echo base_url() . 'stok/get_stok_plasma' ?>",  
          cache :false,  
          data :{jenis_bahan:jenis_bahan,kode_plasma:kode_plasma},
          beforeSend:function(){
          $(".tunggu").show();  
        },
 success : function(data) { 
            $("#stok").html(data);   
            $(".tunggu").hide();              
          },  
          error : function() {  
            alert("Data gagal dimasukkan.");  
          }  
        });
    
}

function openOpname($this) {
  $('#modal_opname').find('input[name="id"]').val($this.data('id'));
  $('#modal_opname').find('input[name="jenis_bahan"]').val($this.data('jenis-bahan'));
  $('#modal_opname').find('#stock_sebelumnya').val($this.data('stock'));
  $('#modal_opname').find('#afkir_sebelumnya').val($this.data('afkir'));
  $('#modal_opname').find('input[name="stock"]').val($this.data('stock'));
  $('#modal_opname').find('input[name="afkir"]').val($this.data('afkir'));
  $('#modal_opname').find('.nama_bahan').text($this.data('nama'));
  $('#modal_opname').modal('show');
}

$('#modal_opname').on('hidden.bs.modal', function(){
  $('#modal_opname').find('input[name="id"]').val('');
  $('#modal_opname').find('input[name="jenis_bahan"]').val('');
  $('#modal_opname').find('#stock_sebelumnya').val('');
  $('#modal_opname').find('#afkir_sebelumnya').val('');
  $('#modal_opname').find('input[name="stock"]').val('');
  $('#modal_opname').find('input[name="afkir"]').val('');
  $('#modal_opname').find('.nama_bahan').text('');
});

function saveOpname() {
  var id = $('#modal_opname').find('input[name="id"]').val();
  var jenis_bahan = $('#modal_opname').find('input[name="jenis_bahan"]').val();
  var stock = $('#modal_opname').find('input[name="stock"]').val();
  var afkir = $('#modal_opname').find('input[name="afkir"]').val();
  var kode_plasma = "<?php echo $this->uri->segment(3); ?>";
  ajaxSaveOpname = $.ajax( {  
    type :"post",  
    url :"<?php echo base_url() . 'stok/save_opname_plasma' ?>",  
    cache :false,  
    data :{jenis_bahan:jenis_bahan,kode_plasma:kode_plasma,id:id,stock:stock,afkir:afkir},
    dataType: 'json',
    beforeSend:function(){
      if (ajaxSaveOpname) {
        ajaxSaveOpname.abort();
      }
      $(".tunggu").show();  
    },
    success : function(data) { 
      if (!data.error) {
        alert('Data Berhasil Disimpan');
      } else {
        alert(data.msg);  
      }
    },  
    error : function(data) {  
      alert("Data gagal dimasukkan.");  
    }  
  }).always(function(){  
    $('#btn_cari').trigger('click');
    $(".tunggu").hide();
    $('#modal_opname').modal('hide'); 
    ajaxSaveOpname = null;
  });
}

</script>

