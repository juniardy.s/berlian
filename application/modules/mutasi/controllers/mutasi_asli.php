<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mutasi extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['aktif'] = 'setting';
		$data['konten'] = $this->load->view('setting/form', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function coba()
	{
		$data['aktif'] = 'setting';
		$data['konten'] = $this->load->view('setting/coba', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);	
	}


	public function daftar()
	{
		$data['aktif'] = 'setting';
		$data['konten'] = $this->load->view('setting/daftar', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function pendaftaran()
	{
		$data['aktif'] = 'setting';
		$data['konten'] = $this->load->view('setting/form', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function ubah()
	{
		$data['aktif'] = 'setting';
		$data['konten'] = $this->load->view('setting/form', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function detail()
	{
		$data['aktif'] = 'setting';
		$data['konten'] = $this->load->view('setting/detail', $data, TRUE);
		$data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
		$this->load->view('main', $data);			
	}


	public function hapus(){
		$kode = $this->input->post("key");
		$this->db->delete( 'master_anggota', array('nomor_anggota' => $kode) );
		echo '<div class="alert alert-success">Sudah dihapus.</div>';            

	}

	public function simpan_tambah()

	{

		
		

		$data['no_transaksi']      = $this->input->post("no_transaksi_anggota");//fild no transaksi
		$data['nama'] 			   = $this->input->post("nama_anggota");//fild nama anggota
		$data['no_ktp']            = $this->input->post("no_ktp");//fild nama ktp	
		$data['alamat']            = $this->input->post("alamat");//fild alamat anggota	
		$data['nomor_anggota']            = $this->input->post("nomor_anggota");


		// Untuk fild kode_jenis_anggota
		// Untuk fild nama_jenis_anggota

		// $data['kode_jenis_anggota']  = 'Kode jenis anggota';
		// $data['nama_jenis_anggota']  = 'nama jenis anggota';
		$data['kode_jenis_anggota'] = $this->input->post("kode_jenis_anggota");		
		$kode_jenis_anggota = $this->input->post("kode_jenis_anggota");
		$ambil_kode_jenis_anggota = $this->db->query(" SELECT * FROM master_jenis_anggota where kode_jenis_anggota='$kode_jenis_anggota' ");
		$hasil_ambil_kode_jenis_anggota = $ambil_kode_jenis_anggota->row();
		$data['nama_jenis_anggota'] = $hasil_ambil_kode_jenis_anggota->nama_jenis_anggota;

		$kode_kelompok = $this->input->post("kode_kelompok");
		$ambil_kode_kelompok= $this->db->query(" SELECT * FROM master_kelompok where kode_kelompok='$kode_kelompok' ");
		$hasil_ambil_kode_kelompok = $ambil_kode_kelompok->row();
		$data['nama_pos_penampungan_susu'] = $hasil_ambil_kode_kelompok->nama_pos_penampungan_susu;
		$data['kode_pos_penampungan_susu'] = $hasil_ambil_kode_kelompok->kode_pos_penampungan_susu;
		$data['kode_kelompok'] 		= $hasil_ambil_kode_kelompok->kode_kelompok;
		$data['nama_kelompok'] 		= $hasil_ambil_kode_kelompok->nama_kelompok;
		$data['kode_cooling_unit']  = $hasil_ambil_kode_kelompok->kode_cooling_unit;
		$data['nama_cooling_unit']  = $hasil_ambil_kode_kelompok->nama_cooling_unit;

		$name_foto = $this->input->post("box_foto_upload");	//simpan foto masih gagal pak	
			// $foto = '';
			// for($i=0; $i < count($name_foto); $i++)			//
			// {				
			// 	$foto = $name_foto[$i].'|'.$foto;				

			// }
			$foto=$name_foto[0]; 			

			$data['foto'] = $foto;

		$data['jabatan']            = $this->input->post("jabatan_anggota");	
		//$data['foto']  				= $this->input->post("foto");
		$data['usr']  				= 'Data Username';	
		$data['pwd']  				= 'Data Password';

		$data['tanggal_pendaftaran']  = date("Y-m-d");//fild alamat anggota	
		$data['status'] = $this->input->post("status");

		$insert = $this->db->insert("master_anggota", $data); 
		echo '<div class="alert alert-success">Sudah tersimpan.</div>';
		$this->session->set_flashdata('message', $data['no_transaksi']);
	}
		
	function simpan_edit(){

		$this->load->library('form_validation');
	    $this->form_validation->set_rules('nama_anggota', 'Nama Anggota', 'required'); 
	    $this->form_validation->set_rules('alamat', 'Alamat', 'required'); 
	    $this->form_validation->set_rules('no_transaksi_anggota', 'No Transaksi Anggota', 'required'); 
	    $this->form_validation->set_rules('no_ktp', 'No KTP Anggota', 'required'); 
	    $this->form_validation->set_rules('kode_jenis_anggota', 'Kode Jenis Anggota', 'required'); 
	    $this->form_validation->set_rules('kode_kelompok', 'Kode Kelompok', 'required');
	    $this->form_validation->set_rules('jabatan_anggota', 'Jabatan Anggota', 'required');
	    $this->form_validation->set_rules('status', 'Status', 'required'); 

	    if ($this->form_validation->run() == FALSE) {
	        echo '<div class="alert alert-warning">Gagal tersimpan.</div>';
	    } 
	    else {
			$kode = $this->input->post("nomor_anggota");
			$data['no_transaksi'] = $this->input->post("no_transaksi_anggota");
		    $data['nama'] = $this->input->post("nama_anggota");
		    $data['alamat'] = $this->input->post("alamat");
		    $data['no_ktp'] = $this->input->post("no_ktp");
		    $data['kode_jenis_anggota'] = $this->input->post("kode_jenis_anggota");
		    $kode_jenis_anggota = $this->input->post("kode_jenis_anggota");
			$ambil_kode_jenis_anggota = $this->db->query(" SELECT * FROM master_jenis_anggota where kode_jenis_anggota='$kode_jenis_anggota' ");
			$hasil_ambil_kode_jenis_anggota = $ambil_kode_jenis_anggota->row();
			$data['nama_jenis_anggota'] = $hasil_ambil_kode_jenis_anggota->nama_jenis_anggota;

		    $data['kode_kelompok'] = $this->input->post("kode_kelompok");
		    $kode_kelompok = $this->input->post("kode_kelompok");
			$ambil_kode_kelompok= $this->db->query(" SELECT * FROM master_kelompok where kode_kelompok='$kode_kelompok' ");
			$hasil_ambil_kode_kelompok = $ambil_kode_kelompok->row();
			$data['nama_pos_penampungan_susu'] = $hasil_ambil_kode_kelompok->nama_pos_penampungan_susu;
			$data['kode_pos_penampungan_susu'] = $hasil_ambil_kode_kelompok->kode_pos_penampungan_susu;
			$data['nama_kelompok'] 		= $hasil_ambil_kode_kelompok->nama_kelompok;
			$data['kode_cooling_unit']  = $hasil_ambil_kode_kelompok->kode_cooling_unit;
			$data['nama_cooling_unit']  = $hasil_ambil_kode_kelompok->nama_cooling_unit;
			$data['jabatan'] = $this->input->post("jabatan_anggota");
		    $data['status'] = $this->input->post("status");
		    $query=$this->db->update( "master_anggota", $data, array('nomor_anggota' => $kode) );
		    $this->session->set_flashdata('message', $data['no_transaksi']);  
		    echo '<div class="alert alert-success">Sudahe Tersimpan.</div>';         
        }

	}

	
	
}
