<?php
if($kode){
  $pembelian = $this->db->get_where('master_bahan_jadi',[
    'status' => 'sendiri'
  ]);
  $list_pembelian = $pembelian->result();
  $nomor = 1;  $total = 0;
  foreach($list_pembelian as $daftar){ 
    $mut = $this->db->get_where('opsi_transaksi_mutasi', [
      'status_mutasi' => 'Mutasi Ke Petugas',
      'kode_surat_jalan' => $kode,
      'kode_bahan' => $daftar->kode_bahan_jadi
    ])->row();
    ?>
    <tr style="font-size: 15px;">
      <td>
        <?php echo $nomor; ?>
        <input type="hidden" name="id[]" value="<?php echo (@$mut->id)? $mut->id : ''; ?>">
        <input type="hidden" name="kode_bahan[]" value="<?php echo $daftar->kode_bahan_jadi; ?>">
      </td>
      <td><?php echo $daftar->nama_bahan_jadi; ?></td>
      <td><?php echo $daftar->real_stock." ".$daftar->satuan_stok; ?></td>
      <td>
        <?php echo $mut->jumlah_awal; ?>
        <input type="hidden" name="jumlah_awal[]" id="jumlah_awal" value="<?php echo $mut->jumlah_awal; ?>">
      </td>
      <td>
        <input type="number" name="jumlah[]" class="form-control" id="jumlah" value="" onkeyup="hitung_total(this)">
      </td>
      <td id="jml_mutasi"><?php echo $mut->jumlah ?></td>
    </tr>
    <?php 
    $nomor++; 
  } 
}
?>