      
<?php
$param = $this->input->post();
if(@$param['tgl_awal'] && @$param['tgl_akhir']){

  $tgl_awal = $param['tgl_awal'];
  $tgl_akhir = $param['tgl_akhir'];

  $this->db->where('tanggal_update >=', $tgl_awal);
  $this->db->where('tanggal_update <=', $tgl_akhir);
} 

$this->db->group_by('kode_mutasi','desc');
$this->db->select('*');
$this->db->from('opsi_transaksi_mutasi');  
$transaksi = $this->db->get();
$hasil_transaksi = $transaksi->result();

?>
<br>
<div class="row">
  <div class="col-md-4">

  </div>
</div>
<br>

<div class="col-md-12">
  <table id="tabel_daftar" class="table table-bordered table-striped">
    <?php

    ?>
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Mutasi</th>
        <th>No. Surat Jalan</th>
        <th>Unit Tujuan</th>
        <th>Tanggal Mutasi</th>
        <th>Petugas</th>
        <th>Status Mutasi</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $nomor = 1;
      foreach($hasil_transaksi as $daftar){ 
      //$query = $this->db->query(" SELECT * FROM transaksi_stok where kode_transaksi= '$daftar->kode_mutasi' and kode_unit_asal='$kode_unit' ");
       $query = $this->db->query(" SELECT * FROM transaksi_stok where kode_transaksi= '$daftar->kode_mutasi'  ");
       $this->db->last_query();
       $cek=$query->num_rows();
       $petugas=$query->row();
       if($cek>0){

         ?> 
         <tr style="font-size: 15px;">
          <td><?php echo $nomor; ?></td>
          <td><?php echo @$daftar->kode_mutasi; ?></td>
          <td><?php echo @$daftar->kode_surat_jalan; ?></td>
          <td><?php echo @$daftar->nama_unit_tujuan; ?></td>

          <td><?php echo TanggalIndo($daftar->tanggal_update); ?></td>
          <td><?php echo @$petugas->nama_petugas; ?></td>
          <td><div style="text-decoration: none;" class="btn <?php if($daftar->status_mutasi == 'Mutasi Ke Gudang'){echo 'blue';}else{echo 'yellow';}?> btn-lg"><?php echo @$daftar->status_mutasi; ?></div></td>
          <td align="center"><?php echo get_detail_mutasi($daftar->kode_mutasi); ?></td>
        </tr>
        <?php $nomor++; }} ?>
      </tbody>
    </table>
  </div>