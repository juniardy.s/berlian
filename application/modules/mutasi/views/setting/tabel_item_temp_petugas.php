<?php
if($kode){
  $pembelian = $this->db->get_where('opsi_transaksi_mutasi_temp',array('kode_mutasi'=>$kode));
  $list_pembelian = $pembelian->result();
  $nomor = 1;  $total = 0;
  foreach($list_pembelian as $daftar){ 
    $this->db->where('status','sendiri');
    $value = $this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi' => $daftar->kode_bahan))->row();
    ?>
    <tr style="font-size: 15px;">
      <td><?php echo $nomor; ?></td>
      <td><?php echo $daftar->nama_bahan; ?></td>
      <td><?php echo (new Fraction(intval($value->real_stock), 12))." lusin | ".$value->real_stock." ".$value->satuan_stok; ?></td>
      <td><input id="qty_<?php echo $value->kode_bahan_jadi ?>" data-id="<?php echo $value->kode_bahan_jadi ?>" class="form-control qty" name="qty[<?php echo $value->kode_bahan_jadi ?>]" type="text" data-stok="<?php echo $value->real_stock ?>"></td>
      <td>
        <select id="qty_satuan_<?php echo $value->kode_bahan_jadi ?>" name="qty_satuan[<?php echo $value->kode_bahan_jadi ?>]" data-id="<?php echo $value->kode_bahan_jadi ?>" class="form-control qty_satuan">
          <option value="lusin">lusin</option>
          <option value="pcs"><?php echo $value->satuan_stok ?></option>
        </select>
      </td>
    </tr>
    <?php 
    $nomor++; 
  } 
}
else{
  ?>
  <tr style="font-size: 15px;">
    <td><?php echo @$nomor; ?></td>
    <td><?php echo @$daftar->nama_bahan; ?></td>
    <td><?php echo @$daftar->jumlah; ?> </td>
    <td><?php echo get_edit_del_id(@$daftar->id); ?></td>
  </tr>
  <?php
}
?>