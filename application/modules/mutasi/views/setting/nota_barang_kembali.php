<!DOCTYPE html>
<html>
<head>
	<title>NOTA BARANG KEMBALI</title>
	<link href="<?php echo site_url('public/fonts/saxmono.css') ?>" rel="stylesheet" type="text/css" />
	<style type="text/css">
		html, body {
			display: block;
			font-family: 'saxMono', "Calibri" !important;
		}

		table {
			font-size: auto;
		}
		.table1{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 0px;
			text-align: left;
			vertical-align: middle;
		}

		.table2{
			width:100%; 
			border-collapse: collapse;
		}

		.table3{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 160px;
			text-align: left;
			vertical-align: middle;
		}

		@media print {
			html, body {
				display: block;
				font-family: 'saxMono', "Calibri" !important;
			}

			table {
				font-size: 12px;
			}

			@page
			{
				/* size: 21cm 14cm; */
				size: 9.5in 11in;
			}

		}

		div.page { page-break-after: always;
			position: relative;
			margin:10px 15px 0px 15px;
			padding:0px; }

		</style>
	</head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php
	$kode_mutasi=$this->uri->segment(3);
	$get_transaksi=$this->db->get_where('transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
	$hasil_transaksi=$get_transaksi->row();
	if($hasil_transaksi->kategori_petugas=='member'){
		if (substr($hasil_transaksi->kode_unit_tujuan, 0, 3) == "SA_"){
			$get_petugas=$this->db->get_where('master_sales',array('kode_sales' => $hasil_transaksi->kode_unit_tujuan ));		
		} else {
			$get_petugas=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_unit_tujuan ));		
		}
	}else if($hasil_transaksi->kategori_petugas=='sales' or $hasil_transaksi->kategori_petugas=='sales & sopir'){
		$get_petugas=$this->db->get_where('master_sales',array('kode_sales' => $hasil_transaksi->kode_unit_tujuan ));		
	}else{
		$get_petugas=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_unit_tujuan ));		
	}
	$hasil_petugas=$get_petugas->row();
	if($hasil_transaksi->kategori_petugas=='sales & sopir'){
		$sopir=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_sopir))->row();
	}
	$jalur = $this->db->get_where('master_jalur', array('kode_jalur' => $hasil_transaksi->kode_jalur))->row();
	?>
	<style>
		html, body {
			font-weight: 600;
			font-family: Arial, Helvetica, sans-serif !important;
		}
	</style>
	<body onload="print()">
		<table width="100%">
			<tr>
				<td colspan="2"><h4><b>PT. BERLIAN</b></h4></td>
				<td colspan="2" align="right"><h4><b>NOTA BARANG KEMBALI</b></h4></td>
			</tr>
			<tr>
				<td colspan="2" width="50%">Jl. Gurang Anyar No 17 - 19, Cerme, Gresik</td>
				<td width="15%">NO SJ</td>
				<td width="45%">: <?php echo @$hasil_transaksi->kode_surat_jalan;?></td>
			</tr>
			<tr>
				<td colspan="2">TELP. : (031) 7991119 / 08123219722 FAX. (031) 7991597</td>
				<td valign="top">TANGGAL</td>
				<td>: <?php echo tanggalIndo($hasil_transaksi->tanggal_transaksi); ?></td>
			</tr>
			<tr>
				<td colspan="2" valign="top">EMAIL : Berlian_broom@yahoo.com</td>
			<?php if($hasil_transaksi->kategori_petugas=='member'){ ?>
				<td valign="top">MEMBER</td>
				<td>: <?php echo @$hasil_transaksi->nama_member; ?></td>
			<?php } else { ?>
				<td valign="top">JALUR</td>
				<td>: <?php echo @$jalur->nama_jalur; ?></td>
			<?php } ?>
			</tr>
			<!-- <tr>
				<td colspan="2"></td>
				<td valign="top">Kepada Yth.</td>
				<td>: <?php echo @$hasil_transaksi->nama_unit_tujuan; ?></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td></td>
				<td> <?php echo @$hasil_petugas->alamat; ?></td>
			</tr> -->
		</table>
		<br>
		<table border="1"  width="100%" class="table table-bordered">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">NAMA PRODUK</th>
					<th style="text-align: center;">KELOMPOK HARGA</th>
					<th style="text-align: center;">STOK AWAL</th>
					<th style="text-align: center;">STOK KEMBALI</th>
					<th style="text-align: center;">STOK ASLI</th>
					<th style="text-align: center;">SELISIH</th>
				</tr>
			</thead>
			<tbody>	
				<?php

				$get_opsi=$this->db->get_where('opsi_transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
				$hasil_opsi=$get_opsi->result();
				$no=1;
				$total = 0 ;
				foreach ($hasil_opsi as  $value) {
					$this->db->where('status','sendiri');
					$get_bahan = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi' => $value->kode_bahan ))->row();
					if ((@$value->jumlah % 12) == 0) {
						$qty = @$value->jumlah/12;
						$satuan = "lusin";
					} else {
						$qty = @$value->jumlah;
						$satuan = $get_bahan->satuan_stok;
					}
					?>
					<tr>
                        <td align="center">
                            <?php echo $no++; ?>
                        </td>
                        <td><?php echo $value->nama_bahan; ?></td>
                        <td align="center"><?php echo format_rupiah($get_bahan->harga_jual); ?></td>
                        <td align="center">
                            <?php echo $value->jumlah_awal; ?>
                        </td>
                        <td align="center">
                            <?php echo $value->jumlah; ?>
                        </td>
                        <td align="center">
                            <?php echo $value->jumlah_real; ?>
                        </td>
                        <td align="center"><?php echo $value->jumlah_real - $value->jumlah ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
        <table border="1"  width="100%" class="table table-bordered">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">KELOMPOK HARGA</th>
					<th style="text-align: center;">QTY AWAL</th>
					<th style="text-align: center;">QTY HILANG</th>
					<th style="text-align: center;">SUBTOTAL</th>
				</tr>
			</thead>
			<tbody>	
            <?php 
                $barang_keluar = $this->db
                                    ->select('mbj.harga_jual, sum(jumlah - jumlah_real) as selisih, sum(jumlah_real) as qty_awal')
                                    ->from('opsi_transaksi_mutasi otm')
                                    ->join('master_bahan_jadi mbj', 'otm.kode_bahan = mbj.kode_bahan_jadi', 'left')
                                    ->where(['otm.kode_surat_jalan'=>$hasil_transaksi->kode_surat_jalan,'otm.status_mutasi'=>'Mutasi Ke Gudang','status'=>'sendiri'])
                                    ->group_by('mbj.harga_jual')
                                    ->order_by('mbj.harga_jual', 'desc')
                                    ->get()->result();
                $no = 1;
                $total = 0;
                foreach(@$barang_keluar as $value) {
					if ($value->selisih < 0) $value->selisih = 0;
            ?>
                <tr>
                    <td align="center">
                        <?php echo $no++; ?>
                    </td>
                    <td align="center"><?php echo format_rupiah($value->harga_jual); ?></td>
                    <td align="center">
                        <?php echo $value->qty_awal; ?>
                    </td>
                    <td align="center">
                        <?php echo $value->selisih; ?>
                    </td>
                    <td align="right">
                        <?php echo format_rupiah($value->selisih * $value->harga_jual); ?>
                    </td>
                </tr>
            <?php 
                    $total += $value->selisih * $value->harga_jual;
                } 
            ?>
                <tr>
                    <td colspan="4" align="right">TOTAL</td>
                    <td align="right"><?php echo format_rupiah($total) ?></td>
                </tr>
			</tbody>
		</table>
		<h5>Barang Retur</h5>
		<table border="1" width="100%" class="table table-bordered">
		<thead>
			<tr>
			<th>No</th>
			<th>Nama Produk</th>
			<th>QTY </th>
			</tr>
		</thead>
		<tbody>
		<?php
		if($hasil_transaksi->kode_surat_jalan){
		$query = "SELECT mbj.kode_bahan_jadi as kode_bahan, mbj.nama_bahan_jadi as nama_bahan, sum(otrp.jumlah) as jumlah FROM opsi_transaksi_retur_penjualan otrp JOIN master_bahan_jadi mbj ON (otrp.kode_produk = mbj.kode_bahan_jadi) WHERE otrp.kode_penjualan IN (select kode_penjualan from transaksi_penjualan WHERE kode_surat_jalan = ?) AND mbj.status = 'sendiri' GROUP BY kode_bahan";
		$pembelian = $this->db->query($query, array($hasil_transaksi->kode_surat_jalan));
		$list_pembelian = $pembelian->result();
		$nomor = 1;  $total = 0;
		foreach($list_pembelian as $daftar){ 

			?>
			<tr style="font-size: 15px;">
			<td>
				<?php echo $nomor; ?>
			</td>
			<td><?php echo $daftar->nama_bahan; ?></td>
			<td>
				<?php echo $daftar->jumlah; ?>
			</td>
			</tr>
			<?php 
			$nomor++; 
		} 
		}
		?>
		</tbody>
		<tfoot>

		</tfoot>
		</table>
		<br><br><br><br>
		<table width="100%">
			<tr>
				<td width="80%"></td>
				<td align="right"><?php echo $hasil_transaksi->nama_unit_asal;?><?php if($hasil_transaksi->kategori_petugas=='sales & sopir'){echo ' / '.$sopir->nama_sopir;}?></td>
			</tr>
			<tr>
				<td></td>
				<td align="right">PLAT NO. <?php echo $hasil_transaksi->no_kendaraan; ?></td>
			</tr>
		</table>
		<br>
		<br>
		
	</body>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</html>