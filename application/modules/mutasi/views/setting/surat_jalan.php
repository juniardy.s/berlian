<!DOCTYPE html>
<html>
<head>
	<title>PRINT SURAT JALAN</title>
	<link href="<?php echo site_url('public/fonts/saxmono.css') ?>" rel="stylesheet" type="text/css" />
	<style type="text/css">
		html, body {
			display: block;
			font-family: 'saxMono', "Calibri" !important;
		}

		table {
			font-size: auto;
		}
		.table1{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 0px;
			text-align: left;
			vertical-align: middle;
		}

		.table2{
			width:100%; 
			border-collapse: collapse;
		}

		.table3{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 160px;
			text-align: left;
			vertical-align: middle;
		}

		@media print {
			html, body {
				display: block;
				font-family: 'saxMono', "Calibri" !important;
			}

			table {
				font-size: 12px;
			}

			@page
			{
				/* size: 21cm 14cm; */
				size: 9.5in 11in;
			}

		}

		div.page { page-break-after: always;
			position: relative;
			margin:10px 15px 0px 15px;
			padding:0px; }

		</style>
	</head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php
	$kode_mutasi=$this->uri->segment(3);
	$get_transaksi=$this->db->get_where('transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
	$hasil_transaksi=$get_transaksi->row();
	if($hasil_transaksi->kategori_petugas=='member'){
		if (substr($hasil_transaksi->kode_unit_tujuan, 0, 3) == "SA_"){
			$get_petugas=$this->db->get_where('master_sales',array('kode_sales' => $hasil_transaksi->kode_unit_tujuan ));		
		} else {
			$get_petugas=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_unit_tujuan ));		
		}
	}else if($hasil_transaksi->kategori_petugas=='sales' or $hasil_transaksi->kategori_petugas=='sales & sopir'){
		$get_petugas=$this->db->get_where('master_sales',array('kode_sales' => $hasil_transaksi->kode_unit_tujuan ));		
	}else{
		$get_petugas=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_unit_tujuan ));		
	}
	$hasil_petugas=$get_petugas->row();
	if($hasil_transaksi->kategori_petugas=='sales & sopir'){
		$sopir=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_sopir))->row();
	}
	$jalur = $this->db->get_where('master_jalur', array('kode_jalur' => $hasil_transaksi->kode_jalur))->row();
	?>
	<style>
		html, body {
			font-weight: 600;
			font-family: Arial, Helvetica, sans-serif !important;
		}
	</style>
	<body onload="print()">
		<table width="100%">
			<tr>
				<td colspan="2"><h4><b>PT. BERLIAN</b></h4></td>
				<td colspan="2" align="right"><h4><b>SURAT JALAN</b></h4></td>
			</tr>
			<tr>
				<td colspan="2" width="50%">Jl. Gurang Anyar No 17 - 19, Cerme, Gresik</td>
				<td width="15%">NO SJ</td>
				<td width="45%">: <?php echo @$hasil_transaksi->kode_surat_jalan;?></td>
			</tr>
			<tr>
				<td colspan="2">TELP. : (031) 7991119 / 08123219722 FAX. (031) 7991597</td>
				<td valign="top">TANGGAL</td>
				<td>: <?php echo tanggalIndo($hasil_transaksi->tanggal_transaksi); ?></td>
			</tr>
			<tr>
				<td colspan="2" valign="top">EMAIL : Berlian_broom@yahoo.com</td>
			<?php if($hasil_transaksi->kategori_petugas=='member'){ ?>
				<td valign="top">MEMBER</td>
				<td>: <?php echo @$hasil_transaksi->nama_member; ?></td>
			<?php } else { ?>
				<td valign="top">JALUR</td>
				<td>: <?php echo @$jalur->nama_jalur; ?></td>
			<?php } ?>
			</tr>
			<!-- <tr>
				<td colspan="2"></td>
				<td valign="top">Kepada Yth.</td>
				<td>: <?php echo @$hasil_transaksi->nama_unit_tujuan; ?></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td></td>
				<td> <?php echo @$hasil_petugas->alamat; ?></td>
			</tr> -->
		</table>
		<br>
		<table border="1"  width="100%" class="table table-bordered">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">KODE</th>
					<th style="text-align: center;">NAMA BARANG</th>
					<th style="text-align: center;">JUMLAH BARANG</th>
					<th style="text-align: center;">SATUAN</th>
					<th style="text-align: center;">KETERANGAN</th>
				</tr>
			</thead>
			<tbody>	
				<?php

				$get_opsi=$this->db->get_where('opsi_transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
				$hasil_opsi=$get_opsi->result();
				$no=1;
				$total = 0 ;
				foreach ($hasil_opsi as  $value) {
					$this->db->where('status','sendiri');
					$get_bahan = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi' => $value->kode_bahan ))->row();
					if ((@$value->jumlah % 12) == 0) {
						$qty = @$value->jumlah/12;
						$satuan = "lusin";
					} else {
						$qty = @$value->jumlah;
						$satuan = $get_bahan->satuan_stok;
					}
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td align="left"><?php echo @$value->kode_bahan;?></td>
						<td align="center"><?php echo @$value->nama_bahan;?></td>
						<td align="center" ><?php echo $qty;?></td>
						<?php
						$get_bahan_jadi = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>@$value->kode_bahan));
						$hasil_bahanjadi = $get_bahan_jadi->row();
						?>
						<td align="center" ><?php echo $satuan; ?></td>
						<td align="left" style="text-align: center;"></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
		<br><br><br><br>
		<table width="100%">
			<tr>
				<td align="center">PENERIMA</td>
				<td></td>
				<td colspan="2" align="right"><?php echo $hasil_transaksi->nama_unit_tujuan;?><?php if($hasil_transaksi->kategori_petugas=='sales & sopir'){echo ' / '.$sopir->nama_sopir;}?></td>
			</tr>
			<tr>
				<td colspan="3"></td>
				<td align="right">PLAT NO. <?php echo $hasil_transaksi->no_kendaraan; ?></td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td width="30%" align="center">NAMA TERANG DAN CAP PERUSAHAAN</td>
				<td width="20%" align="center">SOPIR</td>
				<td width="20%" align="center">GUDANG</td>
				<td width="20%" align="center">SATPAM</td>
			</tr>
		</table>
		<br>
		<br>
		<!-- <table width="100%">
			<tr>
				<td>1. Barang yang diterima harap dihitung dengan teliti dan disaksikan oleh pihak pengirim dan penerima</td>
			</tr>
			<tr>
				<td>2. Complain kualitas maksimal 10 hari dari tanggal penerimaan. Retur barang hanya dapat dilayani 30 hari sesudah penerimaan.</td>
			</tr>
		</table> -->
		<!--
		<table width="100%" style="font-size: 11px;">
			<tr>
				<td colspan="2"><img src="<?php echo base_url().'component/img/logo berlian tm.jpg' ?>" width="150px" alt="" title="" />	</td>
				<td colspan="2" rowspan="2" align="center"><h5><b>BUKTI SURAT JALAN</b></h5></td>
				<td>NO. PENGIRIMAN :</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">www.sapuberlian.com</td>
				<td>NO. BUKU :</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">(031) 7991119/ 7997597/ WA 08128322744</td>
				<td>SALES & DRIVER :</td>
				<td><?php echo $hasil_transaksi->nama_unit_tujuan;?><?php if($hasil_transaksi->kategori_petugas=='sales & sopir'){echo ' / '.$sopir->nama_sopir;}?></td>
				<td>TANGGAL :</td>
				<td><?php echo tanggalIndo($hasil_transaksi->tanggal_transaksi); ?></td>
			</tr>
			<tr>
				<td colspan="2">Jl. Gurang Anyar No 17 - 19, Cerme, Gresik</td>
				<td>NO. SURAT JALAN :</td>
				<td><?php echo @$hasil_transaksi->kode_surat_jalan;?></td>
				<td>JALUR :</td>
				<?php 
					$kode 	=	$hasil_transaksi->kode_jalur;
					$get 	=	$this->db->get_where('master_jalur' , ['kode_jalur' => $kode ] )->result_array();
				 ?>
				<td><?php echo $get[0]['nama_jalur'] ;?></td>
			</tr>
		</table>
		<br>
		<table border="1"  width="100%" class="table table-bordered" style="font-size: 11px">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">KODE</th>
					<th style="text-align: center;">NAMA BARANG</th>
					<th style="text-align: center;">JUMLAH BARANG</th>
					<th style="text-align: center;">SATUAN</th>
					<th style="text-align: center;">KETERANGAN</th>
				</tr>
			</thead>
			<tbody>	
				<?php

				$get_opsi=$this->db->get_where('opsi_transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
				$hasil_opsi=$get_opsi->result();
				$no=1;
				$total = 0 ;
				foreach ($hasil_opsi as  $value) {
					$this->db->where('status','sendiri');
					$get_bahan = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi' => $value->kode_bahan ))->row();
					if ((@$value->jumlah % 12) == 0) {
						$qty = @$value->jumlah/12;
						$satuan = "lusin";
					} else {
						$qty = @$value->jumlah;
						$satuan = $get_bahan->satuan_stok;
					}
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td align="left"><?php echo @$value->kode_bahan;?></td>
						<td align="center"><?php echo @$value->nama_bahan;?></td>
						<td align="center" ><?php echo $qty;?></td>
						<?php
						$get_bahan_jadi = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>@$value->kode_bahan));
						$hasil_bahanjadi = $get_bahan_jadi->row();
						?>
						<td align="center" ><?php echo $satuan; ?></td>
						<td align="left" style="text-align: center;"></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td style="text-align: right;" colspan="5"><b>TOTAL</b></td>
					<td style="text-align: right;">0</td>
				</tr>
			</tbody>
		</table>
		<table width="100%" style="font-size: 11px">
			<tr>
				<td align="center"><b>Pengirim</b></td>
				<td align="center"><b>Gudang</b></td>
				<td align="center"><b>Sopir</b></td>
				<td align="center"><b>Satpam</b></td>
				<td align="center"><b>Penerima</b></td>
			</tr>
			<tr>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
			</tr>
		</table>
		-->
	</body>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</html>