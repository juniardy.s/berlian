<?php
if($kode){
  $pembelian = $this->db
                  ->select('otm.*, mbj.harga_jual')
                  ->from('opsi_transaksi_mutasi otm')
                  ->join('master_bahan_jadi mbj', 'otm.kode_bahan = mbj.kode_bahan_jadi', 'left')
                  ->where(['otm.kode_surat_jalan'=>$kode,'otm.status_mutasi'=>'Mutasi Ke Petugas','status'=>'sendiri'])
                  ->order_by('mbj.harga_jual', 'desc')
                  ->order_by('otm.nama_bahan', 'asc')
                  ->get();
  $list_pembelian = $pembelian->result();
  $nomor = 1;  $total = 0;
  foreach($list_pembelian as $daftar){ 
    unset($stok_terjual);
    $stok_terjual = $this->db->select('sum(otp.jumlah) as jumlah')
                            ->from('opsi_transaksi_penjualan otp')
                            ->join('transaksi_penjualan tp', 'otp.kode_penjualan = tp.kode_penjualan')
                            ->where(['tp.kode_surat_jalan' => $kode, 'otp.kode_menu' => $daftar->kode_bahan])
                            ->get()->row_array()['jumlah'];
    ?>
    <tr style="font-size: 15px;">
      <td>
        <?php echo $nomor; ?>
        <input type="hidden" name="id[]" value="<?php echo $daftar->id; ?>">
        <input type="hidden" name="kode_bahan[]" value="<?php echo $daftar->kode_bahan; ?>">
      </td>
      <td><?php echo $daftar->nama_bahan; ?></td>
      <td><?php echo format_rupiah($daftar->harga_jual); ?></td>
      <td>
        <?php echo $daftar->jumlah_awal; ?>
        <input type="hidden" name="jumlah_awal[]" id="jumlah_awal" value="<?php echo $daftar->jumlah; ?>">
      </td>
      <td>
        <?php echo intval($stok_terjual) ?>
      </td>
      <td>
        <?php echo $daftar->jumlah; ?>
        <input type="hidden" name="jumlah[]" id="jumlah" value="<?php echo $daftar->jumlah; ?>">
      </td>
      <td><input type="number" name="jml_mutasi[]" value="<?php echo $daftar->jumlah ?>" onkeyup="hitung_sisa(this)"></td>
      <td><label id="sisa_stok">0</label></td>
    </tr>
    <?php 
    $nomor++; 
  } 
}
?>