<?php
defined('BASEPATH') or exit('No direct script access allowed');
class revisi_invoice extends MY_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('astrosession') == false) {
            redirect(base_url('authenticate'));
        }
        $this->load->library('form_validation');
    }
    //------------------------------------------ View Data Table----------------- --------------------//
    public function index()
    {
        $data['aktif'] = 'kasir';
        $data['halaman'] = $this->load->view('revisi_invoice/setting/cari_invoice', $data, true);
        $this->load->view('revisi_invoice/main', $data);
    }

    public function print_revisi_invoice()
    {
        $this->load->view('setting/print_revisi_invoice');
    }

    public function cari_kode_penjualan()
    {
        $json = array('error' => true);
        $kode = $this->input->post('kode_penjualan');
        try {
            if (!$kode) throw new Exception("Error Processing Request");
            $data = $this->db->get_where('transaksi_penjualan', array('kode_penjualan' => $kode))->row_array();
            if ($data) {
                $json['data'] = $data;
                $json['error'] = false;
            }
            echo json_encode($json);
            exit();
        } catch (Exception $e) {
            echo json_encode($json);
        }

    }

    public function cancel_invoice()
    {
        $json = array('error' => true);
        $kode = $this->input->post('kode_penjualan');
        try {
            if (!$kode) throw new Exception("Error Processing Request");
            $transaksi_penjualan = $this->db->get_where('transaksi_penjualan', array('kode_penjualan' => $kode))->row_array();
            if (!$transaksi_penjualan) throw new Exception("Kode Penjualan tidak ditemukan");
            $surat_jalan = $transaksi_penjualan['kode_surat_jalan'];
            $opsi_transaksi_penjualan = $this->db->get_where('opsi_transaksi_penjualan', array('kode_penjualan' => $kode))->result_array();
            $transaksi_mutasi = $this->db->get_where('transaksi_mutasi', array('kode_surat_jalan' => $surat_jalan, 'status_mutasi' => 'Mutasi Ke Petugas'))->row_array();
            $opsi_transaksi_mutasi = $this->db->get_where('opsi_transaksi_mutasi', array('kode_mutasi' => $transaksi_mutasi['kode_mutasi']))->result_array();
            $kode_mutasi = $transaksi_mutasi['kode_mutasi'];
            $opsi_transaksi_retur_penjualan = $this->db->get_where('opsi_transaksi_retur_penjualan', array('kode_penjualan' => $kode, 'jenis_retur' => 'tukar_barang'))->result_array();

            // Delete Invoice
            $this->db->trans_begin();
            $this->db->delete('transaksi_retur_penjualan', array('kode_penjualan' => $kode));
            $this->db->delete('transaksi_stok_repair', array('kode_penjualan' => $kode));
            $this->db->delete('opsi_transaksi_retur_penjualan', array('kode_penjualan' => $kode));
            $this->db->delete('transaksi_komisi_revisi', array('kode_transaksi_asal_komisi' => $kode));
            $this->db->delete('keuangan_keluar', array('kode_referensi' => $kode));
            $this->db->delete('keuangan_masuk', array('kode_referensi' => $kode));
            $this->db->delete('transaksi_piutang', array('kode_transaksi' => $kode));
            $this->db->delete('transaksi_withdraw', array('kode_transaksi' => $kode));
            $this->db->delete('transaksi_penjualan', array('kode_penjualan' => $kode));
            $this->db->delete('transaksi_stok', array('kode_transaksi' => $kode));
            $this->db->delete('opsi_transaksi_penjualan', array('kode_penjualan' => $kode));
            
            if ($transaksi_penjualan['kategori_penjualan'] == 'Member') {
                // Delete Mutasi
                $this->db->delete('transaksi_stok', array('kode_transaksi' => $kode_mutasi));
                $this->db->delete('opsi_transaksi_mutasi', array('kode_surat_jalan' => $surat_jalan));
                $this->db->delete('transaksi_mutasi', array('kode_surat_jalan' => $surat_jalan));
    
                // Stok Kembali
                foreach ($opsi_transaksi_mutasi as $daftar) {
                    $data_master = $this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi' => $daftar['kode_bahan'], 'status' => 'sendiri'))->row_array();
                    $hasil_update['real_stock'] = $data_master['real_stock'] + $daftar['jumlah_awal'];
                    $this->db->update('master_bahan_jadi', $hasil_update, array('kode_bahan_jadi' => $daftar['kode_bahan'], 'status' => 'sendiri'));
                }
            } else {
                // Stok Kembali
                if ($input['kategori_petugas'] == 'sales' or $input['kategori_petugas'] == 'sales & sopir') {
                    $status = 'sales';
                } else if ($input['kategori_petugas'] == 'member') {
                    $status = 'member';
                } else {
                    $status = 'sopir';
                }
                foreach ($opsi_transaksi_penjualan as $daftar) {
                    $get_bahan_jadi       = $this->db->get_where('master_bahan_jadi', array(
                        'kode_bahan_jadi' => $daftar['kode_menu'],
                        'kode_member' => $transaksi_penjualan['kode_petugas'],
                        'status' => $status
                    ));
                    $hasil_get_bahan_jadi = $get_bahan_jadi->row();
                    
                    $tambah_stok['real_stock'] = $hasil_get_bahan_jadi->real_stock + $daftar['jumlah'];
                    $this->db->update('master_bahan_jadi', $tambah_stok, array(
                        'kode_bahan_jadi' => $daftar['kode_menu'],
                        'kode_member' => $transaksi_penjualan['kode_petugas'],
                        'status' => $status
                    ));
                    unset($tambah_stok);

                    $get_item_mutasi = $this->db->get_where('opsi_transaksi_mutasi', array(
                        'kode_bahan' => $daftar['kode_menu'],
                        'kode_mutasi' => $transaksi_mutasi['kode_mutasi'],
                        'status_mutasi' => 'Mutasi Ke Petugas'
                    ))->row();

                    $tambah_stok['jumlah'] = $get_item_mutasi->jumlah + $daftar['jumlah'];
                    $tambah_stok['jumlah_terjual'] = $get_item_mutasi->jumlah_terjual - $daftar['jumlah'];
                    $this->db->update('opsi_transaksi_mutasi', $tambah_stok, array(
                        'kode_bahan' => $daftar['kode_menu'],
                        'kode_mutasi' => $transaksi_mutasi['kode_mutasi'],
                        'status_mutasi' => 'Mutasi Ke Petugas'
                    ));
                    unset($tambah_stok);
                }

                // Stok Retur
                foreach ($opsi_transaksi_retur_penjualan as $daftar) {
                    $get_bahan_jadi       = $this->db->get_where('master_bahan_jadi', array(
                        'kode_bahan_jadi' => $daftar['kode_produk_konversi'],
                        'kode_member' => $transaksi_penjualan['kode_petugas'],
                        'status' => $status
                    ));
                    $hasil_get_bahan_jadi = $get_bahan_jadi->row();
                    
                    $tambah_stok['real_stock'] = $hasil_get_bahan_jadi->real_stock + $daftar['jumlah_konversi'];
                    $this->db->update('master_bahan_jadi', $tambah_stok, array(
                        'kode_bahan_jadi' => $daftar['kode_produk_konversi'],
                        'kode_member' => $transaksi_penjualan['kode_petugas'],
                        'status' => $status
                    ));
                    unset($tambah_stok);

                    $get_item_mutasi = $this->db->get_where('opsi_transaksi_mutasi', array(
                        'kode_bahan' => $daftar['kode_produk_konversi'],
                        'kode_mutasi' => $transaksi_mutasi['kode_mutasi'],
                        'status_mutasi' => 'Mutasi Ke Petugas'
                    ))->row();

                    $tambah_stok['jumlah_real'] = $get_item_mutasi->jumlah + $daftar['jumlah_konversi'];
                    $this->db->update('opsi_transaksi_mutasi', $tambah_stok, array(
                        'kode_bahan' => $daftar['kode_produk_konversi'],
                        'kode_mutasi' => $transaksi_mutasi['kode_mutasi'],
                        'status_mutasi' => 'Mutasi Ke Petugas'
                    ));
                }
            }
            $this->db->trans_commit();
            $json['error'] = false;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $json['message'] = $e->getMessage();
        }
        echo json_encode($json);
    }

    public function simpan_retur_pt_temp()
    {
        $masukan = $this->input->post();

            $transaksi_penjualan = $this->db->get_where('transaksi_penjualan', array('kode_penjualan' => $masukan['kode_penjualan']))->row_array();
            $kode_produk = $masukan['kode_menu'];
            $this->db->where('status', 'sendiri');
            $get_menu = $this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi' =>
                $kode_produk));
            $hasil_getmenu = $get_menu->row();

            
            $cek_menu = $this->db->get_where('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' =>
                $masukan['kode_penjualan'], 'kode_produk' => $kode_produk, 'jenis_retur' => 'potong_tagihan'));
            $hasil_cek_menu = $cek_menu->row();

            if (count($hasil_cek_menu) < 1) {
                $dtInsert['kode_kasir'] = $transaksi_penjualan['kode_kasir'];
                $dtInsert['kode_retur'] = "R".$masukan['kode_penjualan'];
                $dtInsert['kode_penjualan'] = $masukan['kode_penjualan'];
                $dtInsert['kode_produk'] = $masukan['kode_menu'];
                $dtInsert['nama_produk'] = $hasil_getmenu->nama_bahan_jadi;
                $dtInsert['jumlah'] = intval($masukan['jumlah']);
                $dtInsert['harga_satuan'] = intval($masukan['harga_satuan']);
                $dtInsert['subtotal'] = intval($masukan['jumlah'] ) * intval($masukan['harga_satuan']);
                $dtInsert['status'] = 'retur';
                $dtInsert['jenis_retur'] = 'potong_tagihan';

                $this->db->insert('opsi_transaksi_retur_penjualan_temp', $dtInsert);
            } else {
                $dtUpdate['jumlah'] = intval($masukan['jumlah']);
                $dtUpdate['harga_satuan'] = intval($masukan['harga_satuan']);
                $dtUpdate['subtotal'] = intval($masukan['jumlah'] ) * intval($masukan['harga_satuan']);

                $this->db->update('opsi_transaksi_retur_penjualan_temp', $dtUpdate, array('kode_penjualan' =>
                    $masukan['kode_penjualan'], 'kode_produk' => $kode_produk, 'jenis_retur' => 'potong_tagihan'));
            }
            echo "0|berhasil";

    }

    public function simpan_retur_brg_temp()
    {
        $masukan = $this->input->post();

            $transaksi_penjualan = $this->db->get_where('transaksi_penjualan', array('kode_penjualan' => $masukan['kode_penjualan']))->row_array();
            $kode_produk = $masukan['kode_menu'];
            $this->db->where('status', 'sendiri');
            $get_menu = $this->db->get_where('master_bahan_jadi', array('kode_bahan_jadi' =>
                $kode_produk));
            $hasil_getmenu = $get_menu->row();

            
            $cek_menu = $this->db->get_where('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' =>
                $masukan['kode_penjualan'], 'kode_produk' => $kode_produk, 'jenis_retur' => 'barang'));
            $hasil_cek_menu = $cek_menu->row();

            if ($cek_menu->num_rows() < 1) {
                $dtInsert['kode_kasir'] = $transaksi_penjualan['kode_kasir'];
                $dtInsert['kode_retur'] = "R".$masukan['kode_penjualan'];
                $dtInsert['kode_penjualan'] = $masukan['kode_penjualan'];
                $dtInsert['kode_produk'] = $masukan['kode_menu'];
                $dtInsert['nama_produk'] = $hasil_getmenu->nama_bahan_jadi;
                $dtInsert['jumlah'] = intval($masukan['jumlah']);
                $dtInsert['harga_satuan'] = 0;
                $dtInsert['subtotal'] = 0;
                $dtInsert['status'] = 'retur';
                $dtInsert['jenis_retur'] = 'barang';

                $this->db->insert('opsi_transaksi_retur_penjualan_temp', $dtInsert);
            } else {
                $dtUpdate['jumlah'] = intval($masukan['jumlah']);
                $dtUpdate['harga_satuan'] = 0;
                $dtUpdate['subtotal'] = 0;

                $this->db->update('opsi_transaksi_retur_penjualan_temp', $dtUpdate, array('kode_penjualan' =>
                    $masukan['kode_penjualan'], 'kode_produk' => $kode_produk, 'jenis_retur' => 'barang'));
            }
            echo "0|berhasil";
    }

    public function retur_brg_temp($kode_penjualan)
    {
        @$data['kode'] = @$kode_penjualan;
        $this->load->view('revisi_invoice/setting/daftar_retur_brg_temp', $data);
    }

    public function hapus_retur_brg_temp()
    {
        $id = $this->input->post('id');
        $cek = $this->db->get_where('opsi_transaksi_retur_penjualan_temp', array('id' => $id, 'jenis_retur' => 'barang'));
        $hasil_cek = $cek->row();
    
        $this->db->delete('opsi_transaksi_retur_penjualan_temp', array('id' => $id, 'jenis_retur' => 'barang'));
    }

    public function revisi($kode_penjualan)
    {
        $this->db->delete('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' => $kode_penjualan));
        $data['kode_penjualan'] = $kode_penjualan;
        $data['aktif'] = 'kasir';
        $data['halaman'] = $this->load->view('revisi_invoice/setting/menu_revisi_invoice', $data, true);
        $this->load->view('revisi_invoice/main', $data);
    }

    public function simpan_revisi()
    {
        $input = $this->input->post();
        $penjualan = $this->db->get_where('transaksi_penjualan', array('kode_penjualan' => $input['kode_penjualan']))->row_array();
        $grand_total = 0;
        $this->db->trans_begin();
        try {
            foreach ($input['qty'] as $id => $value) {
                $opsi_penjualan = $this->db->get_where('opsi_transaksi_penjualan', array('id' => $id))->row_array();
                $opsi_mutasi = $this->db->get_where('opsi_transaksi_mutasi', array('kode_surat_jalan' => $penjualan['kode_surat_jalan'], 'status_mutasi' => 'Mutasi Ke Petugas', 'kode_bahan' => $opsi_penjualan['kode_menu']))->row_array();
                if ($value < 1) {
                    $this->db->delete('opsi_transaksi_penjualan', array('id' => $id));
                } else {
                    $this->db->update('opsi_transaksi_penjualan', array('jumlah' => $value, 'harga_satuan' => $input['harga'][$id], 'subtotal' => ($value * $input['harga'][$id]) - $opsi_penjualan['diskon_rupiah']), array('id' => $id));
                }
                $this->db->update('opsi_transaksi_mutasi', array('jumlah' => (($opsi_mutasi['jumlah'] + $opsi_penjualan['jumlah']) - $value)), array('kode_surat_jalan' => $penjualan['kode_surat_jalan'], 'status_mutasi' => 'Mutasi Ke Petugas', 'kode_bahan' => $opsi_penjualan['kode_menu']));
                $grand_total += (($value * $input['harga'][$id]) - $opsi_penjualan['diskon_rupiah']);
            }
            $this->db->update('transaksi_penjualan', [
                'total_nominal' => $grand_total,
                'grand_total' => $penjualan['diskon_rupiah'],
                'tanggal_penjualan' => date('Y-m-d', strtotime($input['tanggal_penjualan'])),
            ], [
                'kode_penjualan' => $input['kode_penjualan']
            ]);

            $this->db->delete('opsi_transaksi_retur_penjualan', array('kode_penjualan' => $input['kode_penjualan']));

            $get_id_petugas = $this->session->userdata('astrosession');
            $id_petugas = $get_id_petugas->id;
            $nama_petugas = $get_id_petugas->uname;
            $total_nominal_retur = 0;

            $get_retur_pt = $this->db->get_where('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' => $input['kode_penjualan'], 'jenis_retur' => 'potong_tagihan'));
            $hasil_retur_pt = $get_retur_pt->result();
            foreach (@$hasil_retur_pt as $retur_pt) {
                $total_nominal_retur += $retur_pt->subtotal;

                unset($retur_pt->id);
                $insertOpsi = json_decode(json_encode($retur_pt), true);
                $this->db->insert('opsi_transaksi_retur_penjualan', $insertOpsi);

                // Stok Repair
                $n=$retur_pt->jumlah;
                for($ulang=1;$ulang <=$n;$ulang++){

                    $user = $this->session->userdata('astrosession');
                    $id_user=$user->id;

                    $this->db->select_max('id');
                    $get_max_repair = $this->db->get('transaksi_stok_repair');
                    $max_repair = $get_max_repair->row();

                    $this->db->where('id', $max_repair->id);
                    $get_repair = $this->db->get('transaksi_stok_repair');
                    $repair = $get_repair->row();
                    $tahun = substr(@$repair->kode_repair, 4,4);
                    if(date('Y')==$tahun){
                        $nomor = substr(@$repair->kode_repair, 12);
                            //echo $nomor;
                        $nomor = $nomor + 1;
                        $string = strlen($nomor);
                        if($string == 1){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                        } else if($string == 2){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                        } else if($string == 3){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_000'.$nomor;
                        } else if($string == 4){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_00'.$nomor;
                        } else if($string == 5){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_0'.$nomor;
                        } else if($string == 6){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_'.$nomor;
                        }
                    } else {
                        $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_000001';
                    }
                    
                    $data_repair['kode_repair'] = $kode_repair;
                    $data_repair['kode_retur'] = $retur_pt->kode_retur;
                    $data_repair['kode_penjualan'] = $data['kode_penjualan'];
                    $data_repair['tanggal_retur'] = date('Y-m-d');
                    $data_repair['kode_produk'] = $retur_pt->kode_produk;
                    $data_repair['nama_produk'] = $retur_pt->nama_produk;
                    $data_repair['jumlah'] =1;
                    $data_repair['status'] ='proses repair';
                    

                    $this->db->insert('transaksi_stok_repair',$data_repair);
                }
            }
            $this->db->delete('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' => $input['kode_penjualan'], 'jenis_retur' => 'potong_tagihan'));

            $get_retur_brg = $this->db->get_where('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' => $input['kode_penjualan'], 'jenis_retur' => 'barang'));
            $hasil_retur_brg = $get_retur_brg->result();
            foreach (@$hasil_retur_brg as $retur_brg) {
                $total_nominal_retur += $retur_brg->subtotal;

                unset($retur_brg->id);
                $insertOpsi = json_decode(json_encode($retur_brg), true);
                $this->db->insert('opsi_transaksi_retur_penjualan', $insertOpsi);

                // Stok Repair
                $n=$retur_brg->jumlah;
                for($ulang=1;$ulang <=$n;$ulang++){

                    $user = $this->session->userdata('astrosession');
                    $id_user=$user->id;

                    $this->db->select_max('id');
                    $get_max_repair = $this->db->get('transaksi_stok_repair');
                    $max_repair = $get_max_repair->row();

                    $this->db->where('id', $max_repair->id);
                    $get_repair = $this->db->get('transaksi_stok_repair');
                    $repair = $get_repair->row();
                    $tahun = substr(@$repair->kode_repair, 4,4);
                    if(date('Y')==$tahun){
                        $nomor = substr(@$repair->kode_repair, 12);
                            //echo $nomor;
                        $nomor = $nomor + 1;
                        $string = strlen($nomor);
                        if($string == 1){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                        } else if($string == 2){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                        } else if($string == 3){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_000'.$nomor;
                        } else if($string == 4){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_00'.$nomor;
                        } else if($string == 5){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_0'.$nomor;
                        } else if($string == 6){
                            $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_'.$nomor;
                        }
                    } else {
                        $kode_repair = 'REP_'.date('Y').'_'.$id_user.'_000001';
                    }
                    
                    $data_repair['kode_repair'] = $kode_repair;
                    $data_repair['kode_retur'] = $retur_brg->kode_retur;
                    $data_repair['kode_penjualan'] = $data['kode_penjualan'];
                    $data_repair['tanggal_retur'] = date('Y-m-d');
                    $data_repair['kode_produk'] = $retur_brg->kode_produk;
                    $data_repair['nama_produk'] = $retur_brg->nama_produk;
                    $data_repair['jumlah'] =1;
                    $data_repair['status'] ='proses repair';
                    

                    $this->db->insert('transaksi_stok_repair',$data_repair);
                }
            }
            $this->db->delete('opsi_transaksi_retur_penjualan_temp', array('kode_penjualan' => $input['kode_penjualan'], 'jenis_retur' => 'barang'));

            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
    }

}
