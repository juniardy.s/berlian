<div class="page-content">
  <!-- Content Header (Page header) -->
  <style type="text/css">
    .ombo{
      width: 600px;
    }
  </style>    
  <!-- Main content -->
  <div>             
    <!-- Main row -->
    <div class="row">
    <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Cari Penjualan
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row">
            <div class="col-md-12">


              <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                  <div class="box box-info">                 
                    <div class="box-body">           

                      <form id="data_form" action="" method="post">

                        <div class="box-body" >
                          <div class="row">

                            <div class="col-md-12">
                              <label>Kode Penjualan </label>
                              <input type="text" name="kode_penjualan" id="kode_penjualan" class="form-control">
                            </div>
                            
                          </div>
                          
                          <div class="row">

                            <div class="col-md-3 col-sm-6">
                              <label>Kode Petugas </label>
                              <input type="text" id="kode_petugas" class="form-control" disabled>
                            </div>

                            <div class="col-md-3 col-sm-6">
                              <label>Nama Petugas </label>
                              <input type="text" id="nama_petugas" class="form-control" disabled>
                            </div>

                            <div class="col-md-3 col-sm-6">
                              <label>Kode Member </label>
                              <input type="text" id="kode_member" class="form-control" disabled>
                            </div>

                            <div class="col-md-3 col-sm-6">
                                <label>Nama Member </label>
                                <input type="text" id="nama_member" class="form-control" disabled>
                            </div>
                            
                          </div>
                          <br><br>

                        </div> 
                        <div class="box-footer btn_controls">
                          <!-- <button type="submit" class="btn btn-primary ">Simpan</button> -->
                          <center>
                            <a href="javascript:;" data-toggle="modal" data-target="#modal-confirm-hapus" class="btn btn-lg red " style="width:200px;"><i class="fa fa-trash"></i> Hapus</a>
                            <button type="button" onclick="revisi_invoice()" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-edit"></i> Revisi</button></center>
                          </div>
                        </form>
                      </div>
                    </div>
                  </section><!-- /.Left col -->      
                </div>
              </div>
            </div>
          </div>

          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>
    </div><!-- /.col -->
  </div>
    <!-- /.Left col -->      
    </div>
<!-- /.row (main row) -->
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div id="modal-confirm-hapus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Penjualan
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin menghapus transaksi penjualan tersebut ?
        </span>
        <input id="no" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button id="ya" onclick="hapus_invoice()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<script>
$('.btn_controls').hide();
$(document).ready(function(){
    $('#kode_penjualan').keyup(function(){
        $('#kode_petugas').val('');
        $('#nama_petugas').val('');
        $('#kode_member').val('');
        $('#nama_member').val('');
        var kode_penjualan = $(this).val();
        $('.btn_controls').hide();
        $.ajax({
            url: "<?php echo site_url('revisi_invoice/cari_kode_penjualan') ?>",
            type: 'POST',
            dataType: 'JSON',
            data: { kode_penjualan: kode_penjualan },
            success: function(result) {
                if (typeof result.error !== 'undefined') {
                    if (!result.error) {
                        $('#kode_petugas').val(result.data.kode_petugas);
                        $('#nama_petugas').val(result.data.nama_petugas);
                        $('#kode_member').val(result.data.kode_member);
                        $('#nama_member').val(result.data.nama_member);
                        $('.btn_controls').show();
                    }
                }
            }
        })
    });
});

function hapus_invoice()
{
    $('#modal-confirm-hapus').hide();
    var kode = $('#kode_penjualan').val();
    $.ajax({
        url: "<?php echo site_url('revisi_invoice/cancel_invoice') ?>",
        type: 'POST',
        dataType: 'JSON',
        beforeSend: function() {
            $('.tunggu').show();
        },
        data: { kode_penjualan: kode },
        success: function(result) {
            if (typeof result.error !== 'undefined') {
                if (!result.error) {
                    $('#kode_petugas').val('');
                    $('#nama_petugas').val('');
                    $('#kode_member').val('');
                    $('#nama_member').val('');
                    $('.btn_controls').hide();
                    alert('Invoice telah dihapus');
                    document.location.reload();
                } else {
                  alert(result.message);
                }
            }
        },
        complete: function() {
            $('.tunggu').hide();
        }
    });
}

function revisi_invoice()
{
  var kode_penjualan = $('#kode_penjualan').val();
  var url = '<?php echo site_url('revisi_invoice/revisi') ?>/' + kode_penjualan;
  document.location.href=url;
}
</script>
