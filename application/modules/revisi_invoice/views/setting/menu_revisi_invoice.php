<?php 
   $transaksi_penjualan = $this->db->get_where('transaksi_penjualan', ['kode_penjualan' => $kode_penjualan])->row();
   $transaksi_mutasi = $this->db->get_where('transaksi_mutasi', ['kode_surat_jalan' => $transaksi_penjualan->kode_surat_jalan, 'status_mutasi' => 'Mutasi Ke Petugas'])->row();
   $member = $this->db->get_where('master_member', ['kode_member' => $transaksi_penjualan->kode_member, 'status_member' => '1'])->row();
   $opsi_penjualan = $this->db->get_where('opsi_transaksi_penjualan', ['kode_penjualan' => $transaksi_penjualan->kode_penjualan])->result();
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <style type="text/css">
    .ombo{
      width: 600px;
    }
  </style>    
  <!-- Main content -->
  <section class="content">             
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-12 connectedSortable">
        <div class="portlet box grey-steel">
          <div class="portlet-title">
            <div class="caption">
              <span style="font-size: large; color: black;" class="pull-left">Revisi Invoice
              </span>
            </div>
            <table id="col_manual" class="table table-striped table-bordered table-advance table-hover">
              <tbody >
                <form >
                  <tr style="background-color:#229fcd;">
                    <td width="100px">
                      Kategori Penjualan
                      <select class="form-control" id="jenis" disabled>
                        <!-- <option value="non member">Non Member</option> -->
                        <option value="member" <?php ($transaksi_penjualan->kategori_penjualan == 'Member')? 'selected':'' ?>>Member
                        </option>
                        <option value="sales" <?php ($transaksi_penjualan->kategori_penjualan == 'sales')? 'selected':'' ?>>Sales
                        </option>
                        <option value="sopir" <?php ($transaksi_penjualan->kategori_penjualan == 'sopir')? 'selected':'' ?>>Sopir
                        </option>
                        <option value="sales & sopir" <?php ($transaksi_penjualan->kategori_penjualan == 'sales & sopir')? 'selected':'' ?>>Sales & Sopir
                        </option>
                      </select>
                      Tanggal Invoice
                      <input type="date" id="tanggal_penjualan" class="form-control" value="<?php echo date('Y-m-d', strtotime($transaksi_penjualan->tanggal_penjualan)) ?>">
                    </td>
                    <td width="300px">
                      <div>
                          Surat Jalan
                        <input type="text" class="form-control" value="<?php echo $transaksi_penjualan->kode_surat_jalan ?>" placeholder="No. Surat Jalan" id="no_surat" disabled />
                        <input type="hidden" class="form-control" id="kode_member" value="<?php echo $transaksi_penjualan->kode_member ?>" disabled />
                        Nama Member
                        <input type="text" class="form-control" id="nama_member" value="<?php echo $transaksi_penjualan->kode_member ?> - <?php echo $transaksi_penjualan->nama_member ?>" disabled>
                        Alamat Member
                        <textarea class="form-control" id="alamat_member" disabled><?php echo $member->nama_jalur ?> - <?php echo $member->alamat_member ?></textarea>
                      </div>
                    </td>
                    <td width="300px">
                      <div class="">
                        <div class="">
                          <span class="">
                          <?php if ($transaksi_penjualan->kategori_penjualan == 'Member') { ?>
                            Nama Penerima
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_sales ?> - <?php echo $transaksi_mutasi->nama_sales ?>"/>
                            Nama Pengantar
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_unit_tujuan ?> - <?php echo $transaksi_mutasi->nama_unit_tujuan ?>"/>
                          <?php } else if($transaksi_penjualan->kategori_penjualan == 'sales') { ?>
                            Nama Sales
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_petugas ?> - <?php echo $transaksi_mutasi->nama_petugas ?>"/>
                          <?php } else if($transaksi_penjualan->kategori_penjualan == 'sopir') { ?>
                            Nama Sopir
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_petugas ?> - <?php echo $transaksi_mutasi->nama_petugas ?>"/>
                          <?php } else if($transaksi_penjualan->kategori_penjualan == 'sales & sopir') { ?>
                            Nama Sales
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_petugas ?> - <?php echo $transaksi_mutasi->nama_petugas ?>"/>
                            Nama Sopir
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_sopir ?> - <?php echo $transaksi_mutasi->nama_sopir ?>"/>
                          <?php } ?>
                            Nama Member
                            <input disabled type="text" class="form-control" value="<?php echo $transaksi_mutasi->kode_member ?> - <?php echo $transaksi_mutasi->nama_member ?>"/>
                          </span>
                        </div>
                      </div>
                    </td>
                  </tr>
                </form>
              </tbody>
            </table>
          </div>
          <div class="portlet-body">
          <form id="form_revisi" action="<?php echo site_url('revisi_invoice/simpan_revisi') ?>" method="post">
            <div class="box-body">            
              <div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none" >
                <img src="<?php echo base_url() . '/public/images/loading1.gif' ?>" >
              </div>
              <div class="sukses">
              </div>
              <div class="row">
                <div class="col-md-8">
                  <table style="white-space: nowrap; font-size: 1.5em;" id="data" class="table table-bordered  table-hover">
                    <thead>
                      <tr>
                        <th style="background-color:#229fcd; color:white" class="text-center" width="50px">No.
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Nama Produk
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center" width="125px">Qty
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center" width="125px">Harga
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center" width="125px">Harga Lusinan
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center" width="125px">Subtotal
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                      $no = 0;
                    ?>
                    <input type="hidden" name="kode_penjualan" id="kode_penjualan" value="<?php echo $kode_penjualan ?>">
                    <?php foreach ($opsi_penjualan as $item) { ?>
                      <tr id="row_<?php echo $item->id ?>">
                        <td>
                           <?php echo ++$no; ?>
                        </td>
                        <td>
                           <?php echo $item->nama_menu; ?>
                        </td>
                        <td>
                          <input type="number" id="qty_<?php echo $item->id ?>" class="form-control qty" data-id="<?php echo $item->id ?>" name="qty[<?php echo $item->id ?>]" value="<?php echo $item->jumlah ?>" onkeyup="hitungSubtotal('<?php echo $item->id ?>')">
                        </td>
                        <td>
                          <input type="number" id="harga_<?php echo $item->id ?>" class="form-control" data-id="<?php echo $item->id ?>" name="harga[<?php echo $item->id ?>]" value="<?php echo $item->harga_satuan ?>" onkeyup="hitungSubtotal('<?php echo $item->id ?>')">
                        </td>
                        <td id="harga_lusin_<?php echo $item->id ?>">
                           <?php echo format_rupiah($item->harga_satuan*12); ?>
                        </td>
                        <td id="subtotal_<?php echo $item->id ?>" data-total="<?php echo $item->harga_satuan*$item->jumlah ?>" class="subtotal">
                           <?php echo format_rupiah($item->harga_satuan*$item->jumlah); ?>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                    </form>
                  </table>
                  <div class="row">
                    <div class="form-group col-md-4"  >
                      <div class="input-group">
                        <span class="input-group-addon">All Diskon
                        </span>
                        <select hidden name="jenis_diskon_all" disabled id="jenis_diskon_all" onchange="change_diskon_all()" class="form-control">
                          <option value="persen" <?php echo ($transaksi_penjualan->diskon_persen)? 'selected':'' ?>>Persen
                          </option>
                          <option value="rupiah" <?php echo (!$transaksi_penjualan->diskon_persen)? 'selected':'' ?>>Rupiah
                          </option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group col-md-5"  >
                    <?php if($transaksi_penjualan->diskon_persen) { ?>
                      <div class="input-icon right" id="persen_all">
                        <i class="fa">%
                        </i>
                        <input type="text" name="persen" id="persen" disabled class="form-control" placeholder="Diskon Persen" value="<?php echo $transaksi_penjualan->diskon_persen ?>">
                      </div>
                    <?php } else { ?>
                      <div class="input-icon left" id="rupiah_all">
                        <i class="fa">Rp.
                        </i>
                        <input type="text" name="rupiah" id="rupiah" disabled class="form-control" placeholder=" Diskon Rupiah" value="<?php echo $transaksi_penjualan->diskon_rupiah ?>">
                      </div>
                    <?php } ?>
                    </div>
                    <!-- <div class="form-group col-md-6">
<div class="input-group">
<span class="input-group-addon">Diskon(%)</span>
<span id="dibayar">
<input type="text" onkeyup="diskon_persen()" onclick="this.select();" value="0" class="form-control" name="persen" id="persen" />
</span>
</div>
</div>
<div class="form-group col-md-6">
<div class="input-group">
<span class="input-group-addon">Diskon(Rp)</span>
<span id="dibayar">
<input type="text" onkeyup="diskon_rupiah()" onclick="this.select();" class="form-control" value="0" name="rupiah" id="rupiah" />
</span>
</div>
</div> -->
                  </div>
                </div>
                <div style="margin-top: 0px;" class="col-md-4">
                <!--
                  <div class="bg-yellow" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                    <span style="font-size:22px; " class="pull-right" id="total_pesanan">
                    </span>
                    <input type="hidden" id="total_pesanan_hidden">
                    <p style="font-size: 18px;">Total Pesanan
                    </p>
                  </div>
                  <div class="bg-red" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                    <span style="font-size:22px; " class="pull-right" id="diskon_all">Rp 0
                    </span>
                    <i style="font-size:56px; margin-top:5px">
                    </i>
                    <p style="font-size: 18px;">Discount
                    </p>
                  </div>
                  <div class="bg-green" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                    <span style="font-size:22px; " class="pull-right" id="potong_tagihan">Rp 0
                    </span>
                    <i style="font-size:56px; margin-top:5px">
                    </i>
                    <p style="font-size: 18px;">Potong Tagihan
                    </p>
                  </div>
                -->
                  <div class="bg-blue" style="height:40px; padding: 0px 10px 0px 10px; margin-bottom:5px">
                    <span style="font-size:22px; " class="pull-right" id="grand_total">Rp 0
                    </span>
                    <i style="font-size:56px; margin-top:5px">
                    </i>
                    <p style="font-size: 18px;">Grand Total
                    </p>
                  </div>
                  <div style="height:60px; margin-top:5px">
                    <div style="height: 40px;" class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">Jenis Transaksi
                        </span>
                        <span id="golongan">
                          <select class="form-control" id="jenis_transaksi" name="jenis_transaksi">
                            <option selected="" value="tunai">Tunai
                            </option>
                            <option value="kredit">Kredit
                            </option>
                            <option value="konsinyasi">Konsinyasi
                            </option>
                          </select>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div id="div_jatuh_tempo" style="height:60px; margin-top:-20px">
                    <div style="height: 40px;" class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">Jatuh Tempo &nbsp;&nbsp;&nbsp;
                        </span>
                        <span id="golongan">
                          <input type="date" class="form-control" name="jatuh_tempo" id="jatuh_tempo" value="<?php echo date('Y-m-d', strtotime("+30 days")) ?>">
                        </span>
                      </div>
                    </div>
                  </div>
                  <!--
                  <div style="height:60px; margin-top: -20px;">
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon" style="font-size: x-large;font-weight: bolder;width: 100px">
                          <strong >Dibayar &nbsp;
                          </strong>
                        </span>
                        <span id="dibayar">
                          <input type="hidden" id="total_no" />
                          <input type="hidden" id="total2" />
                          <input type="hidden" id="potong_tagihan_hidden" />
                          <input type="hidden" id="kembalian2" />
                          <input style="font-size: 30px;" onkeyup="kembalian()" type="text" class="form-control input-lg" name="bayar" id="bayar" />
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="bg-purple" style="height:40px; padding: 0px 10px 0px 10px; margin-top:-5px">
                    <span style="font-size:26px; " class="pull-right totalDiskon" id="kembalian">Rp 0
                    </span>
                    <i style="font-size:56px; margin-top:5px">
                    </i>
                    <p id="text_dibayar" style="font-size: 18px;">Kembalian
                    </p>
                  </div>
                  -->
                  <!--<div class="bg-green" style="height:55px; padding: 10px 10px 0px 10px; margin-top:35px">
<button class="btn btn-large btn-default" style="width:100%" onclick="list_suspend()">LIST PESANAN</button>
</div>-->
                </div>
                <br />
                <div id="rupiah_bayar" style="padding-left: 500px;margin-top:50px;font-size:35px" class="pull-right col-md-12">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <input type="hidden" id="id_meja" value="<?php echo $this->uri->segment(3); ?>" />
                  <input type="hidden" id="hasil_meja" />
                </div>
              </div>
              </form>
              <br />
              <br />
              <!-- <div id="data_pengiriman" class="row" style=" margin-top: -10px;">
<fieldset>
<div class="col-md-4">
<div class="form-group">
<label>Nama Penerima</label>
<input placeholder="Nama" type="text" class="form-control" id="penerima" />
</div>
<div class="form-group">
<label>Jam Pengiriman</label>
<input placeholder="No. Telp" type="text" class="form-control timepicker" id="jam_kirim" />
</div>
</div>
<div class="col-md-4">
<div class="form-group">
<label>No. Telp</label>
<input placeholder="No. Telp" type="text" class="form-control" id="no_telp" />
</div>
<div class="form-group">
<label>Tanggal Pengiriman</label>
<input placeholder="Tanggal" type="date" class="form-control" id="tgl_kirim" />
</div>
</div>
<div class="col-md-4">
<div class="form-group">
<label>Alamat</label>
<textarea id="alamat" class="form-control" placeholder="Alamat"></textarea>
</div>
</div>
</fieldset>
</div> -->
              <div class="row">
                <div class="col-md-12 text-center">
                  <h3>Retur Potong Tagihan</h3>
                </div>
                <div class="col-md-12 sukses_retur_pt">
                </div>
                <div class="col-md-12">
                  <table id="tb_form_retur_pt" class="table table-striped table-bordered table-advance table-hover">
                    <tbody>
                      <form id="form_retur_pt">
                        <tr>
                          <td width="250px" style="background-color:#229fcd;">
                            <input type="text" name="retur_pt_id_penjualan" id="retur_pt_id_penjualan" value="" hidden/>
                            <?php
                              $this->db->where('status', 'sendiri');
                              $menu_resto = $this->db->get('master_bahan_jadi');
                              $hasil_menu = $menu_resto->result();
                            ?>
                            <select name="retur_pt_menu" id="retur_pt_menu" class="form-control select2" onchange="clearOldStepReturPT();">
                              <option value="" selected="true">--Pilih Produk--
                              </option>
                              <?php
                                foreach($hasil_menu as $daftar){
                                ?>
                              <option value="<?php echo $daftar->kode_bahan_jadi; ?>">
                                <?php echo $daftar->nama_bahan_jadi; ?>
                              </option>
                              <?php } ?>
                            </select>
                          </td>
                          <td width="100px" style="background-color:#229fcd;">
                            <input type="number" name="retur_pt_qty" onclick="this.select();" id="retur_pt_qty" onkeyup="get_kategori_harga_retur_pt()" class="form-control" placeholder="jumlah" />
                          </td>
                          <td width="100px" style="background-color:#229fcd;">
                            <input type="number" name="retur_pt_harga" id="retur_pt_harga" onkeyup="trigger_harga_pcs('retur_pt_harga', 'retur_pt_harga_lusin', 'retur_pt_qty', 'retur_pt_subtotal')" onchange="trigger_harga_pcs('retur_pt_harga', 'retur_pt_harga_lusin', 'retur_pt_qty', 'retur_pt_subtotal')" class="form-control" placeholder="harga/pcs">
                          </td>
                          <td width="100px" style="background-color:#229fcd;">
                            <input type="number" name="retur_pt_harga_lusin" id="retur_pt_harga_lusin" onkeyup="trigger_harga_lusin('retur_pt_harga_lusin', 'retur_pt_harga', 'retur_pt_qty', 'retur_pt_subtotal')" onchange="trigger_harga_lusin('retur_pt_harga_lusin', 'retur_pt_harga', 'retur_pt_qty', 'retur_pt_subtotal')" class="form-control" placeholder="harga/lusin">
                          </td>
                          <td width="100px" style="background-color:#229fcd;">
                            <input type="number" name="retur_pt_subtotal" id="retur_pt_subtotal" disabled class="form-control" placeholder="subtotal">
                          </td>
                          <td width="75px" style="background-color:#229fcd;" >
                            <div onclick="simpan_retur_pt_temp()" class="btn purple">Add
                            </div>
                          </td>
                        </tr>
                      </form>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  <table style="white-space: nowrap; font-size: 1.5em;" id="data" class="table table-bordered  table-hover">
                    <thead>
                      <tr>
                        <th style="background-color:#229fcd; color:white" class="text-center">No.
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Nama Produk
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Qty
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Harga
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Subtotal
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Action
                        </th>
                      </tr>
                    </thead>
                    <tbody id="table_retur_pt_temp">
                      <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-center">
                  <h3>Retur Barang</h3>
                </div>
                <div class="col-md-12 sukses_retur_brg">
                </div>
                <div class="col-md-12">
                  <table id="tb_form_retur_brg" class="table table-striped table-bordered table-advance table-hover">
                    <tbody>
                      <form id="form_retur_brg">
                        <tr>
                          <td width="250px" style="background-color:#229fcd;">
                            <input type="text" name="retur_brg_id_penjualan" id="retur_brg_id_penjualan" value="" hidden/>
                            <?php
                              $this->db->where('status', 'sendiri');
                              $menu_resto = $this->db->get('master_bahan_jadi');
                              $hasil_menu = $menu_resto->result();
                            ?>
                            <select name="retur_brg_menu" id="retur_brg_menu" class="form-control select2">
                              <option value="" selected="true">--Pilih Produk--
                              </option>
                              <?php
                                foreach($hasil_menu as $daftar){
                                ?>
                              <option value="<?php echo $daftar->kode_bahan_jadi; ?>">
                                <?php echo $daftar->nama_bahan_jadi; ?>
                              </option>
                              <?php } ?>
                            </select>
                          </td>
                          <td width="100px" style="background-color:#229fcd;">
                            <input type="number" name="retur_brg_qty" onclick="this.select();" id="retur_brg_qty" class="form-control" placeholder="jumlah" />
                          </td>
                          <td width="75px" style="background-color:#229fcd;" >
                            <div onclick="simpan_retur_brg_temp()" class="btn purple">Add
                            </div>
                          </td>
                        </tr>
                      </form>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12">
                  <table style="white-space: nowrap; font-size: 1.5em;" id="data" class="table table-bordered  table-hover">
                    <thead>
                      <tr>
                        <th style="background-color:#229fcd; color:white" class="text-center">No.
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Nama Produk
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Qty
                        </th>
                        <th style="background-color:#229fcd; color:white" class="text-center">Action
                        </th>
                      </tr>
                    </thead>
                    <tbody id="table_retur_brg_temp">
                      <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <a style="text-decoration: none;" onclick="konfirm_bayar()"  class="bg-green btn col-md-12">
                    <center>
                      <span style="font-size:35px; font-weight: bold; ">
                        <i style="font-size: 35px;" class="fa fa-money">
                        </i> Bayar
                      </span>
                    </center> 
                  </a>
                </div>
              </div>
              <br />
              <br />
              <!------------------------------------------------------------------------------------------------------>
            </div>
          </div>
          <!-- /.row (main row) -->
          </section>
        <!-- /.content -->
        </div>
    </div>  
    </div>
  </section>
<!-- /.Left col -->      
</div>
<!-- /.row (main row) -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div id="modal-confirm-bayar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Revisi
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan dengan data tersebut ?
        </span>
        <input id="no" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button id="ya" onclick="bayar()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<!-------------------------------------- modal next -------------------------------------->
<div id="show_modal_next" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin Data tersebut ?
        </span>
        <input id="no" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button id="ya" onclick="next_step()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<!-------------------------------------- modal cancel -------------------------------------->
<div id="show_modal_cancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin Membatalkan Data Ini ?
        </span>
        <input id="no" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button id="tidak" class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button id="ya" onclick="clear_temp()()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus pesanan tersebut ?
        </span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button onclick="delData()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<div id="modal-delete-pt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus pesanan tersebut ?
        </span>
        <input id="id-delete-pt" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button onclick="delDataPT()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<div id="modal-delete-brg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        </button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data
        </h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus pesanan tersebut ?
        </span>
        <input id="id-delete-brg" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak
        </button>
        <button onclick="delDataBrg()" class="btn green">Ya
        </button>
      </div>
    </div>
  </div>
</div>
<script>

function currencyFormat(num) {
  return 'Rp ' + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ',00';
}

function hitungSubtotal(id) {
  var qty = parseInt($('#qty_'+id).val());
  var harga = parseInt($('#harga_'+id).val());
  if (isNaN(harga)) harga = 0;
  if (isNaN(qty)) {
    qty = 0;
  }
  $('#qty_'+id).val(qty);
  $('#harga_'+id).val(harga);
  $('#harga_lusin_'+id).html(currencyFormat(harga*12));
  $('#subtotal_'+id).data('total', parseInt(qty * harga));
  $('#subtotal_'+id).html(currencyFormat(qty * harga));
  hitung_grand_total();
}

function hitung_grand_total()
{
  var grand_total = 0;
  $('.subtotal').each(function( key, value ) {
    grand_total += $(this).data('total');
  });
  $('#grand_total').data('total', grand_total);
  $('#grand_total').html(currencyFormat(grand_total));
}

function konfirm_bayar()
{
  $('#modal-confirm-bayar').modal('show');
}

function bayar()
{
    var url = "<?php echo base_url().'revisi_invoice/simpan_revisi'; ?>";
    var formData = new FormData($('#form_revisi')[0]);
    var kode_penjualan = $('#kode_penjualan').val();
    formData.append('tanggal_penjualan', $('#tanggal_penjualan').val());
    $.ajax({
      type: 'POST',
      url: url,
      data: formData,
      processData: false,
      contentType: false,
      success: function(hasil) {
        $("#modal-confirm-bayar").modal('hide');
        $(".sukses").html('<div style="font-size:1.5em" class="alert alert-success">Revisi Invoice Berhasil</div>');
        var link = "<?php echo base_url('kasir/kasir/cetak_pembayaran'); ?>";
        setTimeout(function() {
          $('.sukses').html('');
          window.location = "<?php echo base_url() . 'revisi_invoice/' ?>";
          window.open("<?php echo base_url() . 'revisi_invoice/print_revisi_invoice/' ?>" + kode_penjualan);
        }
                    , 1000);
      }
    });
}

function trigger_harga_pcs(harga, harga_lusin, qty, subtotal) {
  var val = $('#'+harga).val() || 0;
  var qty = $('#'+qty).val() || 0;
  if ($('#'+harga).is(':focus')) {
    if ((val.match(/^[0-9.,]+$/))) {
      $('#'+harga).val(val);
      val = parseFloat(val);
      $('#'+harga_lusin).val(parseFloat(val*12));
      $('#'+subtotal).val(parseFloat(val*qty));
    } else {
      val = parseFloat(val);
      $('#'+harga).val(val);
      $('#'+harga_lusin).val(parseFloat(val*12));
      $('#'+subtotal).val(parseFloat(val*qty));
    }
  }
}

function trigger_harga_lusin(harga_lusin, harga, qty, subtotal) {
  var val = $('#'+harga_lusin).val() || 0;
  var qty = $('#'+qty).val() || 0;
  if ($('#'+harga_lusin).is(':focus')) {
    if ((val.match(/^[0-9.,]+$/))) {
      $('#'+harga_lusin).val(val);
      val = parseFloat(val);
      $('#'+harga).val(parseFloat(val/12));
      $('#'+subtotal).val(parseFloat(val*qty));
    } else {
      val = parseFloat(val);
      $('#'+harga_lusin).val(val);
      $('#'+harga).val(parseFloat(val/12));
      $('#'+subtotal).val(parseFloat(val*qty));
    }
  }
}

function get_kategori_harga_retur_pt(){
    var url = "<?php echo base_url().'kasir/get_kategori_harga'; ?>";
    var qty = $("#retur_pt_qty").val();
    var id_menu = $("#retur_pt_menu").val();
    var jenis=$('#jenis').val();
    if(jenis=='member'){
      var kode_member = $("#kode_member").val();
    }
    else{
      var kode_member = $("#kode_member_sales").val();
    }
    $.ajax( {
      type:"POST", 
      url : url,  
      cache :false,
      data :{
        id_menu:id_menu,qty:qty,kode_member:kode_member}
      ,
      dataType : 'json',
      success : function(data) {
        if(data==''){
          $("#retur_pt_harga").val('0');
          $("#retur_pt_harga_lusin").val('0');
          $("#retur_pt_subtotal").val('0');
        }
        else{
          $("#retur_pt_harga").val(data.harga);
          $("#retur_pt_harga_lusin").val(data.harga*12);
          $("#retur_pt_subtotal").val(data.harga*qty);
        }
      }
      ,  
    }
          );
  }

function simpan_retur_pt_temp()
{
  var url = "<?php echo base_url().'revisi_invoice/simpan_retur_pt_temp'; ?>";
  var kode_penjualan = $("#kode_penjualan").val();
  var tanggal_penjualan = $("#tanggal_penjualan").val();
  var menu = $("#retur_pt_menu").val();
  var jumlah = $("#retur_pt_qty").val();
  var harga = $("#retur_pt_harga").val();
  var jenis = $("#jenis").val();
  var no_surat = $("#no_surat").val();
  if(jumlah < 1 || menu==""){
    $(".sukses_retur_pt").html("<div class='alert alert-warning'>Jumlah Barang Salah</div>");
    setTimeout(function(){
      $('.sukses_retur_pt').html('');
    }
                ,1500);
  }
  else{
    $.ajax({
      type: "POST",
      url: url,
      cache: false,
      data: {
        no_surat: no_surat,
        kode_penjualan: kode_penjualan,
        kode_menu: menu,
        jumlah: jumlah,
        jenis: jenis,
        harga_satuan: harga,
      }
      ,
      beforeSend:function(){
        $(".tunggu").show();
      }
      ,
      success : function(data) {
        pesan=data.split("|");
        $(".tunggu").hide();
        if(pesan[0]==1){
          $(".sukses_retur_pt").html(pesan[1]);
          setTimeout(function(){
            $(".sukses_retur_pt").html('');
          }
                      ,1700);
        }
        else{
          $("#table_retur_pt_temp").load('<?php echo base_url().'kasir/retur_pt_temp/'; ?>'+kode_penjualan);
          $("#retur_pt_menu").select2().select2('val', '');
          $("#retur_pt_qty").val('');
          $("#retur_pt_harga").val('');
          $("#retur_pt_harga_lusin").val('');
          $("#retur_pt_subtotal").val('');
        }
      }
      ,  
      error : function(data) {
        pesan=data.split("|");
        // alert(pesan[1]);
      }
    }
          );
  }
}

function simpan_retur_brg_temp()
{
  var url = "<?php echo base_url().'revisi_invoice/simpan_retur_brg_temp'; ?>";
  var kode_penjualan = $("#kode_penjualan").val();
  var tanggal_penjualan = $("#tanggal_penjualan").val();
  var menu = $("#retur_brg_menu").val();
  var jumlah = $("#retur_brg_qty").val();
  var jenis = $("#jenis").val();
  var no_surat = $("#no_surat").val();
  if(jumlah < 1 || menu==""){
    $(".sukses_retur_pt").html("<div class='alert alert-warning'>Jumlah Barang Salah</div>");
    setTimeout(function(){
      $('.sukses_retur_pt').html('');
    }
                ,1500);
  }
  else{
    $.ajax({
      type: "POST",
      url: url,
      cache: false,
      data: {
        no_surat: no_surat,
        kode_penjualan: kode_penjualan,
        kode_menu: menu,
        jumlah: jumlah,
        jenis: jenis,
      }
      ,
      beforeSend:function(){
        $(".tunggu").show();
      }
      ,
      success : function(data) {
        pesan=data.split("|");
        $(".tunggu").hide();
        if(pesan[0]==1){
          $(".sukses_retur_brg").html(pesan[1]);
          setTimeout(function(){
            $(".sukses_retur_brg").html('');
          }
                      ,1700);
        }
        else{
          $("#table_retur_brg_temp").load('<?php echo base_url().'revisi_invoice/retur_brg_temp/'; ?>'+kode_penjualan);
          $("#retur_brg_menu").select2().select2('val', '');
          $("#retur_brg_qty").val('');
        }
      }
      ,  
      error : function(data) {
        pesan=data.split("|");
        // alert(pesan[1]);
      }
    }
          );
  }
}

function delDataPT() {
  var id = $('#id-delete-pt').val();
  var url = '<?php echo base_url().'kasir/kasir/hapus_retur_pt_temp'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id:id
    }
    ,
    success: function(msg) {
      var kode_penjualan = $("#kode_penjualan").val()
      $('#modal-delete-pt').modal('hide');
      $("#table_retur_pt_temp").load('<?php echo base_url().'kasir/kasir/retur_pt_temp/'; ?>'+kode_penjualan);
      totalan();
      totalan_pt();
      grand_total();
    }
  }
        );
  return false;
}

function delDataBrg() {
  var id = $('#id-delete-brg').val();
  var url = '<?php echo base_url().'revisi_invoice/hapus_retur_brg_temp'; ?>';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id:id
    }
    ,
    success: function(msg) {
      var kode_penjualan = $("#kode_penjualan").val()
      $('#modal-delete-brg').modal('hide');
      $("#table_retur_brg_temp").load('<?php echo base_url().'revisi_invoice/retur_brg_temp/'; ?>'+kode_penjualan);
    }
  }
        );
  return false;
}

function actDeleteReturPT(Object) {
  $('#id-delete-pt').val(Object);
  $('#modal-delete-pt').modal('show');
}

function actDeleteReturBrg(Object) {
  $('#id-delete-brg').val(Object);
  $('#modal-delete-brg').modal('show');
}

hitung_grand_total();

</script>