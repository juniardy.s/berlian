<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class transaksional extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function index()
	{
		$data['konten'] = $this->load->view('setting/daftar', NULL, TRUE);
		$this->load->view ('main', $data);
	}

	public function daftar()
	{
		$data['konten'] = $this->load->view('setting/daftar', NULL, TRUE);
		$this->load->view ('main', $data);		
	}

	public function riwayat_pembelian(){
		$data['konten'] = $this->load->view('setting/daftar_trns_pembelian', NULL, TRUE);
		$this->load->view ('main', $data);	
	}
	public function riwayat_penjualan(){
		$data['konten'] = $this->load->view('setting/daftar_trns_penjualan', NULL, TRUE);
		$this->load->view ('main', $data);	
	}
	public function riwayat_penjualan_detail(){
		 $this->load->view('setting/daftar_trns_penjualan_detail');
	}
    public function load_toko_from_jalur($jalur){   

        if ($jalur != '0') {
            $this->db->where(['kode_jalur' => $jalur]);
        }
        $query  =   $this->db->get('master_member'  )->result();


        echo "<option value='0' selected> Pilih semua Toko</option>";
        foreach ($query as $val) {
            echo "<option value='".$val->kode_member."'>".$val->kode_member.' - '.$val->nama_member."</option>";
        }
    }

    public function table_load_penjualan($jenis=null, $toko=null , $jalur=null , $sales=null){

        if ($jenis == 'mutasi') {

            if ($jalur != '0') {
                $this->db->where(['kode_jalur' => $jalur]);
            }

            if ($sales != '0') {
                $this->db->where(['kode_unit_tujuan' => $sales]);  
            }
        
            $this->db->where(['nama_unit_asal' => 'Gudang']);  

            $query  =   $this->db->order_by('tanggal_transaksi', 'DESC')->get('transaksi_mutasi')->result();

        } else {

            if ($toko != '0') {
                $this->db->where(['kode_member' => $toko]);
            }

            if ($sales != '0') {
                $this->db->where(['kode_petugas' => $sales]);  
            }


            $query  =   $this->db->order_by('tanggal_penjualan', 'DESC')->order_by('jam_penjualan', 'DESC')->group_by('kode_penjualan')->get('transaksi_penjualan')->result();
        }

        $return     =   [
            'data' => $query,
            'tipe' => $jenis 
        ];
        
        // echo $this->db->last_query();
        // exit();
        $this->load->view('setting/penjualan_transaksi', $return);


    }

	public function list_penjualan_serverside(){

		$select = '*';
        $tbl = 'transaksi_penjualan';
        //LIMIT
        $limit = array(
            'start'  => $this->input->get('start'),
            'finish' => $this->input->get('length')
        );
        //WHERE LIKE
        $where_like['data'][] = array(
            'column' => 'memo_detail_qty,memo_detail_discount',
            'param'  => $this->input->get('search[value]')
        );
        //ORDER
        $index_order = $this->input->get('order[0][column]');
        $order['data'][] = array(
            'column' => $this->input->get('columns['.$index_order.'][name]'),
            'type'   => $this->input->get('order[0][dir]')
        );

        $where['data'][]=array(
            'column'    =>'a.status_delete',
            'param'     => 0
            );

        $join['data'][] = array(
            'table'     => 'memos d',
            'join'      => 'a.memo_id = d.memo_id',
            'type'      => 'left'
        );


        $join['data'][] = array(
            'table'     => 'items b',
            'join'      => 'a.item_id = b.item_id',
            'type'      => 'left'
        );

        $join['data'][] = array(
            'table'     => 'item_packages c',
            'join'      => 'a.item_id = c.item_package_id',
            'type'      => 'left'
        );

        $orderby = "memo_date asc";


        $query_total    = $this->Master->data_table_load($select,$tbl,NULL,NULL,NULL,$join,$where,$where2,null,$orderby);
        $query_filter   = $this->Master->data_table_load($select,$tbl,NULL,$where_like,$order,$join,$where,$where2,null,$orderby);
        $query          = $this->Master->data_table_load($select,$tbl,$limit,$where_like,$order,$join,$where,$where2,null,$orderby);
        $query_smallest = $this->Master->data_table_load($select,$tbl,array('start' => 0 , 'finish'=>1),$where_like,$order,$join,$where,$where2,null,$orderby);

        $response['data'] = array();
        if ($query<>false) {
            $no = $limit['start']+1;
            $angka  =   1;
            $hasil  =   $query_smallest->result_array();

            $terkecil   =   $hasil[0]['memo_date'];

            // $the_smallest   =   $terkecil;
            foreach ($query->result() as $val) {
                if ($val->memo_detail_id>0) {

                    $current_permintaan     =   $val->memo_detail_qty - $val->memo_accumulation;

                    $button     =  
                    '<button type="button" class="btn btn-danger btn-outline-info" onclick="delete_nota_detail('.$val->memo_detail_id.')" ><i class="icofont icofont-delete-alt"></i></button>';                  
                    
                    $input      =   '<input type="number" onchange="get_price(this.value , '.$val->memo_detail_id.')" id="detail'.$val->memo_detail_id.'" value="'.$val->memo_detail_fill.'">';
                    $hidden      =   '<input type="hidden" id="hidden_'.$val->memo_detail_id.'" >';

                    if ($val->type_item_id == 1) {
                        $name   = $val->item_name. ' - ' .$val->item_code;
                        $price  = $val->item_het;   
                    } else {
                        $name   = $val->item_package_name. ' - ' .$val->item_package_code;
                        $price  = $val->item_package_het;
                    }

                    if ($val->memo_date == $terkecil) {
                        $name   = "<p style='color:red'>".$name."</p";   
                    }


                    $hidden      .=    '<input type="hidden" id="price'.$val->memo_detail_id.'" value="'.$price.'">'.'<input type="hidden" id="discitem'.$val->memo_detail_id.'" value="'.$val->memo_detail_discount.'">';    
                    // $hidden      .=    ;
                    $disc_het    = ($price * $val->memo_detail_discount) / 100 ;
                    $het_setelah_disc   =   $price - $disc_het;



                    $subtotal   =  $val->memo_accumulation * $val->memo_detail_price ;

                    $response['data'][] = array(
                        
                        // $hidden.$val->memo_date, 
                        $hidden,
                        $name,
                        $val->memo_detail_qty - $val->memo_accumulation,
                        $val->memo_detail_discount,
                        number_format($price),
                        number_format($het_setelah_disc),
                        number_format($subtotal),
                        $input,
                        $button 
                    );
                    $no++;  
                    $angka++;
                }
            }
        }

        $response['recordsTotal'] = 0;
        if ($query_total<>false) {
            $response['recordsTotal'] = $query_total->num_rows();
        }
        $response['recordsFiltered'] = 0;
        if ($query_filter<>false) {
            $response['recordsFiltered'] = $query_filter->num_rows();
        }

        $response['last_query']     =   $this->db->last_query();

        echo json_encode($response);

    }
    
    public function print_nota_retur()
    {
        $this->load->view('setting/print_nota_retur');       
    }
}
