<!DOCTYPE html>
<html>
<head>
	<title>PRINT RETUR TUKAR BARANG</title>
	<link href="<?php echo site_url('public/fonts/saxmono.css') ?>" rel="stylesheet" type="text/css" />
	<style type="text/css">
		html, body {
			display: block;
			font-family: 'saxMono', "Calibri" !important;
		}

		table {
			font-size: auto;
		}

		.table1{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 0px;
			text-align: left;
			vertical-align: middle;
		}

		.table2{
			width:100%; 
			border-collapse: collapse;
		}

		.table3{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 160px;
			text-align: left;
			vertical-align: middle;
		}

		@media print {
			html, body {
				display: block;
				font-family: 'saxMono', "Calibri" !important;
			}

			table {
				font-size: 12px;
			}

			@page
			{
				/* size: 21cm 14cm; */
				size: 9.5in 11in;
			}

		}

		div.page { page-break-after: always;
			position: relative;
			margin:10px 15px 0px 15px;
			padding:0px; }

		</style>
	</head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php
	$param=$this->uri->segment(3);
	$transaksi = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$param));
	$hasil_transaksi = $transaksi->row();
	$dtmember = $this->db->get_where('master_member', array('kode_member' => $hasil_transaksi->kode_member))->row();
	$diskon = 0;
	if($hasil_transaksi->diskon_persen > 0) {
		$diskon = $hasil_transaksi->grand_total - ($hasil_transaksi->grand_total * $hasil_transaksi->diskon_persen / 100);
	} else {
		$diskon = $hasil_transaksi->diskon_rupiah;
	}

	$get_retur = $this->db->get_where('transaksi_retur_penjualan', array('kode_penjualan' => $hasil_transaksi->kode_penjualan))->row();
	$diskon += @$get_retur->grand_total;
	?>
	<style>
		html, body {
			font-weight: 600;
			font-family: Arial, Helvetica, sans-serif !important;
		}
	</style>
	<body onload="print()">
		<table width="100%">
			<tr>
				<td colspan="2"><h4><b>PT. BERLIAN</b></h4></td>
				<td colspan="2" align="right"><h4><b>RETUR TUKAR BARANG</b></h4></td>
			</tr>
			<tr>
				<td colspan="2" width="50%">Jl. Gurang Anyar No 17 - 19, Cerme, Gresik</td>
				<td width="15%">TANGGAL</td>
				<td width="45%">: <?php echo tanggalIndo(date('Y-m-d')); ?></td>
			</tr>
			<tr>
				<td colspan="2">TELP. : (031) 7991119 / 08123219722 FAX. (031) 7991597</td>
				<td valign="top">Kepada Yth</td>
				<td>: <?php echo $dtmember->nama_member; ?></td>
			</tr>
			<tr>
				<td colspan="2" valign="top">EMAIL : Berlian_broom@yahoo.com</td>
				<td></td>
				<td> <?php echo $dtmember->alamat_member; ?></td>
			</tr>
		</table>
		<table>
			<tr>
				<td>NO NOTA</td>
				<td width="40%">: <?php echo $hasil_transaksi->kode_penjualan; ?></td>
				<td>NO SJ</td>
				<td width="40%">: <?php echo $hasil_transaksi->kode_surat_jalan; ?></td>
			</tr>
		</table>
		<?php
			$opsi_transaksi = $this->db->get_where('opsi_transaksi_retur_penjualan',array('kode_penjualan'=>$param, 'jenis_retur' => 'tukar_barang'));
			$hasil_opsi_transaksi = $opsi_transaksi->result();
			if (!empty($hasil_opsi_transaksi)) {
		?>
		<table border="1" width="100%" class="table table-bordered">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">NAMA BARANG</th>
					<th style="text-align: center;">JUMLAH</th>
					<th style="text-align: center;">NAMA BARANG (TUKAR)</th>
					<th style="text-align: center;">JUMLAH (TUKAR)</th>
				</tr>
			</thead>
			<tbody>	
				<?php
				$no=1;
				$total = 0 ;
				foreach ($hasil_opsi_transaksi as  $value) {
					$total += $value->subtotal;
					$this->db->where('status','sendiri');
					$get_bahan = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi' => $value->kode_produk ))->row();
					if ((@$value->jumlah % 12) == 0) {
						$qty = @$value->jumlah/12;
						$satuan = "lusin";
					} else {
						$qty = @$value->jumlah;
						$satuan = $get_bahan->satuan_stok;
					}

					$this->db->where('status','sendiri');
					$get_bahan_konversi = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi' => $value->kode_produk ))->row();
					if ((@$value->jumlah_konversi % 12) == 0) {
						$qty_konversi = @$value->jumlah_konversi/12;
						$satuan_konversi = "lusin";
					} else {
						$qty_konversi = @$value->jumlah_konversi;
						$satuan_konversi = $get_bahan_konversi->satuan_stok;
					}
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td align="center" ><?php echo @$value->nama_produk;?></td>
						<td align="center" ><?php echo @$qty;?> <?php echo ($satuan == 'pieces')? 'biji' : $satuan; ?></td>
						<td align="center" ><?php echo @$value->nama_produk_konversi;?></td>
						<td align="center" ><?php echo @$qty_konversi;?> <?php echo ($satuan == 'pieces')? 'biji' : $satuan_konversi; ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
		<?php 
			}
		?>
        <table width="100%">
			<tr>
				<td colspan="2"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="30%"></td>
				<td width="20%"></td>
				<td width="25%" align="center" valign="top" style="font-size: 16px">PENGIRIM</td>
				<td width="25%" align="center" valign="top" style="font-size: 16px">PENERIMA</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td align="center">(.........................)</td>
				<td align="center">(.........................)</td>
			</tr>
	</body>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</html>