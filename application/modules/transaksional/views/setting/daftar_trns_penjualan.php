<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
$('.btn-back').click(function(){
  $(".tunggu").show();
  window.location = "<?php echo base_url().'transaksional'; ?>";
});
</script>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            Widget settings form goes here
          </div>
          <div class="modal-footer">
            <button type="button" class="btn blue">Save changes</button>
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    <div class="theme-panel">
    </div>
            <!-- END STYLE CUSTOMIZER -->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
              Transaksional</h3>
              <div class="page-bar">
                <ul class="page-breadcrumb">
                  <li>
                    <i class="fa fa-home"></i>
                    <a href="#">Home</a>
                    <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                    <a href="<?php echo base_url() ?>transaksional">Transaksional</a>
                    <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                    <a href="#">Riwayat Penjualan</a>
                  </li>
                </ul>
                <div class="page-toolbar">
                  <!-- <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range"> -->
                    <!-- <i class="icon-calendar"></i> -->
                    <!-- &nbsp;&nbsp; <i class="fa fa-angle-down"></i> -->
                  </div>
                </div>
              <section class="connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Daftar Riwayat Penjualan
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>

            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>


            <div class="box-body">            
              <div class="sukses"></div>
              <!-- <div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none">
                <img src="http://localhost/berlian//public/img/loading.gif">
              </div> -->
              <form id="pencarian_form" method="post" style="margin-left: 18px;" class="form-horizontal" target="_blank">

                <div class="row">
                  <div class="col-md-12" id="trx_penjualan">
                    <div class="input-group">
                      <label> <b> Tipe :</b></label>
                      <input type="radio" value="mutasi" checked name="tipe_transaksi" id="mutasi">  
                      <label for="mutasi"> Mutasi </label>
                      <input type="radio" value="penjualan" name="tipe_transaksi" id="penjualan">  
                      <label for="penjualan"> Penjualan </label>
                    </div>
                  </div>
                  <!-- <div class="col-md-4" id="trx_penjualan">
                    <div class="input-group">
                      <span class="input-group-addon">Tanggal Akhir</span>
                      <input type="date" class="form-control tgl" id="tgl_akhir">
                    </div>
                  </div> -->

                  
                </div>
                <?php 
                  $jalur   = $this->db->get_where('master_jalur' , ['status' => 1] )->result();
                  $toko  = $this->db->get('master_member' )->result();
                  $sales  = $this->db->get_where('master_sales' , ['status' => 1])->result();
                 ?>
                <div class="row">
                  <div class="col-md-3" id="">
                    <div class="input-group" >
                      <span class="input-group-addon">Pilih Sales</span>
                      <select class="select2 form-control" id="sales_kode">
                        <option selected value='0'>Semua Sales </option>
                        <?php 
                          foreach ($sales as $val) {
                            ?>
                            <option value="<?php echo $val->kode_sales ?>"><?php echo $val->kode_sales . ' - '.$val->nama_sales ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3" id="kolom_jalur">
                    <div class="input-group" id="mutasi">
                      <span class="input-group-addon">Pilih Jalur</span>
                      <select class="select2 form-control" onchange="get_member(this.value)" id="jalur_kode">
                        <option selected value='0'> Semua Jalur </option>
                        <?php 
                          foreach ($jalur as $val) {
                            ?>
                            <option value="<?php echo $val->kode_jalur ?>"><?php echo $val->kode_jalur .' - '.$val->nama_jalur ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3" >
                    <div class="input-group" id="kolom_toko">
                      <span class="input-group-addon">Pilih Toko</span>
                      <select class="select2 form-control "  id="member_kode">
                        <option selected value='0'>Semua Toko</option>
                         <?php 
                          foreach ($toko as $val) {
                            ?>
                            <option value="<?php echo $val->kode_member ?>"><?php echo $val->kode_member .' - '.$val->nama_member ?></option>
                            <?php
                          }
                         ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class=" col-md-4">
                    <div class="input-group">
                      <button type="button" class="btn btn-success" onclick="load_table_daftar()"><i class="fa fa-search"></i> Cari</button>

                    </div>
                  </div>
                </div>

                <!-- <br> -->
                <hr>

              </form>

              <table class="table table-hover table-bordered" id="table_hasil">
                
              </table>



              <?php
                //$kasir = $this->db->get('transaksi_penjualan');
                //$hasil_kasir = $kasir->result();
              ?>
                <!-- <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Penjualan</th>
                      <th>Kode Kasir</th>
                      <th>Tanggal & Waktu Transaksi</th>
                      <th>Nama Member</th>
                      <th>Petugas</th>
                      <th>Grand Total </th>
                      <th>Dibayar</th>
                      <th>Lihat Detail</th>
                     
                    </tr>
                  </thead> -->
                  <!-- <tbody>
                    <?php
                    $nomor=1;
                    foreach($hasil_kasir as $daftar){
                      $kode   = $daftar->kode_penjualan ;
                      $total  = $daftar->grand_total;
                      ?>
                      <tr class="<?php if($daftar->status=="open"){ echo "danger"; }elseif ($daftar->status=="close" && $daftar->validasi=="") {
                        echo "warning";
                      } ?>">
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo $daftar->kode_penjualan; ?></td>
                      <td><?php echo $daftar->kode_kasir; ?></td>
                      <td><?php echo TanggalIndo($daftar->tanggal_penjualan) . ' - ' . $daftar->jam_penjualan; ?></td>
                      <td><?php echo '<b>'.$daftar->kode_member . '</b> - '.$daftar->nama_member  ?></td>
                      <td><?php echo $daftar->petugas ?></td>
                      <td><?php echo format_rupiah($daftar->grand_total) ?></td>
                      <td><?php echo format_rupiah($daftar->bayar) ?></td>
                      <td>
                        
                        <button class="btn btn-primary btn-sm" onclick="lihat_detail('<?php echo $kode ?>' , '<?php echo $total ?>')">
                            <i class="fa fa-search"></i> Detail
                        </button>

                      </td>

                    </tr>
                    <?php $nomor++; } ?>
                  </tbody> -->
                  <tbody>
                    
                  </tbody>
                </table>




              <!------------------------------------------------------------------------------------------------------>

            </div>
          </div>

          <!-- /.row (main row) -->
        </div></section>
        </div>

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Item terjual</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_detail">
          
      </div>
      <div class="modal-footer">
        <span id="content_footer">
        </span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">
  
  $(document).ready(function(){
    // load_table_daftar()

    $("#kolom_toko").hide();

    $("input[name=tipe_transaksi]").change(function() {
        var value = this.value;

        if (value=='mutasi') {
          // $("#kolom_jalur").show();
          $("#kolom_toko").hide();
          $("#kolom_jalur").show();
        }else {
          $("#kolom_jalur").hide();
          $("#kolom_toko").show();
        }
    });

    $("#exampleModalLong").on('hidden.bs.modal', function(){
      $('#content_footer').empty();
    })

})


  function get_member(kode_jalur){

    

    var url =  '<?php echo base_url() ?>transaksional/load_toko_from_jalur/'+kode_jalur;

    $.ajax({
      url: url,
      type: 'post',
      dataType: 'html',
      success: function(respose){
        $("#member_kode").html(respose);
      }
    });

  }


  function lihat_detail(kode , total) {
    $("#exampleModalLong").modal('show');
    $('#content_footer').append('<a href="<?php echo base_url() ?>transaksional/print_nota_retur/'+kode+'" class="btn btn-success btn-flat" target="_blank">Print</a>');
    $("#content_detail").load('<?php echo base_url() ?>transaksional/riwayat_penjualan_detail/'+kode);

  }
  function load_table_daftar(){

    var jenis =  $("input[name=tipe_transaksi]:checked").val();
    var toko   =  $("#member_kode").val();
    var sales  = $("#sales_kode").val();
    var jalur  = $("#jalur_kode").val();
    // console.log(mutasi + toko + sales + jalur);

    $("#table_hasil").load('<?php echo base_url() ?>transaksional/table_load_penjualan/'+jenis+'/'+toko+'/'+jalur+'/'+sales);

    $("#table_hasil").DataTable();
  }
</script>
  