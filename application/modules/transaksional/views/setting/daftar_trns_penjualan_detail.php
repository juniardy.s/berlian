
		<table id="tbl3" class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Harga Satuan </th>
                <th>Harga Lusin</th>
                <th>Jumlah Terjual</th>
                <th>SubTotal </th>
              </tr>
            </thead>
            <tbody>
<?php 
	
	$kode 	=	$this->uri->segment(3);

	$transaksi_penjualan = $this->db->get_where('transaksi_penjualan', ['kode_penjualan' => $kode])->row();
	$hasil 	=	$this->db->get_where('opsi_transaksi_penjualan' , ['kode_penjualan' => $kode])->result();
	$no 	=	1;

	foreach ($hasil as $res) {
 ?>
				 <tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo '<b>'.$res->kode_menu .'</b> - '.$res->nama_menu ?></td>
					<td><?php echo format_rupiah($res->harga_satuan) ?></td>
					<td><?php echo format_rupiah($res->harga_satuan * 12); ?></td>
					<td><?php echo $res->jumlah.' '.$res->nama_satuan ?></td>
					<td><?php echo format_rupiah($res->subtotal) ?></td>
				</tr>
 <?php 
	}
 ?>
              
            </tbody>
            <tfoot>
              <tr>
                <th colspan="5"></th>
                <th>
                  <label> <b> Total </b></label>
                  <input type="text" readonly="" id="totalX" name="" value="<?php echo format_rupiah($transaksi_penjualan->total_nominal) ?>" class="form-control">
                </th>
              </tr>
            </tfoot>

          </table>
          <table id="retur_pt" class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Jumlah Terjual</th>
                <th>Harga Satuan </th>
                <th>SubTotal </th>
              </tr>
            </thead>
            <tbody>
<?php 
	
	$kode 	=	$this->uri->segment(3);

	$hasil 	=	$this->db->get_where('opsi_transaksi_retur_penjualan' , ['kode_penjualan' => $kode, 'jenis_retur' => 'potong_tagihan'])->result();
	$no 	=	1;
	$total = 0;

	foreach ($hasil as $res) {
 ?>   
				<tr>
					<td><?php echo ++$no; ?></td>
					<td><?php echo $res->nama_produk; ?></td>
					<td><?php echo $res->jumlah; ?></td>
					<td><?php echo $res->harga_satuan; ?></td>
					<td><?php echo $res->subtotal; ?></td>
				</tr>
 <?php
		$total += $res->subtotal;
	}
 ?>    
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4"></th>
                <th>
                  <label> <b> Total </b></label>
                  <input type="text" readonly="" id="totalX" name="" value="<?php echo format_rupiah($total) ?>" class="form-control">
                </th>
              </tr>
            </tfoot>
          </table>
          <table id="retur_tb" class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Qty</th>
                <th>Nama Barang Tukar </th>
                <th>Qty Tukar </th>
              </tr>
            </thead>
            <tbody>
<?php 
	
	$kode 	=	$this->uri->segment(3);

	$hasil 	=	$this->db->get_where('opsi_transaksi_retur_penjualan' , ['kode_penjualan' => $kode, 'jenis_retur' => 'tukar_barang'])->result();
	$no 	=	1;
	$total = 0;

	foreach ($hasil as $res) {
 ?>   
				<tr>
					<td><?php echo ++$no; ?></td>
					<td><?php echo $res->nama_produk; ?></td>
					<td><?php echo $res->jumlah; ?></td>
					<td><?php echo $res->nama_produk_konversi; ?></td>
					<td><?php echo $res->jumlah_konversi; ?></td>
				</tr>
 <?php
		$total += $res->subtotal;
	}
 ?>          
            </tbody>
          </table>
		  <table id="retur_tb" class="table table-hover table-bordered">
            <tbody>
				<tr>
					<td><b>Diskon</b></td>
					<td><?php echo format_rupiah($transaksi_penjualan->diskon_rupiah) ?></td>
				</tr>
				<tr>
					<td><b>Grand Total</b></td>
					<td><?php echo format_rupiah($transaksi_penjualan->grand_total) ?></td>
				</tr>
            </tbody>
          </table>