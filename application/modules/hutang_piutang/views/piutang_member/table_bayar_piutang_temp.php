
<?php 
$get_temp=$this->db->get_where('opsi_bayar_piutang_member_temp',array('kode_transaksi'=>$this->uri->segment(4)));
$hasil=$get_temp->result();
$no=1;
$total=0;

foreach ($hasil as $list) {
  $subtotal=$list->jumlah * $list->harga_satuan;
  $total+=$subtotal;

  $cek_angsuran=$this->db->get_where('opsi_bayar_piutang_member',array('kode_transaksi'=>$this->uri->segment(4),'kode_bahan'=>$list->kode_bahan));
  $hasil_angsuran=$cek_angsuran->row();
  if(!empty($hasil_angsuran)){
    $qty_beli=$list->qty_beli - @$hasil_angsuran->jumlah;
  }else{
    $qty_beli=$list->qty_beli;
  }

  ?>
  

  <tr>
    <td><?php echo $no++ ?></td>
    <td><?php echo $list->nama_bahan ?></td>
    <td>
      <input type="number" name="jumlah" id="jumlah_<?php echo $list->id;?>" value="<?php echo $list->jumlah;?>" onkeyup="get_subtotal('<?php echo $list->id;?>')" />
      <input type="hidden" name="qty_beli" id="qty_beli_<?php echo $list->id;?>" value="<?php echo $qty_beli;?>" />
    </td>
    <td><?php echo $list->harga_satuan ?></td>
    <td align="right"><?php echo format_rupiah($subtotal) ?></td>

    <td><?php echo get_del_id(@$list->id)?></td>
  </tr>


  <?php } ?>

  <tr align="right">
    <td align="rigth" colspan="4" >Total</td>
    <td colspan="1" align="right"><?php  echo format_rupiah($total);?></td>
    <td></td>
  </tr>

  <script type="text/javascript">

    function get_subtotal(obj){
     // var jumlah = parseInt($(obj).val());
     var id = obj;
     var jumlah = $("#jumlah_"+obj).val();
     var qty_beli = $("#qty_beli_"+obj).val();
     // alert(qty_beli);
     if(jumlah > qty_beli || parseInt(jumlah) <= 0){

       alert("Jumlah Yang anda masukan salah atau mungkin melebihi batas yang di tentukan");
       $("#jumlah_"+obj).val('');
     }else{
       var url = "<?php echo base_url() . 'hutang_piutang/piutang_member/update_subtotal' ?>";
       $.ajax({
        type: 'POST',
        url: url,
        data: {id:id,jumlah:jumlah},
        success: function(msg){

          var kode_transaksi = $('#kode_transaksi').val();
          $("#tabel_temp_data_transaksi").load("<?php echo base_url().'hutang_piutang/piutang_member/get_bayar_piutang_member/'; ?>"+kode_transaksi);
        }
      });
     }
   }
 </script>