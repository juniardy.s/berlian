      
<?php
$param = $this->input->post();
$kode_customer = $param['kode_customer'];
if(@$param['tgl_awal'] && @$param['tgl_akhir']){
  $tgl_awal = $param['tgl_awal'];
  $tgl_akhir = $param['tgl_akhir'];
  
  $this->db->where('tanggal_transaksi >=', $tgl_awal);
  $this->db->where('tanggal_transaksi <=', $tgl_akhir);
  $this->db->where('status', 'member');
  $this->db->where('kategori_member', 'konsinyasi');
  
}


$this->db->select('*');
$this->db->where('kode_customer',$kode_customer);
$this->db->from('transaksi_piutang');
$this->db->order_by('sisa','desc');
$this->db->order_by('tanggal_transaksi','desc');
$transaksi = $this->db->get();
$hasil_transaksi = $transaksi->result();
$total=0;
?>

<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-12">
   
    <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
     <thead>
      <tr>
        <th>No</th>
        <th>Kode Transaksi</th>
        <th>Nama Member</th>
        <th>Nominal Piutang</th>
        <th>Sisa</th>
        <th>Tanggal Transaksi</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $nomor = 1;
      foreach($hasil_transaksi as $daftar){ 
        $tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : TanggalIndo(@$daftar->tanggal_transaksi) ;
        ?> 
        <tr class=" <?php if($daftar->sisa!=0){ echo "danger"; } ?>">
          <td><?php echo $nomor; ?></td>
          <td><?php echo @$daftar->kode_transaksi; ?></td>
          <td><?php echo @$daftar->nama_customer; ?></td>
          <td><?php echo format_rupiah(@$daftar->nominal_piutang); ?></td>
          <td><?php echo format_rupiah(@$daftar->sisa); ?></td>
          <td><?php echo $tgl; ?></td>
          <td><?php echo cek_status_piutang($daftar->sisa); ?></td>
          <td><?php if($daftar->sisa!=0){ ?>
            <a class="btn green" href="<?php echo base_url().'hutang_piutang/piutang_member/detail_lunas/'.$daftar->kode_transaksi;  ?>"><i class="fa fa-search"></i> Detail</a>
            <a class="btn btn-primary" href="<?php echo base_url().'hutang_piutang/piutang_member/proses/'.$daftar->kode_transaksi ?>"><i class="fa fa-money"></i> Proses</a>

            <?php }else if($daftar->sisa=='0'){ ?>
            <!-- <a class="btn btn-primary" href="<?php echo base_url().'hutang_piutang/piutang_member/detail_lunas/'.$daftar->kode_transaksi;  ?>"><i class="fa fa-pencil"></i> Detail</a> -->
            <?php } ?></td>
          </tr>
          <?php $nomor++; } ?>
          
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Kode Transaksi</th>
            <th>Nama Member</th>
            <th>Nominal Piutang</th>
            <th>Sisa</th>
            <th>Tanggal Transaksi</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
      <div class="row">
       <!--<div class="col-md-10" align="right">
              <label style="font-size: 20px"><b>Total :</label>
        </div>
        <div class="col-md-2 pull-right">
              <span><button style="width: 147px" type="button" class="btn btn-warning pull-right" id="cari"><i class=""></i><?php  echo format_rupiah($total); ?></button></span>
            </div>-->
          </div>
        </div>
      </div>

      <script type="text/javascript">
       $("#tabel_daftar").dataTable({
        "paging":   false,
        "ordering": false,
        "info":     false
      });
     </script>