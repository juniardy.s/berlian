<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pembelian extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

	public function daftar_pembelian()
	{
		$data['aktif']='pembelian';
		$data['konten'] = $this->load->view('pembelian/pembelian/daftar_pembelian', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function tambah()
	{
		$data['aktif']='pembelian';
		$data['konten'] = $this->load->view('pembelian/pembelian/tambah_pembelian', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}
	public function edit()
	{
		$data['aktif']='pembelian';
		$data['konten'] = $this->load->view('pembelian/pembelian/edit_pembelian', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}
	public function tambah_supplier()
	{
		$data['aktif']='pembelian';
		$data['konten'] = $this->load->view('pembelian/pembelian/tambah_supplier', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}
	public function pembayaran()
	{
		$data['aktif']='pembelian';
		$data['konten'] = $this->load->view('pembelian/pembelian/tambah_pembayaran', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}
	public function coba()
	{
		$data['aktif']='coba';
		$data['konten'] = $this->load->view('pembelian/pembelian/coba', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);
	}

	public function get_form_coba(){
		@$id = $this->input->post('id');
		@$data['id'] = @$id ;
		$this->load->view('pembelian/pembelian/form_penyesuaian',$data);
	}

	public function cari_pembelian()
	{
		
		$this->load->view('pembelian/pembelian/cari_pembelian');		
	}

	public function detail($kode)
	{
		$data['aktif']='pembelian';
		$data['kode'] = $kode;
		$data['konten'] = $this->load->view('pembelian/pembelian/detail_pembelian', NULL, TRUE);
		$data['halaman'] = $this->load->view('menu', $data, TRUE);
		$this->load->view('main', $data);		
	}

	public function tabel_temp_data_transaksi($kode)
	{
		$data['diskon'] = $this->diskon_tabel();
		$data['kode'] = $kode ;
		$this->load->view ('pembelian/pembelian/tabel_transaksi_temp',$data);		
	}

	public function get_pembelian($kode){
		$data['kode'] = $kode ;
		$this->load->view('pembelian/pembelian/tabel_transaksi_temp',$data);
	}
	public function get_pembelian_edit($kode){
		$data['kode'] = $kode ;
		$this->load->view('pembelian/pembelian/tabel_transaksi_edit_temp',$data);
	}
	public function get_penyesuaian($kode){
		$data['kode'] = $kode ;
		$this->load->view('pembelian/pembelian/tabel_transaksi_penyesuaian',$data);
	}
	public function table_pembelian($kode){
		$data['kode'] = $kode ;
		$this->load->view('pembelian/pembelian/table_transaksi',$data);
	}
	//------------------------------------------ Proses ----------------- --------------------//
	public function get_grandtotal()
	{
		$kode_pembelian = $this->input->post('kode_pembelian');
		$kode_supplier = $this->input->post('kode_supplier');
		$diskon = $this->input->post('diskon');
		$jenis_diskon = $this->input->post('jenis_diskon');

		$this->db->select_sum('subtotal') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian,'kode_supplier'=>$kode_supplier));
		$data = $query->row();

		if($jenis_diskon=='Persen'){
			$nilai_diskon=($data->subtotal * $diskon) /100;
			$grand_total=($data->subtotal - $nilai_diskon);
		}elseif ($jenis_diskon=='Rupiah') {
			$nilai_diskon=$diskon;
			$grand_total=($data->subtotal - $diskon);
		}
		$hasil = array('grand_total' =>$grand_total,'nilai_diskon'=>@format_rupiah($nilai_diskon),'nilai_grand_total'=>@format_rupiah($grand_total),
			'nilai_pembelian'=>@format_rupiah($data->subtotal),'total_pembelian'=>@$data->subtotal);
		echo json_encode($hasil);
	}
	public function get_kode_nota()
	{
		$nomor_nota = $this->input->post('nomor_nota');
		$query = $this->db->get_where('transaksi_pembelian',array('nomor_nota' => $nomor_nota, 'tanggal_pembelian'=> date('Y-m-d') ))->num_rows();

		if($query > 0){
			echo "1";
		}
		else{

			echo "0";
		}
	}
	public function get_kode_po()
	{
		$kode_po = $this->input->post('kode_po');
		$kode_pembelian = $this->input->post('kode_pembelian');
		$query = $this->db->get_where('transaksi_po',array('kode_transaksi' => $kode_po,'status' =>'menunggu'));
		$data=$query->row();
		$jumlah = count($data);
		if($jumlah > 0){
			$cek_temp = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
			$hasil_cek = $cek_temp->result();
			if(count($hasil_cek)<1){
				$pembelian = $this->db->get_where('opsi_transaksi_po',array('kode_po'=>$data->kode_po));
				$list_pembelian = $pembelian->result();
				foreach($list_pembelian as $daftar){ 
					$masukan['kode_pembelian'] = $kode_pembelian;
					$masukan['kategori_bahan'] = $daftar->kategori_bahan;
					$masukan['kode_bahan'] = $daftar->kode_bahan;
					$masukan['nama_bahan'] = $daftar->nama_bahan; 
					$masukan['jumlah'] = $daftar->jumlah; 
					$masukan['kode_satuan'] = $daftar->kode_satuan;
					$masukan['nama_satuan'] = $daftar->nama_satuan;
					$bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$daftar->kode_bahan,'nama_bahan_baku'=>$daftar->nama_bahan));
					$hasil_bahan = $bahan->row();
					$masukan['harga_satuan'] = $hasil_bahan->hpp;
					$trans_po = $this->db->get_where('transaksi_po',array('kode_po'=>$data->kode_po));
					$list_trans_po = $trans_po->row();

					$masukan['kode_supplier'] = @$list_trans_po->kode_supplier; 
					$masukan['nama_supplier'] = @$list_trans_po->nama_supplier; 
					$masukan['subtotal'] = $hasil_bahan->hpp * $daftar->jumlah;

					$input = $this->db->insert('opsi_transaksi_pembelian_temp',$masukan);
				}
			}else{
				$this->db->delete('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
				$pembelian = $this->db->get_where('opsi_transaksi_po',array('kode_po'=>$data->kode_po));
				$list_pembelian = $pembelian->result();
				foreach($list_pembelian as $daftar){ 
					$masukan['kode_pembelian'] = $kode_pembelian;
					$masukan['kategori_bahan'] = $daftar->kategori_bahan;
					$masukan['kode_bahan'] = $daftar->kode_bahan;
					$masukan['nama_bahan'] = $daftar->nama_bahan; 
					$masukan['jumlah'] = $daftar->jumlah; 
					$masukan['kode_satuan'] = $daftar->kode_satuan;
					$masukan['nama_satuan'] = $daftar->nama_satuan;
					$bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$daftar->kode_bahan,'nama_bahan_baku'=>$daftar->nama_bahan));
					$hasil_bahan = $bahan->row();
					$masukan['harga_satuan'] = $hasil_bahan->hpp;
					$trans_po = $this->db->get_where('transaksi_po',array('kode_po'=>$data->kode_po));
					$list_trans_po = $trans_po->row();

					$masukan['kode_supplier'] = @$list_trans_po->kode_supplier; 
					$masukan['nama_supplier'] = @$list_trans_po->nama_supplier; 
					$masukan['subtotal'] = $hasil_bahan->hpp * $daftar->jumlah;

					$input = $this->db->insert('opsi_transaksi_pembelian_temp',$masukan);
				}
			}
			echo "1|".$data->kode_po.'|'.$data->kode_supplier;
		}
		else{
			
			echo "0";
		}
	}

	public function temp_data_transaksi()
	{
		$kode_pembelian = $this->input->post('kode_pembelian');

		$this->db->select('*, SUM(subtotal)as grand_total') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		echo $data->grand_total ;
        #echo $this->db->last_query();
	}
	public function get_grand_total()
	{
		$grand_total = $this->input->post('grand_total');
		$diskon_rupiah = $this->input->post('diskon_rupiah');
		
		echo @format_rupiah($grand_total - $diskon_rupiah) ;
        #echo $this->db->last_query();
	}

	public function diskon_tabel()
	{
		$input = $this->input->post('diskon');
		echo $input ;
	}

	public function item_bahan_baku()
	{
		$kode_pembelian = $this->input->post('kode_pembelian');
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		echo $input ;
	}

	public function simpan_item_temp()
	{
		$kode_pembelian = $this->input->post('kode_pembelian');
		$kode_bahan = $this->input->post('kode_bahan');
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian,'kode_bahan'=>$kode_bahan));
		$data = $query->row();
		if(empty($data)){
			$masukan = $this->input->post();

			$nama_suplier = $this->db->get_where('master_supplier',array('kode_supplier'=>$masukan['kode_supplier']));
			$hasil_nama_supplier = $nama_suplier->row();
			$masukan['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
			$masukan['position'] ='gudang';

			$cari = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku' => $kode_bahan));
			$hasil_cari = $cari->row();
			$masukan['nama_bahan'] = $hasil_cari->nama_bahan_baku;


			$this->db->insert('opsi_transaksi_pembelian_temp',$masukan);
			echo "0";
		}else{
			echo "1";
		}

	}

	public function update_item_temp(){
		$update = $this->input->post();
		$masukan = $this->input->post();

		$this->db->update('opsi_transaksi_pembelian_temp',$update,array('id'=>$update['id']));
		echo "sukses";
	}
	public function get_kembali()
	{	
		$grand_total = $this->input->post('grand_total');
		$uang_muka = $this->input->post('uang_muka');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		if($jenis_pembayaran=='cash'){
			$kembalian=@$uang_muka - @$grand_total;
			if($kembalian < 0){
				$kembalian=0;
			}else{
				$kembalian=$uang_muka - $grand_total;
			}
			$hasil=array('nilai_kembali' => @format_rupiah($kembalian),'kembali'=> $kembalian,'nilai_uang_muka'=>@format_rupiah($uang_muka));
		}else{
			$hasil=array('nilai_kembali' => @format_rupiah($grand_total - $uang_muka),'nilai_uang_muka'=>@format_rupiah($uang_muka));
		}

		echo json_encode($hasil);
	}
	public function simpan_transaksi()
	{
		$input = $this->input->post();
		$kode_pembelian = $input['kode_pembelian'];
		$kode_po = @$input['kode_po'];
		$proses_bayar = @$input['proses_pembayaran'];

		$get_id_petugas = $this->session->userdata('astrosession');
		$id_petugas = $get_id_petugas->id;
		$nama_petugas = $get_id_petugas->uname;


		unset($input['kode_po2']);
		unset($input['keterangan']);
		unset($input['tgl_barang_datang']);
		$kode_supplier = $input['kode_supplier'];

		$this->db->select('*') ;
		$query_pembelian_temp = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$get_supplier = $this->db->get_where('master_supplier',array('kode_supplier'=>$kode_supplier));
		$hasil_nama_supplier = $get_supplier->row();

		$total = 0;
		foreach ($query_pembelian_temp->result() as $item){
			$data_opsi['kode_pembelian'] = $item->kode_pembelian;
			$data_opsi['kode_po'] = $kode_po;
			$data_opsi['kategori_bahan'] = $item->kategori_bahan;

			$data_opsi['kode_bahan'] = $item->kode_bahan;


			$get_hpp_lama=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$item->kode_bahan));
			$hasil_get_hpp_lama=$get_hpp_lama->row();

			$hpp_lama 			=$hasil_get_hpp_lama->hpp;

			$hpp_baru 			=round($item->harga_satuan/$hasil_get_hpp_lama->jumlah_dalam_satuan_pembelian,2);
			//$hasil_hpp_baru 	=($hpp_lama+$hpp_baru)/2;
			//if ($hpp_lama=='' or $hpp_lama=='0'){
				$update_hpp['hpp'] 	=$hpp_baru;
			//}else{
			//	$update_hpp['hpp'] 	=$hasil_hpp_baru;
			//}
			
			
			$hasil_update_hpp 	=$this->db->update('master_bahan_baku',$update_hpp,array('kode_bahan_baku'=>$item->kode_bahan));
			
			$data_opsi['nama_bahan'] = $item->nama_bahan;
			$data_opsi['jumlah'] = $item->jumlah;
			$data_opsi['kode_satuan'] = $item->kode_satuan;
			$data_opsi['nama_satuan'] = $item->nama_satuan;
			$data_opsi['harga_satuan'] = $item->harga_satuan;
			$data_opsi['jenis_diskon'] = $item->jenis_diskon;
			$data_opsi['diskon_item'] = $item->diskon_item;
			$data_opsi['diskon_rupiah'] = $item->diskon_rupiah;
			$data_opsi['kode_supplier'] = $input['kode_supplier'];
			$data_opsi['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
			$data_opsi['subtotal'] = $item->subtotal;
			$data_opsi['position'] = 'gudang';

			$tabel_opsi_transaksi_pembelian = $this->db->insert("opsi_transaksi_pembelian", $data_opsi);


			$grand_total = $total + $item->subtotal;
			$harga_satuan = $item->harga_satuan;
			$nama_bahan = $item->nama_bahan;
			$stok_masuk = $item->jumlah;
			$kode_pembelian = $item->kode_pembelian;
			$kategori_bahan = $item->kategori_bahan;
			$kode_bahan = $item->kode_bahan;
			$nama_supplier = $item->nama_supplier;
			$kode_supplier = $item->kode_supplier;

			$kode_default = $this->db->get('setting_gudang');
			$hasil_kode_default = $kode_default->row();

			$this->db->select('*') ;
			$real_stock = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
			$stok_real = $real_stock->row()->real_stock ;
			$konversi_stok = $real_stock->row()->jumlah_dalam_satuan_pembelian ;

			$hpp=$real_stock->row()->hpp ;

			if(@$hpp != @$data_opsi['harga_satuan']){
				$tidak_sesuai='1';
			}

			if(empty($stok_real)){

				$data_stok['real_stock'] = $stok_masuk * $konversi_stok;
				$this->db->update('master_bahan_baku',$data_stok,array('kode_bahan_baku'=>$kode_bahan));
				
				$this->db->select('kode_unit');
				$kode_default = $this->db->get('setting_gudang');
				$hasil_kode_default = $kode_default->row();
				$kode_unit = @$hasil_kode_default->kode_unit;
				$kode_rak = @$hasil_kode_default->kode_rak;

				$this->db->select('nama_unit');
				$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
				$hasil_master_unit = $master_unit->row();
				$nama_unit = @$hasil_master_unit->nama_unit;

				$this->db->select('nama_rak');
				$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
				$hasil_master_rak = $master_rak->row();
				$nama_rak = @$hasil_master_rak->nama_rak;

				$harga_satuan_stok = round($harga_satuan / $konversi_stok,2);

				$stok['jenis_transaksi'] = 'pembelian' ;
				$stok['kode_transaksi'] = $kode_pembelian ;
				$stok['kategori_bahan'] = $kategori_bahan ;
				$stok['kode_bahan'] = $kode_bahan ;
				$stok['nama_bahan'] = $nama_bahan ;
				$stok['stok_keluar'] = '';
				$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
				$stok['posisi_awal'] = 'supplier';
				$stok['posisi_akhir'] = 'gudang';
				$stok['hpp'] = $harga_satuan_stok ;
				$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
				$stok['kode_unit_tujuan'] = $kode_unit;
				$stok['nama_unit_tujuan'] = $nama_unit;
				$stok['kode_rak_tujuan'] = $kode_rak;
				$stok['nama_rak_tujuan'] = $nama_rak;
				$stok['id_petugas'] = $id_petugas;
				$stok['nama_petugas'] = $nama_petugas;
				$stok['tanggal_transaksi'] = date("Y-m-d") ;

				$transaksi_stok = $this->db->insert("transaksi_stok", $stok);



			}
			else{
				$this->db->select('*') ;
				$hpp = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
				$hpp = $hpp->row()->hpp ;
				$jumlah_stok = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
				$jumlah_stok = $jumlah_stok->row()->real_stock ;



				$data_stok['real_stock'] = ($stok_masuk * $konversi_stok)  + $jumlah_stok;
				$this->db->update('master_bahan_baku',$data_stok,array('kode_bahan_baku'=>$kode_bahan));
					//echo $this->db->last_query();
				$this->db->select('kode_unit');
				$kode_default = $this->db->get('setting_gudang');
				$hasil_kode_default = $kode_default->row();
				$kode_unit = @$hasil_kode_default->kode_unit;
				$kode_rak = @$hasil_kode_default->kode_rak;

				$this->db->select('nama_unit');
				$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
				$hasil_master_unit = $master_unit->row();
				$nama_unit = @$hasil_master_unit->nama_unit;

				$this->db->select('nama_rak');
				$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
				$hasil_master_rak = $master_rak->row();
				$nama_rak = @$hasil_master_rak->nama_rak;

				$harga_satuan_stok = $harga_satuan / $konversi_stok;

				$stok['jenis_transaksi'] = 'pembelian' ;
				$stok['kode_transaksi'] = $kode_pembelian ;
				$stok['kategori_bahan'] = $kategori_bahan ;
				$stok['kode_bahan'] = $kode_bahan ;
				$stok['nama_bahan'] = $nama_bahan ;
				$stok['stok_keluar'] = '';
				$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
				$stok['posisi_awal'] = 'supplier';
				$stok['posisi_akhir'] = 'gudang';
				$stok['hpp'] = $harga_satuan_stok ;
				$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
				$stok['kode_unit_tujuan'] = $kode_unit;
				$stok['nama_unit_tujuan'] = $nama_unit;
				$stok['kode_rak_tujuan'] = $kode_rak;
				$stok['nama_rak_tujuan'] = $nama_rak;
				$stok['id_petugas'] = $id_petugas;
				$stok['nama_petugas'] = $nama_petugas;
				$stok['tanggal_transaksi'] = date("Y-m-d") ;

				$transaksi_stok = $this->db->insert("transaksi_stok", $stok);


			}
			


			
		}

			//if($transaksi_stok){
		
		$this->db->select('*') ;
		@$query_akun = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>$input['kode_sub']))->row();
		@$kode_sub = $query_akun->kode_sub_kategori_akun;
		@$nama_sub = $query_akun->nama_sub_kategori_akun;
		@$kode_kategori = $query_akun->kode_kategori_akun;
		@$nama_kategori = $query_akun->nama_kategori_akun;
		@$kode_jenis = $query_akun->kode_jenis_akun;
		@$nama_jenis = $query_akun->nama_jenis_akun;

		$this->db->select('*, SUM(subtotal)as grand_total') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		$grand_total = $data->grand_total ;

		

		$this->db->select_max('urut');
		$result = $this->db->get_where('transaksi_pembelian');
		$hasil = @$result->result();
		if($result->num_rows()) $no = ($hasil[0]->urut)+1;
		else $no = 1;

		if($no<10)$no = '000'.$no;
		else if($no<100)$no = '00'.$no;
		else if($no<1000)$no = '0'.$no;
		else if($no<10000)$no = ''.$no;
                  //else if($no<100000)$no = $no;
		$code = $no;

		$this->db->select('kode_pembelian');
		$get_kode_pembelian = $this->db->get('master_setting');
		$hasil_kode_pembelian = $get_kode_pembelian->row();


		unset($input['jenis_diskon']);
		$input['tanggal_pembelian'] = date("Y-m-d") ;
		$input['total_nominal'] = $grand_total ;
		@$input['grand_total'] = @$grand_total - $input['diskon_rupiah'];
		$input['petugas'] = $nama_petugas ;
		$input['id_petugas'] = $id_petugas;
		$input['keterangan'] = '' ;
		$input['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
		$input['status'] = 'belum divalidasi';
		$input['position'] = 'gudang' ;
		$input['kode_transaksi'] = @$hasil_kode_pembelian->kode_pembelian."_".date("dmYhis")."_".$code;
		$input['urut'] = $no;
		if ($input['proses_pembayaran']=='credit' || $input['proses_pembayaran']=='kredit') {
			$input['jatuh_tempo'] = $input['tanggal_jatuh_tempo'];
			$input['proses_pembayaran']='credit';
		}else{
			$input['jatuh_tempo'] = '-';
		}
		$tanggal_jatuh_tempo = $input['tanggal_jatuh_tempo'];

		$input['dibayar']=@$input['uang_muka'];
		unset($input['tanggal_jatuh_tempo']);
		unset($input['kategori_bahan']);
		unset($input['kode_bahan']);
		unset($input['nama_bahan']);
		unset($input['jumlah']);
		unset($input['kode_satuan']);
		unset($input['nama_satuan']);
		unset($input['harga_satuan']);
		unset($input['id_item']);
		unset($input['kode_sub']);
		unset($input['uang_muka']);
		unset($input['total_pembelian']);
		unset($input['satuan']);
		unset($input['diskon']);
		unset($input['jenis_diskon']);
		unset($input['kode_perlengkapan']);
		unset($input['harga']);
		unset($input['id_item']);
		unset($input['expired_date']);
		unset($input['sub_total']);
		unset($input['jenis_diskon_item']);
		unset($input['diskon_item']);
		$tabel_transaksi_pembelian = $this->db->insert("transaksi_pembelian", $input);

		if($tabel_transaksi_pembelian){

			if(@$input['proses_pembayaran'] == 'cash'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $grand_total - @$input['diskon_rupiah'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
			}
			else if(@$input['proses_pembayaran'] == 'debet'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $grand_total - $input['diskon_rupiah'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
			}
			else if(@$input['proses_pembayaran'] == 'credit'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = @$input['dibayar'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);

				$data_hutang['kode_transaksi'] = $kode_pembelian ;

				$get_nama_suplier =$this->db->get('transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
				$hasil_supplier=$get_nama_suplier->row();

				$data_hutang['kode_supplier'] =  $hasil_supplier->kode_supplier;
				$data_hutang['nama_supplier'] =  $hasil_supplier->nama_supplier ;
				$data_hutang['tanggal_jatuh_tempo'] =  $tanggal_jatuh_tempo;
				$data_hutang['nominal_hutang'] = ($grand_total - @$input['diskon_rupiah']) - @$input['dibayar'];
				$data_hutang['angsuran'] = '' ;
				$data_hutang['sisa'] = ($grand_total - @$input['diskon_rupiah']) - @$input['dibayar'] ;
				$data_hutang['tanggal_transaksi'] = date("Y-m-d") ;
				$data_hutang['petugas'] = $nama_petugas ;
				$data_hutang['id_petugas'] = $id_petugas;

				$hutang = $this->db->insert("transaksi_hutang", $data_hutang);
				
			}
			$status_po['status']='selesai';
			$this->db->update('transaksi_po',$status_po,array('kode_transaksi'=>$kode_po));
			
			$this->db->delete( 'opsi_transaksi_pembelian_temp', array('kode_pembelian' => $kode_pembelian) );


			echo '1|<div class="alert alert-success">Berhasil Melakukan Pembelian.</div>';
			echo "|".@$tidak_sesuai;
		}
		else{
			echo '1|<div class="alert alert-danger">Gagal Melakukan Pembelian (Trx_pmbelian) .</div>';  
		}

		
	}

	public function simpan_ubah_transaksi()
	{
		$input = $this->input->post();
		$kode_pembelian = $input['kode_pembelian'];
		$kode_po = @$input['kode_po'];
		$proses_bayar = @$input['proses_pembayaran'];

		$get_id_petugas = $this->session->userdata('astrosession');
		$id_petugas = $get_id_petugas->id;
		$nama_petugas = $get_id_petugas->uname;


		$query_hapus = $this->db->get_where('transaksi_stok',array('kode_transaksi'=>$kode_pembelian));
		$data_hapus = $query_hapus->result();
		foreach ($data_hapus as $value) {
			
			$stok_hapus = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$value->kode_bahan,'status'=>'sendiri'));
			$stok_real_hapus = $stok_hapus->row()->real_stock ;

			$data_bahan['real_stock']=$stok_real_hapus - $value->stok_masuk;
			$this->db->update('master_bahan_baku',$data_bahan,array('kode_bahan_baku'=>$value->kode_bahan,'status'=>'sendiri'));

		}
		$this->db->delete('transaksi_stok',array('kode_transaksi'=>$kode_pembelian));
		$this->db->delete('opsi_transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
		$this->db->delete('transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
		$this->db->delete('transaksi_hutang',array('kode_transaksi'=>$kode_pembelian));
		$this->db->delete('keuangan_keluar',array('kode_referensi'=>$kode_pembelian));
		// $this->db->select('*, SUM(subtotal)as grand_total') ;
		// $query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		// $data = $query->row();
		// $grand_total = $data->grand_total ;
		// @$tot_pembelian = @$grand_total - $input['diskon_rupiah'];

		// $input_jdwl['kode_transaksi'] = $input['kode_pembelian'];
		// $input_jdwl['kode_po'] = $input['kode_po2'];
		// $input_jdwl['keterangan'] = $input['keterangan'];
		// $input_jdwl['tanggal_barang_datang'] = $input['tgl_barang_datang'];
		// $this->db->insert("input_jadwal_barang_datang", $input_jdwl);

		// ---------------------singkron----------------------------------------------------------------------------------------
		// $singkron_query = $this->db->last_query();
		// $singkron['jenis_singkron'] = 'tambah';
		// $singkron['query'] = $singkron_query;
		// $singkron['status'] = 'pending';
		// $this->db->insert("singkronasi", $singkron);

		unset($input['kode_po2']);
		unset($input['keterangan']);
		unset($input['tgl_barang_datang']);
		$kode_supplier = $input['kode_supplier'];

		// if($input['dibayar'] < $tot_pembelian && $proses_bayar != 'credit'){
		// 	echo '0|<div class="alert alert-danger">Periksa nilai pembayaran.</div>';  
		// }
		// else{
		$this->db->select('*') ;
		$query_pembelian_temp = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$get_supplier = $this->db->get_where('master_supplier',array('kode_supplier'=>$kode_supplier));
		$hasil_nama_supplier = $get_supplier->row();

		$total = 0;
		foreach ($query_pembelian_temp->result() as $item){
			$data_opsi['kode_pembelian'] = $item->kode_pembelian;
			$data_opsi['kode_po'] = $kode_po;
			$data_opsi['kategori_bahan'] = $item->kategori_bahan;

			$data_opsi['kode_bahan'] = $item->kode_bahan;
			//P. Dion // Update HPP (Average)

			$get_hpp_lama=$this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$item->kode_bahan));
			$hasil_get_hpp_lama=$get_hpp_lama->row();

			$hpp_lama 			=$hasil_get_hpp_lama->hpp;

			$hpp_baru 			=round($item->harga_satuan/$hasil_get_hpp_lama->jumlah_dalam_satuan_pembelian,2);
			$hasil_hpp_baru 	=($hpp_lama+$hpp_baru)/2;
			if ($hpp_lama=='' or $hpp_lama=='0'){
				$update_hpp['hpp'] 	=$hpp_baru;
			}else{
				$update_hpp['hpp'] 	=$hasil_hpp_baru;
			}
			
			
			$hasil_update_hpp 	=$this->db->update('master_bahan_baku',$update_hpp,array('kode_bahan_baku'=>$item->kode_bahan));
			#echo $this->db->last_query();
			$data_opsi['nama_bahan'] = $item->nama_bahan;
			$data_opsi['jumlah'] = $item->jumlah;
			$data_opsi['kode_satuan'] = $item->kode_satuan;
			$data_opsi['nama_satuan'] = $item->nama_satuan;
			$data_opsi['harga_satuan'] = $item->harga_satuan;
			$data_opsi['jenis_diskon'] = $item->jenis_diskon;
			$data_opsi['diskon_item'] = $item->diskon_item;
			$data_opsi['diskon_rupiah'] = $item->diskon_rupiah;
			$data_opsi['kode_supplier'] = $input['kode_supplier'];
			$data_opsi['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
			$data_opsi['subtotal'] = $item->subtotal;
			$data_opsi['position'] = 'gudang';

			$tabel_opsi_transaksi_pembelian = $this->db->insert("opsi_transaksi_pembelian", $data_opsi);

			// ---------------------singkron----------------------------------------------------------------------------------------
			// $singkron_query = $this->db->last_query();
			// $singkron['jenis_singkron'] = 'tambah';
			// $singkron['query'] = $singkron_query;
			// $singkron['status'] = 'pending';
			// $this->db->insert("singkronasi", $singkron);

			$grand_total = $total + $item->subtotal;
			$harga_satuan = $item->harga_satuan;
			$nama_bahan = $item->nama_bahan;
			$stok_masuk = $item->jumlah;
			$kode_pembelian = $item->kode_pembelian;
			$kategori_bahan = $item->kategori_bahan;
			$kode_bahan = $item->kode_bahan;
			$nama_supplier = $item->nama_supplier;
			$kode_supplier = $item->kode_supplier;
			//echo $kategori_bahan;
			
			$kode_default = $this->db->get('setting_gudang');
			$hasil_kode_default = $kode_default->row();

			$this->db->select('*') ;
			$real_stock = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
			$stok_real = $real_stock->row()->real_stock ;
			$konversi_stok = $real_stock->row()->jumlah_dalam_satuan_pembelian ;

			$hpp=$real_stock->row()->hpp ;
			//echo"@$hpp != @$data_opsi[harga_satuan]";
			if(@$hpp != @$data_opsi['harga_satuan']){
				$tidak_sesuai='1';
			}

			if(empty($stok_real)){

				$data_stok['real_stock'] = $stok_masuk * $konversi_stok;
				$this->db->update('master_bahan_baku',$data_stok,array('kode_bahan_baku'=>$kode_bahan));
					//echo $this->db->last_query();
				$this->db->select('kode_unit');
				$kode_default = $this->db->get('setting_gudang');
				$hasil_kode_default = $kode_default->row();
				$kode_unit = @$hasil_kode_default->kode_unit;
				$kode_rak = @$hasil_kode_default->kode_rak;

				$this->db->select('nama_unit');
				$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
				$hasil_master_unit = $master_unit->row();
				$nama_unit = @$hasil_master_unit->nama_unit;

				$this->db->select('nama_rak');
				$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
				$hasil_master_rak = $master_rak->row();
				$nama_rak = @$hasil_master_rak->nama_rak;

				$harga_satuan_stok = round($harga_satuan / $konversi_stok,2);

				$stok['jenis_transaksi'] = 'pembelian' ;
				$stok['kode_transaksi'] = $kode_pembelian ;
				$stok['kategori_bahan'] = $kategori_bahan ;
				$stok['kode_bahan'] = $kode_bahan ;
				$stok['nama_bahan'] = $nama_bahan ;
				$stok['stok_keluar'] = '';
				$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
				$stok['posisi_awal'] = 'supplier';
				$stok['posisi_akhir'] = 'gudang';
				$stok['hpp'] = $harga_satuan_stok ;
				$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
				$stok['kode_unit_tujuan'] = $kode_unit;
				$stok['nama_unit_tujuan'] = $nama_unit;
				$stok['kode_rak_tujuan'] = $kode_rak;
				$stok['nama_rak_tujuan'] = $nama_rak;
				$stok['id_petugas'] = $id_petugas;
				$stok['nama_petugas'] = $nama_petugas;
				$stok['tanggal_transaksi'] = date("Y-m-d") ;

				$transaksi_stok = $this->db->insert("transaksi_stok", $stok);


					// $get_hpp = $this->db->query("SELECT MIN(hpp) as hpp FROM transaksi_stok WHERE jenis_transaksi='pembelian' and kode_bahan='$kode_bahan'");
					// $hpp = $get_hpp->row();
					// // echo "get_hpp".$get_hpp;
					// // echo "hpp".$hpp->hpp;
					// $hpp_baru['hpp']= $hpp->hpp;
					// $this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));	

			}
			else{
				$this->db->select('*') ;
				$hpp = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
				$hpp = $hpp->row()->hpp ;
				$jumlah_stok = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
				$jumlah_stok = $jumlah_stok->row()->real_stock ;



				$data_stok['real_stock'] = ($stok_masuk * $konversi_stok)  + $jumlah_stok;
				$this->db->update('master_bahan_baku',$data_stok,array('kode_bahan_baku'=>$kode_bahan));
					//echo $this->db->last_query();
				$this->db->select('kode_unit');
				$kode_default = $this->db->get('setting_gudang');
				$hasil_kode_default = $kode_default->row();
				$kode_unit = @$hasil_kode_default->kode_unit;
				$kode_rak = @$hasil_kode_default->kode_rak;

				$this->db->select('nama_unit');
				$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
				$hasil_master_unit = $master_unit->row();
				$nama_unit = @$hasil_master_unit->nama_unit;

				$this->db->select('nama_rak');
				$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
				$hasil_master_rak = $master_rak->row();
				$nama_rak = @$hasil_master_rak->nama_rak;

				$harga_satuan_stok = $harga_satuan / $konversi_stok;

				$stok['jenis_transaksi'] = 'pembelian' ;
				$stok['kode_transaksi'] = $kode_pembelian ;
				$stok['kategori_bahan'] = $kategori_bahan ;
				$stok['kode_bahan'] = $kode_bahan ;
				$stok['nama_bahan'] = $nama_bahan ;
				$stok['stok_keluar'] = '';
				$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
				$stok['posisi_awal'] = 'supplier';
				$stok['posisi_akhir'] = 'gudang';
				$stok['hpp'] = $harga_satuan_stok ;
				$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
				$stok['kode_unit_tujuan'] = $kode_unit;
				$stok['nama_unit_tujuan'] = $nama_unit;
				$stok['kode_rak_tujuan'] = $kode_rak;
				$stok['nama_rak_tujuan'] = $nama_rak;
				$stok['id_petugas'] = $id_petugas;
				$stok['nama_petugas'] = $nama_petugas;
				$stok['tanggal_transaksi'] = date("Y-m-d") ;

				$transaksi_stok = $this->db->insert("transaksi_stok", $stok);

					// $get_hpp = $this->db->query("SELECT MIN(hpp) as hpp FROM transaksi_stok WHERE jenis_transaksi='pembelian' and kode_bahan='$kode_bahan'");
					// $hpp = $get_hpp->row();
					// // echo "get_hpp".$get_hpp;
					// // echo "hpp".$hpp->hpp;
					// $hpp_baru['hpp']= $hpp->hpp;
					// $this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));				
			}
			


			
		}

			//if($transaksi_stok){
		unset($input['kategori_bahan']);
		unset($input['kode_bahan']);
		unset($input['nama_bahan']);
		unset($input['jumlah']);
		unset($input['kode_satuan']);
		unset($input['nama_satuan']);
		unset($input['harga_satuan']);
		unset($input['id_item']);

		$this->db->select('*') ;
		@$query_akun = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>$input['kode_sub']))->row();
		@$kode_sub = $query_akun->kode_sub_kategori_akun;
		@$nama_sub = $query_akun->nama_sub_kategori_akun;
		@$kode_kategori = $query_akun->kode_kategori_akun;
		@$nama_kategori = $query_akun->nama_kategori_akun;
		@$kode_jenis = $query_akun->kode_jenis_akun;
		@$nama_jenis = $query_akun->nama_jenis_akun;

		$this->db->select('*, SUM(subtotal)as grand_total') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		$grand_total = $data->grand_total ;

		unset($input['kode_sub']);
		unset($input['diskon_item']);
		unset($input['kode_edit_penjualan']);

		$this->db->select_max('urut');
		$result = $this->db->get_where('transaksi_pembelian');
		$hasil = @$result->result();
		if($result->num_rows()) $no = ($hasil[0]->urut)+1;
		else $no = 1;

		if($no<10)$no = '000'.$no;
		else if($no<100)$no = '00'.$no;
		else if($no<1000)$no = '0'.$no;
		else if($no<10000)$no = ''.$no;
                  //else if($no<100000)$no = $no;
		$code = $no;

		$this->db->select('kode_pembelian');
		$get_kode_pembelian = $this->db->get('master_setting');
		$hasil_kode_pembelian = $get_kode_pembelian->row();


		unset($input['jenis_diskon']);
		$input['tanggal_pembelian'] = date("Y-m-d") ;
		$input['total_nominal'] = $grand_total ;
		@$input['grand_total'] = @$grand_total - $input['diskon_rupiah'];
		$input['petugas'] = $nama_petugas ;
		$input['id_petugas'] = $id_petugas;
		$input['keterangan'] = '' ;
		$input['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
		$input['status'] = 'belum divalidasi';
		$input['position'] = 'gudang' ;
		$input['kode_transaksi'] = @$hasil_kode_pembelian->kode_pembelian."_".date("dmYhis")."_".$code;
		$input['urut'] = $no;
		if ($input['proses_pembayaran']=='credit') {
			$input['jatuh_tempo'] = $input['jatuh_tempo'];
		}else{
			$input['jatuh_tempo'] = '-';
		}
		
		$tabel_transaksi_pembelian = $this->db->insert("transaksi_pembelian", $input);
		//echo $this->db->last_query();
		// ---------------------singkron----------------------------------------------------------------------------------------
		// $singkron_query = $this->db->last_query();
		// $singkron['jenis_singkron'] = 'tambah';
		// $singkron['query'] = $singkron_query;
		// $singkron['status'] = 'pending';
		// $this->db->insert("singkronasi", $singkron);

		if($tabel_transaksi_pembelian){

			if(@$input['proses_pembayaran'] == 'cash'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $grand_total - $input['diskon_rupiah'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
			}
			else if(@$input['proses_pembayaran'] == 'debet'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $grand_total - $input['diskon_rupiah'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
			}
			else if(@$input['proses_pembayaran'] == 'credit'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $input['dibayar'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);

				$data_hutang['kode_transaksi'] = $kode_pembelian ;
				$get_nama_suplier =$this->db->get('transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
				$hasil_supplier=$get_nama_suplier->row();

				$data_hutang['kode_supplier'] =  $hasil_supplier->kode_supplier;
				$data_hutang['nama_supplier'] =  $hasil_supplier->nama_supplier ;
				$data_hutang['nominal_hutang'] = ($grand_total - $input['diskon_rupiah']) - $input['dibayar'];
				$data_hutang['angsuran'] = '' ;
				$data_hutang['sisa'] = ($grand_total - $input['diskon_rupiah']) - $input['dibayar'] ;
				$data_hutang['tanggal_transaksi'] = date("Y-m-d") ;
				$data_hutang['petugas'] = $nama_petugas ;
				$data_hutang['id_petugas'] = $id_petugas;

				$hutang = $this->db->insert("transaksi_hutang", $data_hutang);
				// $this->db->last_query();

			}
			$status_po['status']='selesai';
			$this->db->update('transaksi_po',$status_po,array('kode_po'=>$kode_po));
			$this->db->delete( 'opsi_transaksi_pembelian_temp', array('kode_pembelian' => $kode_pembelian) );

				    //$this->db->truncate('opsi_transaksi_pembelian_temp');
			echo '1|<div class="alert alert-success">Berhasil Melakukan Pembelian.</div>';
			echo "|".@$tidak_sesuai;  
		}
		else{
			echo '1|<div class="alert alert-danger">Gagal Melakukan Pembelian (Trx_pmbelian) .</div>';  
		}
			//}else{
				//echo '1|<div class="alert alert-danger">Gagal Melakukan Pembelian (update_stok).</div>';  
			//}
		
	}

	public function simpan_transaksi_pembayaran()
	{
		$input = $this->input->post();
		$kode_pembelian = $input['kode_pembelian'];
		$kode_po = $input['kode_po'];
		$proses_bayar = $input['proses_pembayaran'];

		$get_id_petugas = $this->session->userdata('astrosession');
		$id_petugas = $get_id_petugas->id;
		$nama_petugas = $get_id_petugas->uname;

		$this->db->select('*, SUM(subtotal)as grand_total, SUM(subtotal_retur)as grand_total_retur') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		$grand_total = $data->grand_total;
		$grand_total_retur = $data->grand_total_retur;
		$tot_pembelian = $grand_total - $input['diskon_rupiah'] - $grand_total_retur;


		unset($input['kode_po2']);
		unset($input['keterangan']);
		unset($input['tgl_barang_datang']);


		if($input['dibayar'] < $tot_pembelian && $proses_bayar != 'credit'){
			echo '0|<div class="alert alert-danger">Periksa nilai pembayaran.</div>';  
		}
		else{
			$this->db->select('*') ;
			$query_pembelian_temp = $this->db->get_where('opsi_transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));

			$total = 0;
			foreach ($query_pembelian_temp->result() as $item){
				$data_opsi['kode_pembelian'] = $item->kode_pembelian;
				$data_opsi['kode_po'] = $kode_po;
				$data_opsi['kategori_bahan'] = $item->kategori_bahan;
				$data_opsi['kode_bahan'] = $item->kode_bahan;
				$data_opsi['nama_bahan'] = $item->nama_bahan;
				$data_opsi['jumlah'] = $item->jumlah;
				$data_opsi['kode_satuan'] = $item->kode_satuan;
				$data_opsi['nama_satuan'] = $item->nama_satuan;
				$data_opsi['harga_satuan'] = $item->harga_satuan;
		     	//$data_opsi['diskon_item'] = $item->diskon_item;
				$data_opsi['kode_supplier'] = $input['kode_supplier'];
				$data_opsi['nama_supplier'] = $input['nama_supplier'];
				$data_opsi['subtotal'] = $item->subtotal;
				$data_opsi['position'] = 'gudang';
				$hpp_pembelian=$item->harga_satuan;
			//$tabel_opsi_transaksi_pembelian = $this->db->insert("opsi_transaksi_pembelian", $data_opsi);

				$grand_total = $total + $item->subtotal;
				$harga_satuan = $item->harga_satuan;
				$nama_bahan = $item->nama_bahan;
				$stok_masuk = $item->jumlah;
				$kode_pembelian = $item->kode_pembelian;
				$kategori_bahan = $item->kategori_bahan;
				$kode_bahan = $item->kode_bahan;
				$nama_supplier = $item->nama_supplier;
				$kode_supplier = $item->kode_supplier;

				if($kategori_bahan=='stok'){
					$kode_default = $this->db->get('setting_gudang');
					$hasil_kode_default = $kode_default->row();

					$this->db->select('*') ;
					$real_stock = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
					$stok_real = $real_stock->row()->real_stock ;
					$konversi_stok = $real_stock->row()->jumlah_dalam_satuan_pembelian ;

					if(empty($stok_real)){

						$data_stok['real_stock'] = $stok_masuk * $konversi_stok;
					//$this->db->update('master_bahan_baku',$data_stok,array('kode_bahan_baku'=>$kode_bahan));

						$this->db->select('kode_unit');
						$kode_default = $this->db->get('setting_gudang');
						$hasil_kode_default = $kode_default->row();
						$kode_unit = @$hasil_kode_default->kode_unit;
						$kode_rak = @$hasil_kode_default->kode_rak;

						$this->db->select('nama_unit');
						$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
						$hasil_master_unit = $master_unit->row();
						$nama_unit = @$hasil_master_unit->nama_unit;

						$this->db->select('nama_rak');
						$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
						$hasil_master_rak = $master_rak->row();
						$nama_rak = @$hasil_master_rak->nama_rak;

						$harga_satuan_stok = $harga_satuan / $konversi_stok;

						$stok['jenis_transaksi'] = 'pembelian' ;
						$stok['kode_transaksi'] = $kode_pembelian ;
						$stok['kategori_bahan'] = $kategori_bahan ;
						$stok['kode_bahan'] = $kode_bahan ;
						$stok['nama_bahan'] = $nama_bahan ;
						$stok['stok_keluar'] = '';
						$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
						$stok['posisi_awal'] = 'supplier';
						$stok['posisi_akhir'] = 'gudang';
						$stok['hpp'] = $harga_satuan_stok ;
						$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
						$stok['kode_unit_tujuan'] = $kode_unit;
						$stok['nama_unit_tujuan'] = $nama_unit;
						$stok['kode_rak_tujuan'] = $kode_rak;
						$stok['nama_rak_tujuan'] = $nama_rak;
						$stok['id_petugas'] = $id_petugas;
						$stok['nama_petugas'] = $nama_petugas;
						$stok['tanggal_transaksi'] = date("Y-m-d") ;

						//$transaksi_stok = $this->db->insert("transaksi_stok", $stok);
						$hpp = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan));
						$hpp_hasil = $hpp->row()->hpp ;
						
						$hpp_baru['hpp']= ($hpp_hasil + $hpp_pembelian) /2 ;
						$this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));
						echo $this->db->last_query();
					}
					else{
						$this->db->select('*') ;
						$jumlah_stok = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan,'kode_unit'=>$hasil_kode_default->kode_unit));
						$jumlah_stok = $jumlah_stok->row()->real_stock ;

						$data_stok['real_stock'] = ($stok_masuk * $konversi_stok)  + $jumlah_stok;
					//$this->db->update('master_bahan_baku',$data_stok,array('kode_bahan_baku'=>$kode_bahan));

						$this->db->select('kode_unit');
						$kode_default = $this->db->get('setting_gudang');
						$hasil_kode_default = $kode_default->row();
						$kode_unit = @$hasil_kode_default->kode_unit;
						$kode_rak = @$hasil_kode_default->kode_rak;

						$this->db->select('nama_unit');
						$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
						$hasil_master_unit = $master_unit->row();
						$nama_unit = @$hasil_master_unit->nama_unit;

						$this->db->select('nama_rak');
						$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
						$hasil_master_rak = $master_rak->row();
						$nama_rak = @$hasil_master_rak->nama_rak;

						$harga_satuan_stok = $harga_satuan / $konversi_stok;

						$stok['jenis_transaksi'] = 'pembelian' ;
						$stok['kode_transaksi'] = $kode_pembelian ;
						$stok['kategori_bahan'] = $kategori_bahan ;
						$stok['kode_bahan'] = $kode_bahan ;
						$stok['nama_bahan'] = $nama_bahan ;
						$stok['stok_keluar'] = '';
						$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
						$stok['posisi_awal'] = 'supplier';
						$stok['posisi_akhir'] = 'gudang';
						$stok['hpp'] = $harga_satuan_stok ;
						$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
						$stok['kode_unit_tujuan'] = $kode_unit;
						$stok['nama_unit_tujuan'] = $nama_unit;
						$stok['kode_rak_tujuan'] = $kode_rak;
						$stok['nama_rak_tujuan'] = $nama_rak;
						$stok['id_petugas'] = $id_petugas;
						$stok['nama_petugas'] = $nama_petugas;
						$stok['tanggal_transaksi'] = date("Y-m-d") ;

						//$transaksi_stok = $this->db->insert("transaksi_stok", $stok);
						$hpp = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan));
						$hpp_hasil = $hpp->row()->hpp ;
						
						$hpp_baru['hpp']= ($hpp_hasil + $hpp_pembelian) /2 ;
						$this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));
						echo $this->db->last_query();
					}
				}
				if($kategori_bahan=='barang'){
					$kode_default = $this->db->get('setting_gudang');
					$hasil_kode_default = $kode_default->row();

					$this->db->select('*') ;
					$real_stock = $this->db->get_where('master_barang',array('kode_barang'=>$kode_bahan,'position'=>$hasil_kode_default->kode_unit));
					$stok_real = $real_stock->row()->real_stok ;
					$konversi_stok = $real_stock->row()->jumlah_dalam_satuan_pembelian ;

					if(empty($stok_real)){

						$data_stok['real_stok'] = $stok_masuk * $konversi_stok;
					//$this->db->update('master_barang',$data_stok,array('kode_barang'=>$kode_bahan));

						$this->db->select('kode_unit');
						$kode_default = $this->db->get('setting_gudang');
						$hasil_kode_default = $kode_default->row();
						$kode_unit = @$hasil_kode_default->kode_unit;
						$kode_rak = @$hasil_kode_default->kode_rak;

						$this->db->select('nama_unit');
						$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
						$hasil_master_unit = $master_unit->row();
						$nama_unit = @$hasil_master_unit->nama_unit;

						$this->db->select('nama_rak');
						$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
						$hasil_master_rak = $master_rak->row();
						$nama_rak = @$hasil_master_rak->nama_rak;

						$harga_satuan_stok = $harga_satuan / $konversi_stok;

						$stok['jenis_transaksi'] = 'pembelian' ;
						$stok['kode_transaksi'] = $kode_pembelian ;
						$stok['kategori_bahan'] = $kategori_bahan ;
						$stok['kode_bahan'] = $kode_bahan ;
						$stok['nama_bahan'] = $nama_bahan ;
						$stok['stok_keluar'] = '';
						$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
						$stok['posisi_awal'] = 'supplier';
						$stok['posisi_akhir'] = 'gudang';
						$stok['hpp'] = $harga_satuan_stok ;
						$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
						$stok['kode_unit_tujuan'] = $kode_unit;
						$stok['nama_unit_tujuan'] = $nama_unit;
						$stok['kode_rak_tujuan'] = $kode_rak;
						$stok['nama_rak_tujuan'] = $nama_rak;
						$stok['id_petugas'] = $id_petugas;
						$stok['nama_petugas'] = $nama_petugas;
						$stok['tanggal_transaksi'] = date("Y-m-d") ;

						//$transaksi_stok = $this->db->insert("transaksi_stok", $stok);

					}
					else{
						$this->db->select('*') ;
						$jumlah_stok = $this->db->get_where('master_barang',array('kode_barang'=>$kode_bahan,'position'=>$hasil_kode_default->kode_unit));
						$jumlah_stok = $jumlah_stok->row()->real_stok ;

						$data_stok['real_stok'] = ($stok_masuk * $konversi_stok)  + $jumlah_stok;
					//$this->db->update('master_barang',$data_stok,array('kode_barang'=>$kode_bahan));

						$this->db->select('kode_unit');
						$kode_default = $this->db->get('setting_gudang');
						$hasil_kode_default = $kode_default->row();
						$kode_unit = @$hasil_kode_default->kode_unit;
						$kode_rak = @$hasil_kode_default->kode_rak;

						$this->db->select('nama_unit');
						$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
						$hasil_master_unit = $master_unit->row();
						$nama_unit = @$hasil_master_unit->nama_unit;

						$this->db->select('nama_rak');
						$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
						$hasil_master_rak = $master_rak->row();
						$nama_rak = @$hasil_master_rak->nama_rak;

						$harga_satuan_stok = $harga_satuan / $konversi_stok;

						$stok['jenis_transaksi'] = 'pembelian' ;
						$stok['kode_transaksi'] = $kode_pembelian ;
						$stok['kategori_bahan'] = $kategori_bahan ;
						$stok['kode_bahan'] = $kode_bahan ;
						$stok['nama_bahan'] = $nama_bahan ;
						$stok['stok_keluar'] = '';
						$stok['stok_masuk'] = $stok_masuk * $konversi_stok ;
						$stok['posisi_awal'] = 'supplier';
						$stok['posisi_akhir'] = 'gudang';
						$stok['hpp'] = $harga_satuan_stok ;
						$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
						$stok['kode_unit_tujuan'] = $kode_unit;
						$stok['nama_unit_tujuan'] = $nama_unit;
						$stok['kode_rak_tujuan'] = $kode_rak;
						$stok['nama_rak_tujuan'] = $nama_rak;
						$stok['id_petugas'] = $id_petugas;
						$stok['nama_petugas'] = $nama_petugas;
						$stok['tanggal_transaksi'] = date("Y-m-d") ;

						//$transaksi_stok = $this->db->insert("transaksi_stok", $stok);

					}
				}

				if($kategori_bahan==''){
					$this->db->select('*') ;
					$real_stock = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
					$stok_real = $real_stock->row()->real_stock ;
					$kode_konversi_stok_bahan_jadi = $real_stock->row()->kode_satuan_stok ;


					$konversi_stok_bahan_jadi = $this->db->get_where('master_satuan',array('kode'=>$kode_konversi_stok_bahan_jadi));
					$konversi_stok_bahan_jadi = $konversi_stok_bahan_jadi->row()->acuan ;

					if(empty($stok_real)){

						$data_stok['real_stock'] = $stok_masuk;
					//$this->db->update('master_bahan_jadi',$data_stok,array('kode_bahan_jadi'=>$kode_bahan));

						$this->db->select('kode_unit,kode_rak');
						$kode_default = $this->db->get('master_setting');
						$hasil_kode_default = $kode_default->row();
						$kode_unit = @$hasil_kode_default->kode_unit;
						$kode_rak = @$hasil_kode_default->kode_rak;

						$this->db->select('nama_unit');
						$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
						$hasil_master_unit = $master_unit->row();
						$nama_unit = @$hasil_master_unit->nama_unit;

						$this->db->select('nama_rak');
						$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
						$hasil_master_rak = $master_rak->row();
						$nama_rak = @$hasil_master_rak->nama_rak;

						$harga_satuan_stok =$harga_satuan / $konversi_stok_bahan_jadi;

						$stok['jenis_transaksi'] = 'pembelian' ;
						$stok['kode_transaksi'] = $kode_pembelian ;
						$stok['kategori_bahan'] = $kategori_bahan ;
						$stok['kode_bahan'] = $kode_bahan ;
						$stok['nama_bahan'] = $nama_bahan ;
						$stok['stok_keluar'] = '';
						$stok['stok_masuk'] = $stok_masuk ;
						$stok['posisi_awal'] = 'supplier';
						$stok['posisi_akhir'] = 'gudang';
						$stok['hpp'] = $harga_satuan_stok ;
						$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
						$stok['kode_unit_tujuan'] = $kode_unit;
						$stok['nama_unit_tujuan'] = $nama_unit;
						$stok['kode_rak_tujuan'] = $kode_rak;
						$stok['nama_rak_tujuan'] = $nama_rak;
						$stok['id_petugas'] = $id_petugas;
						$stok['nama_petugas'] = $nama_petugas;
						$stok['tanggal_transaksi'] = date("Y-m-d") ;
						$hpp = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan));
						$hpp_hasil = $hpp->row()->hpp ;
						
						$hpp_baru['hpp']= ($hpp_hasil + $hpp_pembelian) /2 ;
						$this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));
						echo $this->db->last_query();
						//$transaksi_stok = $this->db->insert("transaksi_stok", $stok);

					}
					else{
						$this->db->select('*') ;
						$jumlah_stok = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>$kode_bahan));
						$jumlah_stok = $jumlah_stok->row()->real_stock ;

						$data_stok['real_stock'] = $stok_masuk + $jumlah_stok;
					//$this->db->update('master_bahan_jadi',$data_stok,array('kode_bahan_jadi'=>$kode_bahan));

						$this->db->select('kode_unit,kode_rak');
						$kode_default = $this->db->get('master_setting');
						$hasil_kode_default = $kode_default->row();
						$kode_unit = @$hasil_kode_default->kode_unit;
						$kode_rak = @$hasil_kode_default->kode_rak;

						$this->db->select('nama_unit');
						$master_unit = $this->db->get_where('master_unit', array('kode_unit'=>$kode_unit));
						$hasil_master_unit = $master_unit->row();
						$nama_unit = @$hasil_master_unit->nama_unit;

						$this->db->select('nama_rak');
						$master_rak = $this->db->get_where('master_rak', array('kode_rak'=>$kode_rak));
						$hasil_master_rak = $master_rak->row();
						$nama_rak = @$hasil_master_rak->nama_rak;

						$harga_satuan_stok =$harga_satuan / $konversi_stok_bahan_jadi;

						$stok['jenis_transaksi'] = 'pembelian' ;
						$stok['kode_transaksi'] = $kode_pembelian ;
						$stok['kategori_bahan'] = $kategori_bahan ;
						$stok['kode_bahan'] = $kode_bahan ;
						$stok['nama_bahan'] = $nama_bahan ;
						$stok['stok_keluar'] = '';
						$stok['stok_masuk'] = $stok_masuk ;
						$stok['posisi_awal'] = 'supplier';
						$stok['posisi_akhir'] = 'gudang';
						$stok['hpp'] = $harga_satuan_stok ;
						$stok['sisa_stok'] = $stok_masuk * $konversi_stok ;
						$stok['kode_unit_tujuan'] = $kode_unit;
						$stok['nama_unit_tujuan'] = $nama_unit;
						$stok['kode_rak_tujuan'] = $kode_rak;
						$stok['nama_rak_tujuan'] = $nama_rak;
						$stok['id_petugas'] = $id_petugas;
						$stok['nama_petugas'] = $nama_petugas;
						$stok['tanggal_transaksi'] = date("Y-m-d") ;

						$hpp = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$kode_bahan));
						$hpp_hasil = $hpp->row()->hpp ;
						
						$hpp_baru['hpp']= ($hpp_hasil + $hpp_pembelian) /2 ;
						$this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));
						echo $this->db->last_query();
						//$transaksi_stok = $this->db->insert("transaksi_stok", $stok);

					}
				}
			}

			//if($transaksi_stok){
			unset($input['kategori_bahan']);
			unset($input['kode_bahan']);
			unset($input['nama_bahan']);
			unset($input['jumlah']);
			unset($input['kode_satuan']);
			unset($input['nama_satuan']);
			unset($input['harga_satuan']);
			unset($input['id_item']);

			$this->db->select('*') ;
			$query_akun = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>$input['kode_sub']))->row();
			$kode_sub = $query_akun->kode_sub_kategori_akun;
			$nama_sub = $query_akun->nama_sub_kategori_akun;
			$kode_kategori = $query_akun->kode_kategori_akun;
			$nama_kategori = $query_akun->nama_kategori_akun;
			$kode_jenis = $query_akun->kode_jenis_akun;
			$nama_jenis = $query_akun->nama_jenis_akun;

			$this->db->select('*, SUM(subtotal)as grand_total') ;
			$query = $this->db->get_where('opsi_transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
			$data = $query->row();
			$grand_total = $data->grand_total ;

			unset($input['kode_sub']);
			unset($input['kode_po']);
			unset($input['tgl_jatuh_tempo']);

			$input['total_nominal'] = $grand_total ;
			$input['grand_total'] = $grand_total - $input['diskon_rupiah'];


		//$tabel_transaksi_pembelian = $this->db->update("transaksi_pembelian", $input);
			$tabel_transaksi_pembelian = $this->db->update("transaksi_pembelian", $input, array('kode_pembelian'=>$kode_pembelian));

		//if($tabel_transaksi_pembelian){

			if($input['proses_pembayaran'] == 'cash'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $grand_total - $input['diskon_rupiah'] - $grand_total_retur;
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
			}
			else if($input['proses_pembayaran'] == 'debet'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $grand_total - $input['diskon_rupiah'] - $grand_total_retur;
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
			}
			else if($input['proses_pembayaran'] == 'credit'){
				$data_keu['id_petugas'] = $id_petugas;
				$data_keu['petugas'] = $nama_petugas ;
				$data_keu['kode_referensi'] = $kode_pembelian ;
				$data_keu['tanggal_transaksi'] = date("Y-m-d") ;
				$data_keu['keterangan'] = 'pembelian' ;
				$data_keu['nominal'] = $input['dibayar'];
				$data_keu['kode_jenis_keuangan'] = $kode_jenis ;
				$data_keu['nama_jenis_keuangan'] = $nama_jenis ;
				$data_keu['kode_kategori_keuangan'] = $kode_kategori ;
				$data_keu['nama_kategori_keuangan'] = $nama_kategori ;
				$data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
				$data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

				$keuangan = $this->db->insert("keuangan_keluar", $data_keu);

				$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
				$data_hutang['kode_transaksi'] = $kode_pembelian ;
				$data_hutang['kode_supplier'] = $input['kode_supplier'];
				$data_hutang['nama_supplier'] = $hasil_nama_supplier->nama_supplier;
				$data_hutang['nominal_hutang'] = ($grand_total - $input['diskon_rupiah']) - $input['dibayar'];
				$data_hutang['angsuran'] = '' ;
				$data_hutang['sisa'] = ($grand_total - $input['diskon_rupiah']) - $input['dibayar'] - $grand_total_retur;
				$data_hutang['tanggal_transaksi'] = date("Y-m-d") ;
				$data_hutang['petugas'] = $nama_petugas ;
				$data_hutang['id_petugas'] = $id_petugas;
				$data_hutang['tanggal_jatuh_tempo'] = $tgl_jatuh_tempo;

				$this->db->select('kode_pembelian');
				$get_kode_ro = $this->db->get('master_setting');
				$hasil_kode_ro = $get_kode_ro->row();

				$this->db->select_max('urut');
				$result = $this->db->get_where('transaksi_hutang');
				$hasil = @$result->result();
				if($result->num_rows()) $no = ($hasil[0]->urut)+1;
				else $no = 1;

				if($no<10)$no = '000'.$no;
				else if($no<100)$no = '00'.$no;
				else if($no<1000)$no = '0'.$no;
				else if($no<10000)$no = ''.$no;
                  //else if($no<100000)$no = $no;
				$code = $no;
				$data_hutang['kode_hutang'] = @$hasil_kode_ro->kode_pembelian."_".$code;
				$data_hutang['urut'] = $no;
				$hutang = $this->db->insert("transaksi_hutang", $data_hutang);

			}
			//$this->db->delete( 'opsi_transaksi_pembelian_temp', array('kode_pembelian' => $kode_pembelian) );
				    //$this->db->truncate('opsi_transaksi_pembelian_temp');
			echo '1|<div class="alert alert-success">Berhasil Melakukan Pembelian.</div>';  
		//}else{
			//echo '1|<div class="alert alert-danger">Gagal Melakukan Pembelian (Trx_pmbelian) .</div>';  
		//}
			// }else{
			// 	echo '1|<div class="alert alert-danger">Gagal Melakukan Pembelian (update_stok).</div>';  
			// }
		}
		
	}

	public function hapus_bahan_temp(){
		$id = $this->input->post('id');
		$this->db->delete('opsi_transaksi_pembelian_temp',array('id'=>$id));
	}

	public function hapus_transaksi(){
		$kode_pembelian = $this->input->post('kode_pembelian');

		$query = $this->db->get_where('transaksi_stok',array('kode_transaksi'=>$kode_pembelian));
		$data = $query->result();
		foreach ($data as $value) {
			
			$real_stock = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku'=>$value->kode_bahan,'status'=>'sendiri'));
			$stok_real = $real_stock->row()->real_stock ;

			$data_bahan['real_stock']=$stok_real - $value->stok_masuk;
			$this->db->update('master_bahan_baku',$data_bahan,array('kode_bahan_baku'=>$value->kode_bahan,'status'=>'sendiri'));

		}
		$this->db->delete('transaksi_stok',array('kode_transaksi'=>$kode_pembelian));
		$this->db->delete('opsi_transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
		$this->db->delete('transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
		$this->db->delete('transaksi_hutang',array('kode_transaksi'=>$kode_pembelian));
		$this->db->delete('keuangan_keluar',array('kode_referensi'=>$kode_pembelian));

	}

	public function get_rupiah(){
		$dibayar = $this->input->post('dibayar');
		$hasil = format_rupiah($dibayar);
		$kode_pembelian = $this->input->post('kode_pembelian');
		$grand = $this->input->post('grand');

		$this->db->select('*, SUM(subtotal)as grand_total') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		$grand_total = $data->grand_total ;

		if($dibayar == $grand){
			echo 'Kembali '.format_rupiah(0).'|'.$hasil;
		}
		else if($dibayar > $grand){
			echo 'Kembali '.format_rupiah($dibayar - $grand).'|'.$hasil;
		}
		else if($dibayar < $grand){
			echo 'Kurang '.format_rupiah($grand - $dibayar).'|'.$hasil;
		}

	}
	public function get_opsi_temp(){
		$kode_pembelian = $this->input->post('kode_pembelian');
		
		
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->result();

		if(empty($data)){
			$data_opsi = $this->db->get_where('opsi_transaksi_pembelian',array('kode_pembelian'=>$kode_pembelian));
			$hasil_data_opsi = $data_opsi->result_array();
			foreach ($hasil_data_opsi as  $value) {
				unset($value['id']);
				unset($value['kode_po']);
				unset($value['status']);
				unset($value['status_sesuai']);
				unset($value['jumlah_retur']);
				unset($value['subtotal_retur']);

				$this->db->insert('opsi_transaksi_pembelian_temp',$value);
			}
		}
		

	}

	public function get_rupiah_kredit(){
		$dibayar = $this->input->post('dibayar');
		$hasil = format_rupiah($dibayar);
		$kode_pembelian = $this->input->post('kode_pembelian');
		$grand = $this->input->post('grand');

		$this->db->select('*, SUM(subtotal)as grand_total') ;
		$query = $this->db->get_where('opsi_transaksi_pembelian_temp',array('kode_pembelian'=>$kode_pembelian));
		$data = $query->row();
		$grand_total = $data->grand_total ;

		if($dibayar == $grand){
			echo 'Hutang '.format_rupiah(0).'|'.$hasil;
		}
		else if($dibayar > $grand){
			echo 'Tidak Boleh melebihi Grand Total |'.$hasil;
		}
		else if($dibayar < $grand){
			echo 'Hutang '.format_rupiah($grand - $dibayar).'|'.$hasil;
		}

	}

	public function get_bahan()
	{
		$param = $this->input->post();
		$jenis = $param['jenis_bahan'];
		$unit = $this->db->get('setting_gudang');
		$hasil_unit = $unit->row(); 
		if($jenis == 'bahan baku'){
			$opt = '';
			$query = $this->db->get_where('master_bahan_baku',array('kode_unit'=> $hasil_unit->kode_unit));
			$opt = '<option value="">--Pilih Bahan Baku--</option>';
			foreach ($query->result() as $key => $value) {
				$opt .= '<option value="'.$value->kode_bahan_baku.'">'.$value->nama_bahan_baku.'</option>';  
			}
			echo $opt;

		}else if ($jenis == 'bahan jadi') {
			$opt = '';
			$query = $this->db->get_where('master_bahan_jadi',array('kode_unit'=> $hasil_unit->kode_unit));
			$opt = '<option value="">--Pilih Bahan Jadi--</option>';
			foreach ($query->result() as $key => $value) {
				$opt .= '<option value="'.$value->kode_bahan_jadi.'">'.$value->nama_bahan_jadi.'</option>';  
			}
			echo $opt;
		}else if ($jenis == 'barang') {
			$opt = '';
			$query = $this->db->get_where('master_barang',array('position'=> $hasil_unit->kode_unit));
			$opt = '<option value="">--Pilih Barang--</option>';
			foreach ($query->result() as $key => $value) {
				$opt .= '<option value="'.$value->kode_barang.'">'.$value->nama_barang.'</option>';  
			}
			echo $opt;
		}
	}

	public function get_satuan()
	{
		$kode_bahan = $this->input->post('kode_bahan');
		$nama_bahan = $this->db->get_where('master_bahan_baku',array('kode_bahan_baku' => $kode_bahan));
		$hasil_bahan = $nama_bahan->row();

		echo json_encode($hasil_bahan);

	}

	public function get_temp_pembelian(){
		$id = $this->input->post('id');
		$pembelian = $this->db->get_where('opsi_transaksi_pembelian_temp',array('id'=>$id));
		$hasil_pembelian = $pembelian->row();
		echo json_encode($hasil_pembelian);
	}

	public function keterangan()
	{
		$data = $this->input->post();
		$hutang = $this->db->insert("setting_pembelian", $data);		
	}
	public function sesuaikan_hpp()
	{
		$kode_bahan = $this->input->post('kode_bahan');
		$hpp = $this->input->post('hpp_baru');
		$hpp_baru['hpp']= $hpp;
		$this->db->update('master_bahan_baku',$hpp_baru,array('kode_bahan_baku'=>$kode_bahan));	

	}

}

