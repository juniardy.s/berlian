<?php 
$tgl_awal=$this->input->post('tgl_awal');
$tgl_akhir=$this->input->post('tgl_akhir');

?>

<div class="col-md-12">

</div>
<div class="col-md-12" id="container" >

</div>

<!------------------------------------------------------------------------------------------------------>

<script type="text/javascript">

  Highcharts.chart('container', {
    chart: {
      type: 'line'
    },
    title: {
      text: 'Grafik Omset'
    },
    subtitle: {
      text: '<?php echo @TanggalIndo($tgl_awal)." "."-"." ".@TanggalIndo($tgl_akhir)  ?>'
    },
    xAxis: {
     categories: [

     <?php 

     if (!empty($tgl_awal) && !empty($tgl_akhir)) {
      $this->db->where('tanggal_penjualan >=', $tgl_awal);
      $this->db->where('tanggal_penjualan <=', $tgl_akhir);
      $this->db->select('');
      $this->db->group_by('tanggal_penjualan');
      $omset = $this->db->get('transaksi_penjualan');
    } else {
      $this->db->select('');
      $this->db->group_by('tanggal_penjualan');
      $omset = $this->db->get('transaksi_penjualan');
    }

    $get_tanggal =$omset->result();
    foreach($get_tanggal as $tgl){
      $t = $tgl->tanggal_penjualan;
      echo '"'.TanggalIndo($t).'"'.",";
    } ?>

    ]
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  yAxis: {
    title: {
     text: 'Jumlah Omset',
     style: {
      color: 'black',
      fontWeight: 'bold'
    }
  }
},
plotOptions: {
  series: {
    color:"black"
  }
},
series: [{
  name: ' ',
  data: [

  <?php
  foreach($get_tanggal as $tgl){
    $this->db->select_sum('grand_total');
    $hasil_jual = $this->db->get_where('transaksi_penjualan',array('tanggal_penjualan'=>$tgl->tanggal_penjualan));
    $hasil_jumlah = $hasil_jual->row();
    echo "$hasil_jumlah->grand_total,";
  }

  ?>
  ],

}]
});


</script>