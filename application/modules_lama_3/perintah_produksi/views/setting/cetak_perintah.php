<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
    <?php
    $kode = $this->uri->segment(3);
    ?>
    <style>
    table{
        border-style: solid;
        border-width: 1px;
        border-collapse: collapse;
    }
    table tr td{
        border-style: solid;
        border-width: 1px;
        border-collapse: collapse;
    }
    table th{
        border-style: solid;
        border-width: 1px;
        border-collapse: collapse;
    }
    </style>

    <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
<hr>
    <h3><center>PERINTAH PRODUKSI</center></h3>
    <?php
    $transaksi = $this->db->get_where('transaksi_perintah_produksi',array('kode_transaksi'=>$kode));
    $hasil_transaksi = $transaksi->row();
    ?>
    <p>Kode Transaksi : <?php echo $kode; ?></p>
    <p>Perintah Produksi : <?php if($hasil_transaksi->jenis_produksi=='sendiri'){echo 'Factory';}else{ echo "Plasma";}; ?></p>
    <table width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Produk</th>
                <th>Jumlah</th>
                <th>Opsi Produk</th>
                <th>Opsi Jumlah</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $opsi_transaksi = $this->db->get_where('opsi_transaksi_perintah_produksi',array('kode_transaksi'=>$kode));
            $hasil_opsi_transaksi = $opsi_transaksi->result();
            $no=1;
            foreach ($hasil_opsi_transaksi as $value) {
                ?>
                <tr>
                    <td align="center"><?php echo $no++;?></td>
                    <td><?php echo $value->nama_karyawan;?></td>
                    <td><?php echo $value->nama_bahan;?></td>
                    <td align="center"><?php echo @format_angka($value->jumlah);?></td>
                    <?php
                    if($value->kategori_bahan=='setengah_jadi'){
                        $opsi_bahan = $this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$value->kode_bahan));
                        $hasil_opsi_bahan = $opsi_bahan->result();
                    }elseif ($value->kategori_bahan=='jadi') {
                        $opsi_bahan = $this->db->get_where('opsi_master_bahan_jadi',array('kode_bahan_jadi'=>$value->kode_bahan));
                        $hasil_opsi_bahan = $opsi_bahan->result();
                    }
                    
                    
                    $no_opsi=1;
                    foreach ($hasil_opsi_bahan as $bahan) {
                        if($no_opsi==1){
                            ?>
                            <td><?php echo $bahan->nama_bahan;?></td>
                            <td align="center"><?php echo @format_angka($bahan->jumlah * $value->jumlah);?></td>
                            <?php

                        }else{
                            ?>
                            <tr>
                                <td colspan="4"></td>
                                <td><?php echo $bahan->nama_bahan;?></td>
                                <td align="center"><?php echo @format_angka($bahan->jumlah *  $value->jumlah);?></td>
                            </tr>
                            <?php
                        }
                        $no_opsi++;
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </tbody>

    </table>


</body>