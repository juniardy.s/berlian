<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class withdraw extends CI_Controller {
	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
		$this->load->library('form_validation');
		$this->load->library('session');
	}	

	public function index()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('withdraw/withdraw/daftar_withdraw', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function proses_withdraw()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('withdraw/withdraw/proses_withdraw', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function tambah_gaji_tetap()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/tambah_gaji_tetap', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function daftar()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/daftar_penggajian', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function detail()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('withdraw/withdraw/detail_transaksi', $data, TRUE);
		$this->load->view('main', $data);	
	}

	public function detail_gaji_tetap()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/detail_gaji_tetap', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function detail_withdraw()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('withdraw/withdraw/detail_withdraw', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function withdraw()
	{
		$data['aktif'] = 'setting';
		$data['halaman'] = $this->load->view('penggajian/penggajian/withdraw', $data, TRUE);
		$this->load->view('main', $data);	
	}
	public function get_rupiah()
	{
		$nominal = format_rupiah($this->input->post('value'));
		echo $nominal;
	}

	public function get_gaji()
	{
		$data = $this->input->post();
		$karyawan = $this->db->get_where('master_karyawan', array('kode_karyawan' =>$data['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();
		echo json_encode($hasil_karyawan);

	}

	public function simpan_gaji_tetap()
	{
		$input = $this->input->post();
		$user = $this->session->userdata('astrosession');

		$data['kode_karyawan']=$input['kode_karyawan'];
		$karyawan = $this->db->get_where('master_karyawan', array('kode_karyawan' =>$input['kode_karyawan']));
		$hasil_karyawan = $karyawan->row();

		$data['nama_karyawan']=$hasil_karyawan->nama_karyawan;
		$data['total_gaji']=$input['total_gaji'];
		$data['tanggal']=$input['tanggal'];
		$data['jenis_karyawan']= 'tetap';
		$this->db->insert('transaksi_penggajian', $data);
		//echo $this->db->last_query();
		$keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.7.1'));
		$hasil_keuangan = $keuangan->row();

		$keluar['kode_jenis_keuangan'] = $hasil_keuangan->kode_jenis_akun;
		$keluar['nama_jenis_keuangan'] = $hasil_keuangan->nama_jenis_akun;
		$keluar['kode_kategori_keuangan'] = $hasil_keuangan->kode_kategori_akun;
		$keluar['nama_kategori_keuangan'] = $hasil_keuangan->nama_kategori_akun;
		$keluar['kode_sub_kategori_keuangan'] = $hasil_keuangan->kode_sub_kategori_akun;
		$keluar['nama_sub_kategori_keuangan'] = $hasil_keuangan->nama_sub_kategori_akun;
		$keluar['nominal'] = $input['total_gaji'];
		$keluar['keterangan'] = "Pembayaran Gaji"." ".$hasil_karyawan->nama_karyawan;
		$keluar['tanggal_transaksi'] = date("Y-m-d");
		$keluar['id_petugas'] =$user->id;
		$keluar['petugas'] = $user->uname;
		$this->db->insert('keuangan_keluar',$keluar);
		//echo $this->db->last_query();


		echo 'berhasil';
		
	}

	public function simpan_withdraw()
	{
		$input = $this->input->post();
		$user = $this->session->userdata('astrosession');
		if($input['angsuran_gaji']>$input['total_gaji'] or $input['angsuran_gaji']==''){
			echo 'salah';
		} else{
			$kode_karyawan=$input['kode_karyawan'];
			$get_gaji = $this->db->get_where('transaksi_withdraw',array('kode_petugas'=>$kode_karyawan));
			$hasil_get_gaji = $get_gaji->row();

			$angsuran 		= $hasil_get_gaji->pengambilan;
			$hasil_angsuran = $angsuran + $input['angsuran_gaji'];

			$sisa 			= $hasil_get_gaji->sisa;
			$hasil_sisa		= $sisa - $input['angsuran_gaji'];

			$data['pengambilan']=$hasil_angsuran;
			$data['sisa']=$hasil_sisa;
			$this->db->update("transaksi_withdraw", $data,array('kode_petugas'=>$kode_karyawan));


			$opsi_widthdraw['kode_petugas']=$kode_karyawan;
			$opsi_widthdraw['nama_petugas']=$hasil_get_gaji->nama_petugas;
			$opsi_widthdraw['kategori_petugas']=$hasil_get_gaji->nama_petugas;
			$opsi_widthdraw['pengambilan']= $input['angsuran_gaji'];
			$opsi_widthdraw['sisa']=$hasil_sisa;
			$opsi_widthdraw['tanggal_transaksi']=date('Y-m-d');
			$this->db->insert('opsi_transaksi_withdraw',$opsi_widthdraw);
			
			$user = $this->session->userdata('astrosession');
			$keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.7.2'));
			$hasil_keuangan = $keuangan->row();

			$keluar['kode_jenis_keuangan'] = $hasil_keuangan->kode_jenis_akun;
			$keluar['nama_jenis_keuangan'] = $hasil_keuangan->nama_jenis_akun;
			$keluar['kode_kategori_keuangan'] = $hasil_keuangan->kode_kategori_akun;
			$keluar['nama_kategori_keuangan'] = $hasil_keuangan->nama_kategori_akun;
			$keluar['kode_sub_kategori_keuangan'] = $hasil_keuangan->kode_sub_kategori_akun;
			$keluar['nama_sub_kategori_keuangan'] = $hasil_keuangan->nama_sub_kategori_akun;
			$keluar['nominal'] =  $input['angsuran_gaji'];
			$keluar['keterangan'] = "Pembayaran Withdraw"." ".$hasil_get_gaji->nama_petugas;
			$keluar['tanggal_transaksi'] = date("Y-m-d");
			$keluar['id_petugas'] =$user->id;
			$keluar['petugas'] = $user->uname;
			$this->db->insert('keuangan_keluar',$keluar);

			echo 'berhasil';
		}
	}

	function tarik_tunai(){

		$tarik 	=	$_POST['withdraw'];
		$current_saldo 	=	$_POST['current_sisa'];



		if ($tarik > $current_saldo) {
					$msg['status'] = 500;
		}	else {
			$insert_to_withdraw 	=	[
				'kode_pengambilan' => date('Y/m/d').$_POST['customer'].'-'.$tarik,
				'tujuan_komisi' => $this->baca_tipe_code($_POST['customer']),
				'kode_sales' => $_POST['customer'],
				'nominal_withdraw' => $tarik ,
				'tanggal_withdraw' => date('Y-m-d')
			];

			$res 	=	$this->db->insert('transaksi_komisi_withdraw' , $insert_to_withdraw);

			/* insert to laporan pengeluaran*/
			$user = $this->session->userdata('astrosession');
			$keuangan = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=>'2.7.2'));
			$hasil_keuangan = $keuangan->row();

			$insert_to_keluar    =	[
				'kode_jenis_keuangan'        =>$hasil_keuangan->kode_jenis_akun ,
				'nama_jenis_keuangan'        =>$hasil_keuangan->nama_jenis_akun ,
				'kode_kategori_keuangan'     =>$hasil_keuangan->kode_kategori_akun ,
				'nama_kategori_keuangan'     =>$hasil_keuangan->nama_kategori_akun ,
				'kode_sub_kategori_keuangan' =>$hasil_keuangan->kode_sub_kategori_akun ,
				'nama_sub_kategori_keuangan' =>$hasil_keuangan->nama_sub_kategori_akun ,
				'nominal'                    =>$tarik ,
				'keterangan'                 =>'Penarikan Withdraw oleh customer '.$_POST['customer'] ,
				'tanggal_transaksi'          =>date('Y-m-d') ,
				'id_petugas'                 =>$user->id ,
				'petugas'                    =>$user->uname 

			];
				$this->db->insert('keuangan_keluar',$insert_to_keluar);


			$msg['status'] = 200;


		}	
		echo json_encode($msg);

	}

	function baca_tipe_code($code){
		$tipe   = substr($code, 0, 3);

		if ($tipe == 'SA_') {
		  $tbl = 'SALES';
		} else {
		  $tbl = 'SOPIR';
		}

		return $tbl;

	}
}

/* End of file penggajian.php */
/* Location: ./application/modules/penggajian/controllers/penggajian.php */