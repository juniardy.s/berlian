
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
        .ombo{
          width: 400px;
        } 

      </style>    
      <!-- Main content -->
      <section class="content">             
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Daftar Withdraw
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body">            
                  <div class="sukses" ></div>
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <?php
                    $karyawan = $this->db->query("SELECT * from transaksi_withdraw");
                    $hasil_karyawan = $karyawan->result();
                    ?>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori Petugas</th>
                        <th>Komisi</th>
                        <th>Total Diambil</th>
                        <th>Sisa</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php 
                        $sales  = $this->db->get_where('master_sales' , ['status' => 1] )->result();
                        $sopir  = $this->db->get_where('master_sopir' , ['status' => 1] )->result();
                        $no=1;
                        foreach($sales as $field){  

                          $query  = "select sum(nominal_komisi) as komisi from transaksi_komisi_revisi where kode_sales = '$field->kode_sales'  group by kode_sales";
                          $komisi = $this->db->query($query)->result_array();
                          $komisi = count($komisi) > 0 ? $komisi[0]['komisi'] : 0; 

                          /* itung narik */
                          $total_narik  = $this->db->select('sum(nominal_withdraw) as jmlh')
                                                      ->where(['kode_sales'=>$field->kode_sales])
                                                      ->get('transaksi_komisi_withdraw')->row();

                          /* itung sisa */
                          $sisa   = $komisi - $total_narik->jmlh;
            
                      ?>
                      <tr>
                        <td> <?php echo $no++; ?></td>
                        <td> <?php echo $field->kode_sales ?></td>
                        <td> <?php echo $field->nama_sales ?></td>
                        <td> <b><?php echo format_rupiah($komisi) ?> </b></td>
                        <td> <?php echo format_rupiah($total_narik->jmlh) ?> </td>
                        <td> <?php echo format_rupiah($sisa); ?></td>
                        <td align="center"><?php echo get_detail_withdraw($field->kode_sales."/") ;//.$daftar->kategori_petugas); ?></td>
                      </tr>
                        <?php } $no2 = $no; ?>
                        <?php foreach($sopir as $field2){ 

                          $query  = "select sum(nominal_komisi) as komisi from transaksi_komisi_revisi where kode_sales = '$field2->kode_sopir'  group by kode_sales";
                          $komisi = $this->db->query($query)->result_array();
                          $komisi = count($komisi) > 0 ? $komisi[0]['komisi'] : 0; 
                          $total_narik  = $this->db->select('sum(nominal_withdraw) as jmlh')
                                                    ->where(['kode_sales'=>$field2->kode_sopir])
                                                    ->get('transaksi_komisi_withdraw')->row();
                          $sisa   = $komisi - $total_narik->jmlh;

                        ?>
                          <tr>
                        <td> <?php echo $no2++; ?></td>
                        <td> <?php echo $field2->kode_sopir ?></td>
                        <td> <?php echo $field2->nama_sopir ?></td>
                        <td> <b><?php echo format_rupiah($komisi) ?> </b></td>
                        <td> <?php echo format_rupiah($total_narik->jmlh) ?></td>
                        <td> <?php echo format_rupiah($sisa); ?></td>
                        <td align="center"><?php echo get_detail_withdraw($field2->kode_sopir."/") ; //.$daftar->kategori_petugas); ?></td>
                      </tr>
                        <?php } ?>
                    </tbody>
                    
                  </table>


                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
      .btn-back
      {
        position: fixed;
        bottom: 10px;
        left: 10px;
        z-index: 999999999999999;
        vertical-align: middle;
        cursor:pointer
      }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
      $('.btn-back').click(function(){
        $(".tunggu").show();
        window.location = "<?php echo base_url().'penggajian'; ?>";
      });
    </script>
    <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color:grey">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
          </div>
          <div class="modal-body">
            <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data jabatan tersebut ?</span>
            <input id="id-delete" type="hidden">
          </div>
          <div class="modal-footer" style="background-color:#eee">
            <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
            <button onclick="delData()" class="btn red">Ya</button>
          </div>
        </div>
      </div>
    </div>


    <script>

      function actDelete(Object) {
        $('#id-delete').val(Object);
        $('#modal-confirm').modal('show');
      }

      function delData() {
        var id = $('#id-delete').val();
        var url = '<?php echo base_url().'master/karyawan/hapus'; ?>';
        $.ajax({
          type: "POST",
          url: url,
          data: {
            id: id
          },
          beforeSend:function(){
            $(".tunggu").show();  
          },
          success: function(msg) {
            $('#modal-confirm').modal('hide');
            window.location.reload();
          }
        });
        return false;
      }

      $(document).ready(function(){
        $("#tabel_daftar").dataTable();
      })

    </script>