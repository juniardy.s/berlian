
<div class="page-content">
  <div id="box_load">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <style type="text/css">
      .ombo{
        width: 400px;
      } 

      </style>    
      <!-- Main content -->
      <section class="content"> 
        <?php
          $code   = $this->uri->segment(3);

          $tipe   = substr($code, 0, 3);
        
       
            
          

        ?>
               
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-12 connectedSortable">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  Detail Transaksi <?php echo $hasil_gaji->nama_petugas ?>
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>

                </div>
              </div>
              <div class="portlet-body">
                <!------------------------------------------------------------------------------------------------------>


                <div class="box-body">            
                  <div class="sukses" ></div>
                  <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                    <?php
                    $jenis= $this->uri->segment(4);
                    if($jenis=='sopir'){
                      $produksi =$this->db->query("SELECT * from transaksi_penjualan where kode_petugas='$kode_karyawan' or kode_sopir='$kode_karyawan'");
                    }else{
                      //$this->db->where('kategori_penjualan', $this->uri->segment(4));
                      $this->db->where('kode_petugas', $kode_karyawan);
                      $produksi = $this->db->get('transaksi_penjualan');  
                    }
                    
                    $hasil_produksi = $produksi->result();
                    ?>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Transaksi</th>
                        <th>Kode Transaksi</th>
                        <th>Nominal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $nomor = 1;

                      foreach($hasil_produksi as $daftar){
                        // $this->db->where('kategori_penjualan', $this->uri->segment(4));
                        // $this->db->where('kode_penjualan', $daftar->kode_penjualan);
                        // $produksi = $this->db->get('opsi_transaksi_penjualan');
                        // $hasil_produksi = $produksi->row();
                        ?>
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo TanggalIndo($daftar->tanggal_penjualan); ?></td>
                          <td><?php echo $daftar->kode_penjualan; ?></td>
                          <?php 
                          $ketegori=$this->uri->segment(4);
                          $this->db->where('kode_penjualan', $daftar->kode_penjualan);
                          $nominal = $this->db->get('opsi_transaksi_penjualan');
                          $hasil_nominal = $nominal->row();
                          ?>
                          <td><?php if ($ketegori=='sales') {
                            echo @format_rupiah($hasil_nominal->subtotal_komisi_sales);
                          }else{
                            echo @format_rupiah($hasil_nominal->subtotal_komisi_sopir);
                          } ?></td>
                        </tr>
                        <?php $nomor++; 
                      } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Transaksi</th>
                        <th>Kode Transaksi</th>
                        <th>Total</th>
                      </tr>
                    </tfoot>
                  </table>


                </div>

                <!------------------------------------------------------------------------------------------------------>

              </div>
            </div>


            <div class="box box-info">


              <div class="box-body">            



              </section><!-- /.Left col -->      
            </div><!-- /.row (main row) -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </div><!-- /.content-wrapper -->
    <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
    </style>
    <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

    <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'withdraw/'; ?>";
    });
    </script>
    


    <script>

    $(document).ready(function(){
      $("#tabel_daftar").dataTable();
    })

    </script>