<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('karyawan/daftar_karyawan', NULL, TRUE);
        $data['halaman'] = $this->load->view('karyawan/menu', $data, TRUE);
        $this->load->view('karyawan/main', $data);  

       /* $data['aktif']='master';
		$data['konten'] = $this->load->view('jabatan/daftar_jabatan', NULL, TRUE);
		$this->load->view ('main', $data);	*/
		
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }

    public function hak_akses()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('karyawan/daftar_karyawan', NULL, TRUE);
        $this->load->view ('main', $data);	
    }

    public function tambah()
    {
     $data['aktif'] = 'master';
     $data['konten'] = $this->load->view('karyawan/tambah_karyawan', NULL, TRUE);
     $data['halaman'] = $this->load->view('karyawan/menu', $data, TRUE);
     $this->load->view('karyawan/main', $data);  


 }

 public function detail()
 {
    $data['aktif'] = 'master';
    $data['konten'] = $this->load->view('karyawan/detail_karyawan', NULL, TRUE);
    $data['halaman'] = $this->load->view('karyawan/menu', $data, TRUE);
    $this->load->view('karyawan/main', $data);  


}
public function print_daftar()
    {
        
        $this->load->view('karyawan/print_daftar'); 
    }
    
public function get_kode()
{
    $kode_jabatan = $this->input->post('kode_jabatan');
    $query = $this->db->get_where('master_jabatan',array('kode_jabatan' => $kode_jabatan))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}

public function rupiah()
{
    $gaji = $this->input->post('gaji_tetap');
    echo format_rupiah($gaji);
}

    	//------------------------------------------ Proses Simpan----------------- --------------------//


public function simpan_tambah_karyawan()
{
    
    $data = $this->input->post(NULL, TRUE);
    $insert = $this->db->insert("master_karyawan",$data);
        //echo $this->db->last_query();
    echo '<div class="alert alert-success">Sudah tersimpan.</div>'; 
}


    //------------------------------------------ Proses Update----------------- --------------------//

public function simpan_edit_karyawan()
{
    
    $data = $this->input->post(NULL, TRUE);
    $update = $this->db->update("master_karyawan", $data, array('id' => $data['id']));
    echo '<div class="alert alert-success">Sudah Tersimpan.</div>';  
           // echo $this->db->last_query();  
    
}


    //------------------------------------------ Proses Delete----------------- --------------------//
public function hapus(){
    $id = $this->input->post('id');
    $this->db->delete('master_karyawan',array('kode_karyawan'=>$id));
}

}
