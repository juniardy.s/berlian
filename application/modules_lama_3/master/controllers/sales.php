<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sales extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('sales/daftar_sales', NULL, TRUE);
        $data['halaman'] = $this->load->view('sales/menu', $data, TRUE);
        $this->load->view('sales/main', $data);  

       /* $data['aktif']='master';
		$data['konten'] = $this->load->view('sales/daftar_sales', NULL, TRUE);
		$this->load->view ('main', $data);	*/
		
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }

    public function tambah()
    {
     $data['aktif'] = 'master';
     $data['konten'] = $this->load->view('sales/tambah_sales', NULL, TRUE);
     $data['halaman'] = $this->load->view('sales/menu', $data, TRUE);
     $this->load->view('sales/main', $data);  


 }

 public function detail()
 {
    $data['aktif'] = 'master';
    $data['konten'] = $this->load->view('sales/detail_sales', NULL, TRUE);
    $data['halaman'] = $this->load->view('sales/menu', $data, TRUE);
    $this->load->view('sales/main', $data);  


}
public function print_daftar()
    {
        
        $this->load->view('sales/print_daftar'); 
    }
public function get_kode()
{
    $kode_sales = $this->input->post('kode_sales');
    $query = $this->db->get_where('master_sales',array('kode_sales' => $kode_sales))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}

    	//------------------------------------------ Proses Simpan----------------- --------------------//


public function simpan_tambah_sales()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_sales', 'kode_sales', 'required');
    $this->form_validation->set_rules('nama_sales', 'nama_sales', 'required');
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    $this->form_validation->set_rules('telp', 'telp', 'required');
    $this->form_validation->set_rules('status', 'status', 'required'); 

            //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
    } 
            //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $insert = $this->db->insert("master_sales",$data);
            //echo $this->db->last_query();
        echo '<div class="alert alert-success">Sudah tersimpan.</div>'; 

    }
}

    //------------------------------------------ Proses Update----------------- --------------------//

    public function simpan_edit_sales()
    {
          $this->load->library('form_validation');
          $this->form_validation->set_rules('kode_sales', 'kode_sales', 'required');
          $this->form_validation->set_rules('nama_sales', 'nama_sales', 'required');
          $this->form_validation->set_rules('alamat', 'alamat', 'required');
          $this->form_validation->set_rules('telp', 'telp', 'required');
          $this->form_validation->set_rules('status', 'status', 'required'); 
                //jika form validasi berjalan salah maka tampilkan GAGAL
          if ($this->form_validation->run() == FALSE) {
            echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
            } 
                    //jika form validasi berjalan benar maka inputkan data
        else {
            $data = $this->input->post(NULL, TRUE);
            $update = $this->db->update("master_sales", $data, array('id' => $data['id']));
            echo '<div class="alert alert-success">Sudah Tersimpan.</div>';  
            //echo $this->db->last_query();  
        }  
    }



    //------------------------------------------ Proses Delete----------------- --------------------//
public function hapus(){
    $id = $this->input->post('id');
    $this->db->delete('master_sales',array('kode_sales'=>$id));
}

}
