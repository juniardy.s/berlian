<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class expedisi extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('expedisi/daftar_expedisi', NULL, TRUE);
        $data['halaman'] = $this->load->view('expedisi/menu', $data, TRUE);
        $this->load->view('expedisi/main', $data);  

       /* $data['aktif']='master';
		$data['konten'] = $this->load->view('expedisi/daftar_expedisi', NULL, TRUE);
		$this->load->view ('main', $data);	*/
		
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }

    public function tambah()
    {
     $data['aktif'] = 'master';
     $data['konten'] = $this->load->view('expedisi/tambah_expedisi', NULL, TRUE);
     $data['halaman'] = $this->load->view('expedisi/menu', $data, TRUE);
     $this->load->view('expedisi/main', $data);  


 }

 public function detail()
 {
    $data['aktif'] = 'master';
    $data['konten'] = $this->load->view('expedisi/detail_expedisi', NULL, TRUE);
    $data['halaman'] = $this->load->view('expedisi/menu', $data, TRUE);
    $this->load->view('expedisi/main', $data);  


}
public function print_daftar()
    {
        
        $this->load->view('expedisi/print_daftar'); 
    }
public function get_kode()
{
    $kode_expedisi = $this->input->post('kode_expedisi');
    $query = $this->db->get_where('master_expedisi',array('kode_expedisi' => $kode_expedisi))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}

    	//------------------------------------------ Proses Simpan----------------- --------------------//


public function simpan_tambah_expedisi()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_expedisi', 'kode_expedisi', 'required');
    $this->form_validation->set_rules('nama_expedisi', 'nama_expedisi', 'required');
   

            //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
    } 
            //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $insert = $this->db->insert("master_expedisi",$data);
            //echo $this->db->last_query();
        echo '<div class="alert alert-success">Sudah tersimpan.</div>'; 

    }
}

    //------------------------------------------ Proses Update----------------- --------------------//

    public function simpan_edit_expedisi()
    {
          $this->load->library('form_validation');
          $this->form_validation->set_rules('kode_expedisi', 'kode_expedisi', 'required');
          $this->form_validation->set_rules('nama_expedisi', 'nama_expedisi', 'required');
          
                //jika form validasi berjalan salah maka tampilkan GAGAL
          if ($this->form_validation->run() == FALSE) {
            echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
            } 
                    //jika form validasi berjalan benar maka inputkan data
        else {
            $data = $this->input->post(NULL, TRUE);
            $update = $this->db->update("master_expedisi", $data, array('id' => $data['id']));
            echo '<div class="alert alert-success">Sudah Tersimpan.</div>';  
            //echo $this->db->last_query();  
        }  
    }



    //------------------------------------------ Proses Delete----------------- --------------------//
public function hapus(){
    $id = $this->input->post('id');
    $this->db->delete('master_expedisi',array('kode_expedisi'=>$id));
}

}
