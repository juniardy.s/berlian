<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MY_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
	}
	public function index()
	{
	    $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('user/daftar_user', NULL, TRUE);
        $data['halaman'] = $this->load->view('user/menu', $data, TRUE);
        $this->load->view('user/main', $data);  
       
        	
	}	
    //------------------------------------------ View Data Table----------------- --------------------//
    public function menu()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }
	public function user()
	{ 
        $data['aktif'] = 'master';
		$data['konten'] = $this->load->view('user/daftar_user', NULL, TRUE);
		$this->load->view ('main', $data);	
	}
	public function tambah()
	{
	   $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('user/tambah_user', NULL, TRUE);
        $data['halaman'] = $this->load->view('user/menu', $data, TRUE);
        $this->load->view('user/main', $data);  
       
	}
    public function update()
    {
        $data = $this->input->post();
        $data['upass'] = paramEncrypt($data['upass']);

        $insert['nama_user']  = $data['nama_user'];
        $insert['uname']  = $data['uname'];
        $insert['upass']  = $data['upass'];
        $insert['jabatan']  = $data['kode_jabatan'];
        $insert['status']  = $data['status'];
        $this->db->where('kode_user', $data['kode_user']);
        $isi = $this->db->update('master_user', $insert);
        
        if ($isi) {
            $data['response'] = 'sukses';
        }else{
            $data['response'] = 'gagal';
        }

        echo json_encode($data);
    }
    public function simpan()
    {
        $data = $this->input->post();
        
        $insert['kode_user']     = $data['kode_user'];
        $insert['nama_user']    = $data['nama_user'];
        $insert['uname']        = $data['uname'];
        $insert['upass']        = paramEncrypt($data['upass']);
        $insert['jabatan']  = $data['kode_jabatan'];
        $insert['status']        = $data['status'];

        $isi = $this->db->insert('master_user', $insert);
        if ($isi) {
            $data['response'] = 'sukses';
        }else{
            $data['response'] = 'gagal';
        }

        echo json_encode($data);
    }
    public function hapus(){
        $kode_user = $this->input->post('kode_user');
        $this->db->delete('master_user', array('kode_user' => $kode_user ));
    }
    public function detail()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('user/detail_user', NULL, TRUE);
        $data['halaman'] = $this->load->view('user/menu', $data, TRUE);
        $this->load->view('user/main', $data); 
    }
    public function edit()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('user/edit_user', NULL, TRUE);
        $data['halaman'] = $this->load->view('user/menu', $data, TRUE);
        $this->load->view('user/main', $data); 
    }
    	
}