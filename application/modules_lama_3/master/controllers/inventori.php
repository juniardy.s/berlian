<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inventori extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('inventori/daftar_inventori', NULL, TRUE);
        $data['halaman'] = $this->load->view('inventori/menu', $data, TRUE);
        $this->load->view('inventori/main', $data);  

       /* $data['aktif']='master';
		$data['konten'] = $this->load->view('inventori/daftar_inventori', NULL, TRUE);
		$this->load->view ('main', $data);	*/
		
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }

    public function tambah()
    {
     $data['aktif'] = 'master';
     $data['konten'] = $this->load->view('inventori/tambah_inventori', NULL, TRUE);
     $data['halaman'] = $this->load->view('inventori/menu', $data, TRUE);
     $this->load->view('inventori/main', $data);  


 }

 public function detail()
 {
    $data['aktif'] = 'master';
    $data['konten'] = $this->load->view('inventori/detail_inventori', NULL, TRUE);
    $data['halaman'] = $this->load->view('inventori/menu', $data, TRUE);
    $this->load->view('inventori/main', $data);  


}
public function print_daftar()
    {
        
        $this->load->view('inventori/print_daftar'); 
    }
public function get_kode()
{
    $kode_inventori = $this->input->post('kode_inventori');
    $query = $this->db->get_where('master_inventori',array('kode_inventori' => $kode_inventori))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}

    	//------------------------------------------ Proses Simpan----------------- --------------------//


public function simpan_tambah_inventori()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_inventori', 'kode_inventori', 'required');
    $this->form_validation->set_rules('nama_inventori', 'nama_inventori', 'required');
   

            //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
    } 
            //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $insert = $this->db->insert("master_inventori",$data);
            //echo $this->db->last_query();
        echo '<div class="alert alert-success">Sudah tersimpan.</div>'; 

    }
}

    //------------------------------------------ Proses Update----------------- --------------------//

    public function simpan_edit_inventori()
    {
          $this->load->library('form_validation');
          $this->form_validation->set_rules('kode_inventori', 'kode_inventori', 'required');
          $this->form_validation->set_rules('nama_inventori', 'nama_inventori', 'required');
          
                //jika form validasi berjalan salah maka tampilkan GAGAL
          if ($this->form_validation->run() == FALSE) {
            echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
            } 
                    //jika form validasi berjalan benar maka inputkan data
        else {
            $data = $this->input->post(NULL, TRUE);
            $update = $this->db->update("master_inventori", $data, array('id' => $data['id']));
            echo '<div class="alert alert-success">Sudah Tersimpan.</div>';  
            //echo $this->db->last_query();  
        }  
    }



    //------------------------------------------ Proses Delete----------------- --------------------//
public function hapus(){
    $id = $this->input->post('id');
    $this->db->delete('master_inventori',array('kode_inventori'=>$id));
}

}
