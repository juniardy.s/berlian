<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambahan extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif'] = 'master';
        $data['konten'] = $this->load->view('tambahan/daftar_tambahan', NULL, TRUE);
        $data['halaman'] = $this->load->view('tambahan/menu', $data, TRUE);
        $this->load->view('tambahan/main', $data);  

       /* $data['aktif']='master';
		$data['konten'] = $this->load->view('jabatan/daftar_jabatan', NULL, TRUE);
		$this->load->view ('main', $data);	*/
		
	}	

    //------------------------------------------ View Data Table----------------- --------------------//

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $this->load->view ('main', $data);      
    }

    public function hak_akses()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('tambahan/daftar_tambahan', NULL, TRUE);
        $this->load->view ('main', $data);	
    }

    public function tambah()
    {
     $data['aktif'] = 'master';
     $data['konten'] = $this->load->view('tambahan/tambah_tambahan', NULL, TRUE);
     $data['halaman'] = $this->load->view('tambahan/menu', $data, TRUE);
     $this->load->view('tambahan/main', $data);  


 }

 public function detail()
 {
    $data['aktif'] = 'master';
    $data['konten'] = $this->load->view('tambahan/detail_tambahan', NULL, TRUE);
    $data['halaman'] = $this->load->view('tambahan/menu', $data, TRUE);
    $this->load->view('tambahan/main', $data);  


}

public function get_kode()
{
    $kode_jabatan = $this->input->post('kode_jabatan');
    $query = $this->db->get_where('master_jabatan',array('kode_jabatan' => $kode_jabatan))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}

    	//------------------------------------------ Proses Simpan----------------- --------------------//


    public function simpan_tambah_tambahan()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('item', 'item', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('status', 'status', 'required'); 

            //jika form validasi berjalan salah maka tampilkan GAGAL
        if ($this->form_validation->run() == FALSE) {
            echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
        } 
            //jika form validasi berjalan benar maka inputkan data
        else {
            $data = $this->input->post(NULL, TRUE);
            $insert = $this->db->insert("master_tambahan",$data);
            //echo $this->db->last_query();
            echo '<div class="alert alert-success">Sudah Tersimpan.</div>'; 
                
        }
    }


    //------------------------------------------ Proses Update----------------- --------------------//

    public function simpan_edit_tambahan()
    {
         $this->load->library('form_validation');
        $this->form_validation->set_rules('item', 'item', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('status', 'status', 'required'); 
            //jika form validasi berjalan salah maka tampilkan GAGAL
        if ($this->form_validation->run() == FALSE) {
            echo '<div class="alert alert-warning">Gagal Tersimpan.</div>';
        } 
            //jika form validasi berjalan benar maka inputkan data
        else {
            $data = $this->input->post(NULL, TRUE);
            $update = $this->db->update("master_tambahan", $data, array('id' => $data['id']));
            echo '<div class="alert alert-success">Sudah Tersimpan.</div>';  
           // echo $this->db->last_query();  
        }  
    }


    //------------------------------------------ Proses Delete----------------- --------------------//
    public function hapus(){
        $id = $this->input->post('id');
        $this->db->delete('master_tambahan',array('id'=>$id));
    }

}
