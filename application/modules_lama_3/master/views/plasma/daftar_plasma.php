

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Daftar Plasma
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>

          <div class="row">
            <div class="col-md-12">
              <a onclick="print_daftar()" style="margin-top: 5px;" class="btn btn-lg blue "><i class="fa fa-print"></i> Print</a>

            </div>
          </div>
          <br>
          <div class="box-body">            
            <div class="sukses" ></div>
            <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
              <?php
              $this->db->limit(50);
              $this->db->order_by('nama_plasma','ASC');
              $plasma = $this->db->get('master_plasma');
              $hasil_plasma = $plasma->result();
              ?>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Plasma</th>
                  <th>Nama Plasma</th>
                  <th>Alamat</th>
                  <th>Telepon</th>
                  <th>Keterangan</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="daftar_plasma">
                <?php
                $nomor = 1;

                foreach($hasil_plasma as $daftar){ ?> 
                <tr>
                  <td><?php echo $nomor; ?></td>
                  <td><?php echo $daftar->kode_plasma; ?></td>
                  <td><?php echo $daftar->nama_plasma; ?></td>
                  <td><?php echo $daftar->alamat; ?></td>
                  <td><?php echo $daftar->telp; ?></td>
                  <td><?php echo $daftar->keterangan; ?></td>
                  <td><?php echo cek_status($daftar->status); ?></td>
                  <td align="center"><?php echo get_detail_edit_delete($daftar->kode_plasma); ?></td>
                </tr>
                <?php $nomor++; } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kode Plasma</th>
                  <th>Nama Plasma</th>
                  <th>Alamat</th>
                  <th>Telepon</th>
                  <th>Keterangan</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
            <?php 
            $get_jumlah = $this->db->get('master_plasma');
            $jumlah = $get_jumlah->num_rows();
            $jumlah = floor($jumlah/50);
            ?>
            <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
            <input type="hidden" class="form-control pagenum" value="0">

          </div>

          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>


      <div class="box box-info">


        <div class="box-body">            



        </section><!-- /.Left col -->      
      </div><!-- /.row (main row) -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'master/daftar/'; ?>";
    });
  </script>
  <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data plasma tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="delData()" class="btn red">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(window).scroll(function(){
      if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
        if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
          var pagenum = parseInt($(".pagenum").val()) + 1;
          $(".pagenum").val(pagenum);
          load_table(pagenum);
        }
      }
    });

    function load_table(page){
      var nama_plasma = $("#nama_plasma").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() . 'master/plasma/get_table' ?>",
        data: ({nama_plasma:nama_plasma, page:$(".pagenum").val()}),
        beforeSend: function(){
          $(".tunggu").show();  
        },
        success: function(msg)
        {
          $(".tunggu").hide();
          $("#daftar_plasma").append(msg);

        }
      });
    }
  </script>


  <script>
    function actDelete(Object) {
      $('#id-delete').val(Object);
      $('#modal-confirm').modal('show');
    }

    function delData() {
      var id = $('#id-delete').val();
      var url = '<?php echo base_url().'master/plasma/hapus'; ?>';
      $.ajax({
        type: "POST",
        url: url,
        data: {
          id: id
        },
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success: function(msg) {
          $('#modal-confirm').modal('hide');
          window.location.reload();
        }
      });
      return false;
    }
    function print_daftar(){

      window.open('<?php echo base_url().'master/plasma/print_daftar'; ?>/');
    }
    $(document).ready(function(){
      $("#tabel_daftar").dataTable({
        "paging":   false,
        "ordering": true,
        "searching": false
      });
    })

  </script>