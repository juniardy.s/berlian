<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Form Bahan Jadi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>

        <?php
        $param = $this->uri->segment(4);
        if(!empty($param)){
          $bahan_jadi = $this->db->get_where('master_bahan_jadi',array('id'=>$param));
          $hasil_bahan_jadi = $bahan_jadi->row();
        }    

        ?>
        <div class="box-body">                   
          <div class="sukses" ></div>
          <form id="data_form" method="post">
            <div class="box-body">            
              <div class="row">
                <div class="form-group  col-xs-6">
                  <label><b>Kode Bahan Jadi</label>

                  <div class="input-group">
                    <?php
                    $this->db->select('kode_bahan_jadi');
                    $bb = $this->db->get('master_setting');
                    $hasil_bb = $bb->row();
                    ?>
                    <span class="input-group-addon"><?php if(empty($param)){ echo $hasil_bb->kode_bahan_jadi.'_'; } ?></span>
                    <input type="hidden" id="kode_setting" value="<?php echo @$hasil_bb->kode_bahan_jadi ?>"></input>
                    <input required name="kode_bahan_jadi" value="<?php echo @$hasil_bahan_jadi->kode_bahan_jadi ?>"  <?php if(!empty($param)){ echo "readonly='true'"; } ?> type="text" class="form-control" id="kode_bahan_jadi" />
                  </div>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Nama Bahan Jadi</label>
                  <input required value="<?php echo @$hasil_bahan_jadi->nama_bahan_jadi; ?>" type="text" class="form-control" name="nama_bahan_jadi" />
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Unit</label>
                  <?php
                  $kode_default = $this->db->get('setting_gudang');
                  $hasil_unit =$kode_default->row();
                  $kode_unit=$hasil_unit->kode_unit;

                  $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
                  $hasil_unit = $unit->row();
                  ?>
                  <input required value="<?php echo @$hasil_unit->nama_unit; ?>" type="text" class="form-control" readonly />
                  <input value="<?php echo @$hasil_unit->kode_unit; ?>" type="hidden" class="form-control" name="kode_unit" />
                </div>

                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Kategori</label>
                  <?php

                  $kode_unit = @$hasil_unit->kode_unit;
                  $get_rak = $this->db->get_where('master_rak',array('status'=>'1'));
                  $hasil_get_rak = $get_rak->result();

                  ?>
                  <select required name="kode_rak" id="kode_rak" class="form-control">
                    <option selected="true" value="">--Pilih Kategori--</option>
                    <?php 
                    foreach($hasil_get_rak as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_jadi->kode_rak==$daftar->kode_rak){ echo "selected"; } ?> value="<?php echo $daftar->kode_rak; ?>"><?php echo $daftar->nama_rak; ?></option>
                      <?php  
                    } ?>
                  </select>
                </div>

                <div class="form-group  col-xs-6">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Satuan Stok</label>
                  <select required class="form-control stok select2" name="id_satuan_stok">
                    <option selected="true" value="">--Pilih satuan stok--</option>
                    <?php 
                    foreach($hasil_dft_satuan as $daftar){    
                      ?>
                      <option <?php if(@$hasil_bahan_jadi->id_satuan_stok==$daftar->kode){ echo "selected"; } ?> value="<?php echo $daftar->kode; ?>"><?php echo $daftar->nama; ?></option>
                      <?php 
                    } ?>
                  </select> 
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Stok Minimal</label>
                  <input required type="text" class="form-control" name="stok_minimal" value="<?php echo @$hasil_bahan_jadi->stok_minimal ?>" />
                </div>
                <div class="form-group  col-xs-6">
                  <?php
                  $dft_satuan = $this->db->get_where('master_satuan');
                  $hasil_dft_satuan = $dft_satuan->result();

                  ?>
                  <label class="gedhi"><b>Jenis Produksi</label>
                  <select required class="form-control" name="jenis_produksi">
                    <option selected="true" value="">--Pilih jenis--</option>
                    <option <?php if(@$hasil_bahan_jadi->jenis_bahan=="sendiri"){ echo "'"; } ?> selected='true' value="sendiri">Factory</option>
                    <!-- <option <?php #if(@$hasil_bahan_jadi->jenis_bahan=="plasma"){ echo "selected='true'"; } ?> value="plasma">Plasma</option> -->
                  </select> 
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>HPP</label>
                  <input required type="text" class="form-control" name="hpp" id="hpp" value="<?php echo @$hasil_bahan_jadi->hpp ?>" />
                  <h3><div id="nilai_hpp"></div></h3>
                </div>
                <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Harga Jual</label>
                  <input required type="text" class="form-control" name="harga_jual" id="harga_jual" value="<?php echo @$hasil_bahan_jadi->harga_jual ?>" />
                  <h3><div id="nilai_harga_jual"></div></h3>
                </div>
                <div class="form-group  col-xs-6">
                 <label class="gedhi"><b>Tarif Borongan</label>
                 <input required type="text" class="form-control" name="tarif_borongan" id="tarif_borongan" value="<?php echo @$hasil_bahan_jadi->tarif_borongan ?>" />
                 <h3><div id="nilai_tarif_borongan"></div></h3>
               </div>
                <!-- <div class="form-group  col-xs-6">
                  <label class="gedhi"><b>Isi Per Lusin</label>
                  <input required type="text" class="form-control" name="isi_per_lusin" id="isi_per_lusin" value="<?php echo @$hasil_bahan_jadi->isi_per_lusin ?>" />
                  
                </div> -->
                <div class="form-group  col-xs-12">
                  <table width="100%" border="1"> 
                    <tr>
                      <td></td>
                    </tr>
                  </table> 
                  <div class="portlet box blue">
                    <div class="portlet-title">
                      <div class="caption">
                        Komposisi Bahan
                      </div>
                    </div>
                  </div>

                </div>


                <div class="form-group  col-xs-12">
                  <div class="sukses_bahan" ></div>
                  <div class="row">
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Jenis Bahan</label>
                      <select name="jenis_bahan" id="jenis_bahan" class="form-control">
                        <option selected="true" value="">--Pilih Jenis Bahan--</option>
                        <option value="bahan baku">Bahan Baku</option>
                        <option value="bahan setengah jadi">Bahan Setengah Jadi</option>
                      </select>
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Bahan</label>
                      <select name="bahan" id="bahan" class="form-control">
                        <option selected="true" value="">--Pilih Bahan--</option>
                      </select>
                    </div>

                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Satuan</label>
                      <input readonly="" type="text" class="form-control" name="nama_satuan" id="nama_satuan" value=""/>
                      <input type="hidden" class="form-control" name="id_satuan" id="id_satuan" value=""/>
                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>HPP</label>
                      <input readonly="" type="text" class="form-control" id="hpp_bb" value=""/>

                    </div>
                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Persentase hpp</label>
                      <div class="input-group">

                        <input type="text" class="form-control" onkeyup="get_presentase_hpp()" value="0" id="persentase_hpp" />
                        <span class="input-group-addon">%</span>
                      </div>

                    </div>

                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Jumlah</label>
                      <input type="text" class="form-control" name="jumlah" id="jumlah" />
                    </div>



                  </div>
                </div>
                <div class="form-group col-md-12">
                  <div class="row">

                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Total hpp</label>
                      <input type="text" class="form-control" id="total_hpp1" />

                    </div>

                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Persentase Harga</label>
                      <div class="input-group">

                        <input type="text" class="form-control" value="" id="rumus" />
                        <span class="input-group-addon">%</span>
                      </div>

                    </div>

                    <div class="form-group hidden  col-xs-2">
                      <label class="gedhi"><b>25%</label>
                      <input readonly="" type="text" class="form-control" id="persen_hpp" value=""/>

                    </div> 

                    <div class="form-group  col-xs-2">
                      <label class="gedhi"><b>Harga</label>
                      <input type="text" class="form-control" id="harga" value=""/>

                    </div>

                    <div class="form-group hidden  col-xs-2">
                      <label class="gedhi"><b>Jumlah hpp</label>
                      <input type="text" class="form-control" value="0" id="jumlah_hpp" />

                    </div>
                    <div class="form-group  col-xs-12">
                      <button type="button" id="add-item" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</button>
                    </div>
                  </div>
                </div>
                <div class="form-group  col-xs-12">
                  <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Bahan</th>
                        <th>Jumlah</th>
                        <th>HPP</th>
                        <th>Rumus</th>
                        <th>Harga</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody id="opsi_bahan_jadi">
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="form-group  col-xs-12">
                 <!--  <label class="gedhi"><b>Tarif Borongan</label>
                  <input required type="text" class="form-control" name="tarif_borongan" id="tarif_borongan" value="<?php echo @$hasil_bahan_jadi->tarif_borongan ?>" />
                  <h3><div id="nilai_tarif_borongan"></div></h3>
                </div>
                <div class="form-group  col-xs-12">
                <table width="100%" border="1"> 
                  <tr>
                    <td></td>
                  </tr>
                </table> -->
                <div class="portlet box blue">
                  <div class="portlet-title">
                    <div class="caption">
                      Harga Bahan Jadi 
                    </div>
                  </div>
                </div>

              </div>
              <div class="form-group  col-xs-12">
                <div class="row">
                  <div class="form-group  col-xs-3">
                    <label class="gedhi"><b>Kategori</label>
                    <select  id="kategori_member" class="form-control">
                      <option selected="true" value="">--Pilih Kategori--</option>
                      <option value="agen">Agen</option>
                      <option value="kanvas">Kanvas</option>
                      <option value="supermarket">Supermarket</option>
                    </select>
                  </div>
                  <div class="form-group  col-xs-3">
                    <label class="gedhi"><b>Qty Min </label>
                    <input type="number" class="form-control"  id="qty_min" value=""/>

                  </div>
                  <div class="form-group  col-xs-3">
                    <label class="gedhi"><b>Qty Max </label>
                    <input type="number" class="form-control"  id="qty_max" value=""/>

                  </div>

                  <div class="form-group  col-xs-3">
                    <label class="gedhi"><b>Harga</label>
                    <input type="number" class="form-control"  id="harga_bahan_jadi" value=""/>

                  </div>
                  <div class="form-group  col-xs-12">
                    <button type="button" id="add_price" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</button>
                  </div>
                  <div class="form-group  col-xs-12">
                    <table class="table table-striped table-hover table-bordered" style="font-size:1.5em;">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kategori</th>
                          <th>Qty Min </th>
                          <th>Qty Max </th>
                          <th>Harga</th>
                          <th width="10%">Action</th>
                        </tr>
                      </thead>
                      <tbody id="opsi_harga_bj">
                      </tbody>
                    </table>
                  </div>
                 <!--

                 
                  <div class="form-group  col-xs-2">
                    <label class="gedhi"><b>Persentase hpp</label>
                    <div class="input-group">

                      <input type="text" class="form-control" value="0" id="persentase_hpp" />
                      <span class="input-group-addon">%</span>
                    </div>

                  </div>

                  <div class="form-group  col-xs-2">
                    <label class="gedhi"><b>Jumlah</label>
                    <input type="text" class="form-control" name="jumlah" id="jumlah" />
                  </div> -->



                </div>
              </div>

              <div class="box-footer">
                <!-- <center><button type="submit" class="btn btn-primary btn-lg" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center> -->
                <center><button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data bahan jadi tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="actDelete()" class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<div id="price_harga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus data harga bahan jadi tersebut ?</span>
        <input id="id-delete_h" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="actDelete_harga()" class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------------------------------------------------------------>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  function get_temp(){
    kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
    $("#opsi_bahan_jadi").load('<?php echo base_url().'master/bahan_jadi/table_temp/'; ?>'+kode_bahan_jadi);
  }
  function get_opsi(){
    kode_bahan_jadi = $("#kode_bahan_jadi").val();
    $("#opsi_bahan_jadi").load('<?php echo base_url().'master/bahan_jadi/table_opsi/'; ?>'+kode_bahan_jadi);
  }

  function get_temp_harga(){
    kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
    $("#opsi_harga_bj").load('<?php echo base_url().'master/bahan_jadi/table_temp_harga/'; ?>'+kode_bahan_jadi);
  }
  function get_opsi_harga(){
    kode_bahan_jadi = $("#kode_bahan_jadi").val();
    $("#opsi_harga_bj").load('<?php echo base_url().'master/bahan_jadi/table_opsi_harga/'; ?>'+kode_bahan_jadi);
  }
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'master/bahan_jadi/'; ?>";
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){

    $(".select2").select2();
    <?php 
    if(!empty($param)){ ?>
      get_opsi();
      get_opsi_harga();
      <?php 
    }else{ ?>
      get_temp();
      get_temp_harga();
      <?php } ?>
    });
  $(function(){
    $('#kode_bahan_jadi').on('change',function(){
      var kode_input = $('#kode_bahan_jadi').val();
      var kode_setting = $('#kode_setting').val();
      var kode_bahan_jadi = kode_setting + "_" + kode_input ;
      var url = "<?php echo base_url() . 'master/bahan_jadi/get_kode' ?>";
      $.ajax({
        type: 'POST',
        url: url,
        data: {kode_bahan_jadi:kode_bahan_jadi},
        success: function(msg){
          if(msg == 1){
            $(".sukses").html('<div class="alert alert-warning">Kode Telah Dipakai</div>');
            setTimeout(function(){
              $('.sukses').html('');
            },1700);              
            $('#kode_bahan_jadi').val('');

          }
          else{

          }
        }
      });
    });

    $("#jenis_bahan").change(function(){
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'master/bahan_jadi/get_bahan' ?>",
        data: {jenis_bahan:$(this).val()},
        success: function(data){
          $("#bahan").html(data);
        },
        error : function(data) {  
          alert('Sorry');
        }  
      });
    });

    $("#bahan").change(function(){
      jenis_bahan = $("#jenis_bahan").val();
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'master/bahan_jadi/get_satuan' ?>",
        dataType: 'json',
        data: {kode_bahan_baku:$(this).val(), jenis_bahan:jenis_bahan},
        success: function(data){
          $("#id_satuan").val(data.id_satuan_stok);
          $("#nama_satuan").val(data.satuan_stok);
          $("#hpp_bb").val(data.hpp);
          get_presentase_hpp();
        },
        error : function(data) {  
          alert('Sorry');
        }  
      });
    });
    
   //  $("#persentase_hpp").keyup(function(){
   //   var hpp = $("#hpp_bb").val();
   //   var persen = $("#persentase_hpp").val();
   //   var jumlah_hpp = $("#jumlah_hpp").val();
   //   var hasil = hpp*persen/100;
   //   var bulat = hasil.toFixed(2);
   //   var desimal = parseFloat(bulat);
   //   var hasil_jumlah = desimal + parseFloat(hpp);
   //   var hasil_bulat = hasil_jumlah.toFixed(2);
   //   $("#persen_hpp").val(desimal); 
   //   $("#jumlah_hpp").val(hasil_bulat);
   // });

   $("#jumlah").keyup(function(){
     var jumlah_hpp = $("#jumlah_hpp").val();
     var jumlah = $("#jumlah").val();
     var hasil = jumlah_hpp * jumlah;
     var hasil_bulat = hasil.toFixed(2);
     $("#total_hpp1").val(hasil_bulat);
   });

   $("#rumus").keyup(function(){
     var hpp = $("#hpp_bb").val();
     var rumus = $("#rumus").val();
     var total_hpp = $("#total_hpp1").val();
     var harga = (total_hpp*rumus/100);
     var harga_akhir = parseFloat(harga) + parseFloat(total_hpp); 
     var hasil_bulat = harga_akhir.toFixed(2);
     if(parseInt(rumus) < 0  || parseInt(rumus) >100){
      alert("Jumlah Persentase Harga Salah");
      $("#harga").val('');  
      $("#rumus").val('');
    }else{
      $("#harga").val(hasil_bulat);     
    }
    
  });

   $("#add_price").click(function(){
    <?php 
    if(!empty($param)){ ?>
      kode_bahan_jadi = $("#kode_bahan_jadi").val();
      <?php 
    } else { ?>
      kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
      <?php
    } ?>
    var kategori_member = $("#kategori_member").val();
    var qty_min = $("#qty_min").val();
    var qty_max = $("#qty_max").val();
    var harga = $("#harga_bahan_jadi").val();
    if(kategori_member=="" || qty_min=="" || harga==""){
      alert("lengkapi form harga bahan jadi!");
    }else{


      $.ajax({
        type: 'POST',
        <?php 
        if(!empty($param)){ ?>
          url: "<?php echo base_url() . 'master/bahan_jadi/tambah_opsi_harga' ?>",
          <?php 
        } else { ?>
          url: "<?php echo base_url() . 'master/bahan_jadi/tambah_temp_harga' ?>",
          <?php
        } ?>
        data: {kode_bahan_jadi:kode_bahan_jadi, kategori_member:kategori_member, qty_min:qty_min, qty_max:qty_max, harga:harga},
        success: function(data){
          $("#kategori_member").val('');
          $("#qty_min").val('');
          $("#qty_max").val('');
          $("#harga_bahan_jadi").val('');
          <?php 
          if(!empty($param)){ ?>
            get_opsi_harga();
            <?php 
          } else { ?>
            get_temp_harga();
            <?php
          } ?>
        },
        error : function(data) {  
          alert('Sorry');
        }  
      });
    }
  });

   $("#add-item").click(function(){
    <?php 
    if(!empty($param)){ ?>
      kode_bahan_jadi = $("#kode_bahan_jadi").val();
      <?php 
    } else { ?>
      kode_bahan_jadi = $("#kode_setting").val()+'_'+$("#kode_bahan_jadi").val();
      <?php
    } ?>
    jenis_bahan = $("#jenis_bahan").val();
    bahan_baku = $("#bahan").val();
    jumlah = $("#jumlah").val();
    id_satuan = $("#id_satuan").val();
    satuan = $("#nama_satuan").val();
    var hpp = $("#hpp_bb").val();
    var rumus = $("#rumus").val();
    var harga = $("#harga").val();

    if($("#jenis_bahan").val()==""||$("#bahan").val()==""||$("#hpp_bb").val()==""||$("#jumlah").val()==""||$("#rumus").val()==""||$("#persentase_hpp").val()==""||$("#total_hpp1").val()==""){
      alert("lengkapi form Komposisi!!!");
    }else{

      $.ajax({
        type: 'POST',
        <?php 
        if(!empty($param)){ ?>
          url: "<?php echo base_url() . 'master/bahan_jadi/tambah_opsi' ?>",
          <?php 
        } else { ?>
          url: "<?php echo base_url() . 'master/bahan_jadi/tambah_temp' ?>",
          <?php
        } ?>
        data: {kode_bahan_jadi:kode_bahan_jadi, jenis_bahan:jenis_bahan, bahan_baku:bahan_baku, 
          jumlah:jumlah, id_satuan:id_satuan, satuan:satuan,hpp:hpp,rumus:rumus,harga:harga},
          success: function(data){
            if(data==1){
             $(".sukses_bahan").html('<div class="alert alert-danger">Bahan Sudah  Ada!!!</div>');
           }else{
            $(".sukses_bahan").html('');
          }
          $("#bahan").val('');
          $("#jumlah").val('');
          $("#id_satuan").val('');
          $("#nama_satuan").val('');
          $("#hpp_bb").val('');
          $("#harga").val('');
          $("#rumus").val('');
          $("#total_hpp1").val('');
          $("#persentase_hpp").val('');
          <?php 
          if(!empty($param)){ ?>
            get_opsi();
            <?php 
          } else { ?>
            get_temp();
            <?php
          } ?>
          get_hpp();
        },
        error : function(data) {  
          alert('Sorry');
        }  
      });
    }
  });

   $("#hpp").keyup(function(){
    var temp_data = "<?php echo base_url().'master/bahan_jadi/get_rupiah/'?>";
    var hpp = $('#hpp').val() ;
    $.ajax({
      type: "POST",
      url: temp_data,
      data: {hpp:hpp},
      success: function(hasil) {              
        $("#nilai_hpp").html(hasil);
      }
    });

  });
   $("#harga_jual").keyup(function(){
    var temp_data = "<?php echo base_url().'master/bahan_jadi/get_rupiah/'?>";
    var hpp = $('#harga_jual').val() ;
    $.ajax({
      type: "POST",
      url: temp_data,
      data: {hpp:hpp},
      success: function(hasil) {              
        $("#nilai_harga_jual").html(hasil);
      }
    });

  });
   $("#tarif_borongan").keyup(function(){
    var temp_data = "<?php echo base_url().'master/bahan_jadi/get_rupiah/'?>";
    var hpp = $('#tarif_borongan').val() ;
    $.ajax({
      type: "POST",
      url: temp_data,
      data: {hpp:hpp},
      success: function(hasil) {              
        $("#nilai_tarif_borongan").html(hasil);
      }
    });

  });

   $("#data_form").submit( function() {
    <?php if(!empty($param)){ ?>
      var url = "<?php echo base_url(). 'master/bahan_jadi/simpan_edit'; ?>";  
      <?php 
    }else{ ?>
      var url = "<?php echo base_url(). 'master/bahan_jadi/simpan'; ?>";
      <?php 
    } ?>
    $.ajax( {
      type:"POST", 
      url : url,  
      cache :false,  
      dataType : 'json',
      data :$(this).serialize(),

      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
        if(data.hasil=="opsi"){
          $(".sukses").html('<div class="alert alert-danger">Opsi Tidak Ada!!!</div>');
          setTimeout(function(){$('.sukses').html('');},1000);
          $(".tunggu").hide();
        }
        else{
          $(".sukses").html('<div class="alert alert-success">Data Berhasil Disimpan</div>');
          setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'master/bahan_jadi/' ?>";},1000);  
        }
        $(".loading").hide();   

      },  
      error : function(data) {  
        $(".sukses").html('<div class="alert alert-success">Data Gagal Disimpan</div>');
      }  
    });
    return false;                    
  }); 
 })
</script>
<script type="text/javascript">
  function get_presentase_hpp(){
    var hpp = $("#hpp_bb").val();
    var persen = $("#persentase_hpp").val();
    var jumlah_hpp = $("#jumlah_hpp").val();
    var hasil = hpp*persen/100;
    var bulat = hasil.toFixed(2);
    var desimal = parseFloat(bulat);
    var hasil_jumlah = desimal + parseFloat(hpp);
    var hasil_bulat = hasil_jumlah.toFixed(2);
    if(parseInt(persen) < 0  || parseInt(persen) >100){ 
      alert("Jumlah Persentase Hpp Salah");
      $("#persentase_hpp").val("");
      $("#persen_hpp").val(""); 
      $("#jumlah_hpp").val("");
    }else{
      $("#persen_hpp").val(desimal); 
      $("#jumlah_hpp").val(hasil_bulat);
    }

  }
  function get_hpp(){
    var url = "<?php echo base_url().'master/bahan_jadi/get_hpp/'?>";
    <?php 
    if(!empty($param)){ ?>
     var table = "table_opsi";
     var kode_bahan_jadi = $("#kode_bahan_jadi").val();
     <?php 
   } else { ?>
     var table = "table_temp";
     var kode_bahan_jadi = $("#kode_setting").val()+"_"+$("#kode_bahan_jadi").val();
     <?php
   } ?>
   $.ajax({
    type: "POST",
    url: url,
    data: {kode_bahan_jadi:kode_bahan_jadi,table:table},
    success: function(hasil) {              
      $("#hpp").val(hasil);
    }
  });

 }

</script>