<!DOCTYPE html>
<html>
<head>
	<title>Print Bahan Baku</title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <table  >
    <tr>
      <td>
        <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
      </td>
      
    </tr>
    
  </table>
  <hr>
  <?php
  $kode_default = $this->db->get('setting_gudang');
  $hasil_unit =$kode_default->row();
  $param=$hasil_unit->kode_unit;

  $data = $this->uri->segment(4);
  
  $data=str_replace('%20', ' ', $data);
  
  if(@$data){
    $produk = $data;
    $this->db->like('nama_bahan_baku',$produk,'both');
  }
  $this->db->order_by('nama_bahan_baku','asc');
  $bahan_baku = $this->db->get_where('master_bahan_baku',array('kode_unit' => $param,'status'=>'sendiri'));
  $hasil_bahan_baku = $bahan_baku->result();
  ?>
  <table class="" width="100%" border="0" style="border-collapse: collapse;">

    <tr>
      
      <th  colspan="3" rowspan="" headers="" align="center" style="font-size:20px">Data Bahan Baku</th>
    </tr>
  </table><br>
  <table class="" width="100%" border="1" style="border-collapse: collapse;">

   <thead>
    <tr width="100%">
      <th>No</th>
      <th>Kode Bahan</th>
      <th>Nama Bahan Baku</th>
      <th>Unit</th>
      <th>Kategori</th>
      <th>Satuan Pembelian</th>
      <th>Satuan Penggunaan</th>
      <th style="width:50px">Isi Dalam 1 <br>(Satuan Pembelian)</th>
      <th>Stok Minimal</th>
      <th>HPP</th>

    </tr>
  </thead>
  <tbody>
    <?php
    $nomor=1;
    foreach($hasil_bahan_baku as $daftar){
      ?>
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo $daftar->kode_bahan_baku; ?></td>
        <td><?php echo $daftar->nama_bahan_baku; ?></td>

        <td><?php echo $daftar->nama_unit; ?></td>
        <td><?php echo $daftar->nama_rak; ?></td>
        <td><?php echo $daftar->satuan_pembelian; ?></td>
        <td><?php echo $daftar->satuan_stok; ?></td>
        <td><?php echo $daftar->jumlah_dalam_satuan_pembelian; ?></td>
        <td><?php echo $daftar->stok_minimal; ?></td>
        <td><?php echo format_rupiah($daftar->hpp); ?></td>

      </tr>
      <?php $nomor++; } ?>
    </tbody>

  </table>



  </html>