<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
         Validasi Opname 
       </div>
       <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>
    </div>
    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/opname_s_jadi/daftar_opname'; ?>"><i class="fa fa-list"></i> Opname  </a>
      <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/opname_s_jadi/daftar_validasi_nominal'; ?>"><i class="fa fa-check"></i> Validasi  </a>
        <!-- <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_opname_view'; ?>"><i class="fa fa-list"></i> Opname View </a>
          <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'opname/daftar_validasi_view'; ?>"><i class="fa fa-check"></i> Validasi View </a> -->
          <div class="box-body">            
            <div class="sukses" ></div>
            <form id="data_form" method="post">
              <div class="box-body">            
                <div class="row">
                  <div class="col-md-3 filter_tanggal" >
                    <div>
                      <label>Tanggal Awal</label>
                      <input type="date" class="form-control" name="tanggal_awal" id="tanggal_awal">
                    </div>
                  </div>
                  <div class="col-md-3 filter_tanggal" >
                    <div>
                      <label>Tanggal Akhir</label>
                      <input type="date" class="form-control" name="tanggal_akhir" id="tanggal_akhir">
                    </div>
                  </div>
                  <div class="col-md-1">
                    <div style="height: 25px"></div>
                    <a class="btn btn-success" onclick="cari_penjualan()"><i class="fa fa-search"></i> Cari</a>
                  </div>

                </div> 

              </div>
            </div><br>
          </form>

          <form id="data_opname">
            <div id="cari_transaksi">

              <table id="datatable" style="font-size:1.5em;" class="table table-striped table-bordered">  
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Opname</th>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th>Stok Awal</th>
                    <th>Stok Akhir</th>
                    <th>Selisih</th>
                    <th>Status</th>

                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="daftar_list_stock">

                  <?php

                  $this->db->select('*, opsi_transaksi_opname.validasi as validasi_opsi,opsi_transaksi_opname.id as id_opsi');
                  $this->db->from('opsi_transaksi_opname');
                  $this->db->where('MONTH(transaksi_opname.tanggal_opname)', date('m'));
                  $this->db->where('YEAR(transaksi_opname.tanggal_opname)', date('Y'));
                  $this->db->where('opsi_transaksi_opname.jenis_bahan', 'bahan setengah jadi');
                  $this->db->order_by('opsi_transaksi_opname.id','DESC');
                  $this->db->join('transaksi_opname', 'transaksi_opname.kode_opname = opsi_transaksi_opname.kode_opname', 'left');
                  $get_stok = $this->db->get();
                  $hasil_stok = $get_stok->result_array();
                  $no=1;
                  foreach ($hasil_stok as $item) {

                    ?>   
                    <tr <?php  if($item['validasi_opsi'] =='belum divalidasi') { ?>  style="background: #f1c6c6;"  <?php } ?> >
                      <td><?php echo $no++;?></td>
                      <td><?php echo $item['kode_opname'];?></td>
                      <td><?php echo $item['kode_bahan'];?></td>
                      <td><?php echo $item['nama_bahan'];?></td>
                      <td><?php echo $item['stok_awal'];?></td>
                      <td><?php echo $item['stok_akhir'];?></td>
                      <td><?php echo $item['selisih'];?></td>
                      <td><?php echo $item['status'];?></td>


                      <td align="center">
                        <?php if($item['validasi_opsi']=="belum divalidasi" or empty($item['validasi_opsi'])){ ?>
                        <a onclick="validasi('<?php echo $item['id_opsi']?>','<?php echo $item['status']?>')" data-toggle="tooltip" title="Validasi" class="btn btn-xs btn-primary validasi"><i class="icon-check"></i> Validasi</a>
                        <?php } ?>

                        <!-- <a  data-toggle="tooltip" onclick="print('<?php echo $item['kode_opname'];?>')" key="<?php echo $item['kode_opname'];?>" title="Validasi" class="btn btn-xs blue"><i class="fa fa-print"></i>  Print</a> -->
                      </td>
                    </tr>

                    <?php } ?>

                  </tbody>
                  
                </table>
                <?php 

                $this->db->select('*, opsi_transaksi_opname.validasi as validasi_opsi,opsi_transaksi_opname.id as id_opsi');
                $this->db->from('opsi_transaksi_opname');
                $this->db->where('MONTH(transaksi_opname.tanggal_opname)', date('m'));
                $this->db->where('YEAR(transaksi_opname.tanggal_opname)', date('Y'));
                $this->db->where('opsi_transaksi_opname.jenis_bahan', 'bahan setengah jadi');
                $this->db->order_by('opsi_transaksi_opname.id','DESC');
                $this->db->join('transaksi_opname', 'transaksi_opname.kode_opname = opsi_transaksi_opname.kode_opname', 'left');
                $get_jumlah = $this->db->get();
                $jumlah = $get_jumlah->num_rows();
                $jumlah = floor($jumlah/50);
                ?>
                <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
                <input type="hidden" class="form-control pagenum" value="0">
              </div>
            </form>
            <div class="row">
              <!-- <a style="padding:13px; margin-bottom:10px; margin-right:15px;" id="opname" class="btn btn-app green pull-right" ><i class="fa fa-edit"></i> Opname </a> -->
            </div>  
          </div>

          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>
    </div><!-- /.col -->
  </div>
</div>    
</div>  

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data menu tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>

<div id="modal-kurang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Validasi Data Kurang</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menyelesaikan data tersebut ?</span>
        <input id="id-kurang" type="hidden">
        <!--  <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Nominal" /> -->
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button key="kurang_tanpa_nominal" class="btn green proses" >dihibahkan</button>
        <button  class="btn red tindak_lanjuti">ditindak lanjuti</button>
      </div>
    </div>
  </div>
</div>
<div id="modal-nominal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Validasi Data Kurang</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Masukan Nominal Untuk Menindak Lanjuti ?</span>
        <input id="id-nominal" type="hidden">
        <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Nominal" />
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">tidak</button>
        <button key="kurang" class="btn red proses">Ya</button>
      </div>
    </div>
  </div>
</div>

<div id="modal-lebih" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Validasi Data Lebih</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">apakah anda yakin memvalidasi data tersebut ?</span>
        <input id="id-lebih" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button onclick="jangan_disesuaikan()" class="btn green">Tidak disesuaikan</button>
        <button key="lebih" class="btn red proses">disesuaikan</button>
      </div>
    </div>
  </div>
</div>

<div id="modal-cocok" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Validasi Data Cocok</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">apakah anda yakin memvalidasi data tersebut ?</span>
        <input id="id-cocok" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button key="cocok" class="btn red proses">Ya</button>
      </div>
    </div>
  </div>
</div>

<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">


<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'opname/opname_s_jadi/daftar_opname'; ?>";
  });
</script>
<script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});

</script>
<script>

  $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function cari_penjualan(){

    tanggal_awal = $('#tanggal_awal').val();
    tanggal_akhir = $('#tanggal_akhir').val();
    var url = "<?php echo base_url().'opname/cari_setengah_jadi'?>";
    if (tanggal_awal!='' && tanggal_akhir!='' ) {
      $.ajax({
        type: "POST",
        url: url,
        data: {tanggal_awal:tanggal_awal,tanggal_akhir:tanggal_akhir},
        beforeSend:function(){
          $(".tunggu").show();
        },
        success: function(msg)
        {
          $(".tunggu").hide();
          $('#cari_transaksi').html(msg);

        }
      });
    }else{
      alert('Isi Tanggal Awal Dan Tanggal Akhir !');
    }
  }
  function load_table(page){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'opname/opname_s_jadi/get_table_validasi_nominal' ?>",
      data: ({page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#daftar_list_stock").append(msg);

      }
    });
  }

  $(document).ready(function(){
    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": true,
      "info":     false
    });
    $('#opsi_filter').hide();
  });


  function validasi(id,status){
    // var key = $(this).attr('key');
    // alert(id);
    // alert(status);
    if(status=="kurang"){
      $('#id-kurang').val(id);
      $('#modal-kurang').modal('show');
    }else if(status=="cocok"){

      $('#id-cocok').val(id);
      $('#modal-cocok').modal('show');
    }else if(status=="lebih"){
      $('#id-lebih').val(id);
      
      $('#modal-lebih').modal('show');
    }
  }


  $('.tindak_lanjuti').click(function(){
    var id = $('#id-kurang').val();
      // alert(id);
      $('#id-nominal').val(id) ;
      $('#modal-nominal').modal('show');

    });

  $('.proses').click(function(){
    // alert(status);
    var status_validasi = $(this).attr('key');
    // alert(status_validasi);
    if(status_validasi=="kurang"){

      var id = $('#id-nominal').val();
      var nominal_opname = $('#nominal').val();
      var url = "<?php echo base_url().'opname/opname_s_jadi/sesuaikan/'; ?>"
    }else if(status_validasi=="cocok"){

      var id = $('#id-cocok').val();
      var url = "<?php echo base_url().'opname/opname_s_jadi/sesuaikan/'; ?>"
    }else if(status_validasi=="lebih"){

      var id = $('#id-lebih').val();
      var url = "<?php echo base_url().'opname/opname_s_jadi/sesuaikan/'; ?>"
    }else if(status_validasi=="kurang_tanpa_nominal"){
      var id = $('#id-kurang').val();
      // var nominal_opname = $('#nominal').val();
      var url = "<?php echo base_url().'opname/opname_s_jadi/sesuaikan/'; ?>"
    }
    if(parseInt(nominal_opname) < 0 || nominal_opname=='-'){
      alert('Nominal Opname Salah');
    }else{
     $.ajax( {  
      type :"post",  
      url : url,  
      cache :false,
      data : {status_validasi:status_validasi,id:id,nominal_opname:nominal_opname},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();
       window.location = "<?php echo base_url().'opname/opname_s_jadi/daftar_validasi_nominal/'; ?>";
     },  
     error : function(data) {  
      // alert("das");  
    }  
  });
   }



 });

  function jangan_disesuaikan(){
    var id = $('#id-lebih').val();
    // alert(status);
    // var status_validasi = status;
    // alert(id);
    $.ajax( {  
      type :"post",  
      url : "<?php echo base_url().'opname/opname_s_jadi/jangan_sesuaikan/'; ?>",  
      cache :false,
      data : {id:id},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();
       window.location = "<?php echo base_url().'opname/opname_s_jadi/daftar_validasi_nominal/'; ?>";
     },  
     error : function(data) {  
      // alert("das");  
    }  
  });
  }
  
  function print(Object) {
    var kode_opname = Object;
    window.open("<?php echo base_url() . 'opname/opname_s_jadi/print_opname_nominal/' ; ?>"+kode_opname);
    $('#modal-confirm-print').modal('hide');
  }
</script>