

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->

  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Daftar Piutang Member
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>


          <div class="box-body">            
            <div class="sukses" ></div>
            <div class="row">
              <div class="col-md-3" id="">
                <div class="input-group">
                  <span class="input-group-addon">Tanggal Awal</span>
                  <input type="text" class="form-control tgl" id="tgl_awal">
                </div>
              </div>

              <div class="col-md-3" id="">
                <div class="input-group">
                  <span class="input-group-addon">Tanggal Akhir</span>
                  <input type="text" class="form-control tgl" id="tgl_akhir">
                </div>
              </div> 
              <div class="col-md-3" id="">
                <div class="input-group">
                  <span class="input-group-addon">Jalur</span>
                  <select class="select2 form-control" id="kode_jalur">
                    <option value="0"> Pilih Semua Jalur</option>
                    <?php 
                      $jalur =  $this->db->get_where('master_jalur' , ['status'=>1])->result();
                      foreach( $jalur as $val){
                     ?>
                     <option value="<?php echo $val->kode_jalur ?>"><?php echo $val->kode_jalur . ' - '. $val->nama_jalur ?></option>

                    <?php } ?>
                  </select>
                </div>
              </div>                        
              <div class="col-md-2 pull-left">
                <button style="width: 147px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
              </div>
            </div><br>
            <div id="cari_transaksi">
              <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                <?php
                $this->db->order_by('sisa','desc');
                $this->db->group_by('nama_customer');
                $hutang = $this->db->get_where('transaksi_piutang',array('status'=>'member','kategori_member'=>'member','sisa !='=>'0'));
                $hasil_hutang = $hutang->result();
                ?>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Nominal Piutang</th>
                    <th>Sisa</th>
                    <th>Jalur</th>
                    <th>Status</th>
                    <th width="200px" >Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $nomor = 1;
                  foreach($hasil_hutang as $daftar){ 
                    ?> 
                      <tr>
                    <!-- <tr class=" <?php if($daftar->sisa!=0){ echo "danger"; } ?>"> -->
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo @$daftar->nama_customer; ?></td>

                      <?php
                       //$this->db->where('kategori_member','reguler');
                      $get_nominal_hutang=$this->db->query("SELECT sum(nominal_piutang) as nominal,sum(sisa) as sisa from transaksi_piutang where kode_customer='$daftar->kode_customer' and kategori_member='member'");
                      $hasil=$get_nominal_hutang->row();
                      $jalur  = $this->db->get_where('master_member' , ['kode_member' => $daftar->kode_customer])->result_array();
                      ?>
                      <td><?php echo format_rupiah(@$hasil->nominal); ?></td>
                      <td><?php echo format_rupiah(@$hasil->sisa); ?></td>
                      <td><?php if($hasil->sisa=='0'){ ?>
                        <label class="label label-primary">Lunas</label>
                      <?php }else { ?>   <label class="label label-warning">Angsuran</label> <?php  } ?></td>
                      <!--  <td><?php //echo format_rupiah(@$hasil->sisa); ?></td> -->
                      <!-- <td><?php// echo cek_status_piutang($daftar->sisa); ?></td> -->
                      <td> <?php echo $jalur[0]['nama_jalur'] ?></td>
                      <td align="center" width="125px">

                       <!--   <a class="btn btn-primary" href="<?php echo base_url().'hutang_piutang/piutang_member/detail_piutang/'.$daftar->kode_customer ?>"><i class="fa fa-pencil"></i> Detail</a> -->
                       <?php
                       echo get_detail_member($daftar->kode_customer);

                       ?></td>
                     </tr>
                     <?php $nomor++; } ?>

                   </tbody>
                   <tfoot>
                     <tr>
                      <th>No</th>
                      <th>Nama Member</th>
                      <th>Nominal Piutang</th>
                      <th>Sisa</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section> 
    </div>
  </section> 
</div>
<script type="text/javascript">
  $(document).ready(function() {

   $("#tabel_daftar").dataTable({
    // "paging":   false,
    // "ordering": false,
    // "info":     false,
    // "searching":     false
  });   
 } );
</script>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'record/'; ?>";
  });
</script>
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});
  $('#cari').click(function(){
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    var kode_unit =$("#kode_unit").val();
    var kode_jalur =$("#kode_jalur").val();
    if (tgl_awal=='' || tgl_akhir==''){ 
      alert('Masukan Tanggal Awal & Tanggal Akhir..!')
    }
    else{
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'hutang_piutang/piutang_member/cari_piutang'; ?>",  
        cache :false,
        
        data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit , jalur : kode_jalur},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
         $(".tunggu").hide();  
         $("#cari_transaksi").html(data);
       },  
       error : function(data) {  
         // alert("das");  
       }  
     });
    }

    $('#tgl_awal').val('');
    $('#tgl_akhir').val('');

  });
</script>
