

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption"> 
            Proses Pembayaran Piutang Konsinyasi
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>


          <div class="box-body">            

            <form id="data_form" action="" method="post">
              <div class="box-body">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Kode Transaksi</label>
                        <?php
                        $kode = $this->uri->segment(4);

                        $transaksi_penjualan = $this->db->get_where('transaksi_penjualan',array('kode_penjualan'=>$kode));
                        $hasil_transaksi_penjualan = $transaksi_penjualan->row();
                        ?>
                        <input readonly="true" type="text" value="<?php echo @$hasil_transaksi_penjualan->kode_penjualan; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_transaksi" id="kode_transaksi" />
                      </div>

                      <div class="form-group">
                        <label class="gedhi">Tanggal Transaksi</label>
                        <input type="text" value="<?php echo TanggalIndo($hasil_transaksi_penjualan->tanggal_penjualan); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_pembelian" id="tanggal_pembelian"/>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Kode Member</label>
                        <input readonly="true" type="text" value="<?php echo $hasil_transaksi_penjualan->kode_member ?>" class="form-control" placeholder="Kode Member" name="kode_member" id="kode_member" />
                      </div>

                      <div class="form-group">
                        <label>Nama Member</label>
                        <input readonly="true" type="text" value="<?php echo $hasil_transaksi_penjualan->nama_member ?>" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" />
                      </div>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Nominal Hutang</label>
                      <input readonly="true" type="text" value="<?php echo format_rupiah($hasil_transaksi_penjualan->grand_total) ?>" class="form-control" placeholder="Nota Referensi" name="" id="" />

                      <input readonly="true" type="hidden" value="<?php echo $hasil_transaksi_penjualan->grand_total ?>" class="form-control" placeholder="Nota Referensi" name="nominal_hutang" id="nominal_hutang" />
                      <?php 
                        $get_sisa=$this->db->get_where('transaksi_piutang',array('kode_customer'=>$hasil_transaksi_penjualan->kode_member,'kode_transaksi'=>$hasil_transaksi_penjualan->kode_penjualan));
                        $hasil_sisa=$get_sisa->row();
                       // echo $this->db->last_query();
                      ?>
                       <input readonly="true" type="hidden" value="<?php echo $hasil_sisa->sisa ?>" class="form-control" placeholder="Nota Referensi" name="sisa" id="sisa" />
                    </div>
                    <div class="form-group col-md-6">
                      <label>Petugas</label>
                      <input readonly="true" type="text" value="<?php echo ($hasil_transaksi_penjualan->petugas) ?>" class="form-control" placeholder="Nota Referensi" name="nomor_nota" id="nomor_nota" />
                    </div>
                  </div>
                </div>
              </div> 

              <div class="sukses" ></div>
             
              <br><br>
             
          </div>

          <div class="box-body">
            <div id="list_transaksi_pembelian">
              <div class="box-body"><br>
                <table id="tabel_daftar" style="font-size:2em;" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama bahan</th>
                      <th>QTY</th>
                      <th>Harga Satuan</th>
                      <th>Subtotal</th>
                      <th width="100px">Action</th>
                    </tr>
                  </thead>
                  <tbody id="tabel_temp_data_transaksi">

                  </tbody>
                  <tfoot>

                  </tfoot>
                </table>
              </div>
            </div>
            <br>
            <div class="row">
              <!-- <div class="col-md-1 " style="padding-top:26px; padding-left:0px;" > -->
               <center><div onclick="simpan_bayar_member()"  class="btn btn-primary btn-lg  btn-block" style="width:200px;"><i class="fa fa-save"></i> Simpan </div></center>
             <!-- </div> -->
           </div>
         </div>

       </form>
     </div>

     <!------------------------------------------------------------------------------------------------------>

   </div>
 </div>

</section><!-- /.Left col -->      
</div><!-- /.row (main row) -->


</section><!-- /.Left col -->      
</div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus produk tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    var kode_member =$('#kode_member').val();
    window.location = "<?php echo base_url().'hutang_piutang/piutang_member/detail/'; ?>"+kode_member;
  });
</script>
<script>
  function setting() {
    $('#modal_setting').modal('show');
  }
  function confirm_bayar(){
    $("#modal-confirm-bayar").modal('show');
  }


  $(document).ready(function(){
    $("#update").hide();
    var kode_transaksi = $('#kode_transaksi').val();
    var url = '<?php echo base_url().'hutang_piutang/piutang_member/move_table'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        kode_transaksi:kode_transaksi
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
      
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'hutang_piutang/piutang_member/get_bayar_piutang_member/'; ?>"+kode_transaksi);

      }
    });

    
      //  $("#tabel_daftar").dataTable();
      $(".tgl").datepicker();
      $(".select2").select2();
      //$("#div_dibayar").hide();
    /*  var temp_data = "<?php #echo base_url().'pembelian/tabel_temp_data_transaksi/'?>";
      $.ajax({
        type: "POST",
        url: temp_data,
        data: {},
          success: function(temp) {
            // alert(temp);
            //var data = temp.split("|");
            $("#tabel_temp_data_transaksi").html(temp);
            
          }
        });*/
        $('#ok').on('click',function(){
         var simpan_transaksi = "<?php echo base_url().'pre_order/simpan_transaksi/'?>";
         $.ajax({
          type: "POST",
          url: simpan_transaksi,
          data: $('#data_form').serialize(),
          beforeSend:function(){
            $(".tunggu").show();  
          },
          success: function(msg)
          {

            $(".tunggu").hide();
            var data = msg.split("|");
            var num = data[0];
            var pesan = data[1];

            if(num == 0){  
              $(".sukses").html(pesan);   
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'pre_order/daftar' ?>";
              },1500);   

            }
            else{
              $(".sukses").html(pesan);   
              setTimeout(function(){$('.sukses').html('');
                window.location = "<?php echo base_url() . 'pre_order/daftar' ?>";
              },1500); 
            }  

            $("#modal-confirm-bayar").modal('hide');   
          }
        });
         return false;

       });
        



        $('#kode_bahan').on('change',function(){
          var kode_bahan = $('#kode_bahan').val();
          var kode_transaksi = $('#kode_transaksi').val();
          var url = "<?php echo base_url() . 'hutang_piutang/piutang_member/get_satuan' ?>";
          $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: {kode_bahan:kode_bahan,kode_transaksi:kode_transaksi},
            success: function(msg){
              if(msg.nama_satuan){
                $('#nama_satuan').val(msg.nama_satuan);
              }
              if(msg.nama_menu){
                $("#nama_bahan").val(msg.nama_menu);
              }
              if(msg.harga_satuan){
                $("#harga_satuan").val(msg.harga_satuan);
              }
            }
          });
        });



        $("#data_form").submit(function(){
          var simpan_transaksi = "<?php echo base_url().'pre_order/simpan_transaksi/'?>";
          $.ajax({
            type: "POST",
            url: simpan_transaksi,
            data: $('#data_form').serialize(),
            beforeSend:function(){
              $(".tunggu").show();  
            },
            success: function(msg)
            {
              $(".tunggu").hide();
              var data = msg.split("|");
              var num = data[0];
              var pesan = data[1];

              if(num == 0){  
                $(".sukses").html(pesan);   
                setTimeout(function(){$('.sukses').html('');
                  window.location = "<?php echo base_url() . 'pre_order/daftar' ?>";
                },1500);   

              }
              else{
                $(".sukses").html(pesan);   
                setTimeout(function(){$('.sukses').html('');
              },1500); 
              }     
            }
          });
          return false;

        });

      });

  function add_item(){
    var kode_transaksi = $('#kode_transaksi').val();
    var kode_bahan = $('#kode_bahan').val();
    var jumlah = $('#jumlah').val();
    var harga_satuan = $("#harga_satuan").val();
    var sub_total = jumlah * harga_satuan; 
    var nama_bahan = $("#nama_bahan").val();
    var keterangan = $("#keterangan").val();
    var kode_member = $("#kode_member").val();
    
    var url = "<?php echo base_url().'hutang_piutang/piutang_member/simpan_item_temp/'?> ";

    $.ajax({
      type: "POST",
      url: url,
      data: { kode_member:kode_member,kode_transaksi:kode_transaksi,
        kode_bahan:kode_bahan,
        nama_bahan:nama_bahan,
        keterangan:keterangan,
        jumlah:jumlah,harga_satuan:harga_satuan,sub_total:sub_total
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(data)
      {
      $(".sukses").html(data); 
       $(".tunggu").hide(); 
       $("#tabel_temp_data_transaksi").load("<?php echo base_url().'hutang_piutang/piutang_member/get_bayar_piutang_member/'; ?>"+kode_transaksi);
       $('#kode_bahan').val('');
       $('#jumlah').val('');
       ('#harga_satuan').val('');
       $('#nama_satuan').val('');      
       $("#keterangan").val('');             
     }
   });

  }

  function simpan_bayar_member(){
    var kode_transaksi = $('#kode_transaksi').val();
    var kode_member = $('#kode_member').val();
    var nominal_hutang = $("#sisa").val();
    var total = $('#total').val();
    //alert(nominal_hutang);


    var url = "<?php echo base_url().'hutang_piutang/piutang_member/simpan_bayar_member/'?> ";

    $.ajax({
      type: "POST",
      url: url,
      data: { kode_member:kode_member,kode_transaksi:kode_transaksi,total:total,nominal_hutang:nominal_hutang
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(data)
      {
       $(".tunggu").hide(); 

       window.location = "<?php echo base_url().'hutang_piutang/piutang_member/detail/'; ?>"+kode_member;
     }
   });

  }

  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }

  function actEdit(id) {
    var id = id;
    var kode_transaksi = $('#kode_transaksi').val();
    var url = "<?php echo base_url().'hutang_piutang/piutang_member/get_temp_bayar_piutang/'; ?>";
    $.ajax({
      type: 'POST',
      url: url,
      dataType: 'json',
      data: {id:id},
      success: function(piutang){

         $("#kode_bahan").select2().select2('val', piutang.kode_bahan);
        $('#jumlah').val(piutang.jumlah);
        $("#id_item").val(piutang.id);
        $("#keterangan").val(piutang.keterangan);
        $("#harga_satuan").val(piutang.harga_satuan);
        $("#add").hide();
        $("#update").show();
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'hutang_piutang/piutang_member/get_bayar_piutang_member/'; ?>"+kode_transaksi);
         //get_satuan();
      }
    });
  }

  function update_item(){
    var kode_transaksi = $('#kode_transaksi').val();
    var kode_bahan = $('#kode_bahan').val();
    var jumlah = $('#jumlah').val();
    var nama_bahan = $("#nama_bahan").val();
    var keterangan = $("#keterangan").val();
    var id = $("#id_item").val();
    var url = "<?php echo base_url().'hutang_piutang/piutang_member/update_item_temp/'?> ";

    $.ajax({
      type: "POST",
      url: url,
      data: { kode_transaksi:kode_transaksi,
        kode_bahan:kode_bahan,
        nama_bahan:nama_bahan,
        keterangan:keterangan,
        jumlah:jumlah,
        id:id
      },
      success: function(data)
      {
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'hutang_piutang/piutang_member/get_bayar_piutang_member/'; ?>"+kode_transaksi);
      //$('#kategori_bahan').val('');
      $('#kode_bahan').val('');
      $('#jumlah').val('');
      $("#nama_bahan").val('');
      $("#keterangan").val('');
      $("#id_item").val('');
      $("#add").show();
      $("#update").hide();
    }
  });
  }

  function delData() {
    var id = $('#id-delete').val();
    var kode_transaksi = $('#kode_transaksi').val();
    var url = '<?php echo base_url().'hutang_piutang/piutang_member/hapus_item_temp'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id:id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $(".tunggu").hide();
        $('#modal-confirm').modal('hide');
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'hutang_piutang/piutang_member/get_bayar_piutang_member/'; ?>"+kode_transaksi);

      }
    });
    return false;
  }
</script>
