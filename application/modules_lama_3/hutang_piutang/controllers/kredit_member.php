<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kredit_member extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('astrosession') == false) {
            redirect(base_url('authenticate'));
        }
        $this->load->library('form_validation');
    }

    //------------------------------------------ View Data Table----------------- --------------------//

    public function daftar_kredit()
    
    {
        $data['aktif'] = 'hutang_piutang';
        $data['konten'] = $this->load->view('hutang_piutang/kredit_member/daftar_kredit', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/kredit_member/menu', $data, true);
        $this->load->view('main', $data);
    }

    public function daftar_hutang_belum_lunas()
    {
        $data['aktif'] = 'hutang_piutang';
        $data['konten'] = $this->load->view('hutang_piutang/hutang/daftar_hutang', null, true);
        $this->load->view('main', $data);
    }

    public function cari_hutang()
    {
        $this->load->view('hutang_piutang/hutang/cari_hutang');
    }

    public function detail($kode)
    {
        $data['aktif'] = 'hutang_piutang';
        $data['kode'] = $kode;
        $data['konten'] = $this->load->view('hutang_piutang/kredit_member/detail_kredit', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/kredit_member/menu', $data, true);
        $this->load->view('main', $data);


    }
    public function detail_lunas($kode)
    {
        $data['aktif'] = 'hutang_piutang';
        $data['kode'] = $kode;
        $data['konten'] = $this->load->view('hutang_piutang/kredit_member/detail_angsuran', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/kredit_member/menu', $data, true);
        $this->load->view('main', $data);


    }

    public function proses($kode)
    {
        $data['aktif'] = 'hutang_piutang';
        $data['kode'] = $kode;
        $data['konten'] = $this->load->view('hutang_piutang/kredit_member/proses_piutang', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/kredit_member/menu', $data, true);
        $this->load->view('main', $data);


    }


    //------------------------------------------ Proses ----------------- --------------------//

    public function get_rupiah()
    {
        $angsuran = $this->input->post('angsuran');
        $hasil = format_rupiah($angsuran);

        echo $hasil;

    }

    public function cek_sisa()
    {
        $kode_transaksi = $this->input->post('kode_transaksi');
        $transaksi_hutang = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
            $kode_transaksi));
        $hasil_transaksi_hutang = $transaksi_hutang->row();
        $sisa = $hasil_transaksi_hutang->sisa;

        echo $sisa;

    }

    public function simpan_piutang()
    {
       $kode_transaksi = $this->input->post('kode_transaksi');
       $sisa           = $this->input->post('sisa');
       $angsuran       = $this->input->post('angsuran');
       $jatuh_tempo    = $this->input->post('jatuh_tempo');
       $jenis_transaksi= $_POST['jenis_transaksi'];
        if($angsuran > $sisa){
            echo '<div style="font-size:1.5em" class="alert alert-danger">Angsuran Lebih Besar Dari Sisa.</div>';
        }else{
            $query                    = $this->db->get_where('transaksi_piutang',array('kode_transaksi' => $kode_transaksi) )->row();
            // echo $this->db->last_query();
            // exit();
            $cek_angsuran             = $query->angsuran;
            $cek_nominal              = $query->nominal_piutang;
            $opsi['kode_transaksi']   = $kode_transaksi ;
            $opsi['angsuran']         = $angsuran ;
            $opsi['tanggal_angsuran'] = date("Y-m-d") ;
            $this->db->insert('opsi_piutang',$opsi);

            $this->db->select('*') ;
            $query_akun     = $this->db->get_where('keuangan_sub_kategori_akun',array('kode_sub_kategori_akun'=> '1.1.4'))->row();
            $kode_sub       = $query_akun->kode_sub_kategori_akun;
            $nama_sub       = $query_akun->nama_sub_kategori_akun;
            $kode_kategori  = $query_akun->kode_kategori_akun;
            $nama_kategori  = $query_akun->nama_kategori_akun;
            $kode_jenis     = $query_akun->kode_jenis_akun;
            $nama_jenis     = $query_akun->nama_jenis_akun;
            
            $get_id_petugas = $this->session->userdata('astrosession');
            $id_petugas     = $get_id_petugas->id;
            $nama_petugas   = $get_id_petugas->uname;

            if(empty($cek_angsuran)){
               $update_hutang['angsuran']              = $angsuran;
               $update_hutang['sisa']                  = $cek_nominal - $angsuran ;
               $update_hutang['jatuh_tempo']           = $jatuh_tempo;
               $sukses = $this->db->update('transaksi_piutang', $update_hutang, array('kode_transaksi'=>$kode_transaksi) );
               
               $data_keu['petugas']                    = $nama_petugas ;
               $data_keu['kode_referensi']             = $kode_transaksi ;
               $data_keu['tanggal_transaksi']          = date("Y-m-d") ;
               $data_keu['keterangan']                 = 'angsuran penjualan' ;
               $data_keu['nominal']                    = $angsuran;
               $data_keu['kode_jenis_keuangan']        = $kode_jenis ;
               $data_keu['nama_jenis_keuangan']        = $nama_jenis ;
               $data_keu['kode_kategori_keuangan']     = $kode_kategori ;
               $data_keu['nama_kategori_keuangan']     = $nama_kategori ;
               $data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
               $data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;
               $keuangan                               = $this->db->insert("keuangan_masuk", $data_keu);
            } else{
                $update_angsuran['angsuran'] = $cek_angsuran + $angsuran;
                $update_angsuran['jatuh_tempo'] = $jatuh_tempo;
                $this->db->update('transaksi_piutang', $update_angsuran,  array('kode_transaksi'=>$kode_transaksi) );

                $query_angsuran = $this->db->get_where('transaksi_piutang',array('kode_transaksi' => $kode_transaksi) )->row()->angsuran;
                $update_sisa['sisa'] = $cek_nominal - $query_angsuran ;
                $sukses = $this->db->update('transaksi_piutang', $update_sisa,  array('kode_transaksi'=>$kode_transaksi) );

                $data_keu['petugas'] = $nama_petugas ;
                $data_keu['kode_referensi'] = $kode_transaksi ;
                $data_keu['tanggal_transaksi'] = date("Y-m-d") ;
                $data_keu['keterangan'] = 'angsuran penjualan' ;
                $data_keu['nominal'] = $angsuran;
                $data_keu['kode_jenis_keuangan'] = $kode_jenis ;
                $data_keu['nama_jenis_keuangan'] = $nama_jenis ;
                $data_keu['kode_kategori_keuangan'] = $kode_kategori ;
                $data_keu['nama_kategori_keuangan'] = $nama_kategori ;
                $data_keu['kode_sub_kategori_keuangan'] = $kode_sub ;
                $data_keu['nama_sub_kategori_keuangan'] = $nama_sub ;

                $keuangan = $this->db->insert("keuangan_masuk", $data_keu);
        }
        if($sukses){
            

            $sales_penagih  =   $_POST['sales'];

            #Check the Transaksi Penjualan based on Kode Penjualan
            $new  =   $this->db->get_where('transaksi_penjualan' , ['kode_penjualan' => $kode_transaksi ] )->result_array();

            #ambil kode sales yang bikin order ke member
            $kode_sales_repeat     =   $new[0]['kode_petugas'];

            #ambil sales yang mendaftarkannya
            $cust   =   $this->db->get_where('master_member' , ['kode_member' => $new[0]['kode_member']])->result_array();
            $registered_sales   =   $cust[0]['registered_sales'];



            $komisi_for_penagih     = 0.5 ;
            $komisi_for_register    = 0.25 ;
            $komisi_for_repeat_order= 0.25 ;

            $jatuh_tempo = !empty($jatuh_tempo) ? date('Y-m-d') : $jatuh_tempo;           

            #
            $tanggal    =   date("Y-m-d");
            if ($tanggal <= $jatuh_tempo || $jenis_transaksi == 'Lunas' ) {
                #diibayar sebelum jatuh tempo berarti dapat komisi
                $komisi     =   1;
            } else {
                #dibayar lewat jatuh tempo berarti tidak dapat komisi
                $komisi     =   0;
            }
            
            if ($komisi == 1) {
                #jika sales register sama dengan sales yang menagih

                $registered_sales = empty($registered_sales) ? 'Tidak ada' : $registered_sales;
                

                $komisi_penagih     =   $angsuran * $komisi_for_penagih /100 ;
                $komisi_register    =   $angsuran * $komisi_for_register / 100;
                $komisi_repeat      =   $angsuran * $komisi_for_repeat_order / 100;

                $insert_komisi['penagih']  =   [
                    'kode_transaksi_asal_komisi' => $kode_transaksi ,
                    'nominal_terbayar'           => $angsuran ,
                    'tujuan_komisi'              => 'SALES' ,
                    'kode_sales'                 => $sales_penagih ,
                    'besar_komisi'               => 0.5 ,
                    'nominal_komisi'             => $komisi_penagih ,
                    'kode_customer'              => $new[0]['kode_member'] ,
                    'tanggal'                    => date('Y-m-d'),
                    'keterangan'                 => 'Komisi Penagihan'
                ];

                $insert_komisi['register']  =   [
                    'kode_transaksi_asal_komisi' => $kode_transaksi ,
                    'nominal_terbayar'           => $angsuran ,
                    'tujuan_komisi'              => 'SALES' ,
                    'kode_sales'                 => $registered_sales ,
                    'besar_komisi'               => 0.25 ,
                    'nominal_komisi'             => $komisi_register ,
                    'kode_customer'              => $new[0]['kode_member'] ,
                    'tanggal'                    => date('Y-m-d'),
                    'keterangan'                 => 'Komisi mendaftarkan Member Baru'

                ];

                $insert_komisi['repeat']  =   [
                    'kode_transaksi_asal_komisi' => $kode_transaksi ,
                    'nominal_terbayar'           => $angsuran ,
                    'tujuan_komisi'              => 'SALES' ,
                    'kode_sales'                 => $kode_sales_repeat ,
                    'besar_komisi'               => 0.25 ,
                    'nominal_komisi'             => $komisi_repeat ,
                    'kode_customer'              => $new[0]['kode_member'] ,
                    'tanggal'                    => date('Y-m-d'),
                    'keterangan'                 => 'Komisi melakukan Penjualan / Repeat Order'
                ];

                $this->db->insert('transaksi_komisi_revisi' , $insert_komisi['penagih'] );
                $this->db->insert('transaksi_komisi_revisi' , $insert_komisi['register'] );
                $this->db->insert('transaksi_komisi_revisi' , $insert_komisi['repeat'] );
                
            }

            echo 1;

        }       
    }
    
}



}
