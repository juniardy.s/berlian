<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('astrosession') == false) {
            redirect(base_url('authenticate'));
        }
        $this->load->library('form_validation');
    }

    //------------------------------------------ View Data Table----------------- --------------------//

    public function daftar_hutang()
    {
        $data['aktif'] = 'hutang_piutang';
        $data['konten'] = $this->load->view('hutang_piutang/hutang/daftar_hutang', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/hutang/menu', $data, true);
        $this->load->view('main', $data);


    }

    public function daftar_hutang_belum_lunas()
    {
        $data['aktif'] = 'hutang_piutang';
        $data['konten'] = $this->load->view('hutang_piutang/hutang/daftar_hutang', null, true);
        $this->load->view('main', $data);
    }

    public function cari_hutang()
    {
        $this->load->view('hutang_piutang/hutang/cari_hutang');
    }

    public function detail($kode)
    {
        $data['aktif'] = 'hutang_piutang';
        $data['kode'] = $kode;
        $data['konten'] = $this->load->view('hutang_piutang/hutang/detail_hutang', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/hutang/menu', $data, true);
        $this->load->view('main', $data);


    }

    public function proses($kode)
    {
        $data['aktif'] = 'hutang_piutang';
        $data['kode'] = $kode;
        $data['konten'] = $this->load->view('hutang_piutang/hutang/proses_hutang', $data, true);
        $data['halaman'] = $this->load->view('hutang_piutang/hutang/menu', $data, true);
        $this->load->view('main', $data);


    }


    //------------------------------------------ Proses ----------------- --------------------//

    public function get_rupiah()
    {
        $angsuran = $this->input->post('angsuran');
        $hasil = format_rupiah($angsuran);

        echo $hasil;

    }

    public function cek_sisa()
    {
        $kode_transaksi = $this->input->post('kode_transaksi');
        $transaksi_hutang = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
            $kode_transaksi));
        $hasil_transaksi_hutang = $transaksi_hutang->row();
        $sisa = $hasil_transaksi_hutang->sisa;

        echo $sisa;

    }

    public function simpan_hutang()
    {
        $kode_transaksi = $this->input->post('kode_transaksi');
        $angsuran = $this->input->post('angsuran');
        $jatuh_tempo = $this->input->post('jatuh_tempo');
        $jenis_transaksi = $this->input->post('jenis_transaksi');
        #echo $jenis_transaksi;
        $transaksi_hutang = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
            $kode_transaksi));
        $hasil_transaksi_hutang = $transaksi_hutang->row();
        $sisa = $hasil_transaksi_hutang->sisa;

        if ($angsuran > $sisa) {
            echo '<div style="font-size:1.5em" class="alert alert-danger">angsuran lebih besar dari sisa.</div>';
        } else {


            $query = $this->db->get_where('transaksi_hutang', array('kode_transaksi' => $kode_transaksi))->
            row();
            $cek_angsuran = $query->angsuran;
            $cek_nominal = $query->nominal_hutang;


            if($jenis_transaksi=='Lunas'){
                $this->db->select('*');
                $query_akun = $this->db->get_where('keuangan_sub_kategori_akun', array('kode_sub_kategori_akun' =>
                    '2.1.5'))->row();        
            }else if ($jenis_transaksi=='Angsuran') {
               $this->db->select('*');
               $query_akun = $this->db->get_where('keuangan_sub_kategori_akun', array('kode_sub_kategori_akun' =>
                '2.1.4'))->row();   
           }
           
           $kode_sub = $query_akun->kode_sub_kategori_akun;
           $nama_sub = $query_akun->nama_sub_kategori_akun;
           $kode_kategori = $query_akun->kode_kategori_akun;
           $nama_kategori = $query_akun->nama_kategori_akun;
           $kode_jenis = $query_akun->kode_jenis_akun;
           $nama_jenis = $query_akun->nama_jenis_akun;

           $get_id_petugas = $this->session->userdata('astrosession');
           $id_petugas = $get_id_petugas->id;
           $nama_petugas = $get_id_petugas->uname;

           if (empty($cek_angsuran)) {

            $opsi['kode_transaksi'] = $kode_transaksi;
            $opsi['angsuran'] = $angsuran;
            $opsi['tanggal_angsuran'] = date("Y-m-d");
            $this->db->insert('opsi_hutang', $opsi);

            $update_hutang['angsuran'] = $angsuran;
            $update_hutang['sisa'] = $cek_nominal - $angsuran;
            $sukses = $this->db->update('transaksi_hutang', $update_hutang, array('kode_transaksi' =>
                $kode_transaksi));

            $data_keu['petugas'] = $nama_petugas;
            $data_keu['kode_referensi'] = $kode_transaksi;
            $data_keu['tanggal_transaksi'] = date("Y-m-d");
            $data_keu['keterangan'] = 'angsuran pembelian';
            $data_keu['nominal'] = $angsuran;
            $data_keu['kode_jenis_keuangan'] = $kode_jenis;
            $data_keu['nama_jenis_keuangan'] = $nama_jenis;
            $data_keu['kode_kategori_keuangan'] = $kode_kategori;
            $data_keu['nama_kategori_keuangan'] = $nama_kategori;
            $data_keu['kode_sub_kategori_keuangan'] = $kode_sub;
            $data_keu['nama_sub_kategori_keuangan'] = $nama_sub;

            $keuangan = $this->db->insert("keuangan_keluar", $data_keu);
        } else {
            $opsi['kode_transaksi'] = $kode_transaksi;
            $opsi['angsuran'] = $angsuran;
            $opsi['tanggal_angsuran'] = date("Y-m-d");
            $this->db->insert('opsi_hutang', $opsi);

            

            $update_angsuran['angsuran'] = $cek_angsuran + $angsuran;
            $this->db->update('transaksi_hutang', $update_angsuran, array('kode_transaksi' =>
                $kode_transaksi));

            $query_angsuran = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
                $kode_transaksi))->row()->angsuran;
            $update_sisa['sisa'] = $cek_nominal - $query_angsuran;
            $update_sisa['tanggal_jatuh_tempo']=$jatuh_tempo;
            $sukses = $this->db->update('transaksi_hutang', $update_sisa, array('kode_transaksi' =>
                $kode_transaksi));

            $data_keu['petugas'] = $nama_petugas;
            $data_keu['kode_referensi'] = $kode_transaksi;
            $data_keu['tanggal_transaksi'] = date("Y-m-d");
            $data_keu['keterangan'] = 'angsuran pembelian';
            $data_keu['nominal'] = $angsuran;
            $data_keu['kode_jenis_keuangan'] = $kode_jenis;
            $data_keu['nama_jenis_keuangan'] = $nama_jenis;
            $data_keu['kode_kategori_keuangan'] = $kode_kategori;
            $data_keu['nama_kategori_keuangan'] = $nama_kategori;
            $data_keu['kode_sub_kategori_keuangan'] = $kode_sub;
            $data_keu['nama_sub_kategori_keuangan'] = $nama_sub;

            $keuangan = $this->db->insert("keuangan_keluar", $data_keu);

        }
        

        if ($sukses) {
            echo '1';
        }
    }
}


}
