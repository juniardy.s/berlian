<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
    	parent::__construct();
    	if ($this->session->userdata('astrosession') == false) {
    		redirect(base_url('authenticate'));
    	}
    	$this->load->library('form_validation');
    }

    //------------------------------------------ View Data Table----------------- --------------------//
    public function get_hutang()
    {

    	$start = (50*$this->input->post('page'));
    	$param = $this->input->post();
    	@$transaksi_hutang=$param['kode_transaksi'];
    	if(@$param['tgl_awal'] && @$param['tgl_akhir']){
    		$tgl_awal = $param['tgl_awal'];
    		$tgl_akhir = $param['tgl_akhir'];

    		$this->db->where('tanggal_transaksi >=', $tgl_awal);
    		$this->db->where('tanggal_transaksi <=', $tgl_akhir);

    	}
    	$this->db->group_by('kode_supplier');
    	$this->db->limit(50,$start);
    	$this->db->order_by('nama_supplier', 'asc');
    	$this->db->order_by('tanggal_transaksi', 'desc');
    	$hutang = $this->db->get('transaksi_hutang');

    	$hasil_ambil = $hutang->result();
    	$nomor = $start+1;
    	foreach ($hasil_ambil as $daftar) {
    		$this->db->where('kode_supplier', $daftar->kode_supplier);
    		$this->db->select('SUM(nominal_hutang) as hutang, SUM(sisa) as sisa');
    		$get_total = $this->db->get_where('transaksi_hutang');
    		$hasil_total = $get_total->row();
    		$tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : @TanggalIndo(@$daftar->tanggal_transaksi) ;
    		?>
    		<tr>
    			<td><?php echo $nomor; ?></td>
    			<!--<td><?php echo @$daftar->kode_transaksi; ?></td>-->
    			<td><?php echo @$daftar->nama_supplier; ?></td>
    			<td><?php echo format_rupiah(@$hasil_total->hutang); ?></td>
    			<td><?php echo format_rupiah(@$hasil_total->sisa); ?></td>
    			<td><?php echo get_detail_hut($daftar->kode_supplier)?></td>
    		</tr>
    		<?php

    	}
    }


    public function get_daftar_hutang()
    {

    	$start = (50*$this->input->post('page'));
    	$kode = $this->input->post('kode');



    // if(!empty($nama))
    // {
    //     $this->db->where('nama_supplier',$nama);
    // }

    	$this->db->limit(50,$start);
    	$pembelian = $this->db->get_where('transaksi_hutang',array('kode_supplier'=>$kode));
    	$hasil_transaksi = $pembelian->result();
    	$nomor = $start+1;
    	foreach ($hasil_transaksi as $daftar) {
    		$total +=$daftar->nominal_hutang;
    		$tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : TanggalIndo(@$daftar->tanggal_transaksi) ;

    		?>
    		<tr class="<?php if(@$daftar->sisa > 0){ echo "warning"; }?>">
    			<td><?php echo $nomor; ?></td>
    			<td><?php echo @$daftar->kode_transaksi; ?></td>
    			<td><?php echo $tgl; ?></td>
    			<td><?php echo @$daftar->nama_supplier; ?></td>
    			<td><?php echo format_rupiah(@$daftar->sisa); ?></td>
    			<td><?php if($daftar->tanggal_jatuh_tempo=='0000-00-00' || empty($daftar->tanggal_jatuh_tempo)){echo '-';}else{ echo @TanggalIndo(@$daftar->tanggal_jatuh_tempo); }?></td>
    			<td><?php echo cek_status_piutang($daftar->sisa); ?></td>
    			</tr>
    			<?php

        }//echo $this->db->last_query();


    }
    public function index()
    {
    	$data['aktif'] = 'hutang_piutang';
    	$data['konten'] = $this->load->view('hutang/daftar_hutang', $data, true);
    	$data['halaman'] = $this->load->view('hutang/menu', $data, true);
    	$this->load->view('hutang/main', $data);


    }

    public function daftar_hutang()
    {
    	$data['aktif'] = 'hutang_piutang';
    	$data['konten'] = $this->load->view('hutang/daftar_hutang', $data, true);
    	$data['halaman'] = $this->load->view('hutang/menu', $data, true);
    	$this->load->view('main', $data);


    }

    public function daftar_hutang_belum_lunas()
    {
    	$data['aktif'] = 'hutang_piutang';
    	$data['konten'] = $this->load->view('hutang/daftar_hutang', null, true);
    	$this->load->view('main', $data);
    }

    public function cari_hutang()
    {
    	$this->load->view('hutang/cari_hutang');
    }

    public function cari_daftar_hutang()
    {
    	$this->load->view('hutang/cari_daftar_hutang');
    }


    public function detail($kode)
    {
    	$data['aktif'] = 'hutang_piutang';
    	$data['kode'] = @$kode;
    	$data['konten'] = $this->load->view('hutang/detail_hutang', $data, true);
    	$data['halaman'] = $this->load->view('hutang/menu', $data, true);
    	$this->load->view('main', $data);


    }
    public function detail_cari($kode)
    {
    	$data['aktif'] = 'hutang_piutang';
    	$data['kode'] = @$kode;
    	$data['konten'] = $this->load->view('hutang/detail_hutang_cari', $data, true);
    	$data['halaman'] = $this->load->view('hutang/menu', $data, true);
    	$this->load->view('main', $data);


    }

    public function detail_daftar($kode)
    {
    	$data['aktif'] = 'hutang_piutang';
    	$data['konten'] = $this->load->view('hutang/detail_daftar_hutang', $data, true);
    	$data['halaman'] = $this->load->view('hutang/menu', $data, true);
    	$this->load->view('main', $data);
    }

    /*public function proses($kode)
    {
        $data['aktif'] = 'hutang_piutang';
        $data['kode'] = $kode;
        $data['konten'] = $this->load->view('hutang/proses_hutang', $data, true);
        $data['halaman'] = $this->load->view('hutang/menu', $data, true);
        $this->load->view('main', $data);


    }*/

    //------------------------------------------ Proses ----------------- --------------------//

    public function get_rupiah()
    {
    	$angsuran = $this->input->post('angsuran');
    	$hasil = format_rupiah($angsuran);

    	echo $hasil;

    }
    public function get_potongan()
    {
    	$potongan = $this->input->post('potongan_hutang');
    	$hasil = format_rupiah($potongan);

    	echo $hasil;

    }

    public function cek_sisa()
    {
    	$kode_transaksi = $this->input->post('kode_transaksi');
    	$transaksi_hutang = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
    		$kode_transaksi));
    	$hasil_transaksi_hutang = $transaksi_hutang->row();
    	$sisa = $hasil_transaksi_hutang->sisa;

    	echo $sisa;

    }

    public function simpan_hutang()
    {
    	$kode_transaksi = $this->input->post('kode_transaksi');
    	$angsuran = $this->input->post('angsuran');
    	$jatuh_tempo = $this->input->post('jatuh_tempo');

        // $potongan_hutang = $this->input->post('potongan_hutang');
    	$jenis_transaksi = $this->input->post('jenis_transaksi');

    	$transaksi_hutang = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
    		$kode_transaksi));
    	$hasil_transaksi_hutang = $transaksi_hutang->row();
    	$sisa = $hasil_transaksi_hutang->sisa;
        //echo $this->db->last_query();

        // if ($angsuran > $sisa) {
        //     echo '<div style="font-size:1.5em" class="alert alert-danger">angsuran lebih besar dari sisa.</div>';
        // } else {


    	$query = $this->db->get_where('transaksi_hutang', array('kode_transaksi' => $kode_transaksi));
    	$hasil_query= $query->row();
    	$cek_angsuran = $hasil_query->angsuran;
        // $cek_potongan_hutang = $hasil_query->potongan_hutang;
    	$cek_nominal = $hasil_query->nominal_hutang;

    	if($jenis_transaksi=='Lunas'){
    		$this->db->select('*');
    		$query_akun = $this->db->get_where('keuangan_sub_kategori_akun', array('kode_sub_kategori_akun' =>
    			'2.1.5'))->row();        
    	}else if ($jenis_transaksi=='Angsuran') {
    		$this->db->select('*');
    		$query_akun = $this->db->get_where('keuangan_sub_kategori_akun', array('kode_sub_kategori_akun' =>
    			'2.1.4'))->row();   
    	}

    	$kode_sub = $query_akun->kode_sub_kategori_akun;
    	$nama_sub = $query_akun->nama_sub_kategori_akun;
    	$kode_kategori = $query_akun->kode_kategori_akun;
    	$nama_kategori = $query_akun->nama_kategori_akun;
    	$kode_jenis = $query_akun->kode_jenis_akun;
    	$nama_jenis = $query_akun->nama_jenis_akun;

    	$get_id_petugas = $this->session->userdata('astrosession');
    	$id_petugas = $get_id_petugas->id;
    	$nama_petugas = $get_id_petugas->uname;

    	if (empty($cek_angsuran)) {

    		if($angsuran){

    			$opsi['kode_transaksi'] = $kode_transaksi;
    			$opsi['angsuran'] = $angsuran;
    			$opsi['tanggal_angsuran'] = date("Y-m-d");
    			$this->db->insert('opsi_hutang', $opsi);
    		}
    		$update_hutang['angsuran'] = $angsuran;
        // $update_hutang['potongan_hutang'] = $potongan_hutang;
    		if ($angsuran > $sisa) {
    			$update_hutang['sisa'] = '0';
    		}else{
            // $potongan = $cek_nominal - $potongan_hutang;

    			$update_hutang['sisa'] = @$cek_nominal - $angsuran;
    			if($jenis_transaksi=='Angsuran')
    			{
    				$update_hutang['tanggal_jatuh_tempo'] =$jatuh_tempo;
    			}
    		}
    		$sukses = $this->db->update('transaksi_hutang', $update_hutang, array('kode_transaksi' =>
    			$kode_transaksi));
     //echo $this->db->last_query();
    		if($angsuran){

    			$data_keu['petugas'] = $nama_petugas;
    			$data_keu['kode_referensi'] = $kode_transaksi;
    			$data_keu['tanggal_transaksi'] = date("Y-m-d");
    			$data_keu['keterangan'] = 'angsuran pembelian';
    			$data_keu['nominal'] = $angsuran;
    			$data_keu['kode_jenis_keuangan'] = $kode_jenis;
    			$data_keu['nama_jenis_keuangan'] = $nama_jenis;
    			$data_keu['kode_kategori_keuangan'] = $kode_kategori;
    			$data_keu['nama_kategori_keuangan'] = $nama_kategori;
    			$data_keu['kode_sub_kategori_keuangan'] = $kode_sub;
    			$data_keu['nama_sub_kategori_keuangan'] = $nama_sub;

    			$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
    		}
    	} else {
    		if($angsuran){

    			$opsi['kode_transaksi'] = $kode_transaksi;
    			$opsi['angsuran'] = $angsuran;
    			$opsi['tanggal_angsuran'] = date("Y-m-d");
    			$this->db->insert('opsi_hutang', $opsi);
    		}



    		$update_angsuran['angsuran'] = $cek_angsuran + $angsuran;
        // if ($potongan_hutang) {
        //     $update_angsuran['potongan_hutang'] = $cek_potongan_hutang + $potongan_hutang;
        // }
    		$this->db->update('transaksi_hutang', $update_angsuran, array('kode_transaksi' =>
    			$kode_transaksi));

        // $query_angsuran = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
        //     $kode_transaksi))->row()->angsuran;
    		$sisa_hutang = $this->db->get_where('transaksi_hutang', array('kode_transaksi' =>
    			$kode_transaksi))->row()->sisa;

    		if ($angsuran > $sisa){
    			$update_sisa['sisa'] = '0';
    		}else{
    			$update_sisa['sisa'] = $sisa_hutang - $angsuran;
    			if($jenis_transaksi=="Angsuran"){
    				$update_sisa['tanggal_jatuh_tempo'] =$jatuh_tempo;
    			}
    		}

    		$sukses = $this->db->update('transaksi_hutang', $update_sisa, array('kode_transaksi' =>
    			$kode_transaksi));
    		if ($angsuran) {

    			$data_keu['petugas'] = $nama_petugas;
    			$data_keu['kode_referensi'] = $kode_transaksi;
    			$data_keu['tanggal_transaksi'] = date("Y-m-d");
    			$data_keu['keterangan'] = 'angsuran pembelian';
    			$data_keu['nominal'] = $angsuran;
    			$data_keu['kode_jenis_keuangan'] = $kode_jenis;
    			$data_keu['nama_jenis_keuangan'] = $nama_jenis;
    			$data_keu['kode_kategori_keuangan'] = $kode_kategori;
    			$data_keu['nama_kategori_keuangan'] = $nama_kategori;
    			$data_keu['kode_sub_kategori_keuangan'] = $kode_sub;
    			$data_keu['nama_sub_kategori_keuangan'] = $nama_sub;

    			$keuangan = $this->db->insert("keuangan_keluar", $data_keu);
    		}

    	}


    	if ($sukses) {
    		echo '1';
    	}
        // }
    }


}
