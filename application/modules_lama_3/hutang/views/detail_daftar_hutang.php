
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Daftar Hutang
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row">

            <div class="col-md-5" id="">
              <div class="input-group">
                <span class="input-group-addon">Tanggal Awal</span>
                <input type="text" class="form-control tgl"  id="tgl_awal">
              </div>
            </div>
            <div class="col-md-5" id="">
              <div class="input-group">
                <span class="input-group-addon">Tanggal Akhir</span>
                <input type="text" class="form-control tgl" id="tgl_akhir">
              </div>
            </div>                        
            <div class="col-md-2 pull-left">
              <button style="width: 147px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
            </div>
          </div><br>
          <div id="cari_transaksi">
            <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">

              <?php 
              $kode = $this->uri->segment(3);
              $this->db->limit(50,0);
              $pembelian = $this->db->get_where('transaksi_hutang',array('kode_supplier'=>$kode));
              $hasil_transaksi = $pembelian->result();
              ?>
              <tr>
                <th>No</th>
                <th>Kode Transaksi</th>
                <th>Tanggal Transaksi</th>
                <th>Supplier</th>
                <th>Hutang</th>
                <th>Jatuh Tempo</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="scroll_data">
              <?php
              $nomor = 1;
              $total=0;
              foreach($hasil_transaksi as $daftar){ 
                $total +=$daftar->nominal_hutang;
                $tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : TanggalIndo(@$daftar->tanggal_transaksi) ;

                $tanggal=date("Y-m-d");
                if ($tanggal >= @$daftar->tanggal_jatuh_tempo) {
                  $alert ='danger';
                }else{
                  $alert = 'warning';
                }
                ?> 
                <tr class="<?php if(@$daftar->sisa > 0){ echo $alert; }?>">
                  <td><?php echo $nomor; ?></td>
                  <td><?php echo @$daftar->kode_transaksi; ?></td>
                  <td><?php echo $tgl; ?></td>
                  <td><?php echo @$daftar->nama_supplier; ?></td>
                  <td><?php echo format_rupiah(@$daftar->sisa); ?></td>
                  <td><?php if($daftar->tanggal_jatuh_tempo=='0000-00-00' || empty($daftar->tanggal_jatuh_tempo)){echo '-';}else{ echo @TanggalIndo(@$daftar->tanggal_jatuh_tempo); }?></td>
                  <td><?php echo cek_status_piutang($daftar->sisa); ?></td>
                </tr>
                <?php $nomor++; } ?>

              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kode Transaksi</th>
                  <th>Tanggal Transaksi</th>
                  <th>Supplier</th>
                  <th>Hutang</th>
                  <th>Jatuh Tempo</th>
                  <th>Status</th>
                </tr>
              </tfoot>
            </table>
            <?php 
            $get_jumlah = $this->db->get_where('transaksi_hutang',array('kode_supplier'=>$kode));
            $jumlah = $get_jumlah->num_rows();
            $jumlah = floor($jumlah/50);
            ?>
            <div class="text-center">
              <a id="ngeload" class="btn btn-success btn-lg ngeload"><i class="fa fa-refresh"></i> Load More</a>
            </div>
            <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
            <input type="hidden" class="form-control pagenum " value="0">
            <input type="hidden" class="form-control  " value="<?php echo $kode ?>" id="kode_supplier">

          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
</div>
</div>    
</div>  

<script type="text/javascript">
  $(document).ready(function() {
   $("#tabel_daftar").dataTable({
    "paging":   false,
    "ordering": false,
    "info":     false
  });   
 });
</script>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'hutang/'; ?>";
  });
</script>

<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});
  $('#cari').click(function(){
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    var kode_unit =$("#kode_unit").val();
    var kode_transaksi= "<?php echo $this->uri->segment(3); ?>";

    if (tgl_awal=='' || tgl_akhir==''){ 
      alert('Masukan Tanggal Awal & Tanggal Akhir..!')
    }
    else{
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'hutang/cari_hutang'; ?>",  
        cache :false,
        beforeSend:function(){
          $(".tunggu").show();  
        },  
        data : {tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,kode_unit:kode_unit,kode_transaksi:kode_transaksi},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
         $(".tunggu").hide(); 
         $("#cari_transaksi").html(data);
       },  
       error : function(data) {  
         // alert("das");  
       }  
     });
    }

    $('#tgl_awal').val('');
    $('#tgl_akhir').val('');

  });
</script>
<script>
  $(".ngeload").click(function(){
    if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
      var pagenum = parseInt($(".pagenum").val()) + 1;
      $(".pagenum").val(pagenum);
      load_table(pagenum);
    }
  })

  function load_table(page){
    var kode = $("#kode_supplier").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'hutang/get_daftar_hutang' ?>",
      data: ({  page:$(".pagenum").val(),kode:kode}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#scroll_data").append(msg);

      }
    });
  }
</script>




