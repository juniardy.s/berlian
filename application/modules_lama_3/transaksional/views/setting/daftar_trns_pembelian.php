
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="">
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            Widget settings form goes here
          </div>
          <div class="modal-footer">
            <button type="button" class="btn blue">Save changes</button>
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    <div class="theme-panel">
    </div>
            <!-- END STYLE CUSTOMIZER -->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
              Transaksional</h3>
              <div class="page-bar">
                <ul class="page-breadcrumb">
                  <li>
                    <i class="fa fa-home"></i>
                    <a href="#">Home</a>
                    <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                    <a href="#">Transaksional</a>
                    <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                    <a href="#">Riwayat Pembelian</a>
                  </li>
                </ul>
                <div class="page-toolbar">
                  <!-- <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range"> -->
                    <!-- <i class="icon-calendar"></i> -->
                    <!-- &nbsp;&nbsp; <i class="fa fa-angle-down"></i> -->
                  </div>
                </div>
              </div>
              <section class="col-lg-12 connectedSortable">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              Daftar Transaksi Kasir
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>

            </div>
          </div>
          <div class="portlet-body">
            <!------------------------------------------------------------------------------------------------------>


            <div class="box-body">            
              <div class="sukses"></div>
              <div class="loading" style="z-index:9999999999999999; background:rgba(255,255,255,0.8); width:100%; height:100%; position:fixed; top:0; left:0; text-align:center; padding-top:25%; display:none">
                <img src="http://localhost/berlian//public/img/loading.gif">
              </div>
              <form id="pencarian_form" method="post" style="margin-left: 18px;" class="form-horizontal" target="_blank">

                <div class="row">
                 <!--  <div class="col-md-4" id="trx_penjualan">
                    <div class="input-group">
                      <span class="input-group-addon">Tanggal Awal</span>
                      <input type="date" class="form-control tgl" id="tgl_awal">
                    </div>
                  </div>
                  <div class="col-md-4" id="trx_penjualan">
                    <div class="input-group">
                      <span class="input-group-addon">Tanggal Akhir</span>
                      <input type="date" class="form-control tgl" id="tgl_akhir">
                    </div>
                  </div> -->

                  <div class=" col-md-4">
                    <div class="input-group">
                      <!-- <button type="button" class="btn btn-success" onclick="cari_transaksi()"><i class="fa fa-search"></i> Cari</button> -->

                    </div>
                  </div>
                </div>

                <br>


              </form>
              <?php
                $kasir = $this->db->get('transaksi_pembelian');
                $hasil_kasir = $kasir->result();
              ?>
                <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Transaksi</th>
                      <th>Tanggal</th>
                      <th>Nomor Nota</th>
                      <th>Nama Supplier</th>
                      <th>Petugas</th>
                      <th>Grand Total </th>
                      <th>Dibayar</th>
                      <th>Metode Pembayaran</th>
                     
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $nomor=1;
                    foreach($hasil_kasir as $daftar){
                      ?>
                      <tr class="<?php if($daftar->status=="open"){ echo "danger"; }elseif ($daftar->status=="close" && $daftar->validasi=="") {
                        echo "warning";
                      } ?>">
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo $daftar->kode_transaksi; ?></td>
                      <td><?php echo TanggalIndo($daftar->tanggal_pembelian) ?></td>
                      <td><?php echo $daftar->nomor_nota ?></td>
                      <td><?php echo $daftar->kode_supplier . ' - '.$daftar->nama_supplier  ?></td>
                      <td><?php echo $daftar->petugas ?></td>
                      <td><?php echo format_rupiah($daftar->grand_total) ?></td>
                      <td><?php echo format_rupiah($daftar->dibayar) ?></td>
                      
                      <td><?php echo $daftar->proses_pembayaran ?></td>

                    </tr>
                    <?php $nomor++; } ?>
                  </tbody>
                </table>




              <!------------------------------------------------------------------------------------------------------>

            </div>
          </div>

          <!-- /.row (main row) -->
        </div></section>
<script type="text/javascript">
  
  $(document).ready(function(){
    $("#tabel_daftar").dataTable({
      scrollX: true,
    });

  //$(".tgl").datepicker();
})
</script>
  