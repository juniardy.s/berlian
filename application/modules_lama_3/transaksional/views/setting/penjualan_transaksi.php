<?php if ($tipe == 'mutasi'){ ?>
	
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Transaksi</th>
			<th>Kode Surat Jalan</th>
			<th>Jalur</th>
			<th>Keterangan</th>
			<th>Sales</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$no =1 ; 

			if (count($data) > 0) {
				foreach( $data as $val){ 
					if (!empty($val->kode_jalur)) {
						$jalur 	=	$this->db->get_where('master_jalur' , ['kode_jalur' => $val->kode_jalur , 'status' => 1] )->result_array();
					}else {
						$jalur[0]['nama_jalur'] = ' - ';
					}

					$sales 	=	$this->db->get_where('master_sales' , ['kode_sales' => $val->kode_unit_tujuan ] )->result_array();  

			
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo TanggalIndo($val->tanggal_transaksi) ?></td>
			<td><?php echo $val->kode_surat_jalan ?></td>
			<td><?php echo $jalur[0]['nama_jalur'] ?></td>
			<td><?php echo $val->keterangan ?></td>
			<td><?php echo $val->nama_unit_tujuan//$sales[0]['nama_sales'] ?></td>
			<td>
				<button class="btn btn-sm btn-info" onclick="lihat_mutasi('<?= $val->kode_surat_jalan ?>')">
					<i class="fa fa-search"></i> Detail
				</button>
			</td>
		</tr>
		<?php }  } else { ?>

			<tr>
				<td colspan="6"> Tidak Ada Data </td>
			</tr>

		<?php } ?>
	</tbody>


<?php } else { ?>
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Transaksi</th>
			<th>Kode Transaksi</th>
			<th>Nama Member</th>
			<th>Jalur</th>
			<th>Sales</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$nox =1 ; 
			if (count($data) > 0) {
				
			
			foreach( $data as $val){ 
				

				$jalur 	=	$this->db->get_where('master_member' , ['kode_member' => $val->kode_member ] )->result_array();

			
		?>
		<tr>
			<td><?php echo $nox++ ?></td>
			<td><?php echo TanggalIndo($val->tanggal_penjualan) ?></td>
			<td><?php echo $val->kode_penjualan ?></td>
			<td><?php echo $val->nama_member ?></td>
			<td><?php echo $jalur[0]['nama_jalur'] ?></td>
			<td><?php echo $val->nama_petugas  ?></td>
			<td>
				<button class="btn btn-sm btn-info" onclick="lihat_mutasi('<?= $val->kode_penjualan ?>')">
					<i class="fa fa-search"></i> Detail
				</button>
			</td>
		</tr>

		<?php } }  else {?>

			<tr>
				<td colspan="5"> Tidak ada Data</td>
			</tr>
		<?php } ?>
	</tbody>
<?php } ?>