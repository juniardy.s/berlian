<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komisi extends MY_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
*      http://example.com/index.php/welcome
*  - or -
*      http://example.com/index.php/welcome/index
*  - or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see http://codeigniter.com/user_guide/general/urls.html
*/

public function __construct()
{
    parent::__construct();      
    if ($this->session->userdata('astrosession') == FALSE) {
        redirect(base_url('authenticate'));         
    }
    $this->load->library('form_validation');
    $this->load->model('m_komisi');
}

public function index()
{
	$this->daftar_komisi();
}

public function daftar_komisi()
{
	$get_sales = $this->db->select('kode_sales kode, nama_sales nama')
							->where('status', 1)
							->get('master_sales')
							->result();
	$get_sopir = $this->db->select('kode_sopir kode, nama_sopir nama')
							->where('status', 1)
							->get('master_sopir')
							->result();
	$data['daftar_orang'] = array_merge($get_sales, $get_sopir);
	$data['m_komisi'] = $this->m_komisi;

    $data['aktif'] = 'setting';
    $data['konten'] = $this->load->view('daftar_komisi', $data, TRUE);
    $data['halaman'] = $this->load->view('menu', $data, TRUE);
    $this->load->view('main', $data);     
}

public function detail($kode = null)
{
	if (empty($kode)) redirect('komisi');

	$tipe = substr($kode, 0, 3);
	if ($tipe == "SA_") {
		$get_petugas = $this->db->select('*, kode_sales kode, nama_sales nama')
								->get_where('master_sales', array('kode_sales' => $kode, 'status' => 1))
								->row();
		if(!$get_petugas) redirect('komisi');
	} else if ($tipe == "SO_") {
		$get_petugas = $this->db->select('*, kode_sopir kode, nama_sopir nama')
								->get_where('master_sopir', array('kode_sopir' => $kode, 'status' => 1))
								->row();
		if(!$get_petugas) redirect('komisi');
	} else {
		exit();
	}

	$get_riwayat_komisi = $this->db->where(array('kode_penerima_komisi' => $kode))
									->order_by('tanggal_komisi', 'DESC')
									->get('transaksi_komisi')
									->result();

	if (!$get_riwayat_komisi) $get_riwayat_komisi = array();

	$data['riwayat_komisi'] = $get_riwayat_komisi;
	$data['petugas'] = $get_petugas;
	$data['m_komisi'] = $this->m_komisi;

    $data['konten'] = $this->load->view('detail_komisi', $data, TRUE);
    $data['halaman'] = $this->load->view('menu', $data, TRUE);
    $this->load->view('main', $data);  
}

public function get_detail_riwayat_komisi($id = null)
{
	if (empty($id)) exit();

	$trans_komisi = $this->db->get_where('transaksi_komisi', array('id' => $id))->row();

	if (!$trans_komisi) exit();

	$get_detail = $this->db->get_where('opsi_transaksi_komisi', array('id_transaksi_komisi' => $id))->result();

	if (!$get_detail) exit();

		?>
			<div class="row" >
                <form>
                  <div class="form-group  col-xs-3">
                    <label class="gedhi">Tanggal</label>
                    <input readonly="" type="text" class="form-control" value="<?php echo TanggalIndo(substr($trans_komisi->tanggal_komisi, 0, 10)) ?>"/>
                  </div>
                  <div class="form-group  col-xs-3">
                    <label class="gedhi">Jumlah</label>
                    <input readonly="" type="text" class="form-control" value="<?php echo $trans_komisi->total_barang_komisi ?>"/>
                  </div>
                   <div class="form-group  col-xs-6">
                    <label class="gedhi">Komisi</label>
                    <input readonly="" type="text" class="form-control" value="<?php echo format_rupiah($trans_komisi->total_komisi) ?>"/>
                  </div>
                </form>
              </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Kode Bahan</th>
                      <th>Nama Bahan</th>
                      <th>Jumlah Bahan</th>
                      <th>Komisi per Bahan</th>
                      <th>Total Komisi</th>
                    </tr>
                  </thead>
                  <tbody>
				<?php foreach ($get_detail as $item) { ?>
                    <tr>
                      <td><?php echo $item->kode_bahan ?></td>
                      <td><?php echo $item->nama_bahan ?></td>
                      <td><?php echo $item->jumlah ?></td>
                      <td><?php echo format_rupiah($item->komisi_bahan) ?></td>
                      <td><?php echo format_rupiah($item->total_komisi) ?></td>
                    </tr>
				<?php } ?>
                  </tbody>
                </table>
		<?php
}

}