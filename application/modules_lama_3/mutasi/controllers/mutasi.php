<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi extends MY_Controller
{
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('astrosession') == FALSE) {
            redirect(base_url('authenticate'));
        }
        $this->load->library('form_validation');
    }
    
    //------------------------------------------ view ----------------- --------------------//
    public function cari_mutasi()
    {
        $this->load->view('setting/cari_mutasi');
        
    }
    public function surat_jalan()
    {
        $this->load->view('setting/surat_jalan');
    }
    public function tabel_item_mutasi_temp($kode)
    {
        $data['aktif'] = 'stok';
        $data['kode']  = $kode;
        $this->load->view('mutasi/setting/tabel_item_mutasi_temp', $data);
    }
    public function tabel_item_temp_petugas($kode)
    {
        $data['aktif'] = 'stok';
        $data['kode']  = $kode;
        $this->load->view('mutasi/setting/tabel_item_temp_petugas', $data);
    }
    
    public function daftar_mutasi()
    {
        $data['aktif']   = 'setting';
        $data['konten']  = $this->load->view('setting/daftar_mutasi', $data, TRUE);
        $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
        $this->load->view('main', $data);
        
        // $data['aktif'] = 'stok';
        // $data['konten'] = $this->load->view('mutasi/setting/daftar_mutasi', NULL, TRUE);
        // $this->load->view ('main', $data);      
    }
    
    
    public function tambah_mutasi()
    {
        
        $data['aktif']   = 'mutasi';
        $data['konten']  = $this->load->view('setting/mutasi', $data, TRUE);
        $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
        $this->load->view('main', $data);
    }
    
    public function tambah_mutasi_gudang()
    {
        
        $data['aktif']   = 'mutasi';
        $data['konten']  = $this->load->view('setting/mutasi_gudang', $data, TRUE);
        $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
        $this->load->view('main', $data);
    }
    
    public function detail_mutasi()
    {
        
        $data['aktif']   = 'mutasi';
        $data['konten']  = $this->load->view('setting/detail_mutasi', $data, TRUE);
        $data['halaman'] = $this->load->view('setting/menu', $data, TRUE);
        $this->load->view('main', $data);
    }
    
    
    //------------------------------------------ PROSES ----------------- --------------------//
    
    public function get_rak_unit_awal()
    {
        $param = $this->input->post();
        $unit  = $param['unit_awal'];
        
        if ($unit) {
            $opt   = '';
            $query = $this->db->get_where('master_rak', array(
                'kode_unit' => $unit,
                'status' => '1'
            ));
            $opt   = '<option value="">--Pilih Unit--</option>';
            foreach ($query->result() as $key => $value) {
                $opt .= '<option value="' . $value->kode_rak . '">' . $value->nama_rak . '</option>';
            }
            $nama      = $this->db->get_where('master_unit', array(
                'kode_unit' => $unit
            ));
            $nama_unit = $nama->row();
            $nama_unit = $nama_unit->nama_unit;
            
            echo $opt . '|' . $nama_unit;
        }
    }
    
    public function get_rak_unit_akhir()
    {
        $param = $this->input->post();
        $unit  = $param['unit_akhir'];
        
        if ($unit) {
            $opt   = '';
            $query = $this->db->get_where('master_rak', array(
                'kode_unit' => $unit
            ));
            $opt   = '<option value="">--Pilih Unit--</option>';
            foreach ($query->result() as $key => $value) {
                $opt .= '<option value="' . $value->kode_rak . '">' . $value->nama_rak . '</option>';
            }
            $nama      = $this->db->get_where('master_unit', array(
                'kode_unit' => $unit
            ));
            $nama_unit = $nama->row();
            $nama_unit = $nama_unit->nama_unit;
            
            echo $opt . '|' . $nama_unit;
        }
    }
    
    public function get_nama_rak_awal()
    {
        $param    = $this->input->post();
        $rak_awal = $param['rak_awal'];
        
        if ($rak_awal) {
            $nama     = $this->db->get_where('master_rak', array(
                'kode_rak' => $rak_awal
            ));
            $nama_rak = $nama->row();
            $nama_rak = $nama_rak->nama_rak;
            echo $nama_rak;
        }
    }
    
    public function get_nama_rak_akhir()
    {
        $param     = $this->input->post();
        $rak_akhir = $param['rak_akhir'];
        
        if ($rak_akhir) {
            $nama     = $this->db->get_where('master_rak', array(
                'kode_rak' => $rak_akhir
            ));
            $nama_rak = $nama->row();
            $nama_rak = $nama_rak->nama_rak;
            echo $nama_rak;
        }
    }
    
    public function get_bahan()
    {
        $param          = $this->input->post();
        $jenis          = $param['jenis_bahan'];
        $kode_unit_asal = $param['kode_unit_asal'];
        $kode_rak_asal  = $param['kode_rak_asal'];
        
        if ($jenis == 'bahan baku') {
            $opt   = '';
            $query = $this->db->get_where('master_bahan_baku', array(
                'kode_unit' => $kode_unit_asal,
                'kode_rak' => $kode_rak_asal
            ));
            $opt   = '<option value="">--Pilih Bahan Baku--</option>';
            foreach ($query->result() as $key => $value) {
                $opt .= '<option value="' . $value->kode_bahan_baku . '">' . $value->nama_bahan_baku . '</option>';
            }
            echo $opt;
            
        } else if ($jenis == 'bahan jadi') {
            $opt   = '';
            $query = $this->db->get_where('master_bahan_jadi', array(
                'kode_unit' => $kode_unit_asal,
                'kode_rak' => $kode_rak_asal
            ));
            $opt   = '<option value="">--Pilih Bahan Jadi--</option>';
            foreach ($query->result() as $key => $value) {
                $opt .= '<option value="' . $value->kode_bahan_jadi . '">' . $value->nama_bahan_jadi . '</option>';
            }
            echo $opt;
        }
    }
    
    public function get_satuan()
    {
        
        $kode_bahan  = $this->input->post('kode_bahan');
        $nama_bahan  = $this->db->get_where('master_bahan_jadi', array(
            'kode_bahan_jadi' => $kode_bahan
        ));
        $hasil_bahan = $nama_bahan->row();
        
        echo json_encode($hasil_bahan);
    }
    
    public function simpan_item_mutasi_temp()
    {
        $masukan['kode_mutasi']  = $this->input->post('kode_mutasi');
        $masukan['kode_bahan']   = $this->input->post('kode_bahan');
        $masukan['jumlah']       = $this->input->post('jumlah');
        $masukan['jenis_mutasi'] = $this->input->post('jenis_mutasi');
        $jenis_mutasi            = $this->input->post('jenis_mutasi');
        $kategori_petugas        = $this->input->post('kategori_petugas');
        $kode_petugas            = $this->input->post('kode_petugas');
        if ($jenis_mutasi == 'ke_petugas') {
            $this->db->where('status', 'sendiri');
        } elseif ($jenis_mutasi = 'ke_gudang') {
            
            if (@$kategori_petugas == 'sopir') {
                $this->db->where('status', 'sopir');
                $this->db->where('kode_member', @$kode_petugas);
            } else {
                $this->db->where('status', 'sales');
                $this->db->where('kode_member', @$kode_petugas);
            }
        }
        $query = $this->db->get_where('master_bahan_jadi', array(
            'kode_bahan_jadi' => $masukan['kode_bahan']
        ));
        $cek   = $query->row()->real_stock;
        //echo $this->db->last_query();
        if ($cek < $masukan['jumlah']) {
            echo '0|<div class="alert alert-danger">Jumlah yang dimasukkan melebihi stok.</div>';
        } else {
            $masukan['kode_mutasi'] = $this->input->post('kode_mutasi');
            $jenis_mutasi           = $this->input->post('jenis_mutasi');
            if ($jenis_mutasi == 'ke_petugas') {
                $this->db->where('status', 'sendiri');
            } elseif ($jenis_mutasi = 'ke_gudang') {
                $this->db->where('status', 'sales');
            }
            $get_bahan             = $this->db->get_where('master_bahan_jadi', array(
                'kode_bahan_jadi' => $masukan['kode_bahan']
            ));
            $hasil_get_bahan       = $get_bahan->row();
            $masukan['nama_bahan'] = $hasil_get_bahan->nama_bahan_jadi;
            
            $masukan['kode_unit'] = $hasil_get_bahan->kode_rak;
            $masukan['nama_unit'] = $hasil_get_bahan->nama_rak;
            
            $query = $this->db->query("SELECT * from opsi_transaksi_mutasi_temp where kode_mutasi='$masukan[kode_mutasi]'  AND kode_bahan='$masukan[kode_bahan]'");
            $hasil = $query->row();
            
            $query_rak            = $this->db->query("SELECT * from master_rak where kode_rak='$masukan[kode_unit]' ");
            $hasil_rak            = $query_rak->row();
            $masukan['nama_unit'] = @$hasil_rak->nama_rak;
            $cek                  = count($hasil);
            
            
            
            if ($cek >= 1) {
                $masukan['jumlah'] = $hasil->jumlah + $masukan['jumlah'];
                $this->db->update('opsi_transaksi_mutasi_temp', $masukan, array(
                    'kode_mutasi' => $masukan['kode_mutasi'],
                    'kode_bahan' => $masukan['kode_bahan']
                ));
            } else {
                $input = $this->db->insert('opsi_transaksi_mutasi_temp', $masukan);
                
            }
            echo "1|";
            
        }
    }
    
    public function get_temp_mutasi()
    {
        $id           = $this->input->post('id');
        $mutasi       = $this->db->get_where('opsi_transaksi_mutasi_temp', array(
            'id' => $id
        ));
        $hasil_mutasi = $mutasi->row();
        echo json_encode($hasil_mutasi);
    }
    
    public function ubah_item_mutasi_temp()
    {
        $id                      = $this->input->post('id');
        $masukan['kode_mutasi']  = $this->input->post('kode_mutasi');
        $masukan['kode_bahan']   = $this->input->post('kode_bahan');
        $masukan['jenis_mutasi'] = $this->input->post('jenis_mutasi');
        $jenis_mutasi            = $this->input->post('jenis_mutasi');
        $kategori_petugas        = $this->input->post('kategori_petugas');
        $kode_petugas            = $this->input->post('kode_petugas');
        if ($jenis_mutasi == 'ke_petugas') {
            $this->db->where('status', 'sendiri');
        } elseif ($jenis_mutasi = 'ke_gudang') {
            if (@$kategori_petugas == 'sopir') {
                $this->db->where('status', 'sopir');
                $this->db->where('kode_member', @$kode_petugas);
            } else {
                $this->db->where('status', 'sales');
                $this->db->where('kode_member', @$kode_petugas);
            }
        }
        
        $query = $this->db->get_where('master_bahan_jadi', array(
            'kode_bahan_jadi' => $masukan['kode_bahan']
        ));
        $cek   = $query->row()->real_stock;
        
        $masukan['jumlah'] = $this->input->post('jumlah');
        if ($cek < $masukan['jumlah']) {
            echo '0|<div class="alert alert-danger">Jumlah yang dimasukkan melebihi stok.</div>';
        } else {
            if ($jenis_mutasi == 'ke_petugas') {
                $this->db->where('status', 'sendiri');
            } elseif ($jenis_mutasi = 'ke_gudang') {
                $this->db->where('status', 'sales');
            }
            $get_bahan             = $this->db->get_where('master_bahan_jadi', array(
                'kode_bahan_jadi' => $masukan['kode_bahan']
            ));
            $hasil_get_bahan       = $get_bahan->row();
            $masukan['nama_bahan'] = $hasil_get_bahan->nama_bahan_jadi;
            $masukan['jumlah']     = $this->input->post('jumlah');
            $input                 = $this->db->update('opsi_transaksi_mutasi_temp', $masukan, array(
                'id' => $id
            ));
            echo "1|";
        }
    }
    
    public function simpan_mutasi()
    {
        $input       = $this->input->post();
        $kode_mutasi = $input['kode_mutasi'];
        $this->db->insert('transaksi_komisi', array(
            'tanggal_komisi' => date('Y-m-d H:i:s')
        ));
        $id_transaksi_komisi = $this->db->insert_id();
        
        $kode_rak = @$input['kode_rak'];
        
        $this->db->where('kode_rak', $kode_rak);
        $get_rak   = $this->db->get('master_rak');
        $hasil_rak = $get_rak->row();
        
        $get_id_petugas = $this->session->userdata('astrosession');
        $id_petugas     = $get_id_petugas->id;
        $nama_petugas   = $get_id_petugas->uname;
        
        $this->db->select('*');
        if ($input['posisi_awal'] == "Petugas") {
            $input['status_mutasi'] = 'Mutasi Ke Gudang';
            $query_mutasi_temp      = $this->db->get_where('opsi_transaksi_mutasi', array(
                'kode_surat_jalan' => $input['kode_surat_jalan'],
                'status_mutasi' => 'Mutasi Ke Petugas'
            ));
        } else {
            $input['status_mutasi'] = 'Mutasi Ke Petugas';
            $query_mutasi_temp      = $this->db->get_where('opsi_transaksi_mutasi_temp', array(
                'kode_mutasi' => $kode_mutasi
            ));
        }
        $hasil_opsi = $query_mutasi_temp->result();
        
        $total        = 0;
        $total_komisi = 0;
        $jml_opsi     = 0;
        foreach ($hasil_opsi as $item) {
            $jml_mutasi = @$input['jml_mutasi'][$jml_opsi];
            $id         = @$input['id'][$jml_opsi];
            $jml_opsi++;
            $data_opsi['kode_mutasi'] = $kode_mutasi;
            
            $data_opsi['kode_bahan'] = $item->kode_bahan;
            $data_opsi['nama_bahan'] = $item->nama_bahan;
            if ($input['posisi_awal'] == "Petugas") {
                $data_opsi['jumlah'] = $jml_mutasi;
            } else {
                $data_opsi['jumlah'] = $item->jumlah;
            }
            
            $total += $data_opsi['jumlah'];
            
            $data_opsi['kode_unit']        = $item->kode_unit;
            $data_opsi['nama_unit']        = $item->nama_unit;
            $data_opsi['status_mutasi']    = $input['status_mutasi'];
            $data_opsi['kode_surat_jalan'] = $input['kode_surat_jalan'];
            $kode_rak                      = $item->kode_unit;
            $nama_rak                      = $item->nama_unit;
            $tabel_opsi_transaksi_mutasi   = $this->db->insert("opsi_transaksi_mutasi", $data_opsi);
            
            $nama_bahan  = $item->nama_bahan;
            $stok_mutasi = $item->jumlah;
            
            $kode_bahan = $item->kode_bahan;
            
            
            if ($input['posisi_awal'] == "Petugas") {
                
                $query_mutasi = $this->db->get_where('opsi_transaksi_mutasi', array(
                    'kode_surat_jalan' => $input['kode_surat_jalan'],
                    'kode_bahan' => $kode_bahan,
                    'id' => $id
                ));
                $opsi_mutasi  = $query_mutasi->row();
                
                // $stok_jual['jumlah'] = $stok_mutasi - $jml_mutasi;
                // $this->db->update('opsi_transaksi_mutasi',$stok_jual,array('kode_surat_jalan'=>$input['kode_surat_jalan'],'kode_bahan'=>$kode_bahan,'id'=>$id));
                
                $opsi_komisi['id_transaksi_komisi'] = $id_transaksi_komisi;
                $opsi_komisi['kode_bahan']          = $kode_bahan;
                $opsi_komisi['nama_bahan']          = $nama_bahan;
                $opsi_komisi['jumlah']              = $stok_mutasi - $jml_mutasi;
                $opsi_komisi['kode_unit']           = $item->kode_unit;
                $opsi_komisi['nama_unit']           = $item->nama_unit;
                $this->db->insert('opsi_transaksi_komisi', $opsi_komisi);
                $opsi_komisi_id = $this->db->insert_id();
            }
            
            if ($input['kategori_petugas'] == 'sales' or $input['kategori_petugas'] == 'sales & sopir') {
                $status = 'sales';
            } else {
                $status = 'sopir';
            }
            if ($input['posisi_awal'] == "Gudang") {
                
                
                $cek_bahan       = $this->db->get_where('master_bahan_jadi', array(
                    'kode_bahan_jadi' => $kode_bahan,
                    'kode_rak' => $kode_rak,
                    'kode_member' => $input['kode_petugas'],
                    'status' => $status
                ));
                $hasil_cek_bahan = $cek_bahan->num_rows();
                
                $get_bahan       = $this->db->get_where('master_bahan_jadi', array(
                    'kode_bahan_jadi' => $kode_bahan,
                    'kode_rak' => $kode_rak,
                    'status' => 'sendiri'
                ));
                $hasil_get_bahan = $get_bahan->row();
                
                if ($hasil_cek_bahan == 0) {
                    
                    $hasil_bahan['kode_bahan_jadi'] = $hasil_get_bahan->kode_bahan_jadi;
                    $hasil_bahan['nama_bahan_jadi'] = $hasil_get_bahan->nama_bahan_jadi;
                    $hasil_bahan['jenis_bahan']     = $hasil_get_bahan->jenis_bahan;
                    $hasil_bahan['kode_unit']       = $hasil_get_bahan->kode_unit;
                    $hasil_bahan['nama_unit']       = $hasil_get_bahan->nama_unit;
                    $hasil_bahan['kode_rak']        = $hasil_get_bahan->kode_rak;
                    $hasil_bahan['nama_rak']        = $hasil_get_bahan->nama_rak;
                    $hasil_bahan['id_satuan_stok']  = $hasil_get_bahan->id_satuan_stok;
                    $hasil_bahan['satuan_stok']     = $hasil_get_bahan->satuan_stok;
                    $hasil_bahan['stok_minimal']    = $hasil_get_bahan->stok_minimal;
                    $hasil_bahan['real_stock']      = $item->jumlah;
                    $hasil_bahan['hpp']             = $hasil_get_bahan->hpp;
                    $hasil_bahan['harga_jual']      = $hasil_get_bahan->harga_jual;
                    $hasil_bahan['status']          = $status;
                    $hasil_bahan['kode_member']     = $input['kode_petugas'];
                    $hasil_bahan['nama_member']     = $input['nama_petugas'];
                    $hasil_bahan['tarif_borongan']  = $hasil_get_bahan->tarif_borongan;
                    $hasil_bahan['komisi_supir']    = $hasil_get_bahan->komisi_supir;
                    $hasil_bahan['sales_komisi']    = $hasil_get_bahan->sales_komisi;
                    
                    $this->db->insert('master_bahan_jadi', $hasil_bahan);
                    
                    
                    $kurang_stok['real_stock'] = $hasil_get_bahan->real_stock - $stok_mutasi;
                    $this->db->update('master_bahan_jadi', $kurang_stok, array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'status' => 'sendiri'
                    ));
                    
                } else if ($hasil_cek_bahan > 0) {
                    $get_bahan_jadi       = $this->db->get_where('master_bahan_jadi', array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'kode_member' => $input['kode_petugas'],
                        'status' => $status
                    ));
                    $hasil_get_bahan_jadi = $get_bahan_jadi->row();
                    
                    $tambah_stok['real_stock'] = $hasil_get_bahan_jadi->real_stock + $stok_mutasi;
                    $this->db->update('master_bahan_jadi', $tambah_stok, array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'kode_member' => $input['kode_petugas'],
                        'status' => $status
                    ));
                    
                    $kurang_stok['real_stock'] = $hasil_get_bahan->real_stock - $stok_mutasi;
                    $this->db->update('master_bahan_jadi', $kurang_stok, array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'status' => 'sendiri'
                    ));
                    
                }
                
                $stok['jenis_transaksi']   = 'mutasi';
                $stok['kode_transaksi']    = $kode_mutasi;
                $stok['kategori_bahan']    = 'Bahan Jadi';
                $stok['kode_bahan']        = $kode_bahan;
                $stok['nama_bahan']        = $nama_bahan;
                $stok['stok_keluar']       = $stok_mutasi;
                $stok['stok_masuk']        = '';
                $stok['posisi_awal']       = $input['posisi_awal'];
                $stok['kode_unit_asal']    = $hasil_get_bahan->kode_unit;
                $stok['nama_unit_asal']    = $hasil_get_bahan->nama_unit;
                $stok['kode_rak_asal']     = $kode_rak;
                $stok['nama_rak_asal']     = $hasil_get_bahan->nama_rak;
                $stok['posisi_akhir']      = 'Petugas';
                $stok['kode_unit_tujuan']  = $input['kode_petugas'];
                $stok['nama_unit_tujuan']  = $input['nama_petugas'];
                $stok['kode_rak_tujuan']   = $kode_rak;
                $stok['nama_rak_tujuan']   = $hasil_get_bahan->nama_rak;
                $stok['id_petugas']        = $id_petugas;
                $stok['nama_petugas']      = $nama_petugas;
                $stok['tanggal_transaksi'] = date("Y-m-d");
                $stok['status']            = 'keluar';
                $stok['kode_jalur']        = $input['kode_jalur'];
                
                $transaksi_stok = $this->db->insert("transaksi_stok", $stok);
                
                $stok['kode_bahan']  = @$kode_bahan;
                $stok['stok_keluar'] = '';
                $stok['stok_masuk']  = $stok_mutasi;
                $stok['status']      = 'masuk';
                
                $transaksi_stok = $this->db->insert("transaksi_stok", $stok);
            } else {
                $kode_bahan_ori  = substr($kode_bahan, 2);
                $get_bahan       = $this->db->get_where('master_bahan_jadi', array(
                    'kode_bahan_jadi' => $kode_bahan,
                    'kode_rak' => $kode_rak,
                    'kode_member' => $input['kode_petugas'],
                    'status' => $status
                ));
                $hasil_get_bahan = $get_bahan->row();
                
                $cek_bahan       = $this->db->get_where('master_bahan_jadi', array(
                    'kode_bahan_jadi' => $kode_bahan,
                    'kode_rak' => $kode_rak,
                    'status' => 'sendiri'
                ));
                $hasil_cek_bahan = $cek_bahan->row();
                
                
                $hasil_cek_bahan = $cek_bahan->num_rows();
                if ($hasil_cek_bahan == 0) {
                    
                    $hasil_bahan['kode_bahan_jadi'] = $hasil_get_bahan->kode_bahan_jadi;
                    $hasil_bahan['nama_bahan_jadi'] = $hasil_get_bahan->nama_bahan_jadi;
                    $hasil_bahan['jenis_bahan']     = $hasil_get_bahan->jenis_bahan;
                    $hasil_bahan['kode_unit']       = $hasil_get_bahan->kode_unit;
                    $hasil_bahan['nama_unit']       = $hasil_get_bahan->nama_unit;
                    $hasil_bahan['kode_rak']        = $hasil_get_bahan->kode_rak;
                    $hasil_bahan['nama_rak']        = $hasil_get_bahan->nama_rak;
                    $hasil_bahan['id_satuan_stok']  = $hasil_get_bahan->id_satuan_stok;
                    $hasil_bahan['satuan_stok']     = $hasil_get_bahan->satuan_stok;
                    $hasil_bahan['stok_minimal']    = $hasil_get_bahan->stok_minimal;
                    $hasil_bahan['real_stock']      = $item->jumlah - $jml_mutasi;
                    $hasil_bahan['hpp']             = $hasil_get_bahan->hpp;
                    $hasil_bahan['harga_jual']      = $hasil_get_bahan->harga_jual;
                    $hasil_bahan['status']          = 'sendiri';
                    $hasil_bahan['kode_member']     = $input['kode_petugas'];
                    $hasil_bahan['nama_member']     = $input['nama_petugas'];
                    $hasil_bahan['tarif_borongan']  = $hasil_get_bahan->tarif_borongan;
                    $hasil_bahan['komisi_supir']    = $hasil_get_bahan->komisi_supir;
                    $hasil_bahan['sales_komisi']    = $hasil_get_bahan->sales_komisi;
                    
                    $this->db->insert('master_bahan_jadi', $hasil_bahan);
                    
                    $hasil_bahan               = $get_bahan->row();
                    $kurang_stok['real_stock'] = $hasil_bahan->real_stock - $jml_mutasi;
                    $this->db->update('master_bahan_jadi', $kurang_stok, array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'kode_member' => $input['kode_petugas'],
                        'status' => $status
                    ));
                    
                    unset($opsi_komisi);
                    $opsi_komisi['komisi_bahan'] = $hasil_get_bahan->sales_komisi;
                    $opsi_komisi['total_komisi'] = $hasil_get_bahan->sales_komisi * ($stok_mutasi - $jml_mutasi);
                    $this->db->update('opsi_transaksi_komisi', $opsi_komisi, array(
                        'id' => $opsi_komisi_id
                    ));
                    $total_komisi += $opsi_komisi['total_komisi'];
                    
                } else if ($hasil_cek_bahan > 0) {
                    $cek_bahan       = $this->db->get_where('master_bahan_jadi', array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'status' => 'sendiri'
                    ));
                    $hasil_cek_bahan = $cek_bahan->row();
                    
                    $tambah_stok['real_stock'] = $hasil_cek_bahan->real_stock + $jml_mutasi;
                    $this->db->update('master_bahan_jadi', $tambah_stok, array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'status' => 'sendiri'
                    ));
                    
                    $hasil_bahan               = $get_bahan->row();
                    $kurang_stok['real_stock'] = $hasil_bahan->real_stock - $jml_mutasi;
                    $this->db->update('master_bahan_jadi', $kurang_stok, array(
                        'kode_bahan_jadi' => $kode_bahan,
                        'kode_rak' => $kode_rak,
                        'kode_member' => $input['kode_petugas'],
                        'status' => $status
                    ));
                    
                    unset($opsi_komisi);
                    $opsi_komisi['komisi_bahan'] = $hasil_get_bahan->sales_komisi;
                    $opsi_komisi['total_komisi'] = $hasil_get_bahan->sales_komisi * ($stok_mutasi - $jml_mutasi);
                    $this->db->update('opsi_transaksi_komisi', $opsi_komisi, array(
                        'id' => $opsi_komisi_id
                    ));
                    $total_komisi += $opsi_komisi['total_komisi'];
                    
                }
                
                $stok['jenis_transaksi']   = 'mutasi';
                $stok['kode_transaksi']    = $kode_mutasi;
                $stok['kategori_bahan']    = 'Bahan Jadi';
                $stok['kode_bahan']        = $kode_bahan;
                $stok['nama_bahan']        = $nama_bahan;
                $stok['stok_keluar']       = $jml_mutasi;
                $stok['stok_masuk']        = '';
                $stok['posisi_awal']       = $input['posisi_awal'];
                $stok['kode_unit_asal']    = $input['kode_petugas'];
                $stok['nama_unit_asal']    = $input['nama_petugas'];
                $stok['kode_rak_asal']     = $hasil_get_bahan->kode_rak;
                $stok['nama_rak_asal']     = $hasil_get_bahan->nama_rak;
                $stok['posisi_akhir']      = 'Gudang';
                $stok['kode_unit_tujuan']  = $hasil_get_bahan->kode_unit;
                $stok['nama_unit_tujuan']  = $hasil_get_bahan->nama_unit;
                $stok['kode_rak_tujuan']   = $hasil_get_bahan->kode_rak;
                $stok['nama_rak_tujuan']   = $hasil_get_bahan->nama_rak;
                $stok['id_petugas']        = $id_petugas;
                $stok['nama_petugas']      = $nama_petugas;
                $stok['tanggal_transaksi'] = date("Y-m-d");
                $stok['status']            = 'keluar';
                
                
                $transaksi_stok = $this->db->insert("transaksi_stok", $stok);
                
                $stok['kode_bahan']  = @$kode_bahan;
                $stok['stok_keluar'] = '';
                $stok['stok_masuk']  = $jml_mutasi;
                $stok['status']      = 'masuk';
                
                $transaksi_stok = $this->db->insert("transaksi_stok", $stok);
            }
        }
        
        if (@$transaksi_stok) {
            if ($input['posisi_awal'] == "Gudang") {
                $input['kode_unit_asal']   = $hasil_get_bahan->kode_unit;
                $input['nama_unit_asal']   = 'Gudang';
                $input['kode_unit_tujuan'] = $input['kode_petugas'];
                $input['nama_unit_tujuan'] = $input['nama_petugas'];
            } else {
                $input['kode_unit_asal']   = $input['kode_petugas'];
                $input['nama_unit_asal']   = $input['nama_petugas'];
                $input['kode_unit_tujuan'] = $hasil_get_bahan->kode_unit;
                $input['nama_unit_tujuan'] = 'Gudang';
            }
            $input['kode_rak_asal']   = $hasil_get_bahan->kode_rak;
            $input['kode_rak_tujuan'] = $hasil_get_bahan->kode_rak;
            $input['nama_rak_asal']   = $hasil_get_bahan->nama_rak;
            $input['nama_rak_tujuan'] = $hasil_get_bahan->nama_rak;
            $inp                      = $input;
            unset($input['posisi_awal']);
            unset($input['jenis_bahan']);
            unset($input['unit_asal']);
            unset($kode_rak);
            unset($input['kode_petugas']);
            unset($input['nama_petugas']);
            unset($input['kode_gudang']);
            unset($input['kategori_bahan']);
            unset($input['kode_bahan']);
            unset($input['nama_bahan']);
            unset($input['jumlah']);
            unset($input['id_item_temp']);
            unset($input['surat_jalan']);
            unset($input['id']);
            unset($input['jumlah']);
            unset($input['jml_mutasi']);
            //unset($input['kategori_petugas']);
            
            $input['tanggal_update']    = date("Y-m-d");
            $input['tanggal_transaksi'] = date("Y-m-d");
            $input['petugas']           = $nama_petugas;
            $input['kode_jalur']        = $_POST['kode_jalur'];
            
            $tabel_transaksi_mutasi = $this->db->insert("transaksi_mutasi", $input);
            if ($tabel_transaksi_mutasi) {
                if ($inp['posisi_awal'] == "Petugas") {
                    $trans_komisi['status_komisi']        = 'masuk';
                    $trans_komisi['asal_komisi']          = 'mutasi';
                    $trans_komisi['kode_asal_komisi']     = $kode_mutasi;
                    $trans_komisi['kode_penerima_komisi'] = $inp['kode_petugas'];
                    $trans_komisi['nama_penerima_komisi'] = $inp['nama_petugas'];
                    $trans_komisi['total_barang_komisi']  = $total;
                    $trans_komisi['total_komisi']         = $total_komisi;
                    $this->db->update('transaksi_komisi', $trans_komisi, array(
                        'id' => $id_transaksi_komisi
                    ));
                }
                echo '<div class="alert alert-success">Berhasil Melakukan Mutasi.</div>';
                $delete_temp = $this->db->delete("opsi_transaksi_mutasi_temp", array(
                    'kode_mutasi' => $kode_mutasi
                ));
            } else {
                $this->db->delete('transaksi_komisi', array(
                    'id' => $id_transaksi_komisi
                ));
                $this->db->delete('opsi_transaksi_komisi', array(
                    'id_transaksi_komisi' => $id_transaksi_komisi
                ));
            }
        }

        /* calculating komisi from supir */

        if ($inp['kategori_petugas'] == 'sales' || $input['kategori_petugas'] == 'sales'  ) {
            
        } else {

            if (@$inp['kategori_petugas'] == 'sopir') {
            
            $code_supir     =   $inp['kode_petugas'];

            } else if (@$inp['kategori_petugas'] == 'sales & sopir') {

                $code_supir     =   $inp['kode_sopir'];

            }

            $sj     =   @$inp['kode_surat_jalan'];
            $komisi_jumlah_sopir    =   0;
            $dataX   =   $this->db->get_where('opsi_transaksi_mutasi' , ['kode_surat_jalan' => $sj ] )->result();
            foreach ($dataX as $value) {
                
                $komisi_jumlah_sopir += $value->jumlah;

            }

            $insert_to_komisi   =   [
                'kode_transaksi_asal_komisi' => $sj ,
                'tujuan_komisi' => 'SOPIR',
                'nominal_terbayar' => $komisi_jumlah_sopir ,
                'kode_sales' => $code_supir ,
                'besar_komisi' => 350 ,
                'nominal_komisi' => 350 * $komisi_jumlah_sopir ,
                'keterangan' => 'Sopir membawa '.$komisi_jumlah_sopir .' Barang',
                'tanggal' => date('Y-m-d'), 
            ];

            $this->db->insert('transaksi_komisi_revisi' , $insert_to_komisi);



        }

        







    }
    
    public function hapus_temp()
    {
        $id = $this->input->post('id');
        $this->db->delete('opsi_transaksi_mutasi_temp', array(
            'id' => $id
        ));
    }
    public function hapus_mutasi_temp()
    {
        $kode_mutasi  = $this->input->post('kode_mutasi');
        $jenis_mutasi = $this->input->post('jenis_mutasi');
        $this->db->delete('opsi_transaksi_mutasi_temp', array(
            'kode_mutasi' => $kode_mutasi,
            'jenis_mutasi' => $jenis_mutasi
        ));
    }
    public function get_rak()
    {
        $gudang = $this->input->post('gudang');
        $this->db->where('status', '1');
        $this->db->where('kode_unit_jabung', 'KES_001');
        $get_rak   = $this->db->get_where('master_rak', array(
            'kode_unit' => $gudang
        ));
        $hasil_rak = $get_rak->result();
        echo '<option value="">Pilih Rak</option>';
        foreach ($hasil_rak as $rak) {
            echo '<option value="' . $rak->kode_rak . '">' . $rak->nama_rak . '</option>';
        }
    }
    public function get_jenis_bahan()
    {
        $kode_rak         = $this->input->post('kode_rak');
        $jenis_mutasi     = $this->input->post('jenis_mutasi');
        $kode_petugas     = $this->input->post('kode_petugas');
        $kategori_petugas = $this->input->post('kategori_petugas');
        if ($jenis_mutasi == 'ke_petugas') {
            $this->db->where('status', 'sendiri');
            $this->db->where('kode_rak', $kode_rak);
        } elseif ($jenis_mutasi = 'ke_gudang') {
            if ($kategori_petugas == 'sales' or $kategori_petugas == 'sales & sopir') {
                $this->db->where('status', 'sales');
                $this->db->where('kode_member', $kode_petugas);
            } elseif ($kategori_petugas == 'sopir') {
                $this->db->where('status', 'sopir');
                $this->db->where('kode_member', $kode_petugas);
            }
        }
        
        $jenis_bahan       = $this->db->get('master_bahan_jadi');
        $hasil_jenis_bahan = $jenis_bahan->result();
        echo "<option value=''>Pilih Produk</option>";
        foreach ($hasil_jenis_bahan as $value) {
            echo "<option value=" . $value->kode_bahan_jadi . ">" . $value->nama_bahan_jadi . "</option>";
        }
        echo $this->db->last_query();
        
    }
    public function get_petugas()
    {
        $kode_petugas     = $this->input->post('kode_petugas');
        $kategori_petugas = $this->input->post('kategori_petugas');
        if ($kategori_petugas == 'sales' or $kategori_petugas == 'sales & sopir') {
            $petugas = $this->db->get_where('master_sales', array(
                'kode_sales' => $kode_petugas
            ));
        } elseif ($kategori_petugas == 'sopir') {
            $petugas = $this->db->get_where('master_sopir', array(
                'kode_sopir' => $kode_petugas
            ));
        }
        $hasil_petugas = @$petugas->row();
        
        echo json_encode($hasil_petugas);
        
    }
    public function get_sopir()
    {
        $kode_petugas  = $this->input->post('kode_sopir');
        $petugas       = $this->db->get_where('master_sopir', array(
            'kode_sopir' => $kode_petugas
        ));
        $hasil_petugas = @$petugas->row();
        
        echo json_encode($hasil_petugas);
    }
    public function get_kendaraan()
    {
        $kode_petugas  = $this->input->post('no_kendaraan');
        $petugas       = $this->db->get_where('master_kendaraan', array(
            'no_kendaraan' => $kode_petugas
        ));
        $hasil_petugas = @$petugas->row();
        
        echo json_encode($hasil_petugas);
    }
    public function get_surat_jalan()
    {
        $kode_surat_jalan = $this->input->post('kode_surat_jalan');
        
        $cek_ditutup = $this->db->get_where('transaksi_mutasi', array(
            'kode_surat_jalan' => $kode_surat_jalan,
            'status_mutasi' => 'Mutasi Ke Gudang'
        ));
        if ($cek_ditutup->num_rows()) {
            echo json_encode(array(
                'sudah_tutup' => true
            ));
            exit();
        }
        
        $kode_mutasi       = $this->db->get_where('transaksi_mutasi', array(
            'kode_surat_jalan' => $kode_surat_jalan,
            'kode_unit_asal ' => 'U001'
        ));
        $hasil_kode_mutasi = @$kode_mutasi->row();
        //echo $this->db->last_query();
        echo json_encode($hasil_kode_mutasi);
        
    }
    public function get_petugas_kategori()
    {
        $kategori_petugas = $this->input->post('kategori_petugas');
        
        if ($kategori_petugas == 'sales' or $kategori_petugas == 'sales & sopir') {
            $petugas       = $this->db->get_where('master_sales', array(
                'status' => '1'
            ));
            $hasil_petugas = $petugas->result();
            echo "<option value=''>-- Pilih Sales --</option>";
            foreach ($hasil_petugas as $value) {
                echo "<option value='" . $value->kode_sales . "'>" . $value->nama_sales . "</option>";
            }
        } elseif ($kategori_petugas == 'sopir') {
            $petugas       = $this->db->get_where('master_sopir', array(
                'status' => '1'
            ));
            $hasil_petugas = $petugas->result();
            echo "<option value=''>-- Pilih Sopir --</option>";
            foreach ($hasil_petugas as $value) {
                echo "<option value='" . $value->kode_sopir . "'>" . $value->nama_sopir . "</option>";
            }
        }
        
    }
    public function get_jenis_bahan_anggota()
    { {
            $jenis_bahan = $this->input->post('jenis_bahan');
            $kode_unit   = $this->input->post('kode_petugas');
            $kode_rak    = $this->input->post('kode_rak');
            if ($jenis_bahan == 'peralatan') {
                
                $this->db->where('kode_unit_jabung', 'KES_001');
                $jenis_bahan       = $this->db->get_where('master_peralatan', array(
                    'kode_unit' => $kode_unit,
                    'kode_rak' => $kode_rak,
                    'status_kepemilikan' => 'Anggota'
                ));
                $hasil_jenis_bahan = $jenis_bahan->result();
                echo "<option value=''>Pilih Peralatan</option>";
                foreach ($hasil_jenis_bahan as $value) {
                    echo "<option value=" . $value->kode_peralatan . ">" . $value->nama_peralatan . "</option>";
                }
                
            } elseif ($jenis_bahan == 'obat') {
                
                $this->db->where('kode_unit_jabung', 'KES_001');
                $jenis_bahan = $this->db->get_where('master_obat', array(
                    'kode_unit' => $kode_unit,
                    'kode_rak' => $kode_rak,
                    'status_kepemilikan' => 'Anggota'
                ));
                echo $this->db->last_query();
                $hasil_jenis_bahan = $jenis_bahan->result();
                echo "<option value=''>Pilih Obat</option>";
                foreach ($hasil_jenis_bahan as $value) {
                    echo "<option value=" . $value->kode_obat . ">" . $value->nama_obat . "</option>";
                }
            }
            
            
        }
    }
    
}
