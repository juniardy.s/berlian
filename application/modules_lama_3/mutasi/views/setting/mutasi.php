<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Tambah Mutasi Ke Petugas
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>


        <div class="box-body">            
          <div class="sukses" ></div>
          <div class="row">
            <div class="col-md-12">


              <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                  <div class="box box-info">                 
                    <div class="box-body">           

                      <form id="data_form" action="" method="post">

                        <div class="box-body">
                          <div class="callout callout-info">

                          </div>


                          <?php
                          $tgl = date("Y-m-d");
                          $no_belakang = 0;
                          $user = $this->session->userdata('astrosession');
                          $id_user=$user->id;

                          $this->db->select_max('id');
                          $get_max_mut = $this->db->get('transaksi_mutasi');
                          $max_mut = $get_max_mut->row();

                          $this->db->where('id', $max_mut->id);
                          $get_mut = $this->db->get('transaksi_mutasi');
                          $mut = $get_mut->row();

                          $tahun = substr(@$mut->kode_mutasi, 4,4);

                          if(date('Y')==$tahun){
                            $nomor = substr(@$mut->kode_mutasi, 12);

                            $nomor = $nomor + 1;
                            $string = strlen($nomor);
                            if($string == 1){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                            } else if($string == 2){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                            } else if($string == 3){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_000'.$nomor;
                            } else if($string == 4){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_00'.$nomor;
                            } else if($string == 5){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_0'.$nomor;
                            } else if($string == 6){
                              $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_'.$nomor;
                            }
                          } else {
                            $kode_trans = 'MUT_'.date('Y').'_'.$id_user.'_000001';
                          }

                          if(date('Y')==$tahun){
                            $nomor = substr(@$mut->kode_mutasi, 12);

                            $nomor = $nomor + 1;
                            $string = strlen($nomor);
                            if($string == 1){
                              $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                            } else if($string == 2){
                              $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                            } else if($string == 3){
                              $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_000'.$nomor;
                            } else if($string == 4){
                              $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_00'.$nomor;
                            } else if($string == 5){
                              $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_0'.$nomor;
                            } else if($string == 6){
                              $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_'.$nomor;
                            }
                          } else {
                            $kode_surat = 'SJ_'.date('Y').'_'.$id_user.'_000001';
                          }
                          ?>
                          <input type="hidden" value="<?php echo @$kode_trans; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_mutasi" id="kode_mutasi" readonly/>
                          <input type="hidden" value="<?php echo @$kode_surat; ?>" class="form-control" placeholder="Kode Transaksi" name="kode_surat_jalan" id="kode_surat_jalan" readonly/>
                          <input type="hidden" value="Gudang" class="form-control" placeholder="Kode Transaksi" name="posisi_awal" id="posisi_awal"/>                           


                        </div> 
                        <br>

                        <div class="box-body" >
                          <div class="row">

                            <div class="col-md-3">
                              <label>Kategori Petugas </label>
                              <select class="form-control" id="kategori_petugas" name="kategori_petugas" required>
                                <option value="">-- Pilih Kategori --</option>
                                <option value="sales">Sales</option>
                                <option value="sopir">Sopir</option>
                                <option value="sales & sopir">Sales & Sopir</option>
                              </select>
                            </div>
                            <div class="col-md-3">
                              <label>Petugas </label>
                              <select class="form-control" id="kode_petugas" name="kode_petugas" required>
                                <option selected="true" value="">Pilih Petugas</option>

                              </select>
                              <input type="hidden" readonly class="form-control"   id="nama_petugas" name="nama_petugas" />
                            </div>
                            <div class="col-md-3 sopir">
                              <label>Sopir</label>
                              <select class="form-control" id="kode_sopir" name="kode_sopir" >
                                <option selected="true" value="">--Pilih Sopir--</option>
                                <?php
                                $sopir = $this->db->get_where('master_sopir', array('status'=>'1'));
                                $hasil_sopir = $sopir->result();
                                foreach ($hasil_sopir as $value) {
                                  echo "<option value='".$value->kode_sopir."'>".$value->nama_sopir."</option>";
                                }
                                ?>
                              </select>
                              <input type="hidden" readonly class="form-control"   id="nama_sopir" name="nama_sopir" />
                            </div>
                            <div class="col-md-3">
                              <label>Kendaraan</label>
                              <select class="form-control" id="no_kendaraan" name="no_kendaraan" >
                                <option selected="true" value="">--Pilih Kendaraan--</option>
                                <?php
                                $kendaraan = $this->db->get_where('master_kendaraan', array('status'=>'1'));
                                $hasil_kendaraan = $kendaraan->result();
                                foreach ($hasil_kendaraan as $value) {
                                  echo "<option value='".$value->no_kendaraan."'>".$value->no_kendaraan."</option>";
                                }
                                ?>
                              </select>
                              <input type="hidden" readonly class="form-control" id="nama_kendaraan" name="nama_kendaraan" />
                            </div>


                            <div class="col-md-3">
                              <label>Jalur</label>
                              <select class="form-control select2" id="jalur" onchange="send_this_jalur_val(this.value)" >
                                <option selected="true" value="">--Pilih Jalur--</option>
                                <?php
                                $kendaraan = $this->db->get_where('master_jalur', array('status'=>'1'));
                                $hasilx = $kendaraan->result();
                                foreach ($hasilx as $value) {
                                  echo "<option value='".$value->kode_jalur."'>".$value->nama_jalur."</option>";
                                }
                                ?>
                              </select>
                              <input type="hidden" readonly class="form-control" id="kode_jalur" name="kode_jalur" />
                            </div>

                          </div>



                          <div class="row">
                            <div class="col-md-6">
                              <label>Keterangan</label>
                              <textarea class="form-control" value="" name="keterangan" id="keterangan" required=""></textarea>
                            </div>
                            <div class="col-md-2" style="padding-top:26px;">
                              <div onclick="pop_up_save()" id="add_posisi"  class="btn btn-lg btn-primary btn-block pull-left">Save</div>
                              
                            </div>
                          </div>
                          

                        </div> 
                        <div class="sukses" ></div>
                        <br>
                        <br>
                        <div class="gagal" ></div>
                        <div class="box-body lock_posisi">
                          <div class="row">
                            <div class="">

                              <div class="col-md-3">
                                <label>Nama Produk</label>
                                <select id="kode_bahan" name="kode_bahan" class="form-control select2">
                                  <option value="">Pilih Produk</option>
                                  <?php
                                  $this->db->where('status','sendiri');
                                  $jenis_bahan = $this->db->get('master_bahan_jadi');
                                  $hasil_jenis_bahan = $jenis_bahan->result();

                                  foreach ($hasil_jenis_bahan as  $value) {
                                    echo "<option value=".$value->kode_bahan_jadi.">".$value->nama_bahan_jadi."</option>";
                                  }
                                  ?>
                                </select>
                                <input type="hidden" readonly class="form-control" placeholder="Satuan Stok"  id="nama_bahan" />
                                <input type="hidden" readonly class="form-control" placeholder="Satuan Stok"  id="stok_awal" />
                                <input type="hidden" readonly class="form-control" placeholder="Satuan Stok"  id="hpp" />
                                <input type="hidden" readonly class="form-control" value="ke_petugas"  id="jenis_mutasi" />
                              </div>
                              <div class="col-md-3">
                                <label>QTY</label>
                                <input type="number" onkeyup="cek_jumlah()" class="form-control" placeholder="QTY" name="jumlah" id="jumlah" />
                              </div>
                              <div class="col-md-2" style="padding-top:26px;">
                                <div onclick="add_item()" id="add"  class="btn btn-primary btn-block pull-left">Add</div>
                                <div onclick="update_item()" id="update"  class="btn btn-primary btn-block">Update</div>
                                <input type="hidden" name="id_item_temp" id="id_item_temp" />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div id="list_transaksi_pembelian" class="lock_posisi">
                          <div class="box-body"><br>
                            <table id="tabel_daftar" class="table table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <!--<th>Kategori Bahan</th>-->
                                  <th>Nama Produk</th>
                                  <th>QTY</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody id="tabel_temp_data_mutasi">

                              </tbody>
                              <tfoot>

                              </tfoot>
                            </table>
                          </div>

                        </div>

                        <br>
                        <div class="box-footer lock_posisi">
                          <!-- <button type="submit" class="btn btn-primary ">Simpan</button> -->
                          <center>
                            <a onclick="batal_posisi()" class="btn btn-lg red " style="width:200px;"><i ></i> Batal</a>
                            <button type="submit" class="btn btn-lg green-seagreen" style="width:200px;"><i class="fa fa-save"></i> Simpan</button></center>

                          </div>
                        </form>
                      </div>
                    </div>
                  </section><!-- /.Left col -->      
                </div>
              </div>
            </div>
          </div>

          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>
    </div><!-- /.col -->
  </div>

  <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="delData()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <div id="modal-confirm-save" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Tambah Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menambah data tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="lock_posisi()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <div id="modal-confirm-batal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:grey">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#fff;">Konfirmasi Batal Data</h4>
        </div>
        <div class="modal-body">
          <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data tersebut ?</span>
          <input id="id-delete" type="hidden">
        </div>
        <div class="modal-footer" style="background-color:#eee">
          <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
          <button onclick="simpan_batal_posisi()" class="btn green">Ya</button>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css" media="screen">
    .btn-back
    {
      position: fixed;
      bottom: 10px;
      left: 10px;
      z-index: 999999999999999;
      vertical-align: middle;
      cursor:pointer
    }
  </style>
  <img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

  <script>
    $('.btn-back').click(function(){
      $(".tunggu").show();
      window.location = "<?php echo base_url().'mutasi/daftar_mutasi'; ?>";
    });
  </script>

  <script>

 $(document).ready(function() {
    $("#update").hide();
    $(".sopir").hide();
    $(".kendaraan").hide();
    $(".lock_posisi").hide();
    var kode_mutasi = $("#kode_mutasi").val();

    $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_temp_petugas/'; ?>" + kode_mutasi);

    $("#kode_rak").change(function() {
        var jenis_mutasi = $("#jenis_mutasi").val();
        var kode_rak = $("#kode_rak").val();
        var url = "<?php echo base_url().'mutasi/get_jenis_bahan/'?> ";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                kode_rak: kode_rak,
                jenis_mutasi: jenis_mutasi
            },

            success: function(data) {
                $(".tunggu").hide();
                $("#kode_bahan").html('');
                $("#kode_bahan").html(data);
            }
        });
    });
    $("#kategori_petugas").change(function() {

        var kategori_petugas = $("#kategori_petugas").val();
        var url = "<?php echo base_url().'mutasi/get_petugas_kategori/'?> ";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                kategori_petugas: kategori_petugas
            },


            success: function(data) {
                $(".tunggu").hide();
                $("#kode_petugas").html(data);
                if (kategori_petugas == 'sales & sopir') {
                    $(".sopir").show();
                    $(".kendaraan").show();
                } else if (kategori_petugas == 'sopir') {
                    $(".kendaraan").show();
                    $(".sopir").hide();
                    $("#kode_sopir").val('');
                    $("#nama_sopir").val('');
                } else {
                    $(".sopir").hide();
                    $(".kendaraan").hide();
                    $("#no_kendaraan").val('');
                    $("#nama_kendaraan").val('');
                    $("#kode_sopir").val('');
                    $("#nama_sopir").val('');
                }
            }
        });
    });



    $("#kode_petugas").change(function() {
        var kategori_petugas = $("#kategori_petugas").val();
        var kode_petugas = $("#kode_petugas").val();
        var url = "<?php echo base_url().'mutasi/get_petugas/'?> ";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                kode_petugas: kode_petugas,
                kategori_petugas: kategori_petugas
            },
            dataType: 'json',

            success: function(data) {
                $(".tunggu").hide();

                if (data.nama_sales) {
                    $("#nama_petugas").val(data.nama_sales);
                } else if (data.nama_sopir) {
                    $("#nama_petugas").val(data.nama_sopir);
                }

            }
        });
    });
    $("#kode_sopir").change(function() {

        var kode_sopir = $("#kode_sopir").val();
        var url = "<?php echo base_url().'mutasi/get_sopir/'?> ";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                kode_sopir: kode_sopir
            },
            dataType: 'json',
            success: function(data) {

                if (data.nama_sopir) {
                    $("#nama_sopir").val(data.nama_sopir);
                }

            }
        });
    });
    $("#no_kendaraan").change(function() {

        var no_kendaraan = $("#no_kendaraan").val();
        var url = "<?php echo base_url().'mutasi/get_kendaraan/'?> ";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                no_kendaraan: no_kendaraan
            },
            dataType: 'json',
            success: function(data) {

                if (data.nama_kendaraan) {
                    $("#nama_kendaraan").val(data.nama_kendaraan);
                }

            }
        });
    });

    $('#kode_bahan').on('change', function() {

        var kode_bahan = $('#kode_bahan').val();
        var url = "<?php echo base_url() . 'mutasi/get_satuan' ?>";
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                kode_bahan: kode_bahan
            },
            success: function(msg) {
                if (msg.satuan_stok) {
                    $('#satuan_stok').val(msg.satuan_stok);
                }

                if (msg.nama_bahan_jadi) {
                    $('#nama_bahan').val(msg.nama_bahan_jadi);
                }
                if (msg.real_stock) {
                    $('#stok_awal').val(msg.real_stock);
                }
                if (msg.hpp) {
                    $('#hpp').val(msg.hpp);
                }
            }
        });
    });

    $("#data_form").submit(function() {
        var kode_mutasi = $("#kode_mutasi").val();
        var kategori_petugas = $("#kategori_petugas").val();
        var kode_petugas = $("#kode_petugas").val();
        var nama_petugas = $("#nama_petugas").val();
        var kode_sopir = $("#kode_sopir").val();
        var no_kendaraan = $("#no_kendaraan").val();
        var keterangan = $("#keterangan").val();
        var kode_surat_jalan = $("#kode_surat_jalan").val();
        var posisi_awal = $("#posisi_awal").val();
        var jalur = $("#kode_jalur").val();

        var simpan_mutasi = "<?php echo base_url().'mutasi/simpan_mutasi/'?>";
        $.ajax({
            type: "POST",
            url: simpan_mutasi,
            //data: $('#data_form').serialize(),
            data: {
                kode_mutasi: kode_mutasi,
                kategori_petugas: kategori_petugas,
                kode_petugas: kode_petugas,
                kode_sopir: kode_sopir,
                no_kendaraan: no_kendaraan,
                keterangan: keterangan,
                kode_surat_jalan: kode_surat_jalan,
                posisi_awal: posisi_awal,
                nama_petugas: nama_petugas,
                kode_jalur: jalur
            },
            /*beforeSend: function() {
                $(".tunggu").show();
            },*/
            success: function(msg) {
                $(".tunggu").hide();
                if (msg == "stok kurang") {

                    $(".sukses").html('<div class="alert alert-warning">Mutasi lebih kecil dari bahan pending</div>');
                    setTimeout(function() {
                        $('.sukses').html('');
                    }, 1000);
                } else {
                    var kode_mutasi = $('#kode_mutasi').val();
                    window.open("<?php echo base_url() . 'mutasi/surat_jalan/' ?>" + kode_mutasi);

                    $(".sukses").html(msg);
                    setTimeout(function() {
                        $('.sukses').html('');
                        window.location = "<?php echo base_url() . 'mutasi/daftar_mutasi' ?>";
                    }, 1000);
                }

            }
        });
        return false;

    });

})

function send_this_jalur_val(val){
  $("#kode_jalur").val(val);
}

function add_item() {
    var kode_mutasi = $("#kode_mutasi").val();
    var kode_rak = $("#kode_rak").val();
    var kode_petugas = $("#unit_akhir").val();
    var kode_bahan = $('#kode_bahan').val();
    var nama_bahan = $('#nama_bahan').val();
    var jumlah = $('#jumlah').val();
    var jenis_mutasi = $("#jenis_mutasi").val();
    var url = "<?php echo base_url().'mutasi/simpan_item_mutasi_temp/'?> ";
    if (jumlah == '' || kode_bahan == '') {
        alert('Lengkapi Form ...!');
    } else {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                kode_mutasi: kode_mutasi,
                kode_rak: kode_rak,
                kode_petugas: kode_petugas,
                kode_bahan: kode_bahan,
                nama_bahan: nama_bahan,
                jumlah: jumlah,
                jenis_mutasi: jenis_mutasi
            },
            beforeSend: function() {
                $(".tunggu").show();
            },
            success: function(hasil) {
                $(".tunggu").hide();
                var data = hasil.split("|");
                var num = data[0];
                var pesan = data[1];
                if (num == 1) {
                    $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_temp_petugas/'; ?>" + kode_mutasi);
                    $('#jenis_bahan').val('');
                    $('#kode_bahan').val('');
                    $('#jumlah').val('');
                } else {
                    $(".gagal").html(pesan);
                    setTimeout(function() {
                        $('.gagal').html('');
                    }, 1500);
                }
            }
        });
    }

}

function pop_up_save() {
    var kategori_petugas = $("#kategori_petugas").val();
    var kode_petugas = $("#kode_petugas").val();
    var kode_sopir = $("#kode_sopir").val();
    var no_kendaraan = $("#no_kendaraan").val();

    if (kategori_petugas == 'sales' && (kode_petugas == '' || no_kendaraan == '')) {
        alert('Lengkapi Form');
    } else if (kategori_petugas == 'sopir' && (kode_petugas == '' || no_kendaraan == '')) {
        alert('Lengkapi Form');
    } else if (kategori_petugas == 'sales & sopir' && (kode_petugas == '' || no_kendaraan == '' || kode_sopir == '')) {
        alert('Lengkapi Form');
    } else if (kategori_petugas == '' && (kode_petugas == '' || no_kendaraan == '')) {
        alert('Lengkapi Form');
    } else {

        $('#modal-confirm-save').modal('show');
    }

}

function lock_posisi() {
    var kategori_petugas = $("#kategori_petugas").val();
    var kode_petugas = $("#kode_petugas").val();
    var kode_sopir = $("#kode_sopir").val();
    var no_kendaraan = $("#no_kendaraan").val();

    if (kategori_petugas == 'sales' && (kode_petugas == '' || no_kendaraan == '')) {
        alert('Lengkapi Form');
    } else if (kategori_petugas == 'sopir' && (kode_petugas == '' || no_kendaraan == '')) {
        alert('Lengkapi Form');
    } else if (kategori_petugas == 'sales & sopir' && (kode_petugas == '' || no_kendaraan == '' || kode_sopir == '')) {
        alert('Lengkapi Form');
    } else if (kategori_petugas == '' && (kode_petugas == '' || no_kendaraan == '')) {
        alert('Lengkapi Form');
    } else {

        $(".lock_posisi").show();
        $("#add_posisi").hide();

        $("#keterangan").attr('readonly', true);
        $("#kategori_petugas").attr('disabled', true);
        $("#kode_petugas").attr('disabled', true);
        $("#jalur").attr('disabled', true);
        $("#kode_sopir").attr('disabled', true);
        $("#no_kendaraan").attr('disabled', true);
        $('#modal-confirm-save').modal('hide');


    }

}

function batal_posisi() {
    $('#modal-confirm-batal').modal('show');
}

function simpan_batal_posisi() {

    var kode_mutasi = $('#kode_mutasi').val();
    var jenis_mutasi = 'ke_petugas';
    var url = "<?php echo base_url().'mutasi/hapus_mutasi_temp'; ?>";
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            kode_mutasi: kode_mutasi,
            jenis_mutasi: jenis_mutasi
        },
        beforeSend: function() {
            $(".tunggu").show();
        },
        success: function(pembelian) {
            $(".tunggu").hide();
            $('#modal-confirm-batal').modal('hide');
            $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_temp_petugas/'; ?>" + kode_mutasi);

            $(".lock_posisi").hide();
            $("#add_posisi").show();
            $("#keterangan").attr('readonly', false);
            $("#kategori_petugas").attr('disabled', false);
            $("#kode_petugas").attr('disabled', false);
            $("#kode_sopir").attr('disabled', false);
            $("#no_kendaraan").attr('disabled', false);
        }
    });



}

function cek_jumlah() {
    var jumlah = $("#jumlah").val();
    if (parseInt(jumlah) < 0 || jumlah == '0') {
        alert('QTY Salah');
        $("#jumlah").val('');

    }
}

function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
}

function delData() {
    var id = $('#id-delete').val();
    var url = '<?php echo base_url().'master/menu_resto/hapus_bahan_jadi'; ?>/delete';
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id: id
        },
        beforeSend: function() {
            $(".tunggu").show();
        },
        success: function(msg) {
            $(".tunggu").hide();
            $('#modal-confirm').modal('hide');

            window.location.reload();
        }
    });
    return false;
}

function actEdit(id) {
    var id = id;
    var kode_mutasi = $('#kode_mutasi').val();
    var url = "<?php echo base_url().'mutasi/get_temp_mutasi'; ?>";
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            id: id
        },
        success: function(mutasi) {
            $('#jenis_bahan').val(mutasi.kategori_bahan);
            get_jenis_bahan2(mutasi.kode_bahan);
            $("#nama_bahan").val(mutasi.nama_bahan);
            $('#jumlah').val(mutasi.jumlah);
            $("#id_item_temp").val(mutasi.id);
            $("#add").hide();
            $("#update").show();
            $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_temp_petugas/'; ?>" + kode_mutasi);
        }
    });
}

function update_item() {
    var kode_mutasi = $("#kode_mutasi").val();
    var kode_rak = $("#kode_rak").val();
    var kode_petugas = $("#unit_akhir").val();
    var kode_bahan = $('#kode_bahan').val();
    var nama_bahan = $('#nama_bahan').val();
    var jumlah = $('#jumlah').val();
    var jenis_mutasi = $("#jenis_mutasi").val();
    var id_item_temp = $("#id_item_temp").val();
    var url = "<?php echo base_url().'mutasi/ubah_item_mutasi_temp/'?> ";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            kode_mutasi: kode_mutasi,
            kode_rak: kode_rak,
            kode_petugas: kode_petugas,
            kode_bahan: kode_bahan,
            nama_bahan: nama_bahan,
            jumlah: jumlah,
            jenis_mutasi: jenis_mutasi,
            id: id_item_temp
        },
        success: function(hasil) {
            var data = hasil.split("|");

            var num = data[0];
            var pesan = data[1];

            if (num == 1) {
                $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_temp_petugas/'; ?>" + kode_mutasi);
                $('#kategori_bahan').val('');
                $('#kode_bahan').val('');
                $('#jumlah').val('');
                $("#nama_bahan").val('');
                $("#id_item_temp").val('');
                $("#add").show();
                $("#update").hide();
            } else {
                $(".gagal").html(pesan);
                setTimeout(function() {
                    $('.gagal').html('');
                }, 1500);
            }



        }
    });
}

function delData() {
    var id = $('#id-delete').val();
    var kode_mutasi = $('#kode_mutasi').val();
    var url = "<?php echo base_url().'mutasi/hapus_temp'; ?>";
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            id: id
        },
        beforeSend: function() {
            $(".tunggu").show();
        },
        success: function(pembelian) {
            $(".tunggu").hide();
            $('#modal-confirm').modal('hide');
            $("#tabel_temp_data_mutasi").load("<?php echo base_url().'mutasi/tabel_item_temp_petugas/'; ?>" + kode_mutasi);

            $('#kode_bahan').val('');
            $('#jumlah').val('');
        }
    });
}

function get_jenis_bahan() {

    var jenis_bahan = $("#jenis_bahan").val();
    var kode_rak = $("#kode_rak").val();
    var url = "<?php echo base_url().'mutasi/get_jenis_bahan/'?> ";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            kode_rak: kode_rak,
            jenis_bahan: jenis_bahan
        },
        beforeSend: function() {
            $(".tunggu").show();
        },
        success: function(data) {
            $(".tunggu").hide();
            $("#kode_bahan").html('');
            $("#kode_bahan").html(data);
        }
    });
}


function get_jenis_bahan2(Object) {

    var jenis_mutasi = $("#jenis_mutasi").val();
    var kode_rak = $("#kode_rak").val();
    var url = "<?php echo base_url().'mutasi/get_jenis_bahan/'?> ";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            kode_rak: kode_rak,
            jenis_mutasi: jenis_mutasi
        },
        beforeSend: function() {
            $(".tunggu").show();
        },
        success: function(data) {
            $(".tunggu").hide();
            $("#kode_bahan").html('');
            $("#kode_bahan").html(data);
            $("#kode_bahan").val(Object);
        }
    });
}

</script>
