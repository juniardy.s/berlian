<?php if ($tipe == 'mutasi'){ ?>
	
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Transaksi</th>
			<th>Kode Surat Jalan</th>
			<th>Jalur</th>
			<th>Keterangan</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$no =1 ; 
			foreach( $data as $val){ 
				$jalur 	=	$this->db->get_where('master_jalur' , ['kode_jalur' => $val->kode_jalur , 'status' => 1] )->result_array();

			}
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo TanggalIndo($val->tanggal_transaksi) ?></td>
			<td><?php echo $val->kode_surat_jalan ?></td>
			<td><?php echo $jalur[0]['nama_jalur'] ?></td>
			<td><?php echo $val->keterangan ?></td>
			<td>
				<button class="btn btn-sm btn-info" onclick="lihat_mutasi('<?= $val->kode_surat_jalan ?>')">
					<i class="fa fa-search"></i> Detail
				</button>
			</td>
		</tr>
	</tbody>


<?php } else { ?>
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Transaksi</th>
			<th>Kode Transaksi</th>
			<th>Jalur</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$nox =1 ; 
			foreach( $data as $val){ 
				$jalur 	=	$this->db->get_where('master_member' , ['kode_member' => $val->kode_member ] )->result_array();

			}
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo TanggalIndo($val->tanggal_penjualan) ?></td>
			<td><?php echo $val->kode_penjualan ?></td>
			<td><?php echo $jalur[0]['nama_jalur'] ?></td>
			<td>
				<button class="btn btn-sm btn-info" onclick="lihat_mutasi('<?= $val->kode_penjualan ?>')">
					<i class="fa fa-search"></i> Detail
				</button>
			</td>
		</tr>
	</tbody>
<?php } ?>