<!DOCTYPE html>
<html>
<head>
	<title>PRINT NOTA PENJUALAN</title>
	<style type="text/css">
		.table1{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 0px;
			text-align: left;
			vertical-align: middle;
		}

		.table2{
			width:100%; 
			border-collapse: collapse;
		}

		.table3{
			border-collapse: collapse;
			width:100%;
			position: absolute;
			top: 160px;
			text-align: left;
			vertical-align: middle;
		}

		@media print {
			html, body {


				display: block;
				font-family: "Dotrice";
				font-size: auto;
			}

			@page
			{
				size: 21cm 14cm;
			}

		}

		div.page { page-break-after: always;
			position: relative;
			margin:10px 15px 0px 15px;
			padding:0px; }

		</style>
	</head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<?php
	$kode_mutasi=$this->uri->segment(3);
	$get_transaksi=$this->db->get_where('transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
	$hasil_transaksi=$get_transaksi->row();
	if($hasil_transaksi->kategori_petugas=='sales' or $hasil_transaksi->kategori_petugas=='sales & sopir'){
		$get_petugas=$this->db->get_where('master_sales',array('kode_sales' => $hasil_transaksi->kode_unit_tujuan ));		
	}else{
		$get_petugas=$this->db->get_where('master_sopir',array('kode_sopir' => $hasil_transaksi->kode_unit_tujuan ));		
	}
	$hasil_petugas=$get_petugas->row();
	?>
	<style>
		html, body {
			font-weight: 600;
			font-family: Arial, Helvetica, sans-serif !important;
		}
	</style>
	<body onload="print()">
		<table width="100%" style="font-size: 11px;">
			<tr>
				<td colspan="2"><img src="<?php echo base_url().'component/img/logo berlian tm.jpg' ?>" width="150px" alt="" title="" />	</td>
				<td colspan="2" rowspan="2" align="center"><h5><b>BUKTI SURAT JALAN</b></h5></td>
				<td>NO. PENGIRIMAN :</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">www.sapuberlian.com</td>
				<td>NO. BUKU :</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">(031) 7991119/ 7997597/ WA 08128322744</td>
				<td>SALES & DRIVER :</td>
				<td><?php echo $hasil_transaksi->nama_unit_tujuan;?> / <?php if($hasil_transaksi->kategori_petugas=='sales & sopir'){echo $hasil_transaksi->nama_sopir;}?></td>
				<td>TANGGAL :</td>
				<td><?php echo tanggalIndo($hasil_transaksi->tanggal_transaksi); ?></td>
			</tr>
			<tr>
				<td colspan="2">Jl. Gurang Anyar No 17 - 19, Cerme, Gresik</td>
				<td>NO. SURAT JALAN :</td>
				<td><?php echo @$hasil_transaksi->kode_surat_jalan;?></td>
				<td>JALUR :</td>
				<?php 
					$kode 	=	$hasil_transaksi->kode_jalur;
					$get 	=	$this->db->get_where('master_jalur' , ['kode_jalur' => $kode ] )->result_array();
				 ?>
				<td><?php echo $get[0]['nama_jalur'] ;?></td>
			</tr>
		</table>
		<br>
		<table border="1"  width="100%" class="table table-bordered" style="font-size: 11px">
			<thead>
				<tr>
					<th style="text-align: center;">NO</th>
					<th style="text-align: center;">KODE BARANG</th>
					<th style="text-align: center;">NAMA BARANG</th>
					<th style="text-align: center;">JUMLAH BARANG</th>
					<th style="text-align: center;">SATUAN</th>
					<th style="text-align: center;">KETERANGAN</th>
				</tr>
			</thead>
			<tbody>	
				<?php

				$get_opsi=$this->db->get_where('opsi_transaksi_mutasi',array('kode_mutasi' => $kode_mutasi ));
				$hasil_opsi=$get_opsi->result();
				$no=1;
				$total = 0 ;
				foreach ($hasil_opsi as  $value) {
					?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td align="left"><?php echo @$value->kode_bahan;?></td>
						<td align="center"><?php echo @$value->nama_bahan;?></td>
						<td align="center" ><?php echo @$value->jumlah;?></td>
						<?php
						$get_bahan_jadi = $this->db->get_where('master_bahan_jadi',array('kode_bahan_jadi'=>@$value->kode_bahan));
						$hasil_bahanjadi = $get_bahan_jadi->row();
						?>
						<td align="center" ><?php echo @$hasil_bahanjadi->satuan_stok;?></td>
						<td align="left" style="text-align: center;"></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td style="text-align: right;" colspan="5"><b>TOTAL</b></td>
					<td style="text-align: right;">0</td>
				</tr>
			</tbody>
		</table>
		<table width="100%" style="font-size: 11px">
			<tr>
				<td align="center"><b>Pengirim</b></td>
				<td align="center"><b>Gudang</b></td>
				<td align="center"><b>Sopir</b></td>
				<td align="center"><b>Satpam</b></td>
				<td align="center"><b>Penerima</b></td>
			</tr>
			<tr>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
				<td height="150px" align="center">(......................................)</td>
			</tr>
		</table>
	</body>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</html>