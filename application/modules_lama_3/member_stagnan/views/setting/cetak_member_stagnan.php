<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
  <img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
  <hr>
  <p><h3 align="center">Laporan member Stagnan</h3></p>
  <?php
  $get_member = $this->db->get_where('master_member',array('status_member '=>1));
  $hasil_member = $get_member->result();
  ?>
  <table width="100%" id="tabel_daftar" class="table" border="1" style="border-collapse: collapse;">
    <thead>
      <tr>
        <th width="50px;">No</th>
        <th>Kode Member</th>
        <th>Nama Member</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;

      foreach($hasil_member as $item){ 

        $tgl1 = date('Y-m-d');
        $tgl2 = date('Y-m-d', strtotime('-7 days', strtotime($tgl1))); 
        $this->db->where('tanggal_penjualan >=',$tgl2);
        $this->db->where('tanggal_penjualan <=',$tgl1);
        $this->db->where('kode_member',$item->kode_member);

        $get_penjualan = $this->db->get('transaksi_penjualan');
        $hasil_penjualan = $get_penjualan->row();

        if(count($hasil_penjualan) == 0){
          ?> 
          <td><?php echo $no; ?></td> 
          <td><?php echo $item->kode_member; ?></td>
          <td><?php echo $item->nama_member; ?></td>                  
        </tr>
        <?php 
        $no++;
      }  }?>
    </tbody>            
  </table>
</body>
</html>
<script type="text/javascript">
 $(document).ready(function() {

  $('#tabel_daftar').DataTable( {
    "order": [[ 1, "desc" ]],
    "searching": false
  } );

    //$('#tabel_daftar').dataTable();
  } );

</script>