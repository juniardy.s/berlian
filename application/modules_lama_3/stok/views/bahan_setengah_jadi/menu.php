<div class="">
  <div class="page-content">

    <div class="box-footer clearfix">

      <div class="row">
        <div class="col-md-3">
          <a class="dashboard-stat dashboard-stat-light blue-soft" id="daftar_cooling" href="<?php echo base_url().'stok/bahan_setengah_jadi'?>">
            <div class="visual">
              <i class="glyphicon glyphicon-taskss" >></i>
            </div>
            <div class="details" >
              <div class="number">

              </div>
              <div class="desc">
                Stok Bahan Setengah Jadi
              </div>
            </div>
          </a>
        </div>
        
        <div class="col-md-3">
          <a class="dashboard-stat dashboard-stat-light red-soft" href="<?php echo base_url().'spoil/spoil_s_jadi/daftar_spoil'?>" id="daftar_pos">
            <div class="visual">
              <i class="glyphicon glyphicon-shopping-cart"></i>
            </div>
            <div class="details">
              <div class="number">

              </div>
              <div class="desc">
                Spoil
              </div>
            </div>
          </a>
        </div>
        <?php
        $get_user = $this->session->userdata('astrosession');
        if(@$get_user->jabatan=='001' || @$get_user->jabatan=='002'){
          ?>
          <div class="col-md-3">
            <a class="dashboard-stat dashboard-stat-light green-soft" href="<?php echo base_url().'opname/opname_s_jadi/daftar_opname' ?>" id="daftar_kelompok">
              <div class="visual">
                <i class="glyphicon glyphicon-th" ></i>
              </div>
              <div class="details">
                <div class="number">

                </div>
                <div class="desc">
                  Opname
                </div>
              </div>
            </a>
          </div>
          <?php
        }
        ?>
      </div>

    </div>


    <div id="box_load">
      <?php echo @$konten; ?>
    </div>
  </div>
</div>
