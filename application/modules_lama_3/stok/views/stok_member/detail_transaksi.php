

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  <style type="text/css">
   .ombo{
    width: 400px;
  } 

</style>    
<!-- Main content -->
<section class="content">             
  <!-- Main row -->

  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <div class="portlet box blue">
        <div class="portlet-title">
          <div class="caption">
            Detail Transaksi
          </div>
          <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="reload">
            </a>

          </div>
        </div>
        <div class="portlet-body">
          <!------------------------------------------------------------------------------------------------------>


          <div class="box-body">            
            <div class="sukses" ></div>
            <div class="row">
              <div class="col-md-5" id="">
                <div class="input-group">
                  <span class="input-group-addon">Tanggal Awal</span>
                  <input type="text" class="form-control tgl" id="tgl_awal">
                </div>
              </div>

              <div class="col-md-5" id="">
                <div class="input-group">
                  <span class="input-group-addon">Tanggal Akhir</span>
                  <input type="text" class="form-control tgl" id="tgl_akhir">
                </div>
              </div>                        
              <div class="col-md-2 pull-left">
                <button style="width: 147px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
              </div>
            </div><br>
            <input type="hidden" id="kode_member" value="<?php echo $this->uri->segment(3); ?>">
            <div id="cari_transaksi">
              <table style="font-size: 1.5em;" id="tabel_daftar" class="table table-bordered table-striped">
                <?php
                $this->db->order_by('tanggal_transaksi','desc');
                $hutang = $this->db->get_where('transaksi_piutang',array('kode_customer'=>$this->uri->segment(3)));
                $hasil_hutang = $hutang->result();
                ?>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Transaksi</th>
                    <th>Nama Member</th>
                    <th>Nominal</th>
                    <th>Tanggal Transaksi</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $nomor = 1;
                  foreach($hasil_hutang as $daftar){ 
                    $tgl = ($daftar->tanggal_transaksi=='0000-00-00' || empty($daftar->tanggal_transaksi)) ? '-' : TanggalIndo(@$daftar->tanggal_transaksi) ;
                    ?> 
                    <tr>
                      <td><?php echo $nomor; ?></td>
                      <td><?php echo @$daftar->kode_transaksi; ?></td>
                      <td><?php echo @$daftar->nama_customer; ?></td>
                      <td><?php echo format_rupiah(@$daftar->nominal_piutang); ?></td>
                      <td><?php echo $tgl; ?></td>
                      <td align="center">
                        <a class="btn btn-primary" href="<?php echo base_url().'stok/detail_stok_member/'.$daftar->kode_customer."/".$daftar->kode_transaksi;  ?>"><i class="fa fa-pencil"></i> Detail</a>
                      </td>
                    </tr>
                    <?php $nomor++; } ?>

                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Kode Transaksi</th>
                      <th>Nama Member</th>
                      <th>Nominal</th>
                      <th>Tanggal Transaksi</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section> 
    </div>
  </section> 
</div>
<script type="text/javascript">
  $(document).ready(function() {

   $("#tabel_daftar").dataTable({
    "paging":   false,
    "ordering": false,
    "info":     false
  });   
 } );
</script>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'stok/stok_member'; ?>";
  });
</script>
<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<script type="text/javascript">

  $('.tgl').Zebra_DatePicker({});
  $('#cari').click(function(){
    var tgl_awal =$("#tgl_awal").val();
    var tgl_akhir =$("#tgl_akhir").val();
    var kode_member =$("#kode_member").val();
    $.ajax( {  
      type :"post",  
      url : "<?php echo base_url().'stok/cari_detail_transaksi'; ?>",  
      cache :false,

      data : {kode_member:kode_member,tgl_awal:tgl_awal,tgl_akhir:tgl_akhir},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success : function(data) {
       $(".tunggu").hide();  
       $("#cari_transaksi").html(data);
     },  
     error : function(data) {  
         // alert("das");  
       }  
     });


    $('#tgl_awal').val('');
    $('#tgl_akhir').val('');

  });
</script>
