
<div class="row">      

  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Piutang
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">
        <!------------------------------------------------------------------------------------------------------>
        <div class="row">

          <div class="col-md-4">
            <div class="form-group">
              <label>Nama Customer</label>
              <input type="text" class="form-control" id="nama_customer" />
              <input type="hidden" class="form-control" id="kode_customer" value="<?php echo $this->uri->segment(4)?>" />
            </div>
          </div>
          <div class="col-md-3">
            <a onclick="cari_produk()" style="margin-top: 25px;" class="btn btn-md green-seagreen"><i class="fa fa-search"></i> Cari</a>

          </div>
        </div>

        <div class="box-body">            
          <div class="sukses" ></div>
          <table class="table table-striped table-hover table-bordered" id="tabel_daftar"  style="font-size:1.5em;">
            <thead>
              <tr>
                <th width="50px;">No</th>
                <th>Customer</th>
                <th>Nominal Piutang</th>
                <th>Sisa Piutang</th>
                <th width="220px">Action</th>
              </tr>
            </thead>
            <tbody id="scroll_data">
              <?php
              $this->db->limit(50);
              $get_piutang = $this->db->get('transaksi_piutang');
              $hasil_piutang =$get_piutang->result();
              $no = 1;
              foreach($hasil_piutang as $item){
                if($this->session->flashdata('message')==$item->kode_customer){

                  echo '<tr id="warna" style="background: #88cc99; display: none;">';
                }
                else{
                  echo '<tr>';
                }
                ?>
                <td><?php echo $no;?></td>
                <td><?php echo $item->nama_customer ?></td>
                <td><?php echo format_rupiah($item->nominal_piutang)?></td>
                <td><?php echo format_rupiah($item->sisa)?></td>
                <td align="center"><a href="<?php echo base_url().'piutang/detail_piutang/'.$item->kode_customer?>" class="btn btn-primary btn-lg"><i class="fa fa-eye"></i> Detail</a></td>
              </tr>
              <?php
              $no++;
            } ?>
          </tbody>                
        </table>
        <?php 
        $get_jumlah = $this->db->get('transaksi_piutang');
        $jumlah = $get_jumlah->num_rows();
        $jumlah = floor($jumlah/50);
        ?>
        <div class="text-center">
          <a id="ngeload" class="btn btn-success btn-lg ngeload"><i class="fa fa-refresh"></i> Load More</a>
        </div>
        <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
        <input type="hidden" class="form-control pagenum " value="0">

      </div>

      <!------------------------------------------------------------------------------------------------------>

    </div>
  </div>
</div><!-- /.col -->
</div>
</div>    
</div>  


<style type="text/css" media="screen">
.btn-back
{
  position: fixed;
  bottom: 10px;
  left: 10px;
  z-index: 999999999999999;
  vertical-align: middle;
  cursor:pointer
}
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'analisa/menu_data'; ?>";
  });

  $(".ngeload").click(function(){
    if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
      var pagenum = parseInt($(".pagenum").val()) + 1;
      $(".pagenum").val(pagenum);
      load_table(pagenum);
    }
  })

  function load_table(page){
    var nama = $("#nama_customer").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'piutang/get_load_member' ?>",
      data: ({  page:$(".pagenum").val(),nama:nama,}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#scroll_data").append(msg);

      }
    });
  }
</script>


<script>
  $(document).ready(function() {

    
    
    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": true,
      "info":     false,
      "searching": false,
    });
  } );
  setTimeout(function(){
    $("#warna").css("background-color", "white");
    $("#warna").css("transition", "all 3000ms linear");
  }, 3000);



  function cari_produk(){
    var nama_customer = $("#nama_customer").val();

    var url = '<?php echo base_url().'piutang/cari_daftar_member'; ?>';
    $.ajax({
      type: "POST",
      url: url,
      data: {nama_customer:nama_customer},
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $("#tabel_daftar").html(msg);
        $(".tunggu").hide();
      }
    });

  }
</script>

