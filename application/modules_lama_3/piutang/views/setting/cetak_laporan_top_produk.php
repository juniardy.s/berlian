<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
<img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
<hr>
<p><h3 align="center">Laporan Top Produk</h3></p>
	<table width="100%" id="tabel_daftar" class="table" border="1" style="border-collapse: collapse;">
            <thead>
            
            <th>Nama Produk</th>
            <th>Jumlah Terjual</th>
            
            </thead>
                
                    <tbody>
                <?php
                $no = 1;
                    $this->db->group_by('kode_menu');
                    $this->db->order_by('nama_menu','asc');
                    $this->db->select('kode_menu,nama_menu,jumlah,nama_satuan');
                    $this->db->limit('10');
                    $top_produk = $this->db->get('opsi_transaksi_penjualan');
                    #echo $this->db->last_query();
                    $hasil_top_produk = $top_produk->result();
                    foreach($hasil_top_produk as $daftar){
                ?>
                <tr>
                
                <td><?php echo $daftar->nama_menu; ?></td>
                <?php
                    $this->db->select_sum('jumlah');
                    $jumlah_terjual = $this->db->get_where('opsi_transaksi_penjualan',array('kode_menu'=>$daftar->kode_menu));
                    $hasil_terjual = $jumlah_terjual->row();
                ?>
                <td><?php echo $hasil_terjual->jumlah." ".$daftar->nama_satuan; ?></td>
                </tr>
            <?php $no++; } ?>
            </tbody>
               
            
                            
      </table>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
    
      $('#tabel_daftar').DataTable( {
        "order": [[ 1, "desc" ]],
        "searching": false
    } );
      
    //$('#tabel_daftar').dataTable();
  } );
  
</script>