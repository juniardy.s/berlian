<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
    html, body {
        font-weight: 600;
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<body onload="print()">
	<?php
	$awal = $this->uri->segment(3);
	$akhir = $this->uri->segment(4);
	if(!empty($awal) && !empty($akhir)){

		$bulan_param=$akhir.'-'.$awal;
		$this->db->like('tanggal_retur',$bulan_param);
	}else{
		$tanggal_saiki=date("m"); 
		$where = "MONTH(tanggal_retur) = $tanggal_saiki"; 
		$this->db->where($where);
	}

	$this->db->select('*'); 
	$this->db->distinct();
	$this->db->order_by('tanggal_retur','desc');
	$this->db->group_by('tanggal_retur');
	$penjualan = $this->db->get('transaksi_retur_penjualan');
	$hasil_penjualan = $penjualan->result();

	?>
	<img src="<?php echo base_url().'component/img/berlian.jpg' ?>" width="400px" alt="" title="" />
	<?php
	$keuangan = 0;
	foreach($hasil_penjualan as $total){
		$keuangan += $total->grand_total;
	}

	?>

	<center><h2>LAPORAN RETUR PENJUALAN <?php if (!empty($awal) && !empty($akhir)) {
		echo strtoupper(BulanIndo($awal)) ." TAHUN " .($akhir);
	} else {
		echo "";
	}

	?></h2></center>
	<div class="table-responsive">
		<table width="100%" id="table_daftar" class="table table-bordered table-striped" border='1' style="font-size:12pt;border-collapse: collapse;">

			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>Nominal</th>

				</tr>
			</thead>
			<tbody>
				<?php
				$nomor = 1;
				$total=0;
				foreach($hasil_penjualan as $daftar){ ?> 
				<tr>
					<td><?php echo $nomor; ?></td>
					<td><?php echo TanggalIndo(@$daftar->tanggal_retur);?></td>
					<td align="right">
						<?php
						$this->db->select_sum('grand_total');
						$total_jual = $this->db->get_where('transaksi_retur_penjualan',array('tanggal_retur'=>$daftar->tanggal_retur));
						$hasil_total = $total_jual->row();
						$total +=@$hasil_total->grand_total;
						echo format_rupiah(@$hasil_total->grand_total); 
						?>
					</td>
				</tr>
				<?php $nomor++; } ?>
				<tr>
					<th colspan="2" align="center">Total</th>
					<th align="right"><?php echo format_rupiah(@$total); ?></th>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>