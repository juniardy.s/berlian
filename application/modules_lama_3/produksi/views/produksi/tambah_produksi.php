<div class="row">      
  <style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    $(".tunggu").show();
    window.location = "<?php echo base_url().'produksi/daftar'; ?>";
  });
</script>
<div class="col-xs-12">
  <!-- /.box -->
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        Produksi

      </div>
      <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>

      </div>
    </div>
    <div class="portlet-body">
      <!------------------------------------------------------------------------------------------------------>
        <!-- <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url() . 'produksi/tambah' ?>"><i class="fa fa-edit"></i> Tambah </a>
        <a style="padding:13px; margin-bottom:10px;" class="btn btn-app blue" href="<?php echo base_url() . 'produksi/daftar_produksi' ?>"><i class="fa fa-list"></i> Daftar </a> 
        <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green pull-right" href="<?php echo base_url() . 'produksi/tambah_supplier' ?>"><i class="fa fa-edit"></i> Tambah Supplier </a>  -->

        <?php
        $param = $this->uri->segment(4);
        if(!empty($param)){
          $bahan_baku = $this->db->get_where('master_bahan_baku',array('id'=>$param));
          $hasil_bahan_baku = $bahan_baku->row();
        }    

        ?>
        <div class="box-body">                   

          <form id="data_form" action="" method="post">
            <div class="box-body">
              <div class="notif_nota" ></div>
              <label><h3><b></b></h3></label>
              
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Kode Produksi</label>
                    <?php
                    $tgl = date("Y-m-d");
                    $no_belakang = 0;
                    $this->db->select_max('kode_produksi');
                    $kode = $this->db->get_where('transaksi_produksi',array('tanggal_produksi'=>$tgl));
                    $hasil_kode = $kode->row();
  #$pecah_kode = explode("_",$hasil_kode_produksi->kode_produksi);
  #echo $pecah_kode[0];
  #echo $pecah_kode[2];
                    $this->db->select('kode_produksi');
                    $kode_produksi = $this->db->get('master_setting');
                    $hasil_kode_produksi = $kode_produksi->row();

                    if(count($hasil_kode)==0){
                      $no_belakang = 1;
                    }
                    else{
                      $pecah_kode = explode("_",$hasil_kode->kode_produksi);
                      $no_belakang = @$pecah_kode[2]+1;
                    }

                    $user = $this->session->userdata('astrosession');
                    $id_user=$user->id;

                    $this->db->select_max('id');
                    $get_max_po = $this->db->get('transaksi_produksi');
                    $max_po = $get_max_po->row();

                    $this->db->where('id', $max_po->id);
                    $get_po = $this->db->get('transaksi_produksi');
                    $po = $get_po->row();
                    $tahun = substr(@$po->kode_produksi, 4,4);
                    if(date('Y')==$tahun){
                      $nomor = substr(@$po->kode_produksi, 12);
                      //echo $nomor;
                      $nomor = $nomor + 1;
                      $string = strlen($nomor);
                      if($string == 1){
                        $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_00000'.$nomor;
                      } else if($string == 2){
                        $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_0000'.$nomor;
                      } else if($string == 3){
                        $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_000'.$nomor;
                      } else if($string == 4){
                        $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_00'.$nomor;
                      } else if($string == 5){
                        $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_0'.$nomor;
                      } else if($string == 6){
                        $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_'.$nomor;
                      }
                    } else {
                      $kode_trans = 'PRO_'.date('Y').'_'.$id_user.'_000001';
                    }

                    ?>
                    <?php @$hasil_kode_produksi->kode_produksi."_".date("dmyHis")."_".$no_belakang ?>
                    <input readonly="true" type="text" value="<?php echo $kode_trans ?>" class="form-control" placeholder="Kode Transaksi" name="kode_produksi" id="kode_produksi" />
                    <input type="hidden" id="hasil_kode_perintah" name="kode_po" >
                  </div>

                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="gedhi">Tanggal Produksi</label>
                    <input type="text" value="<?php echo TanggalIndo(date("Y-m-d")); ?>" readonly="true" class="form-control" placeholder="Tanggal Transaksi" name="tanggal_produksi" id="tanggal_produksi"/>
                  </div>



                </div>

                <div class="col-md-6">
                  <div class="form-group">  
                    <label>Kode Perintah Produksi</label>
                    <input type="text" class="form-control" placeholder="Kode Perntah" name="kode_perintah" id="kode_p2" required="" readonly="true" />
                  </div>

                </div>

                <div class="col-md-6">
                  <div class="form-group">  
                    <label>Keterangan</label>
                    <!-- <input type="text" class="form-control" placeholder="Keterangan" name="keterangan" id="keterangan" required=""/> -->
                    <textarea  class="form-control" placeholder="Keterangan" name="keterangan" id="keterangan" required=""></textarea>
                  </div>

                </div>

                <div class="col-md-6">
                  <div class="form-group">  
                    <label>Jenis Produksi</label>
                    <input readonly type="text" class="form-control" placeholder="Jenis Produksi" name="jenis_produksi" id="jenis_produksi" required=""/>
                  </div>

                </div>

              </div>
            </div> 

            <div class="sukses" ></div>
            <div class="box-body update_item">
              <div class="row">
                <div class="">
                  <div class="col-md-2 sendiri" id="sendiri">
                    <label>Pilih Karyawan</label>
                    <select  name="kode_karyawan"  id="karyawan" required class="form-control select2" readonly>
                      <option value="">--Pilih Karyawan--</option>
                      <?php
                      $karyawan = $this->db->query(" SELECT * FROM master_karyawan where status='1'");
                      $hasil_karyawan = $karyawan->result();
                      foreach ($hasil_karyawan as $data) {
                        ?>
                        <option value="<?php echo $data->kode_karyawan; ?>"><?php echo $data->nama_karyawan; ?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="col-md-2 plasma" id="plasma">
                   <label>Pilih Plasma</label>
                   <select  name="kode_plasma"  id="kode_plasma" required class="form-control select2" readonly>
                    <option value="">--Pilih Plasma--</option>
                    <?php
                    $plasma = $this->db->query(" SELECT * FROM master_plasma where status='1'");
                    $hasil_plasma = $plasma->result();
                    foreach ($hasil_plasma as $data) {
                      ?>
                      <option value="<?php echo $data->kode_plasma; ?>"><?php echo $data->nama_plasma; ?></option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
                <div class="col-md-2" >
                  <label>Jenis bahan</label>
                  <select onchange="get_bahan()" name="jenis_bahan" id="jenis_bahan" class="form-control" readonly>
                    <option value="" >--Pilih Jenis bahan--</option>
                    <option  value="Bahan Setengah Jadi">Bahan Setengah Jadi</option>                     
                    <option value="Bahan Jadi">Bahan Jadi</option> 
                  </select>

                </div> 

                <div class="col-md-2">
                  <label>Nama Produk</label>
                  <select id="kode_bahan" name="kode_bahan" class="form-control select2" readonly>
                    <option value="">Pilih Produk</option>

                  </select>
                </div>
                <input type="hidden" id="nama_bahan" name="nama_bahan" />
                <div class="col-md-2">
                  <label>QTY</label>
                  <input type="text" class="form-control" placeholder="QTY" name="jumlah" id="jumlah" />
                </div>
                <!--   <div class="col-md-2">
                    <label>Satuan</label>
                    <input type="text" readonly="true" class="form-control" placeholder="Satuan produksi" name="nama_satuan" id="nama_satuan" />
                    <input type="hidden" name="kode_satuan" id="kode_satuan" />
                  </div>
                  <div class="col-md-2">
                    <label>Harga Satuan</label>
                    <input type="text" class="form-control" placeholder="Harga Satuan" name="harga_satuan" id="harga_satuan" />
                    
                  </div> -->

                  <input type="hidden" name="id_item" id="id_item" />
                  <div class="col-md-1" style="padding-top:25px;">
                    <!-- <div onclick="add_item()" id="add"  class="btn btn-primary pull-right" hidden>Add</div> -->
                    <div onclick="update_item()" id="update"  class="btn btn-primary pull-right">Update</div>
                  </div>
                </div>
              </div>
            </div>

            <div id="list_transaksi_produksi">
              <div class="box-body"><br><br><br>
                <table id="tabel_daftar" class="table table-bordered table-hover" style="font-size:1.5em;">
                  <thead>
                    <tr>
                      <th width="30px">No</th>
                      <th>Nama</th>
                      <th>Nama bahan</th>
                      <th>QTY</th>
                      <th>Harga(Tarif Borongan)</th>
                      <th>Subtotal</th>

                      <th >Action</th>
                    </tr>
                  </thead>
                  <tbody id="tabel_temp_data_transaksi">

                  </tbody>
                  <tfoot>

                  </tfoot>
                </table>
              </div>
            </div>

            <br><br>
            <center><a onclick="confirm_produksi()"  class="btn btn-success btn-lg " style="width:200px;"><i class="fa fa-save"></i> Simpan</a></center>
            <div class="box-footer clearfix">

            </div>
          </form>
          <!--  -->
        </div>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------------------------------------------------------------>
<!-- Content Wrapper. Contains page content -->
<!-- /.content-wrapper -->
<div id="modal-regular" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="cari_produksi" method="post">
        <div class="modal-header" style="background-color:grey">

          <h4 class="modal-title" style="color:#fff;">Produksi</h4>
        </div>
        <div class="modal-body" >
          <div class="form-body">

           <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Kode Perintah Produksi</label>
                <input type="text" id="kode_perintah" name="kode_perintah" class="form-control" placeholder="Kode Perintah" required="">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer" style="background-color:#eee">
        <a href="<?php echo base_url().'produksi/daftar'; ?>" class="btn blue" aria-hidden="true">Cancel</a>
        <button type="submit" class="btn green">Cari</button>
      </div>
    </form>
  </div>
</div>
</div>
</div>
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">

        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan menghapus produksi Produk tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()"   class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>

<div id="modal-confirm-produksi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Produksi</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:12pt">Apakah anda yakin akan produksi ini  ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn red" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button id="simpan_transaksi"  class="btn green">Ya</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

  $(document).ready(function(){
    $("#update").hide();
    $("#update_barang").hide();
    $("#modal-regular").modal({
      backdrop:'static',
      keyboard:false
    })
    $("#modal-regular").modal('show');

    $(".plasma").hide();
    $(".update_item").hide();
    $(".sendiri").hide();


    $("#cari_produksi").submit(function(){
      var kode_produksi = $('#kode_produksi').val();  
      var kode_perintah = $('#kode_perintah').val();  
      var keterangan = "<?php echo base_url().'produksi/get_kode_perintah'?>";
      $('#kode_p2').val(kode_perintah);
      $.ajax({
        type: "POST",
        url: keterangan,
        data: {kode_produksi:kode_produksi,kode_perintah:kode_perintah},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success: function(msg)
        {
          var data = msg.split("|");
          if(data[0] == 1){  

            $("#tabel_temp_data_transaksi").load("<?php echo base_url().'produksi/get_produksi_temp/'; ?>"+kode_produksi);
            $('#modal-regular').modal('hide');
            $("#hasil_kode_perintah").val(data[1]);
            $("#jenis_produksi").val(data[2]);
            if(data[2]=='plasma'){
              $(".plasma").show();
              $(".sendiri").hide();

              $("#kode_plasma").val(data[3]);
              $("#nama_plasma").val(data[4]);
              $("#kode_karyawan").val('');
              $("#nama_karyawan").val('');
              $("#tabel_temp_barang_lain").load("<?php echo base_url().'produksi/get_barang_lain_temp/'; ?>"+kode_produksi);
            }else if(data[2]=='sendiri'){
              $(".plasma").hide();
              $(".sendiri").show();

              $("#kode_karyawan").val(data[3]);
              $("#nama_karyawan").val(data[4]);
              $("#kode_plasma").val('');
              $("#nama_plasma").val('');
            }

            $(".tunggu").hide();
          }
          else{
            alert('Kode Perintah Produksi Tidak Ditemukan');            
            $('#kode_supplier_awal').val('');
            $(".tunggu").hide();

          }  

        }
      });
      return false;
    });




    $("#kode_bahan").change(function(){
      var kode_bahan = $(this).val();
      var jenis_bahan = $('#jenis_bahan').val();

      var url = "<?php echo base_url().'produksi/get_kode_bahan'; ?>";
      $.ajax({
        type: "POST",
        url: url,
        dataType:'json',
        data: {kode_bahan:kode_bahan,jenis_bahan:jenis_bahan},
        success: function(pilihan) {    
          if(pilihan.nama_bahan_setengah_jadi){
            $("#nama_bahan").val(pilihan.nama_bahan_setengah_jadi);
          }else if(pilihan.nama_bahan_jadi){

            $("#nama_bahan").val(pilihan.nama_bahan_jadi);
          }         

        }
      });
    });

    $("#kode_barang_lain").change(function(){
      var kode_barang_lain = $(this).val();

      var url = "<?php echo base_url().'produksi/get_kode_barang_lain'; ?>";
      $.ajax({
        type: "POST",
        url: url,
        dataType:'json',
        data: {kode_barang_lain:kode_barang_lain},
        success: function(barang) {    
          if(barang.nama_bahan_baku){
            $("#nama_barang_lain").val(barang.nama_bahan_baku);
          }      

        }
      });
    });


  });


  $("#simpan_transaksi").click(function(){
    var simpan_transaksi = "<?php echo base_url().'produksi/simpan_transaksi/'?>";
    angsuran_piutang();
    $.ajax({
      type: "POST",
      url: simpan_transaksi,
      data: $('#data_form').serialize(),
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#modal-confirm-produksi").modal('hide');
        data_str=msg.split('|');
        if(data_str[0]==1){
          $('.sukses').html(data_str[1]);
          setTimeout(function(){$('.sukses').html('');window.location = "<?php echo base_url() . 'produksi/daftar' ?>";},1500);  
        }else{
          $('.sukses').html(msg);
        }
      //window.location = "<?php echo base_url() . 'produksi/daftar' ?>";
    }
  });
    return false;

  });

  function angsuran_piutang(){
   var kode_produksi = $('#kode_produksi').val();
   var jenis_produksi = $('#jenis_produksi').val();

   var url = "<?php echo base_url().'produksi/angsuran_piutang'; ?>";
   $.ajax({
    type: "POST",
    url: url,
    data: {kode_produksi:kode_produksi,jenis_produksi:jenis_produksi},
    success: function(pilihan) {              
    //$("#kode_bahan").html(pilihan);
  }
});
 }
 function get_bahan(){
   var jenis_bahan = $("#jenis_bahan").val();
   var jenis_produksi = $('#jenis_produksi').val();
   var url = "<?php echo base_url().'produksi/get_bahan'; ?>";
   $.ajax({
    type: "POST",
    url: url,
    data: {jenis_bahan:jenis_bahan,jenis_produksi:jenis_produksi},
    success: function(pilihan) {              
      $("#kode_bahan").html(pilihan);
    }
  });
 }

 function add_item(){
  var kode_produksi = $('#kode_produksi').val();
  var jenis_produksi = $('#jenis_produksi').val();
  var jenis_bahan = $('#jenis_bahan').val();
  var kode_bahan = $('#kode_bahan').val();
  var jumlah = $('#jumlah').val();
  var kode_satuan = $('#kode_satuan').val();
  var nama_satuan = $("#nama_satuan").val();
  var harga_satuan = $('#harga_satuan').val();
  var nama_bahan = $("#nama_bahan").val();
  var url = "<?php echo base_url().'produksi/simpan_produksi_temp/'?> ";
  
  $.ajax({
    type: "POST",
    url: url,
    data: { kode_produksi:kode_produksi,
      jenis_produksi:jenis_produksi,
      jenis_bahan:jenis_bahan,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      jumlah:jumlah,
      kode_satuan:kode_satuan,  
      nama_satuan:nama_satuan,
      harga_satuan:harga_satuan,

    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(data)
    {
      $(".tunggu").hide();

      $('.sukses').html('');     
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'produksi/get_produksi_temp/'; ?>"+kode_produksi);
      $('#kode_bahan').val('');
      $('#jumlah').val('');
      $("#nama_satuan").val('');
      $('#harga_satuan').val('');
      $("#nama_bahan").val('');
      $("#jenis_bahan").val('');
    }
  });
}
function actEdit(id) {
  var id = id;
  var kode_produksi =$('#kode_produksi').val();
  var url = "<?php echo base_url().'produksi/get_temp_produksi'; ?>";
  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: {id:id,kode_produksi:kode_produksi},
    success: function(produksi){
      $(".update_item").show();
      $('#jenis_bahan').val(produksi.kategori_bahan);
         //$('#kode_bahan').val(produksi.kode_bahan); 
         $('#kode_bahan').html("<option value="+produksi.kode_bahan+" selected='true'>"+produksi.nama_bahan+"</option>");        
         if(produksi.jenis_produksi=='plasma'){
          $('#kode_plasma').html("<option value="+produksi.kode_karyawan+" selected='true'>"+produksi.nama_karyawan+"</option>");        
        }

        if(produksi.jenis_produksi=='sendiri'){
          //alert(produksi.jenis_produksi);
          $('#karyawan').html("<option value="+produksi.kode_karyawan+" selected='true'>"+produksi.nama_karyawan+"</option>");        
        }

        //$("#kode_bahan").val(produksi.kode_bahan);
        $("#nama_bahan").val(produksi.nama_bahan);
        $('#jumlah').val(produksi.jumlah);
        // $('#kode_satuan').val(produksi.kode_satuan);
        // $("#nama_satuan").val(produksi.nama_satuan);
        // $('#harga_satuan').val(produksi.harga_satuan);
        $("#id_item").val(produksi.id);
        
        $("#add").hide();
        $("#update").show();
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'produksi/get_produksi_temp/'; ?>"+kode_produksi);
      }
    });
}


$("#jumlah").keyup(function(){

 var jumlah = $('#jumlah').val();

 if (jumlah <  0 ){
  alert("Silahkan cek inputan quantitynya !");
  $('#jumlah').val('');
}

});

function update_item(){
 var kode_produksi = $('#kode_produksi').val();
 var kode_perintah = $('#kode_perintah').val();
 var jenis_produksi = $('#jenis_produksi').val();
 var jenis_bahan = $('#jenis_bahan').val();
 var kode_bahan = $('#kode_bahan').val();
 var jumlah = $('#jumlah').val();
 var kode_satuan = $('#kode_satuan').val();
 var nama_satuan = $("#nama_satuan").val();
 var harga_satuan = $('#harga_satuan').val();
 var nama_bahan = $("#nama_bahan").val();
 var id_item = $("#id_item").val();
 var url = "<?php echo base_url().'produksi/update_produksi_temp/'?> ";

//  if (jumlah <=   0 ){
//   alert("Mohon isi QTY dengan benar !!!!");
//   $('#jumlah').val('');
// }else{
 $.ajax({
  type: "POST",
  url: url,
  data: { 
    id:id_item,
    kode_produksi:kode_produksi,
    kode_perintah:kode_perintah,
    jenis_produksi:jenis_produksi,
    jenis_bahan:jenis_bahan,
    kode_bahan:kode_bahan,
    nama_bahan:nama_bahan,
    jumlah:jumlah,
    kode_satuan:kode_satuan,  
    nama_satuan:nama_satuan,
    harga_satuan:harga_satuan,

  },
  success: function(data)
  {
    $(".update_item").hide();
    $(".tunggu").hide();

    $('.sukses').html(data);
    setTimeout(function(){$('.sukses').html('');},1500);  

    
    $("#tabel_temp_data_transaksi").load("<?php echo base_url().'produksi/get_produksi_temp/'; ?>"+kode_produksi);
    $('#kode_bahan').val('');
    $('#jumlah').val('');
    $("#nama_satuan").val('');
    $('#harga_satuan').val('');
    $("#nama_bahan").val('');
    $("#jenis_bahan").val('');
    $("#add").hide();
    $("#update").hide();
  }
});
// }
}
function actDelete(Object) {
  $('#id-delete').val(Object);
  $('#modal-confirm').modal('show');
}
function delData() {
  var id = $('#id-delete').val();
  var kode_produksi = $('#kode_produksi').val();
  var url = '<?php echo base_url().'produksi/hapus_produksi_temp'; ?>/delete';
  $.ajax({
    type: "POST",
    url: url,
    data: {
      id:id,kode_produksi:kode_produksi
    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(msg) {
      $(".tunggu").hide();
      $('#modal-confirm').modal('hide');
      $("#tabel_temp_data_transaksi").load("<?php echo base_url().'produksi/get_produksi_temp/'; ?>"+kode_produksi);
      $("#tabel_temp_barang_lain").load("<?php echo base_url().'produksi/get_barang_lain_temp/'; ?>"+kode_produksi);
    }
  });
  return false;
}
function add_item_barang(){
  var kode_produksi = $('#kode_produksi').val();
  var jenis_produksi = $('#jenis_produksi').val();
  var jenis_barang_lain = $('#jenis_barang_lain').val();
  var kode_bahan = $('#kode_barang_lain').val();
  var nama_bahan = $("#nama_barang_lain").val();
  var jumlah = $('#jumlah_barang_lain').val();

  
  var url = "<?php echo base_url().'produksi/simpan_barang_lain_temp/'?> ";
  
  $.ajax({
    type: "POST",
    url: url,
    data: { kode_produksi:kode_produksi,
      jenis_produksi:jenis_produksi,
      jenis_barang_lain:jenis_barang_lain,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      jumlah:jumlah,


    },
    beforeSend:function(){
      $(".tunggu").show();  
    },
    success: function(data)
    {
      $(".tunggu").hide();

      $('.sukses').html('');     
      $("#tabel_temp_barang_lain").load("<?php echo base_url().'produksi/get_barang_lain_temp/'; ?>"+kode_produksi);
      $('#kode_barang_lain').val('');
      $('#jumlah_barang_lain').val('');
      $("#nama_barang_lain").val('');
      // $("#nama_satuan").val('');
      // $('#harga_satuan').val('');
    }
  });
}
function actEditbarang(id) {
  var id = id;
  var kode_produksi =$('#kode_produksi').val();
  var url = "<?php echo base_url().'produksi/get_temp_barang_lain'; ?>";
  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: {id:id,kode_produksi:kode_produksi},
    success: function(barang_lain){
      $('#jenis_bahan').val(barang_lain.kategori_bahan);
      $('#kode_barang_lain').val(barang_lain.kode_bahan);
        //$("#kode_bahan").val(barang_lain.kode_bahan);
        $("#nama_barang_lain").val(barang_lain.nama_bahan);
        $('#jumlah_barang_lain').val(barang_lain.jumlah);
        // $('#kode_satuan').val(barang_lain.kode_satuan);
        // $("#nama_satuan").val(barang_lain.nama_satuan);
        // $('#harga_satuan').val(barang_lain.harga_satuan);
        $("#id_item_barang_lain").val(barang_lain.id);
        
        $("#add_barang").hide();
        $("#update_barang").show();
        $("#tabel_temp_data_transaksi").load("<?php echo base_url().'produksi/get_produksi_temp/'; ?>"+kode_produksi);
      }
    });
}
function update_item_barang(){
  var kode_produksi = $('#kode_produksi').val();
  var jenis_produksi = $('#jenis_produksi').val();
  var jenis_barang_lain = $('#jenis_barang_lain').val();
  var kode_bahan = $('#kode_barang_lain').val();
  var nama_bahan = $("#nama_barang_lain").val();
  var jumlah = $('#jumlah_barang_lain').val();

  var id_item = $("#id_item_barang_lain").val();
  var url = "<?php echo base_url().'produksi/update_barang_lain_temp/'?> ";

  $.ajax({
    type: "POST",
    url: url,
    data: { 
      id:id_item,
      kode_produksi:kode_produksi,
      jenis_produksi:jenis_produksi,
      jenis_barang_lain:jenis_barang_lain,
      kode_bahan:kode_bahan,
      nama_bahan:nama_bahan,
      jumlah:jumlah,

    },
    success: function(data)
    {
     $('.sukses').html('');     
     $("#tabel_temp_barang_lain").load("<?php echo base_url().'produksi/get_barang_lain_temp/'; ?>"+kode_produksi);

      //$('#kode_barang_lain').html();
      $('#kode_barang_lain').val('');
      $('#jumlah_barang_lain').val('');
      $("#nama_barang_lain").val('');
    }
  });
}
function confirm_produksi(){
  $("#modal-confirm-produksi").modal('show');
}
</script>

