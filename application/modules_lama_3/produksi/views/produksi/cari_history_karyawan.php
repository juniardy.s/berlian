<table class="table table-striped table-hover table-bordered" id="daftar_produksi"  style="font-size:1.5em;">
 <?php
         //   $this->db->order_by('id', 'desc');
         // //$this->db->where('position','gudang');
 $kode_karyawan=$this->input->post('kode_karyawan');
 if(!empty($kode_karyawan)){
  $this->db->where('kode_karyawan',$kode_karyawan);
 }
 
 $produksi = $this->db->get('master_karyawan');
 $hasil_produksi = $produksi->result();
 ?>
 <thead>
  <tr>
    <th>No</th>
    <th>Kode Karyawan</th>
    <th>Nama Karyawan</th>
    <th>Action</th>
  </tr>
</thead>
<tbody>
  <?php
  $nomor = 1;

  foreach($hasil_produksi as $daftar){ 
            //$jml_produksi=$this->db->get_where('opsi_transaksi_produksi');
    ?> 
    <tr>
      <td><?php echo $nomor; ?></td>
      <td><?php echo @$daftar->kode_karyawan; ?></td>
      <td><?php echo @$daftar->nama_karyawan; ?></td>

      <td align="center">
        <a href="<?php echo base_url().'produksi/detail_history_produksi/'.$daftar->kode_karyawan ?>" data-toggle="tooltip" title="Detail" class="btn btn-icon-only btn-circle green"><i class="fa fa-search"></i></a>
      </td>
    </tr>
    <?php $nomor++; } ?>

  </tbody>
  <tfoot>
    <tr>
     <th>No</th>
     <th>Kode Karyawan</th>
     <th>Nama Karyawan</th>
     <th>Action</th>
   </tr>
 </tfoot>
</table>