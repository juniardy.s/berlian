
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->

<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Save changes</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<!-- BEGIN STYLE CUSTOMIZER -->

	<!-- END STYLE CUSTOMIZER -->
	<!-- BEGIN PAGE HEADER-->
	<h3 class="page-title">
		Menu</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="#">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Produksi</a>
				</li>
			</ul>
			<div class="page-toolbar">

			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light purple-soft" id="perintah_produksi">
					<div class="visual">
						<i class="glyphicon glyphicon-taskss" ></i>
					</div>
					<div class="details">
						<div class="number">

						</div>
						<div class="desc">
							Perintah Produksi
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light green-soft"  id="produksi">
					<div class="visual">
						<i class="glyphicon glyphicon-taskss" ></i>
					</div>
					<div class="details">
						<div class="number">

						</div>
						<div class="desc">
							Produksi
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light red"  id="kebutuhan_plasma">
					<div class="visual">
						<i class="glyphicon glyphicon-taskss" ></i>
					</div>
					<div class="details">
						<div class="number">

						</div>
						<div class="desc">
							Kebutuhan Plasma
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light yellow"  id="history_produksi">
					<div class="visual">
						<i class="glyphicon glyphicon-taskss" ></i>
					</div>
					<div class="details">
						<div class="number">

						</div>
						<div class="desc">
							History Produksi
						</div>
					</div>
				</a>
			</div>


		</div>
		<!-- END DASHBOARD STATS -->
		<div class="clearfix">
		</div>
		<div class="row">

		</div>
		<!--  -->                                        
		<!-- END QUICK SIDEBAR -->
	</div>    

	<script type="text/javascript">
	$(document).ready(function(){

		$("#produksi").click(function(){
			$('.tunggu').show();
			window.location = "<?php echo base_url() . 'produksi/daftar' ?>";

		});
		$("#plasma").click(function(){
			$('.tunggu').show();
			window.location = "<?php echo base_url() . 'master/plasma/' ?>";

		});

		$("#rak").click(function(){
			$('.tunggu').show();
			window.location = "<?php echo base_url() . 'master/rak/' ?>";

		});

		$('#perintah_produksi').click(function(){
			$('.tunggu').show();
			window.location = "<?php echo base_url().'perintah_produksi/' ?>";
		});

		$('#kebutuhan_plasma').click(function(){
			$('.tunggu').show();
			window.location = "<?php echo base_url().'input_plasma/' ?>";
		});
		$('#history_produksi').click(function(){
			$('.tunggu').show();
			window.location = "<?php echo base_url().'produksi/history_produksi' ?>";
		});

	});
	</script>
