
<div class="row">      
  <div class="col-xs-12">
    <!-- /.box -->
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Spoil Bahan Setengah Jadi
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
          <a href="javascript:;" class="reload">
          </a>

        </div>
      </div>
      <div class="portlet-body">

        <!------------------------------------------------------------------------------------------------------>

        <div class="row"> 

          <div class="col-md-12">

            <!-- <a style="padding:13px; margin-bottom:10px;" class="btn btn-app green" href="<?php echo base_url().'spoil/tambah_spoil/'; ?>"><i class="fa fa-edit"></i> Tambah </a> -->

            <div class="box-body">            
              <div class="sukses" ></div>
              <div class="col-md-10 row" id="">
                <div class="col-md-5 row" id="">
                  <div class="input-group">
                    <span class="input-group-addon">Filter</span>
                    <select class="form-control" id="filter">
                      <option value="">- PILIH -</option>
                      <!-- <option value="kategori">Kategori Produk</option> -->
                      <option value="blok">Kategori</option>
                    </select>
                  </div>
                  <br>
                </div>
              </div>
              
              <div class="col-md-10 row" id="opsi_filter">
                <div class="col-md-5 row" id="">
                  <div class="input-group">
                    <span class="input-group-addon" id="nama_input_opsi"></span>
                    <select class="form-control" id="option_opsi_filter">
                      <option value="">- PILIH -</option>
                      <option value="kategori">Kategori Produk</option>
                      <option value="blok">Blok</option>
                    </select>
                  </div>
                  <br>
                </div>                        
              </div>                        
              <div class="col-md-10 row" id="">
                <div class="col-md-5 row" id="">
                  <button style="width: 148px" type="button" class="btn btn-warning pull-right" id="cari"><i class="fa fa-search"></i> Cari</button>
                </div>
              </div>  
              <br><br>
              
              <form method="post" id="opsi_spoil">
                <a style="padding:13px; margin-bottom:10px; margin-right:0px;" id="spoil_tambah" class="btn btn-app green pull-right" ><i class="fa fa-edit"></i> Spoil </a>
                <div class="col-md-12 row" id="cari_transaksi">
                  <table class="table table-striped table-hover table-bordered" id="tabel_daftarr" style="font-size:1.5em;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Nama Kategori</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="table_bahan_setengah_jadi">
                      <?php
                      $kode_default = $this->db->get('setting_gudang');
                      $hasil_unit =$kode_default->row();
                      $param =$hasil_unit->kode_unit;
                      $this->db->limit(100);
                      $this->db->where('status','sendiri');
                      $spoil = $this->db->get_where('master_bahan_setengah_jadi',array('kode_unit' => $param,'real_stock >' => 0));
                      $list_spoil = $spoil->result();
                      $nomor = 1;  
                      
                      foreach($list_spoil as $daftar){
                        ?> 
                        <tr>
                          <td><?php echo $nomor; ?></td>
                          <td><?php echo $daftar->nama_bahan_setengah_jadi; ?></td>
                          <td><?php echo $daftar->real_stock.' '.$daftar->satuan_stok; ?></td>
                          <td><?php echo $daftar->nama_rak; ?></td>
                          <td align="center"><input name="opsi_spoil[]" type="checkbox"  id="opsi_pilihan" value="<?php echo $daftar->kode_bahan_setengah_jadi; ?>"></td>
                        </tr>
                        <?php 
                        $nomor++; 
                      } 
                      ?>

                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Nama Kategori</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>            
                  </table>
                  <input type="hidden" name="kode_unit" id="kode_unit" value="<?php echo $param ?>">
                  <?php
                  $kode_spoil = $this->uri->segment(3);

                  $kode_default = $this->db->get('setting_gudang');
                  $hasil_unit =$kode_default->row();
                  $kode_unit = $hasil_unit->kode_unit; 
                  $unit = $this->db->get_where('master_unit',array('kode_unit' => $kode_unit));
                  $hasil_unite = $unit->row();

                  $this->db->select_max('id');
                  $this->db->where('jenis_bahan', "bahan_setengah_jadi");
                  $get_max_po = $this->db->get('transaksi_spoil');
                  $max_po = $get_max_po->row();

                  $this->db->where('id', $max_po->id);
                  $get_po = $this->db->get('transaksi_spoil');
                  $po = $get_po->row();
                  // $tahun = substr(@$po->kode_spoil, 3,4);
                  // if(date('Y')==$tahun){
                  $nomor = substr(@$po->kode_spoil, 18);
                  $nomor = $nomor + 1;
                  $string = strlen($nomor);
                  if($string == 1){
                    $kode_trans = 'SPSJ_'.date('ymdhis').'_000'.$nomor;
                  } else if($string == 2){
                    $kode_trans = 'SPSJ_'.date('ymdhis').'_00'.$nomor;
                  } else if($string == 3){
                    $kode_trans = 'SPSJ_'.date('ymdhis').'_0'.$nomor;
                  } else if($string == 4){
                    $kode_trans = 'SPSJ_'.date('ymdhis').'_0'.$nomor;
                  }
                  // } else {
                  //   $kode_trans = 'SP_'.date('Y').'_000001';
                  // }

                  ?>
                  <input type="hidden" name="kode_spoil" id="kode_spoil" value="<?php echo @$kode_trans; ?>">
                </div>
                <a style="padding:13px; margin-bottom:10px; margin-right:0px;" id="spoil_tambah" class="btn btn-app green pull-right" ><i class="fa fa-edit"></i> Spoil </a>
                <br><br><br><br><br><br><br><br>
                <br><br><br><br><br><br><br><br>
              </form>
              <div class="row" id="" style="height:100px">
                <?php 
                $get_jumlah = $this->db->get_where('master_bahan_setengah_jadi', array('kode_unit' => $param,'real_stock >' => 0,'status'=>'sendiri'));
                $jumlah = $get_jumlah->num_rows();
                $jumlah = floor($jumlah/100);
                ?>
                <input type="hidden" class="form-control rowcount" value="<?php echo $jumlah ?>">
                <input type="hidden" class="form-control pagenum" value="0">
                
                <!-- <button type="button" class="btn btn-success btn-block pull-right" id="spoil_tambah"><font size="6"><b>SPOIL</b></font></button> -->
              </div>  
            </div>
          </div>
          <!------------------------------------------------------------------------------------------------------>

        </div>
      </div>
    </div><!-- /.col -->
  </div>
</div>    
<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:grey">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="color:#fff;">Konfirmasi Hapus Data</h4>
      </div>
      <div class="modal-body">
        <span style="font-weight:bold; font-size:14pt">Apakah anda yakin akan menghapus data menu tersebut ?</span>
        <input id="id-delete" type="hidden">
      </div>
      <div class="modal-footer" style="background-color:#eee">
        <button class="btn green" data-dismiss="modal" aria-hidden="true">Tidak</button>
        <button onclick="delData()" class="btn red">Ya</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url().'component/lib/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'component/lib/zebra_datepicker.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'component/lib/css/default.css'?>"/>
<style type="text/css" media="screen">
  .btn-back
  {
    position: fixed;
    bottom: 10px;
    left: 10px;
    z-index: 999999999999999;
    vertical-align: middle;
    cursor:pointer
  }
</style>
<img class="btn-back" src="<?php echo base_url().'component/img/back_icon.png'?>" style="width: 70px;height: 70px;">

<script>
  $('.btn-back').click(function(){
    window.location = "<?php echo base_url().'stok/daftar_menu'; ?>";
  });
</script>
<script type="text/javascript">
  $(window).scroll(function(){
    if (Math.round($(window).scrollTop()) == ($(document).height() - $(window).height())){
      if(parseInt($(".pagenum").val()) <= parseInt($(".rowcount").val())) {
        var pagenum = parseInt($(".pagenum").val()) + 1;
        $(".pagenum").val(pagenum);
        load_table(pagenum);
      }
    }
  });

  function load_table(page){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url() . 'spoil/spoil_s_jadi/get_table' ?>",
      data: ({page:$(".pagenum").val()}),
      beforeSend: function(){
        $(".tunggu").show();  
      },
      success: function(msg)
      {
        $(".tunggu").hide();
        $("#table_bahan_setengah_jadi").append(msg);

      }
    });
  }

  $('#filter').change(function(){
    filter = $('#filter').val();
    if(filter != ""){
      $("#opsi_filter").show();
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'spoil/spoil_s_jadi/get_opsi_filter'; ?>",  
        cache :false,
        data : {filter:filter},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
          $(".tunggu").hide();  
          $("#option_opsi_filter").html(data);
          if(filter == "kategori"){
            $("#nama_input_opsi").html("Kategori Produk");
          } else if(filter == "blok"){
            $("#nama_input_opsi").html("Kategori");
          }
        },  
        error : function(data) {
        }  
      });
    } else {
      $("#opsi_filter").hide();
    }
  });

  $('.tgl').Zebra_DatePicker({});

  $('#spoil_tambah').click(function(){
    checkedValue = $('#opsi_pilihan:checked').val();
    kode_spoil = $('#kode_spoil').val();
    if(!checkedValue){
      alert("Pilih Barang Yang Akan Di Spoil");
    } else {
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'spoil/spoil_s_jadi/simpan_spoil_temp_baru'; ?>",  
        cache :false,
        data : $("#opsi_spoil").serialize(),
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
          $(".tunggu").hide();  
          setTimeout(function(){
            window.location = "<?php echo base_url() . 'spoil/spoil_s_jadi/tambah_spoil/'; ?>"+kode_spoil;
          },15);       
        },  
        error : function(data) {
        }  
      });
    }

  });

  $('#cari').click(function(){

    filter = $('#filter').val();
    opsi_filter = $('#option_opsi_filter').val();
    kode_unit =$("#kode_unit").val();
    if (filter=='' || filter==''){ 
      alert('Masukan Fiter & Kategori Produk atau blok..!')
    }
    else{
      $.ajax( {  
        type :"post",  
        url : "<?php echo base_url().'spoil/spoil_s_jadi/cari_produk'; ?>",  
        cache :false,
        data : {filter:filter,opsi_filter:opsi_filter,kode_unit:kode_unit},
        beforeSend:function(){
          $(".tunggu").show();  
        },
        success : function(data) {
          $(".tunggu").hide();  
          $("#cari_transaksi").html(data);

          $("#opsi_filter").hide();
        },  
        error : function(data) {
        }  
      });
    }

    $('#filter').val('');
    $('#option_opsi_filter').val('');

  });

</script>
<script>
  $(document).ready(function(){
    $("#opsi_filter").hide();

    $("#tabel_daftar").dataTable({
      "paging":   false,
      "ordering": true,
      "info":     false
    });
  })

  function actDelete(Object) {
    $('#id-delete').val(Object);
    $('#modal-confirm').modal('show');
  }

  function delData() {
    var id = $('#id-delete').val();
    var url = '<?php echo base_url().'master/menu_resto/hapus_bahan_jadi'; ?>/delete';
    $.ajax({
      type: "POST",
      url: url,
      data: {
        id: id
      },
      beforeSend:function(){
        $(".tunggu").show();  
      },
      success: function(msg) {
        $('#modal-confirm').modal('hide');
        window.location.reload();
      }
    });
    return false;
  }

</script>
