<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bahan_setengah extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at h  ttp://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();		
		if ($this->session->userdata('astrosession') == FALSE) {
			redirect(base_url('authenticate'));			
		}
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_setengah/daftar_bahan_setengah', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_setengah/menu', $data, TRUE);
        $this->load->view('bahan_setengah/main', $data);	

    }

    public function menu()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('master/menu', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_setengah/menu', $data, TRUE);
        $this->load->view('bahan_setengah/main', $data);		
    }

    public function tambah()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_setengah/tambah_bahan_setengah', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_setengah/menu', $data, TRUE);
        $this->load->view('bahan_setengah/main', $data);		
    }
    
    public function ubah()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_setengah/ubah_bahan_setengah', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_setengah/menu', $data, TRUE);
        $this->load->view('bahan_setengah/main', $data);		
    }
    
    public function detail()
    {
        $data['aktif']='master';
        $data['konten'] = $this->load->view('bahan_setengah/detail_bahan_setengah', NULL, TRUE);
        $data['halaman'] = $this->load->view('bahan_setengah/menu', $data, TRUE);
        $this->load->view('bahan_setengah/main', $data);		
    }
    public function table_temp()
    {
        $this->load->view('bahan_setengah/table_temp');        
    }

    public function table_opsi()
    {
        $this->load->view('bahan_setengah/table_temp');        
    }

    public function table_detail()
    {
        $this->load->view('bahan_setengah/table_detail');        
    }

    public function get_satuan()
    {
        $bahan_baku = $this->input->post('kode_bahan_baku');
        $this->db->where('kode_bahan_baku', $bahan_baku);
        $this->db->where('status','sendiri');
        $get_bahan_baku = $this->db->get('master_bahan_baku');
        $hasil_bb = $get_bahan_baku->row();
        echo json_encode($hasil_bb);
    }
    

    public function tambah_temp()
    {
        $input = $this->input->post();

        $get_setengah_jadi=$this->db->get_where('opsi_master_bahan_setengah_jadi_temp',array('kode_bahan'=>$input['bahan_baku'],'kode_bahan_setengah_jadi'=>$input['kode_bahan_setengah']));
        $hasil_get_setengah_jadi=$get_setengah_jadi->row();
        if (count($hasil_get_setengah_jadi)=='1') {

            $jumlah_awal=$hasil_get_setengah_jadi->jumlah;
            $data_update['jumlah']=$jumlah_awal + $input['jumlah'];

            $this->db->update("opsi_master_bahan_setengah_jadi_temp", $data_update, array('kode_bahan' => $input['bahan_baku'],'kode_bahan_setengah_jadi'=>$input['kode_bahan_setengah']));
            echo $this->db->last_query();
        }else{
           $data['kode_bahan_setengah_jadi'] = $input['kode_bahan_setengah'];
           $data['kode_bahan'] = $input['bahan_baku'];
           $this->db->where('kode_bahan_baku', $data['kode_bahan']);
           $get_bahan_baku = $this->db->get('master_bahan_baku');
           $hasil_bb = $get_bahan_baku->row();
           $data['nama_bahan'] = $hasil_bb->nama_bahan_baku;
           $data['jenis_bahan'] = 'bahan baku';
           $data['jumlah'] = $input['jumlah'];
           $data['id_satuan'] = $input['id_satuan'];
           $data['nama_satuan'] = $input['satuan'];
           $data['hpp'] = $input['hpp'];
           $data['rumus'] = $input['rumus'];
           $data['harga'] = $input['harga'];
           $this->db->insert('opsi_master_bahan_setengah_jadi_temp', $data);
       }

   }
   public function tambah_opsi()
   {
    $input = $this->input->post();
    
    $data['kode_bahan_setengah_jadi'] = $input['kode_bahan_setengah'];
    $data['kode_bahan'] = $input['bahan_baku'];
    $this->db->where('kode_bahan_baku', $data['kode_bahan']);
    $get_bahan_baku = $this->db->get('master_bahan_baku');
    $hasil_bb = $get_bahan_baku->row();
    $data['nama_bahan'] = $hasil_bb->nama_bahan_baku;
    $data['jenis_bahan'] = 'bahan baku';
    $data['jumlah'] = $input['jumlah'];
    $data['id_satuan'] = $input['id_satuan'];
    $data['nama_satuan'] = $input['satuan'];
    $data['hpp'] = $input['hpp'];
    $data['rumus'] = $input['rumus'];
    $data['harga'] = $input['harga'];
    $this->db->where('kode_bahan_setengah_jadi',$input['kode_bahan_setengah']);
    $get_setengah_jadi=$this->db->get_where('opsi_master_bahan_setengah_jadi',array('kode_bahan'=>$input['bahan_baku']));
    $hasil_get_setengah_jadi=$get_setengah_jadi->row();
    if(count($hasil_get_setengah_jadi)=="1"){
        $jumlah_awal=$hasil_get_setengah_jadi->jumlah;
        $data_update['jumlah']=$jumlah_awal + $input['jumlah'];
        $this->db->update("opsi_master_bahan_setengah_jadi", $data_update, array('kode_bahan' => $input['bahan_baku'],
            'kode_bahan_setengah_jadi'=>$input['kode_bahan_setengah']));
    }else{
        $this->db->insert('opsi_master_bahan_setengah_jadi', $data);
    }
    
    echo $this->db->last_query();
    
}
public function delete_temp()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_master_bahan_setengah_jadi_temp', array('id' => $id));
}
public function delete_opsi()
{
    $id = $this->input->post('id');

    $this->db->delete('opsi_master_bahan_setengah_jadi', array('id' => $id));
}

public function simpan()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_bahan_setengah', 'Kode bahan baku', 'required');
    $this->form_validation->set_rules('nama_bahan_setengah_jadi', 'Nama bahan baku', 'required');
    $this->form_validation->set_rules('kode_rak', 'Kode Rak', 'required');
    $this->form_validation->set_rules('id_satuan_stok', 'Satuan', 'required');    
    $this->form_validation->set_rules('stok_minimal', 'Stok Minimal', 'required');      
        //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo warn_msg(validation_errors());

    } 
        //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $unit = $this->db->get_where('master_unit',array('kode_unit'=>$data['kode_unit']));
        $hasil_unit = $unit->row();
        $rak = $this->db->get_where('master_rak',array('kode_rak'=>$data['kode_rak']));
        $hasil_rak = $rak->row();
        $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_stok']));
        $this->db->select('kode_bahan_setengah_jadi');
        $bb = $this->db->get('master_setting');
        $hasil_bb = $bb->row();
        $hasil_satuan_stok = $satuan_stok->row();
        $data['kode_bahan_setengah_jadi'] = $hasil_bb->kode_bahan_setengah_jadi.'_'.$data['kode_bahan_setengah'];
        unset($data['kode_bahan_setengah']);
        unset($data['bahan']);
        unset($data['jumlah']);
        unset($data['id_satuan']);
        unset($data['nama_satuan']);
        $data['satuan_stok'] = $hasil_satuan_stok->nama;
        $data['nama_unit'] = $hasil_unit->nama_unit;
        $data['nama_rak'] = $hasil_rak->nama_rak;
        $data['real_stock'] = 0;
        $data['status'] = 'sendiri';
        $this->db->where('kode_bahan_setengah_jadi', $data['kode_bahan_setengah_jadi']);
        $get_temp = $this->db->get('opsi_master_bahan_setengah_jadi_temp');
        $hasil_temp = $get_temp->result();
        $jumlah_temp = count($hasil_temp);
        if($jumlah_temp > 0){
            foreach ($hasil_temp as $temp) {
                $opsi['kode_bahan_setengah_jadi'] = $temp->kode_bahan_setengah_jadi;
                $opsi['kode_bahan'] = $temp->kode_bahan;
                $opsi['nama_bahan'] = $temp->nama_bahan;
                $opsi['jenis_bahan'] = $temp->jenis_bahan;
                $opsi['jumlah'] = $temp->jumlah;
                $opsi['id_satuan'] = $temp->id_satuan;
                $opsi['nama_satuan'] = $temp->nama_satuan;
                $opsi['hpp'] = $temp->hpp;
                $opsi['rumus'] = $temp->rumus;
                $opsi['harga'] = $temp->harga;
                $this->db->insert("opsi_master_bahan_setengah_jadi", $opsi);
            }

            $delete = $this->db->delete('opsi_master_bahan_setengah_jadi_temp', array('kode_bahan_setengah_jadi' => $data['kode_bahan_setengah_jadi']));

            $this->db->insert("master_bahan_setengah_jadi", $data);
            //echo $this->db->last_query();
            echo json_encode(array('hasil' => 'berhasil'));
        }
        else{
            echo json_encode(array('hasil' => 'opsi'));
        }

    }
}

public function truncate(){
    $this->db->truncate('opsi_master_bahan_setengah_jadi_temp');
}

public function simpan_edit()
{
    $this->load->library('form_validation');
    $this->form_validation->set_rules('kode_bahan_setengah', 'Kode bahan baku', 'required');
    $this->form_validation->set_rules('nama_bahan_setengah_jadi', 'Nama bahan baku', 'required');
    $this->form_validation->set_rules('kode_rak', 'Kode Rak', 'required');
    $this->form_validation->set_rules('id_satuan_stok', 'Satuan', 'required');    
    $this->form_validation->set_rules('stok_minimal', 'Stok Minimal', 'required');      
        //jika form validasi berjalan salah maka tampilkan GAGAL
    if ($this->form_validation->run() == FALSE) {
        echo warn_msg(validation_errors());

    } 
        //jika form validasi berjalan benar maka inputkan data
    else {
        $data = $this->input->post(NULL, TRUE);
        $kode_bahan = $data['kode_bahan_setengah'];
     
        $get = $this->db->get_where('master_bahan_setengah_jadi' , ['kode_bahan_setengah_jadi' => $kode_bahan])->row();

        // $awal_hpp = $data['hpp'];

        $dt = [
            'bahan_type' => 2,
            'bahan_code' => $kode_bahan,
            'hpp_sebelumnya'=> $get->hpp ,
            'tgl_update'=> date('Y-m-d') 
        ];

        $this->db->insert('riwayat_hpp' , $dt);

        $unit = $this->db->get_where('master_unit',array('kode_unit'=>$data['kode_unit']));
        $hasil_unit = $unit->row();
        $rak = $this->db->get_where('master_rak',array('kode_rak'=>$data['kode_rak']));
        $hasil_rak = $rak->row();
        $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$data['id_satuan_stok']));
        $hasil_satuan_stok = $satuan_stok->row();
        $data['kode_bahan_setengah_jadi'] = $data['kode_bahan_setengah'];
        unset($data['kode_bahan_setengah']);
        unset($data['bahan']);
        unset($data['jumlah']);
        unset($data['id_satuan']);
        unset($data['nama_satuan']);
        $data['satuan_stok'] = $hasil_satuan_stok->nama;
        $data['nama_unit'] = $hasil_unit->nama_unit;
        $data['nama_rak'] = $hasil_rak->nama_rak;
        $data['real_stock'] = 0;

        $this->db->where('kode_bahan_setengah_jadi', $data['kode_bahan_setengah_jadi']);
        $get_temp = $this->db->get('opsi_master_bahan_setengah_jadi');
        $hasil_temp = $get_temp->result();
        $jumlah_temp = count($hasil_temp);
        if($jumlah_temp > 0){
            $this->db->update("master_bahan_setengah_jadi", $data, array('kode_bahan_setengah_jadi' => $data['kode_bahan_setengah_jadi']));
            echo json_encode(array('hasil' => 'berhasil'));
        }
        else{
            echo json_encode(array('hasil' => 'opsi'));
        }

    }
}   


public function hapus(){
    $kode = $this->input->post('id');
    $get_hapus = $this->db->get_where('master_bahan_setengah_jadi',array('id'=>$kode));
    $hasil_get = $get_hapus->row();
    $this->db->delete('opsi_master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$hasil_get->kode_bahan_setengah_jadi));
    $this->db->delete('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi'=>$hasil_get->kode_bahan_setengah_jadi,
        'kode_unit'=>$hasil_get->kode_unit,'kode_rak'=>$hasil_get->kode_rak));
}

public function get_satuan_stok(){
    $param = $this->input->post();
    $satuan_stok = $this->db->get_where('master_satuan',array('kode'=>$param['id_pembelian']));
    $hasil_satuan_stok = $satuan_stok->row();
    $dft_satuan = $this->db->get_where('master_satuan');
    $hasil_dft_satuan = $dft_satuan->result();
        #$desa = $desa->result();
    $list = '';
    foreach($hasil_dft_satuan as $daftar){
      $list .= 
      "
      <option value='$daftar->kode'>$daftar->nama</option>
      ";
  }
  $opt = "<option selected='true' value=''>Pilih Satuan Stok</option>";
  echo $opt.$list;
}

public function get_rak()
{
    $kode_unit = $this->input->post('kode_unit');
    $opt = "<option selected='true' value=''>--Pilih Rak--</option>";
    $data = $this->db->get_where('master_rak',array('kode_unit' => $kode_unit));
    foreach ($data->result() as $key => $value) {
        $opt .= "<option value=".$value->kode_rak.">".$value->nama_rak."</option>";
    }
    echo $opt;
}

public function get_kode()
{
    $kode_bahan_setengah = $this->input->post('kode_bahan_setengah');
    $query = $this->db->get_where('master_bahan_setengah_jadi',array('kode_bahan_setengah_jadi' => $kode_bahan_setengah))->num_rows();

    if($query > 0){
        echo "1";
    }
    else{
        echo "0";
    }
}
public function get_rupiah()
{
    $hpp = $this->input->post('hpp');
    echo format_rupiah($hpp);
}

public function get_table()
{
    $kode_default = $this->db->get('setting_gudang');
    $hasil_unit =$kode_default->row();
    $param=$hasil_unit->kode_unit;
    $start = (50*$this->input->post('page'));

    $data = $this->input->post();
    $this->db->limit(50, $start);
    if(@$data['nama_produk']){
        $produk = $data['nama_produk'];
        $this->db->like('nama_bahan_setengah_jadi',$produk,'both');
    }
    $this->db->where('status','sendiri');
    $bahan_setengah = $this->db->get_where('master_bahan_setengah_jadi',array('kode_unit' => $param));
    $hasil_bahan_setengah = $bahan_setengah->result();

    $nomor = $start+1;
    foreach($hasil_bahan_setengah as $daftar){
        ?>
        <tr>
            <td><?php echo $nomor; ?></td>
            <td><?php echo $daftar->kode_bahan_setengah_jadi; ?></td>
            <td><?php echo $daftar->nama_bahan_setengah_jadi; ?></td>

            <td><?php echo $daftar->nama_unit; ?></td>
            <td><?php echo $daftar->nama_rak; ?></td>
            <td><?php  if($daftar->status=='sendiri'){echo "Factory";}else{echo "Plasma";} ?></td>
            <td><?php echo $daftar->satuan_stok; ?></td>
            <td><?php echo $daftar->stok_minimal; ?></td>
            <td><?php echo format_rupiah($daftar->hpp); ?></td>
            <td><?php echo get_detail_edit_delete_string($daftar->id); ?></td>
        </tr>
        <?php $nomor++; 
    }
}

public function get_produk(){
    $this->load->view('bahan_setengah/cari_produk');
}
public function print_produk(){
    $this->load->view('bahan_setengah/print_sj');
}
public function get_hpp(){
    $get_kode = $this->input->post('kode_bahan_setengah');
    $table = $this->input->post('table');

    $this->db->where('kode_bahan_setengah_jadi', $get_kode);
    if($table == 'table_temp'){
        $get_temp = $this->db->get('opsi_master_bahan_setengah_jadi_temp');
    } else{
        $get_temp = $this->db->get('opsi_master_bahan_setengah_jadi');
    }
    $hasil_temp = $get_temp->result();

    $no = 1;
    $total = 0;
    foreach ($hasil_temp as $temp) {

        $no++;
        $total += $temp->harga;
    }

    $tambahan = $this->db->get('master_tambahan');
    $hasil_tambahan = $tambahan->result();
    $total_tambahan = 0;
    foreach($hasil_tambahan as $daftar){

     $total_tambahan += $daftar->harga; 
 } 

     if($total !=0){ 
        echo ($total * 10 / 100) + ($total + $total_tambahan) ; 
    }else{
        echo "0";
    } 


}

}
